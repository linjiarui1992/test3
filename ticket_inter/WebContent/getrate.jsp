<%@ page contentType="text/html; charset=UTF-8"
%><%@page import="org.jdom.Element,com.ccservice.b2b2c.base.zrate.Zrate"
%><%@page import="java.util.List,java.text.SimpleDateFormat,java.sql.Timestamp,com.ccservice.inter.server.Server"
%><%@page import="org.jdom.Document"
%><%@page import="java.io.StringReader"
%><%@page import="org.jdom.input.SAXBuilder" 
%><%@page import="java.io.ByteArrayOutputStream"
%><%@page import="java.io.InputStream"
%><%
	/**
	* 票盟政策同步接口
	
	<root><pol event="11" id="53e4bc2a2524003b0125245408250f58" aircom="GS" from="WNZ" to="XIY" flightno="" flighscope="0" routetype="1" policytype="" rate="10.6" fromtime="2009-11-24 0:00:00" totime="2009-12-2 0:00:00" applyclass="E;U;X;Q;M;L;K;H;B;Y" printtickfromtime="2009-11-24 00:00:00" printticktotime="2009-12-02 00:00:00" worktimefrom="100" worktimeto="2400" paytype="0" changerecord="0" automata="0" contype="1" note="出票快" />
	</root>

	*/
	InputStream in= request.getInputStream();
	ByteArrayOutputStream bout = new ByteArrayOutputStream();
	byte [] buf = new byte[1024];
	int len=0;
	while((len = in.read(buf))>0 ){	
		bout.write(buf,0,len);
	}
	
	String xml = new String(bout.toByteArray(),"UTF-8");
	System.out.println(xml);
	if(xml!=null && xml.length()>0){
		SAXBuilder build = new SAXBuilder();
		Document doc =build.build(new StringReader(xml));
		Element root = doc.getRootElement();
		
		List pols = root.getChildren("pol");
		
		
		
		for(Object o:pols){
			Element pol = (Element)o;
			Zrate rate = new Zrate();	
			try{
			rate.setOutid(pol.getAttributeValue("id"));
			
			if(pol.getAttributeValue("event").equals("13")){ //删除政策
				Server.getInstance().getAirService().excuteZrateBySql("delete from "+Zrate.TABLE + " where " +Zrate.COL_outid+ "='"+pol.getAttributeValue("id")+"'");
		
			}else{ //新增或替换政策
				rate.setAgentid(5L);//政策提供商id
				
				rate.setCreatetime(new Timestamp(System.currentTimeMillis()));
				rate.setCreateuser("push");
				rate.setModifytime(new Timestamp(System.currentTimeMillis()));
				rate.setModifyuser("push");

				rate.setCabincode(pol.getAttributeValue("applyclass"));
				rate.setAircompanycode(pol.getAttributeValue("aircom")); //航空公司
				rate.setDepartureport(pol.getAttributeValue("from")); //起始机场
				rate.setArrivalport(pol.getAttributeValue("to")); //到达机场
				rate.setFlightnumber(pol.getAttributeValue("flightno"));//航线
				rate.setTraveltype(Integer.parseInt(pol.getAttributeValue("routetype")));
				rate.setType(Integer.parseInt(pol.getAttributeValue("flighscope"))); //航线规则 0全部，1适用,2不适用
				rate.setRatevalue(Float.parseFloat(pol.getAttributeValue("rate")));  //政策
				rate.setBegindate(new Timestamp( new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(pol.getAttributeValue("fromtime")).getTime())); //开始时间
				rate.setEnddate(new Timestamp( new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(pol.getAttributeValue("totime")).getTime())); //结束时间
				
				rate.setIssuedstartdate(new Timestamp( new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(pol.getAttributeValue("printtickfromtime")).getTime())); //出票开始时间
				rate.setIssuedendate(new Timestamp( new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(pol.getAttributeValue("printticktotime")).getTime()));      //出票结束时间
				rate.setRemark(pol.getAttributeValue("note"));
				rate.setGeneral(Long.parseLong(pol.getAttributeValue("contype"))-1); //政策类型 0 普通 1特价 2 团队 3 儿童
				
				//B2B-ET 或BSP-ET 或B2B-ET/BSP-ET
				String ptype = pol.getAttributeValue("policytype");
				if(ptype!=null){
					if(ptype.equals("BSP-ET")){
						rate.setTickettype(1) ;//b2b
					}else if(ptype.equals("B2B-ET/BSP-ET")){
						rate.setTickettype(3) ;//b2b
					}else{
						rate.setTickettype(0); 
					}
				}
				
		
				rate.setIstype(Long.parseLong(pol.getAttributeValue("isspecial"))); //1为特殊政策
				
				List<Zrate> list = Server.getInstance().getAirService().findAllZrate(" where "+ Zrate.COL_outid+ "='"+rate.getOutid()+"'", "", -1, 0);
				if(list==null || list.isEmpty()){
					Server.getInstance().getAirService().createZrate(rate);
				}else if(list.size()==1){
					rate.setId(list.get(0).getId());
					Server.getInstance().getAirService().updateZrateIgnoreNull(rate);
				}else{
					Server.getInstance().getAirService().excuteZrateBySql("delete from "+Zrate.TABLE + " where " + Zrate.COL_outid+ "='"+rate.getOutid()+"'");
					Server.getInstance().getAirService().createZrate(rate);
				}
	
			}
			}catch (Exception e2){
				e2.printStackTrace();
			}
		}
	
	}
	
%>0