<%@page import="org.apache.commons.logging.LogFactory"%>
<%@page import="org.apache.commons.logging.Log"
%><%@page import="com.ccservice.b2b2c.base.orderinfo.Orderinfo"%>
<%@ page contentType="text/html; charset=UTF-8"
%><%@page import="org.jdom.Element,com.ccservice.b2b2c.base.zrate.Zrate"
%><%@page import="java.util.List,java.text.SimpleDateFormat,java.sql.Timestamp,com.ccservice.inter.server.Server"
%><%@page import="org.jdom.Document"
%><%@page import="java.io.StringReader"
%><%@page import="org.jdom.input.SAXBuilder" 
%><%@page import="java.io.ByteArrayOutputStream"
%><%@page import="java.io.InputStream"
%><%
	final Log log = LogFactory.getLog("orderstate");  
	/**
	* 票盟订单状态同步接口
	10：支付完成，等待出票
	12：无法出票
	13：出票完成
	14：更换编码出票完成
	21：退票处理中
	22：无法退票
	31：废票处理中
	32：无法废票
	90：完成退费票
	99：交易取消退款

	*cmd=SUBMITORDERSTATUS&Status=10&orderId=100426201204168&transNo=2010042623309612
	*/
	log.debug(request.getRequestURI());
	String cmd = request.getParameter("cmd");
	String Status = request.getParameter("Status");
	String orderId = request.getParameter("orderId");
	String transNo = request.getParameter("transNo");
	Orderinfo order =(Orderinfo) Server.getInstance().getAirService().findAllOrderinfo("Where " +Orderinfo.COL_extorderid + "='"+orderId+"'","",-1,0).get(0);
	
	if(Status.equals("13")||Status.equals("14")){
		order.setExtorderstatus(1);
		order.setOrderstatus(3);
		
	}else{
		order.setExtorderstatus(Integer.parseInt(Status));
	
	}
	Server.getInstance().getAirService().updateOrderinfoIgnoreNull(order);
	
	
%>0