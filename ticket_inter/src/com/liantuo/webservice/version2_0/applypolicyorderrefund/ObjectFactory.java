
package com.liantuo.webservice.version2_0.applypolicyorderrefund;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.liantuo.webservice.version2_0.applypolicyorderrefund package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ApplyPolicyOrderRefundReplyReturnCode_QNAME = new QName("http://applypolicyorderrefund.version2_0.webservice.liantuo.com", "returnCode");
    private final static QName _ApplyPolicyOrderRefundReplyParam2_QNAME = new QName("http://applypolicyorderrefund.version2_0.webservice.liantuo.com", "param2");
    private final static QName _ApplyPolicyOrderRefundReplyParam1_QNAME = new QName("http://applypolicyorderrefund.version2_0.webservice.liantuo.com", "param1");
    private final static QName _ApplyPolicyOrderRefundReplyParam3_QNAME = new QName("http://applypolicyorderrefund.version2_0.webservice.liantuo.com", "param3");
    private final static QName _ApplyPolicyOrderRefundReplyReturnMessage_QNAME = new QName("http://applypolicyorderrefund.version2_0.webservice.liantuo.com", "returnMessage");
    private final static QName _ApplyPolicyOrderRefundRequestSegment_QNAME = new QName("http://applypolicyorderrefund.version2_0.webservice.liantuo.com", "segment");
    private final static QName _ApplyPolicyOrderRefundRequestRefundAnnexDirPath_QNAME = new QName("http://applypolicyorderrefund.version2_0.webservice.liantuo.com", "refundAnnexDirPath");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.liantuo.webservice.version2_0.applypolicyorderrefund
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ArrayOfString }
     * 
     */
    public ArrayOfString createArrayOfString() {
        return new ArrayOfString();
    }

    /**
     * Create an instance of {@link ApplyPolicyOrderRefundResponse }
     * 
     */
    public ApplyPolicyOrderRefundResponse createApplyPolicyOrderRefundResponse() {
        return new ApplyPolicyOrderRefundResponse();
    }

    /**
     * Create an instance of {@link ApplyPolicyOrderRefundReply }
     * 
     */
    public ApplyPolicyOrderRefundReply createApplyPolicyOrderRefundReply() {
        return new ApplyPolicyOrderRefundReply();
    }

    /**
     * Create an instance of {@link ApplyPolicyOrderRefundRequest }
     * 
     */
    public ApplyPolicyOrderRefundRequest createApplyPolicyOrderRefundRequest() {
        return new ApplyPolicyOrderRefundRequest();
    }

    /**
     * Create an instance of {@link ApplyPolicyOrderRefund }
     * 
     */
    public ApplyPolicyOrderRefund createApplyPolicyOrderRefund() {
        return new ApplyPolicyOrderRefund();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://applypolicyorderrefund.version2_0.webservice.liantuo.com", name = "returnCode", scope = ApplyPolicyOrderRefundReply.class)
    public JAXBElement<String> createApplyPolicyOrderRefundReplyReturnCode(String value) {
        return new JAXBElement<String>(_ApplyPolicyOrderRefundReplyReturnCode_QNAME, String.class, ApplyPolicyOrderRefundReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://applypolicyorderrefund.version2_0.webservice.liantuo.com", name = "param2", scope = ApplyPolicyOrderRefundReply.class)
    public JAXBElement<String> createApplyPolicyOrderRefundReplyParam2(String value) {
        return new JAXBElement<String>(_ApplyPolicyOrderRefundReplyParam2_QNAME, String.class, ApplyPolicyOrderRefundReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://applypolicyorderrefund.version2_0.webservice.liantuo.com", name = "param1", scope = ApplyPolicyOrderRefundReply.class)
    public JAXBElement<String> createApplyPolicyOrderRefundReplyParam1(String value) {
        return new JAXBElement<String>(_ApplyPolicyOrderRefundReplyParam1_QNAME, String.class, ApplyPolicyOrderRefundReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://applypolicyorderrefund.version2_0.webservice.liantuo.com", name = "param3", scope = ApplyPolicyOrderRefundReply.class)
    public JAXBElement<String> createApplyPolicyOrderRefundReplyParam3(String value) {
        return new JAXBElement<String>(_ApplyPolicyOrderRefundReplyParam3_QNAME, String.class, ApplyPolicyOrderRefundReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://applypolicyorderrefund.version2_0.webservice.liantuo.com", name = "returnMessage", scope = ApplyPolicyOrderRefundReply.class)
    public JAXBElement<String> createApplyPolicyOrderRefundReplyReturnMessage(String value) {
        return new JAXBElement<String>(_ApplyPolicyOrderRefundReplyReturnMessage_QNAME, String.class, ApplyPolicyOrderRefundReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://applypolicyorderrefund.version2_0.webservice.liantuo.com", name = "param2", scope = ApplyPolicyOrderRefundRequest.class)
    public JAXBElement<String> createApplyPolicyOrderRefundRequestParam2(String value) {
        return new JAXBElement<String>(_ApplyPolicyOrderRefundReplyParam2_QNAME, String.class, ApplyPolicyOrderRefundRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://applypolicyorderrefund.version2_0.webservice.liantuo.com", name = "param1", scope = ApplyPolicyOrderRefundRequest.class)
    public JAXBElement<String> createApplyPolicyOrderRefundRequestParam1(String value) {
        return new JAXBElement<String>(_ApplyPolicyOrderRefundReplyParam1_QNAME, String.class, ApplyPolicyOrderRefundRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://applypolicyorderrefund.version2_0.webservice.liantuo.com", name = "segment", scope = ApplyPolicyOrderRefundRequest.class)
    public JAXBElement<String> createApplyPolicyOrderRefundRequestSegment(String value) {
        return new JAXBElement<String>(_ApplyPolicyOrderRefundRequestSegment_QNAME, String.class, ApplyPolicyOrderRefundRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://applypolicyorderrefund.version2_0.webservice.liantuo.com", name = "param3", scope = ApplyPolicyOrderRefundRequest.class)
    public JAXBElement<String> createApplyPolicyOrderRefundRequestParam3(String value) {
        return new JAXBElement<String>(_ApplyPolicyOrderRefundReplyParam3_QNAME, String.class, ApplyPolicyOrderRefundRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://applypolicyorderrefund.version2_0.webservice.liantuo.com", name = "refundAnnexDirPath", scope = ApplyPolicyOrderRefundRequest.class)
    public JAXBElement<String> createApplyPolicyOrderRefundRequestRefundAnnexDirPath(String value) {
        return new JAXBElement<String>(_ApplyPolicyOrderRefundRequestRefundAnnexDirPath_QNAME, String.class, ApplyPolicyOrderRefundRequest.class, value);
    }

}
