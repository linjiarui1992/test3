
package com.liantuo.webservice.version2_0.createpolicyorderbyspecialpnr;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.liantuo.webservice.version2_0.createpolicyorderbyspecialpnr package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PaymentInfoPaymentUrl_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "paymentUrl");
    private final static QName _PaymentInfoPayerAccount_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "payerAccount");
    private final static QName _PaymentInfoSettlePrice_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "settlePrice");
    private final static QName _PaymentInfoParam1_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "param1");
    private final static QName _PaymentInfoTradeNo_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "tradeNo");
    private final static QName _WSPolicyOrderSequenceNo_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "sequenceNo");
    private final static QName _WSPolicyOrderPnrTxt_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "pnrTxt");
    private final static QName _WSPolicyOrderCommisionInfo_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "commisionInfo");
    private final static QName _WSPolicyOrderIncreaseSystemCharge_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "increaseSystemCharge");
    private final static QName _WSPolicyOrderFlightInfoList_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "flightInfoList");
    private final static QName _WSPolicyOrderPaymentInfo_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "paymentInfo");
    private final static QName _WSPolicyOrderPnrNo_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "pnrNo");
    private final static QName _WSPolicyOrderSystemAlipayAccount_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "systemAlipayAccount");
    private final static QName _WSPolicyOrderParam3_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "param3");
    private final static QName _WSPolicyOrderPassengerList_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "passengerList");
    private final static QName _WSPolicyOrderParam2_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "param2");
    private final static QName _CreatePolicyOrderBySpecialPnrRequestPataTxt_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "pataTxt");
    private final static QName _CreatePolicyOrderBySpecialPnrRequestBPnrNo_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "BPnrNo");
    private final static QName _CreatePolicyOrderBySpecialPnrRequestTicketPrice_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "ticketPrice");
    private final static QName _CreatePolicyOrderBySpecialPnrRequestRouteType_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "routeType");
    private final static QName _CreatePolicyOrderBySpecialPnrRequestOilPrice_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "oilPrice");
    private final static QName _CreatePolicyOrderBySpecialPnrRequestNotifiedUrl_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "notifiedUrl");
    private final static QName _CreatePolicyOrderBySpecialPnrRequestPassengers_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "passengers");
    private final static QName _CreatePolicyOrderBySpecialPnrRequestBPnrTxt_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "BPnrTxt");
    private final static QName _CreatePolicyOrderBySpecialPnrRequestIsGroup_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "isGroup");
    private final static QName _CreatePolicyOrderBySpecialPnrRequestPaymentReturnUrl_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "paymentReturnUrl");
    private final static QName _CreatePolicyOrderBySpecialPnrRequestPolicyId_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "policyId");
    private final static QName _CreatePolicyOrderBySpecialPnrRequestB2CCreatorCn_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "b2cCreatorCn");
    private final static QName _CreatePolicyOrderBySpecialPnrRequestBuildPrice_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "buildPrice");
    private final static QName _CreatePolicyOrderBySpecialPnrRequestSegments_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "segments");
    private final static QName _PassengerInfoIdentityType_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "identityType");
    private final static QName _PassengerInfoBirth_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "birth");
    private final static QName _PassengerInfoIdentityNo_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "identityNo");
    private final static QName _PassengerInfoName_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "name");
    private final static QName _SegmentInfoArrivalDate_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "arrivalDate");
    private final static QName _SegmentInfoArrivalTime_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "arrivalTime");
    private final static QName _SegmentInfoFlightNo_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "flightNo");
    private final static QName _SegmentInfoArrivalAirport_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "arrivalAirport");
    private final static QName _SegmentInfoDepartureAirport_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "departureAirport");
    private final static QName _SegmentInfoSeatClass_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "seatClass");
    private final static QName _SegmentInfoDepartureTime_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "departureTime");
    private final static QName _SegmentInfoPlaneModle_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "planeModle");
    private final static QName _SegmentInfoSeatDiscount_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "seatDiscount");
    private final static QName _SegmentInfoDepartureDate_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "departureDate");
    private final static QName _WSPassengerFuelTax_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "fuelTax");
    private final static QName _WSPassengerCertificateNum_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "certificateNum");
    private final static QName _WSPassengerTicketNo_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "ticketNo");
    private final static QName _WSPassengerRefundStatus_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "refundStatus");
    private final static QName _WSPassengerAirportTax_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "airportTax");
    private final static QName _WSPassengerCertificateType_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "certificateType");
    private final static QName _WSPassengerPassengerType_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "passengerType");
    private final static QName _CreatePolicyOrderBySpecialPnrReplyReturnMessage_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "returnMessage");
    private final static QName _CreatePolicyOrderBySpecialPnrReplyReturnCode_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "returnCode");
    private final static QName _FlightInfoArrCode_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "arrCode");
    private final static QName _FlightInfoDepDate_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "depDate");
    private final static QName _FlightInfoArrDate_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "arrDate");
    private final static QName _FlightInfoPlaneModel_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "planeModel");
    private final static QName _FlightInfoDepCode_QNAME = new QName("http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", "depCode");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.liantuo.webservice.version2_0.createpolicyorderbyspecialpnr
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreatePolicyOrderBySpecialPnrRequest }
     * 
     */
    public CreatePolicyOrderBySpecialPnrRequest createCreatePolicyOrderBySpecialPnrRequest() {
        return new CreatePolicyOrderBySpecialPnrRequest();
    }

    /**
     * Create an instance of {@link ArrayOfFlightInfo }
     * 
     */
    public ArrayOfFlightInfo createArrayOfFlightInfo() {
        return new ArrayOfFlightInfo();
    }

    /**
     * Create an instance of {@link PassengerInfo }
     * 
     */
    public PassengerInfo createPassengerInfo() {
        return new PassengerInfo();
    }

    /**
     * Create an instance of {@link SegmentInfo }
     * 
     */
    public SegmentInfo createSegmentInfo() {
        return new SegmentInfo();
    }

    /**
     * Create an instance of {@link ArrayOfPassengerInfo }
     * 
     */
    public ArrayOfPassengerInfo createArrayOfPassengerInfo() {
        return new ArrayOfPassengerInfo();
    }

    /**
     * Create an instance of {@link FlightInfo }
     * 
     */
    public FlightInfo createFlightInfo() {
        return new FlightInfo();
    }

    /**
     * Create an instance of {@link PaymentInfo }
     * 
     */
    public PaymentInfo createPaymentInfo() {
        return new PaymentInfo();
    }

    /**
     * Create an instance of {@link CreatePolicyOrderBySpecialPnrResponse }
     * 
     */
    public CreatePolicyOrderBySpecialPnrResponse createCreatePolicyOrderBySpecialPnrResponse() {
        return new CreatePolicyOrderBySpecialPnrResponse();
    }

    /**
     * Create an instance of {@link ArrayOfSegmentInfo }
     * 
     */
    public ArrayOfSegmentInfo createArrayOfSegmentInfo() {
        return new ArrayOfSegmentInfo();
    }

    /**
     * Create an instance of {@link CreatePolicyOrderBySpecialPnr }
     * 
     */
    public CreatePolicyOrderBySpecialPnr createCreatePolicyOrderBySpecialPnr() {
        return new CreatePolicyOrderBySpecialPnr();
    }

    /**
     * Create an instance of {@link WSPolicyOrder }
     * 
     */
    public WSPolicyOrder createWSPolicyOrder() {
        return new WSPolicyOrder();
    }

    /**
     * Create an instance of {@link ArrayOfWSPassenger }
     * 
     */
    public ArrayOfWSPassenger createArrayOfWSPassenger() {
        return new ArrayOfWSPassenger();
    }

    /**
     * Create an instance of {@link WSPassenger }
     * 
     */
    public WSPassenger createWSPassenger() {
        return new WSPassenger();
    }

    /**
     * Create an instance of {@link CreatePolicyOrderBySpecialPnrReply }
     * 
     */
    public CreatePolicyOrderBySpecialPnrReply createCreatePolicyOrderBySpecialPnrReply() {
        return new CreatePolicyOrderBySpecialPnrReply();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "paymentUrl", scope = PaymentInfo.class)
    public JAXBElement<String> createPaymentInfoPaymentUrl(String value) {
        return new JAXBElement<String>(_PaymentInfoPaymentUrl_QNAME, String.class, PaymentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "payerAccount", scope = PaymentInfo.class)
    public JAXBElement<String> createPaymentInfoPayerAccount(String value) {
        return new JAXBElement<String>(_PaymentInfoPayerAccount_QNAME, String.class, PaymentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "settlePrice", scope = PaymentInfo.class)
    public JAXBElement<Double> createPaymentInfoSettlePrice(Double value) {
        return new JAXBElement<Double>(_PaymentInfoSettlePrice_QNAME, Double.class, PaymentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "param1", scope = PaymentInfo.class)
    public JAXBElement<String> createPaymentInfoParam1(String value) {
        return new JAXBElement<String>(_PaymentInfoParam1_QNAME, String.class, PaymentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "tradeNo", scope = PaymentInfo.class)
    public JAXBElement<String> createPaymentInfoTradeNo(String value) {
        return new JAXBElement<String>(_PaymentInfoTradeNo_QNAME, String.class, PaymentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "sequenceNo", scope = WSPolicyOrder.class)
    public JAXBElement<String> createWSPolicyOrderSequenceNo(String value) {
        return new JAXBElement<String>(_WSPolicyOrderSequenceNo_QNAME, String.class, WSPolicyOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "pnrTxt", scope = WSPolicyOrder.class)
    public JAXBElement<String> createWSPolicyOrderPnrTxt(String value) {
        return new JAXBElement<String>(_WSPolicyOrderPnrTxt_QNAME, String.class, WSPolicyOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "commisionInfo", scope = WSPolicyOrder.class)
    public JAXBElement<String> createWSPolicyOrderCommisionInfo(String value) {
        return new JAXBElement<String>(_WSPolicyOrderCommisionInfo_QNAME, String.class, WSPolicyOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "increaseSystemCharge", scope = WSPolicyOrder.class)
    public JAXBElement<String> createWSPolicyOrderIncreaseSystemCharge(String value) {
        return new JAXBElement<String>(_WSPolicyOrderIncreaseSystemCharge_QNAME, String.class, WSPolicyOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFlightInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "flightInfoList", scope = WSPolicyOrder.class)
    public JAXBElement<ArrayOfFlightInfo> createWSPolicyOrderFlightInfoList(ArrayOfFlightInfo value) {
        return new JAXBElement<ArrayOfFlightInfo>(_WSPolicyOrderFlightInfoList_QNAME, ArrayOfFlightInfo.class, WSPolicyOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PaymentInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "paymentInfo", scope = WSPolicyOrder.class)
    public JAXBElement<PaymentInfo> createWSPolicyOrderPaymentInfo(PaymentInfo value) {
        return new JAXBElement<PaymentInfo>(_WSPolicyOrderPaymentInfo_QNAME, PaymentInfo.class, WSPolicyOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "pnrNo", scope = WSPolicyOrder.class)
    public JAXBElement<String> createWSPolicyOrderPnrNo(String value) {
        return new JAXBElement<String>(_WSPolicyOrderPnrNo_QNAME, String.class, WSPolicyOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "systemAlipayAccount", scope = WSPolicyOrder.class)
    public JAXBElement<String> createWSPolicyOrderSystemAlipayAccount(String value) {
        return new JAXBElement<String>(_WSPolicyOrderSystemAlipayAccount_QNAME, String.class, WSPolicyOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "param3", scope = WSPolicyOrder.class)
    public JAXBElement<String> createWSPolicyOrderParam3(String value) {
        return new JAXBElement<String>(_WSPolicyOrderParam3_QNAME, String.class, WSPolicyOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfWSPassenger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "passengerList", scope = WSPolicyOrder.class)
    public JAXBElement<ArrayOfWSPassenger> createWSPolicyOrderPassengerList(ArrayOfWSPassenger value) {
        return new JAXBElement<ArrayOfWSPassenger>(_WSPolicyOrderPassengerList_QNAME, ArrayOfWSPassenger.class, WSPolicyOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "param1", scope = WSPolicyOrder.class)
    public JAXBElement<String> createWSPolicyOrderParam1(String value) {
        return new JAXBElement<String>(_PaymentInfoParam1_QNAME, String.class, WSPolicyOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "param2", scope = WSPolicyOrder.class)
    public JAXBElement<String> createWSPolicyOrderParam2(String value) {
        return new JAXBElement<String>(_WSPolicyOrderParam2_QNAME, String.class, WSPolicyOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "pataTxt", scope = CreatePolicyOrderBySpecialPnrRequest.class)
    public JAXBElement<String> createCreatePolicyOrderBySpecialPnrRequestPataTxt(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderBySpecialPnrRequestPataTxt_QNAME, String.class, CreatePolicyOrderBySpecialPnrRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "BPnrNo", scope = CreatePolicyOrderBySpecialPnrRequest.class)
    public JAXBElement<String> createCreatePolicyOrderBySpecialPnrRequestBPnrNo(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderBySpecialPnrRequestBPnrNo_QNAME, String.class, CreatePolicyOrderBySpecialPnrRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "ticketPrice", scope = CreatePolicyOrderBySpecialPnrRequest.class)
    public JAXBElement<String> createCreatePolicyOrderBySpecialPnrRequestTicketPrice(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderBySpecialPnrRequestTicketPrice_QNAME, String.class, CreatePolicyOrderBySpecialPnrRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "routeType", scope = CreatePolicyOrderBySpecialPnrRequest.class)
    public JAXBElement<String> createCreatePolicyOrderBySpecialPnrRequestRouteType(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderBySpecialPnrRequestRouteType_QNAME, String.class, CreatePolicyOrderBySpecialPnrRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "oilPrice", scope = CreatePolicyOrderBySpecialPnrRequest.class)
    public JAXBElement<String> createCreatePolicyOrderBySpecialPnrRequestOilPrice(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderBySpecialPnrRequestOilPrice_QNAME, String.class, CreatePolicyOrderBySpecialPnrRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "pnrNo", scope = CreatePolicyOrderBySpecialPnrRequest.class)
    public JAXBElement<String> createCreatePolicyOrderBySpecialPnrRequestPnrNo(String value) {
        return new JAXBElement<String>(_WSPolicyOrderPnrNo_QNAME, String.class, CreatePolicyOrderBySpecialPnrRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "notifiedUrl", scope = CreatePolicyOrderBySpecialPnrRequest.class)
    public JAXBElement<String> createCreatePolicyOrderBySpecialPnrRequestNotifiedUrl(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderBySpecialPnrRequestNotifiedUrl_QNAME, String.class, CreatePolicyOrderBySpecialPnrRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfPassengerInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "passengers", scope = CreatePolicyOrderBySpecialPnrRequest.class)
    public JAXBElement<ArrayOfPassengerInfo> createCreatePolicyOrderBySpecialPnrRequestPassengers(ArrayOfPassengerInfo value) {
        return new JAXBElement<ArrayOfPassengerInfo>(_CreatePolicyOrderBySpecialPnrRequestPassengers_QNAME, ArrayOfPassengerInfo.class, CreatePolicyOrderBySpecialPnrRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "BPnrTxt", scope = CreatePolicyOrderBySpecialPnrRequest.class)
    public JAXBElement<String> createCreatePolicyOrderBySpecialPnrRequestBPnrTxt(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderBySpecialPnrRequestBPnrTxt_QNAME, String.class, CreatePolicyOrderBySpecialPnrRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "isGroup", scope = CreatePolicyOrderBySpecialPnrRequest.class)
    public JAXBElement<Boolean> createCreatePolicyOrderBySpecialPnrRequestIsGroup(Boolean value) {
        return new JAXBElement<Boolean>(_CreatePolicyOrderBySpecialPnrRequestIsGroup_QNAME, Boolean.class, CreatePolicyOrderBySpecialPnrRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "paymentReturnUrl", scope = CreatePolicyOrderBySpecialPnrRequest.class)
    public JAXBElement<String> createCreatePolicyOrderBySpecialPnrRequestPaymentReturnUrl(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderBySpecialPnrRequestPaymentReturnUrl_QNAME, String.class, CreatePolicyOrderBySpecialPnrRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "policyId", scope = CreatePolicyOrderBySpecialPnrRequest.class)
    public JAXBElement<Integer> createCreatePolicyOrderBySpecialPnrRequestPolicyId(Integer value) {
        return new JAXBElement<Integer>(_CreatePolicyOrderBySpecialPnrRequestPolicyId_QNAME, Integer.class, CreatePolicyOrderBySpecialPnrRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "pnrTxt", scope = CreatePolicyOrderBySpecialPnrRequest.class)
    public JAXBElement<String> createCreatePolicyOrderBySpecialPnrRequestPnrTxt(String value) {
        return new JAXBElement<String>(_WSPolicyOrderPnrTxt_QNAME, String.class, CreatePolicyOrderBySpecialPnrRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "b2cCreatorCn", scope = CreatePolicyOrderBySpecialPnrRequest.class)
    public JAXBElement<String> createCreatePolicyOrderBySpecialPnrRequestB2CCreatorCn(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderBySpecialPnrRequestB2CCreatorCn_QNAME, String.class, CreatePolicyOrderBySpecialPnrRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "buildPrice", scope = CreatePolicyOrderBySpecialPnrRequest.class)
    public JAXBElement<String> createCreatePolicyOrderBySpecialPnrRequestBuildPrice(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderBySpecialPnrRequestBuildPrice_QNAME, String.class, CreatePolicyOrderBySpecialPnrRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "param3", scope = CreatePolicyOrderBySpecialPnrRequest.class)
    public JAXBElement<String> createCreatePolicyOrderBySpecialPnrRequestParam3(String value) {
        return new JAXBElement<String>(_WSPolicyOrderParam3_QNAME, String.class, CreatePolicyOrderBySpecialPnrRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSegmentInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "segments", scope = CreatePolicyOrderBySpecialPnrRequest.class)
    public JAXBElement<ArrayOfSegmentInfo> createCreatePolicyOrderBySpecialPnrRequestSegments(ArrayOfSegmentInfo value) {
        return new JAXBElement<ArrayOfSegmentInfo>(_CreatePolicyOrderBySpecialPnrRequestSegments_QNAME, ArrayOfSegmentInfo.class, CreatePolicyOrderBySpecialPnrRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "param1", scope = CreatePolicyOrderBySpecialPnrRequest.class)
    public JAXBElement<String> createCreatePolicyOrderBySpecialPnrRequestParam1(String value) {
        return new JAXBElement<String>(_PaymentInfoParam1_QNAME, String.class, CreatePolicyOrderBySpecialPnrRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "param2", scope = CreatePolicyOrderBySpecialPnrRequest.class)
    public JAXBElement<String> createCreatePolicyOrderBySpecialPnrRequestParam2(String value) {
        return new JAXBElement<String>(_WSPolicyOrderParam2_QNAME, String.class, CreatePolicyOrderBySpecialPnrRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "identityType", scope = PassengerInfo.class)
    public JAXBElement<String> createPassengerInfoIdentityType(String value) {
        return new JAXBElement<String>(_PassengerInfoIdentityType_QNAME, String.class, PassengerInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "birth", scope = PassengerInfo.class)
    public JAXBElement<String> createPassengerInfoBirth(String value) {
        return new JAXBElement<String>(_PassengerInfoBirth_QNAME, String.class, PassengerInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "param3", scope = PassengerInfo.class)
    public JAXBElement<String> createPassengerInfoParam3(String value) {
        return new JAXBElement<String>(_WSPolicyOrderParam3_QNAME, String.class, PassengerInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "identityNo", scope = PassengerInfo.class)
    public JAXBElement<String> createPassengerInfoIdentityNo(String value) {
        return new JAXBElement<String>(_PassengerInfoIdentityNo_QNAME, String.class, PassengerInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "name", scope = PassengerInfo.class)
    public JAXBElement<String> createPassengerInfoName(String value) {
        return new JAXBElement<String>(_PassengerInfoName_QNAME, String.class, PassengerInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "param1", scope = PassengerInfo.class)
    public JAXBElement<String> createPassengerInfoParam1(String value) {
        return new JAXBElement<String>(_PaymentInfoParam1_QNAME, String.class, PassengerInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "param2", scope = PassengerInfo.class)
    public JAXBElement<String> createPassengerInfoParam2(String value) {
        return new JAXBElement<String>(_WSPolicyOrderParam2_QNAME, String.class, PassengerInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "arrivalDate", scope = SegmentInfo.class)
    public JAXBElement<String> createSegmentInfoArrivalDate(String value) {
        return new JAXBElement<String>(_SegmentInfoArrivalDate_QNAME, String.class, SegmentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "arrivalTime", scope = SegmentInfo.class)
    public JAXBElement<String> createSegmentInfoArrivalTime(String value) {
        return new JAXBElement<String>(_SegmentInfoArrivalTime_QNAME, String.class, SegmentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "flightNo", scope = SegmentInfo.class)
    public JAXBElement<String> createSegmentInfoFlightNo(String value) {
        return new JAXBElement<String>(_SegmentInfoFlightNo_QNAME, String.class, SegmentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "arrivalAirport", scope = SegmentInfo.class)
    public JAXBElement<String> createSegmentInfoArrivalAirport(String value) {
        return new JAXBElement<String>(_SegmentInfoArrivalAirport_QNAME, String.class, SegmentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "departureAirport", scope = SegmentInfo.class)
    public JAXBElement<String> createSegmentInfoDepartureAirport(String value) {
        return new JAXBElement<String>(_SegmentInfoDepartureAirport_QNAME, String.class, SegmentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "seatClass", scope = SegmentInfo.class)
    public JAXBElement<String> createSegmentInfoSeatClass(String value) {
        return new JAXBElement<String>(_SegmentInfoSeatClass_QNAME, String.class, SegmentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "departureTime", scope = SegmentInfo.class)
    public JAXBElement<String> createSegmentInfoDepartureTime(String value) {
        return new JAXBElement<String>(_SegmentInfoDepartureTime_QNAME, String.class, SegmentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "planeModle", scope = SegmentInfo.class)
    public JAXBElement<String> createSegmentInfoPlaneModle(String value) {
        return new JAXBElement<String>(_SegmentInfoPlaneModle_QNAME, String.class, SegmentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "seatDiscount", scope = SegmentInfo.class)
    public JAXBElement<String> createSegmentInfoSeatDiscount(String value) {
        return new JAXBElement<String>(_SegmentInfoSeatDiscount_QNAME, String.class, SegmentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "param3", scope = SegmentInfo.class)
    public JAXBElement<String> createSegmentInfoParam3(String value) {
        return new JAXBElement<String>(_WSPolicyOrderParam3_QNAME, String.class, SegmentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "param1", scope = SegmentInfo.class)
    public JAXBElement<String> createSegmentInfoParam1(String value) {
        return new JAXBElement<String>(_PaymentInfoParam1_QNAME, String.class, SegmentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "departureDate", scope = SegmentInfo.class)
    public JAXBElement<String> createSegmentInfoDepartureDate(String value) {
        return new JAXBElement<String>(_SegmentInfoDepartureDate_QNAME, String.class, SegmentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "param2", scope = SegmentInfo.class)
    public JAXBElement<String> createSegmentInfoParam2(String value) {
        return new JAXBElement<String>(_WSPolicyOrderParam2_QNAME, String.class, SegmentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "fuelTax", scope = WSPassenger.class)
    public JAXBElement<Double> createWSPassengerFuelTax(Double value) {
        return new JAXBElement<Double>(_WSPassengerFuelTax_QNAME, Double.class, WSPassenger.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "certificateNum", scope = WSPassenger.class)
    public JAXBElement<String> createWSPassengerCertificateNum(String value) {
        return new JAXBElement<String>(_WSPassengerCertificateNum_QNAME, String.class, WSPassenger.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "ticketPrice", scope = WSPassenger.class)
    public JAXBElement<Double> createWSPassengerTicketPrice(Double value) {
        return new JAXBElement<Double>(_CreatePolicyOrderBySpecialPnrRequestTicketPrice_QNAME, Double.class, WSPassenger.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "settlePrice", scope = WSPassenger.class)
    public JAXBElement<Double> createWSPassengerSettlePrice(Double value) {
        return new JAXBElement<Double>(_PaymentInfoSettlePrice_QNAME, Double.class, WSPassenger.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "ticketNo", scope = WSPassenger.class)
    public JAXBElement<String> createWSPassengerTicketNo(String value) {
        return new JAXBElement<String>(_WSPassengerTicketNo_QNAME, String.class, WSPassenger.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "name", scope = WSPassenger.class)
    public JAXBElement<String> createWSPassengerName(String value) {
        return new JAXBElement<String>(_PassengerInfoName_QNAME, String.class, WSPassenger.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "refundStatus", scope = WSPassenger.class)
    public JAXBElement<Integer> createWSPassengerRefundStatus(Integer value) {
        return new JAXBElement<Integer>(_WSPassengerRefundStatus_QNAME, Integer.class, WSPassenger.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "airportTax", scope = WSPassenger.class)
    public JAXBElement<Double> createWSPassengerAirportTax(Double value) {
        return new JAXBElement<Double>(_WSPassengerAirportTax_QNAME, Double.class, WSPassenger.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "certificateType", scope = WSPassenger.class)
    public JAXBElement<String> createWSPassengerCertificateType(String value) {
        return new JAXBElement<String>(_WSPassengerCertificateType_QNAME, String.class, WSPassenger.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "param3", scope = WSPassenger.class)
    public JAXBElement<String> createWSPassengerParam3(String value) {
        return new JAXBElement<String>(_WSPolicyOrderParam3_QNAME, String.class, WSPassenger.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "passengerType", scope = WSPassenger.class)
    public JAXBElement<String> createWSPassengerPassengerType(String value) {
        return new JAXBElement<String>(_WSPassengerPassengerType_QNAME, String.class, WSPassenger.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "param1", scope = WSPassenger.class)
    public JAXBElement<String> createWSPassengerParam1(String value) {
        return new JAXBElement<String>(_PaymentInfoParam1_QNAME, String.class, WSPassenger.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "param2", scope = WSPassenger.class)
    public JAXBElement<String> createWSPassengerParam2(String value) {
        return new JAXBElement<String>(_WSPolicyOrderParam2_QNAME, String.class, WSPassenger.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "returnMessage", scope = CreatePolicyOrderBySpecialPnrReply.class)
    public JAXBElement<String> createCreatePolicyOrderBySpecialPnrReplyReturnMessage(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderBySpecialPnrReplyReturnMessage_QNAME, String.class, CreatePolicyOrderBySpecialPnrReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "returnCode", scope = CreatePolicyOrderBySpecialPnrReply.class)
    public JAXBElement<String> createCreatePolicyOrderBySpecialPnrReplyReturnCode(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderBySpecialPnrReplyReturnCode_QNAME, String.class, CreatePolicyOrderBySpecialPnrReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "param1", scope = CreatePolicyOrderBySpecialPnrReply.class)
    public JAXBElement<String> createCreatePolicyOrderBySpecialPnrReplyParam1(String value) {
        return new JAXBElement<String>(_PaymentInfoParam1_QNAME, String.class, CreatePolicyOrderBySpecialPnrReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "param2", scope = CreatePolicyOrderBySpecialPnrReply.class)
    public JAXBElement<String> createCreatePolicyOrderBySpecialPnrReplyParam2(String value) {
        return new JAXBElement<String>(_WSPolicyOrderParam2_QNAME, String.class, CreatePolicyOrderBySpecialPnrReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "seatClass", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoSeatClass(String value) {
        return new JAXBElement<String>(_SegmentInfoSeatClass_QNAME, String.class, FlightInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "arrCode", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoArrCode(String value) {
        return new JAXBElement<String>(_FlightInfoArrCode_QNAME, String.class, FlightInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "flightNo", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoFlightNo(String value) {
        return new JAXBElement<String>(_SegmentInfoFlightNo_QNAME, String.class, FlightInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "depDate", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoDepDate(String value) {
        return new JAXBElement<String>(_FlightInfoDepDate_QNAME, String.class, FlightInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "arrDate", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoArrDate(String value) {
        return new JAXBElement<String>(_FlightInfoArrDate_QNAME, String.class, FlightInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "planeModel", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoPlaneModel(String value) {
        return new JAXBElement<String>(_FlightInfoPlaneModel_QNAME, String.class, FlightInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "depCode", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoDepCode(String value) {
        return new JAXBElement<String>(_FlightInfoDepCode_QNAME, String.class, FlightInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", name = "param1", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoParam1(String value) {
        return new JAXBElement<String>(_PaymentInfoParam1_QNAME, String.class, FlightInfo.class, value);
    }

}
