
package com.liantuo.webservice.version2_0.createpolicyorderbyspecialpnr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CreatePolicyOrderBySpecialPnrRequest" type="{http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com}CreatePolicyOrderBySpecialPnrRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "createPolicyOrderBySpecialPnrRequest"
})
@XmlRootElement(name = "createPolicyOrderBySpecialPnr")
public class CreatePolicyOrderBySpecialPnr {

    @XmlElement(name = "CreatePolicyOrderBySpecialPnrRequest", required = true, nillable = true)
    protected CreatePolicyOrderBySpecialPnrRequest createPolicyOrderBySpecialPnrRequest;

    /**
     * Gets the value of the createPolicyOrderBySpecialPnrRequest property.
     * 
     * @return
     *     possible object is
     *     {@link CreatePolicyOrderBySpecialPnrRequest }
     *     
     */
    public CreatePolicyOrderBySpecialPnrRequest getCreatePolicyOrderBySpecialPnrRequest() {
        return createPolicyOrderBySpecialPnrRequest;
    }

    /**
     * Sets the value of the createPolicyOrderBySpecialPnrRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreatePolicyOrderBySpecialPnrRequest }
     *     
     */
    public void setCreatePolicyOrderBySpecialPnrRequest(CreatePolicyOrderBySpecialPnrRequest value) {
        this.createPolicyOrderBySpecialPnrRequest = value;
    }

}
