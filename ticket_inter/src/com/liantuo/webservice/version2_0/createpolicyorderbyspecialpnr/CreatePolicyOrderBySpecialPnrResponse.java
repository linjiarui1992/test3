
package com.liantuo.webservice.version2_0.createpolicyorderbyspecialpnr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CreatePolicyOrderBySpecialPnrReply" type="{http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com}CreatePolicyOrderBySpecialPnrReply"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "createPolicyOrderBySpecialPnrReply"
})
@XmlRootElement(name = "createPolicyOrderBySpecialPnrResponse")
public class CreatePolicyOrderBySpecialPnrResponse {

    @XmlElement(name = "CreatePolicyOrderBySpecialPnrReply", required = true, nillable = true)
    protected CreatePolicyOrderBySpecialPnrReply createPolicyOrderBySpecialPnrReply;

    /**
     * Gets the value of the createPolicyOrderBySpecialPnrReply property.
     * 
     * @return
     *     possible object is
     *     {@link CreatePolicyOrderBySpecialPnrReply }
     *     
     */
    public CreatePolicyOrderBySpecialPnrReply getCreatePolicyOrderBySpecialPnrReply() {
        return createPolicyOrderBySpecialPnrReply;
    }

    /**
     * Sets the value of the createPolicyOrderBySpecialPnrReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreatePolicyOrderBySpecialPnrReply }
     *     
     */
    public void setCreatePolicyOrderBySpecialPnrReply(CreatePolicyOrderBySpecialPnrReply value) {
        this.createPolicyOrderBySpecialPnrReply = value;
    }

}
