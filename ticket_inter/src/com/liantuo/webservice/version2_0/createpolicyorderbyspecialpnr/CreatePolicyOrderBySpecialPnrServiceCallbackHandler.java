
/**
 * CreatePolicyOrderBySpecialPnrService_2_0CallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.2  Built on : Sep 06, 2010 (09:42:01 CEST)
 */

    package com.liantuo.webservice.version2_0.createpolicyorderbyspecialpnr;

    /**
     *  CreatePolicyOrderBySpecialPnrService_2_0CallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class CreatePolicyOrderBySpecialPnrServiceCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public CreatePolicyOrderBySpecialPnrServiceCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public CreatePolicyOrderBySpecialPnrServiceCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for createPolicyOrderBySpecialPnr method
            * override this method for handling normal response from createPolicyOrderBySpecialPnr operation
            */
           public void receiveResultcreatePolicyOrderBySpecialPnr(
                    com.liantuo.webservice.version2_0.createpolicyorderbyspecialpnr.CreatePolicyOrderBySpecialPnrServiceStub.CreatePolicyOrderBySpecialPnrResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from createPolicyOrderBySpecialPnr operation
           */
            public void receiveErrorcreatePolicyOrderBySpecialPnr(java.lang.Exception e) {
            }
                


    }
    