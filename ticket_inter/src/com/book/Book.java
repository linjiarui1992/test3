package com.book;

import java.rmi.RemoteException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.apache.axiom.om.OMElement;
import org.apache.axis2.AxisFault;

import client.CreatePolicyOrderByPNRService_2_0Stub;
import client.CreatePolicyOrderByPNRTxtService_2_0Stub;
import client.CreatePolicyOrderBySpecialPnrService_2_0Stub;
import client.GetPolicyDataByIdService_2_0Stub;
import client.GetPolicyDataByPNRTxtService_2_0Stub;
import client.CreatePolicyOrderBySpecialPnrService_2_0Stub.ArrayOfPassengerInfo;
import client.CreatePolicyOrderBySpecialPnrService_2_0Stub.ArrayOfSegmentInfo;
import client.GetPolicyDataByIdService_2_0Stub.ModifiedPolicyData;

import com.ccservice.b2b2c.base.orderinfo.Orderinfo;
import com.ccservice.b2b2c.base.segmentinfo.*;
import com.ccservice.b2b2c.base.passenger.Passenger;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.inter.server.Server;
import com.ccservice.test.HttpClient;
import com.liantuo.webservice.client.CancelPolicyOrderService_2_0Stub;
import com.liantuo.webservice.client.GetPolicyOrderStatusByOrderNoService_2_0Stub;
import com.liantuo.webservice.client.PayPolicyOrderService_2_0Stub;
import com.liantuo.webservice.version2_0.SecurityCredential;
import com.liantuo.webservice.version2_0.createpolicyorderbypnr.CreatePolicyOrderByPNR;
import com.liantuo.webservice.version2_0.createpolicyorderbypnr.CreatePolicyOrderByPNRReply;
import com.liantuo.webservice.version2_0.createpolicyorderbypnr.CreatePolicyOrderByPNRRequest;
import com.liantuo.webservice.version2_0.createpolicyorderbypnr.CreatePolicyOrderByPNRResponse;
import com.liantuo.webservice.version2_0.createpolicyorderbypnr.CreatePolicyOrderByPNRService20;
import com.liantuo.webservice.version2_0.createpolicyorderbypnr.CreatePolicyOrderByPNRService20PortType;
import com.liantuo.webservice.version2_0.createpolicyorderbypnr.WSPolicyOrder;
import com.liantuo.webservice.version2_0.createpolicyorderbypnrtxt.CreatePolicyOrderByPNRTxt;
import com.liantuo.webservice.version2_0.createpolicyorderbypnrtxt.CreatePolicyOrderByPNRTxtRequest;
import com.liantuo.webservice.version2_0.createpolicyorderbypnrtxt.CreatePolicyOrderByPNRTxtService20;





public class Book {
	private static final String name = "yptchen";
	private static final String pwd = "yptchen";

	public String Create51BookOrder(Orderinfo orderinfo,Segmentinfo segment,List<Passenger> listPassenger){
		
		
		
		
		
		
		try {
			CreatePolicyOrderBySpecialPnrService_2_0Stub  stub = new CreatePolicyOrderBySpecialPnrService_2_0Stub();
			
			CreatePolicyOrderBySpecialPnrService_2_0Stub.CreatePolicyOrderBySpecialPnr data = new CreatePolicyOrderBySpecialPnrService_2_0Stub.CreatePolicyOrderBySpecialPnr();
			CreatePolicyOrderBySpecialPnrService_2_0Stub.CreatePolicyOrderBySpecialPnrRequest re = new CreatePolicyOrderBySpecialPnrService_2_0Stub.CreatePolicyOrderBySpecialPnrRequest();
			CreatePolicyOrderBySpecialPnrService_2_0Stub.SecurityCredential sec = new CreatePolicyOrderBySpecialPnrService_2_0Stub.SecurityCredential();
			
			re.setPnrNo(orderinfo.getPnr());//小PNR
			re.setBPnrNo(orderinfo.getBigpnr());//大PNR
			re.setTicketPrice(listPassenger.get(0).getPrice().toString());//单人票面价
			re.setOilPrice(listPassenger.get(0).getFuelprice().toString());//单人燃油价
			re.setBuildPrice(listPassenger.get(0).getAirportfee().toString());//单人机场建设费
			re.setPaymentReturnUrl("http://www.alhk999.com");//支付完成后返回的url
			re.setNotifiedUrl("http://www.alhk999.com");//出票通知地址..
			re.setB2CCreatorCn("cangnan");//订单创建人(客户端)
			re.setRouteType("ONEWAY");//星程类型  单程：ONEWAY   往返：ROUND 联程：JOIN  往返联程：JOIN_AND_ROUND  缺口程：ARNK
			re.setIsGroup(false);//是否是团队   	True    false
			//re.setParam1("");
			//re.setParam2("");
			//re.setParam3("");
			CreatePolicyOrderBySpecialPnrService_2_0Stub.PassengerInfo passengerInfo = new CreatePolicyOrderBySpecialPnrService_2_0Stub.PassengerInfo();
			ArrayOfPassengerInfo arrayOfPassengerInfo = new CreatePolicyOrderBySpecialPnrService_2_0Stub.ArrayOfPassengerInfo();
			
			
			for(int a=0;a<listPassenger.size();a++){
			passengerInfo.setName(listPassenger.get(a).getName());//姓名
			if(listPassenger.get(a).getPtype()==1){//1,成人  2 儿童   3婴儿
				passengerInfo.setType(0);//乘客类型  =0成人;=1儿童；=2无人陪伴儿童
				
			}else if(listPassenger.get(a).getPtype()==2){//1,成人  2 儿童   3婴儿
				passengerInfo.setType(1);//乘客类型  =0成人;=1儿童；=2无人陪伴儿童
				
			}else{
				
				passengerInfo.setType(1);//乘客类型  =0成人;=1儿童；=2无人陪伴儿童
				
			}
			if(listPassenger.get(a).getIdtype()==1){// 1,身份证  3,护照  4港澳通行证  5台湾通行证   6台胞证  7回乡证
				passengerInfo.setIdentityType("1");//证件类型   =1身份证；=2护照；=3军官证；=4士兵证；	=5 台胞证；=6 其他.
			}
			if(listPassenger.get(a).getIdtype()==3){// 1,身份证  3,护照  4港澳通行证  5台湾通行证   6台胞证  7回乡证
				passengerInfo.setIdentityType("2");//证件类型   =1身份证；=2护照；=3军官证；=4士兵证；	=5 台胞证；=6 其他.
			}
			if(listPassenger.get(a).getIdtype()==5){// 1,身份证  3,护照  4港澳通行证  5台湾通行证   6台胞证  7回乡证
				passengerInfo.setIdentityType("5");//证件类型   =1身份证；=2护照；=3军官证；=4士兵证；	=5 台胞证；=6 其他.
			}else{
				passengerInfo.setIdentityType("6");//证件类型   =1身份证；=2护照；=3军官证；=4士兵证；	=5 台胞证；=6 其他.
			}
			//passengerInfo.setIdentityType("1");//证件类型   =1身份证；=2护照；=3军官证；=4士兵证；	=5 台胞证；=6 其他.
			passengerInfo.setIdentityNo(listPassenger.get(a).getIdnumber());//证件号码
			passengerInfo.setParam1("");	
			arrayOfPassengerInfo.addPassengerInfo(passengerInfo);
   	}
			re.setPassengers(arrayOfPassengerInfo);//乘机人
			
			CreatePolicyOrderBySpecialPnrService_2_0Stub.SegmentInfo segmentInfo = new CreatePolicyOrderBySpecialPnrService_2_0Stub.SegmentInfo();
			
			ArrayOfSegmentInfo arrayOfSegmentInfo = new CreatePolicyOrderBySpecialPnrService_2_0Stub.ArrayOfSegmentInfo();
			
			segmentInfo.setFlightNo(segment.getFlightnumber());//航班号
			segmentInfo.setDepartureAirport(segment.getStartairport());//出发地三字码
			segmentInfo.setArrivalAirport(segment.getEndairport());//抵达地三字码
			segmentInfo.setDepartureDate(formatTimestampYYYYMMDD(segment.getDeparttime()));//出发日期  格式：2009-10-10
			segmentInfo.setDepartureTime(formatTimestampToHm(segment.getDeparttime()));//出发时间 格式：0730 代表7点30
			segmentInfo.setArrivalDate(formatTimestampYYYYMMDD(segment.getArrivaltime()));//抵达日期  格式：2009-10-10
			segmentInfo.setArrivalTime(formatTimestampToHm(segment.getArrivaltime()));//抵达时间 格式：0730 代表7点30
			segmentInfo.setPlaneModle(segment.getFlightmodel());//机型
			segmentInfo.setSeatClass(segment.getCabincode());//仓位
			segmentInfo.setParam1("");
			arrayOfSegmentInfo.addSegmentInfo(segmentInfo);
			re.setSegments(arrayOfSegmentInfo);//行程
			if(orderinfo.getPolicyid()!=null){
			Zrate zrate = Server.getInstance().getAirService().findZrate(orderinfo.getPolicyid());
			if(zrate.getOutid()!=null){
				re.setPolicyId(Integer.parseInt(zrate.getOutid()));
			}
			}
			sec.setAgencyCode("BJS_S111016");
			String sign=HttpClient.MD5(sec.getAgencyCode()+re.getPnrNo()+re.getBPnrNo()+re.getPolicyId()+"n5Y)F6r^");
			sec.setSign(sign);
			re.setCredential(sec);
			data.setCreatePolicyOrderBySpecialPnrRequest(re);
			
			
			CreatePolicyOrderBySpecialPnrService_2_0Stub.CreatePolicyOrderBySpecialPnrResponse res = stub.createPolicyOrderBySpecialPnr(data);
			System.out.println("----"+res.getCreatePolicyOrderBySpecialPnrReply().getReturnMessage());
			if(res.getCreatePolicyOrderBySpecialPnrReply().getOrder().getSequenceNo()!=null){//成功  
			
				System.out.println("外部订单号:"+res.getCreatePolicyOrderBySpecialPnrReply().getOrder().getSequenceNo());
				
				return res.getCreatePolicyOrderBySpecialPnrReply().getOrder().getSequenceNo();
			
			}
		} catch (AxisFault e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "-1";
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "-1";
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "-1";
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		return "-1";
	}
	
	public Zrate FindOneZrateByIdTo51Book(String zid){
		Zrate zrate = new Zrate();
		System.out.println("FindOneZrateByIdTo51Book------------zid-----="+zid);
		try {
			GetPolicyDataByIdService_2_0Stub stub = new GetPolicyDataByIdService_2_0Stub();
			
			GetPolicyDataByIdService_2_0Stub.GetPolicyById data = new GetPolicyDataByIdService_2_0Stub.GetPolicyById();
			GetPolicyDataByIdService_2_0Stub.GetPolicyByIdRequest re = new GetPolicyDataByIdService_2_0Stub.GetPolicyByIdRequest();
			
			GetPolicyDataByIdService_2_0Stub.SecurityCredential sec = new GetPolicyDataByIdService_2_0Stub.SecurityCredential();
			
			re.setPolicyId(zid);//政策ID,外部id
			
			sec.setAgencyCode("BJS_S111016");
			String sign=HttpClient.MD5(sec.getAgencyCode()+re.getPolicyId()+"n5Y)F6r^");
			sec.setSign(sign);

			
			re.setCredential(sec);
			data.setIn0(re);
			
			GetPolicyDataByIdService_2_0Stub.GetPolicyByIdResponse res = stub.getPolicyById(data);
			System.out.println("----"+res.getOut().getReturnMessage());
			if(res.getOut().getReturnCode()!=null&&res.getOut().getReturnCode().equals("S")){//成功  S   失败  F
			

				ModifiedPolicyData aa= 	res.getOut().getModifiedPolicyData();
				if(aa!=null){
				//System.out.println("返点="+aa.getCommision()+",航空公司代码="+aa.getAirlineCode()+",适合航班="+aa.getFlightNoIncluding()+",不适合航班="+aa.getFlightNoExclude()+",航线="+aa.getFlightCourse());
				
				
				
				String where =" where 1=1 and "+Zrate.COL_outid+" ='"+aa.getId()+"' and "+Zrate.COL_agentid+" =5";
				List<Zrate>listz= Server.getInstance().getAirService().findAllZrate(where, " ORDER BY ID DESC", -1, 0);
				if(listz.size()>0){
					zrate=listz.get(0);
					
				}
				
				zrate.setCreatetime(new Timestamp(System.currentTimeMillis()));
				zrate.setCreateuser("job");
				zrate.setModifytime(new Timestamp(System.currentTimeMillis()));
				zrate.setModifyuser("job");
				zrate.setAgentid(5l);
				zrate.setTickettype(1);
				zrate.setIsenable(1);
				
				zrate.setOutid(aa.getId()+"");
				
				zrate.setAircompanycode(aa.getAirlineCode());
				if(aa.getFlightCourse()!=null&&aa.getFlightCourse().length()>0){
				zrate.setDepartureport(aa.getFlightCourse().split("-")[0]);//出发机场
				zrate.setArrivalport(aa.getFlightCourse().split("-")[1]);//到达
				}
				if(aa.getCommision()>0){
				zrate.setRatevalue(aa.getCommision());
				}
				zrate.setCabincode(aa.getSeatClass());
				zrate.setFlightnumber(aa.getFlightNoIncluding());// 适用的航班
				zrate.setWeeknum(aa.getFlightNoExclude());//不适用的航班
				zrate.setSchedule(aa.getFlightCycle());//航班周期  1234567
				
				//aa.getNeedSwitchPNR(); True——供应商需要更换成自己的PNR出票 False——不许更换
				
				//System.out.println("shijian=="+new Timestamp(aa.getStartDate().getTime().getTime()));
				//zrate.setBegindate(new Timestamp(aa.getStartDate().getTime().getTime()));
				if(aa.getStartDate()!=null&&aa.getStartDate().getTime()!=null){
				zrate.setBegindate(new Timestamp(aa.getStartDate().getTime().getTime()));
				}
				if(aa.getPrintTicketStartDate()!=null&&aa.getPrintTicketStartDate().getTime()!=null){
				zrate.setIssuedstartdate(new Timestamp(aa.getPrintTicketStartDate().getTime().getTime()));
				}
				if(aa.getExpiredDate()!=null&&aa.getExpiredDate().getTime()!=null){
				zrate.setIssuedendate(new Timestamp(aa.getExpiredDate().getTime().getTime()));  //出票结束时间
				}
				if(aa.getPrintTicketExpiredDate()!=null&&aa.getPrintTicketExpiredDate().getTime()!=null){
				zrate.setEnddate(new Timestamp(aa.getPrintTicketExpiredDate().getTime().getTime()));  //政策有效期结束时间
				
				}
			
				if(aa.getPolicyType()!=null&&aa.getPolicyType().equals("B2B")){
					zrate.setTickettype(2);
					
				}else{
					
					zrate.setTickettype(1);
				}
				
				
				if(aa.getRouteType()!=null&&aa.getRouteType().equals("OW")){//单程  
					
					zrate.setVoyagetype("1");//1单程,2:往返，3:单程或往返
					
				}else{
					zrate.setVoyagetype("2");//1单程,2:往返，3:单程或往返
					
				}
				
				if(aa.getWorkTime()!=null&&aa.getWorkTime().length()>0&&aa.getWorkTime().indexOf("-")!=-1){
					String worktime=aa.getWorkTime().split("-")[0];
					zrate.setWorktime(worktime);
					zrate.setAfterworktime(aa.getWorkTime().split("-")[1]);
					
				}
				if(aa.getBusinessUnitType()!=null){
					
					if(aa.getBusinessUnitType().equals("0")){//=0 普通政策 =1 特殊政策
						
						zrate.setGeneral(1l);
						zrate.setZtype("1");
					}else{
						
						zrate.setGeneral(2l);//1,普通   2高反
						zrate.setZtype("2");
					}
					
				}else{
					
					zrate.setGeneral(1l);
					zrate.setZtype("1");
				}
				zrate.setUsertype("1");
				zrate.setSpeed(aa.getAgencyEfficiency());
				zrate.setRemark(aa.getComment());
				
				
				
				if(listz.size()>0){
					Server.getInstance().getAirService().updateZrateIgnoreNull(zrate);
					System.out.println("update=="+zrate.getRatevalue());
				}else{
					
					try {
						Server.getInstance().getAirService().createZrate(zrate);
						System.out.println("add=="+zrate.getRatevalue());
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						System.out.println("add出错");
					}
					
				}
				
				
				
				
				
				
				
				
				}
			}
		} catch (AxisFault e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
		
		return zrate;
	}
	
	
	public Zrate SerachZrateByPNR(String strpnr,String PataTxt){
		//System.out.println("pnr=="+strpnr);
		List<Zrate> listRate = new ArrayList<Zrate>();
		Zrate zrate = new Zrate();
		try {
			GetPolicyDataByPNRTxtService_2_0Stub stub = new GetPolicyDataByPNRTxtService_2_0Stub();
			GetPolicyDataByPNRTxtService_2_0Stub.GetPolicyDataByPNRTxt data = new GetPolicyDataByPNRTxtService_2_0Stub.GetPolicyDataByPNRTxt();
			GetPolicyDataByPNRTxtService_2_0Stub.GetPolicyDataByPNRTxtRequest re = new GetPolicyDataByPNRTxtService_2_0Stub.GetPolicyDataByPNRTxtRequest();
			GetPolicyDataByPNRTxtService_2_0Stub.SecurityCredential sec = new GetPolicyDataByPNRTxtService_2_0Stub.SecurityCredential();
			
			
			re.setPnrTxt(strpnr);//pnr
			re.setPataTxt(PataTxt);//pate
			re.setParam1("");
			re.setParam2("");
				sec.setAgencyCode("BJS_S111016");
				String sign=HttpClient.MD5(sec.getAgencyCode()+re.getParam1()+re.getParam2()+"n5Y)F6r^");
			
				sec.setSign(sign);

				re.setCredential(sec);
				data.setIn0(re);
				GetPolicyDataByPNRTxtService_2_0Stub.GetPolicyDataByPNRTxtResponse res =stub.getPolicyDataByPNRTxt(data);
				
				System.out.println("--"+res.getOut().getReturnCode()+"--");
				System.out.println("--"+res.getOut().getReturnMessage()+"--");
				if(res.getOut().getPolicyList()!=null&&res.getOut().getPolicyList().getPolicyData()!=null){
					
					int len=res.getOut().getPolicyList().getPolicyData().length;
					
					for(int a=0;a<len;a++){
						
						client.GetPolicyDataByPNRTxtService_2_0Stub.PolicyData aa =res.getOut().getPolicyList().getPolicyData()[a];
						
						
						
						if(aa!=null){
							//System.out.println("返点="+aa.getCommision()+",航空公司代码="+aa.getAirlineCode()+",适合航班="+aa.getFlightNoIncluding()+",不适合航班="+aa.getFlightNoExclude()+",航线="+aa.getFlightCourse());
							
							String where =" where 1=1 and "+Zrate.COL_outid+" ='"+aa.getId()+"' and "+Zrate.COL_agentid+" =5";
							List<Zrate>listz= Server.getInstance().getAirService().findAllZrate(where, " ORDER BY ID DESC", -1, 0);
							if(listz.size()>0){
								zrate=listz.get(0);
								
							}
							
							zrate.setCreatetime(new Timestamp(System.currentTimeMillis()));
							zrate.setCreateuser("job");
							zrate.setModifytime(new Timestamp(System.currentTimeMillis()));
							zrate.setModifyuser("job");
							zrate.setAgentid(5l);
							zrate.setTickettype(1);
							zrate.setIsenable(1);
							
							zrate.setOutid(aa.getId()+"");
							
							zrate.setAircompanycode(aa.getAirlineCode());
							if(aa.getFlightCourse()!=null&&aa.getFlightCourse().length()>0){
							zrate.setDepartureport(aa.getFlightCourse().split("-")[0]);//出发机场
							zrate.setArrivalport(aa.getFlightCourse().split("-")[1]);//到达
							}
							if(aa.getCommision()>0){
							zrate.setRatevalue(aa.getCommision());
							}
							zrate.setCabincode(aa.getSeatClass());
							zrate.setFlightnumber(aa.getFlightNoIncluding());// 适用的航班
							zrate.setWeeknum(aa.getFlightNoExclude());//不适用的航班
							zrate.setSchedule(aa.getFlightCycle());//航班周期  1234567
							
							//aa.getNeedSwitchPNR(); True——供应商需要更换成自己的PNR出票 False——不许更换
							
							//System.out.println("shijian=="+new Timestamp(aa.getStartDate().getTime().getTime()));
							//zrate.setBegindate(new Timestamp(aa.getStartDate().getTime().getTime()));
							if(aa.getStartDate()!=null&&aa.getStartDate().getTime()!=null){
							zrate.setBegindate(new Timestamp(aa.getStartDate().getTime().getTime()));
							}
							if(aa.getPrintTicketStartDate()!=null&&aa.getPrintTicketStartDate().getTime()!=null){
							zrate.setIssuedstartdate(new Timestamp(aa.getPrintTicketStartDate().getTime().getTime()));
							}
							if(aa.getExpiredDate()!=null&&aa.getExpiredDate().getTime()!=null){
							zrate.setIssuedendate(new Timestamp(aa.getExpiredDate().getTime().getTime()));  //出票结束时间
							}
							if(aa.getPrintTicketExpiredDate()!=null&&aa.getPrintTicketExpiredDate().getTime()!=null){
							zrate.setEnddate(new Timestamp(aa.getPrintTicketExpiredDate().getTime().getTime()));  //政策有效期结束时间
							
							}
							//new Timestamp( new SimpleDateFormat("yyyy-MM-dd").p);
							
							
							//zrate.setIssuedendate(new Timestamp( new SimpleDateFormat("yyyy-MM-dd").parse(aa.getExpiredDate()+"").getTime()));  //出票结束时间
								
							//zrate.setEnddate(new Timestamp( new SimpleDateFormat("yyyy-MM-dd").parse(aa.getPrintTicketExpiredDate()+"").getTime()));  //政策有效期结束时间
							
							
							
							if(aa.getPolicyType()!=null&&aa.getPolicyType().equals("B2B")){
								zrate.setTickettype(2);
								
							}else{
								
								zrate.setTickettype(1);
							}
							
							
							if(aa.getRouteType()!=null&&aa.getRouteType().equals("OW")){//单程  
								
								zrate.setVoyagetype("1");//1单程,2:往返，3:单程或往返
								
							}else{
								zrate.setVoyagetype("2");//1单程,2:往返，3:单程或往返
								
							}
							
							if(aa.getWorkTime()!=null&&aa.getWorkTime().length()>0&&aa.getWorkTime().indexOf("-")!=-1){
								String worktime=aa.getWorkTime().split("-")[0];
								zrate.setWorktime(worktime);
								zrate.setAfterworktime(aa.getWorkTime().split("-")[1]);
								
							}
							if(aa.getBusinessUnitType()!=null){
								
								if(aa.getBusinessUnitType().equals("0")){//=0 普通政策 =1 特殊政策
									
									zrate.setGeneral(1l);
									zrate.setZtype("1");
								}else{
									
									zrate.setGeneral(2l);//1,普通   2高反
									zrate.setZtype("2");
								}
								
							}else{
								
								zrate.setGeneral(1l);
								zrate.setZtype("1");
							}
							zrate.setUsertype("1");
							zrate.setSpeed(aa.getAgencyEfficiency());
							zrate.setRemark(aa.getComment());
							
							
							
							if(listz.size()>0){
								Server.getInstance().getAirService().updateZrateIgnoreNull(zrate);
								System.out.println("update=="+zrate.getRatevalue());
							}else{
								
								Server.getInstance().getAirService().createZrate(zrate);
								System.out.println("add=="+zrate.getRatevalue());
							}
							System.out.println("listzrate=="+zrate.getRatevalue());
							listRate.add(zrate);
							break;
						}
						
						
						/*if(listRate.size()>0){
							
							try
							{
								float fzratetemp=0f; //临时政策值
								int intindex=0; //最高政策序号
								for(int z=0;z<listRate.size();z++){
									if(listRate.get(z).getRatevalue()>fzratetemp)
									{
										fzratetemp=listRate.get(z).getRatevalue();
										intindex=z;
									}
								}
								zrate=listRate.get(intindex);
							}
							catch(Exception ex)
							{
								zrate=listRate.get(0);
							}
							
						}*/
						
						
					}
					
					
					
				}
				
				
				
		
		
		
		} catch (AxisFault e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		System.out.println("zrate=="+zrate.getRatevalue());
		
		return zrate;
	}
	
	public String CreateOrderByPNRTxtReply(String pnr,String pnrTxt,String pataTxt,String policyId,String notifiedUrl,String paymentReturnUrl,String b2cCreatorCn) throws RemoteException, NoSuchAlgorithmException{
	
		
		
	
		try {
			CreatePolicyOrderByPNRTxtService_2_0Stub stub = new CreatePolicyOrderByPNRTxtService_2_0Stub();
			
			CreatePolicyOrderByPNRTxtService_2_0Stub.CreatePolicyOrderByPNRTxt data= new CreatePolicyOrderByPNRTxtService_2_0Stub.CreatePolicyOrderByPNRTxt();
			
			//CreatePolicyOrderByPNRTxtService_2_0Stub.CreatePolicyOrderByPNRTxtReply data2= new CreatePolicyOrderByPNRTxtService_2_0Stub.CreatePolicyOrderByPNRTxtReply();;
			//CreatePolicyOrderByPNRTxtService_2_0Stub.CreatePolicyOrderByPNRTxtReply orderByPNRTxtReply = new CreatePolicyOrderByPNRTxtService_2_0Stub.CreatePolicyOrderByPNRTxtReply();
			
			
			CreatePolicyOrderByPNRTxtService_2_0Stub.CreatePolicyOrderByPNRTxtRequest re = new CreatePolicyOrderByPNRTxtService_2_0Stub.CreatePolicyOrderByPNRTxtRequest();
			
			CreatePolicyOrderByPNRTxtService_2_0Stub.SecurityCredential sec = new CreatePolicyOrderByPNRTxtService_2_0Stub.SecurityCredential();
			re.setPnrTxt(pnrTxt);
			re.setPataTxt(pataTxt);
			re.setPolicyId(Integer.parseInt(policyId));
			re.setNotifiedUrl(notifiedUrl);
			re.setPaymentReturnUrl(paymentReturnUrl);
			re.setB2CCreatorCn(b2cCreatorCn);
			//re.setParam1("");
			//re.setParam2("");
			sec.setAgencyCode("BJS_S111016");
			String sign=HttpClient.MD5(sec.getAgencyCode()+re.getPnrTxt()+re.getPolicyId()+re.getNotifiedUrl()+re.getPaymentReturnUrl()+"n5Y)F6r^");
			sec.setSign(sign);
			
			
			re.setCredential(sec);
			data.setIn0(re);
			CreatePolicyOrderByPNRTxtService_2_0Stub.CreatePolicyOrderByPNRTxtResponse res =stub.createPolicyOrderByPNRTxt(data);
			
			System.out.println("--"+res.getOut().getReturnCode()+"--"+res.getOut().getReturnMessage()+"--");
			
		    if(res.getOut().getOrder()!=null){
		    	if(res.getOut().getOrder().getSequenceNo()!=null){
		    		String ordernumandPNR =	res.getOut().getOrder().getSequenceNo()+"_"+res.getOut().getOrder().getPnrNo();
			    	System.out.println("外部订单号_PNR="+ordernumandPNR);
			    	return ordernumandPNR;
		    	}else{
		    		return "-1";
		    	}
		    }else{
		    	return "-1";
		    }
		} catch (AxisFault e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "-1";
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "-1";
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "-1";
		}
		
		
		
		
		
		
	}
	public String CreateOrderByPNR(String pnr,String pnrTxt,String pataTxt,String policyId,String notifiedUrl,String paymentReturnUrl,String b2cCreatorCn) throws RemoteException, NoSuchAlgorithmException{
		System.out.println("政策id=="+policyId+",pnr=="+pnr);
		CreatePolicyOrderByPNRService_2_0Stub stub = new CreatePolicyOrderByPNRService_2_0Stub();
		CreatePolicyOrderByPNRService_2_0Stub.CreatePolicyOrderByPNR data = new CreatePolicyOrderByPNRService_2_0Stub.CreatePolicyOrderByPNR();
		CreatePolicyOrderByPNRService_2_0Stub.CreatePolicyOrderByPNRRequest req = new CreatePolicyOrderByPNRService_2_0Stub.CreatePolicyOrderByPNRRequest();
		CreatePolicyOrderByPNRService_2_0Stub.SecurityCredential sec = new CreatePolicyOrderByPNRService_2_0Stub.SecurityCredential();
		String agencycode="BJS_S111016";
		String securitycode="n5Y)F6r^";	
		
		req.setB2CCreatorCn("b2b");
		req.setIsPay("0");
		
		req.setNotifiedUrl("http://www.sina.com.cn");
		req.setPaymentReturnUrl("http://www.sina.com.cn");
		req.setPnrNo(pnr);
		req.setPolicyId(Integer.parseInt(policyId));
		//req.setParam1("");
		//req.setParam2("");
		//req.setParam3("");
		sec.setAgencyCode(agencycode);
		sec.setSign(HttpClient.MD5(agencycode+req.getPnrNo()+req.getPolicyId()+req.getNotifiedUrl()+req.getPaymentReturnUrl()+securitycode));
		req.setCredential(sec);
		data.setIn0(req);
		CreatePolicyOrderByPNRService_2_0Stub.CreatePolicyOrderByPNRResponse res = stub.createPolicyOrderByPNR(data);
		
		System.out.println(res.getOut().getReturnMessage());
		System.out.println(res.getOut().getOrder().getSequenceNo());
	/*	CreatePolicyOrderByPNRTxtService20 service20 = new CreatePolicyOrderByPNRTxtService20();
		com.liantuo.webservice.version2_0.createpolicyorderbypnrtxt.CreatePolicyOrderByPNRTxt data = new CreatePolicyOrderByPNRTxt();
		com.liantuo.webservice.version2_0.createpolicyorderbypnrtxt.CreatePolicyOrderByPNRTxtRequest re = new CreatePolicyOrderByPNRTxtRequest();
		com.liantuo.webservice.version2_0.SecurityCredential sec = new SecurityCredential();
		
		re.setPnrTxt(pnrTxt);
		re.setPataTxt(pataTxt);
		re.setPolicyId(Integer.parseInt(policyId));
		re.setNotifiedUrl(notifiedUrl);
		re.setPaymentReturnUrl(paymentReturnUrl);
		//re.setB2CCreatorCn();
	
		sec.setAgencyCode("BJS_S111016");
		String sign=HttpClient.MD5(sec.getAgencyCode()+re.getPnrTxt()+re.getPolicyId()+re.getNotifiedUrl()+re.getPaymentReturnUrl()+"n5Y)F6r^");
		sec.setSign(sign);
		
		
		re.setCredential(sec);
		data.setIn0(re);
		//com.liantuo.webservice.version2_0.createpolicyorderbypnrtxt.CreatePolicyOrderByPNRTxtResponse res =service20.
		*/
		
		return "-1";
		
		
	}
	public String Seach51BookOrderStaus(String ordernum,String stype){
		
		
		try {
			GetPolicyOrderStatusByOrderNoService_2_0Stub stub = new GetPolicyOrderStatusByOrderNoService_2_0Stub();
			
			GetPolicyOrderStatusByOrderNoService_2_0Stub.GetPolicyOrderStatusByOrderNo data = new GetPolicyOrderStatusByOrderNoService_2_0Stub.GetPolicyOrderStatusByOrderNo();
			GetPolicyOrderStatusByOrderNoService_2_0Stub.GetPolicyOrderStatusByOrderNoRequest re = new GetPolicyOrderStatusByOrderNoService_2_0Stub.GetPolicyOrderStatusByOrderNoRequest();
			
			GetPolicyOrderStatusByOrderNoService_2_0Stub.SecurityCredential sec = new GetPolicyOrderStatusByOrderNoService_2_0Stub.SecurityCredential();
			
			re.setOrderNo(ordernum);//订单号,51book返回的订单号
			re.setStatusType(Integer.parseInt(stype));//=1订单采购状态；=2 订单供应状态
			String sign=HttpClient.MD5(sec.getAgencyCode()+re.getOrderNo()+re.getStatusType()+"n5Y)F6r^");
			sec.setSign(sign);

			
			re.setCredential(sec);
			data.setIn0(re);
			
			GetPolicyOrderStatusByOrderNoService_2_0Stub.GetPolicyOrderStatusByOrderNoResponse res = stub.getPolicyOrderStatusByOrderNo(data);
			
			System.out.println("订单状态编号为=="+res.getOut().getStatusCode());
			System.out.println("订单状态描述=="+res.getOut().getStatusDesc());
			
			return res.getOut().getStatusDesc();
		} catch (AxisFault e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "未知";
			
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "未知";
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "未知";
		}
		
		
		
		
		
	}
	public String Cancel51BookOrderByID(String orderid){
		
		try {
			CancelPolicyOrderService_2_0Stub stub = new CancelPolicyOrderService_2_0Stub();
			
			
			
			CancelPolicyOrderService_2_0Stub.CancelPolicyOrder data = new CancelPolicyOrderService_2_0Stub.CancelPolicyOrder();
			
			CancelPolicyOrderService_2_0Stub.CancelPolicyOrderRequest re = new CancelPolicyOrderService_2_0Stub.CancelPolicyOrderRequest();
			
			CancelPolicyOrderService_2_0Stub.SecurityCredential sec = new CancelPolicyOrderService_2_0Stub.SecurityCredential();
			
			re.setSequenceNo("");//51book订单号
			
			
			sec.setAgencyCode("BJS_S111016");
			String sign=HttpClient.MD5(sec.getAgencyCode()+re.getSequenceNo()+"n5Y)F6r^");
			sec.setSign(sign);

			
			re.setCredential(sec);
			data.setIn0(re);
			
			CancelPolicyOrderService_2_0Stub.CancelPolicyOrderResponse res = stub.cancelPolicyOrder(data);
			if(res.getOut().getReturnCode().equals("S")){
				System.out.println("取消订单状态=="+res.getOut().getStatus());
				return res.getOut().getStatus();
			}else{
				

				System.out.println("取消失败");
				return "-1";
			}
			
			
			
		} catch (AxisFault e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("取消出现异常");
			return "-1";
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("取消出现异常");
			return "-1";
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("取消出现异常");
			return "-1";
		}
		
		
	}
	public String Get51BookPayUrlByOrderID(String orderid){
		
		try{
			PayPolicyOrderService_2_0Stub stub = new PayPolicyOrderService_2_0Stub();
			
			PayPolicyOrderService_2_0Stub.PayPolicyOrder data = new PayPolicyOrderService_2_0Stub.PayPolicyOrder();
			//GetPolicyDataByIdService_2_0Stub.GetPolicyByIdRequest re = new GetPolicyDataByIdService_2_0Stub.GetPolicyByIdRequest();
			PayPolicyOrderService_2_0Stub.PayPolicyOrderRequest re = new PayPolicyOrderService_2_0Stub.PayPolicyOrderRequest();
			
			PayPolicyOrderService_2_0Stub.SecurityCredential sec = new PayPolicyOrderService_2_0Stub.SecurityCredential();
			
			re.setOrderNo(orderid);//订单号,51book返回的订单号
			re.setUserName("BJS_S111016");//用户名
			re.setUserPassword("1111");//密码
			
			String sign=HttpClient.MD5(sec.getAgencyCode()+re.getOrderNo()+re.getUserName()+re.getUserPassword()+"n5Y)F6r^");
			sec.setSign(sign);

			
			re.setCredential(sec);
			data.setIn0(re);
			
			PayPolicyOrderService_2_0Stub.PayPolicyOrderResponse res = stub.payPolicyOrder(data);
			
			if(res.getOut().getPaymentInfo()!=null){
				
				System.out.println("订单号=="+res.getOut().getOrderNo());
				System.out.println("订单状态=="+res.getOut().getOrderStatus());
				System.out.println("支付URL=="+res.getOut().getPaymentInfo().getPaymentUrl());
				System.out.println("支付价格=="+res.getOut().getPaymentInfo().getSettlePrice());
				System.out.println("支付宝交易号=="+res.getOut().getPaymentInfo().getTradeNo());
				System.out.println("支付宝张号=="+res.getOut().getPaymentInfo().getPayerAccount());	
				
				
				
				return res.getOut().getPaymentInfo().getPaymentUrl();
			}else{
				System.out.println("51book支付URL出问题了");
				return "-1";
			}
			
			
			
		}catch (Exception e) {
			// TODO: handle exception
			System.out.println("51book支付URL出异常了");
		}
		
		
		return "";
		
	}
	public String formatTimestampYYYYMMDD(Timestamp date) {
		try {
			return (new SimpleDateFormat("yyyy-MM-dd").format(date));

		} catch (Exception e) {
			return "";
		}

	}
	public String formatTimestampToHm(Timestamp date) {
		try {
			
			return (new SimpleDateFormat("HH:mm").format(date)).replace(":", "");

		} catch (Exception e) {
			return "";
		}

	}
	 
	public static void main(String[] args) {
		try {
			//String  StrPata	=	Server.getInstance().getTicketSearchService().commandFunction("RTHGGHPK$PN$PAT:A", "");
			
			String  StrPata	=	Server.getInstance().getTicketSearchService().getFullRTPnrResult("JSF8X3");
			
			
			System.out.println("StrPata=="+StrPata);
			
			
			//StrPata="111111111PAT:A1111111111";
			
			//HYF9QC
			
			//new Book().SerachZrateByPNR("HYF9QC",StrPata);
			
			if(StrPata.indexOf("PAT")!=-1){
		    String	pnrTxt=StrPata.split("PAT")[0].replace(">", "");
			
		    String	pataTxt=StrPata.split("PAT")[1].replace(":A", "");
		
		    
		Zrate zrate=new Book().SerachZrateByPNR(pnrTxt,pataTxt);
		
		System.out.println("zrateqqqq=="+zrate.getRatevalue());
		//zrate.setOutid("16079757");
		//String ordernum=	new  Book().CreateOrderByPNRTxtReply("JSF8X3",pnrTxt, pataTxt, zrate.getOutid(), "http://", "http://", "陈星");
		String ordernum=	new  Book().CreateOrderByPNR("JSF8X3",pnrTxt, pataTxt, zrate.getOutid(), "http://", "http://", "陈星");
		//String ordernum =  new Book().Create51OrderByPnrToJdkWebService("JSF8X3", zrate.getOutid());
		System.out.println("ordernum=="+ordernum);	
			}
			
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public String Create51OrderByPnrToJdkWebService(String pnr,String zrateid){
		
		try {
			
			System.out.println("pnr=="+pnr+",zrateid=="+zrateid);
			CreatePolicyOrderByPNRService20 service  = new CreatePolicyOrderByPNRService20();
			CreatePolicyOrderByPNRService20PortType port =  service.getCreatePolicyOrderByPNRService20HttpPort();
			//CreatePolicyOrderByPNR data = new CreatePolicyOrderByPNR();
			CreatePolicyOrderByPNRRequest req = new CreatePolicyOrderByPNRRequest();
			//CreatePolicyOrderByPNRResponse res = new CreatePolicyOrderByPNRResponse();
			SecurityCredential sec = new SecurityCredential();
			req.setPnrNo(pnr);
			req.setPolicyId(Integer.parseInt(zrateid));
			req.setNotifiedUrl("http://www.sina.com.cn");
			req.setPaymentReturnUrl("http://www.sina.com.cn");
			JAXBElement<String> username = new JAXBElement<String>(new QName("http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", "b2creatorcn"), String.class, "Tom");
			req.setB2CCreatorCn(username);
			JAXBElement<String> ispay = new JAXBElement<String>(new QName("http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", "ispay"), String.class, "0");
			req.setIsPay(ispay);
			sec.setAgencyCode("BJS_S111016");
			sec.setSign(HttpClient.MD5(sec.getAgencyCode()+req.getPnrNo()+req.getPolicyId()+req.getNotifiedUrl()+req.getPaymentReturnUrl()+"n5Y)F6r^"));
			req.setCredential(sec);
			CreatePolicyOrderByPNRReply reply  = port.createPolicyOrderByPNR(req);
			JAXBElement<WSPolicyOrder> order =reply.getOrder();
			System.out.println("aaaa=="+order.getValue().getSequenceNo());
			
			
			
			
			
			
			
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
		
		
		
		return "";
	}
	 private static CreatePolicyOrderByPNRRequest buildRequest() {
		 CreatePolicyOrderByPNRRequest request = new CreatePolicyOrderByPNRRequest();
		 
		 
		 return request;

	 }
}
