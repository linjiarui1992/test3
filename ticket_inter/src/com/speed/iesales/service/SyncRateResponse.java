
package com.speed.iesales.service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for syncRateResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="syncRateResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://service.iesales.speed.com/}statusResponse">
 *       &lt;sequence>
 *         &lt;element name="lastStrategyId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pageCount" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="rateList" type="{http://service.iesales.speed.com/}rate" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="updateTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "syncRateResponse", propOrder = {
    "lastStrategyId",
    "pageCount",
    "rateList",
    "updateTime"
})
public class SyncRateResponse
    extends StatusResponse
{

    protected String lastStrategyId;
    protected Integer pageCount;
    @XmlElement(nillable = true)
    protected List<Rate> rateList;
    protected String updateTime;

    /**
     * Gets the value of the lastStrategyId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastStrategyId() {
        return lastStrategyId;
    }

    /**
     * Sets the value of the lastStrategyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastStrategyId(String value) {
        this.lastStrategyId = value;
    }

    /**
     * Gets the value of the pageCount property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPageCount() {
        return pageCount;
    }

    /**
     * Sets the value of the pageCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPageCount(Integer value) {
        this.pageCount = value;
    }

    /**
     * Gets the value of the rateList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rateList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRateList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Rate }
     * 
     * 
     */
    public List<Rate> getRateList() {
        if (rateList == null) {
            rateList = new ArrayList<Rate>();
        }
        return this.rateList;
    }

    /**
     * Gets the value of the updateTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUpdateTime() {
        return updateTime;
    }

    /**
     * Sets the value of the updateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUpdateTime(String value) {
        this.updateTime = value;
    }

}
