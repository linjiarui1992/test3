
package com.speed.iesales.service;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for rate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="aircomp2c" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="backrate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="chngretmemo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="daysFit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="edate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="flightclass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flightnoFit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flightnoNotFit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fromport3c" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="opdate" type="{http://service.iesales.speed.com/}timestamp" minOccurs="0"/>
 *         &lt;element name="outagentid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="psgtype" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ratetype" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="routetype" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="sdate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="state" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="strategyId" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="strategyType" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="thespeed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="toport3c" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="usertype" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="voidtime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="worktime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rate", propOrder = {
    "aircomp2C",
    "backrate",
    "chngretmemo",
    "daysFit",
    "edate",
    "flightclass",
    "flightnoFit",
    "flightnoNotFit",
    "fromport3C",
    "opdate",
    "outagentid",
    "psgtype",
    "ratetype",
    "routetype",
    "sdate",
    "state",
    "strategyId",
    "strategyType",
    "thespeed",
    "toport3C",
    "usertype",
    "voidtime",
    "worktime"
})
public class Rate {

    @XmlElement(name = "aircomp2c")
    protected String aircomp2C;
    protected BigDecimal backrate;
    protected String chngretmemo;
    protected String daysFit;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar edate;
    protected String flightclass;
    protected String flightnoFit;
    protected String flightnoNotFit;
    @XmlElement(name = "fromport3c")
    protected String fromport3C;
    protected Timestamp opdate;
    protected String outagentid;
    protected Integer psgtype;
    protected String ratetype;
    protected BigDecimal routetype;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar sdate;
    protected BigDecimal state;
    protected BigDecimal strategyId;
    protected BigDecimal strategyType;
    protected String thespeed;
    @XmlElement(name = "toport3c")
    protected String toport3C;
    protected Integer usertype;
    protected String voidtime;
    protected String worktime;

    /**
     * Gets the value of the aircomp2C property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAircomp2C() {
        return aircomp2C;
    }

    /**
     * Sets the value of the aircomp2C property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAircomp2C(String value) {
        this.aircomp2C = value;
    }

    /**
     * Gets the value of the backrate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBackrate() {
        return backrate;
    }

    /**
     * Sets the value of the backrate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBackrate(BigDecimal value) {
        this.backrate = value;
    }

    /**
     * Gets the value of the chngretmemo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChngretmemo() {
        return chngretmemo;
    }

    /**
     * Sets the value of the chngretmemo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChngretmemo(String value) {
        this.chngretmemo = value;
    }

    /**
     * Gets the value of the daysFit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDaysFit() {
        return daysFit;
    }

    /**
     * Sets the value of the daysFit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDaysFit(String value) {
        this.daysFit = value;
    }

    /**
     * Gets the value of the edate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEdate() {
        return edate;
    }

    /**
     * Sets the value of the edate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEdate(XMLGregorianCalendar value) {
        this.edate = value;
    }

    /**
     * Gets the value of the flightclass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightclass() {
        return flightclass;
    }

    /**
     * Sets the value of the flightclass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightclass(String value) {
        this.flightclass = value;
    }

    /**
     * Gets the value of the flightnoFit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightnoFit() {
        return flightnoFit;
    }

    /**
     * Sets the value of the flightnoFit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightnoFit(String value) {
        this.flightnoFit = value;
    }

    /**
     * Gets the value of the flightnoNotFit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightnoNotFit() {
        return flightnoNotFit;
    }

    /**
     * Sets the value of the flightnoNotFit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightnoNotFit(String value) {
        this.flightnoNotFit = value;
    }

    /**
     * Gets the value of the fromport3C property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFromport3C() {
        return fromport3C;
    }

    /**
     * Sets the value of the fromport3C property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromport3C(String value) {
        this.fromport3C = value;
    }

    /**
     * Gets the value of the opdate property.
     * 
     * @return
     *     possible object is
     *     {@link Timestamp }
     *     
     */
    public Timestamp getOpdate() {
        return opdate;
    }

    /**
     * Sets the value of the opdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Timestamp }
     *     
     */
    public void setOpdate(Timestamp value) {
        this.opdate = value;
    }

    /**
     * Gets the value of the outagentid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutagentid() {
        return outagentid;
    }

    /**
     * Sets the value of the outagentid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutagentid(String value) {
        this.outagentid = value;
    }

    /**
     * Gets the value of the psgtype property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPsgtype() {
        return psgtype;
    }

    /**
     * Sets the value of the psgtype property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPsgtype(Integer value) {
        this.psgtype = value;
    }

    /**
     * Gets the value of the ratetype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRatetype() {
        return ratetype;
    }

    /**
     * Sets the value of the ratetype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRatetype(String value) {
        this.ratetype = value;
    }

    /**
     * Gets the value of the routetype property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRoutetype() {
        return routetype;
    }

    /**
     * Sets the value of the routetype property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRoutetype(BigDecimal value) {
        this.routetype = value;
    }

    /**
     * Gets the value of the sdate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSdate() {
        return sdate;
    }

    /**
     * Sets the value of the sdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSdate(XMLGregorianCalendar value) {
        this.sdate = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setState(BigDecimal value) {
        this.state = value;
    }

    /**
     * Gets the value of the strategyId property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getStrategyId() {
        return strategyId;
    }

    /**
     * Sets the value of the strategyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setStrategyId(BigDecimal value) {
        this.strategyId = value;
    }

    /**
     * Gets the value of the strategyType property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getStrategyType() {
        return strategyType;
    }

    /**
     * Sets the value of the strategyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setStrategyType(BigDecimal value) {
        this.strategyType = value;
    }

    /**
     * Gets the value of the thespeed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getThespeed() {
        return thespeed;
    }

    /**
     * Sets the value of the thespeed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setThespeed(String value) {
        this.thespeed = value;
    }

    /**
     * Gets the value of the toport3C property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToport3C() {
        return toport3C;
    }

    /**
     * Sets the value of the toport3C property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToport3C(String value) {
        this.toport3C = value;
    }

    /**
     * Gets the value of the usertype property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getUsertype() {
        return usertype;
    }

    /**
     * Sets the value of the usertype property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setUsertype(Integer value) {
        this.usertype = value;
    }

    /**
     * Gets the value of the voidtime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoidtime() {
        return voidtime;
    }

    /**
     * Sets the value of the voidtime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoidtime(String value) {
        this.voidtime = value;
    }

    /**
     * Gets the value of the worktime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorktime() {
        return worktime;
    }

    /**
     * Sets the value of the worktime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorktime(String value) {
        this.worktime = value;
    }

}
