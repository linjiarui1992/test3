package com.piaomeng;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.UtilMethod;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;

/**
 * 
 * 票盟推送政策
 * @author 票盟的推送政策接口 陈星
 * 
 */
public class PiaoMengZrate extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        doPost(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        StringBuffer buffer = new StringBuffer(1024);
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
            String line = "";
            while ((line = br.readLine()) != null) {
                buffer.append(line);
            }
            if (buffer.toString().length() < 15) {
                return;
            }
            String T = new SimpleDateFormat("HH:mm:ss SSS").format(new Date());
            System.out.println(T);
            WriteLog.write("PIAOMENGZRATE", buffer.toString());
            Document document = DocumentHelper.parseText(buffer.toString());
            Element root = document.getRootElement();
            List<Zrate> zrates = new ArrayList<Zrate>();
            if (root.elements("pol").size() > 0) {
                List<Element> listpol = root.elements("pol");
                System.out.println(T + ":listpol==" + listpol.size());
                int tempint = 0;
                int updateint = 0;
                String deleteWhere = "DELETE FROM T_ZRATE WHERE 1=2 ";
                int delete = 0;
                if (listpol.size() > 0) {
                    for (int a = 0, size = listpol.size(); a < size; a++) {
                        try {
                            Element tempZrateElement = listpol.get(a);
                            String staus = tempZrateElement.attributeValue("event");// 状态11添加,13删除
                            String outid = tempZrateElement.attributeValue("id");// 外部ID
                            String providerid = tempZrateElement.attributeValue("providerid");// 供应代码
                            // 13：删除政策
                            if (staus.equals("13") && outid.length() > 0) {
                                deleteWhere += " OR C_OUTID ='" + outid + "'";
                            }
                            else if (staus.equals("18")) {
                                String worktimefrom = tempZrateElement.attributeValue("worktimefrom");// 供应商工作开始时间,格式：Hmm
                                String worktimeto = tempZrateElement.attributeValue("worktimeto");// 供应商工作结束时间，格式：HHmm
                                String where = " UPDATE T_ZRATE SET " + Zrate.COL_worktime + "=" + worktimefrom + ","
                                        + Zrate.COL_afterworktime + "=" + worktimeto + " WHERE " + Zrate.COL_agentcode
                                        + "='" + providerid + "'";
                                WriteLog.write("PIAOMENGZRATE", where);
                                try {
                                    Server.getInstance().getAirService().excuteZrateBySql(where);
                                }
                                catch (Exception e) {
                                    System.out.println(e.getMessage());
                                }
                            }
                            else if (staus.equals("11")) {
                                Zrate zrate = new Zrate();
                                zrate.setModifyuser("PM");
                                zrate.setCreateuser("PM");
                                zrate.setAgentid(2l);
                                zrate.setIsenable(1);
                                zrate.setOutid(outid);
                                zrate.setAgentcode(providerid);
                                // 航空公司
                                String aircom = tempZrateElement.attributeValue("aircom");
                                zrate.setAircompanycode(aircom);
                                // 出发城市.可以一个或多个，多个用/分隔
                                String formcity = tempZrateElement.attributeValue("from");
                                zrate.setDepartureport(formcity);
                                // 到达城市.可以一个或多个，多个用/分隔
                                String tocity = tempZrateElement.attributeValue("to");
                                zrate.setArrivalport(tocity);
                                // 航班号，可以一个或多个，多个用/分隔，当航班标识flighScope为0时空，为1时适用航班号，为2时不适用航班号
                                String flightno = tempZrateElement.attributeValue("flightno");// 航班号
                                // 航班标识0全部 1适用 2不适用
                                String flighscope = tempZrateElement.attributeValue("flighscope");
                                if (flighscope.equals("1")) {// 适用
                                    zrate.setFlightnumber(flightno);
                                }
                                else {// 不适用
                                    zrate.setWeeknum(flightno);
                                }
                                // 1：单程 2：往返 3：联程
                                String routetype = tempZrateElement.attributeValue("routetype");
                                zrate.setVoyagetype(routetype);
                                // 政策类型：B2B-ET 或BSP-ET
                                String policytype = tempZrateElement.attributeValue("policytype");
                                if (policytype.indexOf("B2B") > 0) {
                                    zrate.setTickettype(2);
                                }
                                else {
                                    zrate.setTickettype(1);
                                }
                                //代理费
                                String rate = tempZrateElement.attributeValue("rate");
                                zrate.setRatevalue(Float.parseFloat(rate));
                                // 政策开始时间
                                String fromTime = tempZrateElement.attributeValue("fromtime");
                                zrate.setBegindate(dateToTimestamp(fromTime));
                                // 政策结束时间
                                String toTime = tempZrateElement.attributeValue("totime");
                                zrate.setEnddate(dateToTimestamp(toTime));
                                // 仓位 一个或多个，多个用;分隔
                                String applyclass = tempZrateElement.attributeValue("applyclass");
                                zrate.setCabincode(applyclass);

                                //打票开始时间
                                String printtickfromtime = tempZrateElement.attributeValue("printtickfromtime");
                                zrate.setIssuedstartdate(dateToTimestamp(printtickfromtime));
                                //打票结束时间
                                String printticktotime = tempZrateElement.attributeValue("printticktotime");
                                zrate.setIssuedendate(dateToTimestamp(printticktotime));
                                //供应商工作开始时间,格式：HHmm
                                String worktimefrom = tempZrateElement.attributeValue("worktimefrom");
                                zrate.setWorktime(updateTime(worktimefrom));
                                //供应商工作结束时间,格式：HHmm
                                String worktimeto = tempZrateElement.attributeValue("worktimeto");
                                zrate.setAfterworktime(updateTime(worktimeto));
                                //是否要更换pnr出票0:不需要更换1：需要更换
                                String changerecord = tempZrateElement.attributeValue("changerecord");
                                zrate.setIschange(Long.parseLong(changerecord));
                                //政策使用条件1：普通 3：团队 4：儿童
                                String contype = tempZrateElement.attributeValue("contype");
                                zrate.setUsertype(contype);
                                //政策备注
                                String note = tempZrateElement.attributeValue("note");
                                zrate.setRemark(note);

                                String isspecial = tempZrateElement.attributeValue("isspecial");
                                if (isspecial.equals("0")) {
                                    zrate.setGeneral(1l);
                                    zrate.setZtype("1");
                                }
                                else {
                                    zrate.setZtype("2");
                                    zrate.setGeneral(2l);
                                }

                                //周末供应商工作开始时间,格式:HHmm
                                String weekendworktimef = tempZrateElement.attributeValue("weekendworktimef");
                                zrate.setWeekendworktime(updateTime(weekendworktimef));
                                //周末供应商工作结束时间,格式:HHmm
                                String weekendworktimet = tempZrateElement.attributeValue("weekendworktimef");
                                zrate.setWeekendaftertime(updateTime(weekendworktimet));
                                //政策的星期限制	航班周期 1234567
                                String policyweek = tempZrateElement.attributeValue("policyweek");
                                zrate.setSchedule(policyweek);
                                // 废票截止时间
                                String refundworktimeto = tempZrateElement.attributeValue("refundworktimeto");
                                zrate.setOnetofivewastetime(refundworktimeto);
                                // 周末废票截止时间
                                String refundweekendworktimeto = tempZrateElement
                                        .attributeValue("refundweekendworktimeto");
                                zrate.setWeekendwastetime(refundweekendworktimeto);
                                zrate.setCreatetime(new Timestamp(System.currentTimeMillis()));
                                zrate.setModifytime(new Timestamp(System.currentTimeMillis()));
                                //								Server.getInstance().getAirService().createZrate(zrate);
                                deleteWhere += " OR C_OUTID ='" + outid + "'";
                                zrates.add(zrate);
                                tempint++;
                            }
                            tempZrateElement = null;
                        }
                        catch (Exception e) {
                            UtilMethod.writeEx("PiaoMengZrate", e);
                            e.printStackTrace();
                        }
                    }
                }
                WriteLog.write("PIAOMENGZRATE", deleteWhere);
                delete = Server.getInstance().getAirService().excuteZrateBySql(deleteWhere);
                tempint = Server.getInstance().getAirService().createZrateList(zrates);
                System.out.println(T + ":insert:" + tempint);
                System.out.println(T + ":delete:" + delete);
            }
            buffer = null;
            out.write("0");
            out.close();
        }
        catch (Exception ex) {
            ex.printStackTrace();
            UtilMethod.writeEx("PiaoMengZrate", ex);
            out.print("1");
            out.close();
        }
    }

    /**
     * 用于更新政策如果更新成功则返回更新的数量
     * @param sql
     * @return
     */
    public int updateZrat1e(Zrate zrate) {
        String sql = "UPDATE T_ZRATE SET C_DEPARTUREPORT='" + zrate.getDepartureport() + "',C_ARRIVALPORT='"
                + zrate.getArrivalport() + "',C_CABINCODE='" + zrate.getCabincode() + "',C_RATEVALUE="
                + zrate.getRatevalue() + ",C_MODIFYTIME='" + zrate.getModifytime().toLocaleString() + "',C_REMARK='"
                + zrate.getRemark() + "',C_AIRCOMPANYCODE='" + zrate.getAircompanycode() + "',C_TICKETTYPE='"
                + zrate.getTickettype() + "',C_GENERAL='" + zrate.getGeneral() + "',C_BEGINDATE='"
                + zrate.getBegindate().toLocaleString() + "',C_ENDDATE='" + zrate.getEnddate().toLocaleString()
                + "',C_AFTERWORKTIME='" + zrate.getAfterworktime() + "',C_WORKTIME='" + zrate.getWorktime()
                + "',C_SPEED='" + zrate.getSpeed() == null ? "NULL" : zrate.getSpeed() + "',C_ONETOFIVEWASTETIME='"
                + zrate.getOnetofivewastetime() + "',C_VOYAGETYPE='" + zrate.getVoyagetype() + "',C_USERTYPE='"
                + zrate.getUsertype() + "' WHERE C_OUTID='" + zrate.getOutid() + "'";
        WriteLog.write("PIAOMENGZRATE", sql);
        return Server.getInstance().getAirService().excuteZrateBySql(sql);
    }

    private static SimpleDateFormat dateFormat = new SimpleDateFormat();

    public static Timestamp dateToTimestamp(String date) {
        try {

            if (date.length() == 10) {
                dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            }
            else {
                dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            }
            return (new Timestamp(dateFormat.parse(date).getTime()));

        }
        catch (Exception e) {
            return null;
        }
    }

    public String updateTime(String time) {
        String a = "08";
        String b = "00";
        if (time.length() == 3) {
            a = time.substring(0, 1);
            b = time.substring(1, time.length());
        }
        if (time.length() == 4) {
            a = time.substring(0, 2);
            b = time.substring(2, time.length());
        }
        return a + b;
    }

    public static void main(String[] args) {
        test();
    }

    public static void test() {
    }
}
