/**
 * GetPolicyByPnrTxtServiceImpl_1_0ServiceStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.2  Built on : Sep 06, 2010 (09:42:01 CEST)
 */
package com.webservice.fiveonebookv3;

import org.dom4j.io.XMLWriter;

import com.ccservice.inter.job.WriteLog;

/*
*  GetPolicyByPnrTxtServiceImpl_1_0ServiceStub java implementation
*/

public class GetPolicyByPnrTxtServiceImpl_1_0ServiceStub extends org.apache.axis2.client.Stub {
    protected org.apache.axis2.description.AxisOperation[] _operations;

    //hashmaps to keep the fault mapping
    private java.util.HashMap faultExceptionNameMap = new java.util.HashMap();

    private java.util.HashMap faultExceptionClassNameMap = new java.util.HashMap();

    private java.util.HashMap faultMessageMap = new java.util.HashMap();

    private static int counter = 0;

    private static synchronized java.lang.String getUniqueSuffix() {
        // reset the counter if it is greater than 99999
        if (counter > 99999) {
            counter = 0;
        }
        counter = counter + 1;
        return java.lang.Long.toString(System.currentTimeMillis()) + "_" + counter;
    }

    private void populateAxisService() throws org.apache.axis2.AxisFault {

        //creating the Service with a unique name
        _service = new org.apache.axis2.description.AxisService("GetPolicyByPnrTxtServiceImpl_1_0Service"
                + getUniqueSuffix());
        addAnonymousOperations();

        //creating the operations
        org.apache.axis2.description.AxisOperation __operation;

        _operations = new org.apache.axis2.description.AxisOperation[1];

        __operation = new org.apache.axis2.description.OutInAxisOperation();

        __operation.setName(new javax.xml.namespace.QName(
                "http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/", "getPolicyByPnrTxt"));
        _service.addOperation(__operation);

        _operations[0] = __operation;

    }

    //populates the faults
    private void populateFaults() {

    }

    /**
      *Constructor that takes in a configContext
      */

    public GetPolicyByPnrTxtServiceImpl_1_0ServiceStub(
            org.apache.axis2.context.ConfigurationContext configurationContext, java.lang.String targetEndpoint)
            throws org.apache.axis2.AxisFault {
        this(configurationContext, targetEndpoint, false);
    }

    /**
      * Constructor that takes in a configContext  and useseperate listner
      */
    public GetPolicyByPnrTxtServiceImpl_1_0ServiceStub(
            org.apache.axis2.context.ConfigurationContext configurationContext, java.lang.String targetEndpoint,
            boolean useSeparateListener) throws org.apache.axis2.AxisFault {
        //To populate AxisService
        populateAxisService();
        populateFaults();

        _serviceClient = new org.apache.axis2.client.ServiceClient(configurationContext, _service);

        _serviceClient.getOptions().setTo(new org.apache.axis2.addressing.EndpointReference(targetEndpoint));
        _serviceClient.getOptions().setUseSeparateListener(useSeparateListener);

    }

    /**
     * Default Constructor
     */
    public GetPolicyByPnrTxtServiceImpl_1_0ServiceStub(
            org.apache.axis2.context.ConfigurationContext configurationContext) throws org.apache.axis2.AxisFault {

        this(configurationContext, "http://ws.tongyedns.com:8000/ltips/services/getPolicyByPnrTxtService1.0");

    }

    /**
     * Default Constructor
     */
    public GetPolicyByPnrTxtServiceImpl_1_0ServiceStub() throws org.apache.axis2.AxisFault {

        this("http://ws.tongyedns.com:8000/ltips/services/getPolicyByPnrTxtService1.0");

    }

    /**
     * Constructor taking the target endpoint
     */
    public GetPolicyByPnrTxtServiceImpl_1_0ServiceStub(java.lang.String targetEndpoint)
            throws org.apache.axis2.AxisFault {
        this(null, targetEndpoint);
    }

    /**
     * Auto generated method signature
     * 
     * @see com.webservice.fiveonebookv3.GetPolicyByPnrTxtServiceImpl_1_0Service#getPolicyByPnrTxt
     * @param getPolicyByPnrTxt
    
     */

    public com.webservice.fiveonebookv3.GetPolicyByPnrTxtServiceImpl_1_0ServiceStub.GetPolicyByPnrTxtResponseE getPolicyByPnrTxt(

    com.webservice.fiveonebookv3.GetPolicyByPnrTxtServiceImpl_1_0ServiceStub.GetPolicyByPnrTxtE getPolicyByPnrTxt)

    throws java.rmi.RemoteException

    {
        org.apache.axis2.context.MessageContext _messageContext = null;
        try {
            org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[0]
                    .getName());
            _operationClient
                    .getOptions()
                    .setAction(
                            "http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/GetPolicyByPnrTxtService_1_0/getPolicyByPnrTxt");
            _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

            addPropertyToOperationClient(_operationClient,
                    org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

            // create a message context
            _messageContext = new org.apache.axis2.context.MessageContext();

            // create SOAP envelope with that payload
            org.apache.axiom.soap.SOAPEnvelope env = null;

            env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getPolicyByPnrTxt,
                    optimizeContent(new javax.xml.namespace.QName(
                            "http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/",
                            "getPolicyByPnrTxt")));

            //adding SOAP soap_headers
            _serviceClient.addHeadersToEnvelope(env);
            // set the message context with that soap envelope
            _messageContext.setEnvelope(env);

            // add the message contxt to the operation client
            _operationClient.addMessageContext(_messageContext);

            //execute the operation client
            _operationClient.execute(true);

            org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient
                    .getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
            org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
            WriteLog.write("51book1.0GetPolicy", _returnEnv.getText());
            WriteLog.write("51book1.0GetPolicy", _returnEnv.getBody().getText());
            try {
                WriteLog.write("51book1.0GetPolicy", _returnEnv.getBody().getFirstElement().getXMLStreamReader()
                        .getElementText());
            }
            catch (Exception e) {
            }
            java.lang.Object object = fromOM(
                    _returnEnv.getBody().getFirstElement(),
                    com.webservice.fiveonebookv3.GetPolicyByPnrTxtServiceImpl_1_0ServiceStub.GetPolicyByPnrTxtResponseE.class,
                    getEnvelopeNamespaces(_returnEnv));

            return (com.webservice.fiveonebookv3.GetPolicyByPnrTxtServiceImpl_1_0ServiceStub.GetPolicyByPnrTxtResponseE) object;

        }
        catch (org.apache.axis2.AxisFault f) {

            org.apache.axiom.om.OMElement faultElt = f.getDetail();
            if (faultElt != null) {
                if (faultExceptionNameMap.containsKey(faultElt.getQName())) {
                    //make the fault by reflection
                    try {
                        java.lang.String exceptionClassName = (java.lang.String) faultExceptionClassNameMap
                                .get(faultElt.getQName());
                        java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                        java.lang.Exception ex = (java.lang.Exception) exceptionClass.newInstance();
                        //message class
                        java.lang.String messageClassName = (java.lang.String) faultMessageMap.get(faultElt.getQName());
                        java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt, messageClass, null);
                        java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                new java.lang.Class[] { messageClass });
                        m.invoke(ex, new java.lang.Object[] { messageObject });

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    }
                    catch (java.lang.ClassCastException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                    catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                    catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                    catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                    catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                    catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                }
                else {
                    throw f;
                }
            }
            else {
                throw f;
            }
        }
        finally {
            _messageContext.getTransportOut().getSender().cleanup(_messageContext);
        }
    }

    /**
     *  A utility method that copies the namepaces from the SOAPEnvelope
     */
    private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env) {
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
            org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
            returnMap.put(ns.getPrefix(), ns.getNamespaceURI());
        }
        return returnMap;
    }

    private javax.xml.namespace.QName[] opNameArray = null;

    private boolean optimizeContent(javax.xml.namespace.QName opName) {

        if (opNameArray == null) {
            return false;
        }
        for (int i = 0; i < opNameArray.length; i++) {
            if (opName.equals(opNameArray[i])) {
                return true;
            }
        }
        return false;
    }

    //http://ws.tongyedns.com:8000/ltips/services/getPolicyByPnrTxtService1.0
    public static class GetPolicyByPnrTxtE implements org.apache.axis2.databinding.ADBBean {

        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/", "getPolicyByPnrTxt",
                "ns1");

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if (namespace.equals("http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/")) {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
        * field for GetPolicyByPnrTxt
        */

        protected GetPolicyByPnrTxt localGetPolicyByPnrTxt;

        /**
        * Auto generated getter method
        * @return GetPolicyByPnrTxt
        */
        public GetPolicyByPnrTxt getGetPolicyByPnrTxt() {
            return localGetPolicyByPnrTxt;
        }

        /**
           * Auto generated setter method
           * @param param GetPolicyByPnrTxt
           */
        public void setGetPolicyByPnrTxt(GetPolicyByPnrTxt param) {

            this.localGetPolicyByPnrTxt = param;

        }

        /**
        * isReaderMTOMAware
        * @return true if the reader supports MTOM
        */
        public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader
                        .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            }
            catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
        public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this, MY_QNAME) {

                public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    GetPolicyByPnrTxtE.this.serialize(MY_QNAME, factory, xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(MY_QNAME, factory, dataSource);

        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter, boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

            //We can safely assume an element has only one type associated with it

            if (localGetPolicyByPnrTxt == null) {
                throw new org.apache.axis2.databinding.ADBException("Property cannot be null!");
            }
            localGetPolicyByPnrTxt.serialize(MY_QNAME, factory, xmlWriter);

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qnames[i]));
                        }
                    }
                    else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
        * Register a namespace prefix
        */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            //We can safely assume an element has only one type associated with it
            return localGetPolicyByPnrTxt.getPullParser(MY_QNAME);

        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {

            /**
            * static method to create the object
            * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
            *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
            * Postcondition: If this object is an element, the reader is positioned at its end element
            *                If this object is a complex type, the reader is positioned at the end element of its outer element
            */
            public static GetPolicyByPnrTxtE parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
                GetPolicyByPnrTxtE object = new GetPolicyByPnrTxtE();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    while (!reader.isEndElement()) {
                        if (reader.isStartElement()) {

                            if (reader.isStartElement()
                                    && new javax.xml.namespace.QName(
                                            "http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/",
                                            "getPolicyByPnrTxt").equals(reader.getName())) {

                                object.setGetPolicyByPnrTxt(GetPolicyByPnrTxt.Factory.parse(reader));

                            } // End of if for expected property start element

                            else {
                                // A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement "
                                        + reader.getLocalName());
                            }

                        }
                        else {
                            reader.next();
                        }
                    } // end of while loop

                }
                catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class

    }

    public static class AbstractLiantuoReply implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
                name = abstractLiantuoReply
                Namespace URI = http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/
                Namespace Prefix = ns1
                */

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if (namespace.equals("http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/")) {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
        * field for ReturnCode
        */

        protected java.lang.String localReturnCode;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localReturnCodeTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getReturnCode() {
            return localReturnCode;
        }

        /**
           * Auto generated setter method
           * @param param ReturnCode
           */
        public void setReturnCode(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localReturnCodeTracker = true;
            }
            else {
                localReturnCodeTracker = false;

            }

            this.localReturnCode = param;

        }

        /**
        * field for ReturnMessage
        */

        protected java.lang.String localReturnMessage;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localReturnMessageTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getReturnMessage() {
            return localReturnMessage;
        }

        /**
           * Auto generated setter method
           * @param param ReturnMessage
           */
        public void setReturnMessage(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localReturnMessageTracker = true;
            }
            else {
                localReturnMessageTracker = false;

            }

            this.localReturnMessage = param;

        }

        /**
        * isReaderMTOMAware
        * @return true if the reader supports MTOM
        */
        public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader
                        .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            }
            catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
        public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    parentQName) {

                public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    AbstractLiantuoReply.this.serialize(parentQName, factory, xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(parentQName, factory, dataSource);

        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter, boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();

            if ((namespace != null) && (namespace.trim().length() > 0)) {
                java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                if (writerPrefix != null) {
                    xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                }
                else {
                    if (prefix == null) {
                        prefix = generatePrefix(namespace);
                    }

                    xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }
            }
            else {
                xmlWriter.writeStartElement(parentQName.getLocalPart());
            }

            if (serializeType) {

                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/");
                if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix
                            + ":abstractLiantuoReply", xmlWriter);
                }
                else {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "abstractLiantuoReply",
                            xmlWriter);
                }

            }
            if (localReturnCodeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "returnCode", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "returnCode");
                    }

                }
                else {
                    xmlWriter.writeStartElement("returnCode");
                }

                if (localReturnCode == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("returnCode cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localReturnCode);

                }

                xmlWriter.writeEndElement();
            }
            if (localReturnMessageTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "returnMessage", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "returnMessage");
                    }

                }
                else {
                    xmlWriter.writeStartElement("returnMessage");
                }

                if (localReturnMessage == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("returnMessage cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localReturnMessage);

                }

                xmlWriter.writeEndElement();
            }
            xmlWriter.writeEndElement();

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qnames[i]));
                        }
                    }
                    else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
        * Register a namespace prefix
        */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (localReturnCodeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "returnCode"));

                if (localReturnCode != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localReturnCode));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("returnCode cannot be null!!");
                }
            }
            if (localReturnMessageTracker) {
                elementList.add(new javax.xml.namespace.QName("", "returnMessage"));

                if (localReturnMessage != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localReturnMessage));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("returnMessage cannot be null!!");
                }
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(),
                    attribList.toArray());

        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {

            /**
            * static method to create the object
            * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
            *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
            * Postcondition: If this object is an element, the reader is positioned at its end element
            *                If this object is a complex type, the reader is positioned at the end element of its outer element
            */
            public static AbstractLiantuoReply parse(javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                AbstractLiantuoReply object = null;

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance", "type");
                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

                            if (!"abstractLiantuoReply".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (AbstractLiantuoReply) ExtensionMapper.getTypeObject(nsUri, type, reader);
                            }

                            throw new org.apache.axis2.databinding.ADBException(
                                    "The an abstract class can not be instantiated !!!");

                        }

                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "returnCode").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setReturnCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "returnMessage").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setReturnMessage(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement "
                                + reader.getLocalName());

                }
                catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class

    }

    public static class GetPolicyByPnrTxtRequestE implements org.apache.axis2.databinding.ADBBean {

        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/",
                "getPolicyByPnrTxtRequest", "ns1");

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if (namespace.equals("http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/")) {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
        * field for GetPolicyByPnrTxtRequest
        */

        protected GetPolicyByPnrTxtRequest localGetPolicyByPnrTxtRequest;

        /**
        * Auto generated getter method
        * @return GetPolicyByPnrTxtRequest
        */
        public GetPolicyByPnrTxtRequest getGetPolicyByPnrTxtRequest() {
            return localGetPolicyByPnrTxtRequest;
        }

        /**
           * Auto generated setter method
           * @param param GetPolicyByPnrTxtRequest
           */
        public void setGetPolicyByPnrTxtRequest(GetPolicyByPnrTxtRequest param) {

            this.localGetPolicyByPnrTxtRequest = param;

        }

        /**
        * isReaderMTOMAware
        * @return true if the reader supports MTOM
        */
        public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader
                        .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            }
            catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
        public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this, MY_QNAME) {

                public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    GetPolicyByPnrTxtRequestE.this.serialize(MY_QNAME, factory, xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(MY_QNAME, factory, dataSource);

        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter, boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

            //We can safely assume an element has only one type associated with it

            if (localGetPolicyByPnrTxtRequest == null) {
                throw new org.apache.axis2.databinding.ADBException("Property cannot be null!");
            }
            localGetPolicyByPnrTxtRequest.serialize(MY_QNAME, factory, xmlWriter);

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qnames[i]));
                        }
                    }
                    else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
        * Register a namespace prefix
        */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            //We can safely assume an element has only one type associated with it
            return localGetPolicyByPnrTxtRequest.getPullParser(MY_QNAME);

        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {

            /**
            * static method to create the object
            * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
            *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
            * Postcondition: If this object is an element, the reader is positioned at its end element
            *                If this object is a complex type, the reader is positioned at the end element of its outer element
            */
            public static GetPolicyByPnrTxtRequestE parse(javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                GetPolicyByPnrTxtRequestE object = new GetPolicyByPnrTxtRequestE();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    while (!reader.isEndElement()) {
                        if (reader.isStartElement()) {

                            if (reader.isStartElement()
                                    && new javax.xml.namespace.QName(
                                            "http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/",
                                            "getPolicyByPnrTxtRequest").equals(reader.getName())) {

                                object.setGetPolicyByPnrTxtRequest(GetPolicyByPnrTxtRequest.Factory.parse(reader));

                            } // End of if for expected property start element

                            else {
                                // A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement "
                                        + reader.getLocalName());
                            }

                        }
                        else {
                            reader.next();
                        }
                    } // end of while loop

                }
                catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class

    }

    public static class GetPolicyByPnrTxtReplyE implements org.apache.axis2.databinding.ADBBean {

        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/",
                "getPolicyByPnrTxtReply", "ns1");

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if (namespace.equals("http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/")) {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
        * field for GetPolicyByPnrTxtReply
        */

        protected GetPolicyByPnrTxtReply localGetPolicyByPnrTxtReply;

        /**
        * Auto generated getter method
        * @return GetPolicyByPnrTxtReply
        */
        public GetPolicyByPnrTxtReply getGetPolicyByPnrTxtReply() {
            return localGetPolicyByPnrTxtReply;
        }

        /**
           * Auto generated setter method
           * @param param GetPolicyByPnrTxtReply
           */
        public void setGetPolicyByPnrTxtReply(GetPolicyByPnrTxtReply param) {

            this.localGetPolicyByPnrTxtReply = param;

        }

        /**
        * isReaderMTOMAware
        * @return true if the reader supports MTOM
        */
        public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader
                        .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            }
            catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
        public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this, MY_QNAME) {

                public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    GetPolicyByPnrTxtReplyE.this.serialize(MY_QNAME, factory, xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(MY_QNAME, factory, dataSource);

        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter, boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

            //We can safely assume an element has only one type associated with it

            if (localGetPolicyByPnrTxtReply == null) {
                throw new org.apache.axis2.databinding.ADBException("Property cannot be null!");
            }
            localGetPolicyByPnrTxtReply.serialize(MY_QNAME, factory, xmlWriter);

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qnames[i]));
                        }
                    }
                    else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
        * Register a namespace prefix
        */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            //We can safely assume an element has only one type associated with it
            return localGetPolicyByPnrTxtReply.getPullParser(MY_QNAME);

        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {

            /**
            * static method to create the object
            * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
            *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
            * Postcondition: If this object is an element, the reader is positioned at its end element
            *                If this object is a complex type, the reader is positioned at the end element of its outer element
            */
            public static GetPolicyByPnrTxtReplyE parse(javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                GetPolicyByPnrTxtReplyE object = new GetPolicyByPnrTxtReplyE();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    while (!reader.isEndElement()) {
                        if (reader.isStartElement()) {

                            if (reader.isStartElement()
                                    && new javax.xml.namespace.QName(
                                            "http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/",
                                            "getPolicyByPnrTxtReply").equals(reader.getName())) {

                                object.setGetPolicyByPnrTxtReply(GetPolicyByPnrTxtReply.Factory.parse(reader));

                            } // End of if for expected property start element

                            else {
                                // A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement "
                                        + reader.getLocalName());
                            }

                        }
                        else {
                            reader.next();
                        }
                    } // end of while loop

                }
                catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class

    }

    public static class GetPolicyByPnrTxtRequest extends AbstractLiantuoRequest implements
            org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
                name = getPolicyByPnrTxtRequest
                Namespace URI = http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/
                Namespace Prefix = ns1
                */

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if (namespace.equals("http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/")) {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
        * field for AllowSwitchPnr
        */

        protected int localAllowSwitchPnr;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localAllowSwitchPnrTracker = false;

        /**
        * Auto generated getter method
        * @return int
        */
        public int getAllowSwitchPnr() {
            return localAllowSwitchPnr;
        }

        /**
           * Auto generated setter method
           * @param param AllowSwitchPnr
           */
        public void setAllowSwitchPnr(int param) {

            // setting primitive attribute tracker to true

            if (param == java.lang.Integer.MIN_VALUE) {
                localAllowSwitchPnrTracker = false;

            }
            else {
                localAllowSwitchPnrTracker = true;
            }

            this.localAllowSwitchPnr = param;

        }

        /**
        * field for NeedSpePricePolicy
        */

        protected int localNeedSpePricePolicy;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localNeedSpePricePolicyTracker = false;

        /**
        * Auto generated getter method
        * @return int
        */
        public int getNeedSpePricePolicy() {
            return localNeedSpePricePolicy;
        }

        /**
           * Auto generated setter method
           * @param param NeedSpePricePolicy
           */
        public void setNeedSpePricePolicy(int param) {

            // setting primitive attribute tracker to true

            if (param == java.lang.Integer.MIN_VALUE) {
                localNeedSpePricePolicyTracker = false;

            }
            else {
                localNeedSpePricePolicyTracker = true;
            }

            this.localNeedSpePricePolicy = param;

        }

        /**
        * field for NeedSpeRulePolicy
        */

        protected int localNeedSpeRulePolicy;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localNeedSpeRulePolicyTracker = false;

        /**
        * Auto generated getter method
        * @return int
        */
        public int getNeedSpeRulePolicy() {
            return localNeedSpeRulePolicy;
        }

        /**
           * Auto generated setter method
           * @param param NeedSpeRulePolicy
           */
        public void setNeedSpeRulePolicy(int param) {

            // setting primitive attribute tracker to true

            if (param == java.lang.Integer.MIN_VALUE) {
                localNeedSpeRulePolicyTracker = false;

            }
            else {
                localNeedSpeRulePolicyTracker = true;
            }

            this.localNeedSpeRulePolicy = param;

        }

        /**
        * field for OnlyOnWorking
        */

        protected int localOnlyOnWorking;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localOnlyOnWorkingTracker = false;

        /**
        * Auto generated getter method
        * @return int
        */
        public int getOnlyOnWorking() {
            return localOnlyOnWorking;
        }

        /**
           * Auto generated setter method
           * @param param OnlyOnWorking
           */
        public void setOnlyOnWorking(int param) {

            // setting primitive attribute tracker to true

            if (param == java.lang.Integer.MIN_VALUE) {
                localOnlyOnWorkingTracker = false;

            }
            else {
                localOnlyOnWorkingTracker = true;
            }

            this.localOnlyOnWorking = param;

        }

        /**
        * field for Param1
        */

        protected java.lang.String localParam1;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam1Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam1() {
            return localParam1;
        }

        /**
           * Auto generated setter method
           * @param param Param1
           */
        public void setParam1(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam1Tracker = true;
            }
            else {
                localParam1Tracker = false;

            }

            this.localParam1 = param;

        }

        /**
        * field for Param2
        */

        protected java.lang.String localParam2;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam2Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam2() {
            return localParam2;
        }

        /**
           * Auto generated setter method
           * @param param Param2
           */
        public void setParam2(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam2Tracker = true;
            }
            else {
                localParam2Tracker = false;

            }

            this.localParam2 = param;

        }

        /**
        * field for Param3
        */

        protected java.lang.String localParam3;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam3Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam3() {
            return localParam3;
        }

        /**
           * Auto generated setter method
           * @param param Param3
           */
        public void setParam3(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam3Tracker = true;
            }
            else {
                localParam3Tracker = false;

            }

            this.localParam3 = param;

        }

        /**
        * field for Param4
        */

        protected java.lang.String localParam4;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam4Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam4() {
            return localParam4;
        }

        /**
           * Auto generated setter method
           * @param param Param4
           */
        public void setParam4(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam4Tracker = true;
            }
            else {
                localParam4Tracker = false;

            }

            this.localParam4 = param;

        }

        /**
        * field for PataTxt
        */

        protected java.lang.String localPataTxt;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localPataTxtTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getPataTxt() {
            return localPataTxt;
        }

        /**
           * Auto generated setter method
           * @param param PataTxt
           */
        public void setPataTxt(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localPataTxtTracker = true;
            }
            else {
                localPataTxtTracker = false;

            }

            this.localPataTxt = param;

        }

        /**
        * field for PnrTxt
        */

        protected java.lang.String localPnrTxt;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localPnrTxtTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getPnrTxt() {
            return localPnrTxt;
        }

        /**
           * Auto generated setter method
           * @param param PnrTxt
           */
        public void setPnrTxt(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localPnrTxtTracker = true;
            }
            else {
                localPnrTxtTracker = false;

            }

            this.localPnrTxt = param;

        }

        /**
        * isReaderMTOMAware
        * @return true if the reader supports MTOM
        */
        public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader
                        .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            }
            catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
        public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    parentQName) {

                public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    GetPolicyByPnrTxtRequest.this.serialize(parentQName, factory, xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(parentQName, factory, dataSource);

        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter, boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();

            if ((namespace != null) && (namespace.trim().length() > 0)) {
                java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                if (writerPrefix != null) {
                    xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                }
                else {
                    if (prefix == null) {
                        prefix = generatePrefix(namespace);
                    }

                    xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }
            }
            else {
                xmlWriter.writeStartElement(parentQName.getLocalPart());
            }

            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/");
            if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix
                        + ":getPolicyByPnrTxtRequest", xmlWriter);
            }
            else {
                writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "getPolicyByPnrTxtRequest",
                        xmlWriter);
            }

            if (localAgencyCodeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "agencyCode", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "agencyCode");
                    }

                }
                else {
                    xmlWriter.writeStartElement("agencyCode");
                }

                if (localAgencyCode == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("agencyCode cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localAgencyCode);

                }

                xmlWriter.writeEndElement();
            }
            if (localSignTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "sign", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "sign");
                    }

                }
                else {
                    xmlWriter.writeStartElement("sign");
                }

                if (localSign == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("sign cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localSign);

                }

                xmlWriter.writeEndElement();
            }
            if (localAllowSwitchPnrTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "allowSwitchPnr", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "allowSwitchPnr");
                    }

                }
                else {
                    xmlWriter.writeStartElement("allowSwitchPnr");
                }

                if (localAllowSwitchPnr == java.lang.Integer.MIN_VALUE) {

                    throw new org.apache.axis2.databinding.ADBException("allowSwitchPnr cannot be null!!");

                }
                else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localAllowSwitchPnr));
                }

                xmlWriter.writeEndElement();
            }
            if (localNeedSpePricePolicyTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "needSpePricePolicy", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "needSpePricePolicy");
                    }

                }
                else {
                    xmlWriter.writeStartElement("needSpePricePolicy");
                }

                if (localNeedSpePricePolicy == java.lang.Integer.MIN_VALUE) {

                    throw new org.apache.axis2.databinding.ADBException("needSpePricePolicy cannot be null!!");

                }
                else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localNeedSpePricePolicy));
                }

                xmlWriter.writeEndElement();
            }
            if (localNeedSpeRulePolicyTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "needSpeRulePolicy", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "needSpeRulePolicy");
                    }

                }
                else {
                    xmlWriter.writeStartElement("needSpeRulePolicy");
                }

                if (localNeedSpeRulePolicy == java.lang.Integer.MIN_VALUE) {

                    throw new org.apache.axis2.databinding.ADBException("needSpeRulePolicy cannot be null!!");

                }
                else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localNeedSpeRulePolicy));
                }

                xmlWriter.writeEndElement();
            }
            if (localOnlyOnWorkingTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "onlyOnWorking", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "onlyOnWorking");
                    }

                }
                else {
                    xmlWriter.writeStartElement("onlyOnWorking");
                }

                if (localOnlyOnWorking == java.lang.Integer.MIN_VALUE) {

                    throw new org.apache.axis2.databinding.ADBException("onlyOnWorking cannot be null!!");

                }
                else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localOnlyOnWorking));
                }

                xmlWriter.writeEndElement();
            }
            if (localParam1Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param1", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param1");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param1");
                }

                if (localParam1 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param1 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam1);

                }

                xmlWriter.writeEndElement();
            }
            if (localParam2Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param2", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param2");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param2");
                }

                if (localParam2 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param2 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam2);

                }

                xmlWriter.writeEndElement();
            }
            if (localParam3Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param3", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param3");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param3");
                }

                if (localParam3 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param3 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam3);

                }

                xmlWriter.writeEndElement();
            }
            if (localParam4Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param4", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param4");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param4");
                }

                if (localParam4 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param4 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam4);

                }

                xmlWriter.writeEndElement();
            }
            if (localPataTxtTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "pataTxt", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "pataTxt");
                    }

                }
                else {
                    xmlWriter.writeStartElement("pataTxt");
                }

                if (localPataTxt == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("pataTxt cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localPataTxt);

                }

                xmlWriter.writeEndElement();
            }
            if (localPnrTxtTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "pnrTxt", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "pnrTxt");
                    }

                }
                else {
                    xmlWriter.writeStartElement("pnrTxt");
                }

                if (localPnrTxt == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("pnrTxt cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localPnrTxt);

                }

                xmlWriter.writeEndElement();
            }
            xmlWriter.writeEndElement();

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qnames[i]));
                        }
                    }
                    else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
        * Register a namespace prefix
        */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance", "type"));
            attribList.add(new javax.xml.namespace.QName(
                    "http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/",
                    "getPolicyByPnrTxtRequest"));
            if (localAgencyCodeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "agencyCode"));

                if (localAgencyCode != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAgencyCode));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("agencyCode cannot be null!!");
                }
            }
            if (localSignTracker) {
                elementList.add(new javax.xml.namespace.QName("", "sign"));

                if (localSign != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSign));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("sign cannot be null!!");
                }
            }
            if (localAllowSwitchPnrTracker) {
                elementList.add(new javax.xml.namespace.QName("", "allowSwitchPnr"));

                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAllowSwitchPnr));
            }
            if (localNeedSpePricePolicyTracker) {
                elementList.add(new javax.xml.namespace.QName("", "needSpePricePolicy"));

                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil
                        .convertToString(localNeedSpePricePolicy));
            }
            if (localNeedSpeRulePolicyTracker) {
                elementList.add(new javax.xml.namespace.QName("", "needSpeRulePolicy"));

                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil
                        .convertToString(localNeedSpeRulePolicy));
            }
            if (localOnlyOnWorkingTracker) {
                elementList.add(new javax.xml.namespace.QName("", "onlyOnWorking"));

                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOnlyOnWorking));
            }
            if (localParam1Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param1"));

                if (localParam1 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam1));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param1 cannot be null!!");
                }
            }
            if (localParam2Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param2"));

                if (localParam2 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam2));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param2 cannot be null!!");
                }
            }
            if (localParam3Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param3"));

                if (localParam3 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam3));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param3 cannot be null!!");
                }
            }
            if (localParam4Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param4"));

                if (localParam4 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam4));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param4 cannot be null!!");
                }
            }
            if (localPataTxtTracker) {
                elementList.add(new javax.xml.namespace.QName("", "pataTxt"));

                if (localPataTxt != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPataTxt));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("pataTxt cannot be null!!");
                }
            }
            if (localPnrTxtTracker) {
                elementList.add(new javax.xml.namespace.QName("", "pnrTxt"));

                if (localPnrTxt != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPnrTxt));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("pnrTxt cannot be null!!");
                }
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(),
                    attribList.toArray());

        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {

            /**
            * static method to create the object
            * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
            *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
            * Postcondition: If this object is an element, the reader is positioned at its end element
            *                If this object is a complex type, the reader is positioned at the end element of its outer element
            */
            public static GetPolicyByPnrTxtRequest parse(javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                GetPolicyByPnrTxtRequest object = new GetPolicyByPnrTxtRequest();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance", "type");
                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

                            if (!"getPolicyByPnrTxtRequest".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (GetPolicyByPnrTxtRequest) ExtensionMapper.getTypeObject(nsUri, type, reader);
                            }

                        }

                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "agencyCode").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setAgencyCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "sign").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setSign(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "allowSwitchPnr").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setAllowSwitchPnr(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                        object.setAllowSwitchPnr(java.lang.Integer.MIN_VALUE);

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "needSpePricePolicy").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setNeedSpePricePolicy(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToInt(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                        object.setNeedSpePricePolicy(java.lang.Integer.MIN_VALUE);

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "needSpeRulePolicy").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setNeedSpeRulePolicy(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToInt(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                        object.setNeedSpeRulePolicy(java.lang.Integer.MIN_VALUE);

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "onlyOnWorking").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setOnlyOnWorking(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                        object.setOnlyOnWorking(java.lang.Integer.MIN_VALUE);

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param1").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param2").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param3").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam3(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param4").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam4(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "pataTxt").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setPataTxt(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "pnrTxt").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setPnrTxt(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement "
                                + reader.getLocalName());

                }
                catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class

    }

    public static class GetPolicyByPnrTxtResponseE implements org.apache.axis2.databinding.ADBBean {

        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/",
                "getPolicyByPnrTxtResponse", "ns1");

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if (namespace.equals("http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/")) {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
        * field for GetPolicyByPnrTxtResponse
        */

        protected GetPolicyByPnrTxtResponse localGetPolicyByPnrTxtResponse;

        /**
        * Auto generated getter method
        * @return GetPolicyByPnrTxtResponse
        */
        public GetPolicyByPnrTxtResponse getGetPolicyByPnrTxtResponse() {
            return localGetPolicyByPnrTxtResponse;
        }

        /**
           * Auto generated setter method
           * @param param GetPolicyByPnrTxtResponse
           */
        public void setGetPolicyByPnrTxtResponse(GetPolicyByPnrTxtResponse param) {

            this.localGetPolicyByPnrTxtResponse = param;

        }

        /**
        * isReaderMTOMAware
        * @return true if the reader supports MTOM
        */
        public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader
                        .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            }
            catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
        public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this, MY_QNAME) {

                public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    GetPolicyByPnrTxtResponseE.this.serialize(MY_QNAME, factory, xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(MY_QNAME, factory, dataSource);

        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter, boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

            //We can safely assume an element has only one type associated with it

            if (localGetPolicyByPnrTxtResponse == null) {
                throw new org.apache.axis2.databinding.ADBException("Property cannot be null!");
            }
            localGetPolicyByPnrTxtResponse.serialize(MY_QNAME, factory, xmlWriter);

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qnames[i]));
                        }
                    }
                    else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
        * Register a namespace prefix
        */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            //We can safely assume an element has only one type associated with it
            return localGetPolicyByPnrTxtResponse.getPullParser(MY_QNAME);

        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {

            /**
            * static method to create the object
            * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
            *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
            * Postcondition: If this object is an element, the reader is positioned at its end element
            *                If this object is a complex type, the reader is positioned at the end element of its outer element
            */
            public static GetPolicyByPnrTxtResponseE parse(javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                GetPolicyByPnrTxtResponseE object = new GetPolicyByPnrTxtResponseE();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    while (!reader.isEndElement()) {
                        if (reader.isStartElement()) {

                            if (reader.isStartElement()
                                    && new javax.xml.namespace.QName(
                                            "http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/",
                                            "getPolicyByPnrTxtResponse").equals(reader.getName())) {

                                object.setGetPolicyByPnrTxtResponse(GetPolicyByPnrTxtResponse.Factory.parse(reader));

                            } // End of if for expected property start element

                            else {
                                // A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement "
                                        + reader.getLocalName());
                            }

                        }
                        else {
                            reader.next();
                        }
                    } // end of while loop

                }
                catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class

    }

    public static class AbstractLiantuoRequest implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
                name = abstractLiantuoRequest
                Namespace URI = http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/
                Namespace Prefix = ns1
                */

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if (namespace.equals("http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/")) {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
        * field for AgencyCode
        */

        protected java.lang.String localAgencyCode;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localAgencyCodeTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getAgencyCode() {
            return localAgencyCode;
        }

        /**
           * Auto generated setter method
           * @param param AgencyCode
           */
        public void setAgencyCode(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localAgencyCodeTracker = true;
            }
            else {
                localAgencyCodeTracker = false;

            }

            this.localAgencyCode = param;

        }

        /**
        * field for Sign
        */

        protected java.lang.String localSign;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localSignTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getSign() {
            return localSign;
        }

        /**
           * Auto generated setter method
           * @param param Sign
           */
        public void setSign(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localSignTracker = true;
            }
            else {
                localSignTracker = false;

            }

            this.localSign = param;

        }

        /**
        * isReaderMTOMAware
        * @return true if the reader supports MTOM
        */
        public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader
                        .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            }
            catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
        public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    parentQName) {

                public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    AbstractLiantuoRequest.this.serialize(parentQName, factory, xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(parentQName, factory, dataSource);

        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter, boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();

            if ((namespace != null) && (namespace.trim().length() > 0)) {
                java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                if (writerPrefix != null) {
                    xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                }
                else {
                    if (prefix == null) {
                        prefix = generatePrefix(namespace);
                    }

                    xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }
            }
            else {
                xmlWriter.writeStartElement(parentQName.getLocalPart());
            }

            if (serializeType) {

                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/");
                if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix
                            + ":abstractLiantuoRequest", xmlWriter);
                }
                else {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            "abstractLiantuoRequest", xmlWriter);
                }

            }
            if (localAgencyCodeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "agencyCode", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "agencyCode");
                    }

                }
                else {
                    xmlWriter.writeStartElement("agencyCode");
                }

                if (localAgencyCode == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("agencyCode cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localAgencyCode);

                }

                xmlWriter.writeEndElement();
            }
            if (localSignTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "sign", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "sign");
                    }

                }
                else {
                    xmlWriter.writeStartElement("sign");
                }

                if (localSign == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("sign cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localSign);

                }

                xmlWriter.writeEndElement();
            }
            xmlWriter.writeEndElement();

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qnames[i]));
                        }
                    }
                    else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
        * Register a namespace prefix
        */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (localAgencyCodeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "agencyCode"));

                if (localAgencyCode != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAgencyCode));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("agencyCode cannot be null!!");
                }
            }
            if (localSignTracker) {
                elementList.add(new javax.xml.namespace.QName("", "sign"));

                if (localSign != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSign));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("sign cannot be null!!");
                }
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(),
                    attribList.toArray());

        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {

            /**
            * static method to create the object
            * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
            *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
            * Postcondition: If this object is an element, the reader is positioned at its end element
            *                If this object is a complex type, the reader is positioned at the end element of its outer element
            */
            public static AbstractLiantuoRequest parse(javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                AbstractLiantuoRequest object = null;

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance", "type");
                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

                            if (!"abstractLiantuoRequest".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (AbstractLiantuoRequest) ExtensionMapper.getTypeObject(nsUri, type, reader);
                            }

                            throw new org.apache.axis2.databinding.ADBException(
                                    "The an abstract class can not be instantiated !!!");

                        }

                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "agencyCode").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setAgencyCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "sign").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setSign(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement "
                                + reader.getLocalName());

                }
                catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class

    }

    public static class ExtensionMapper {

        public static java.lang.Object getTypeObject(java.lang.String namespaceURI, java.lang.String typeName,
                javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {

            if ("http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/".equals(namespaceURI)
                    && "getPolicyByPnrTxtResponse".equals(typeName)) {

                return GetPolicyByPnrTxtResponse.Factory.parse(reader);

            }

            if ("http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/".equals(namespaceURI)
                    && "getPolicyByPnrTxtRequest".equals(typeName)) {

                return GetPolicyByPnrTxtRequest.Factory.parse(reader);

            }

            if ("http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/".equals(namespaceURI)
                    && "getPolicyByPnrTxt".equals(typeName)) {

                return GetPolicyByPnrTxt.Factory.parse(reader);

            }

            if ("http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/".equals(namespaceURI)
                    && "getPolicyByPnrTxtReply".equals(typeName)) {

                return GetPolicyByPnrTxtReply.Factory.parse(reader);

            }

            if ("http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/".equals(namespaceURI)
                    && "abstractLiantuoRequest".equals(typeName)) {

                return AbstractLiantuoRequest.Factory.parse(reader);

            }

            if ("http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/".equals(namespaceURI)
                    && "abstractLiantuoReply".equals(typeName)) {

                return AbstractLiantuoReply.Factory.parse(reader);

            }

            if ("http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/".equals(namespaceURI)
                    && "wsPolicyData".equals(typeName)) {

                return WsPolicyData.Factory.parse(reader);

            }

            throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
        }

    }

    public static class GetPolicyByPnrTxtResponse implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
                name = getPolicyByPnrTxtResponse
                Namespace URI = http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/
                Namespace Prefix = ns1
                */

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if (namespace.equals("http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/")) {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
        * field for _return
        */

        protected GetPolicyByPnrTxtReply local_return;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean local_returnTracker = false;

        /**
        * Auto generated getter method
        * @return GetPolicyByPnrTxtReply
        */
        public GetPolicyByPnrTxtReply get_return() {
            return local_return;
        }

        /**
           * Auto generated setter method
           * @param param _return
           */
        public void set_return(GetPolicyByPnrTxtReply param) {

            if (param != null) {
                //update the setting tracker
                local_returnTracker = true;
            }
            else {
                local_returnTracker = false;

            }

            this.local_return = param;

        }

        /**
        * isReaderMTOMAware
        * @return true if the reader supports MTOM
        */
        public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader
                        .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            }
            catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
        public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    parentQName) {

                public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    GetPolicyByPnrTxtResponse.this.serialize(parentQName, factory, xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(parentQName, factory, dataSource);

        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter, boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();

            if ((namespace != null) && (namespace.trim().length() > 0)) {
                java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                if (writerPrefix != null) {
                    xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                }
                else {
                    if (prefix == null) {
                        prefix = generatePrefix(namespace);
                    }

                    xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }
            }
            else {
                xmlWriter.writeStartElement(parentQName.getLocalPart());
            }

            if (serializeType) {

                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/");
                if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix
                            + ":getPolicyByPnrTxtResponse", xmlWriter);
                }
                else {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            "getPolicyByPnrTxtResponse", xmlWriter);
                }

            }
            if (local_returnTracker) {
                if (local_return == null) {
                    throw new org.apache.axis2.databinding.ADBException("return cannot be null!!");
                }
                local_return.serialize(new javax.xml.namespace.QName("", "return"), factory, xmlWriter);
            }
            xmlWriter.writeEndElement();

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qnames[i]));
                        }
                    }
                    else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
        * Register a namespace prefix
        */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (local_returnTracker) {
                elementList.add(new javax.xml.namespace.QName("", "return"));

                if (local_return == null) {
                    throw new org.apache.axis2.databinding.ADBException("return cannot be null!!");
                }
                elementList.add(local_return);
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(),
                    attribList.toArray());

        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {

            /**
            * static method to create the object
            * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
            *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
            * Postcondition: If this object is an element, the reader is positioned at its end element
            *                If this object is a complex type, the reader is positioned at the end element of its outer element
            */
            public static GetPolicyByPnrTxtResponse parse(javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                GetPolicyByPnrTxtResponse object = new GetPolicyByPnrTxtResponse();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance", "type");
                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

                            if (!"getPolicyByPnrTxtResponse".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (GetPolicyByPnrTxtResponse) ExtensionMapper.getTypeObject(nsUri, type, reader);
                            }

                        }

                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "return").equals(reader.getName())) {

                        object.set_return(GetPolicyByPnrTxtReply.Factory.parse(reader));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement "
                                + reader.getLocalName());

                }
                catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class

    }

    public static class GetPolicyByPnrTxtReply extends AbstractLiantuoReply implements
            org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
                name = getPolicyByPnrTxtReply
                Namespace URI = http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/
                Namespace Prefix = ns1
                */

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if (namespace.equals("http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/")) {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
        * field for Param1
        */

        protected java.lang.String localParam1;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam1Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam1() {
            return localParam1;
        }

        /**
           * Auto generated setter method
           * @param param Param1
           */
        public void setParam1(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam1Tracker = true;
            }
            else {
                localParam1Tracker = false;

            }

            this.localParam1 = param;

        }

        /**
        * field for Param2
        */

        protected java.lang.String localParam2;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam2Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam2() {
            return localParam2;
        }

        /**
           * Auto generated setter method
           * @param param Param2
           */
        public void setParam2(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam2Tracker = true;
            }
            else {
                localParam2Tracker = false;

            }

            this.localParam2 = param;

        }

        /**
        * field for Param3
        */

        protected java.lang.String localParam3;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam3Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam3() {
            return localParam3;
        }

        /**
           * Auto generated setter method
           * @param param Param3
           */
        public void setParam3(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam3Tracker = true;
            }
            else {
                localParam3Tracker = false;

            }

            this.localParam3 = param;

        }

        /**
        * field for Param4
        */

        protected java.lang.String localParam4;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam4Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam4() {
            return localParam4;
        }

        /**
           * Auto generated setter method
           * @param param Param4
           */
        public void setParam4(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam4Tracker = true;
            }
            else {
                localParam4Tracker = false;

            }

            this.localParam4 = param;

        }

        /**
        * field for PolicyList
        * This was an Array!
        */

        protected WsPolicyData[] localPolicyList;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localPolicyListTracker = false;

        /**
        * Auto generated getter method
        * @return WsPolicyData[]
        */
        public WsPolicyData[] getPolicyList() {
            return localPolicyList;
        }

        /**
         * validate the array for PolicyList
         */
        protected void validatePolicyList(WsPolicyData[] param) {

        }

        /**
         * Auto generated setter method
         * @param param PolicyList
         */
        public void setPolicyList(WsPolicyData[] param) {

            validatePolicyList(param);

            if (param != null) {
                //update the setting tracker
                localPolicyListTracker = true;
            }
            else {
                localPolicyListTracker = true;

            }

            this.localPolicyList = param;
        }

        /**
        * Auto generated add method for the array for convenience
        * @param param WsPolicyData
        */
        public void addPolicyList(WsPolicyData param) {
            if (localPolicyList == null) {
                localPolicyList = new WsPolicyData[] {};
            }

            //update the setting tracker
            localPolicyListTracker = true;

            java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localPolicyList);
            list.add(param);
            this.localPolicyList = (WsPolicyData[]) list.toArray(new WsPolicyData[list.size()]);

        }

        /**
        * isReaderMTOMAware
        * @return true if the reader supports MTOM
        */
        public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader
                        .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            }
            catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
        public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    parentQName) {

                public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    GetPolicyByPnrTxtReply.this.serialize(parentQName, factory, xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(parentQName, factory, dataSource);

        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter, boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();

            if ((namespace != null) && (namespace.trim().length() > 0)) {
                java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                if (writerPrefix != null) {
                    xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                }
                else {
                    if (prefix == null) {
                        prefix = generatePrefix(namespace);
                    }

                    xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }
            }
            else {
                xmlWriter.writeStartElement(parentQName.getLocalPart());
            }

            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/");
            if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix
                        + ":getPolicyByPnrTxtReply", xmlWriter);
            }
            else {
                writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "getPolicyByPnrTxtReply",
                        xmlWriter);
            }

            if (localReturnCodeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "returnCode", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "returnCode");
                    }

                }
                else {
                    xmlWriter.writeStartElement("returnCode");
                }

                if (localReturnCode == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("returnCode cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localReturnCode);

                }

                xmlWriter.writeEndElement();
            }
            if (localReturnMessageTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "returnMessage", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "returnMessage");
                    }

                }
                else {
                    xmlWriter.writeStartElement("returnMessage");
                }

                if (localReturnMessage == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("returnMessage cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localReturnMessage);

                }

                xmlWriter.writeEndElement();
            }
            if (localParam1Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param1", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param1");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param1");
                }

                if (localParam1 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param1 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam1);

                }

                xmlWriter.writeEndElement();
            }
            if (localParam2Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param2", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param2");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param2");
                }

                if (localParam2 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param2 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam2);

                }

                xmlWriter.writeEndElement();
            }
            if (localParam3Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param3", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param3");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param3");
                }

                if (localParam3 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param3 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam3);

                }

                xmlWriter.writeEndElement();
            }
            if (localParam4Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param4", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param4");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param4");
                }

                if (localParam4 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param4 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam4);

                }

                xmlWriter.writeEndElement();
            }
            if (localPolicyListTracker) {
                if (localPolicyList != null) {
                    for (int i = 0; i < localPolicyList.length; i++) {
                        if (localPolicyList[i] != null) {
                            localPolicyList[i].serialize(new javax.xml.namespace.QName("", "policyList"), factory,
                                    xmlWriter);
                        }
                        else {

                            // write null attribute
                            java.lang.String namespace2 = "";
                            if (!namespace2.equals("")) {
                                java.lang.String prefix2 = xmlWriter.getPrefix(namespace2);

                                if (prefix2 == null) {
                                    prefix2 = generatePrefix(namespace2);

                                    xmlWriter.writeStartElement(prefix2, "policyList", namespace2);
                                    xmlWriter.writeNamespace(prefix2, namespace2);
                                    xmlWriter.setPrefix(prefix2, namespace2);

                                }
                                else {
                                    xmlWriter.writeStartElement(namespace2, "policyList");
                                }

                            }
                            else {
                                xmlWriter.writeStartElement("policyList");
                            }

                            // write the nil attribute
                            writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
                            xmlWriter.writeEndElement();

                        }

                    }
                }
                else {

                    // write null attribute
                    java.lang.String namespace2 = "";
                    if (!namespace2.equals("")) {
                        java.lang.String prefix2 = xmlWriter.getPrefix(namespace2);

                        if (prefix2 == null) {
                            prefix2 = generatePrefix(namespace2);

                            xmlWriter.writeStartElement(prefix2, "policyList", namespace2);
                            xmlWriter.writeNamespace(prefix2, namespace2);
                            xmlWriter.setPrefix(prefix2, namespace2);

                        }
                        else {
                            xmlWriter.writeStartElement(namespace2, "policyList");
                        }

                    }
                    else {
                        xmlWriter.writeStartElement("policyList");
                    }

                    // write the nil attribute
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
                    xmlWriter.writeEndElement();

                }
            }
            xmlWriter.writeEndElement();

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qnames[i]));
                        }
                    }
                    else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
        * Register a namespace prefix
        */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance", "type"));
            attribList.add(new javax.xml.namespace.QName(
                    "http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/",
                    "getPolicyByPnrTxtReply"));
            if (localReturnCodeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "returnCode"));

                if (localReturnCode != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localReturnCode));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("returnCode cannot be null!!");
                }
            }
            if (localReturnMessageTracker) {
                elementList.add(new javax.xml.namespace.QName("", "returnMessage"));

                if (localReturnMessage != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localReturnMessage));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("returnMessage cannot be null!!");
                }
            }
            if (localParam1Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param1"));

                if (localParam1 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam1));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param1 cannot be null!!");
                }
            }
            if (localParam2Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param2"));

                if (localParam2 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam2));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param2 cannot be null!!");
                }
            }
            if (localParam3Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param3"));

                if (localParam3 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam3));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param3 cannot be null!!");
                }
            }
            if (localParam4Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param4"));

                if (localParam4 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam4));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param4 cannot be null!!");
                }
            }
            if (localPolicyListTracker) {
                if (localPolicyList != null) {
                    for (int i = 0; i < localPolicyList.length; i++) {

                        if (localPolicyList[i] != null) {
                            elementList.add(new javax.xml.namespace.QName("", "policyList"));
                            elementList.add(localPolicyList[i]);
                        }
                        else {

                            elementList.add(new javax.xml.namespace.QName("", "policyList"));
                            elementList.add(null);

                        }

                    }
                }
                else {

                    elementList.add(new javax.xml.namespace.QName("", "policyList"));
                    elementList.add(localPolicyList);

                }

            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(),
                    attribList.toArray());

        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {

            /**
            * static method to create the object
            * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
            *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
            * Postcondition: If this object is an element, the reader is positioned at its end element
            *                If this object is a complex type, the reader is positioned at the end element of its outer element
            */
            public static GetPolicyByPnrTxtReply parse(javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                GetPolicyByPnrTxtReply object = new GetPolicyByPnrTxtReply();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance", "type");
                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

                            if (!"getPolicyByPnrTxtReply".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (GetPolicyByPnrTxtReply) ExtensionMapper.getTypeObject(nsUri, type, reader);
                            }

                        }

                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    java.util.ArrayList list7 = new java.util.ArrayList();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "returnCode").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setReturnCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "returnMessage").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setReturnMessage(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param1").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param2").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param3").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam3(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param4").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam4(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "policyList").equals(reader.getName())) {

                        // Process the array and step past its final element's end.

                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
                        if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
                            list7.add(null);
                            reader.next();
                        }
                        else {
                            list7.add(WsPolicyData.Factory.parse(reader));
                        }
                        //loop until we find a start element that is not part of this array
                        boolean loopDone7 = false;
                        while (!loopDone7) {
                            // We should be at the end element, but make sure
                            while (!reader.isEndElement())
                                reader.next();
                            // Step out of this element
                            reader.next();
                            // Step to next element event.
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            if (reader.isEndElement()) {
                                //two continuous end elements means we are exiting the xml structure
                                loopDone7 = true;
                            }
                            else {
                                if (new javax.xml.namespace.QName("", "policyList").equals(reader.getName())) {

                                    nillableValue = reader.getAttributeValue(
                                            "http://www.w3.org/2001/XMLSchema-instance", "nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
                                        list7.add(null);
                                        reader.next();
                                    }
                                    else {
                                        list7.add(WsPolicyData.Factory.parse(reader));
                                    }
                                }
                                else {
                                    loopDone7 = true;
                                }
                            }
                        }
                        // call the converter utility  to convert and set the array

                        object.setPolicyList((WsPolicyData[]) org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToArray(WsPolicyData.class, list7));

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement "
                                + reader.getLocalName());

                }
                catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class

    }

    public static class WsPolicyData implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
                name = wsPolicyData
                Namespace URI = http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/
                Namespace Prefix = ns1
                */

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if (namespace.equals("http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/")) {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
        * field for AirlineCode
        */

        protected java.lang.String localAirlineCode;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localAirlineCodeTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getAirlineCode() {
            return localAirlineCode;
        }

        /**
           * Auto generated setter method
           * @param param AirlineCode
           */
        public void setAirlineCode(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localAirlineCodeTracker = true;
            }
            else {
                localAirlineCodeTracker = false;

            }

            this.localAirlineCode = param;

        }

        /**
        * field for ArrivalExclude
        */

        protected java.lang.String localArrivalExclude;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localArrivalExcludeTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getArrivalExclude() {
            return localArrivalExclude;
        }

        /**
           * Auto generated setter method
           * @param param ArrivalExclude
           */
        public void setArrivalExclude(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localArrivalExcludeTracker = true;
            }
            else {
                localArrivalExcludeTracker = false;

            }

            this.localArrivalExclude = param;

        }

        /**
        * field for Comment
        */

        protected java.lang.String localComment;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localCommentTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getComment() {
            return localComment;
        }

        /**
           * Auto generated setter method
           * @param param Comment
           */
        public void setComment(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localCommentTracker = true;
            }
            else {
                localCommentTracker = false;

            }

            this.localComment = param;

        }

        /**
        * field for CommisionMoney
        */

        protected double localCommisionMoney;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localCommisionMoneyTracker = false;

        /**
        * Auto generated getter method
        * @return double
        */
        public double getCommisionMoney() {
            return localCommisionMoney;
        }

        /**
           * Auto generated setter method
           * @param param CommisionMoney
           */
        public void setCommisionMoney(double param) {

            // setting primitive attribute tracker to true

            if (java.lang.Double.isNaN(param)) {
                localCommisionMoneyTracker = false;

            }
            else {
                localCommisionMoneyTracker = true;
            }

            this.localCommisionMoney = param;

        }

        /**
        * field for CommisionPoint
        */

        protected float localCommisionPoint;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localCommisionPointTracker = false;

        /**
        * Auto generated getter method
        * @return float
        */
        public float getCommisionPoint() {
            return localCommisionPoint;
        }

        /**
           * Auto generated setter method
           * @param param CommisionPoint
           */
        public void setCommisionPoint(float param) {

            // setting primitive attribute tracker to true

            if (java.lang.Float.isNaN(param)) {
                localCommisionPointTracker = false;

            }
            else {
                localCommisionPointTracker = true;
            }

            this.localCommisionPoint = param;

        }

        /**
        * field for DepartureExclude
        */

        protected java.lang.String localDepartureExclude;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localDepartureExcludeTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getDepartureExclude() {
            return localDepartureExclude;
        }

        /**
           * Auto generated setter method
           * @param param DepartureExclude
           */
        public void setDepartureExclude(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localDepartureExcludeTracker = true;
            }
            else {
                localDepartureExcludeTracker = false;

            }

            this.localDepartureExclude = param;

        }

        /**
        * field for ExpiredDate
        */

        protected java.lang.String localExpiredDate;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localExpiredDateTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getExpiredDate() {
            return localExpiredDate;
        }

        /**
           * Auto generated setter method
           * @param param ExpiredDate
           */
        public void setExpiredDate(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localExpiredDateTracker = true;
            }
            else {
                localExpiredDateTracker = false;

            }

            this.localExpiredDate = param;

        }

        /**
        * field for FlightCourse
        */

        protected java.lang.String localFlightCourse;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localFlightCourseTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getFlightCourse() {
            return localFlightCourse;
        }

        /**
           * Auto generated setter method
           * @param param FlightCourse
           */
        public void setFlightCourse(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localFlightCourseTracker = true;
            }
            else {
                localFlightCourseTracker = false;

            }

            this.localFlightCourse = param;

        }

        /**
        * field for FlightCycle
        */

        protected java.lang.String localFlightCycle;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localFlightCycleTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getFlightCycle() {
            return localFlightCycle;
        }

        /**
           * Auto generated setter method
           * @param param FlightCycle
           */
        public void setFlightCycle(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localFlightCycleTracker = true;
            }
            else {
                localFlightCycleTracker = false;

            }

            this.localFlightCycle = param;

        }

        /**
        * field for FlightNoExclude
        */

        protected java.lang.String localFlightNoExclude;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localFlightNoExcludeTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getFlightNoExclude() {
            return localFlightNoExclude;
        }

        /**
           * Auto generated setter method
           * @param param FlightNoExclude
           */
        public void setFlightNoExclude(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localFlightNoExcludeTracker = true;
            }
            else {
                localFlightNoExcludeTracker = false;

            }

            this.localFlightNoExclude = param;

        }

        /**
        * field for FlightNoIncluding
        */

        protected java.lang.String localFlightNoIncluding;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localFlightNoIncludingTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getFlightNoIncluding() {
            return localFlightNoIncluding;
        }

        /**
           * Auto generated setter method
           * @param param FlightNoIncluding
           */
        public void setFlightNoIncluding(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localFlightNoIncludingTracker = true;
            }
            else {
                localFlightNoIncludingTracker = false;

            }

            this.localFlightNoIncluding = param;

        }

        /**
        * field for NeedSwitchPNR
        */

        protected int localNeedSwitchPNR;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localNeedSwitchPNRTracker = false;

        /**
        * Auto generated getter method
        * @return int
        */
        public int getNeedSwitchPNR() {
            return localNeedSwitchPNR;
        }

        /**
           * Auto generated setter method
           * @param param NeedSwitchPNR
           */
        public void setNeedSwitchPNR(int param) {

            // setting primitive attribute tracker to true

            if (param == java.lang.Integer.MIN_VALUE) {
                localNeedSwitchPNRTracker = false;

            }
            else {
                localNeedSwitchPNRTracker = true;
            }

            this.localNeedSwitchPNR = param;

        }

        /**
        * field for Param1
        */

        protected java.lang.String localParam1;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam1Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam1() {
            return localParam1;
        }

        /**
           * Auto generated setter method
           * @param param Param1
           */
        public void setParam1(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam1Tracker = true;
            }
            else {
                localParam1Tracker = false;

            }

            this.localParam1 = param;

        }

        /**
        * field for Param2
        */

        protected java.lang.String localParam2;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam2Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam2() {
            return localParam2;
        }

        /**
           * Auto generated setter method
           * @param param Param2
           */
        public void setParam2(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam2Tracker = true;
            }
            else {
                localParam2Tracker = false;

            }

            this.localParam2 = param;

        }

        /**
        * field for Param3
        */

        protected java.lang.String localParam3;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam3Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam3() {
            return localParam3;
        }

        /**
           * Auto generated setter method
           * @param param Param3
           */
        public void setParam3(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam3Tracker = true;
            }
            else {
                localParam3Tracker = false;

            }

            this.localParam3 = param;

        }

        /**
        * field for Param4
        */

        protected java.lang.String localParam4;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam4Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam4() {
            return localParam4;
        }

        /**
           * Auto generated setter method
           * @param param Param4
           */
        public void setParam4(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam4Tracker = true;
            }
            else {
                localParam4Tracker = false;

            }

            this.localParam4 = param;

        }

        /**
        * field for PolicyBelongToFlag
        */

        protected java.lang.String localPolicyBelongToFlag;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localPolicyBelongToFlagTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getPolicyBelongToFlag() {
            return localPolicyBelongToFlag;
        }

        /**
           * Auto generated setter method
           * @param param PolicyBelongToFlag
           */
        public void setPolicyBelongToFlag(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localPolicyBelongToFlagTracker = true;
            }
            else {
                localPolicyBelongToFlagTracker = false;

            }

            this.localPolicyBelongToFlag = param;

        }

        /**
        * field for PolicyId
        */

        protected int localPolicyId;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localPolicyIdTracker = false;

        /**
        * Auto generated getter method
        * @return int
        */
        public int getPolicyId() {
            return localPolicyId;
        }

        /**
           * Auto generated setter method
           * @param param PolicyId
           */
        public void setPolicyId(int param) {

            // setting primitive attribute tracker to true

            if (param == java.lang.Integer.MIN_VALUE) {
                localPolicyIdTracker = false;

            }
            else {
                localPolicyIdTracker = true;
            }

            this.localPolicyId = param;

        }

        /**
        * field for PolicyType
        */

        protected java.lang.String localPolicyType;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localPolicyTypeTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getPolicyType() {
            return localPolicyType;
        }

        /**
           * Auto generated setter method
           * @param param PolicyType
           */
        public void setPolicyType(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localPolicyTypeTracker = true;
            }
            else {
                localPolicyTypeTracker = false;

            }

            this.localPolicyType = param;

        }

        /**
        * field for PrintTicketExpiredDate
        */

        protected java.lang.String localPrintTicketExpiredDate;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localPrintTicketExpiredDateTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getPrintTicketExpiredDate() {
            return localPrintTicketExpiredDate;
        }

        /**
           * Auto generated setter method
           * @param param PrintTicketExpiredDate
           */
        public void setPrintTicketExpiredDate(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localPrintTicketExpiredDateTracker = true;
            }
            else {
                localPrintTicketExpiredDateTracker = false;

            }

            this.localPrintTicketExpiredDate = param;

        }

        /**
        * field for PrintTicketStartDate
        */

        protected java.lang.String localPrintTicketStartDate;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localPrintTicketStartDateTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getPrintTicketStartDate() {
            return localPrintTicketStartDate;
        }

        /**
           * Auto generated setter method
           * @param param PrintTicketStartDate
           */
        public void setPrintTicketStartDate(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localPrintTicketStartDateTracker = true;
            }
            else {
                localPrintTicketStartDateTracker = false;

            }

            this.localPrintTicketStartDate = param;

        }

        /**
        * field for ProductType
        */

        protected int localProductType;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localProductTypeTracker = false;

        /**
        * Auto generated getter method
        * @return int
        */
        public int getProductType() {
            return localProductType;
        }

        /**
           * Auto generated setter method
           * @param param ProductType
           */
        public void setProductType(int param) {

            // setting primitive attribute tracker to true

            if (param == java.lang.Integer.MIN_VALUE) {
                localProductTypeTracker = false;

            }
            else {
                localProductTypeTracker = true;
            }

            this.localProductType = param;

        }

        /**
        * field for RouteType
        */

        protected java.lang.String localRouteType;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localRouteTypeTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getRouteType() {
            return localRouteType;
        }

        /**
           * Auto generated setter method
           * @param param RouteType
           */
        public void setRouteType(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localRouteTypeTracker = true;
            }
            else {
                localRouteTypeTracker = false;

            }

            this.localRouteType = param;

        }

        /**
        * field for SeatClass
        */

        protected java.lang.String localSeatClass;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localSeatClassTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getSeatClass() {
            return localSeatClass;
        }

        /**
           * Auto generated setter method
           * @param param SeatClass
           */
        public void setSeatClass(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localSeatClassTracker = true;
            }
            else {
                localSeatClassTracker = false;

            }

            this.localSeatClass = param;

        }

        /**
        * field for StartDate
        */

        protected java.lang.String localStartDate;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localStartDateTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getStartDate() {
            return localStartDate;
        }

        /**
           * Auto generated setter method
           * @param param StartDate
           */
        public void setStartDate(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localStartDateTracker = true;
            }
            else {
                localStartDateTracker = false;

            }

            this.localStartDate = param;

        }

        /**
        * field for SupplyOfficeNo
        */

        protected java.lang.String localSupplyOfficeNo;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localSupplyOfficeNoTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getSupplyOfficeNo() {
            return localSupplyOfficeNo;
        }

        /**
           * Auto generated setter method
           * @param param SupplyOfficeNo
           */
        public void setSupplyOfficeNo(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localSupplyOfficeNoTracker = true;
            }
            else {
                localSupplyOfficeNoTracker = false;

            }

            this.localSupplyOfficeNo = param;

        }

        /**
        * field for TicketSpeed
        */

        protected java.lang.String localTicketSpeed;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localTicketSpeedTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getTicketSpeed() {
            return localTicketSpeed;
        }

        /**
           * Auto generated setter method
           * @param param TicketSpeed
           */
        public void setTicketSpeed(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localTicketSpeedTracker = true;
            }
            else {
                localTicketSpeedTracker = false;

            }

            this.localTicketSpeed = param;

        }

        /**
        * field for VtWorkTime
        */

        protected java.lang.String localVtWorkTime;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localVtWorkTimeTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getVtWorkTime() {
            return localVtWorkTime;
        }

        /**
           * Auto generated setter method
           * @param param VtWorkTime
           */
        public void setVtWorkTime(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localVtWorkTimeTracker = true;
            }
            else {
                localVtWorkTimeTracker = false;

            }

            this.localVtWorkTime = param;

        }

        /**
        * field for WorkTime
        */

        protected java.lang.String localWorkTime;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localWorkTimeTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getWorkTime() {
            return localWorkTime;
        }

        /**
           * Auto generated setter method
           * @param param WorkTime
           */
        public void setWorkTime(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localWorkTimeTracker = true;
            }
            else {
                localWorkTimeTracker = false;

            }

            this.localWorkTime = param;

        }

        /**
        * isReaderMTOMAware
        * @return true if the reader supports MTOM
        */
        public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader
                        .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            }
            catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
        public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    parentQName) {

                public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    WsPolicyData.this.serialize(parentQName, factory, xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(parentQName, factory, dataSource);

        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter, boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();

            if ((namespace != null) && (namespace.trim().length() > 0)) {
                java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                if (writerPrefix != null) {
                    xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                }
                else {
                    if (prefix == null) {
                        prefix = generatePrefix(namespace);
                    }

                    xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }
            }
            else {
                xmlWriter.writeStartElement(parentQName.getLocalPart());
            }

            if (serializeType) {

                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/");
                if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix
                            + ":wsPolicyData", xmlWriter);
                }
                else {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "wsPolicyData",
                            xmlWriter);
                }

            }
            if (localAirlineCodeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "airlineCode", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "airlineCode");
                    }

                }
                else {
                    xmlWriter.writeStartElement("airlineCode");
                }

                if (localAirlineCode == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("airlineCode cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localAirlineCode);

                }

                xmlWriter.writeEndElement();
            }
            if (localArrivalExcludeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "arrivalExclude", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "arrivalExclude");
                    }

                }
                else {
                    xmlWriter.writeStartElement("arrivalExclude");
                }

                if (localArrivalExclude == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("arrivalExclude cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localArrivalExclude);

                }

                xmlWriter.writeEndElement();
            }
            if (localCommentTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "comment", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "comment");
                    }

                }
                else {
                    xmlWriter.writeStartElement("comment");
                }

                if (localComment == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("comment cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localComment);

                }

                xmlWriter.writeEndElement();
            }
            if (localCommisionMoneyTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "commisionMoney", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "commisionMoney");
                    }

                }
                else {
                    xmlWriter.writeStartElement("commisionMoney");
                }

                if (java.lang.Double.isNaN(localCommisionMoney)) {

                    throw new org.apache.axis2.databinding.ADBException("commisionMoney cannot be null!!");

                }
                else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localCommisionMoney));
                }

                xmlWriter.writeEndElement();
            }
            if (localCommisionPointTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "commisionPoint", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "commisionPoint");
                    }

                }
                else {
                    xmlWriter.writeStartElement("commisionPoint");
                }

                if (java.lang.Float.isNaN(localCommisionPoint)) {

                    throw new org.apache.axis2.databinding.ADBException("commisionPoint cannot be null!!");

                }
                else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localCommisionPoint));
                }

                xmlWriter.writeEndElement();
            }
            if (localDepartureExcludeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "departureExclude", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "departureExclude");
                    }

                }
                else {
                    xmlWriter.writeStartElement("departureExclude");
                }

                if (localDepartureExclude == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("departureExclude cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localDepartureExclude);

                }

                xmlWriter.writeEndElement();
            }
            if (localExpiredDateTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "expiredDate", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "expiredDate");
                    }

                }
                else {
                    xmlWriter.writeStartElement("expiredDate");
                }

                if (localExpiredDate == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("expiredDate cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localExpiredDate);

                }

                xmlWriter.writeEndElement();
            }
            if (localFlightCourseTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "flightCourse", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "flightCourse");
                    }

                }
                else {
                    xmlWriter.writeStartElement("flightCourse");
                }

                if (localFlightCourse == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("flightCourse cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localFlightCourse);

                }

                xmlWriter.writeEndElement();
            }
            if (localFlightCycleTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "flightCycle", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "flightCycle");
                    }

                }
                else {
                    xmlWriter.writeStartElement("flightCycle");
                }

                if (localFlightCycle == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("flightCycle cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localFlightCycle);

                }

                xmlWriter.writeEndElement();
            }
            if (localFlightNoExcludeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "flightNoExclude", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "flightNoExclude");
                    }

                }
                else {
                    xmlWriter.writeStartElement("flightNoExclude");
                }

                if (localFlightNoExclude == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("flightNoExclude cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localFlightNoExclude);

                }

                xmlWriter.writeEndElement();
            }
            if (localFlightNoIncludingTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "flightNoIncluding", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "flightNoIncluding");
                    }

                }
                else {
                    xmlWriter.writeStartElement("flightNoIncluding");
                }

                if (localFlightNoIncluding == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("flightNoIncluding cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localFlightNoIncluding);

                }

                xmlWriter.writeEndElement();
            }
            if (localNeedSwitchPNRTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "needSwitchPNR", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "needSwitchPNR");
                    }

                }
                else {
                    xmlWriter.writeStartElement("needSwitchPNR");
                }

                if (localNeedSwitchPNR == java.lang.Integer.MIN_VALUE) {

                    throw new org.apache.axis2.databinding.ADBException("needSwitchPNR cannot be null!!");

                }
                else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localNeedSwitchPNR));
                }

                xmlWriter.writeEndElement();
            }
            if (localParam1Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param1", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param1");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param1");
                }

                if (localParam1 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param1 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam1);

                }

                xmlWriter.writeEndElement();
            }
            if (localParam2Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param2", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param2");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param2");
                }

                if (localParam2 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param2 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam2);

                }

                xmlWriter.writeEndElement();
            }
            if (localParam3Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param3", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param3");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param3");
                }

                if (localParam3 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param3 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam3);

                }

                xmlWriter.writeEndElement();
            }
            if (localParam4Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param4", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param4");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param4");
                }

                if (localParam4 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param4 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam4);

                }

                xmlWriter.writeEndElement();
            }
            if (localPolicyBelongToFlagTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "policyBelongToFlag", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "policyBelongToFlag");
                    }

                }
                else {
                    xmlWriter.writeStartElement("policyBelongToFlag");
                }

                if (localPolicyBelongToFlag == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("policyBelongToFlag cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localPolicyBelongToFlag);

                }

                xmlWriter.writeEndElement();
            }
            if (localPolicyIdTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "policyId", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "policyId");
                    }

                }
                else {
                    xmlWriter.writeStartElement("policyId");
                }

                if (localPolicyId == java.lang.Integer.MIN_VALUE) {

                    throw new org.apache.axis2.databinding.ADBException("policyId cannot be null!!");

                }
                else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localPolicyId));
                }

                xmlWriter.writeEndElement();
            }
            if (localPolicyTypeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "policyType", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "policyType");
                    }

                }
                else {
                    xmlWriter.writeStartElement("policyType");
                }

                if (localPolicyType == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("policyType cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localPolicyType);

                }

                xmlWriter.writeEndElement();
            }
            if (localPrintTicketExpiredDateTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "printTicketExpiredDate", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "printTicketExpiredDate");
                    }

                }
                else {
                    xmlWriter.writeStartElement("printTicketExpiredDate");
                }

                if (localPrintTicketExpiredDate == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("printTicketExpiredDate cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localPrintTicketExpiredDate);

                }

                xmlWriter.writeEndElement();
            }
            if (localPrintTicketStartDateTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "printTicketStartDate", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "printTicketStartDate");
                    }

                }
                else {
                    xmlWriter.writeStartElement("printTicketStartDate");
                }

                if (localPrintTicketStartDate == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("printTicketStartDate cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localPrintTicketStartDate);

                }

                xmlWriter.writeEndElement();
            }
            if (localProductTypeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "productType", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "productType");
                    }

                }
                else {
                    xmlWriter.writeStartElement("productType");
                }

                if (localProductType == java.lang.Integer.MIN_VALUE) {

                    throw new org.apache.axis2.databinding.ADBException("productType cannot be null!!");

                }
                else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localProductType));
                }

                xmlWriter.writeEndElement();
            }
            if (localRouteTypeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "routeType", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "routeType");
                    }

                }
                else {
                    xmlWriter.writeStartElement("routeType");
                }

                if (localRouteType == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("routeType cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localRouteType);

                }

                xmlWriter.writeEndElement();
            }
            if (localSeatClassTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "seatClass", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "seatClass");
                    }

                }
                else {
                    xmlWriter.writeStartElement("seatClass");
                }

                if (localSeatClass == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("seatClass cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localSeatClass);

                }

                xmlWriter.writeEndElement();
            }
            if (localStartDateTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "startDate", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "startDate");
                    }

                }
                else {
                    xmlWriter.writeStartElement("startDate");
                }

                if (localStartDate == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("startDate cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localStartDate);

                }

                xmlWriter.writeEndElement();
            }
            if (localSupplyOfficeNoTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "supplyOfficeNo", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "supplyOfficeNo");
                    }

                }
                else {
                    xmlWriter.writeStartElement("supplyOfficeNo");
                }

                if (localSupplyOfficeNo == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("supplyOfficeNo cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localSupplyOfficeNo);

                }

                xmlWriter.writeEndElement();
            }
            if (localTicketSpeedTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "ticketSpeed", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "ticketSpeed");
                    }

                }
                else {
                    xmlWriter.writeStartElement("ticketSpeed");
                }

                if (localTicketSpeed == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("ticketSpeed cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localTicketSpeed);

                }

                xmlWriter.writeEndElement();
            }
            if (localVtWorkTimeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "vtWorkTime", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "vtWorkTime");
                    }

                }
                else {
                    xmlWriter.writeStartElement("vtWorkTime");
                }

                if (localVtWorkTime == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("vtWorkTime cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localVtWorkTime);

                }

                xmlWriter.writeEndElement();
            }
            if (localWorkTimeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "workTime", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "workTime");
                    }

                }
                else {
                    xmlWriter.writeStartElement("workTime");
                }

                if (localWorkTime == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("workTime cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localWorkTime);

                }

                xmlWriter.writeEndElement();
            }
            xmlWriter.writeEndElement();

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qnames[i]));
                        }
                    }
                    else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
        * Register a namespace prefix
        */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (localAirlineCodeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "airlineCode"));

                if (localAirlineCode != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAirlineCode));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("airlineCode cannot be null!!");
                }
            }
            if (localArrivalExcludeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "arrivalExclude"));

                if (localArrivalExclude != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localArrivalExclude));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("arrivalExclude cannot be null!!");
                }
            }
            if (localCommentTracker) {
                elementList.add(new javax.xml.namespace.QName("", "comment"));

                if (localComment != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localComment));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("comment cannot be null!!");
                }
            }
            if (localCommisionMoneyTracker) {
                elementList.add(new javax.xml.namespace.QName("", "commisionMoney"));

                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCommisionMoney));
            }
            if (localCommisionPointTracker) {
                elementList.add(new javax.xml.namespace.QName("", "commisionPoint"));

                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCommisionPoint));
            }
            if (localDepartureExcludeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "departureExclude"));

                if (localDepartureExclude != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localDepartureExclude));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("departureExclude cannot be null!!");
                }
            }
            if (localExpiredDateTracker) {
                elementList.add(new javax.xml.namespace.QName("", "expiredDate"));

                if (localExpiredDate != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExpiredDate));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("expiredDate cannot be null!!");
                }
            }
            if (localFlightCourseTracker) {
                elementList.add(new javax.xml.namespace.QName("", "flightCourse"));

                if (localFlightCourse != null) {
                    elementList
                            .add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFlightCourse));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("flightCourse cannot be null!!");
                }
            }
            if (localFlightCycleTracker) {
                elementList.add(new javax.xml.namespace.QName("", "flightCycle"));

                if (localFlightCycle != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFlightCycle));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("flightCycle cannot be null!!");
                }
            }
            if (localFlightNoExcludeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "flightNoExclude"));

                if (localFlightNoExclude != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localFlightNoExclude));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("flightNoExclude cannot be null!!");
                }
            }
            if (localFlightNoIncludingTracker) {
                elementList.add(new javax.xml.namespace.QName("", "flightNoIncluding"));

                if (localFlightNoIncluding != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localFlightNoIncluding));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("flightNoIncluding cannot be null!!");
                }
            }
            if (localNeedSwitchPNRTracker) {
                elementList.add(new javax.xml.namespace.QName("", "needSwitchPNR"));

                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNeedSwitchPNR));
            }
            if (localParam1Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param1"));

                if (localParam1 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam1));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param1 cannot be null!!");
                }
            }
            if (localParam2Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param2"));

                if (localParam2 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam2));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param2 cannot be null!!");
                }
            }
            if (localParam3Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param3"));

                if (localParam3 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam3));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param3 cannot be null!!");
                }
            }
            if (localParam4Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param4"));

                if (localParam4 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam4));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param4 cannot be null!!");
                }
            }
            if (localPolicyBelongToFlagTracker) {
                elementList.add(new javax.xml.namespace.QName("", "policyBelongToFlag"));

                if (localPolicyBelongToFlag != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localPolicyBelongToFlag));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("policyBelongToFlag cannot be null!!");
                }
            }
            if (localPolicyIdTracker) {
                elementList.add(new javax.xml.namespace.QName("", "policyId"));

                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPolicyId));
            }
            if (localPolicyTypeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "policyType"));

                if (localPolicyType != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPolicyType));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("policyType cannot be null!!");
                }
            }
            if (localPrintTicketExpiredDateTracker) {
                elementList.add(new javax.xml.namespace.QName("", "printTicketExpiredDate"));

                if (localPrintTicketExpiredDate != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localPrintTicketExpiredDate));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("printTicketExpiredDate cannot be null!!");
                }
            }
            if (localPrintTicketStartDateTracker) {
                elementList.add(new javax.xml.namespace.QName("", "printTicketStartDate"));

                if (localPrintTicketStartDate != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localPrintTicketStartDate));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("printTicketStartDate cannot be null!!");
                }
            }
            if (localProductTypeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "productType"));

                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localProductType));
            }
            if (localRouteTypeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "routeType"));

                if (localRouteType != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRouteType));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("routeType cannot be null!!");
                }
            }
            if (localSeatClassTracker) {
                elementList.add(new javax.xml.namespace.QName("", "seatClass"));

                if (localSeatClass != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSeatClass));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("seatClass cannot be null!!");
                }
            }
            if (localStartDateTracker) {
                elementList.add(new javax.xml.namespace.QName("", "startDate"));

                if (localStartDate != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStartDate));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("startDate cannot be null!!");
                }
            }
            if (localSupplyOfficeNoTracker) {
                elementList.add(new javax.xml.namespace.QName("", "supplyOfficeNo"));

                if (localSupplyOfficeNo != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localSupplyOfficeNo));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("supplyOfficeNo cannot be null!!");
                }
            }
            if (localTicketSpeedTracker) {
                elementList.add(new javax.xml.namespace.QName("", "ticketSpeed"));

                if (localTicketSpeed != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTicketSpeed));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("ticketSpeed cannot be null!!");
                }
            }
            if (localVtWorkTimeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "vtWorkTime"));

                if (localVtWorkTime != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVtWorkTime));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("vtWorkTime cannot be null!!");
                }
            }
            if (localWorkTimeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "workTime"));

                if (localWorkTime != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localWorkTime));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("workTime cannot be null!!");
                }
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(),
                    attribList.toArray());

        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {

            /**
            * static method to create the object
            * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
            *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
            * Postcondition: If this object is an element, the reader is positioned at its end element
            *                If this object is a complex type, the reader is positioned at the end element of its outer element
            */
            public static WsPolicyData parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
                WsPolicyData object = new WsPolicyData();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();
                    if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance", "type");
                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

                            if (!"wsPolicyData".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (WsPolicyData) ExtensionMapper.getTypeObject(nsUri, type, reader);
                            }

                        }

                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "airlineCode").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setAirlineCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "arrivalExclude").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setArrivalExclude(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "comment").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setComment(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "commisionMoney").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setCommisionMoney(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToDouble(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                        object.setCommisionMoney(java.lang.Double.NaN);

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "commisionPoint").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setCommisionPoint(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToFloat(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                        object.setCommisionPoint(java.lang.Float.NaN);

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "departureExclude").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setDepartureExclude(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "expiredDate").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setExpiredDate(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "flightCourse").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setFlightCourse(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "flightCycle").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setFlightCycle(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "flightNoExclude").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setFlightNoExclude(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "flightNoIncluding").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setFlightNoIncluding(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "needSwitchPNR").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setNeedSwitchPNR(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                        object.setNeedSwitchPNR(java.lang.Integer.MIN_VALUE);

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param1").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param2").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param3").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam3(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param4").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam4(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "policyBelongToFlag").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setPolicyBelongToFlag(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "policyId").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setPolicyId(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                        object.setPolicyId(java.lang.Integer.MIN_VALUE);

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "policyType").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setPolicyType(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "printTicketExpiredDate").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setPrintTicketExpiredDate(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "printTicketStartDate").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setPrintTicketStartDate(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "productType").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setProductType(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                        object.setProductType(java.lang.Integer.MIN_VALUE);

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "routeType").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setRouteType(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "seatClass").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setSeatClass(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "startDate").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setStartDate(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "supplyOfficeNo").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setSupplyOfficeNo(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "ticketSpeed").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setTicketSpeed(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "vtWorkTime").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setVtWorkTime(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "workTime").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setWorkTime(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement "
                                + reader.getLocalName());

                }
                catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class

    }

    public static class GetPolicyByPnrTxt implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
                name = getPolicyByPnrTxt
                Namespace URI = http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/
                Namespace Prefix = ns1
                */

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if (namespace.equals("http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/")) {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
        * field for Request
        */

        protected GetPolicyByPnrTxtRequest localRequest;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localRequestTracker = false;

        /**
        * Auto generated getter method
        * @return GetPolicyByPnrTxtRequest
        */
        public GetPolicyByPnrTxtRequest getRequest() {
            return localRequest;
        }

        /**
           * Auto generated setter method
           * @param param Request
           */
        public void setRequest(GetPolicyByPnrTxtRequest param) {

            if (param != null) {
                //update the setting tracker
                localRequestTracker = true;
            }
            else {
                localRequestTracker = false;

            }

            this.localRequest = param;

        }

        /**
        * isReaderMTOMAware
        * @return true if the reader supports MTOM
        */
        public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader
                        .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            }
            catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
        public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    parentQName) {

                public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    GetPolicyByPnrTxt.this.serialize(parentQName, factory, xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(parentQName, factory, dataSource);

        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter, boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();

            if ((namespace != null) && (namespace.trim().length() > 0)) {
                java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                if (writerPrefix != null) {
                    xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                }
                else {
                    if (prefix == null) {
                        prefix = generatePrefix(namespace);
                    }

                    xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }
            }
            else {
                xmlWriter.writeStartElement(parentQName.getLocalPart());
            }

            if (serializeType) {

                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://getpolicybypnrtxt.b2b.service.version1_0.webservice.model.ltips.com/");
                if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix
                            + ":getPolicyByPnrTxt", xmlWriter);
                }
                else {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "getPolicyByPnrTxt",
                            xmlWriter);
                }

            }
            if (localRequestTracker) {
                if (localRequest == null) {
                    throw new org.apache.axis2.databinding.ADBException("request cannot be null!!");
                }
                localRequest.serialize(new javax.xml.namespace.QName("", "request"), factory, xmlWriter);
            }
            xmlWriter.writeEndElement();

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qnames[i]));
                        }
                    }
                    else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
        * Register a namespace prefix
        */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (localRequestTracker) {
                elementList.add(new javax.xml.namespace.QName("", "request"));

                if (localRequest == null) {
                    throw new org.apache.axis2.databinding.ADBException("request cannot be null!!");
                }
                elementList.add(localRequest);
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(),
                    attribList.toArray());

        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {

            /**
            * static method to create the object
            * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
            *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
            * Postcondition: If this object is an element, the reader is positioned at its end element
            *                If this object is a complex type, the reader is positioned at the end element of its outer element
            */
            public static GetPolicyByPnrTxt parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
                GetPolicyByPnrTxt object = new GetPolicyByPnrTxt();
                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance", "type");
                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

                            if (!"getPolicyByPnrTxt".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (GetPolicyByPnrTxt) ExtensionMapper.getTypeObject(nsUri, type, reader);
                            }

                        }

                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "request").equals(reader.getName())) {

                        object.setRequest(GetPolicyByPnrTxtRequest.Factory.parse(reader));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();
                    WriteLog.write("51book1.0GetPolicy", reader.getElementText());
                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement "
                                + reader.getLocalName());

                }
                catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class

    }

    private org.apache.axiom.om.OMElement toOM(
            com.webservice.fiveonebookv3.GetPolicyByPnrTxtServiceImpl_1_0ServiceStub.GetPolicyByPnrTxtE param,
            boolean optimizeContent) throws org.apache.axis2.AxisFault {

        try {
            return param
                    .getOMElement(
                            com.webservice.fiveonebookv3.GetPolicyByPnrTxtServiceImpl_1_0ServiceStub.GetPolicyByPnrTxtE.MY_QNAME,
                            org.apache.axiom.om.OMAbstractFactory.getOMFactory());
        }
        catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }

    }

    private org.apache.axiom.om.OMElement toOM(
            com.webservice.fiveonebookv3.GetPolicyByPnrTxtServiceImpl_1_0ServiceStub.GetPolicyByPnrTxtResponseE param,
            boolean optimizeContent) throws org.apache.axis2.AxisFault {

        try {
            return param
                    .getOMElement(
                            com.webservice.fiveonebookv3.GetPolicyByPnrTxtServiceImpl_1_0ServiceStub.GetPolicyByPnrTxtResponseE.MY_QNAME,
                            org.apache.axiom.om.OMAbstractFactory.getOMFactory());
        }
        catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }

    }

    private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory,
            com.webservice.fiveonebookv3.GetPolicyByPnrTxtServiceImpl_1_0ServiceStub.GetPolicyByPnrTxtE param,
            boolean optimizeContent) throws org.apache.axis2.AxisFault {

        try {

            org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
            emptyEnvelope
                    .getBody()
                    .addChild(
                            param.getOMElement(
                                    com.webservice.fiveonebookv3.GetPolicyByPnrTxtServiceImpl_1_0ServiceStub.GetPolicyByPnrTxtE.MY_QNAME,
                                    factory));
            return emptyEnvelope;
        }
        catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }

    }

    /* methods to provide back word compatibility */

    /**
    *  get the default envelope
    */
    private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory) {
        return factory.getDefaultEnvelope();
    }

    private java.lang.Object fromOM(org.apache.axiom.om.OMElement param, java.lang.Class type,
            java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault {

        try {

            if (com.webservice.fiveonebookv3.GetPolicyByPnrTxtServiceImpl_1_0ServiceStub.GetPolicyByPnrTxtE.class
                    .equals(type)) {

                return com.webservice.fiveonebookv3.GetPolicyByPnrTxtServiceImpl_1_0ServiceStub.GetPolicyByPnrTxtE.Factory
                        .parse(param.getXMLStreamReaderWithoutCaching());

            }

            if (com.webservice.fiveonebookv3.GetPolicyByPnrTxtServiceImpl_1_0ServiceStub.GetPolicyByPnrTxtResponseE.class
                    .equals(type)) {

                return com.webservice.fiveonebookv3.GetPolicyByPnrTxtServiceImpl_1_0ServiceStub.GetPolicyByPnrTxtResponseE.Factory
                        .parse(param.getXMLStreamReaderWithoutCaching());

            }

        }
        catch (java.lang.Exception e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }
        return null;
    }

}
