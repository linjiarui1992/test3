
package com.webservice.kkkk;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SpecialTreatement.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SpecialTreatement">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="None"/>
 *     &lt;enumeration value="RefundOffline"/>
 *     &lt;enumeration value="Finish"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SpecialTreatement")
@XmlEnum
public enum SpecialTreatement {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("RefundOffline")
    REFUND_OFFLINE("RefundOffline"),
    @XmlEnumValue("Finish")
    FINISH("Finish");
    private final String value;

    SpecialTreatement(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SpecialTreatement fromValue(String v) {
        for (SpecialTreatement c: SpecialTreatement.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
