
package com.webservice.kkkk;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for B2BETDZInteraface.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="B2BETDZInteraface">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Alipay"/>
 *     &lt;enumeration value="ChinaPnr"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "B2BETDZInteraface")
@XmlEnum
public enum B2BETDZInteraface {

    @XmlEnumValue("Alipay")
    ALIPAY("Alipay"),
    @XmlEnumValue("ChinaPnr")
    CHINA_PNR("ChinaPnr");
    private final String value;

    B2BETDZInteraface(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static B2BETDZInteraface fromValue(String v) {
        for (B2BETDZInteraface c: B2BETDZInteraface.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
