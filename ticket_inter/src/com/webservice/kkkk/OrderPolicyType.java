
package com.webservice.kkkk;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderPolicyType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OrderPolicyType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="General"/>
 *     &lt;enumeration value="Special"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "OrderPolicyType")
@XmlEnum
public enum OrderPolicyType {

    @XmlEnumValue("General")
    GENERAL("General"),
    @XmlEnumValue("Special")
    SPECIAL("Special");
    private final String value;

    OrderPolicyType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OrderPolicyType fromValue(String v) {
        for (OrderPolicyType c: OrderPolicyType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
