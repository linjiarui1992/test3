
package com.webservice.kkkk;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CoordinateType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CoordinateType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Urge"/>
 *     &lt;enumeration value="Refund"/>
 *     &lt;enumeration value="Scrap"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CoordinateType")
@XmlEnum
public enum CoordinateType {

    @XmlEnumValue("Urge")
    URGE("Urge"),
    @XmlEnumValue("Refund")
    REFUND("Refund"),
    @XmlEnumValue("Scrap")
    SCRAP("Scrap");
    private final String value;

    CoordinateType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CoordinateType fromValue(String v) {
        for (CoordinateType c: CoordinateType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
