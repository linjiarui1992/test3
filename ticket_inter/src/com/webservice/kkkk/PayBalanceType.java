
package com.webservice.kkkk;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PayBalanceType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PayBalanceType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Auto"/>
 *     &lt;enumeration value="Initiative"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PayBalanceType")
@XmlEnum
public enum PayBalanceType {

    @XmlEnumValue("Auto")
    AUTO("Auto"),
    @XmlEnumValue("Initiative")
    INITIATIVE("Initiative");
    private final String value;

    PayBalanceType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PayBalanceType fromValue(String v) {
        for (PayBalanceType c: PayBalanceType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
