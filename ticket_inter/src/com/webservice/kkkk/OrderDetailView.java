
package com.webservice.kkkk;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderDetailView complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderDetailView">
 *   &lt;complexContent>
 *     &lt;extension base="{http://tempuri.org/}OrderInfo">
 *       &lt;sequence>
 *         &lt;element name="ApplyForms" type="{http://tempuri.org/}ArrayOfApplyFormView" minOccurs="0"/>
 *         &lt;element name="CoordinationInfos" type="{http://tempuri.org/}ArrayOfCoordinationInfo" minOccurs="0"/>
 *         &lt;element name="AtionalInfos" type="{http://tempuri.org/}ArrayOfOrderAtionalView" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderDetailView", propOrder = {
    "applyForms",
    "coordinationInfos",
    "ationalInfos"
})
public class OrderDetailView
    extends OrderInfo
{

    @XmlElement(name = "ApplyForms")
    protected ArrayOfApplyFormView applyForms;
    @XmlElement(name = "CoordinationInfos")
    protected ArrayOfCoordinationInfo coordinationInfos;
    @XmlElement(name = "AtionalInfos")
    protected ArrayOfOrderAtionalView ationalInfos;

    /**
     * Gets the value of the applyForms property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfApplyFormView }
     *     
     */
    public ArrayOfApplyFormView getApplyForms() {
        return applyForms;
    }

    /**
     * Sets the value of the applyForms property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfApplyFormView }
     *     
     */
    public void setApplyForms(ArrayOfApplyFormView value) {
        this.applyForms = value;
    }

    /**
     * Gets the value of the coordinationInfos property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCoordinationInfo }
     *     
     */
    public ArrayOfCoordinationInfo getCoordinationInfos() {
        return coordinationInfos;
    }

    /**
     * Sets the value of the coordinationInfos property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCoordinationInfo }
     *     
     */
    public void setCoordinationInfos(ArrayOfCoordinationInfo value) {
        this.coordinationInfos = value;
    }

    /**
     * Gets the value of the ationalInfos property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfOrderAtionalView }
     *     
     */
    public ArrayOfOrderAtionalView getAtionalInfos() {
        return ationalInfos;
    }

    /**
     * Sets the value of the ationalInfos property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfOrderAtionalView }
     *     
     */
    public void setAtionalInfos(ArrayOfOrderAtionalView value) {
        this.ationalInfos = value;
    }

}
