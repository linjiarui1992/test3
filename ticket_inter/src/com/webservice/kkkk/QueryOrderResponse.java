
package com.webservice.kkkk;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="QueryOrderResult" type="{http://tempuri.org/}OrderDetailView" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "queryOrderResult"
})
@XmlRootElement(name = "QueryOrderResponse")
public class QueryOrderResponse {

    @XmlElement(name = "QueryOrderResult")
    protected OrderDetailView queryOrderResult;

    /**
     * Gets the value of the queryOrderResult property.
     * 
     * @return
     *     possible object is
     *     {@link OrderDetailView }
     *     
     */
    public OrderDetailView getQueryOrderResult() {
        return queryOrderResult;
    }

    /**
     * Sets the value of the queryOrderResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderDetailView }
     *     
     */
    public void setQueryOrderResult(OrderDetailView value) {
        this.queryOrderResult = value;
    }

}
