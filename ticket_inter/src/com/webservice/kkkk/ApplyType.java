
package com.webservice.kkkk;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ApplyType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ApplyType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Refund"/>
 *     &lt;enumeration value="Scrap"/>
 *     &lt;enumeration value="Postpone"/>
 *     &lt;enumeration value="Upgrade"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ApplyType")
@XmlEnum
public enum ApplyType {

    @XmlEnumValue("Refund")
    REFUND("Refund"),
    @XmlEnumValue("Scrap")
    SCRAP("Scrap"),
    @XmlEnumValue("Postpone")
    POSTPONE("Postpone"),
    @XmlEnumValue("Upgrade")
    UPGRADE("Upgrade");
    private final String value;

    ApplyType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ApplyType fromValue(String v) {
        for (ApplyType c: ApplyType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
