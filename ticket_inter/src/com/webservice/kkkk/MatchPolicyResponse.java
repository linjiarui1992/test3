
package com.webservice.kkkk;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MatchPolicyResult" type="{http://tempuri.org/}ArrayOfFlight" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "matchPolicyResult"
})
@XmlRootElement(name = "MatchPolicyResponse")
public class MatchPolicyResponse {

    @XmlElement(name = "MatchPolicyResult")
    protected ArrayOfFlight matchPolicyResult;

    /**
     * Gets the value of the matchPolicyResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfFlight }
     *     
     */
    public ArrayOfFlight getMatchPolicyResult() {
        return matchPolicyResult;
    }

    /**
     * Sets the value of the matchPolicyResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfFlight }
     *     
     */
    public void setMatchPolicyResult(ArrayOfFlight value) {
        this.matchPolicyResult = value;
    }

}
