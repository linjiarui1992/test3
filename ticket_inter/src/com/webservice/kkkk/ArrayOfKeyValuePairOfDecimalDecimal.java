
package com.webservice.kkkk;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfKeyValuePairOfDecimalDecimal complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfKeyValuePairOfDecimalDecimal">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="KeyValuePairOfDecimalDecimal" type="{http://tempuri.org/}KeyValuePairOfDecimalDecimal" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfKeyValuePairOfDecimalDecimal", propOrder = {
    "keyValuePairOfDecimalDecimal"
})
public class ArrayOfKeyValuePairOfDecimalDecimal {

    @XmlElement(name = "KeyValuePairOfDecimalDecimal")
    protected List<KeyValuePairOfDecimalDecimal> keyValuePairOfDecimalDecimal;

    /**
     * Gets the value of the keyValuePairOfDecimalDecimal property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the keyValuePairOfDecimalDecimal property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getKeyValuePairOfDecimalDecimal().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link KeyValuePairOfDecimalDecimal }
     * 
     * 
     */
    public List<KeyValuePairOfDecimalDecimal> getKeyValuePairOfDecimalDecimal() {
        if (keyValuePairOfDecimalDecimal == null) {
            keyValuePairOfDecimalDecimal = new ArrayList<KeyValuePairOfDecimalDecimal>();
        }
        return this.keyValuePairOfDecimalDecimal;
    }

}
