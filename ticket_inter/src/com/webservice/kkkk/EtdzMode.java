
package com.webservice.kkkk;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EtdzMode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EtdzMode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Manual"/>
 *     &lt;enumeration value="BSPAuto"/>
 *     &lt;enumeration value="B2BAuto"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "EtdzMode")
@XmlEnum
public enum EtdzMode {

    @XmlEnumValue("Manual")
    MANUAL("Manual"),
    @XmlEnumValue("BSPAuto")
    BSP_AUTO("BSPAuto"),
    @XmlEnumValue("B2BAuto")
    B_2_B_AUTO("B2BAuto");
    private final String value;

    EtdzMode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EtdzMode fromValue(String v) {
        for (EtdzMode c: EtdzMode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
