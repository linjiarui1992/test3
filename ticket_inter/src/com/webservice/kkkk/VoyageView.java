
package com.webservice.kkkk;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for VoyageView complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VoyageView">
 *   &lt;complexContent>
 *     &lt;extension base="{http://tempuri.org/}EntityOfDecimal">
 *       &lt;sequence>
 *         &lt;element name="SerialNo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DepartureCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DepartureCityName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DepartureAirportName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ArrivalCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ArrivalCityName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ArrivalAirportName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DepartureTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="ArrivalTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Airline" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AirlineName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Flight" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PlaneType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="YbPrice" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="ParValue" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Class" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsShare" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="EI" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClassType" type="{http://tempuri.org/}SeatType"/>
 *         &lt;element name="ClassRemark" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Discount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="AirportFee" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="IsVirtual" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="BAF" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="DepartureTerminal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ArrivalTerminal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VoyageView", propOrder = {
    "serialNo",
    "departureCity",
    "departureCityName",
    "departureAirportName",
    "arrivalCity",
    "arrivalCityName",
    "arrivalAirportName",
    "departureTime",
    "arrivalTime",
    "airline",
    "airlineName",
    "flight",
    "planeType",
    "ybPrice",
    "parValue",
    "clazz",
    "isShare",
    "ei",
    "classType",
    "classRemark",
    "discount",
    "airportFee",
    "isVirtual",
    "baf",
    "departureTerminal",
    "arrivalTerminal"
})
public class VoyageView
    extends EntityOfDecimal
{

    @XmlElement(name = "SerialNo")
    protected int serialNo;
    @XmlElement(name = "DepartureCity")
    protected String departureCity;
    @XmlElement(name = "DepartureCityName")
    protected String departureCityName;
    @XmlElement(name = "DepartureAirportName")
    protected String departureAirportName;
    @XmlElement(name = "ArrivalCity")
    protected String arrivalCity;
    @XmlElement(name = "ArrivalCityName")
    protected String arrivalCityName;
    @XmlElement(name = "ArrivalAirportName")
    protected String arrivalAirportName;
    @XmlElement(name = "DepartureTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar departureTime;
    @XmlElement(name = "ArrivalTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar arrivalTime;
    @XmlElement(name = "Airline")
    protected String airline;
    @XmlElement(name = "AirlineName")
    protected String airlineName;
    @XmlElement(name = "Flight")
    protected String flight;
    @XmlElement(name = "PlaneType")
    protected String planeType;
    @XmlElement(name = "YbPrice", required = true)
    protected BigDecimal ybPrice;
    @XmlElement(name = "ParValue", required = true)
    protected BigDecimal parValue;
    @XmlElement(name = "Class")
    protected String clazz;
    @XmlElement(name = "IsShare")
    protected boolean isShare;
    @XmlElement(name = "EI")
    protected String ei;
    @XmlElement(name = "ClassType", required = true)
    protected SeatType classType;
    @XmlElement(name = "ClassRemark")
    protected String classRemark;
    @XmlElement(name = "Discount", required = true)
    protected BigDecimal discount;
    @XmlElement(name = "AirportFee", required = true)
    protected BigDecimal airportFee;
    @XmlElement(name = "IsVirtual")
    protected boolean isVirtual;
    @XmlElement(name = "BAF", required = true)
    protected BigDecimal baf;
    @XmlElement(name = "DepartureTerminal")
    protected String departureTerminal;
    @XmlElement(name = "ArrivalTerminal")
    protected String arrivalTerminal;

    /**
     * Gets the value of the serialNo property.
     * 
     */
    public int getSerialNo() {
        return serialNo;
    }

    /**
     * Sets the value of the serialNo property.
     * 
     */
    public void setSerialNo(int value) {
        this.serialNo = value;
    }

    /**
     * Gets the value of the departureCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartureCity() {
        return departureCity;
    }

    /**
     * Sets the value of the departureCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartureCity(String value) {
        this.departureCity = value;
    }

    /**
     * Gets the value of the departureCityName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartureCityName() {
        return departureCityName;
    }

    /**
     * Sets the value of the departureCityName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartureCityName(String value) {
        this.departureCityName = value;
    }

    /**
     * Gets the value of the departureAirportName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartureAirportName() {
        return departureAirportName;
    }

    /**
     * Sets the value of the departureAirportName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartureAirportName(String value) {
        this.departureAirportName = value;
    }

    /**
     * Gets the value of the arrivalCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArrivalCity() {
        return arrivalCity;
    }

    /**
     * Sets the value of the arrivalCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArrivalCity(String value) {
        this.arrivalCity = value;
    }

    /**
     * Gets the value of the arrivalCityName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArrivalCityName() {
        return arrivalCityName;
    }

    /**
     * Sets the value of the arrivalCityName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArrivalCityName(String value) {
        this.arrivalCityName = value;
    }

    /**
     * Gets the value of the arrivalAirportName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArrivalAirportName() {
        return arrivalAirportName;
    }

    /**
     * Sets the value of the arrivalAirportName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArrivalAirportName(String value) {
        this.arrivalAirportName = value;
    }

    /**
     * Gets the value of the departureTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDepartureTime() {
        return departureTime;
    }

    /**
     * Sets the value of the departureTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDepartureTime(XMLGregorianCalendar value) {
        this.departureTime = value;
    }

    /**
     * Gets the value of the arrivalTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getArrivalTime() {
        return arrivalTime;
    }

    /**
     * Sets the value of the arrivalTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setArrivalTime(XMLGregorianCalendar value) {
        this.arrivalTime = value;
    }

    /**
     * Gets the value of the airline property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAirline() {
        return airline;
    }

    /**
     * Sets the value of the airline property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAirline(String value) {
        this.airline = value;
    }

    /**
     * Gets the value of the airlineName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAirlineName() {
        return airlineName;
    }

    /**
     * Sets the value of the airlineName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAirlineName(String value) {
        this.airlineName = value;
    }

    /**
     * Gets the value of the flight property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlight() {
        return flight;
    }

    /**
     * Sets the value of the flight property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlight(String value) {
        this.flight = value;
    }

    /**
     * Gets the value of the planeType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlaneType() {
        return planeType;
    }

    /**
     * Sets the value of the planeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlaneType(String value) {
        this.planeType = value;
    }

    /**
     * Gets the value of the ybPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getYbPrice() {
        return ybPrice;
    }

    /**
     * Sets the value of the ybPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setYbPrice(BigDecimal value) {
        this.ybPrice = value;
    }

    /**
     * Gets the value of the parValue property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getParValue() {
        return parValue;
    }

    /**
     * Sets the value of the parValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setParValue(BigDecimal value) {
        this.parValue = value;
    }

    /**
     * Gets the value of the clazz property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClazz() {
        return clazz;
    }

    /**
     * Sets the value of the clazz property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClazz(String value) {
        this.clazz = value;
    }

    /**
     * Gets the value of the isShare property.
     * 
     */
    public boolean isIsShare() {
        return isShare;
    }

    /**
     * Sets the value of the isShare property.
     * 
     */
    public void setIsShare(boolean value) {
        this.isShare = value;
    }

    /**
     * Gets the value of the ei property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEI() {
        return ei;
    }

    /**
     * Sets the value of the ei property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEI(String value) {
        this.ei = value;
    }

    /**
     * Gets the value of the classType property.
     * 
     * @return
     *     possible object is
     *     {@link SeatType }
     *     
     */
    public SeatType getClassType() {
        return classType;
    }

    /**
     * Sets the value of the classType property.
     * 
     * @param value
     *     allowed object is
     *     {@link SeatType }
     *     
     */
    public void setClassType(SeatType value) {
        this.classType = value;
    }

    /**
     * Gets the value of the classRemark property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassRemark() {
        return classRemark;
    }

    /**
     * Sets the value of the classRemark property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassRemark(String value) {
        this.classRemark = value;
    }

    /**
     * Gets the value of the discount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiscount() {
        return discount;
    }

    /**
     * Sets the value of the discount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiscount(BigDecimal value) {
        this.discount = value;
    }

    /**
     * Gets the value of the airportFee property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAirportFee() {
        return airportFee;
    }

    /**
     * Sets the value of the airportFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAirportFee(BigDecimal value) {
        this.airportFee = value;
    }

    /**
     * Gets the value of the isVirtual property.
     * 
     */
    public boolean isIsVirtual() {
        return isVirtual;
    }

    /**
     * Sets the value of the isVirtual property.
     * 
     */
    public void setIsVirtual(boolean value) {
        this.isVirtual = value;
    }

    /**
     * Gets the value of the baf property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBAF() {
        return baf;
    }

    /**
     * Sets the value of the baf property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBAF(BigDecimal value) {
        this.baf = value;
    }

    /**
     * Gets the value of the departureTerminal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartureTerminal() {
        return departureTerminal;
    }

    /**
     * Sets the value of the departureTerminal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartureTerminal(String value) {
        this.departureTerminal = value;
    }

    /**
     * Gets the value of the arrivalTerminal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArrivalTerminal() {
        return arrivalTerminal;
    }

    /**
     * Sets the value of the arrivalTerminal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArrivalTerminal(String value) {
        this.arrivalTerminal = value;
    }

}
