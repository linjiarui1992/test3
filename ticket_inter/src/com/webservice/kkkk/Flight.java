
package com.webservice.kkkk;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Flight complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Flight">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Airline" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AirNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DepartureCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DepartureCityPassengerTerminal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ArriveCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ArrivalCityPassengerTerminal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DepartureDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="DepartTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ArriveTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PlaneType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StopOver" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Food" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ETicket" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Seats" type="{http://tempuri.org/}ArrayOfSeat" minOccurs="0"/>
 *         &lt;element name="DepartureCityName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ArriveCityName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AirlineName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AirlineIcon" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DepartureAirport" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ArriveAirport" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AirportTax" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="BAF" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="ChildBAF" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="FullPrice" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="NeedDelete" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Mileage" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Flight", propOrder = {
    "airline",
    "airNo",
    "departureCity",
    "departureCityPassengerTerminal",
    "arriveCity",
    "arrivalCityPassengerTerminal",
    "departureDate",
    "departTime",
    "arriveTime",
    "planeType",
    "stopOver",
    "food",
    "eTicket",
    "seats",
    "departureCityName",
    "arriveCityName",
    "airlineName",
    "airlineIcon",
    "departureAirport",
    "arriveAirport",
    "airportTax",
    "baf",
    "childBAF",
    "fullPrice",
    "needDelete",
    "mileage"
})
public class Flight {

    @XmlElement(name = "Airline")
    protected String airline;
    @XmlElement(name = "AirNo")
    protected String airNo;
    @XmlElement(name = "DepartureCity")
    protected String departureCity;
    @XmlElement(name = "DepartureCityPassengerTerminal")
    protected String departureCityPassengerTerminal;
    @XmlElement(name = "ArriveCity")
    protected String arriveCity;
    @XmlElement(name = "ArrivalCityPassengerTerminal")
    protected String arrivalCityPassengerTerminal;
    @XmlElement(name = "DepartureDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar departureDate;
    @XmlElement(name = "DepartTime")
    protected String departTime;
    @XmlElement(name = "ArriveTime")
    protected String arriveTime;
    @XmlElement(name = "PlaneType")
    protected String planeType;
    @XmlElement(name = "StopOver")
    protected int stopOver;
    @XmlElement(name = "Food")
    protected String food;
    @XmlElement(name = "ETicket")
    protected boolean eTicket;
    @XmlElement(name = "Seats")
    protected ArrayOfSeat seats;
    @XmlElement(name = "DepartureCityName")
    protected String departureCityName;
    @XmlElement(name = "ArriveCityName")
    protected String arriveCityName;
    @XmlElement(name = "AirlineName")
    protected String airlineName;
    @XmlElement(name = "AirlineIcon")
    protected String airlineIcon;
    @XmlElement(name = "DepartureAirport")
    protected String departureAirport;
    @XmlElement(name = "ArriveAirport")
    protected String arriveAirport;
    @XmlElement(name = "AirportTax", required = true)
    protected BigDecimal airportTax;
    @XmlElement(name = "BAF", required = true)
    protected BigDecimal baf;
    @XmlElement(name = "ChildBAF", required = true)
    protected BigDecimal childBAF;
    @XmlElement(name = "FullPrice", required = true)
    protected BigDecimal fullPrice;
    @XmlElement(name = "NeedDelete")
    protected boolean needDelete;
    @XmlElement(name = "Mileage", required = true)
    protected BigDecimal mileage;

    /**
     * Gets the value of the airline property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAirline() {
        return airline;
    }

    /**
     * Sets the value of the airline property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAirline(String value) {
        this.airline = value;
    }

    /**
     * Gets the value of the airNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAirNo() {
        return airNo;
    }

    /**
     * Sets the value of the airNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAirNo(String value) {
        this.airNo = value;
    }

    /**
     * Gets the value of the departureCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartureCity() {
        return departureCity;
    }

    /**
     * Sets the value of the departureCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartureCity(String value) {
        this.departureCity = value;
    }

    /**
     * Gets the value of the departureCityPassengerTerminal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartureCityPassengerTerminal() {
        return departureCityPassengerTerminal;
    }

    /**
     * Sets the value of the departureCityPassengerTerminal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartureCityPassengerTerminal(String value) {
        this.departureCityPassengerTerminal = value;
    }

    /**
     * Gets the value of the arriveCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArriveCity() {
        return arriveCity;
    }

    /**
     * Sets the value of the arriveCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArriveCity(String value) {
        this.arriveCity = value;
    }

    /**
     * Gets the value of the arrivalCityPassengerTerminal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArrivalCityPassengerTerminal() {
        return arrivalCityPassengerTerminal;
    }

    /**
     * Sets the value of the arrivalCityPassengerTerminal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArrivalCityPassengerTerminal(String value) {
        this.arrivalCityPassengerTerminal = value;
    }

    /**
     * Gets the value of the departureDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDepartureDate() {
        return departureDate;
    }

    /**
     * Sets the value of the departureDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDepartureDate(XMLGregorianCalendar value) {
        this.departureDate = value;
    }

    /**
     * Gets the value of the departTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartTime() {
        return departTime;
    }

    /**
     * Sets the value of the departTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartTime(String value) {
        this.departTime = value;
    }

    /**
     * Gets the value of the arriveTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArriveTime() {
        return arriveTime;
    }

    /**
     * Sets the value of the arriveTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArriveTime(String value) {
        this.arriveTime = value;
    }

    /**
     * Gets the value of the planeType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlaneType() {
        return planeType;
    }

    /**
     * Sets the value of the planeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlaneType(String value) {
        this.planeType = value;
    }

    /**
     * Gets the value of the stopOver property.
     * 
     */
    public int getStopOver() {
        return stopOver;
    }

    /**
     * Sets the value of the stopOver property.
     * 
     */
    public void setStopOver(int value) {
        this.stopOver = value;
    }

    /**
     * Gets the value of the food property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFood() {
        return food;
    }

    /**
     * Sets the value of the food property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFood(String value) {
        this.food = value;
    }

    /**
     * Gets the value of the eTicket property.
     * 
     */
    public boolean isETicket() {
        return eTicket;
    }

    /**
     * Sets the value of the eTicket property.
     * 
     */
    public void setETicket(boolean value) {
        this.eTicket = value;
    }

    /**
     * Gets the value of the seats property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSeat }
     *     
     */
    public ArrayOfSeat getSeats() {
        return seats;
    }

    /**
     * Sets the value of the seats property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSeat }
     *     
     */
    public void setSeats(ArrayOfSeat value) {
        this.seats = value;
    }

    /**
     * Gets the value of the departureCityName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartureCityName() {
        return departureCityName;
    }

    /**
     * Sets the value of the departureCityName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartureCityName(String value) {
        this.departureCityName = value;
    }

    /**
     * Gets the value of the arriveCityName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArriveCityName() {
        return arriveCityName;
    }

    /**
     * Sets the value of the arriveCityName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArriveCityName(String value) {
        this.arriveCityName = value;
    }

    /**
     * Gets the value of the airlineName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAirlineName() {
        return airlineName;
    }

    /**
     * Sets the value of the airlineName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAirlineName(String value) {
        this.airlineName = value;
    }

    /**
     * Gets the value of the airlineIcon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAirlineIcon() {
        return airlineIcon;
    }

    /**
     * Sets the value of the airlineIcon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAirlineIcon(String value) {
        this.airlineIcon = value;
    }

    /**
     * Gets the value of the departureAirport property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartureAirport() {
        return departureAirport;
    }

    /**
     * Sets the value of the departureAirport property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartureAirport(String value) {
        this.departureAirport = value;
    }

    /**
     * Gets the value of the arriveAirport property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArriveAirport() {
        return arriveAirport;
    }

    /**
     * Sets the value of the arriveAirport property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArriveAirport(String value) {
        this.arriveAirport = value;
    }

    /**
     * Gets the value of the airportTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAirportTax() {
        return airportTax;
    }

    /**
     * Sets the value of the airportTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAirportTax(BigDecimal value) {
        this.airportTax = value;
    }

    /**
     * Gets the value of the baf property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBAF() {
        return baf;
    }

    /**
     * Sets the value of the baf property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBAF(BigDecimal value) {
        this.baf = value;
    }

    /**
     * Gets the value of the childBAF property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getChildBAF() {
        return childBAF;
    }

    /**
     * Sets the value of the childBAF property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setChildBAF(BigDecimal value) {
        this.childBAF = value;
    }

    /**
     * Gets the value of the fullPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFullPrice() {
        return fullPrice;
    }

    /**
     * Sets the value of the fullPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFullPrice(BigDecimal value) {
        this.fullPrice = value;
    }

    /**
     * Gets the value of the needDelete property.
     * 
     */
    public boolean isNeedDelete() {
        return needDelete;
    }

    /**
     * Sets the value of the needDelete property.
     * 
     */
    public void setNeedDelete(boolean value) {
        this.needDelete = value;
    }

    /**
     * Gets the value of the mileage property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMileage() {
        return mileage;
    }

    /**
     * Sets the value of the mileage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMileage(BigDecimal value) {
        this.mileage = value;
    }

}
