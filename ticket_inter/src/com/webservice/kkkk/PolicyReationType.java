
package com.webservice.kkkk;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PolicyReationType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PolicyReationType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Peer"/>
 *     &lt;enumeration value="Subordinate"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PolicyReationType")
@XmlEnum
public enum PolicyReationType {

    @XmlEnumValue("Peer")
    PEER("Peer"),
    @XmlEnumValue("Subordinate")
    SUBORDINATE("Subordinate");
    private final String value;

    PolicyReationType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PolicyReationType fromValue(String v) {
        for (PolicyReationType c: PolicyReationType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
