
package com.webservice.kkkk;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OrderStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Ordered"/>
 *     &lt;enumeration value="Paid"/>
 *     &lt;enumeration value="Paused"/>
 *     &lt;enumeration value="Finished"/>
 *     &lt;enumeration value="Canceled"/>
 *     &lt;enumeration value="Applied"/>
 *     &lt;enumeration value="Denied"/>
 *     &lt;enumeration value="Refunding"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "OrderStatus")
@XmlEnum
public enum OrderStatus {

    @XmlEnumValue("Ordered")
    ORDERED("Ordered"),
    @XmlEnumValue("Paid")
    PAID("Paid"),
    @XmlEnumValue("Paused")
    PAUSED("Paused"),
    @XmlEnumValue("Finished")
    FINISHED("Finished"),
    @XmlEnumValue("Canceled")
    CANCELED("Canceled"),
    @XmlEnumValue("Applied")
    APPLIED("Applied"),
    @XmlEnumValue("Denied")
    DENIED("Denied"),
    @XmlEnumValue("Refunding")
    REFUNDING("Refunding");
    private final String value;

    OrderStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OrderStatus fromValue(String v) {
        for (OrderStatus c: OrderStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
