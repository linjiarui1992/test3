
package com.webservice.kkkk;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PolicyExcludeAirline complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PolicyExcludeAirline">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Policyid" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Departurecity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Arrivalcity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolicyExcludeAirline", propOrder = {
    "policyid",
    "departurecity",
    "arrivalcity"
})
public class PolicyExcludeAirline {

    @XmlElement(name = "Policyid", required = true)
    protected BigDecimal policyid;
    @XmlElement(name = "Departurecity")
    protected String departurecity;
    @XmlElement(name = "Arrivalcity")
    protected String arrivalcity;

    /**
     * Gets the value of the policyid property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPolicyid() {
        return policyid;
    }

    /**
     * Sets the value of the policyid property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPolicyid(BigDecimal value) {
        this.policyid = value;
    }

    /**
     * Gets the value of the departurecity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeparturecity() {
        return departurecity;
    }

    /**
     * Sets the value of the departurecity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeparturecity(String value) {
        this.departurecity = value;
    }

    /**
     * Gets the value of the arrivalcity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArrivalcity() {
        return arrivalcity;
    }

    /**
     * Sets the value of the arrivalcity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArrivalcity(String value) {
        this.arrivalcity = value;
    }

}
