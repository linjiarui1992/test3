
package com.webservice.kkkk;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PassengerView complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PassengerView">
 *   &lt;complexContent>
 *     &lt;extension base="{http://tempuri.org/}EntityOfDecimal">
 *       &lt;sequence>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Type" type="{http://tempuri.org/}PassengerType"/>
 *         &lt;element name="CredentialsNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CredentialsType" type="{http://tempuri.org/}CertificationType"/>
 *         &lt;element name="Mobile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SerialNo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="InfantId" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="ParValue" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="AirportFee" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="BAF" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="GetTotalFare" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="IsBuyInsure" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Tickets" type="{http://tempuri.org/}ArrayOfTicketView" minOccurs="0"/>
 *         &lt;element name="KeyAccount" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="InsuranceInfo" type="{http://tempuri.org/}InsuranceInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PassengerView", propOrder = {
    "name",
    "type",
    "credentialsNo",
    "credentialsType",
    "mobile",
    "serialNo",
    "infantId",
    "parValue",
    "airportFee",
    "baf",
    "getTotalFare",
    "isBuyInsure",
    "tickets",
    "keyAccount",
    "insuranceInfo"
})
public class PassengerView
    extends EntityOfDecimal
{

    @XmlElement(name = "Name")
    protected String name;
    @XmlElement(name = "Type", required = true)
    protected PassengerType type;
    @XmlElement(name = "CredentialsNo")
    protected String credentialsNo;
    @XmlElement(name = "CredentialsType", required = true)
    protected CertificationType credentialsType;
    @XmlElement(name = "Mobile")
    protected String mobile;
    @XmlElement(name = "SerialNo")
    protected int serialNo;
    @XmlElement(name = "InfantId", required = true, nillable = true)
    protected BigDecimal infantId;
    @XmlElement(name = "ParValue", required = true)
    protected BigDecimal parValue;
    @XmlElement(name = "AirportFee", required = true)
    protected BigDecimal airportFee;
    @XmlElement(name = "BAF", required = true)
    protected BigDecimal baf;
    @XmlElement(name = "GetTotalFare", required = true)
    protected BigDecimal getTotalFare;
    @XmlElement(name = "IsBuyInsure")
    protected boolean isBuyInsure;
    @XmlElement(name = "Tickets")
    protected ArrayOfTicketView tickets;
    @XmlElement(name = "KeyAccount")
    protected boolean keyAccount;
    @XmlElement(name = "InsuranceInfo")
    protected InsuranceInfo insuranceInfo;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link PassengerType }
     *     
     */
    public PassengerType getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link PassengerType }
     *     
     */
    public void setType(PassengerType value) {
        this.type = value;
    }

    /**
     * Gets the value of the credentialsNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCredentialsNo() {
        return credentialsNo;
    }

    /**
     * Sets the value of the credentialsNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCredentialsNo(String value) {
        this.credentialsNo = value;
    }

    /**
     * Gets the value of the credentialsType property.
     * 
     * @return
     *     possible object is
     *     {@link CertificationType }
     *     
     */
    public CertificationType getCredentialsType() {
        return credentialsType;
    }

    /**
     * Sets the value of the credentialsType property.
     * 
     * @param value
     *     allowed object is
     *     {@link CertificationType }
     *     
     */
    public void setCredentialsType(CertificationType value) {
        this.credentialsType = value;
    }

    /**
     * Gets the value of the mobile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * Sets the value of the mobile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMobile(String value) {
        this.mobile = value;
    }

    /**
     * Gets the value of the serialNo property.
     * 
     */
    public int getSerialNo() {
        return serialNo;
    }

    /**
     * Sets the value of the serialNo property.
     * 
     */
    public void setSerialNo(int value) {
        this.serialNo = value;
    }

    /**
     * Gets the value of the infantId property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getInfantId() {
        return infantId;
    }

    /**
     * Sets the value of the infantId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setInfantId(BigDecimal value) {
        this.infantId = value;
    }

    /**
     * Gets the value of the parValue property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getParValue() {
        return parValue;
    }

    /**
     * Sets the value of the parValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setParValue(BigDecimal value) {
        this.parValue = value;
    }

    /**
     * Gets the value of the airportFee property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAirportFee() {
        return airportFee;
    }

    /**
     * Sets the value of the airportFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAirportFee(BigDecimal value) {
        this.airportFee = value;
    }

    /**
     * Gets the value of the baf property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBAF() {
        return baf;
    }

    /**
     * Sets the value of the baf property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBAF(BigDecimal value) {
        this.baf = value;
    }

    /**
     * Gets the value of the getTotalFare property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getGetTotalFare() {
        return getTotalFare;
    }

    /**
     * Sets the value of the getTotalFare property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setGetTotalFare(BigDecimal value) {
        this.getTotalFare = value;
    }

    /**
     * Gets the value of the isBuyInsure property.
     * 
     */
    public boolean isIsBuyInsure() {
        return isBuyInsure;
    }

    /**
     * Sets the value of the isBuyInsure property.
     * 
     */
    public void setIsBuyInsure(boolean value) {
        this.isBuyInsure = value;
    }

    /**
     * Gets the value of the tickets property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTicketView }
     *     
     */
    public ArrayOfTicketView getTickets() {
        return tickets;
    }

    /**
     * Sets the value of the tickets property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTicketView }
     *     
     */
    public void setTickets(ArrayOfTicketView value) {
        this.tickets = value;
    }

    /**
     * Gets the value of the keyAccount property.
     * 
     */
    public boolean isKeyAccount() {
        return keyAccount;
    }

    /**
     * Sets the value of the keyAccount property.
     * 
     */
    public void setKeyAccount(boolean value) {
        this.keyAccount = value;
    }

    /**
     * Gets the value of the insuranceInfo property.
     * 
     * @return
     *     possible object is
     *     {@link InsuranceInfo }
     *     
     */
    public InsuranceInfo getInsuranceInfo() {
        return insuranceInfo;
    }

    /**
     * Sets the value of the insuranceInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link InsuranceInfo }
     *     
     */
    public void setInsuranceInfo(InsuranceInfo value) {
        this.insuranceInfo = value;
    }

}
