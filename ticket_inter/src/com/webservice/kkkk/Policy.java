
package com.webservice.kkkk;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Policy complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Policy">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PolicyID" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="SupplierID" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="DepartureCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ArrivalCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RangeType" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="OriginalRebate" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="OriginalVIP" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="SupplierRebate" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="FinalRebateTotal" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="CommisionIncomeTotal" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="PolicyPriceForClass" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="PlatRebateForPolicy" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="OfficeNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReationType" type="{http://tempuri.org/}PolicyReationType"/>
 *         &lt;element name="etdzType" type="{http://tempuri.org/}EtdzMode"/>
 *         &lt;element name="IsOutOfWork" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="StakeHolderRebateReport" type="{http://tempuri.org/}ArrayOfKeyValuePairOfDecimalDecimal" minOccurs="0"/>
 *         &lt;element name="PolicyRemark" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AirlineCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Classes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FFFlightType" type="{http://tempuri.org/}RestrictFlightType"/>
 *         &lt;element name="FFFlightNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SFFlightType" type="{http://tempuri.org/}RestrictFlightType"/>
 *         &lt;element name="SFFlightNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FFSchedule" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="SFSchedule" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="FFFlightDateRangeLower" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="FFFlightDateRangeUpper" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="SFFlightDateRangeLower" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="SFFlightDateRangeUpper" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="ETDZDateRangeLower" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="ETDZDateRangeUpper" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="SupplierInfo" type="{http://tempuri.org/}PolicySupplierInfo" minOccurs="0"/>
 *         &lt;element name="TicketType" type="{http://tempuri.org/}TicketType"/>
 *         &lt;element name="OrderPolicyType" type="{http://tempuri.org/}OrderPolicyType"/>
 *         &lt;element name="IsHighRebate" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="SupplierShortName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SortPriorityInt" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="SortPriorityIntBySetting" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PolicyExcludeAirline" type="{http://tempuri.org/}ArrayOfPolicyExcludeAirline" minOccurs="0"/>
 *         &lt;element name="B2BEtdzInterfaceType" type="{http://tempuri.org/}B2BETDZInteraface"/>
 *         &lt;element name="ForKeyAccount" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="RoundPreferential" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ProfitMode" type="{http://tempuri.org/}ProfitMode"/>
 *         &lt;element name="LazyRebate" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="PlatDeductionPolicyForPolicy" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="PlatAddPolicyForPolicy" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="PlatDeductionForVipCoordination" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="PlatDeductionForPolicyCoordination" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="IsSubClass" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Policy", propOrder = {
    "policyID",
    "supplierID",
    "departureCity",
    "arrivalCity",
    "rangeType",
    "originalRebate",
    "originalVIP",
    "supplierRebate",
    "finalRebateTotal",
    "commisionIncomeTotal",
    "policyPriceForClass",
    "platRebateForPolicy",
    "officeNo",
    "reationType",
    "etdzType",
    "isOutOfWork",
    "stakeHolderRebateReport",
    "policyRemark",
    "airlineCode",
    "classes",
    "ffFlightType",
    "ffFlightNo",
    "sfFlightType",
    "sfFlightNo",
    "ffSchedule",
    "sfSchedule",
    "ffFlightDateRangeLower",
    "ffFlightDateRangeUpper",
    "sfFlightDateRangeLower",
    "sfFlightDateRangeUpper",
    "etdzDateRangeLower",
    "etdzDateRangeUpper",
    "supplierInfo",
    "ticketType",
    "orderPolicyType",
    "isHighRebate",
    "supplierShortName",
    "sortPriorityInt",
    "sortPriorityIntBySetting",
    "policyExcludeAirline",
    "b2BEtdzInterfaceType",
    "forKeyAccount",
    "roundPreferential",
    "profitMode",
    "lazyRebate",
    "platDeductionPolicyForPolicy",
    "platAddPolicyForPolicy",
    "platDeductionForVipCoordination",
    "platDeductionForPolicyCoordination",
    "isSubClass"
})
@XmlSeeAlso({
    SpecialPolicy.class
})
public abstract class Policy {

    @XmlElement(name = "PolicyID", required = true)
    protected BigDecimal policyID;
    @XmlElement(name = "SupplierID", required = true)
    protected BigDecimal supplierID;
    @XmlElement(name = "DepartureCity")
    protected String departureCity;
    @XmlElement(name = "ArrivalCity")
    protected String arrivalCity;
    @XmlElement(name = "RangeType")
    protected int rangeType;
    @XmlElement(name = "OriginalRebate", required = true)
    protected BigDecimal originalRebate;
    @XmlElement(name = "OriginalVIP", required = true)
    protected BigDecimal originalVIP;
    @XmlElement(name = "SupplierRebate", required = true)
    protected BigDecimal supplierRebate;
    @XmlElement(name = "FinalRebateTotal", required = true)
    protected BigDecimal finalRebateTotal;
    @XmlElement(name = "CommisionIncomeTotal", required = true)
    protected BigDecimal commisionIncomeTotal;
    @XmlElement(name = "PolicyPriceForClass", required = true)
    protected BigDecimal policyPriceForClass;
    @XmlElement(name = "PlatRebateForPolicy", required = true)
    protected BigDecimal platRebateForPolicy;
    @XmlElement(name = "OfficeNo")
    protected String officeNo;
    @XmlElement(name = "ReationType", required = true)
    protected PolicyReationType reationType;
    @XmlElement(required = true)
    protected EtdzMode etdzType;
    @XmlElement(name = "IsOutOfWork")
    protected boolean isOutOfWork;
    @XmlElement(name = "StakeHolderRebateReport")
    protected ArrayOfKeyValuePairOfDecimalDecimal stakeHolderRebateReport;
    @XmlElement(name = "PolicyRemark")
    protected String policyRemark;
    @XmlElement(name = "AirlineCode")
    protected String airlineCode;
    @XmlElement(name = "Classes")
    protected String classes;
    @XmlElement(name = "FFFlightType", required = true)
    protected RestrictFlightType ffFlightType;
    @XmlElement(name = "FFFlightNo")
    protected String ffFlightNo;
    @XmlElement(name = "SFFlightType", required = true)
    protected RestrictFlightType sfFlightType;
    @XmlElement(name = "SFFlightNo")
    protected String sfFlightNo;
    @XmlElement(name = "FFSchedule", required = true, type = Integer.class, nillable = true)
    protected Integer ffSchedule;
    @XmlElement(name = "SFSchedule", required = true, type = Integer.class, nillable = true)
    protected Integer sfSchedule;
    @XmlElement(name = "FFFlightDateRangeLower", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar ffFlightDateRangeLower;
    @XmlElement(name = "FFFlightDateRangeUpper", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar ffFlightDateRangeUpper;
    @XmlElement(name = "SFFlightDateRangeLower", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar sfFlightDateRangeLower;
    @XmlElement(name = "SFFlightDateRangeUpper", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar sfFlightDateRangeUpper;
    @XmlElement(name = "ETDZDateRangeLower", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar etdzDateRangeLower;
    @XmlElement(name = "ETDZDateRangeUpper", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar etdzDateRangeUpper;
    @XmlElement(name = "SupplierInfo")
    protected PolicySupplierInfo supplierInfo;
    @XmlElement(name = "TicketType", required = true)
    protected TicketType ticketType;
    @XmlElement(name = "OrderPolicyType", required = true)
    protected OrderPolicyType orderPolicyType;
    @XmlElement(name = "IsHighRebate")
    protected boolean isHighRebate;
    @XmlElement(name = "SupplierShortName")
    protected String supplierShortName;
    @XmlElement(name = "SortPriorityInt")
    protected int sortPriorityInt;
    @XmlElement(name = "SortPriorityIntBySetting")
    protected int sortPriorityIntBySetting;
    @XmlElement(name = "PolicyExcludeAirline")
    protected ArrayOfPolicyExcludeAirline policyExcludeAirline;
    @XmlElement(name = "B2BEtdzInterfaceType", required = true, nillable = true)
    protected B2BETDZInteraface b2BEtdzInterfaceType;
    @XmlElement(name = "ForKeyAccount")
    protected boolean forKeyAccount;
    @XmlElement(name = "RoundPreferential")
    protected boolean roundPreferential;
    @XmlElement(name = "ProfitMode", required = true)
    protected ProfitMode profitMode;
    @XmlElement(name = "LazyRebate", required = true)
    protected BigDecimal lazyRebate;
    @XmlElement(name = "PlatDeductionPolicyForPolicy", required = true)
    protected BigDecimal platDeductionPolicyForPolicy;
    @XmlElement(name = "PlatAddPolicyForPolicy", required = true)
    protected BigDecimal platAddPolicyForPolicy;
    @XmlElement(name = "PlatDeductionForVipCoordination", required = true)
    protected BigDecimal platDeductionForVipCoordination;
    @XmlElement(name = "PlatDeductionForPolicyCoordination", required = true)
    protected BigDecimal platDeductionForPolicyCoordination;
    @XmlElement(name = "IsSubClass")
    protected boolean isSubClass;

    /**
     * Gets the value of the policyID property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPolicyID() {
        return policyID;
    }

    /**
     * Sets the value of the policyID property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPolicyID(BigDecimal value) {
        this.policyID = value;
    }

    /**
     * Gets the value of the supplierID property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSupplierID() {
        return supplierID;
    }

    /**
     * Sets the value of the supplierID property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSupplierID(BigDecimal value) {
        this.supplierID = value;
    }

    /**
     * Gets the value of the departureCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartureCity() {
        return departureCity;
    }

    /**
     * Sets the value of the departureCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartureCity(String value) {
        this.departureCity = value;
    }

    /**
     * Gets the value of the arrivalCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArrivalCity() {
        return arrivalCity;
    }

    /**
     * Sets the value of the arrivalCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArrivalCity(String value) {
        this.arrivalCity = value;
    }

    /**
     * Gets the value of the rangeType property.
     * 
     */
    public int getRangeType() {
        return rangeType;
    }

    /**
     * Sets the value of the rangeType property.
     * 
     */
    public void setRangeType(int value) {
        this.rangeType = value;
    }

    /**
     * Gets the value of the originalRebate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOriginalRebate() {
        return originalRebate;
    }

    /**
     * Sets the value of the originalRebate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOriginalRebate(BigDecimal value) {
        this.originalRebate = value;
    }

    /**
     * Gets the value of the originalVIP property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOriginalVIP() {
        return originalVIP;
    }

    /**
     * Sets the value of the originalVIP property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOriginalVIP(BigDecimal value) {
        this.originalVIP = value;
    }

    /**
     * Gets the value of the supplierRebate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSupplierRebate() {
        return supplierRebate;
    }

    /**
     * Sets the value of the supplierRebate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSupplierRebate(BigDecimal value) {
        this.supplierRebate = value;
    }

    /**
     * Gets the value of the finalRebateTotal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFinalRebateTotal() {
        return finalRebateTotal;
    }

    /**
     * Sets the value of the finalRebateTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFinalRebateTotal(BigDecimal value) {
        this.finalRebateTotal = value;
    }

    /**
     * Gets the value of the commisionIncomeTotal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCommisionIncomeTotal() {
        return commisionIncomeTotal;
    }

    /**
     * Sets the value of the commisionIncomeTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCommisionIncomeTotal(BigDecimal value) {
        this.commisionIncomeTotal = value;
    }

    /**
     * Gets the value of the policyPriceForClass property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPolicyPriceForClass() {
        return policyPriceForClass;
    }

    /**
     * Sets the value of the policyPriceForClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPolicyPriceForClass(BigDecimal value) {
        this.policyPriceForClass = value;
    }

    /**
     * Gets the value of the platRebateForPolicy property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPlatRebateForPolicy() {
        return platRebateForPolicy;
    }

    /**
     * Sets the value of the platRebateForPolicy property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPlatRebateForPolicy(BigDecimal value) {
        this.platRebateForPolicy = value;
    }

    /**
     * Gets the value of the officeNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfficeNo() {
        return officeNo;
    }

    /**
     * Sets the value of the officeNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfficeNo(String value) {
        this.officeNo = value;
    }

    /**
     * Gets the value of the reationType property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyReationType }
     *     
     */
    public PolicyReationType getReationType() {
        return reationType;
    }

    /**
     * Sets the value of the reationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyReationType }
     *     
     */
    public void setReationType(PolicyReationType value) {
        this.reationType = value;
    }

    /**
     * Gets the value of the etdzType property.
     * 
     * @return
     *     possible object is
     *     {@link EtdzMode }
     *     
     */
    public EtdzMode getEtdzType() {
        return etdzType;
    }

    /**
     * Sets the value of the etdzType property.
     * 
     * @param value
     *     allowed object is
     *     {@link EtdzMode }
     *     
     */
    public void setEtdzType(EtdzMode value) {
        this.etdzType = value;
    }

    /**
     * Gets the value of the isOutOfWork property.
     * 
     */
    public boolean isIsOutOfWork() {
        return isOutOfWork;
    }

    /**
     * Sets the value of the isOutOfWork property.
     * 
     */
    public void setIsOutOfWork(boolean value) {
        this.isOutOfWork = value;
    }

    /**
     * Gets the value of the stakeHolderRebateReport property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfKeyValuePairOfDecimalDecimal }
     *     
     */
    public ArrayOfKeyValuePairOfDecimalDecimal getStakeHolderRebateReport() {
        return stakeHolderRebateReport;
    }

    /**
     * Sets the value of the stakeHolderRebateReport property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfKeyValuePairOfDecimalDecimal }
     *     
     */
    public void setStakeHolderRebateReport(ArrayOfKeyValuePairOfDecimalDecimal value) {
        this.stakeHolderRebateReport = value;
    }

    /**
     * Gets the value of the policyRemark property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyRemark() {
        return policyRemark;
    }

    /**
     * Sets the value of the policyRemark property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyRemark(String value) {
        this.policyRemark = value;
    }

    /**
     * Gets the value of the airlineCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAirlineCode() {
        return airlineCode;
    }

    /**
     * Sets the value of the airlineCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAirlineCode(String value) {
        this.airlineCode = value;
    }

    /**
     * Gets the value of the classes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClasses() {
        return classes;
    }

    /**
     * Sets the value of the classes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClasses(String value) {
        this.classes = value;
    }

    /**
     * Gets the value of the ffFlightType property.
     * 
     * @return
     *     possible object is
     *     {@link RestrictFlightType }
     *     
     */
    public RestrictFlightType getFFFlightType() {
        return ffFlightType;
    }

    /**
     * Sets the value of the ffFlightType property.
     * 
     * @param value
     *     allowed object is
     *     {@link RestrictFlightType }
     *     
     */
    public void setFFFlightType(RestrictFlightType value) {
        this.ffFlightType = value;
    }

    /**
     * Gets the value of the ffFlightNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFFFlightNo() {
        return ffFlightNo;
    }

    /**
     * Sets the value of the ffFlightNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFFFlightNo(String value) {
        this.ffFlightNo = value;
    }

    /**
     * Gets the value of the sfFlightType property.
     * 
     * @return
     *     possible object is
     *     {@link RestrictFlightType }
     *     
     */
    public RestrictFlightType getSFFlightType() {
        return sfFlightType;
    }

    /**
     * Sets the value of the sfFlightType property.
     * 
     * @param value
     *     allowed object is
     *     {@link RestrictFlightType }
     *     
     */
    public void setSFFlightType(RestrictFlightType value) {
        this.sfFlightType = value;
    }

    /**
     * Gets the value of the sfFlightNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSFFlightNo() {
        return sfFlightNo;
    }

    /**
     * Sets the value of the sfFlightNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSFFlightNo(String value) {
        this.sfFlightNo = value;
    }

    /**
     * Gets the value of the ffSchedule property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFFSchedule() {
        return ffSchedule;
    }

    /**
     * Sets the value of the ffSchedule property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFFSchedule(Integer value) {
        this.ffSchedule = value;
    }

    /**
     * Gets the value of the sfSchedule property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSFSchedule() {
        return sfSchedule;
    }

    /**
     * Sets the value of the sfSchedule property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSFSchedule(Integer value) {
        this.sfSchedule = value;
    }

    /**
     * Gets the value of the ffFlightDateRangeLower property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFFFlightDateRangeLower() {
        return ffFlightDateRangeLower;
    }

    /**
     * Sets the value of the ffFlightDateRangeLower property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFFFlightDateRangeLower(XMLGregorianCalendar value) {
        this.ffFlightDateRangeLower = value;
    }

    /**
     * Gets the value of the ffFlightDateRangeUpper property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFFFlightDateRangeUpper() {
        return ffFlightDateRangeUpper;
    }

    /**
     * Sets the value of the ffFlightDateRangeUpper property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFFFlightDateRangeUpper(XMLGregorianCalendar value) {
        this.ffFlightDateRangeUpper = value;
    }

    /**
     * Gets the value of the sfFlightDateRangeLower property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSFFlightDateRangeLower() {
        return sfFlightDateRangeLower;
    }

    /**
     * Sets the value of the sfFlightDateRangeLower property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSFFlightDateRangeLower(XMLGregorianCalendar value) {
        this.sfFlightDateRangeLower = value;
    }

    /**
     * Gets the value of the sfFlightDateRangeUpper property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSFFlightDateRangeUpper() {
        return sfFlightDateRangeUpper;
    }

    /**
     * Sets the value of the sfFlightDateRangeUpper property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSFFlightDateRangeUpper(XMLGregorianCalendar value) {
        this.sfFlightDateRangeUpper = value;
    }

    /**
     * Gets the value of the etdzDateRangeLower property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getETDZDateRangeLower() {
        return etdzDateRangeLower;
    }

    /**
     * Sets the value of the etdzDateRangeLower property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setETDZDateRangeLower(XMLGregorianCalendar value) {
        this.etdzDateRangeLower = value;
    }

    /**
     * Gets the value of the etdzDateRangeUpper property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getETDZDateRangeUpper() {
        return etdzDateRangeUpper;
    }

    /**
     * Sets the value of the etdzDateRangeUpper property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setETDZDateRangeUpper(XMLGregorianCalendar value) {
        this.etdzDateRangeUpper = value;
    }

    /**
     * Gets the value of the supplierInfo property.
     * 
     * @return
     *     possible object is
     *     {@link PolicySupplierInfo }
     *     
     */
    public PolicySupplierInfo getSupplierInfo() {
        return supplierInfo;
    }

    /**
     * Sets the value of the supplierInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicySupplierInfo }
     *     
     */
    public void setSupplierInfo(PolicySupplierInfo value) {
        this.supplierInfo = value;
    }

    /**
     * Gets the value of the ticketType property.
     * 
     * @return
     *     possible object is
     *     {@link TicketType }
     *     
     */
    public TicketType getTicketType() {
        return ticketType;
    }

    /**
     * Sets the value of the ticketType property.
     * 
     * @param value
     *     allowed object is
     *     {@link TicketType }
     *     
     */
    public void setTicketType(TicketType value) {
        this.ticketType = value;
    }

    /**
     * Gets the value of the orderPolicyType property.
     * 
     * @return
     *     possible object is
     *     {@link OrderPolicyType }
     *     
     */
    public OrderPolicyType getOrderPolicyType() {
        return orderPolicyType;
    }

    /**
     * Sets the value of the orderPolicyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderPolicyType }
     *     
     */
    public void setOrderPolicyType(OrderPolicyType value) {
        this.orderPolicyType = value;
    }

    /**
     * Gets the value of the isHighRebate property.
     * 
     */
    public boolean isIsHighRebate() {
        return isHighRebate;
    }

    /**
     * Sets the value of the isHighRebate property.
     * 
     */
    public void setIsHighRebate(boolean value) {
        this.isHighRebate = value;
    }

    /**
     * Gets the value of the supplierShortName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupplierShortName() {
        return supplierShortName;
    }

    /**
     * Sets the value of the supplierShortName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupplierShortName(String value) {
        this.supplierShortName = value;
    }

    /**
     * Gets the value of the sortPriorityInt property.
     * 
     */
    public int getSortPriorityInt() {
        return sortPriorityInt;
    }

    /**
     * Sets the value of the sortPriorityInt property.
     * 
     */
    public void setSortPriorityInt(int value) {
        this.sortPriorityInt = value;
    }

    /**
     * Gets the value of the sortPriorityIntBySetting property.
     * 
     */
    public int getSortPriorityIntBySetting() {
        return sortPriorityIntBySetting;
    }

    /**
     * Sets the value of the sortPriorityIntBySetting property.
     * 
     */
    public void setSortPriorityIntBySetting(int value) {
        this.sortPriorityIntBySetting = value;
    }

    /**
     * Gets the value of the policyExcludeAirline property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfPolicyExcludeAirline }
     *     
     */
    public ArrayOfPolicyExcludeAirline getPolicyExcludeAirline() {
        return policyExcludeAirline;
    }

    /**
     * Sets the value of the policyExcludeAirline property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfPolicyExcludeAirline }
     *     
     */
    public void setPolicyExcludeAirline(ArrayOfPolicyExcludeAirline value) {
        this.policyExcludeAirline = value;
    }

    /**
     * Gets the value of the b2BEtdzInterfaceType property.
     * 
     * @return
     *     possible object is
     *     {@link B2BETDZInteraface }
     *     
     */
    public B2BETDZInteraface getB2BEtdzInterfaceType() {
        return b2BEtdzInterfaceType;
    }

    /**
     * Sets the value of the b2BEtdzInterfaceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link B2BETDZInteraface }
     *     
     */
    public void setB2BEtdzInterfaceType(B2BETDZInteraface value) {
        this.b2BEtdzInterfaceType = value;
    }

    /**
     * Gets the value of the forKeyAccount property.
     * 
     */
    public boolean isForKeyAccount() {
        return forKeyAccount;
    }

    /**
     * Sets the value of the forKeyAccount property.
     * 
     */
    public void setForKeyAccount(boolean value) {
        this.forKeyAccount = value;
    }

    /**
     * Gets the value of the roundPreferential property.
     * 
     */
    public boolean isRoundPreferential() {
        return roundPreferential;
    }

    /**
     * Sets the value of the roundPreferential property.
     * 
     */
    public void setRoundPreferential(boolean value) {
        this.roundPreferential = value;
    }

    /**
     * Gets the value of the profitMode property.
     * 
     * @return
     *     possible object is
     *     {@link ProfitMode }
     *     
     */
    public ProfitMode getProfitMode() {
        return profitMode;
    }

    /**
     * Sets the value of the profitMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProfitMode }
     *     
     */
    public void setProfitMode(ProfitMode value) {
        this.profitMode = value;
    }

    /**
     * Gets the value of the lazyRebate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLazyRebate() {
        return lazyRebate;
    }

    /**
     * Sets the value of the lazyRebate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLazyRebate(BigDecimal value) {
        this.lazyRebate = value;
    }

    /**
     * Gets the value of the platDeductionPolicyForPolicy property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPlatDeductionPolicyForPolicy() {
        return platDeductionPolicyForPolicy;
    }

    /**
     * Sets the value of the platDeductionPolicyForPolicy property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPlatDeductionPolicyForPolicy(BigDecimal value) {
        this.platDeductionPolicyForPolicy = value;
    }

    /**
     * Gets the value of the platAddPolicyForPolicy property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPlatAddPolicyForPolicy() {
        return platAddPolicyForPolicy;
    }

    /**
     * Sets the value of the platAddPolicyForPolicy property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPlatAddPolicyForPolicy(BigDecimal value) {
        this.platAddPolicyForPolicy = value;
    }

    /**
     * Gets the value of the platDeductionForVipCoordination property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPlatDeductionForVipCoordination() {
        return platDeductionForVipCoordination;
    }

    /**
     * Sets the value of the platDeductionForVipCoordination property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPlatDeductionForVipCoordination(BigDecimal value) {
        this.platDeductionForVipCoordination = value;
    }

    /**
     * Gets the value of the platDeductionForPolicyCoordination property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPlatDeductionForPolicyCoordination() {
        return platDeductionForPolicyCoordination;
    }

    /**
     * Sets the value of the platDeductionForPolicyCoordination property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPlatDeductionForPolicyCoordination(BigDecimal value) {
        this.platDeductionForPolicyCoordination = value;
    }

    /**
     * Gets the value of the isSubClass property.
     * 
     */
    public boolean isIsSubClass() {
        return isSubClass;
    }

    /**
     * Sets the value of the isSubClass property.
     * 
     */
    public void setIsSubClass(boolean value) {
        this.isSubClass = value;
    }

}
