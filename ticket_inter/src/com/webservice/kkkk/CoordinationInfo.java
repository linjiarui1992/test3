
package com.webservice.kkkk;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for CoordinationInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CoordinationInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CoordinateType" type="{http://tempuri.org/}CoordinateType"/>
 *         &lt;element name="ContactType" type="{http://tempuri.org/}ContactType"/>
 *         &lt;element name="Content" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Result" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Coordinator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CoordinatorName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CoordinateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="IsPlatCoordinate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CoordinationInfo", propOrder = {
    "coordinateType",
    "contactType",
    "content",
    "result",
    "coordinator",
    "coordinatorName",
    "coordinateTime",
    "isPlatCoordinate"
})
public class CoordinationInfo {

    @XmlElement(name = "CoordinateType", required = true)
    protected CoordinateType coordinateType;
    @XmlElement(name = "ContactType", required = true)
    protected ContactType contactType;
    @XmlElement(name = "Content")
    protected String content;
    @XmlElement(name = "Result")
    protected String result;
    @XmlElement(name = "Coordinator")
    protected String coordinator;
    @XmlElement(name = "CoordinatorName")
    protected String coordinatorName;
    @XmlElement(name = "CoordinateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar coordinateTime;
    @XmlElement(name = "IsPlatCoordinate")
    protected String isPlatCoordinate;

    /**
     * Gets the value of the coordinateType property.
     * 
     * @return
     *     possible object is
     *     {@link CoordinateType }
     *     
     */
    public CoordinateType getCoordinateType() {
        return coordinateType;
    }

    /**
     * Sets the value of the coordinateType property.
     * 
     * @param value
     *     allowed object is
     *     {@link CoordinateType }
     *     
     */
    public void setCoordinateType(CoordinateType value) {
        this.coordinateType = value;
    }

    /**
     * Gets the value of the contactType property.
     * 
     * @return
     *     possible object is
     *     {@link ContactType }
     *     
     */
    public ContactType getContactType() {
        return contactType;
    }

    /**
     * Sets the value of the contactType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactType }
     *     
     */
    public void setContactType(ContactType value) {
        this.contactType = value;
    }

    /**
     * Gets the value of the content property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContent() {
        return content;
    }

    /**
     * Sets the value of the content property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContent(String value) {
        this.content = value;
    }

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResult(String value) {
        this.result = value;
    }

    /**
     * Gets the value of the coordinator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoordinator() {
        return coordinator;
    }

    /**
     * Sets the value of the coordinator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoordinator(String value) {
        this.coordinator = value;
    }

    /**
     * Gets the value of the coordinatorName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoordinatorName() {
        return coordinatorName;
    }

    /**
     * Sets the value of the coordinatorName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoordinatorName(String value) {
        this.coordinatorName = value;
    }

    /**
     * Gets the value of the coordinateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCoordinateTime() {
        return coordinateTime;
    }

    /**
     * Sets the value of the coordinateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCoordinateTime(XMLGregorianCalendar value) {
        this.coordinateTime = value;
    }

    /**
     * Gets the value of the isPlatCoordinate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPlatCoordinate() {
        return isPlatCoordinate;
    }

    /**
     * Sets the value of the isPlatCoordinate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPlatCoordinate(String value) {
        this.isPlatCoordinate = value;
    }

}
