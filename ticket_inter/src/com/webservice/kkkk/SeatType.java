
package com.webservice.kkkk;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SeatType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SeatType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="General"/>
 *     &lt;enumeration value="VIP"/>
 *     &lt;enumeration value="Special"/>
 *     &lt;enumeration value="SpecialRound"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SeatType")
@XmlEnum
public enum SeatType {

    @XmlEnumValue("General")
    GENERAL("General"),
    VIP("VIP"),
    @XmlEnumValue("Special")
    SPECIAL("Special"),
    @XmlEnumValue("SpecialRound")
    SPECIAL_ROUND("SpecialRound");
    private final String value;

    SeatType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SeatType fromValue(String v) {
        for (SeatType c: SeatType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
