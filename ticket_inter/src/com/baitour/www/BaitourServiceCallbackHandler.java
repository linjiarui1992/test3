
/**
 * BaitourServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.2  Built on : Sep 06, 2010 (09:42:01 CEST)
 */

    package com.baitour.www;

    /**
     *  BaitourServiceCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class BaitourServiceCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public BaitourServiceCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public BaitourServiceCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for returnOrderOutTicket method
            * override this method for handling normal response from returnOrderOutTicket operation
            */
           public void receiveResultreturnOrderOutTicket(
                    com.baitour.www.BaitourServiceStub.ReturnOrderOutTicketResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from returnOrderOutTicket operation
           */
            public void receiveErrorreturnOrderOutTicket(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for detailCreateOrder method
            * override this method for handling normal response from detailCreateOrder operation
            */
           public void receiveResultdetailCreateOrder(
                    com.baitour.www.BaitourServiceStub.DetailCreateOrderResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from detailCreateOrder operation
           */
            public void receiveErrordetailCreateOrder(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for rTCreateOrder method
            * override this method for handling normal response from rTCreateOrder operation
            */
           public void receiveResultrTCreateOrder(
                    com.baitour.www.BaitourServiceStub.RTCreateOrderResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from rTCreateOrder operation
           */
            public void receiveErrorrTCreateOrder(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAlterCommonFileStreamPolicy method
            * override this method for handling normal response from getAlterCommonFileStreamPolicy operation
            */
           public void receiveResultgetAlterCommonFileStreamPolicy(
                    com.baitour.www.BaitourServiceStub.GetAlterCommonFileStreamPolicyResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAlterCommonFileStreamPolicy operation
           */
            public void receiveErrorgetAlterCommonFileStreamPolicy(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getOrderInfo method
            * override this method for handling normal response from getOrderInfo operation
            */
           public void receiveResultgetOrderInfo(
                    com.baitour.www.BaitourServiceStub.GetOrderInfoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getOrderInfo operation
           */
            public void receiveErrorgetOrderInfo(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDomesticMatchNormalZRateByID method
            * override this method for handling normal response from getDomesticMatchNormalZRateByID operation
            */
           public void receiveResultgetDomesticMatchNormalZRateByID(
                    com.baitour.www.BaitourServiceStub.GetDomesticMatchNormalZRateByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDomesticMatchNormalZRateByID operation
           */
            public void receiveErrorgetDomesticMatchNormalZRateByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAVPolicy method
            * override this method for handling normal response from getAVPolicy operation
            */
           public void receiveResultgetAVPolicy(
                    com.baitour.www.BaitourServiceStub.GetAVPolicyResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAVPolicy operation
           */
            public void receiveErrorgetAVPolicy(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getInvalidationProviders method
            * override this method for handling normal response from getInvalidationProviders operation
            */
           public void receiveResultgetInvalidationProviders(
                    com.baitour.www.BaitourServiceStub.GetInvalidationProvidersResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getInvalidationProviders operation
           */
            public void receiveErrorgetInvalidationProviders(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for refundOrder method
            * override this method for handling normal response from refundOrder operation
            */
           public void receiveResultrefundOrder(
                    com.baitour.www.BaitourServiceStub.RefundOrderResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from refundOrder operation
           */
            public void receiveErrorrefundOrder(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for chdDetailCreateOrder method
            * override this method for handling normal response from chdDetailCreateOrder operation
            */
           public void receiveResultchdDetailCreateOrder(
                    com.baitour.www.BaitourServiceStub.ChdDetailCreateOrderResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from chdDetailCreateOrder operation
           */
            public void receiveErrorchdDetailCreateOrder(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAllCommonPolicy method
            * override this method for handling normal response from getAllCommonPolicy operation
            */
           public void receiveResultgetAllCommonPolicy(
                    com.baitour.www.BaitourServiceStub.GetAllCommonPolicyResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAllCommonPolicy operation
           */
            public void receiveErrorgetAllCommonPolicy(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getChangeFlightDate method
            * override this method for handling normal response from getChangeFlightDate operation
            */
           public void receiveResultgetChangeFlightDate(
                    com.baitour.www.BaitourServiceStub.GetChangeFlightDateResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getChangeFlightDate operation
           */
            public void receiveErrorgetChangeFlightDate(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for matchCommonPolicy method
            * override this method for handling normal response from matchCommonPolicy operation
            */
           public void receiveResultmatchCommonPolicy(
                    com.baitour.www.BaitourServiceStub.MatchCommonPolicyResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from matchCommonPolicy operation
           */
            public void receiveErrormatchCommonPolicy(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for matchCommonPolicyNew method
            * override this method for handling normal response from matchCommonPolicyNew operation
            */
           public void receiveResultmatchCommonPolicyNew(
                    com.baitour.www.BaitourServiceStub.MatchCommonPolicyNewResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from matchCommonPolicyNew operation
           */
            public void receiveErrormatchCommonPolicyNew(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAllCommonPolicyZIP method
            * override this method for handling normal response from getAllCommonPolicyZIP operation
            */
           public void receiveResultgetAllCommonPolicyZIP(
                    com.baitour.www.BaitourServiceStub.GetAllCommonPolicyZIPResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAllCommonPolicyZIP operation
           */
            public void receiveErrorgetAllCommonPolicyZIP(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAlterCommonPolicy method
            * override this method for handling normal response from getAlterCommonPolicy operation
            */
           public void receiveResultgetAlterCommonPolicy(
                    com.baitour.www.BaitourServiceStub.GetAlterCommonPolicyResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAlterCommonPolicy operation
           */
            public void receiveErrorgetAlterCommonPolicy(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAllCommonFileStreamPolicyZIP method
            * override this method for handling normal response from getAllCommonFileStreamPolicyZIP operation
            */
           public void receiveResultgetAllCommonFileStreamPolicyZIP(
                    com.baitour.www.BaitourServiceStub.GetAllCommonFileStreamPolicyZIPResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAllCommonFileStreamPolicyZIP operation
           */
            public void receiveErrorgetAllCommonFileStreamPolicyZIP(java.lang.Exception e) {
            }
                


    }
    