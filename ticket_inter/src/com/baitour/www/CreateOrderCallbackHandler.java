
/**
 * CreateOrderCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.2  Built on : Sep 06, 2010 (09:42:01 CEST)
 */

    package com.baitour.www;

    /**
     *  CreateOrderCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class CreateOrderCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public CreateOrderCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public CreateOrderCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for pnrCreateOrder method
            * override this method for handling normal response from pnrCreateOrder operation
            */
           public void receiveResultpnrCreateOrder(
                    com.baitour.www.CreateOrderStub.PnrCreateOrderResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from pnrCreateOrder operation
           */
            public void receiveErrorpnrCreateOrder(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for pnrCreateOrderEx method
            * override this method for handling normal response from pnrCreateOrderEx operation
            */
           public void receiveResultpnrCreateOrderEx(
                    com.baitour.www.CreateOrderStub.PnrCreateOrderExResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from pnrCreateOrderEx operation
           */
            public void receiveErrorpnrCreateOrderEx(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for pnrCreateOrderExStr method
            * override this method for handling normal response from pnrCreateOrderExStr operation
            */
           public void receiveResultpnrCreateOrderExStr(
                    com.baitour.www.CreateOrderStub.PnrCreateOrderExStrResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from pnrCreateOrderExStr operation
           */
            public void receiveErrorpnrCreateOrderExStr(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for pnrCreateOrderStr method
            * override this method for handling normal response from pnrCreateOrderStr operation
            */
           public void receiveResultpnrCreateOrderStr(
                    com.baitour.www.CreateOrderStub.PnrCreateOrderStrResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from pnrCreateOrderStr operation
           */
            public void receiveErrorpnrCreateOrderStr(java.lang.Exception e) {
            }
                


    }
    