package com.jinri;

import java.io.StringReader;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.rmi.RemoteException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.book.Book;
import com.ccservice.b2b2c.air.Airutil;
import com.ccservice.b2b2c.base.orderinfo.Orderinfo;
import com.ccservice.b2b2c.base.passenger.Passenger;
import com.ccservice.b2b2c.base.segmentinfo.Segmentinfo;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.inter.server.Server;
import com.ccservice.service.RateService;
import com.ccservice.test.HttpClient;
import com.liantuo.webservice.client.GetPolicyDataByIdService_2_0Stub;
import com.liantuo.webservice.client.GetPolicyDataByIdService_2_0Stub.ModifiedPolicyData;

public class JinriMethod {

	private static Log log = LogFactory.getLog(RateService.class);

	private static String JRURL = "http://210.83.80.4/ticket_inter/testjr.jsp?";
	private static String JRUSER = "feiji2010";
	private static String JRUSER_xiaji = "feiji2010";
	public static String JRKey = "53fd06411ca24f04a4fc65fa1205b1f6";
	private Airutil ariutil = new Airutil();
	private Book book = new Book();

	/**
	 * 查询今日政策
	 */
	public List<Zrate> FindRate(String scity, String ecity, String sdate,
			int voyageType) {
		List<Zrate> listRate = new ArrayList<Zrate>();

		try {
			String url = JRURL + "service=GetFlightRate&partner=" + JRUSER
					+ "&scity=" + scity + "&ecity=" + ecity + "&sdate=" + sdate
					+ "&voyageType=" + voyageType + "&sign=";
			String[] Sortedstr = new String[] { "service=GetFlightRate",
					"partner=" + JRUSER, "scity=" + scity, "ecity=" + ecity,
					"sdate=" + sdate, "voyageType=" + voyageType };

			url += HttpClient.GetSign(Sortedstr);

			String re = HttpClient.httpget(url, "GB2312");

			log.info(re);
			SAXBuilder builder = new SAXBuilder();
			Document doc = builder.build(new StringReader(re.trim()));
			Element root = doc.getRootElement();
			if (root.getChild("is_success").getTextTrim().equals("T")) {

				Element response = root.getChild("response");
				if (response != null) {
					Element items = response.getChild("RateItems");
					if (items != null) {

						List<Element> list = items.getChildren("RateItem");
						for (Element e : list) {
							Zrate rate = new Zrate();
							rate.setAgentid(6L);

							rate.setCreatetime(new Timestamp(System
									.currentTimeMillis()));
							rate.setCreateuser("web");
							rate.setModifytime(new Timestamp(System
									.currentTimeMillis()));
							rate.setModifyuser("web");

							rate.setOutid(e.getChildTextTrim("RateNo"));
							rate.setGeneral(Long.parseLong(e
									.getChildTextTrim("RateType")));
							rate.setDepartureport(e.getChildTextTrim("Scitys"));// 起始机场
							rate.setArrivalport(e.getChildTextTrim("Ecitys"));// 到达机场
							rate
									.setAircompanycode(e
											.getChildTextTrim("Aircom"));// 航空公司
							rate.setTraveltype(1); // 行程类型
							if (e.getChildTextTrim("Aircom").equals("0")) {// 不适用航班
								rate.setType(2);
								rate
										.setWeeknum(e
												.getChildTextTrim("FlightNos"));

							} else {// 适用航班
								rate.setType(1);
								rate.setFlightnumber(e
										.getChildTextTrim("FlightNos"));
							}

							if (e.getChildTextTrim("TicketType").equals("1")) { // bsp
								rate.setTickettype(1);
							} else if (e.getChildTextTrim("TicketType").equals(
									"2")) { // b2b
								rate.setTickettype(0);
							} else {
								rate.setTickettype(3); // all
							}

							rate.setCabincode(e.getChildTextTrim("Cabins")); // 适用仓位
							rate.setRatevalue(Float.parseFloat(e
									.getChildTextTrim("DisCount")));// 政策返点
							rate.setBegindate(new Timestamp(
									new SimpleDateFormat("yyyy-MM-dd").parse(
											e.getChildTextTrim("SDate"))
											.getTime()));// 政策有效期开始时间
							rate.setEnddate(new Timestamp(new SimpleDateFormat(
									"yyyy-MM-dd").parse(
									e.getChildTextTrim("EDate")).getTime()));// 政策有效期结束时间
							rate.setIssuedstartdate(new Timestamp(
									new SimpleDateFormat("yyyy-MM-dd").parse(
											e.getChildTextTrim("SDate"))
											.getTime())); // 出票开始时间
							rate.setIssuedendate(new Timestamp(
									new SimpleDateFormat("yyyy-MM-dd").parse(
											e.getChildTextTrim("EDate"))
											.getTime())); // 出票结束时间
							try {
								rate.setRemark(URLDecoder.decode(e
										.getChildTextTrim("Remark")));
							} catch (Exception e2) {
								rate.setRemark(e.getChildTextTrim("Remark"));
							}

							List<Zrate> listRates = Server.getInstance()
									.getAirService().findAllZrate(
											" where " + Zrate.COL_outid + "='"
													+ rate.getOutid() + "'",
											"", -1, 0);
							if (listRates == null || listRates.isEmpty()) {
								Server.getInstance().getAirService()
										.createZrate(rate);
							} else if (listRates.size() == 1) {
								rate.setId(listRates.get(0).getId());
								Server.getInstance().getAirService()
										.updateZrateIgnoreNull(rate);
							} else {
								Server
										.getInstance()
										.getAirService()
										.excuteZrateBySql(
												"delete from " + Zrate.TABLE
														+ " where "
														+ Zrate.COL_outid
														+ "='"
														+ rate.getOutid() + "'");
								Server.getInstance().getAirService()
										.createZrate(rate);
							}

							listRate.add(rate);
						}

					}

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			log.info(e.getMessage());
		}

		return listRate;
	}

	/**
	 * 自动支付
	 */

	public String AutoPay(Orderinfo order) {
		log.debug(order);
		long aid = order.getPolicyagentid();
		if (aid == 6) {// 今日
			try {
				String url = JRURL + "service=Order_AutoPay&partner=" + JRUSER +

				"&orderNo=" + order.getExtorderid() + "&autopaymode=1"
						+ "&paytype=A" + "&sign=";
				// 寄送方式(1:邮寄;2:邮寄已支付;3:自取)^联系人姓名^联系电话(电话，手机都可以)^邮政编码^省份^城市^地址
				String[] Sortedstr = new String[] { "service=Order_AutoPay",
						"partner=" + JRUSER,

						"orderNo=" + order.getExtorderid(), "autopaymode=1",
						"paytype=A" };

				url += HttpClient.GetSign(Sortedstr);
				System.out.println(url);
				String re = HttpClient.httpget(url, "GB2312");

				System.out.println((re));
				SAXBuilder builder = new SAXBuilder();
				Document doc = builder.build(new StringReader(re.trim()));
				Element root = doc.getRootElement();
				if (root.getChild("is_success").getTextTrim().equals("T")) {

					Element response = root.getChild("response");
					if (response != null) {
						if (response.getChildText("Pay_Status").trim().equals(
								"T")) {

							order.setExtorderstatus(1);
							Server.getInstance().getAirService()
									.updateOrderinfoIgnoreNull(order);

						}

					}

				} else {
					System.out.println(root.getChild("error").getTextTrim());

				}

			} catch (Exception e) {
				order.setExtorderstatus(-2);
				Server.getInstance().getAirService().updateOrderinfoIgnoreNull(
						order);
				e.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * 创建第三方订单
	 * 
	 * 
	 */

	public String CreateOrder(Orderinfo order, Segmentinfo sinfo,
			List<Passenger> listPassenger) {
		log.debug(order);
		log.debug(sinfo);
		long aid = order.getPolicyagentid();
		System.out.println("政策ID=" + order.getPolicyid());
		String Outid = Server.getInstance().getAirService().findZrate(
				order.getPolicyid()).getOutid();

		System.out.println("政策外部ID=" + Outid);
		String pnr = "";
		if (order.getPnr() != null) {
			pnr = order.getPnr();
			System.out.println("pnr=" + pnr);
		} else {

			System.out.println("pnr为空");
		}
		if (aid == 6) { // 今日

			try {
				Float fenxiaovalue = order.getFenxiaoshangfandian();

				Float jingxiaoshangkoudian = order.getRatevalue()
						- order.getFenxiaoshangfandian();
				System.out.println("zratevalue=="
						+ formatMoney_short(order.getRatevalue() + ""));
				System.out.println("fenxiaovalue=="
						+ formatMoney_short(fenxiaovalue + ""));
				System.out.println("jingxiaoshangkoudian=="
						+ formatMoney_short(jingxiaoshangkoudian + ""));

				String orderInfo = "1^" + JRUSER + "^" + JRUSER_xiaji + "^"
						+ "" + Outid + "^"
						+ formatMoney_short(fenxiaovalue + "") + "^"
						+ formatMoney_short(jingxiaoshangkoudian + "") + "^1";
				System.out.println("orderInfo==" + orderInfo);
				String url = JRURL + "" + "service=B2BCreateOrder&partner="
						+ JRUSER +

						"&orderInfo=" + orderInfo + "&pnr=" + order.getPnr()
						+ "&rateType=1" + "&sign=";

				String[] Sortedstr = new String[] { "service=B2BCreateOrder",
						"partner=" + JRUSER,

						"orderInfo=" + orderInfo, "pnr=" + pnr, "rateType=1" };

				url += HttpClient.GetSign(Sortedstr);

				System.out.println(url);
				String re = HttpClient.httpget(url, "GB2312");

				System.out.println((re));
				log.debug(re);
				SAXBuilder builder = new SAXBuilder();

				Document doc = builder.build(new StringReader(re.trim()));
				Element root = doc.getRootElement();

				if (root.getChild("is_success").getTextTrim().equals("T")) {

					Element response = root.getChild("response");
					if (response != null) {
						Element items = response.getChild("OrderInformation")
								.getChild("OrderItem");
						if (items != null) {

							/*
							 * order.setExtorderid(items
							 * .getChildTextTrim("OrderNo"));
							 * order.setExtorderstatus(0);
							 * order.setExtordercreatetime(new Timestamp(System
							 * .currentTimeMillis()));
							 * Server.getInstance().getAirService()
							 * .updateOrderinfoIgnoreNull(order);
							 */
							System.out.println("今日下单成功,订单号=="
									+ items.getChildTextTrim("OrderNo"));

							if (items.getChildTextTrim("OrderNo") != null
									&& items.getChildTextTrim("OrderNo")
											.length() > 0) {

								return items.getChildTextTrim("OrderNo");
							} else {

								return "-1";
							}

						} else {

							return "-1";
						}

					} else {

						return "-1";
					}

				} else {
					System.out.println("今日下单失败");
					return "-1";
					/*
					 * order.setExtorderid("-1"); order.setExtorderstatus(-1);
					 * order.setExtordercreatetime(new Timestamp(System
					 * .currentTimeMillis()));
					 * Server.getInstance().getAirService()
					 * .updateOrderinfoIgnoreNull(order);
					 */

				}

			} catch (Exception e) {
				System.out.println("今日下单异常");
				e.printStackTrace();
				return "-1";

			}
		}
		if (aid == 3) {
			System.out.println("8000yi政策");
			try {
				// this.ariutil.createEeightOrder(order.getId(),order.getPolicyid(),
				// order.getPnr());
				String orderid = ariutil.createEeightOrder(order.getPolicyid(),
						order.getPnr());
				System.out.println("订单号为" + orderid);
				return orderid;

			} catch (RemoteException e) {
				System.out.println("8000yi 创建订单失败：");
				e.printStackTrace();
				return "-1";

			}
		}
		if (aid == 5) {// 51book

			return "-1";
		}
		/*
		 * if(aid==5){//51book System.out.println("51book政策"); try {
		 * 
		 * String orderid=book.Create51BookOrder(order, sinfo, listPassenger);
		 * System.out.println("订单号为"+orderid); return orderid;
		 *  } catch (Exception e) { System.out.println("51book 创建订单失败：");
		 * e.printStackTrace(); return "-1";
		 *  } }
		 */
		return "-1";
	}

	/**
	 * 收银台模式
	 */

	public String OrderPay(Orderinfo order) {
		// PayURL
		String url = "";
		long aid = order.getPolicyagentid();
		if (aid == 6) {
			try {
				url = JRURL + "service=Order_Pay&partner=" + JRUSER +

				"&orderNo=" + order.getExtorderid() + "&paymethod=0"
						+ "&paytype=A" + "&sign=";
				// 寄送方式(1:邮寄;2:邮寄已支付;3:自取)^联系人姓名^联系电话(电话，手机都可以)^邮政编码^省份^城市^地址
				String[] Sortedstr = new String[] { "service=Order_Pay",
						"partner=" + JRUSER,
						"orderNo=" + order.getExtorderid(), "paymethod=0",
						"paytype=A" };

				url += HttpClient.GetSign(Sortedstr);
				System.out.println(url);
				String re = HttpClient.httpget(url, "GB2312");

				System.out.println((re));
				SAXBuilder builder = new SAXBuilder();
				Document doc = builder.build(new StringReader(re.trim()));
				Element root = doc.getRootElement();
				if (root.getChild("is_success").getTextTrim().equals("T")) {

					Element response = root.getChild("response");
					if (response != null) {
						Element items = response.getChild("Pay_Url");
						if (items != null) {

							return URLDecoder
									.decode(items.getTextTrim(), "GBK");

						}

					}

				} else {
					System.out.println(root.getChild("error").getTextTrim());

				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		log.debug(url);
		return url;

	}

	public boolean ChangeOrder(Orderinfo order, String flightDate,
			String flightNo) {

		long aid = order.getPolicyagentid();

		if (aid == 6) {
			String orderNo = order.getExtorderid();

			String where = " WHERE " + Passenger.COL_orderid + " = "
					+ order.getId();
			List<Passenger> list = Server.getInstance().getAirService()
					.findAllPassenger(where, "", -1, 0);

			String passengers = "";

			for (int i = 0; i < list.size(); i++) {
				passengers += list.get(i).getName();
				if (i != list.size() - 1) {
					passengers += "^";
				}

			}

			try {
				String url = JRURL + "service=ChangeSign_Apply&partner="
						+ JRUSER + "&orderNo=" + orderNo + "&passengers="
						+ passengers + "&flightDate=" + flightDate
						+ "&flightNo=" + flightNo + "&sign=";

				String[] Sortedstr = new String[] { "service=ChangeSign_Apply",
						"partner=" + JRUSER, "orderNo=" + orderNo,
						"passengers=" + passengers, "flightDate=" + flightDate,
						"flightNo=" + flightNo };
				url += HttpClient.GetSign(Sortedstr);

				String re2 = HttpClient.httpget(url, "GB2312");

				SAXBuilder builder = new SAXBuilder();
				Document doc = builder.build(new StringReader(re2.trim()));
				Element root = doc.getRootElement();
				if (root.getChild("is_success").getTextTrim().equals("T")) {
					Element response = root.getChild("response");
					if (response != null) {
						Element items = response.getChild("State");
						if (items != null && items.getTextTrim().equals("T")) {
							return true;
						}
					}
				}

				return false;
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		return false;
	}

	/**
	 * 适时查找单条今日政策
	 * 
	 * @param rateno
	 * @return
	 */
	public Zrate FindRateOne(String rateno) {

		try {
			String url = JRURL + "service=GetRateItem&partner=" + JRUSER
					+ "&rateNo=" + rateno + "&sign=";

			String[] Sortedstr = new String[] { "service=GetRateItem",
					"partner=" + JRUSER, "rateNo=" + rateno };
			url += HttpClient.GetSign(Sortedstr);

			String re2 = HttpClient.httpget(url, "GB2312");

			System.out.println((re2));
			SAXBuilder builder = new SAXBuilder();
			Document doc = builder.build(new StringReader(re2.trim()));
			Element root = doc.getRootElement();
			if (root.getChild("is_success").getTextTrim().equals("T")) {

				Element response = root.getChild("response");
				if (response != null) {

					JSONObject o = JSONObject.fromObject(response.getValue());

					Zrate rate = new Zrate();
					List<Zrate> listz = Server.getInstance().getAirService()
							.findAllZrate(
									" where 1=1 and " + Zrate.COL_outid + " ='"
											+ rateno + "'", "", -1, 0);
					if (listz.size() > 0) {

						rate = listz.get(0);
					}
					rate
							.setCreatetime(new Timestamp(System
									.currentTimeMillis()));
					rate.setCreateuser("job");
					rate
							.setModifytime(new Timestamp(System
									.currentTimeMillis()));
					rate.setModifyuser("job");
					rate.setAgentid(6l);

					rate.setOutid(o.getString("RateNo"));
					rate.setGeneral(Long.parseLong(o.getString("RateType")));
					rate.setDepartureport(o.getString("Scitys")); // 起始机场
					rate.setArrivalport(o.getString("Ecitys")); // 到达机场

					rate.setAircompanycode(o.getString("Aircom")); // 航空公司

					String FlightNos = o.getString("FlightNos");
					String isUse = o.getString("isUse");

					/*
					 * if (FlightNos.length() > 4) { // 适用航班
					 * 
					 * rate.setType(1);
					 * rate.setFlightnumber(FlightNos.replaceAll(o
					 * .getString("FlightNos"), "")); }
					 */
					if (isUse != null && isUse.equals("1")) { // 适用航班

						rate.setType(1);
						rate.setFlightnumber(o.getString("FlightNos"));
					} else {
						rate.setType(2);
						rate.setWeeknum(o.getString("FlightNos"));

					}
					rate.setTickettype(1);

					rate.setCabincode(o.getString("Cabins")); // 适用仓位
					rate
							.setRatevalue(Float.parseFloat(o
									.getString("DisCount"))); // 政策返点

					rate
							.setIssuedstartdate(new Timestamp(
									new SimpleDateFormat("yyyy-MM-dd").parse(
											o.getString("SDate")).getTime())); // 出票开始时间
					rate
							.setIssuedendate(new Timestamp(
									new SimpleDateFormat("yyyy-MM-dd").parse(
											o.getString("EDate")).getTime())); // 出票结束时间

					rate
							.setBegindate(new Timestamp(new SimpleDateFormat(
									"yyyy-MM-dd").parse(o.getString("SDate"))
									.getTime())); // 政策有效期开始时间
					rate
							.setEnddate(new Timestamp(new SimpleDateFormat(
									"yyyy-MM-dd").parse(o.getString("EDate"))
									.getTime())); // 政策有效期结束时间

					rate.setRemark(o.getString("Remark"));
					rate.setUsertype(o.getString("AdultsType"));// 乘客类型
																// 1：成人，2：儿童，3：成人或儿童
					rate.setVoyagetype(o.getString("VoyageType"));// 航程类型
																	// 1单程，2往返，3单程及往返
					rate.setZtype(o.getString("RateType"));// 普通 OR 高反

					rate.setIsenable(Integer.parseInt(o.getString("Status")));
					String WorkTime = o.getString("WorkTime");
					// rate.setWorktime(WorkTime.substring(0,5));
					// rate.setAfterworktime(WorkTime.substring(6));
					Server.getInstance().getAirService().updateZrateIgnoreNull(
							rate);

					System.out.println(rate);
					return rate;

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		return null;
	}

	/**
	 * 适时查询订单详情
	 * 
	 * @param orderno
	 * @return
	 */
	public String FindExtOrder(String orderNo) {
		try {
			String url = JRURL + "service=Order_QueryOrderInfo&partner="
					+ JRUSER + "&orderNo=" + orderNo + "&sign=";

			String[] Sortedstr = new String[] { "service=Order_QueryOrderInfo",
					"partner=" + JRUSER, "orderNo=" + orderNo };
			url += HttpClient.GetSign(Sortedstr);

			String re2 = HttpClient.httpget(url, "GB2312");

			System.out.println((re2));
			return re2;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 申请退费票
	 * 
	 * @param order
	 * @return 凭据号 1（当日作废）, 2（按客规自愿退票）, 3（非自愿及特殊退票）, 4（航班延误申请全退）, 5（取消订单）
	 * 
	 */

	public String ChangeOrder(Orderinfo order) {
		long aid = order.getPolicyagentid();

		if (aid == 6) {
			String orderNo = order.getExtorderid();

			int refundtype = 5;
			String remarks = "取消订单";
			if (order.getOrderstatus() == Orderinfo.FEIPIAOSHENHETONGGUO) {
				refundtype = 1;
				remarks = "当日废票";
			} else if (order.getOrderstatus() == Orderinfo.GAIQIANSHEHETONGGUO) {
				refundtype = 2;
				remarks = "改签";
			} else if (order.getOrderstatus() == Orderinfo.TUIPIAOSHENHETONGGUO) {
				refundtype = 3;
				remarks = "退票";
			}

			String where = " WHERE " + Passenger.COL_orderid + " = "
					+ order.getId();
			List<Passenger> list = Server.getInstance().getAirService()
					.findAllPassenger(where, "", -1, 0);

			String passengers = "";

			for (int i = 0; i < list.size(); i++) {
				passengers += list.get(i).getName();
				if (i != list.size() - 1) {
					passengers += "^";
				}

			}

			try {
				String url = JRURL + "service=Order_Refund&partner=" + JRUSER
						+ "&orderNo=" + orderNo + "&passengers=" + passengers
						+ "&refundtype=" + refundtype + "&remarks=" + remarks
						+ "&sign=";

				String[] Sortedstr = new String[] { "service=Order_Refund",
						"partner=" + JRUSER, "orderNo=" + orderNo,
						"passengers=" + passengers, "refundtype=" + refundtype,
						"remarks=" + remarks };
				url += HttpClient.GetSign(Sortedstr);

				String re2 = HttpClient.httpget(url, "GB2312");

				SAXBuilder builder = new SAXBuilder();
				Document doc = builder.build(new StringReader(re2.trim()));
				Element root = doc.getRootElement();
				if (root.getChild("is_success").getTextTrim().equals("T")) {
					Element response = root.getChild("response");
					if (response != null) {
						Element items = response.getChild("refundNo");

						return items.getTextTrim();

					}
				}

				return "";
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		return "";
	}

	public Orderinfo CreateB2COrder(Orderinfo order, Segmentinfo sinfo,
			List<Passenger> list) {
		log.debug(order);
		log.debug(sinfo);

		if (order.getPolicyagentid() == null) {
			return order;
		}
		long aid = order.getPolicyagentid();

		if (aid == 6) { // 今日
			System.out.println("今日政策");
			try {
				String flightInfo = sinfo.getStartairport()
						+ "^"
						+ sinfo.getEndairport()
						+ "^"
						+ new SimpleDateFormat("yyyy-MM-dd HH:mm").format(sinfo
								.getDeparttime())
						+ "^"
						+ new SimpleDateFormat("yyyy-MM-dd HH:mm").format(sinfo
								.getArrivaltime()) + "^" + sinfo.getCabincode()
						+ "^" + sinfo.getFlightnumber() + "^"
						+ sinfo.getFlightmodelnum() + "^"
						+ (sinfo.getDiscount().intValue()) + "^0";

				// PVG^PEK^2010-09-16 08:30^2010-09-16 10:55^Y^MU5129^320^100^0

				// 王丽嫦^1^20050106^1^1^1^A
				// String where = " WHERE "+Passenger.COL_orderid + " = " +
				// order.getId();
				// List<Passenger> list
				// =Server.getInstance().getAirService().findAllPassenger(where,
				// "", -1, 0);

				String passengerInfo = "";
				for (int i = 0; i < list.size(); i++) {
					passengerInfo += list.get(i).getName().trim();
					if (list.get(i).getIdtype() == 1) {
						passengerInfo += "^1";
					} else if (list.get(i).getIdtype() == 3) {
						passengerInfo += "^2";
					} else {
						passengerInfo += "^3";
					}
					passengerInfo += "^" + list.get(i).getIdnumber();
					if (i != list.size() - 1) {
						passengerInfo += "|";
					}
				}
				passengerInfo += "^0^0^1^";
				// 计算返点

				Float fenxiaovalue = order.getFenxiaoshangfandian();

				Float jingxiaoshangkoudian = order.getRatevalue()
						- order.getFenxiaoshangfandian();
				//

				String orderInfo = "1^"
						+ JRUSER
						+ "^"
						+ JRUSER_xiaji
						+ "^"
						+ ""
						+ Server.getInstance().getAirService().findZrate(
								order.getPolicyid()).getOutid() +
						// "^^^1" ;
						"^^^1";
				// 1^chenhairuif^chenhairuif^100907000285^3^0.1

				String url = JRURL + "" + "service=B2CCreateOrder&partner="
						+ JRUSER +

						"&flightInfo=" + URLEncoder.encode(flightInfo)
						+ "&orderInfo=" + URLEncoder.encode(orderInfo)
						+ "&passengerInfo=" + URLEncoder.encode(passengerInfo) +

						"&rateType=1" + "&sign=";

				String[] Sortedstr = new String[] { "service=B2CCreateOrder",
						"partner=" + JRUSER,

						"flightInfo=" + flightInfo, "orderInfo=" + orderInfo,
						"passengerInfo=" + passengerInfo, "rateType=1" };

				url += HttpClient.GetSign(Sortedstr);

				System.out.println(url);
				String re = HttpClient.httpget(url, "GB2312");

				System.out.println((re));
				log.debug(re);
				SAXBuilder builder = new SAXBuilder();
				Document doc = builder.build(new StringReader(re.trim()));
				Element root = doc.getRootElement();
				if (root.getChild("is_success").getTextTrim().equals("T")) {

					Element response = root.getChild("response");
					if (response != null) {
						// Element items = response.getChild("RateItems");

						// System.out.println("前=="+order);
						// order.setExtorderid(response.getChildTextTrim("OrderNo"));
						// order.setPnr(response.getChildTextTrim("PNR"));
						// order.setExtorderstatus(0);
						// order.setExtordercreatetime(new
						// Timestamp(System.currentTimeMillis()));
						// Server.getInstance().getAirService().updateOrderinfoIgnoreNull(order);
						// System.out.println("后=="+order);

					}

				} else {

					order.setExtorderstatus(-1);
					order.setPnr("NOPNR");
					order.setExtordercreatetime(new Timestamp(System
							.currentTimeMillis()));
					// Server.getInstance().getAirService().updateOrderinfoIgnoreNull(order);

				}

			} catch (Exception e) {
				e.printStackTrace();
				order.setExtorderstatus(-1);
				order.setPnr("NOPNR");
				order.setExtordercreatetime(new Timestamp(System
						.currentTimeMillis()));
				// Server.getInstance().getAirService().updateOrderinfoIgnoreNull(order);
			}
		} else if (aid == 3) {
			System.out.println("8000yi政策");
			try {
				this.ariutil.createEeightOrder(order.getPolicyid(), order
						.getPnr());
			} catch (RemoteException e) {
				System.out.println("8000yi 创建订单失败：");
				e.printStackTrace();
			}
		}

		return order;
	}

	public Zrate FindZrateByFlight(Orderinfo order, Segmentinfo sinfo,
			List<Passenger> listPassenger) throws ParseException, SQLException {
		System.out.println("生成外部订单失败,调用最优政策方法,进来了");
		System.out.println("order==" + order);
		System.out.println("sinfo==" + sinfo);
		System.out.println("Passenger==" + listPassenger);
		List<Zrate> listRate = new ArrayList<Zrate>();

		log.debug(order);
		log.debug(sinfo);
		Zrate zrate = new Zrate();

		long aid = order.getPolicyagentid();

		if (aid == 6) { // 今日
			System.out.println("今日政策");
			try {
				String flightInfo = sinfo.getStartairport()
						+ "^"
						+ sinfo.getEndairport()
						+ "^"
						+ new SimpleDateFormat("yyyy-MM-dd").format(sinfo
								.getDeparttime())
						// +"^"+new SimpleDateFormat("yyyy-MM-dd
						// HH:mm").format(sinfo.getArrivaltime())
						+ "^" + sinfo.getCabincode() + "^"
						+ sinfo.getFlightnumber();

				System.out.println("flightInfo==" + flightInfo);
				String passengerInfo = "";
				for (int i = 0; i < listPassenger.size(); i++) {
					passengerInfo += listPassenger.get(i).getName().trim();
					if (listPassenger.get(i).getIdtype() == 1) {
						passengerInfo += "^1";
					} else if (listPassenger.get(i).getIdtype() == 3) {
						passengerInfo += "^2";
					} else {
						passengerInfo += "^3";
					}
					passengerInfo += "^" + listPassenger.get(i).getIdnumber();
					if (i != listPassenger.size() - 1) {
						passengerInfo += "|";
					}
				}
				passengerInfo += "^0^0^1^";

				//
				String userType = "1";// 用户类型 散客=1, 团队=2,
				String adultsType = "1";// 乘客类型 成人=1, 儿童=2,
				String isOpenLow = "1";// 是否需要低开 不需要低开 = 1,需要低开 = 2,
				String voyageType = "1";// 航程类型 单程=1, 往返=2,
				String rateType = "1";// 政策类型 政策类型，不传代表所有 Y 从1--6
				String Num = "1";// 返回条数 返回条数，不传代表8条 Y 从1--8

				String orderInfo = "1^"
						+ JRUSER
						+ "^"
						+ JRUSER_xiaji
						+ "^"
						+ ""
						+ Server.getInstance().getAirService().findZrate(
								order.getPolicyid()).getOutid() +
						// "^^^1" ;
						"^^^1";

				String url = JRURL
						+ ""
						+ "service=ImportPnrRate&partner="
						+ JRUSER
						+

						"&flightInfo="
						+ URLEncoder.encode(flightInfo)
						+
						// "&orderInfo=" +URLEncoder.encode(orderInfo)+
						// "&passengerInfo=" +URLEncoder.encode(passengerInfo)+
						"&userType=" + userType + "" + "&adultsType="
						+ adultsType + "" + "&isOpenLow=" + isOpenLow + ""
						+ "&voyageType=" + voyageType + "" + "&rateType="
						+ rateType + "" + "&Num=" + Num + "" + "&sign=";

				String[] Sortedstr = new String[] {
						"service=ImportPnrRate",
						"partner=" + JRUSER,

						"flightInfo=" + flightInfo,
						// "orderInfo="+orderInfo,
						// "passengerInfo="+passengerInfo,
						"userType=" + userType, "adultsType=" + adultsType,
						"isOpenLow=" + isOpenLow, "voyageType=" + voyageType,
						"Num=" + Num, "rateType=" + rateType };
				System.out.println("Sortedstr==" + Sortedstr);
				url += HttpClient.GetSign(Sortedstr);

				System.out.println(url);
				String re = HttpClient.httpget(url, "GB2312");

				System.out.println((re));
				log.debug(re);
				SAXBuilder builder = new SAXBuilder();
				Document doc = builder.build(new StringReader(re.trim()));
				Element root = doc.getRootElement();
				if (root.getChild("is_success").getTextTrim().equals("T")) {

					Element response = root.getChild("response");

					Element items = response.getChild("RateItems");
					if (items != null) {

						List<Element> listz = items.getChildren("RateItem");
						for (Element e : listz) {

							System.out.println(e.getChildTextTrim("DisCount"));

							Zrate rate = new Zrate();
							rate.setAgentid(6L);

							rate.setCreatetime(new Timestamp(System
									.currentTimeMillis()));
							rate.setCreateuser("web");
							rate.setModifytime(new Timestamp(System
									.currentTimeMillis()));
							rate.setModifyuser("web");

							rate.setOutid(e.getChildTextTrim("RateNo"));
							rate.setGeneral(Long.parseLong(e
									.getChildTextTrim("RateType")));
							rate.setDepartureport(e.getChildTextTrim("Scitys"));// 起始机场
							rate.setArrivalport(e.getChildTextTrim("Ecitys"));// 到达机场
							rate
									.setAircompanycode(e
											.getChildTextTrim("Aircom"));// 航空公司
							rate.setTraveltype(1); // 行程类型
							if (e.getChildTextTrim("isUse").equals("0")) {// 不适用航班
								rate.setType(2);
								rate
										.setWeeknum(e
												.getChildTextTrim("FlightNos"));
							} else {
								rate.setType(1);
								rate.setFlightnumber(e
										.getChildTextTrim("FlightNos"));
							}

							if (e.getChildTextTrim("TicketType").equals("1")) { // bsp
								rate.setTickettype(1);
							} else if (e.getChildTextTrim("TicketType").equals(
									"2")) { // b2b
								rate.setTickettype(0);
							} else {
								rate.setTickettype(3); // all
							}
							if (e.getChildTextTrim("1") != null) {
								rate.setIsenable(Integer.parseInt((e
										.getChildTextTrim("1"))));
							}
							rate.setCabincode(e.getChildTextTrim("Cabins")); // 适用仓位
							rate.setRatevalue(Float.parseFloat(e
									.getChildTextTrim("DisCount")));// 政策返点
							/*
							 * rate.setBegindate(new Timestamp( new
							 * SimpleDateFormat("yyyy-MM-dd").parse(e.getChildTextTrim("SDate")).getTime()));//
							 * 政策有效期开始时间 rate.setEnddate(new Timestamp(new
							 * SimpleDateFormat("yyyy-MM-dd").parse(e.getChildTextTrim("EDate")).getTime()));//
							 * 政策有效期结束时间 rate.setIssuedstartdate(new Timestamp(
							 * new SimpleDateFormat("yyyy-MM-dd").parse(
							 * e.getChildTextTrim("SDate")) .getTime())); //
							 * 出票开始时间 rate.setIssuedendate(new Timestamp( new
							 * SimpleDateFormat("yyyy-MM-dd").parse(
							 * e.getChildTextTrim("EDate")) .getTime())); //
							 * 出票结束时间
							 */
							rate.setBegindate(new Timestamp(
									new SimpleDateFormat("yyyy-MM-dd").parse(
											"2011-10-01").getTime()));// 政策有效期开始时间
							rate
									.setEnddate(new Timestamp(
											new SimpleDateFormat("yyyy-MM-dd")
													.parse("2011-12-01")
													.getTime()));// 政策有效期结束时间
							rate.setIssuedstartdate(new Timestamp(
									new SimpleDateFormat("yyyy-MM-dd").parse(
											"2011-10-01").getTime())); // 出票开始时间
							rate.setIssuedendate(new Timestamp(
									new SimpleDateFormat("yyyy-MM-dd").parse(
											"2011-12-01").getTime())); // 出票结束时间
							rate.setVoyagetype(voyageType);
							rate.setUsertype(adultsType);
							rate.setZtype(rateType);

							try {
								rate.setRemark(URLDecoder.decode(e
										.getChildTextTrim("Remark")));
							} catch (Exception e2) {
								rate.setRemark(e.getChildTextTrim("Remark"));
							}

							List<Zrate> listRates = Server.getInstance()
									.getAirService().findAllZrate(
											" where " + Zrate.COL_outid + "='"
													+ rate.getOutid() + "'",
											"", -1, 0);
							if (listRates == null || listRates.isEmpty()) {
								Server.getInstance().getAirService()
										.createZrate(rate);
							} else if (listRates.size() == 1) {
								rate.setId(listRates.get(0).getId());
								Server.getInstance().getAirService()
										.updateZrateIgnoreNull(rate);
							} else {
								Server
										.getInstance()
										.getAirService()
										.excuteZrateBySql(
												"delete from " + Zrate.TABLE
														+ " where "
														+ Zrate.COL_outid
														+ "='"
														+ rate.getOutid() + "'");
								Server.getInstance().getAirService()
										.createZrate(rate);
							}
							System.out.println("rate.getFlightnumber()=="
									+ rate.getFlightnumber());
							System.out.println("sinfo.getFlightnumber()=="
									+ sinfo.getFlightnumber());
							System.out.println("rate.getWeeknum()=="
									+ rate.getWeeknum());

							if (rate.getType() == 1) {// 适用

								if (sinfo.getFlightnumber().indexOf(
										rate.getFlightnumber()) != -1) {
									listRate.add(rate);

								}
							} else {// 不适用

								if (sinfo.getFlightnumber().indexOf(
										rate.getWeeknum()) == -1) {

									listRate.add(rate);
								}

							}

							/*
							 * if(sinfo.getFlightnumber().indexOf(rate.getFlightnumber())!=-1||sinfo.getFlightnumber().indexOf(rate.getWeeknum())==-1){
							 * 
							 * listRate.add(rate); }
							 */
							/*
							 * if(rate.getType()==1){
							 * if(sinfo.getFlightnumber().indexOf(rate.getFlightnumber())!=-1){
							 * 
							 *  } listRate.add(rate); }else{
							 * 
							 * listRate.add(rate); }
							 */
							if (listRate.size() > 0) {
								// 取出政策list中最优的政策信息 ---开始
								// 修改人：sunbin
								// 修改时间：2011-10-10
								try {
									float fzratetemp = 0f; // 临时政策值
									int intindex = 0; // 最高政策序号
									for (int z = 0; z < listRate.size(); z++) {
										if (listRate.get(z).getRatevalue() > fzratetemp) {
											fzratetemp = listRate.get(z)
													.getRatevalue();
											intindex = z;
										}
									}
									zrate = listRate.get(intindex);
								} catch (Exception ex) {
									zrate = listRate.get(0);
								}
								// 取出政策list中最优的政策信息 ---结束
							}

						}

					}

				} else {

					zrate = new Zrate();

				}

			} catch (Exception e) {
				e.printStackTrace();
				zrate = new Zrate();
			}
		}
		if (aid == 3) {// 八千亿政策
			System.out.println("八千亿政策");
			try {
				zrate = ariutil.getBestZratebyPnr(order.getPnr(), 0);
			} catch (RemoteException e) {
				System.out.println("八千亿,通过PNR获取最优政策异常!!!");
				e.printStackTrace();
			}

		}

		return zrate;
	}

	public static Log getLog() {
		return log;
	}

	public static void setLog(Log log) {
		JinriMethod.log = log;
	}

	public static String getJRURL() {
		return JRURL;
	}

	public static void setJRURL(String jrurl) {
		JRURL = jrurl;
	}

	public static String getJRUSER() {
		return JRUSER;
	}

	public static void setJRUSER(String jruser) {
		JRUSER = jruser;
	}

	public static String getJRUSER_xiaji() {
		return JRUSER_xiaji;
	}

	public static void setJRUSER_xiaji(String jruser_xiaji) {
		JRUSER_xiaji = jruser_xiaji;
	}

	public static String getJRKey() {
		return JRKey;
	}

	public static void setJRKey(String key) {
		JRKey = key;
	}

	/**
	 * 根据政策ID获取当前政策的最新信息。用于8000翼 (non-Javadoc)
	 * 
	 * @see com.ccservice.service.IRateService#getEightnewZrate(long,
	 *      java.lang.String, java.lang.String, java.lang.String,
	 *      java.lang.String)
	 */

	public Zrate getEightnewZrate(long zid, String outid, String fromcity,
			String tocity, String aircode) {
		try {
			return ariutil.getCurrentZrate(zid, outid, fromcity, tocity,
					aircode);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public Zrate getBestZrateByPNR(String PNR, int special) {
		// TODO Auto-generated method stub
		return null;
	}

	public String formatMoney_short(String s) {

		// String s = "123.456 ";
		float money = Float.valueOf(s).floatValue();

		DecimalFormat format = null;
		format = (DecimalFormat) NumberFormat.getInstance();
		format.applyPattern("###0.00");
		try {
			String result = format.format(money);
			return result;
		} catch (Exception e) {
			return Float.toString(money);
		}
	}

	public Zrate FindZrateByIdTo51Book1(String zrateid)
			throws NoSuchAlgorithmException, RemoteException, SQLException,
			ParseException {
		// TODO Auto-generated method stub
		Zrate zrate = new Zrate();
		GetPolicyDataByIdService_2_0Stub stub = new GetPolicyDataByIdService_2_0Stub();

		GetPolicyDataByIdService_2_0Stub.GetPolicyById data = new GetPolicyDataByIdService_2_0Stub.GetPolicyById();
		GetPolicyDataByIdService_2_0Stub.GetPolicyByIdRequest re = new GetPolicyDataByIdService_2_0Stub.GetPolicyByIdRequest();

		GetPolicyDataByIdService_2_0Stub.SecurityCredential sec = new GetPolicyDataByIdService_2_0Stub.SecurityCredential();

		re.setPolicyId(zrateid);// 政策ID,外部id

		sec.setAgencyCode("BJS_S111016");
		String sign = HttpClient
				.MD5(sec.getAgencyCode() + re.getPolicyId() + re.getParam1()
						+ re.getParam2() + re.getParam3() + "n5Y)F6r^");
		sec.setSign(sign);

		re.setCredential(sec);
		data.setIn0(re);

		GetPolicyDataByIdService_2_0Stub.GetPolicyByIdResponse res = stub
				.getPolicyById(data);

		if (res.getOut().getReturnCode() != null
				&& res.getOut().getReturnCode().equals("S")) {// 成功 S 失败 F

			ModifiedPolicyData aa = res.getOut().getModifiedPolicyData();
			if (aa != null) {
				System.out.println("返点=" + aa.getCommision() + ",航空公司代码="
						+ aa.getAirlineCode() + ",适合航班="
						+ aa.getFlightNoIncluding() + ",不适合航班="
						+ aa.getFlightNoExclude() + ",航线="
						+ aa.getFlightCourse());
				String where = " where 1=1 and " + Zrate.COL_outid + " ='"
						+ aa.getId() + "' and " + Zrate.COL_agentid + " =5";
				List<Zrate> listz = Server.getInstance().getAirService()
						.findAllZrate(where, " ORDER BY ID DESC", -1, 0);
				if (listz.size() > 0) {
					zrate = listz.get(0);

				}
				zrate.setCreatetime(new Timestamp(System.currentTimeMillis()));
				zrate.setCreateuser("job");
				zrate.setModifytime(new Timestamp(System.currentTimeMillis()));
				zrate.setModifyuser("job");
				zrate.setAgentid(5l);
				zrate.setTickettype(1);
				zrate.setIsenable(1);

				zrate.setOutid(aa.getId() + "");

				zrate.setAircompanycode(aa.getAirlineCode());
				if (aa.getFlightCourse() != null
						&& aa.getFlightCourse().length() > 0) {
					zrate.setDepartureport(aa.getFlightCourse().split("-")[0]);// 出发机场
					zrate.setArrivalport(aa.getFlightCourse().split("-")[1]);// 到达
				}
				if (aa.getCommision() > 0) {
					zrate.setRatevalue(aa.getCommision());
				}
				zrate.setCabincode(aa.getSeatClass());
				zrate.setFlightnumber(aa.getFlightNoIncluding());// 适用的航班
				zrate.setWeeknum(aa.getFlightNoExclude());// 不适用的航班
				zrate.setSchedule(aa.getFlightCycle());// 航班周期 1234567

				// aa.getNeedSwitchPNR(); True——供应商需要更换成自己的PNR出票 False——不许更换
				if (aa.getStartDate() != null
						&& aa.getPrintTicketStartDate() != null) {
					zrate.setBegindate(new Timestamp(new SimpleDateFormat(
							"yyyy-MM-dd").parse(aa.getStartDate() + "")
							.getTime()));
					zrate.setIssuedstartdate(new Timestamp(
							new SimpleDateFormat("yyyy-MM-dd").parse(
									aa.getPrintTicketStartDate() + "")
									.getTime()));

				}
				if (aa.getExpiredDate() != null
						&& aa.getPrintTicketExpiredDate() != null) {
					zrate.setIssuedendate(new Timestamp(new SimpleDateFormat(
							"yyyy-MM-dd").parse(aa.getExpiredDate() + "")
							.getTime())); // 出票结束时间

					zrate.setEnddate(new Timestamp(new SimpleDateFormat(
							"yyyy-MM-dd").parse(
							aa.getPrintTicketExpiredDate() + "").getTime())); // 政策有效期结束时间
				}
				if (aa.getPolicyType() != null
						&& aa.getPolicyType().equals("B2B")) {
					zrate.setTickettype(2);

				} else {

					zrate.setTickettype(1);
				}

				if (aa.getRouteType() != null && aa.getRouteType().equals("OW")) {// 单程

					zrate.setVoyagetype("1");// 1单程,2:往返，3:单程或往返

				} else {
					zrate.setVoyagetype("2");// 1单程,2:往返，3:单程或往返

				}

				if (aa.getWorkTime() != null && aa.getWorkTime().length() > 0
						&& aa.getWorkTime().indexOf("-") != -1) {
					String worktime = aa.getWorkTime().split("-")[0];
					zrate.setWorktime(worktime);
					zrate.setAfterworktime(aa.getWorkTime().split("-")[1]);

				}
				if (aa.getBusinessUnitType() != null) {

					if (aa.getBusinessUnitType().equals("0")) {// =0 普通政策 =1
																// 特殊政策

						zrate.setGeneral(1l);
						zrate.setZtype("1");
					} else {

						zrate.setGeneral(2l);// 1,普通 2高反
						zrate.setZtype("2");
					}

				} else {

					zrate.setGeneral(1l);
					zrate.setZtype("1");
				}
				zrate.setUsertype("1");
				zrate.setSpeed(aa.getAgencyEfficiency());
				zrate.setRemark(aa.getComment());

				if (listz.size() > 0) {
					Server.getInstance().getAirService().updateZrateIgnoreNull(
							zrate);

				} else {

					Server.getInstance().getAirService().createZrate(zrate);
					System.out.println("add==" + zrate);
				}

			}
		} else {

			return zrate;
		}

		return zrate;
	}

	public Zrate FindOneZrateTo51Book(String zrateoutid) {
		Zrate zrate = new Zrate();

		try {
			zrate = book.FindOneZrateByIdTo51Book(zrateoutid);
		} catch (RuntimeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return zrate;
	}

	public String PayOrderUrlById(String orderid) {
		// TODO Auto-generated method stub
		return null;
	}

	public static void main(String[] args) {

	}
}
