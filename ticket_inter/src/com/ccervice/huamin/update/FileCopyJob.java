package com.ccervice.huamin.update;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * 
 * @author wzc 复制华闽传送的变价文件，以及房态变化文件，删除老的传送方式文件
 */
public class FileCopyJob implements Job {

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		System.out.println("开始复制文件job……………………");
		new FileCopyAction().exeMethod();
		System.out.println("复制文件job结束……………………");
	}

}
