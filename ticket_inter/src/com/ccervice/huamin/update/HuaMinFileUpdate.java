package com.ccervice.huamin.update;

import java.io.File;
import java.io.FileFilter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.ccservice.b2b2c.base.hmhotelprice.Hmhotelprice;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.b2b2c.base.roomtype.Roomtype;
import com.ccservice.b2b2c.base.util.Util;
import com.ccservice.compareprice.FtpUpfile;
import com.ccservice.inter.server.Server;

/**
 * 
 * @author wzc
 * 
 */
public class HuaMinFileUpdate {

	public Double getSealPrice(Double baseprice) {
		int fee = 0;
		if (baseprice <= 1000) {
			fee = 23;
		} else if (baseprice > 1000 && baseprice <= 2000) {
			fee = 25;
		} else if (baseprice > 2000) {
			fee = 30;
		}
		return baseprice + fee;
	}
	public static void main(String[] args) {
		new HuaMinFileUpdate().test();
	}
	
	public void test() {
		long t1=System.currentTimeMillis();
		//new HuaMinFileUpdate().parselockfile();
		new HuaMinFileUpdate().parseUpdateprice();
		//new HuaMinFileUpdate().copyFile();
		long t2=System.currentTimeMillis();
		System.out.println("用时："+(t2-t1)/1000+"s");
	}

	public void copyFile(){
		File ydx = new File("D:\\酒店价格\\ydx");
		if (!ydx.exists()) {
			ydx.mkdirs();
		}
		FtpUpfile fUp;
		try {
			fUp = new FtpUpfile("123.196.114.122", 21, "HuaMin", "q1w2e3r4t5");
			fUp.login();
			ArrayList<String> list = fUp.fileNames("/ydx/");
			System.out.println(list.size());
			for (String filename : list) {
				try {
					System.out.println("下载文件："+filename);
					fUp.downFile(filename, ydx.getAbsolutePath()+"\\"+getFileName(filename));
				} catch (Exception e) {
					e.printStackTrace();
					fUp.downFile(filename, ydx.getAbsolutePath()+"\\"+getFileName(filename));
				}
			}
			for (String filename : list) {
				System.out.println("开始删除……");
				fUp.deleteLoadFile(fUp.getFtpclient(), filename);
			}
			fUp.logout();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private String getFileName(String filename) {
		return filename.substring(filename.lastIndexOf("/")+1);
	}
	/**
	 * 更新不可用酒店信息
	 * @throws Exception 
	 */
	public void parselockfile() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		File files = new File("D:\\酒店价格\\ydx");
		File over = new File("D:\\酒店价格\\ydx\\over\\" + sdf.format(new Date()));
		File exp = new File("D:\\酒店价格\\ydx\\exp\\" + sdf.format(new Date()));
		if (!over.exists()) {
			over.mkdirs();
		}
		if (!exp.exists()) {
			exp.mkdirs();
		}
		if (files.exists()) {
			File[] file = files.listFiles(new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					String filename = pathname.getName();
					if (filename.indexOf("HMC_PUSHBLOCKHT_") == 0) {
						return true;
					}
					return false;
				}
			});
			for (File f : file) {
				SAXBuilder sb = new SAXBuilder();
				try {
					Document doc = sb.build(f);
					Util.copyfile(f, new File(over, f.getName()));
					f.delete();

					Element root = doc.getRootElement();
					Element contracts = root.getChild("CONTRACTS");
					List<Element> hotels = contracts
							.getChildren("CONTRACT_LIST");
					for (Element temp : hotels) {
						String htcode = temp.getChildText("HT_CODE");
						String startdate = temp.getChildText("FROM_DATE");
						String todata = temp.getChildText("TO_DATE");
						Server
								.getInstance()
								.getSystemService()
								.findMapResultBySql(
										"delete from t_hotelgooddata where c_hotelid in "
												+ "(select id from t_hotel where c_sourcetype=3 and c_hotelcode='"
												+ htcode
												+ "')  and C_DATENUM>='"
												+ startdate
												+ "' and C_DATENUM<='" + todata
												+ "'", null);
						System.out.println("删除该酒店所有的价格信息……");
					}
				} catch (Exception e) {
					e.printStackTrace();
					Util.copyfile(new File(over, f.getName()), new File(exp, f
							.getName()));
				}
			}
		}
	}

	/**
	 * 更新价格数据/解析房态变化文件
	 */
	public void parseUpdateprice() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		File files = new File("D:\\酒店价格");
		File over = new File("D:\\酒店价格\\ydx\\over\\" + sdf.format(new Date()));
		File exp = new File("D:\\酒店价格\\ydx\\exp\\" + sdf.format(new Date()));

		if (!over.exists()) {
			over.mkdirs();
		}
		if (!exp.exists()) {
			exp.mkdirs();
		}
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, 30);
		if (files.exists()) {
			File[] file = files.listFiles(new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					String filename = pathname.getName();
					if (filename.indexOf("HMC_PUSHRATE_CN00839_") == 0
							|| filename.indexOf("HMC_PUSHALLOT_") == 0) {
						return true;
					}
					return false;
				}
			});
			if (file.length > 0) {
				Arrays.sort(file, new Comparator<File>() {
					@Override
					public int compare(File file1, File file2) {
						// TODO Auto-generated method stub
						String s = "";
						String s1 = "";
						if (file1.getName().indexOf("HMC_PUSHRATE_CN00839_") == 0) {
							String[] str = file1.getName().split("_");
							s = str[5] + str[6];
						} else {
							String[] str = file1.getName().split("_");
							s = str[4] + str[5];
						}
						if (file2.getName().indexOf("HMC_PUSHALLOT_") == 0) {
							String[] str = file2.getName().split("_");
							s1 = str[4] + str[5];
						} else {
							String[] str = file2.getName().split("_");
							s1 = str[5] + str[6];
						}
						return s.compareTo(s1);
					}

				});
				for (File f : file) {
					if (f.getName().indexOf("HMC_PUSHRATE_CN00839_") == 0) {
						SAXBuilder sb = new SAXBuilder();
						try {
							Document doc = sb.build(f);
							Util.copyfile(f, new File(over, f.getName()));
							Element root = doc.getRootElement();
							Element result = root.getChild("XML_RESULT");
							if (result != null) {
								List<Element> options = result.getChildren();
								if (options != null && options.size() > 0) {
									for (Element opt : options) {
										String optname = opt.getName();
										if (optname.equals("DELETE")) {
											parseDom(opt, optname);
										}
									}
									for (Element opt : options) {
										String optname = opt.getName();
										if (optname.equals("INSERT")) {
											parseDom(opt, optname);
										} else if (optname.equals("UPDATE")) {
											parseDom(opt, optname);
										}
									}
								}
							}
							f.delete();
						}  catch (Exception e) {
							e.printStackTrace();
							Util.copyfile(new File(over, f.getName()),
									new File(exp, f.getName()));
						}
					} else {
						SAXBuilder sb = new SAXBuilder();
						try {
							SimpleDateFormat sd = new SimpleDateFormat("dd-M-yy");
							Document doc = sb.build(f);
							Util.copyfile(f, new File(over, f.getName()));
							Element root = doc.getRootElement();
							Element xmlresult = root.getChild("XML_RESULT");
							Element update = xmlresult.getChild("UPDATE");
							String contract = update.getChildText("CONTRACT");
							String ver = update.getChildText("VER");
							String hotelID = update.getChildText("HOTEL");
							String cur = update.getChildText("CUR");
							List<Hotel> hotels = Server.getInstance().getHotelService()
							.findAllHotel("where c_hotelcode='"+ hotelID+ "'","", -1, 0);
							if(hotels.size()>0){
								Hotel hotel=hotels.get(0);
								List<Element> products = update.getChildren("PRODUCT");
								for (Element product : products) {
									String prod = product.getChildText("PROD");
									List<Element> rooms = product.getChildren("ROOM");
									for (Element room : rooms) {
										String cat = room.getChildText("CAT");
										String type = room.getChildText("TYPE");
										Roomtype roomtype = null;
										String where = "where c_hotelid="+ hotel.getId()+ " and C_ROOMCODE='"
												+ cat+ "' and  C_BED in (select ID from T_BEDTYPE where C_TYPE='"
												+ type + "')";
										List<Roomtype> roomtypes = Server.getInstance()
												.getHotelService().findAllRoomtype(where, "", -1, 0);
										if(roomtypes.size()>0){
											roomtype=roomtypes.get(0);
											String serv = room.getChildText("SERV");
											String bf = room.getChildText("BF");
											List<Element> stays = room.getChildren("STAY");
											for (Element stay : stays) {
												String statedate = stay.getChildText("STAYDATE");
												String date = sdf.format(sd.parse(statedate));
												if (sd.parse(statedate).before(cal.getTime())) {
													System.out.println("更新房态……");
													String isallot = stay.getChildText("IS_ALLOT");
													String str = "[dbo].[updatehuaminAllot]@hotelid = "
														+ hotel.getId()+ ","+ "@contractid = N'"
														+ contract+ "',"+ "@contractver = N'"
														+ ver+ "',"+ "@prodid = N'"+ prod+ "',"+ "@cat = "
														+ roomtype.getId()+ ","+ "@type = "+ roomtype.getBed()+ ","
														+ "@bf = "+ bf+ ","+ "@date = N'"
														+ date+ "',"+ "@isallot = N'"+ isallot + "'";
													System.out.println(str);
													synchronized (this) {
														List result = Server.getInstance()
														.getSystemService().findMapResultByProcedure(str);
														System.out.println("*********");
													}
												}
										   }
										}
									}
								}
							}
							f.delete();
							System.out.println("删除");
						}catch (Exception e) {
							e.printStackTrace();
							Util.copyfile(f,new File(exp, f.getName()));
						}

					}
				}
			}
		}
	}

	/**
	 * 文档临时节点解析
	 * 
	 * @throws Exception
	 * 
	 */
	public void parseDom(Element opt, String typeopt) throws Exception {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, 28);
		SimpleDateFormat sd = new SimpleDateFormat("dd-M-yy");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String contract = opt.getChildText("CONTRACT");
		String ver = opt.getChildText("VER");
		String hotelID = opt.getChildText("HOTEL");
		Hotel hotel = null;
		List<Hmhotelprice> gooddatas = new ArrayList<Hmhotelprice>();
		List<Hotel> hotels = Server.getInstance().getHotelService()
				.findAllHotel("where c_hotelcode='"+ hotelID+ "'","", -1, 0);
		if (hotels.size() > 0) {
			hotel = hotels.get(0);
			String cur = opt.getChildText("CUR");
			String hotelname = opt.getChildText("HOTELNAME");
			List<Element> products = opt.getChildren("PRODUCT");
			for (Element product : products) {
				String prod = product.getChildText("PROD");
				String rorate = product.getChildText("RORATE");
				String nation = product.getChildText("NATION");
				String nationname = product.getChildText("NATIONNAME");
				String min = product.getChildText("MIN");
				String max = product.getChildText("MAX");
				String advance = product.getChildText("ADVANCE");
				String ticket = product.getChildText("TICKET");
				String cancel = product.getChildText("CANCEL");
				String cutoff = product.getChildText("CUTOFF");
				List<Element> rooms = product.getChildren("ROOM");
				for (Element room : rooms) {
					String cat = room.getChildText("CAT");
					String type = room.getChildText("TYPE");
					String serv = room.getChildText("SERV");
					Roomtype roomtype = null;
					String where = "where c_hotelid="+ hotel.getId()+ " and C_ROOMCODE='"
							+ cat+ "' and  C_BED in (select ID from T_BEDTYPE where C_TYPE='"
							+ type + "')";
					List<Roomtype> roomtypes = Server.getInstance()
							.getHotelService().findAllRoomtype(where, "", -1, 0);
					if (roomtypes.size() > 0) {
						roomtype = roomtypes.get(0);
						String bedypename = "";
						try {
							bedypename = Server.getInstance().getHotelService()
									.findBedtype(roomtype.getBed())
									.getTypename();
						} catch (Exception e) {

						}
						String bf = room.getChildText("BF");
						String deadline = room.getChildText("DEADLINE");
						List<Element> stays = room.getChildren("STAY");
						for (Element stay : stays) {
							String staydate = stay.getChildText("STAYDATE");
							String date = sdf.format(sd.parse(staydate));
							if (sd.parse(staydate).before(cal.getTime())) {
								if (typeopt.equals("INSERT")
										|| typeopt.equals("UPDATE")) {
									Hmhotelprice hm = new Hmhotelprice();
									hm.setHotelid(hotel.getId());
									hm.setRoomtypeid(roomtype.getId());
									hm.setCountryname(nationname);
									hm.setMinday(Long.parseLong(min));
									hm.setMaxday(Long.parseLong(max));
									hm.setAdvancedday(Long.parseLong(advance));
									hm.setTicket(ticket);
									hm.setType(roomtype.getBed().toString());
									hm.setCityid(hotel.getCityid() + "");
									String price = stay.getChildText("PRICE");
									hm.setCur(cur);
									hm.setAbleornot(0);
									hm.setPrice(Double.parseDouble(price));
									hm.setPriceoffer(getSealPrice(Double
											.parseDouble(price)));
									hm.setStatedate(date);
									hm.setServ(serv);
									hm.setCountryid(nation);
									String allot = stay.getChildText("ALLOT");
									hm.setAllot(allot);
									String isallot = stay.getChildText("IS_ALLOT");
									hm.setIsallot(isallot);
									if ("C".equals(isallot)) {//关房
										hm.setYuliuNum(0l);
										hm.setRoomstatus(1l);
										hm.setIsallot("C");
									}else if ("N".equals(isallot)){//等待确认
										hm.setRoomstatus(0l);
										hm.setYuliuNum(0l);// IS_ALLOT
										hm.setIsallot("N");
									}else if ("Y".equals(isallot)){//及时确认
										hm.setRoomstatus(0l);
										hm.setYuliuNum(0l);// IS_ALLOT
										hm.setIsallot("Y");
									}else {
										hm.setRoomstatus(0l);
										hm.setYuliuNum(Long.parseLong(isallot));// IS_ALLOT
										hm.setIsallot("Y");
									}
									hm.setContractid(contract);
									hm.setContractver(ver);
									hm.setBf(Long.parseLong(bf));
									hm.setProd(prod);
									gooddatas.add(hm);
								} else if (typeopt.equals("DELETE")) {
									String str = "[dbo].[deletehuaminData]"
											+ "@hotelid = N'" + hotel.getId()
											+ "'," + "@contractid = N'"
											+ contract + "',"
											+ "@contractver = N'" + ver + "',"
											+ "@prodid = N'" + prod + "',"
											+ "@cat = N'" + roomtype.getId()
											+ "'," + "@bf = " + bf + ","
											+ "@date = N'" + date + "'";
									System.out.println("删除价格……");
									System.out.println(str);
									synchronized (this) {
										List result = Server.getInstance().getSystemService().findMapResultByProcedure(str);
									}
								}
							}
						}
					}
				}
			}
		}
		writeDataDB(gooddatas);

	}

	public void writeDataDB(List<Hmhotelprice> hmprice) {
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for (int i = 0; i < hmprice.size(); i++) {
			Hmhotelprice good = hmprice.get(i);
			good.setUpdatetime(sdf1.format(new Date(System.currentTimeMillis())));
			good.setId(0l);
			// 特殊处理
			System.out.println(good);
			String str = "[dbo].[sp_updatehuaminData] @hotelid = "
					+ good.getHotelid() + ",@roomtypeid = "
					+ good.getRoomtypeid() + ",@baseprice = " + good.getPrice()
					+ ",@sealprice = " + good.getPriceoffer()
					+ ",@roomstatus = " + good.getRoomstatus()
					+ ",@yuliunum = " + good.getYuliuNum() + ",@datenum = N'"
					+ good.getStatedate() + "',@minday = " + good.getMinday()
					+ ",@beforeday = " + good.getAdvancedday()
					+ ",@contractid = N'" + good.getContractid()
					+ "',@contractver = N'" + good.getContractver()
					+ "',@prodid = N'" + good.getProd() + "',@bfcount = "
					+ good.getBf() + ",@updatetime = N'"
					+ good.getUpdatetime() + "',@cityid=N'" + good.getCityid()
					+ "',@bedtypeid=" + good.getType() + ",@allot="
					+ good.getAllot() + ",@cur=N'" + good.getCur()
					+ "',@countryid=N'" + good.getCountryid()
					+ "',@countryname=N'" + good.getCountryname()
					+ "',@maxday=" + good.getMaxday() + ",@ticket=N'"
					+ good.getTicket() + "',@serv=N'" + good.getServ()
					+ "',@isallot=N'" + good.getIsallot() + "',@ableornot="
					+ good.getAbleornot();
			synchronized (this) {
				List result = Server.getInstance().getSystemService()
						.findMapResultByProcedure(str);
			}
		}

	}

}
