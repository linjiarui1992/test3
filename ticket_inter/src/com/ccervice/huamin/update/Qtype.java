package com.ccervice.huamin.update;

import java.io.StringReader;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.ccservice.inter.server.Server;
import com.ccservice.b2b2c.base.bedtype.Bedtype;

/**
 * 接口七 查询床型
 */
public class Qtype {
	public static Integer getType(String type) {
		String totalurl = HMRequestUitl.getHMRequestUrlHeader() + "&api=qtype&p_type="+type+"&p_typename=";
		System.out.println(totalurl);
		String str = Util.getStr(totalurl);
		return parsexml(str);
	}

	public static Integer parsexml(String xml) {
		int res=0;
		SAXBuilder build = new SAXBuilder();
		Document document;
		try {
			document = build.build(new StringReader(xml));
			Element root = document.getRootElement();
			Element result = root.getChild("XML_RESULT");
			List<Element> types = result.getChildren("TYPES");
			for (Element type : types) {
				Bedtype bedtype = new Bedtype();
				String typeid = type.getChildText("TYPE");
				String typename = type.getChildText("TYPENAME");
				String maxguest = type.getChildText("MAXGUEST");
				bedtype.setType(typeid);
				bedtype.setTypename(typename);
				bedtype.setMaxguest(Long.parseLong(maxguest.trim()));
				List<Bedtype> bedtypefromtable = Server.getInstance().getHotelService().findAllBedtype(
						" WHERE " + Bedtype.COL_type + "='" + bedtype.getType() + "'", "", -1, 0);
				if(bedtypefromtable.size()>0){
					if(bedtypefromtable.size()>1){
						System.out.println(bedtypefromtable.get(0).getId());
					}
					bedtype.setId(bedtypefromtable.get(0).getId());
					Server.getInstance().getHotelService().updateBedtypeIgnoreNull(bedtype);
					res=(int)bedtype.getId();
					System.out.println("更新床型:"+typename);
				}else{
					System.out.println(typeid);
					bedtype=Server.getInstance().getHotelService().createBedtype(bedtype);
					res=(int)bedtype.getId();
					System.out.println("插入床型:"+typename);
				}
			}
			Element resultcode = result.getChild("RETURN_CODE");
			String resultco = resultcode.getText();
			String errormessage = result.getChildText("ERROR_MESSAGE");
			if (errormessage != null || !"".equals(errormessage)) {
				WriteLog.write("床型新增数据", "错误信息:" + resultco + "," + errormessage);
			}
			System.out.println("床型数据插入完成……");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}
}
