package com.ccervice.huamin.update;

import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.*;

import com.ccservice.b2b2c.base.bedtype.Bedtype;
import com.ccservice.b2b2c.base.city.City;
import com.ccservice.b2b2c.base.country.Country;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.b2b2c.base.province.Province;
import com.ccservice.b2b2c.base.region.Region;
import com.ccservice.b2b2c.base.roomtype.Roomtype;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.elong.hoteldb.HanziToPinyin;
import com.ccservice.inter.server.Server;

/*
华闽国际酒店录入数据
*/

public class HuaminInternationalHotelDB {
	
	@SuppressWarnings("unchecked")
	public void getHmHotelInfo(){
		String huaminInternationalHotelInfoUpdateBaseFlag = PropertyUtil.getValue("huaminInternationalHotelInfoUpdateBaseFlag");
		if("1".equals(huaminInternationalHotelInfoUpdateBaseFlag)){
			//获取国际酒店
			String curl = HMRequestUitl.getHMRequestUrlHeader() + "&api=qcountry&p_lang=SIM";
			String xmlstr = Util.getStr(curl);
			if(xmlstr!=null && xmlstr.contains("?<?xml version=\"1.0\" encoding=\"UTF-8\"?>")){
				xmlstr = xmlstr.replace("?<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			}
			SAXBuilder build = new SAXBuilder();
			Document document;
			try {
				document = build.build(new StringReader(xmlstr));
				Element root = document.getRootElement();
				Element result = root.getChild("XML_RESULT");
				List<Element> countries = result.getChildren("COUNTRIES");
				for(Element ele:countries){
					String code = ele.getChildText("COUNTRY");//华闽国家
					String name = ele.getChildText("COUNTRYNAME");
					if(code!=null && code.trim().length()>0 && !"CHN".equals(code) && name!=null && !"".equals(name.trim()) && !"其它".equals(name)){
						System.out.println("开始获取国家"+name+"的酒店...");
						getHotel(code, "" , "SIM");
					}
				}
			}catch (Exception e) {
				
			}
			//获取房型、床型
			getHotelCatType();
		}
		//价格
		String huaminCatchPriceMonth = PropertyUtil.getValue("huaminCatchPriceMonth");
		System.out.println("开始更新"+huaminCatchPriceMonth+"个月的价格");
		new HuaminHotelDb().updateInternationalOneMonthPrice();
		System.out.println("结束更新"+huaminCatchPriceMonth+"个月的价格");
	}
	
	//获取酒店
	@SuppressWarnings("unchecked")
	private void getHotel(String countrycode, String hotelcode , String languagetype){
		try {
			String hurl = HMRequestUitl.getHMRequestUrlHeader() + "&api=qhotel&p_lang="+languagetype+"&p_country="+countrycode+"&p_hotel="+hotelcode;
			System.out.println(hurl);
			String returnStr = Util.getStr(hurl);
			if(returnStr!=null && returnStr.contains("?<?xml version=\"1.0\" encoding=\"UTF-8\"?>")){
				returnStr = returnStr.replace("?<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			}
			Document doc = new SAXBuilder().build(new StringReader(returnStr));
			Element hotelroot = doc.getRootElement();
			Element hotelresult = hotelroot.getChild("XML_RESULT");
			List<Element> hotels = hotelresult.getChildren("HOTELS");
			if(hotels!=null && hotels.size()>0){
				for (Element hotel : hotels) {
					String countryname = hotel.getChildText("COUNTRYNAME");
					if (countryname==null || countryname.trim().equals("") || countryname.trim().equals("其它")) {
						continue;
					}
					List<Country> countryfromtable = 
						Server.getInstance().getInterHotelService().findAllCountry(" WHERE " + 
								Country.COL_HUAMINCODE + "='" + countrycode.trim() + "' or " + 
									Country.COL_zhname + "='" + countryname + "'", "", -1,0);
					Country tempc = new Country();
					tempc.setZhname(countryname);
					tempc.setName(HanziToPinyin.getPinYin(countryname));
					tempc.setHuamincode(countrycode.trim());
					if(countryfromtable==null || countryfromtable.size()==0){
						tempc = Server.getInstance().getInterHotelService().createCountry(tempc);
					}else if(countryfromtable.size()==1){
						tempc.setId(countryfromtable.get(0).getId());
						if(!countrycode.trim().equals(countryfromtable.get(0).getHuamincode())){
							Server.getInstance().getInterHotelService().updateCountryIgnoreNull(tempc);
						}
					}else{
						for(Country c:countryfromtable){
							if(countrycode.trim().equals(c.getHuamincode())){
								tempc = c;
								break;
							}
						}
						//编码未匹配上取名称
						if(tempc.getId()==0){
							tempc.setId(countryfromtable.get(0).getId());
							Server.getInstance().getInterHotelService().updateCountryIgnoreNull(tempc);
						}
					}
					Hotel hoteltemp = new Hotel();
					// 1--表示来自于艺龙,3--表示来自于华闽
					hoteltemp.setSourcetype(3l);
					hoteltemp.setCountryid(tempc.getId());
					// 0--表示暂时不可用
					// 2--表示艺龙那边没有提供给我们房型
					// 3--表示可用
					hoteltemp.setState(3);
					hoteltemp.setStatedesc("华闽预付酒店");
					// 1--表示国内,2--表示国外
					hoteltemp.setType(2);
					// 预付-2 现付-1
					hoteltemp.setPaytype(2l);
					//华闽酒店编码
					String huamincode = hotel.getChildText("HOTEL");
					System.out.println("华闽酒店标识：" + huamincode);
					hoteltemp.setHotelcode(huamincode.trim());
					String hotelname = hotel.getChildText("HOTELNAME");
					System.out.println("开始导入：" + hotelname);
					hoteltemp.setName(hotelname);
					if (hotelname==null || "".equals(hotelname.trim()) || 
							",".equals(hotelname.trim()) || ".".equals(hotelname.trim()) || 
								"nil".equals(hotelname.trim().toLowerCase())){
						hoteltemp.setState(0);//表示暂时不可用
					}
					String grade = hotel.getChildText("GRADE");
					try {
						if (grade.equals("N") || grade.equals("B")) {
							hoteltemp.setStar(1);
						} else {
							hoteltemp.setStar(Integer.parseInt(grade.trim()));
						}
					} catch (Exception e) {
						WriteLog.write("星级转换出错了", "错误信息:" + hotelname + "," + e.getMessage());
					}
					String address = hotel.getChildText("ADDRESS");
					hoteltemp.setAddress(address);
					//省份
					String areacode = hotel.getChildText("AREA");
					if("CHN".equals(countrycode) || "HKG".equals(countrycode) || "MFM".equals(countrycode) || "TWN".equals(countrycode)){//内地、港、澳、台
						List<Province> provincefromtable = Server.getInstance().getHotelService().findAllProvince(" WHERE " + Province.COL_type + "=1 AND " + Province.COL_HUAMINCODE + "='" + areacode + "'", "", -1, 0);
						if(provincefromtable!=null && provincefromtable.size()>0){
							hoteltemp.setProvinceid(provincefromtable.get(0).getId());
						}
					}
					//城市
					String cityid = hotel.getChildText("CITY");
					String cityname = hotel.getChildText("CITYNAME");
					if(cityid==null || "".equals(cityid.trim()) || cityname==null || "".equals(cityname.trim())){
						continue;
					}
					City city = new City();
					city.setHuamincode(cityid.trim());
					if("CHN".equals(countrycode) || "HKG".equals(countrycode) || "MFM".equals(countrycode) || "TWN".equals(countrycode)){//内地、港、澳、台
						city.setType(1l);
					}else{
						city.setType(4l);//4 -- 国际城市（除内地、港、澳、台城市）
					}
					city.setLanguage(0);
					city.setProvinceid(hoteltemp.getProvinceid());
					city.setCountryid(hoteltemp.getCountryid());
					List<City> cityfromtable = 
						Server.getInstance().getHotelService().findAllCity(" WHERE " + 
								City.COL_type + "="+city.getType()+" AND (" + City.COL_HUAMINCODE + "='"+ cityid.trim() + "' or " + 
									City.COL_name + "='" + cityname + "')", "", -1, 0);
					if(cityfromtable==null || cityfromtable.size()==0){
						city.setName(cityname);
						city.setEnname(HanziToPinyin.getPinYin(city.getName()));
						city.setSname(city.getEnname());
						city = Server.getInstance().getHotelService().createCity(city);
						city.setSort(Integer.parseInt(city.getId()+""));
						Server.getInstance().getHotelService().updateCityIgnoreNull(city);
					}else{
						City tempCity = new City();
						if(cityfromtable.size()==1){
							tempCity = cityfromtable.get(0);							
						}else{
							for(City c:cityfromtable){
								if(cityid.trim().equals(c.getHuamincode())){
									tempCity = c;
									break;
								}
							}
							if(tempCity.getId()==0) tempCity = cityfromtable.get(0);
						}
						city.setId(tempCity.getId());
						Server.getInstance().getHotelService().updateCityIgnoreNull(city);
					}
					hoteltemp.setCityid(city.getId());
					//区域
					String regioncode = hotel.getChildText("DIST");
					String regionname = hotel.getChildText("DISTNAME");
					if(regioncode!=null && !"".equals(regioncode.trim()) && 
							regionname!=null && !"".equals(regionname.trim()) && !"其它".equals(regionname.trim())){
						List<Region> regionfromtable = 
							Server.getInstance().getHotelService().findAllRegion(" WHERE " + 
									Region.COL_HUAMINCODE + "='" + regioncode.trim() + "' and C_CITYID=" + hoteltemp.getCityid(),"", -1, 0);
						if (regionfromtable!=null && regionfromtable.size() > 0) {
							hoteltemp.setRegionid1(regionfromtable.get(0).getId());
						}else {
							Region region = new Region();
							region.setName(regionname);
							region.setCityid(hoteltemp.getCityid());
							region.setType("1");//商业区
							region.setLanguage(0);
							region.setHuamincode(regioncode.trim());
							region.setCountryid(hoteltemp.getCountryid());
							//region = Server.getInstance().getHotelService().createRegion(region);
							region.setUcode(region.getId());
							//Server.getInstance().getHotelService().updateRegionIgnoreNull(region);
							//hoteltemp.setRegionid1(region.getId());
						}
					}
					//经、纬度
					String lat = hotel.getChildText("LATITUDE");
					System.out.println("lat:" + lat);
					String lon = hotel.getChildText("LONGITUDE");
					System.out.println("lon:" + lon);
					try {
						if (lat != null && !"".equals(lat)) {
							hoteltemp.setLat(Double.parseDouble(lat.trim()));
						}
						if (lon != null && !"".equals(lon)) {
							hoteltemp.setLng(Double.parseDouble(lon.trim()));
						}
					} catch (Exception ex) {
						WriteLog.write("酒店新增数据", "经纬度错误信息:" + hotelname + ","
								+ lat + "," + lon + ":" + ex.getMessage());
					}
					String tel = hotel.getChildText("TEL");
					hoteltemp.setTortell(tel);
					String fax = hotel.getChildText("FAX");
					hoteltemp.setFax1(fax);
					String hoteldesc = hotel.getChildText("HOTELDESC");
					hoteltemp.setDescription(hoteldesc);
					
					SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					hoteltemp.setLastupdatetime(sdf.format(new Date(System.currentTimeMillis())));
					
					List<Hotel> hotelfromtable = Server.getInstance().getHotelService().findAllHotel(
													" WHERE " + Hotel.COL_hotelcode + "='" + hoteltemp.getHotelcode() + 
														"' AND C_SOURCETYPE = 3", "", -1,0);
					if (hotelfromtable!=null && hotelfromtable.size() > 0) {
						hoteltemp.setId(hotelfromtable.get(0).getId());
						Server.getInstance().getHotelService().updateHotelIgnoreNull(hoteltemp);
						System.out.println("酒店更新成功!~~~~~~~~~~~~~~~~");
					} else {
						hoteltemp = Server.getInstance().getHotelService().createHotel(hoteltemp);
						System.out.println("酒店录入成功!~~~~~~~~~~~~~~~~");
					}
					if ("SIM".equals(languagetype) && (hotelname==null || "".equals(hotelname.trim()) || 
							",".equals(hotelname.trim()) || ".".equals(hotelname.trim()) || 
								"nil".equals(hotelname.trim().toLowerCase()))) {
						getHotel(countrycode , huamincode , "ENG");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	private static void getHotelCatType(){
		List<Hotel> hotelList = Server.getInstance().getHotelService().findAllHotel("where C_SOURCETYPE=3 AND C_TYPE=2 and C_HOTELCODE is not null", "order by c_cityid", -1,0);
		int i = hotelList.size();
		for (Hotel h : hotelList) {
			System.out.println("剩余酒店数量：" + i--);
			String url = HMRequestUitl.getHMRequestUrlHeader() + "&api=qhotelcattype&p_lang=SIM&p_hotel=" + h.getHotelcode();
			System.out.println("[查询华闽酒店相关的房型床型数据]url:" + url);
			String xmlstr = Util.getStr(url);
			if(xmlstr!=null && xmlstr.contains("?<?xml version=\"1.0\" encoding=\"UTF-8\"?>")){
				xmlstr = xmlstr.replace("?<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			}
			//解析
			SAXBuilder build = new SAXBuilder();
			Document document;
			try {
				document = build.build(new StringReader(xmlstr));
				Element root = document.getRootElement();
				Element result = root.getChild("XML_RESULT");
				Element hotels = result.getChild("HOTELS");
				if (result.getChildren().size() > 2) {
					List<Element> hotel = hotels.getChildren("HOTEL");
					for (Element hotelbean : hotel) {
						String hotelcode = hotelbean.getChildText("HOTEL");// 酒店代碼
						if(hotelcode==null || !hotelcode.equals(h.getHotelcode())){
							continue;
						}
						Element rooms = hotelbean.getChild("ROOMS");
						List<Element> room = rooms.getChildren("ROOM");
						for (Element roombean : room) {
							String cat = roombean.getChildText("CAT");
							String catname = roombean.getChildText("CATNAME");
							String type = roombean.getChildText("TYPE");
							String typename = roombean.getChildText("TYPENAME");
							String wifi=roombean.getChildText("WIFI");//无线
							String boardband=roombean.getChildText("BOARDBAND");//宽带上网
							String bedtypeid = null;
							if (type != null && !"".equals(type)) {
								List<Bedtype> bedtypeList = Server.getInstance().getHotelService().findAllBedtype("where C_TYPE='" + type + "'", "",-1, 0);
								Bedtype bedtype;
								boolean falgBedtype = true;
								if (bedtypeList != null && bedtypeList.size() > 0) {
									bedtype = bedtypeList.get(0);
									falgBedtype = false;
									bedtypeid = bedtype.getId() + "";
								} else {
									bedtype = new Bedtype();
									bedtype.setId(0L);
									bedtype.setType(type);
								}
								if (typename != null && !"".equals(typename)) {
									bedtype.setTypename(typename);
								}
								// bedtype.setMaxguest(0L);//最多入住人数 默认为0
								if (falgBedtype) {
									System.out.println("[录入华闽酒店相关的房型床型数据]向C_BEDTYPE表添加一条数据");
									Bedtype bt = Server.getInstance().getHotelService().createBedtype(bedtype);
									System.out.println("[录入华闽酒店相关的房型床型数据]向C_BEDTYPE表添加一条数据,添加成功,ID:"+ bt.getId());
									bedtypeid = bt.getId() + "";
								} else {
									System.out.println("[录入华闽酒店相关的房型床型数据]更改C_BEDTYPE表一条数据");
									Server.getInstance().getHotelService().updateBedtypeIgnoreNull(bedtype);
									System.out.println("[录入华闽酒店相关的房型床型数据]更改C_BEDTYPE表一条数据,更改成功");
								}
							}

							if (cat != null && !"".equals(cat)) {
								List<Roomtype> roomtypeList = Server.getInstance().getHotelService().findAllRoomtype("where C_ROOMCODE='" + cat+ "' and  C_BED='"+ bedtypeid+ "' and C_HOTELID="+ h.getId(), "", -1, 0);
								Roomtype rt;
								boolean falgRoomtype = true;
								if (roomtypeList != null && roomtypeList.size() > 0) {
									rt = roomtypeList.get(0);
									falgRoomtype = false;
								} else {
									rt = new Roomtype();
									rt.setId(0L);
									rt.setRoomcode(cat);
									rt.setState(1);
									rt.setLanguage(0);
								}
								if (catname != null && !"".equals(catname)) {
									rt.setName(catname);
								}
								if(wifi!=null&&wifi.length()>0&&!"".equals(wifi)){
									rt.setWidedesc(wifi);
								}
								if(boardband!=null&&!"".equals(boardband)){
									if(rt.getWidedesc()!=null){
										rt.setWidedesc(wifi+","+boardband);
									}else{
										rt.setWidedesc(boardband);
									}
								}
								rt.setHotelid(Long.valueOf(h.getId()));
								if (bedtypeid != null) {
									rt.setBed(Integer.valueOf(bedtypeid));
								}

								if (falgRoomtype) {
									System.out.println("[录入华闽酒店相关的房型床型数据]向C_ROOMTYPE表添加一条数据");
									Roomtype bt = Server.getInstance().getHotelService().createRoomtype(rt);
									System.out.println("[录入华闽酒店相关的房型床型数据]向C_ROOMTYPE表添加一条数据,添加成功,ID:"
													+ bt.getId());
								} else {
									System.out.println("[录入华闽酒店相关的房型床型数据]更改C_ROOMTYPE表一条数据");
									Server.getInstance().getHotelService().updateRoomtypeIgnoreNull(rt);
									System.out.println("[录入华闽酒店相关的房型床型数据]更改C_ROOMTYPE表一条数据,更改成功");
								}

							}
							System.out.println(cat + "-" + catname + "-" + type+ "-" + typename);
						}
					}
				}

				Element resultcode = result.getChild("RETURN_CODE");
				String resultco = resultcode.getText();
				String errormessage = result.getChildText("ERROR_MESSAGE");
				if (errormessage != null || !"".equals(errormessage)) {
					WriteLog.write("查询华闽酒店相关的房型床型数据", "错误信息:" + resultco + "," + errormessage);
				}
			} catch (Exception e) {
				WriteLog.write("查询华闽酒店相关的房型床型数据", "返回数据格式有问题,错误数据:" + xmlstr);
				e.printStackTrace();
			}
		}	
	}
}
