package com.ccervice.huamin.update;

import java.io.File;
import java.io.FileFilter;

import com.ccservice.b2b2c.base.util.Util;

/**
 * 
 * @author wzc 复制华闽传送的变价文件，以及房态变化文件，删除老的传送方式文件
 */
public class FileCopyAction {

	public void exeMethod() {
		File files = new File("D:\\酒店价格");
		if (files.exists()) {
			File[] delfile = files.listFiles(new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					String filename = pathname.getName();
					if (filename.indexOf("HMC_HC_") == 0
							|| filename.indexOf("HMC_ALLOTUPDATE_") == 0) {
						return true;
					}
					return false;
				}
			});
			if (delfile.length > 0) {
				for (File file : delfile) {
					System.out.println("删除文件=====" + file.getName());
					file.delete();
				}
			}
			File[] file = files.listFiles(new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					String filename = pathname.getName();
					if (filename.indexOf("HMC_PUSHRATE_CN00839_") == 0
							|| filename.indexOf("HMC_PUSHALLOT_") == 0
							||filename.indexOf("HMC_PUSHBLOCKHT_CN00839") == 0) {
						return true;
					}
					return false;
				}
			});
			if (file.length > 0) {
				File filequnar = new File("D:\\酒店价格\\qunar");
				if (!filequnar.exists()) {
					filequnar.mkdirs();
				}
				File fileydx = new File("D:\\酒店价格\\ydx");
				if (!fileydx.exists()) {
					fileydx.mkdirs();
				}
				for (File file2 : file) {
					System.out.println("复制："+file2.getName());
					Util.copyfile(file2, new File(filequnar, file2.getName()));
					Util.copyfile(file2, new File(fileydx, file2.getName()));
					file2.delete();
				}
			}
		}
	}
}
