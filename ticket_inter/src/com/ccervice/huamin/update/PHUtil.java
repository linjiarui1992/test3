package com.ccervice.huamin.update;

import java.net.URL;
import java.io.InputStream;
import java.io.BufferedReader;
import java.net.URLConnection;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class PHUtil {

    /**
     * java.net实现 HTTP POST方法提交
     */

    public static StringBuffer submitPost(String url, String paramContent) {
        StringBuffer responseMessage = null;
        URLConnection connection = null;
        URL reqUrl = null;
        OutputStreamWriter out = null;
        InputStream in = null;
        BufferedReader br = null;
        String param = paramContent;
        try {
            responseMessage = new StringBuffer();
            reqUrl = new java.net.URL(url);
            connection = reqUrl.openConnection();
            connection.setDoOutput(true);
            out = new OutputStreamWriter(connection.getOutputStream());
            out.write(param);
            out.flush();
            int charCount = -1;
            in = connection.getInputStream();
            br = new BufferedReader(new InputStreamReader(in, "UTF-8"));
            while ((charCount = br.read()) != -1) {
                responseMessage.append((char) charCount);
            }
        }
        catch (Exception ex) {
        }
        finally {
            try {
                in.close();
                out.close();
                br.close();
            }
            catch (Exception e) {
                System.out.println("paramContent=" + paramContent + "|err=" + e);
            }
        }
        return responseMessage;
    }

    /**
     * java.net实现 HTTP或HTTPs GET方法提交
     */
    public static String submitGet(String strUrl) {
        URLConnection connection = null;
        BufferedReader reader = null;
        String str = null;
        try {
            URL url = new URL(strUrl);
            connection = url.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(false);
            //取得输入流，并使用Reader读取
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String lines;
            StringBuffer linebuff = new StringBuffer("");
            while ((lines = reader.readLine()) != null) {
                linebuff.append(lines);
            }
            str = linebuff.toString();
        }
        catch (Exception e) {
        }
        finally {
            try {
                reader.close();
            }
            catch (Exception e) {
            }
        }
        return str;
    }
}