package com.ccervice.huamin.update;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * 
 * @author wzc 更新易订行变价，房态数据
 */
public class HuaMinFileUpdateJob implements Job {

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		System.out.println("开始更新网站房价房态数据...");
		new HuaMinFileUpdate().test();
		System.out.println("结束更新网站房价房态数据...");
	}

}
