package com.ccervice.db.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mongodb.DBObject;

/**
 * @author 作者 guozhengju:
 * @version 创建时间：2015年8月25日 下午3:14:36 类说明
 */
public class MongoLogicCustomerUser {

    private String collection = "CustomerUser";

    /**
     * Point 根据始发到达站及时间拼出的Key来获取Mongo中的价格
     * 
     * @param key
     *            (始发站三字码+到达站三字码+时间yyyy-MM-dd)
     * @param id
     * @return
     */
    public List<DBObject> SelectPrice(String key, String value) {
        Map<String, Object> query = new HashMap<String, Object>();
        query.put(key, value);
        List<DBObject> listTrainPrice = null;
        try {
            listTrainPrice = MongoHelper.getInstance().find(collection, query);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return listTrainPrice;
    }

    public static Date formatTo(String date) {
        Date dates = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            dates = sdf.parse(date + " 08:00:00");
        }
        catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return dates;
    }
}
