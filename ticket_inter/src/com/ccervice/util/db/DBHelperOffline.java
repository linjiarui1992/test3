package com.ccervice.util.db;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.druid.pool.DruidDataSource;
import com.ccservice.Util.PropertyUtil;

public class DBHelperOffline {

    private static Connection GetCONN() {
        Connection result = null;
        //        net.sourceforge.jtds.jdbc.Driver
        try {
            result = cpds.getConnection();
        }
        catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return result;
    };

    private static DruidDataSource cpds = null;

    //取得连接
    private static boolean GetPool() {
        if (cpds != null)
            return true;
        try {
            cpds = new DruidDataSource();
            String driverClass = PropertyUtil.getValue("druid.driver", "Train.offline.database-config.properties");
            String sDBUrl = PropertyUtil.getValue("druid.url", "Train.offline.database-config.properties");
            String sUserName = PropertyUtil.getValue("druid.username", "Train.offline.database-config.properties");
            String sPassword = PropertyUtil.getValue("druid.password", "Train.offline.database-config.properties");
            
            cpds.setDriverClassName(driverClass);
            cpds.setUrl(sDBUrl);
            cpds.setUsername(sUserName);
            cpds.setPassword(sPassword);
        }
        catch (Exception ex) {
            return false;
        }
        return true;
    }

    //关闭连接
    private static void CloseConn() {
        try {
            cpds.close();
            cpds = null;
        }
        catch (Exception ex) {
            //          System.out.println(ex.getMessage());
            cpds = null;
        }
    }

    private static void CloseConn(Connection myCon) {
        try {
            myCon.close();
            myCon = null;
        }
        catch (Exception ex) {
            //          System.out.println(ex.getMessage());
            myCon = null;
        }
    }

    //测试连接
    public static boolean TestConn() {
        if (!GetPool())
            return false;
        CloseConn();
        return true;
    }

    public ResultSet GetResultSet(String sSQL, Object[] objParams) {
        GetPool();
        ResultSet rs = null;
        Connection myCon = GetCONN();
        try {

            PreparedStatement ps = myCon.prepareStatement(sSQL);
            if (objParams != null) {
                for (int i = 0; i < objParams.length; i++) {
                    ps.setObject(i + 1, objParams[i]);
                }
            }
            rs = ps.executeQuery();
        }
        catch (Exception ex) {
            //          System.out.println(ex.getMessage());
            //CloseConn();
        }
        finally {
            CloseConn(myCon);
        }
        return rs;
    }

    public static Object GetSingle(String sSQL, Object... objParams) {
        GetPool();
        Connection myCon = GetCONN();
        try {
            PreparedStatement ps = myCon.prepareStatement(sSQL);
            if (objParams != null) {
                for (int i = 0; i < objParams.length; i++) {
                    ps.setObject(i + 1, objParams[i]);
                }
            }
            ResultSet rs = ps.executeQuery();
            if (rs.next())
                return rs.getString(1);//索引从1开始
        }
        catch (Exception ex) {
            //          System.out.println(ex.getMessage());
        }
        finally {
            CloseConn(myCon);
        }
        return null;
    }

    public static int GetMaxID(String sTableName, String sKeyField) {
        GetPool();
        Connection myCon = GetCONN();
        try {

            String sSQL = "select isnull(max([" + sKeyField + "]),0) as MaxID from [" + sTableName + "]";
            PreparedStatement ps = myCon.prepareStatement(sSQL);
            ResultSet rs = ps.executeQuery();
            if (rs.next())
                return Integer.parseInt(rs.getString(1));//索引从1开始
        }
        catch (Exception ex) {
            //          System.out.println(ex.getMessage());
        }
        finally {
            CloseConn(myCon);
        }
        return 0;
    }

    /**
    @Description: TODO(获取结果集共有多少条  from)  <BR>
      * @Title: getCount  <BR>
      * @author  Anki  <BR>
      * @param @param sql  
      * @param @param params
      * @param @throws Exception  <BR>
      * @return int    返回类型  <BR>
      */
    public static int getCount(String sql, Object... params) throws Exception {
        StringBuffer buffer = new StringBuffer();
        buffer.append("SELECT COUNT(0) FROM  ");
        buffer.append(sql);
        GetPool();
        Connection myCon = GetCONN();
        int count = 0;
        ResultSet rs = null;
        try {
            PreparedStatement ps = myCon.prepareStatement(buffer.toString());
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    ps.setObject(i + 1, params[i]);
                }
            }
            rs = ps.executeQuery();
            if (rs.next()) {
                count = rs.getInt(1);
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
        finally {
            if (rs != null)
                rs.close();
            CloseConn(myCon);
        }
        return count;
    }

    public static DataTable GetDataTable(String sSQL, Object... objParams) throws Exception {
        GetPool();
        DataTable dt = null;
        Connection myCon = GetCONN();
        try {
            PreparedStatement ps = myCon.prepareStatement(sSQL);
            if (objParams != null) {
                for (int i = 0; i < objParams.length; i++) {
                    ps.setObject(i + 1, objParams[i]);
                }
            }
            ResultSet rs = ps.executeQuery();
            ResultSetMetaData rsmd = rs.getMetaData();

            List<DataRow> row = new ArrayList<DataRow>(); //表所有行集合

            List<DataColumn> col = null; //行所有列集合
            DataRow r = null;// 单独一行
            DataColumn c = null;//单独一列
            String columnName;
            Object value;
            int iRowCount = 0;
            while (rs.next())//开始循环读取，每次往表中插入一行记录
            {
                iRowCount++;
                col = new ArrayList<DataColumn>();//初始化列集合
                for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                    columnName = rsmd.getColumnName(i);
                    value = rs.getObject(columnName);
                    c = new DataColumn(columnName, value);//初始化单元列
                    col.add(c); //将列信息加入到列集合
                }
                r = new DataRow(col);//初始化单元行
                row.add(r);//将行信息加入到行集合
            }
            dt = new DataTable(row);
            dt.RowCount = iRowCount;
            dt.ColumnCount = rsmd.getColumnCount();
        }
        catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
            //          System.out.println(ex.getMessage());
        }
        finally {
            CloseConn(myCon);
        }
        return dt;
    }

    public static int UpdateData(String sSQL) {
        GetPool();
        int iResult = 0;
        Connection myCon = GetCONN();
        try {
            Statement st = myCon.createStatement();
            iResult = st.executeUpdate(sSQL);
        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());
            return -1;
        }
        finally {
            CloseConn(myCon);
        }
        return iResult;
    }

    /**
      * 更新数据
      * @param sSQL
      * @return
      */
    public static int insertSql(String sSQL) {
        GetPool();
        Connection myCon = GetCONN();
        int id = 0;
        try {
            Statement st = myCon.createStatement();
            int row = st.executeUpdate(sSQL, Statement.RETURN_GENERATED_KEYS);
            ResultSet rs = st.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getInt(row);
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return id;
        }
        finally {
            CloseConn(myCon);
        }
        return id;
    }

    /**
      * 更新数据
      * @param sSQL
      * @return
      */
    public static boolean executeSql(String sSQL) {
        GetPool();
        Connection myCon = GetCONN();
        boolean iResult = false;
        try {
            Statement st = myCon.createStatement();
            iResult = st.execute(sSQL);
        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        finally {
            CloseConn(myCon);
        }
        return !iResult;
    }

    /**
     * 通过执行存储过程 返回执行结构
     * @param procName 存储过程名称 eg  sp_Insert_Bus_Test(?,?) or sp_Insert_Bus_Test()
     * @param objectParams  参数集合
     * @return
     */
    public static DataTable getResultByProc(String procName, Object[] objectParams) {
        GetPool();
        DataTable dt = null;
        Connection _CONN = GetCONN();
        ResultSet rs = null;
        try {
            CallableStatement callsta = _CONN.prepareCall("{call " + procName + "}");
            if (objectParams != null) {
                for (int i = 0; i < objectParams.length; i++) {
                    callsta.setObject(i + 1, objectParams[i]);
                }
            }
            // 循环输出调用存储过程的记录结果
            int rsNum = 0;//统计结果集的数量
            int updateCount = -1;
            boolean flag = callsta.execute();// 这个而尔值只说明第一个返回内容是更新计数还是结果集。
            List<DataRow> row = new ArrayList<DataRow>(); //表所有行集合
            List<DataColumn> col = null; //行所有列集合
            int iRowCount = 0;
            int ColumnCount = 0;
            do {
                updateCount = callsta.getUpdateCount();
                if (updateCount != -1) {// 说明当前行是一个更新计数
                    // 处理.
                    callsta.getMoreResults();
                    continue;// 已经是更新计数了,处理完成后应该移动到下一行
                    // 不再判断是否是ResultSet
                }
                rs = callsta.getResultSet();
                if (rs != null) {// 如果到了这里,说明updateCount == -1
                    // 处理rs
                    rsNum++;
                    if (rs != null) {
                        iRowCount++;
                        ResultSetMetaData rsmd = rs.getMetaData(); // 获取字段名
                        int numberOfColumns = rsmd.getColumnCount(); // 获取字段数
                        ColumnCount = numberOfColumns;
                        int i = 0;
                        Object value;
                        String columnName;
                        DataRow r = null;// 单独一行
                        DataColumn c = null;//单独一列
                        while (rs.next()) { // 将查询结果取出
                            col = new ArrayList<DataColumn>();
                            for (i = 1; i <= numberOfColumns; i++) {
                                // System.out.println(rs.getInt("总页数"));
                                columnName = rsmd.getColumnName(i);
                                value = rs.getObject(columnName);
                                c = new DataColumn(columnName, value);//初始化单元列
                                col.add(c); //将列信息加入到列集合
                            }
                            r = new DataRow(col);//初始化单元行
                            row.add(r);//将行信息加入到行集合
                        }
                        rs.close();
                    }
                    callsta.getMoreResults();
                    continue;
                    // 是结果集,处理完成后应该移动到下一行
                }
                // 如果到了这里,说明updateCount == -1 && rs == null,什么也没的了
            }
            while (!(updateCount == -1 && rs == null));
            dt = new DataTable(row);
            dt.RowCount = iRowCount;
            dt.ColumnCount = ColumnCount;
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        finally {
            CloseConn(_CONN);
        }
        return dt;
    }
}