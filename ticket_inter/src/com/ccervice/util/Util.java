package com.ccervice.util;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.WriteLog;

/**
 * 此类为公共工具类需要的可以往这里面添加
 * 所谓工具类是大家都可以用的,方法必须是公用的并且仅仅依赖JDK
 * 
 * @time 2014年9月2日 上午10:42:09
 * @author chendong
 */
public class Util {

    public static final DecimalFormat decimalFormat1 = new DecimalFormat("##0.0");

    public static void main(String[] args) {

        String pnr = "JV802V";
        String rt = "1.艾自英 2.陈洪 3.任明杰 4.唐明周 5.唐萍 6.唐琼 JV802V  7.  8L9871 Y   TU30SEP  YIHNGB HK6   1720 1850          E ";
        //        Pattern p = Pattern.compile(" [A-Z][A-Z][1-9] ");
        //        Matcher m = p.matcher(rt);
        //        if (m.find()) {
        //            System.out.println(m.group(0).trim());
        //        }
        //        System.out.println(getPnrstatusbyPnrRT(pnr, rt));
        System.out.println(getDiscountPrice(530, 8.8F));
    }

    /**
     * 根据agentid判断是否生成pnr的方法，返回是否可以生成pnr
     * @param agentid
     * @return
     * @author dd 
     */
    public static boolean isCreatepnrBycustomeragentid(long agentid) {
        if (agentid == 2 || agentid == 3 || agentid == 4 || agentid == 5 || agentid == 6 || agentid == 7
                || agentid == 8 || agentid == 9 || agentid == 13) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * 判断外部供应商是否是需要下单到供应, 如果是其他供应商政策，则不生成外部订单
     * 此方法包括今日
     * @param agentid
     * @return
     * @author dd 
     */
    public static boolean isCreateExtOrderBycustomeragentid(long agentid) {
        if (agentid == 2 || agentid == 3 || agentid == 4 || agentid == 5 || agentid == 6 || agentid == 7
                || agentid == 8 || agentid == 9 || agentid == 13 || agentid == 15 || agentid == 16 || agentid == 17) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * 判断外部供应商是否是需要下单到供应, 如果是其他供应商政策，则不生成外部订单
     * 不包括今日的
     * @param agentid
     * @return
     * @author dd 
     */
    public static boolean isCreateExtOrderBycustomeragentidnotJinri(long agentid) {
        if (agentid == 2 || agentid == 3 || agentid == 4 || agentid == 5 || agentid == 7 || agentid == 8
                || agentid == 9 || agentid == 13) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * 此方法用来根据pnr和rt获取到pnr的状态,在下单后获取pnr的状态
     * @param pnr
     * @param rt
     * @return
     * @author dd 
     */
    public static String getPnrstatusbyPnrRT(String pnr, String rt) {
        String pnrstatus = "-1";
        try {
            if (pnr != null && rt != null) {
                String[] PNRArr = rt.split(pnr.toUpperCase().trim());
                String strOtherInfo = PNRArr.length > 1 ? PNRArr[1] : "";
                Pattern pOtherchild = Pattern.compile("\n");
                String[] strOrderArr = pOtherchild.split(strOtherInfo);
                if (strOrderArr.length > 1) {
                    String[] segmentinfos = strOrderArr[1].trim().split(" {1,}");
                    pnrstatus = segmentinfos[4] + segmentinfos[5];
                }
                if ("-1".equals(pnrstatus)) {
                    //                String pnr = "HETM26";
                    //                String rt = "1.黄银强 HN874T 2. ZH9878 G WE20AUG NKGSZX HK1 1655 1915 E --T3 3.PEK/T BJS/T-64540699/BEIJING TIAN QU AIR SERVICE LTD.,CO/CHENYIMIN ABCDEFG 4.REMARK 0816 1448 GUEST 5.TL/1455/20AUG/PEK242 6.SSR FOID ZH HK1 NI421127197601201956/P1 7.SSR FQTV ZH HK1 NKGSZX 9878 G20AUG CA005341682843/P1 8.OSI ZH CTCT13393865805 9.OSI ZH CTCT13439311111 10.RMK CA/MY8STL 11.PEK242";
                    Pattern p = Pattern.compile(" [A-Z][A-Z][1-9] ");
                    Matcher m = p.matcher(rt);
                    if (m.find()) {
                        pnrstatus = m.group(0).trim();
                    }
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return pnrstatus;
    }

    /**
     * 此方法用来根据pnr的rt信息获取到pnr//取得PNR编码
     * @param pnrRT
     * @return
     * @author dd
     */
    public static String getPnrcodebyRT(String pnrRT) {
        String pnr = "-1";
        String regPNR = "[R][T][\\s]{0,}[A-Za-z0-9]{6}";
        Pattern pattPNR = Pattern.compile(regPNR);
        Matcher mPNR = pattPNR.matcher(pnrRT.toUpperCase());
        if (mPNR.find() && mPNR.group().toString().length() > 0) {
            pnr = mPNR.group().toString().substring(2, mPNR.group().toString().length());

        }
        return pnr;
    }

    /**
     * 此方法用来根据pnr的rt信息的pnr前面的名字字符串获取名字的字符串信息
     * @param pnrRT
     * @return 陈栋#王战朝
     * @author dd
     */
    public static String getNameStringbyRT(String pnrRT, int intpassengerNum) {
        String strNMString = "";
        Pattern pPerson = Pattern.compile("[0-9]{1,}[\\.]");
        String[] passenger = pPerson.split(pnrRT.replace("**ELECTRONIC TICKET PNR**", "").replace("<br>", ""));
        for (int i = 1; i < passenger.length; i++) {
            intpassengerNum++;
            strNMString += passenger[i].trim() + "#";
        }
        return strNMString;
    }

    /*
     * 获取当前年月日格式yyyyMMdd
     *  @return
     */
    public static String getDateString() {
        try {
            return (new SimpleDateFormat("yyyyMMdd").format(new Date()));
        }
        catch (Exception e) {
            return "000000";
        }
    }

    /**
     * 根据票面价和y仓价格计算折扣
     * @param yprice
     * @param parprice
     * @param discount
     * @return
     */
    public static float getdiscount(float yprice, float parprice, float discount) {
        try {
            float discount1 = parprice * 10 / yprice;
            return Float.parseFloat(decimalFormat1.format(discount1));
        }
        catch (Exception e) {
            return discount;
        }
    }

    public static String[] pnrrtParsegetchildANDINF(String[] strOrderArr, String strNMString) {
        String strChildName = "";
        String strChildIDNum = "";
        for (int i = 0; i < strOrderArr.length; i++) {
            String t_strOrderArr = strOrderArr[i].trim();
            if ("".equals(t_strOrderArr)) {
                continue;
            }
            String regbaby = "[0-9]{1,}[.][\\s]{0,}[X][N][/][I][N][/][\\w|\\W]{2,}[I][N][F][(][a-zA-Z]{3}[0-9]{2}[)][/][P][0-9]{1,}";
            Pattern pattbaby = Pattern.compile(regbaby);

            Matcher mchildname = pattbaby.matcher(t_strOrderArr);
            while (mchildname.find()) {
                strChildName = mchildname.group().toString();
                if (strChildName.trim().length() > 0) {
                    String[] strtempbaby = strChildName.trim().split("[(]");
                    if (strtempbaby.length > 0) {
                        // String
                        // strnewname=strtempbaby[0].split("/")[strtempbaby[0].split("/").length-1];
                        // if(strNMString.indexOf(strnewname)<0)
                        // {
                        // strNMString+=strnewname;
                        // }
                        strChildIDNum = strtempbaby[1].split("[)]")[0];
                    }
                }
            }
            // OSI YY 1INF ZHAOYIXUAN/P1
            String regbaby1 = "[0-9]{1,}[.][\\s]{0,}[O][S][I][\\s]{0,}[Y][Y][\\s]{0,}[0-9]{1,}[\\s]{0,}[I][N][F][\\s]{0,}[\\w|\\W]{2,}[/][P][0-9]{1,}";
            Pattern pattbaby1 = Pattern.compile(regbaby1);
            Matcher mchildname1 = pattbaby1.matcher(t_strOrderArr);
            while (mchildname1.find()) {
                strChildName = mchildname1.group().toString();
                if (strChildName.trim().length() > 0) {
                    String[] strtempbaby = strChildName.trim().split("\\s");
                    if (strtempbaby.length >= 4) {
                        String strnewname = strtempbaby[3].split("/")[0];
                        if (strNMString.indexOf(strnewname) < 0) {
                            strNMString += strnewname + " INF";
                        }
                        // strChildIDNum=strtempbaby[1].split("[)]")[0];
                    }
                }
            }
        }
        return new String[] { strChildName, strChildIDNum };
    }

    /**
     * 根据rt信息获取证件号和证件类型
     * @param strOrderArr  String[] { strIDNumbers, strIDTYpe }
     * @return
     */
    public static String[] pnrrtParsegetIDNumbers(String strOrderArr) {
        String strIDNumbers = "";
        String strIDTYpe = "";
        // 身份证
        if (strOrderArr.indexOf("NI") >= 0) {
            Pattern pidnumber = Pattern.compile("NI");
            String[] strIdArr = pidnumber.split(strOrderArr);
            strIDTYpe = "1" + "#";
            if (strIdArr.length == 2) {
                if (strIdArr[1].indexOf("/") >= 0) {
                    String[] strIDNumberdetail = strIdArr[1].split("/");
                    if (strIDNumberdetail.length == 2) {
                        strIDNumbers += strIDNumberdetail[0] + "#";
                    }
                }
            }
            // 护照
        }
        else if (strOrderArr.indexOf("PP") >= 0) {
            Pattern pidnumber1 = Pattern.compile("PP");
            String[] strIdArr1 = pidnumber1.split(strOrderArr);
            strIDTYpe = "3" + "#";
            if (strIdArr1.length == 2) {
                if (strIdArr1[1].indexOf("/") >= 0) {
                    String[] strIDNumberdetail1 = strIdArr1[1].split("/");
                    if (strIDNumberdetail1.length == 2) {
                        strIDNumbers += strIDNumberdetail1[0] + "#";
                    }
                }
            }
            // 其他证件
        }
        else if (strOrderArr.indexOf("ID") >= 0) {
            Pattern pidnumber2 = Pattern.compile("ID");
            String[] strIdArr2 = pidnumber2.split(strOrderArr);
            strIDTYpe = "8" + "#";
            if (strIdArr2.length == 2) {
                if (strIdArr2[1].indexOf("/") >= 0) {
                    String[] strIDNumberdetail2 = strIdArr2[1].split("/");
                    if (strIDNumberdetail2.length == 2) {
                        strIDNumbers += strIDNumberdetail2[0] + "#";
                    }
                }
            }
        }
        return new String[] { strIDNumbers, strIDTYpe };
    }

    /**
     * 取得航站楼信息
     * 
     * @param strSegmentinfo
     *            航线字符串 2. FM9155 E SU19JUN NKGPEK RR1 1105 1250 E --T2
     * @param intType
     *            1=起飞航站楼，2=到达航站楼
     * @return 航站楼
     */
    public static String getHangzhanlouInfo(String strSegmentinfo, int intType) {
        String strReturn = "";
        Pattern pHangZhanLouDetail = Pattern.compile("\\s");
        String[] arrHangZhanLou = pHangZhanLouDetail.split(strSegmentinfo);
        String strHzl = "";
        if (arrHangZhanLou.length > 0) {
            try {
                strHzl = arrHangZhanLou[arrHangZhanLou.length - 1];
                if (strHzl.length() == 4 && intType == 1) {
                    strReturn = strHzl.substring(0, 2);
                    if (strReturn.equals("--")) {
                        strReturn = "";
                    }
                }
                else if (strHzl.length() == 4 && intType == 2) {
                    strReturn = strHzl.substring(2, 4);
                    if (strReturn.equals("--")) {
                        strReturn = "";
                    }
                }
            }
            catch (Exception ex) {

            }
        }
        else {
            strReturn = "";
        }
        return strReturn;
    }

    /**
     * 获取一个随即的密码
     * @param i 几位
     */
    public static String getrandowPWD(int i) {
        return "";
    }

    /**
     * 生成随即密码
     * @param pwd_len 生成的密码的总长度
     * @param type 1数字加字母2纯数字3纯字母
     * @return  密码的字符串
     */
    public static String getRandomNum(int pwd_len, int type) {
        //35是因为数组是从0开始的，26个字母+10个 数字
        final int maxNum = 36;
        int i; //生成的随机数
        int count = 0; //生成的密码的长度
        char[] str = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
                't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
        if (type == 2) {
            str = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
        }
        if (type == 3) {
            str = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
                    'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
        }
        StringBuffer pwd = new StringBuffer("");
        Random r = new Random();
        while (count < pwd_len) {
            //生成随机数，取绝对值，防止 生成负数，
            i = Math.abs(r.nextInt(maxNum)); //生成的数最大为36-1
            if (i >= 0 && i < str.length) {
                pwd.append(str[i]);
                count++;
            }
        }
        return pwd.toString();
    }

    /**
     * 获取IP
     * @return
     */
    public static String getBrowserIp(HttpServletRequest request) {
        String ipString = "";
        if (request.getHeader("X-real-ip") == null) {
            ipString = request.getRemoteAddr();
        }
        else {
            ipString = request.getHeader("X-real-ip");
        }
        return ipString;
    }

    /**
     * 
     * 根据票面价和折扣算出来票面价
     * @param parprice 票面价
     * @param display 折扣 8.8
     * @time 2014年10月20日 下午5:56:11
     * @author chendong
     */
    public static float getDiscountPrice(Integer parprice, float display) {
        parprice = (int) (parprice * display * 0.1);
        parprice = parprice / 10;
        if (parprice % 10 > 5) {
            parprice = parprice * 10 + 10;
        }
        else {
            parprice = parprice * 10;
        }
        return parprice.floatValue();
    }

    // 将字符串格式化成HTML
    public static String formatPNRHTML(String strIn) {
        int bz = strIn.length();
        if (bz > 0) {
            char[] temps = strIn.toCharArray();
            String temp1 = "";
            int count = 0;
            for (int i = 0; i < temps.length; i++) {
                count++;
                if (temps[i] == (char) 27) {
                    temp1 = temp1 + ">>";
                    continue;
                }
                if (temps[i] == (char) 0x1E) {
                    temp1 = temp1 + "&&";
                    continue;
                }
                if (temps[i] == (char) 0x00) {
                    continue;
                }
                temp1 = temp1 + temps[i];

            }
            temp1 = temp1.replace("b", "");
            temp1 = temp1.replace("&&", "");
            return temp1;
        }
        else {
            return "";
        }
    }

    /**
     * 根据pat获取格式化后的价格
     * 0000,-1Y,1010.00,50.00,110.00,1170.00|YOW,900.00,50.00,110.00,1060.00|Y,1010.00,50.00,110.00,1170.00
     * @param patinfo
     * @return
     */
    public static String getFormatpat(String patinfo) {
        String result = "";
        try {
            Pattern patitem = Pattern.compile("\\s{1,}");
            String[] pats = patinfo.split("SFC:");
            int flag = 0;
            for (int j = 0; j < pats.length; j++) {
                if (pats[j].indexOf("FARE:") >= 0) {
                    String[] strpatItem = patitem.split(pats[j]);
                    for (int i = 0; i < strpatItem.length; i++) {
                        if (strpatItem[i].trim().indexOf("FARE:") >= 0) {
                            if (flag > 0) {
                                result += "|";
                            }
                            result += strpatItem[i - 1].trim() + ",";
                            result += strpatItem[i].trim().replace("FARE:CNY", "") + ",";
                            result += strpatItem[i + 1].trim().replace("TAX:CNY", "") + ",";
                            result += strpatItem[i + 2].trim().replace("YQ:CNY", "") + ",";
                            result += strpatItem[i + 3].trim().replace("TOTAL:", "");
                            flag++;
                        }
                        else {
                            continue;
                        }
                    }
                }
                else {
                    continue;
                }
            }
            if (flag > 0) {
                result = "0000," + result;
            }
            result = result.replaceAll("TAX:TEXEMPTCN", "0");
        }
        catch (Exception e) {
        }
        return result;
    }

    /**
     * 获取正确的票价 temp_price 黑屏价格字符串，价格之间逗号分隔 price 页面价格
     * @param temp_price    0000,Q1,580.00,50.00,130.00,760.00|Q,630.00,50.00,130.00,810.00
     * @param price new String[]{'票面价','基建','燃油'}
     * @return
     */
    public static String[] getTruePrice(String temp_price, Float price, Float airportfee, Float fuelfee) {
        String[] prices;
        if (temp_price.equals("-1")) {
            prices = new String[] { price + "", airportfee + "", fuelfee + "" };
            return prices;
        }
        String pat_Price = "";
        String air_Price = "";
        String fuel_Price = "";
        String temp_price1 = parsePatString(temp_price);
        String[] temp_p = temp_price1.split(",");
        String temp_airprice = parseAirFeeString(temp_price);
        String[] temp_p1 = temp_airprice.split(",");
        String temp_fuelprice = parseFuelfeeString(temp_price);
        String[] temp_p2 = temp_fuelprice.split(",");
        WriteLog.write("getFormatpat", "getTruePrice:0:" + temp_price1);
        WriteLog.write("getFormatpat", "getTruePrice:1:" + temp_airprice);
        WriteLog.write("getFormatpat", "getTruePrice:2:" + temp_fuelprice);
        // 最小价格
        String min_price = "";
        // 真价格： 页面价格与黑屏的某个价格相等的价格
        String true_price = "";
        //把pat里面的价格拼成字符串
        String temp_pString = "";
        if (temp_p.length > 1) {
            min_price = temp_p[0];
            for (int m = 0; m < temp_p.length; m++) {
                temp_pString += temp_p[m] + ",";
                // 取得价格中最小的
                if (m > 0 && temp_p[m - 1] != null && temp_p[m] != null) {
                    if (Double.parseDouble(min_price) > Double.parseDouble(temp_p[m])) {
                        min_price = temp_p[m];
                    }
                }
            }
        }
        else if (temp_p.length == 1) {
            pat_Price = temp_p[0];
        }
        //如果有两个以上的价格,并且客人看到的价格包含在pat里面则选择客人看到的价格
        if (temp_p.length > 1 && temp_pString.indexOf(price + "") >= 0) {
            min_price = price + "";
        }
        WriteLog.write("getFormatpat", "getTruePrice:1:min_price:" + min_price + ":price:" + price);
        air_Price = temp_p1[0];
        fuel_Price = temp_p2[0];
        // 如果真价格为空字符串时把黑屏价格中最小的给真价格
        if ("".equals(true_price) && !"".equals(min_price)) {
            true_price = min_price;
        }
        // 当真价格不为空字符串时，把它赋给pat_Price
        if (!"".equals(true_price)) {
            pat_Price = true_price;
        }
        if (fuel_Price.contains("TEXEMPTYQ")) {
            fuel_Price = "0";
        }
        prices = new String[] { pat_Price, air_Price + "", fuel_Price + "" };
        WriteLog.write("getFormatpat", "getTruePrice:1:" + Arrays.toString(prices));
        return prices;
    }

    /**
     * 转化为以逗号分隔的票面价
     * @param temp_price    0000,Q1,580.00,50.00,130.00,760.00|Q,630.00,50.00,130.00,810.00
     * @return  580.00,630.00
     * @author chend 2013-4-16
     */
    public static String parsePatString(String temp_price) {
        String result = "";
        temp_price = temp_price.substring(5);
        String[] pats = temp_price.split("[|]");
        for (int i = 0, length = pats.length; i < length; i++) {
            result += pats[i].split(",")[1];
            if (i < length - 1) {
                result += ",";
            }
        }
        return result;
    }

    /**
     * 转化为以逗号分隔的票面价
     * @param temp_price    0000,Q1,580.00,50.00,130.00,760.00|Q,630.00,50.00,130.00,810.00
     * @return  580.00,630.00
     * @author chend 2013-4-16
     */
    public static String parseAirFeeString(String temp_price) {
        String result = "";
        temp_price = temp_price.substring(5);
        String[] pats = temp_price.split("[|]");
        for (int i = 0, length = pats.length; i < length; i++) {
            result += pats[i].split(",")[2];
            if (i < length - 1) {
                result += ",";
            }
        }
        return result;
    }

    /**
     * 转化为以逗号分隔的票面价
     * @param temp_price    0000,Q1,580.00,50.00,130.00,760.00|Q,630.00,50.00,130.00,810.00
     * @return  580.00,630.00
     * @author chend 2013-4-16
     */
    public static String parseFuelfeeString(String temp_price) {
        String result = "";
        temp_price = temp_price.substring(5);
        String[] pats = temp_price.split("[|]");
        for (int i = 0, length = pats.length; i < length; i++) {
            result += pats[i].split(",")[3];
            if (i < length - 1) {
                result += ",";
            }
        }
        return result;
    }

    /**
     * 
     * @param pat_Price
     * @return
     * @time 2015年6月29日 上午11:13:35
     * @author chendong
     */
    public static Float getFloatByString(String pat_Price) {
        Float value = 0f;
        try {
            value = Float.parseFloat(pat_Price);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * 获取儿童的手续费价格，直接把价格放到票面价里
     * @return
     * @time 2016年3月14日 下午3:38:28
     * @author chendong
     */
    public static Float getChildcurrplatfee() {
        Float Floatchildcurrplatfee = 0F;
        try {
            String childcurrplatfee = PropertyUtil.getValue("childcurrplatfee", "air.properties");
            Floatchildcurrplatfee = Float.parseFloat(childcurrplatfee);
        }
        catch (Exception e) {
        }
        return Floatchildcurrplatfee;
    }

}
