package com.ccervice.inter.file;

import java.util.List;
import java.util.Map;

import com.ccervice.util.DesUtil;
import com.ccervice.util.FileUtil;
import com.ccervice.util.db.DBHelperAccount;
import com.ccervice.util.db.DataTable;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.inter.train.MongoLogic;
import com.ccservice.qunar.util.ExceptionUtil;
import com.mongodb.DBObject;

public class PersonateLog {
    private final static String[] filePathYYMMDDs = { /*"201701/16", "201701/17", "201701/18", "201701/19", "201701/20",
                                                      "201701/21", "201701/22", "201701/23", "201701/24",*/"201701/25",
            "201701/26", "201701/27", "201701/28", "201701/29", "201701/30", "201701/31", "201702/01", "201702/02",
            "201702/03" };

    private final static String[] filePathHours = { "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16",
            "17", "18", "19", "20", "21", "22" };

    public static void open() {
        String userLogName = "userlog";
        for (String filePathYYMMDD : filePathYYMMDDs) {
            for (String filePathHour : filePathHours) {
                if ("201702/03".equals(filePathYYMMDD) && filePathHour.equals("11")) {
                    System.out.println("-----------跑完了-----------");
                    return;
                }
                String realFilePath = "D://" + userLogName + "/" + filePathYYMMDD + "/" + filePathHour;
                String fileName = "CP_产品下单失败最终失败原因.log";
                System.out.println(realFilePath + "/" + fileName);
                List<String> list = new FileUtil().readLog(realFilePath + "/" + fileName);
                System.out.println(list.size());
                readCPLog(list);
            }
        }
    }

    /**
     * 查看CP最终下单失败log
     * 
     * @param list
     * @time 2017年2月3日 上午10:50:50
     * @author fiend
     */
    @SuppressWarnings("rawtypes")
    private static void readCPLog(List<String> list) {
        for (String str : list) {
            try {
                if (str.contains("_身份信息涉嫌被他人冒用") && str.contains("customeruser") && str.contains("orderId")
                        && str.contains("--->")) {
                    String name = str.split("_身份信息涉嫌被他人冒用")[0].split("customeruser")[1].split("--->")[1];
                    String orderId = str.split("orderId:")[1].split("--->")[0];
                    String sql = "select C_IDNUMBER from t_trainpassenger with (nolock) where C_ORDERID= " + orderId
                            + " and C_NAME='" + name + "'";
                    List dbList = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                    Map dbMap = (Map) dbList.get(0);
                    String idNumber = dbMap.get("C_IDNUMBER").toString();
                    try {
                        mongoReFind(idNumber);
                    }
                    catch (Exception e) {
                        ExceptionUtil.writelogByException("PersonateLog_Exception", e, sql + "--->" + idNumber);
                    }
                }
            }
            catch (Exception e) {
                ExceptionUtil.writelogByException("PersonateLog_Exception", e, str);
            }
        }
    }

    /**
     * 证件号获取账号
     * 
     * @param idNumber
     * @throws Exception
     * @time 2017年2月3日 上午11:15:41
     * @author fiend
     */
    @SuppressWarnings("deprecation")
    private static void mongoReFind(String idNumber) throws Exception {
        //        List<DBObject> list = new MongoLogic().findIDAll(idNumber);
        //        for (DBObject dbObject : list) {
        //            if (dbObject.containsKey("SupplyAccount")) {
        //                String loginName = dbObject.get("SupplyAccount").toString();
        //                dbReFind(loginName);
        //            }
        //        }
    }

    /**
     * DB重查一遍
     * 
     * @param loginName
     * @throws Exception
     * @time 2017年2月3日 上午11:22:36
     * @author fiend
     */
    private static void dbReFind(String loginName) throws Exception {
        try {
            loginName = DesUtil.decrypt(loginName, "A1B2C3D4E5F60708");
        }
        catch (Exception e) {
        }
        String loginNameDes = DesUtil.encrypt(loginName, "A1B2C3D4E5F60708");
        String sql = "SELECT COUNT(1) as 'size' from [T_CUSTOMERUSER] with(nolock) where C_loginname in ('" + loginName
                + "','" + loginNameDes + "')";
        DataTable DataTable = new DBHelperAccount().GetDataTable(sql, null);
        if (DataTable != null && "0".equals(DataTable.GetRow().get(0).GetColumnString("size"))) {
            deleteMongoCus(loginName);
        }
    }

    /**
     * 删除账号
     * 
     * @param loginName
     * @throws Exception
     * @time 2017年2月3日 上午11:22:04
     * @author fiend
     */
    private static void deleteMongoCus(String loginName) throws Exception {
        //        new MongoLogic().DelAccount(loginName);
        //        WriteLog.write("PersonateLog_deleteMongoCus", loginName);
    }
}
