package com.ccervice.inter.file;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;

import com.alibaba.fastjson.JSONObject;
import com.ccervice.db.util.MongoHelper;
import com.ccervice.util.DesUtil;
import com.ccervice.util.db.DBHelperAccount;
import com.ccervice.util.db.DataRow;
import com.ccervice.util.db.DataTable;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.inter.train.MongoLogic;
import com.ccservice.qunar.util.ExceptionUtil;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

public class MongoRefind {
    //上次断的
    //    private static String minID = "558ed585034c420aa0cb31fe";

    //真实最小的
    private static String minID = "558beb94034c4208b88a0209";

    private static boolean enover = true;

    //所有被封状态
    private static int[] disables = { 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44 };

    //默认Log名
    private static final String LOG_NAME = "MongoRefind_v2";

    /**
     * 重新扫描
     * 
     * @time 2017年2月3日 下午12:51:24
     * @author fiend
     */
    public static void refind() {
        while (enover) {
            refindEach();
        }
    }

    /**
     * 逐个账号扫描
     * 
     * @time 2017年2月3日 下午12:51:34
     * @author fiend
     */
    private static void refindEach() {
        BasicDBObject filterItem = new BasicDBObject();
        filterItem.put("$gt", new ObjectId(minID));
        BasicDBObject query = new BasicDBObject();
        query.put("_id", filterItem);
        try {
            DBObject oo = MongoHelper.getInstance().findOne("CustomerUser", query);
            if (oo != null && oo.containsField("_id") && !"".equals(oo.get("_id").toString())) {
                JSONObject jsonObject = JSONObject.parseObject(oo.toString());
                String loginName = jsonObject.getString("SupplyAccount");
                if (loginName != null) {
                    dbReFind(loginName);
                }
                String newMinID = jsonObject.getJSONObject("_id").getString("$oid");
                WriteLog.write(LOG_NAME + "_refindEach", newMinID + "--->" + loginName);
                System.out.println(newMinID + "--->" + loginName);
                if (!newMinID.equals(minID)) {
                    minID = newMinID;
                    return;
                }
            }
            WriteLog.write(LOG_NAME + "_refindEach_over", minID);
            enover = false;
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException(LOG_NAME + "_Exception", e, minID);
        }
    }

    /**
     * DB重查一遍
     * 
     * @param loginName
     * @throws Exception
     * @time 2017年2月3日 上午11:22:36
     * @author fiend
     */
    private static void dbReFind(String loginName) throws Exception {
        try {
            loginName = DesUtil.decrypt(loginName, "A1B2C3D4E5F60708");
        }
        catch (Exception e) {
        }
        String loginNameDes = DesUtil.encrypt(loginName, "A1B2C3D4E5F60708");
        String sql = "SELECT c_isenable from [T_CUSTOMERUSER] with(nolock) where C_loginname in ('" + loginName + "','"
                + loginNameDes + "')";
        DataTable DataTable = new DBHelperAccount().GetDataTable(sql, null);
        if (DataTable == null) {

        }
        else if (DataTable.GetRow().size() == 0) {
            deleteMongoCus(loginName, "_不存在");
        }
        else {
            for (DataRow dataRow : DataTable.GetRow()) {
                int status = dataRow.GetColumnInt("c_isenable");
                if (isDisable(status)) {
                    deleteMongoCus(loginName, "_被封");
                    break;
                }
            }
        }
        //        if (DataTable != null && "0".equals(DataTable.GetRow().get(0).GetColumnString("size"))) {
        //            deleteMongoCus(loginName, "_被封");
        //        }
    }

    /**
     * 删除账号
     * 
     * @param loginName
     * @param logNameSuffix 记录log的尾缀
     * @throws Exception
     * @time 2017年2月3日 上午11:22:04
     * @author fiend
     */
    private static void deleteMongoCus(String loginName, String logNameSuffix) throws Exception {
        saveMongoToBeDeletedCus2DB(loginName, logNameSuffix);
        //        new MongoLogic().DelAccount(loginName);
        WriteLog.write(LOG_NAME + "_deleteMongoCus" + logNameSuffix, loginName);
    }

    /**
     * 将待删除mongo账号的常旅客信息存入DB
     * 
     * @param loginName
     * @param logNameSuffix
     * @throws Exception
     * @time 2017年2月4日 上午11:11:05
     * @author fiend
     */
    private static void saveMongoToBeDeletedCus2DB(String loginName, String logNameSuffix) throws Exception {
        List<DBObject> dbObjects = new ArrayList<DBObject>();//new MongoLogic().FindMongoByCustomerUser(loginName);
        for (DBObject dbObject : dbObjects) {
            String RealName = dbObject.get("RealName").toString();
            String IDType = dbObject.get("IDType").toString();
            String IDNumber = dbObject.get("IDNumber").toString();
            if ("1".equals(IDType)) {
                String sql = " EXEC [sp_MongoDeletePassenger_insert] @IDNumber=" + IDNumber + " ,@Name='" + RealName
                        + "'";
                try {
                    boolean success = new DBHelperAccount().executeSql(sql);
                    //                    Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                    WriteLog.write(LOG_NAME + "_saveMongoToBeDeletedCus2DB" + logNameSuffix, loginName + "--->" + sql
                            + "--->" + success);
                }
                catch (Exception e) {
                    ExceptionUtil.writelogByException(LOG_NAME + "_saveMongoToBeDeletedCus2DB_Exception", e, loginName
                            + "--->" + sql);
                }
            }
        }

    }

    /**
     * 通过状态判断是否该删
     * 
     * @param status
     * @return
     * @time 2017年2月3日 下午3:55:06
     * @author fiend
     */
    private static boolean isDisable(int status) {
        for (int disable : disables) {
            if (status == disable) {
                return true;
            }
        }
        return false;
    }
}
