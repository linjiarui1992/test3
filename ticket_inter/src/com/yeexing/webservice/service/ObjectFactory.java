
package com.yeexing.webservice.service;


import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.yeexing.webservice.service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.yeexing.webservice.service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AirpSync }
     * 
     */
    public AirpSync createAirpSync() {
        return new AirpSync();
    }

    /**
     * Create an instance of {@link VoidInv }
     * 
     */
    public VoidInv createVoidInv() {
        return new VoidInv();
    }

    /**
     * Create an instance of {@link ChangeContractResponse }
     * 
     */
    public ChangeContractResponse createChangeContractResponse() {
        return new ChangeContractResponse();
    }

    /**
     * Create an instance of {@link PnrBookWithChild }
     * 
     */
    public PnrBookWithChild createPnrBookWithChild() {
        return new PnrBookWithChild();
    }

    /**
     * Create an instance of {@link PrintInv }
     * 
     */
    public PrintInv createPrintInv() {
        return new PrintInv();
    }

    /**
     * Create an instance of {@link OrderQueryStatusContractResponse }
     * 
     */
    public OrderQueryStatusContractResponse createOrderQueryStatusContractResponse() {
        return new OrderQueryStatusContractResponse();
    }

    /**
     * Create an instance of {@link CancelTicketTest }
     * 
     */
    public CancelTicketTest createCancelTicketTest() {
        return new CancelTicketTest();
    }

    /**
     * Create an instance of {@link RefundQuery }
     * 
     */
    public RefundQuery createRefundQuery() {
        return new RefundQuery();
    }

    /**
     * Create an instance of {@link PnrBookWithChildResponse }
     * 
     */
    public PnrBookWithChildResponse createPnrBookWithChildResponse() {
        return new PnrBookWithChildResponse();
    }

    /**
     * Create an instance of {@link OrderQueryContract }
     * 
     */
    public OrderQueryContract createOrderQueryContract() {
        return new OrderQueryContract();
    }

    /**
     * Create an instance of {@link ChangeResponse }
     * 
     */
    public ChangeResponse createChangeResponse() {
        return new ChangeResponse();
    }

    /**
     * Create an instance of {@link PnrBook }
     * 
     */
    public PnrBook createPnrBook() {
        return new PnrBook();
    }

    /**
     * Create an instance of {@link PnrBookResponse }
     * 
     */
    public PnrBookResponse createPnrBookResponse() {
        return new PnrBookResponse();
    }

    /**
     * Create an instance of {@link OrderQuery }
     * 
     */
    public OrderQuery createOrderQuery() {
        return new OrderQuery();
    }

    /**
     * Create an instance of {@link CancelTicketContractResponse }
     * 
     */
    public CancelTicketContractResponse createCancelTicketContractResponse() {
        return new CancelTicketContractResponse();
    }

    /**
     * Create an instance of {@link UserUnsignResponse }
     * 
     */
    public UserUnsignResponse createUserUnsignResponse() {
        return new UserUnsignResponse();
    }

    /**
     * Create an instance of {@link ParsePnrBookResponse }
     * 
     */
    public ParsePnrBookResponse createParsePnrBookResponse() {
        return new ParsePnrBookResponse();
    }

    /**
     * Create an instance of {@link QueryFlight }
     * 
     */
    public QueryFlight createQueryFlight() {
        return new QueryFlight();
    }

    /**
     * Create an instance of {@link Pay }
     * 
     */
    public Pay createPay() {
        return new Pay();
    }

    /**
     * Create an instance of {@link PnrMatchAirpResponse }
     * 
     */
    public PnrMatchAirpResponse createPnrMatchAirpResponse() {
        return new PnrMatchAirpResponse();
    }

    /**
     * Create an instance of {@link ParsePnrMatchAirpResponse }
     * 
     */
    public ParsePnrMatchAirpResponse createParsePnrMatchAirpResponse() {
        return new ParsePnrMatchAirpResponse();
    }

    /**
     * Create an instance of {@link RefundTicketResponse }
     * 
     */
    public RefundTicketResponse createRefundTicketResponse() {
        return new RefundTicketResponse();
    }

    /**
     * Create an instance of {@link ParsePnrMatchAirpContract }
     * 
     */
    public ParsePnrMatchAirpContract createParsePnrMatchAirpContract() {
        return new ParsePnrMatchAirpContract();
    }

    /**
     * Create an instance of {@link OrderQueryAirid }
     * 
     */
    public OrderQueryAirid createOrderQueryAirid() {
        return new OrderQueryAirid();
    }

    /**
     * Create an instance of {@link OrderQueryContractResponse }
     * 
     */
    public OrderQueryContractResponse createOrderQueryContractResponse() {
        return new OrderQueryContractResponse();
    }

    /**
     * Create an instance of {@link PnrBookContract }
     * 
     */
    public PnrBookContract createPnrBookContract() {
        return new PnrBookContract();
    }

    /**
     * Create an instance of {@link OrderQueryAiridResponse }
     * 
     */
    public OrderQueryAiridResponse createOrderQueryAiridResponse() {
        return new OrderQueryAiridResponse();
    }

    /**
     * Create an instance of {@link OrderQueryResponse }
     * 
     */
    public OrderQueryResponse createOrderQueryResponse() {
        return new OrderQueryResponse();
    }

    /**
     * Create an instance of {@link PnrBookContractResponse }
     * 
     */
    public PnrBookContractResponse createPnrBookContractResponse() {
        return new PnrBookContractResponse();
    }

    /**
     * Create an instance of {@link ParsePnrBook }
     * 
     */
    public ParsePnrBook createParsePnrBook() {
        return new ParsePnrBook();
    }

    /**
     * Create an instance of {@link AutoOutResponse }
     * 
     */
    public AutoOutResponse createAutoOutResponse() {
        return new AutoOutResponse();
    }

    /**
     * Create an instance of {@link ParsePnrMatchAirpContractResponse }
     * 
     */
    public ParsePnrMatchAirpContractResponse createParsePnrMatchAirpContractResponse() {
        return new ParsePnrMatchAirpContractResponse();
    }

    /**
     * Create an instance of {@link TPay }
     * 
     */
    public TPay createTPay() {
        return new TPay();
    }

    /**
     * Create an instance of {@link PrintInvResponse }
     * 
     */
    public PrintInvResponse createPrintInvResponse() {
        return new PrintInvResponse();
    }

    /**
     * Create an instance of {@link TPayOutResponse }
     * 
     */
    public TPayOutResponse createTPayOutResponse() {
        return new TPayOutResponse();
    }

    /**
     * Create an instance of {@link ParsePnrBookContractResponse }
     * 
     */
    public ParsePnrBookContractResponse createParsePnrBookContractResponse() {
        return new ParsePnrBookContractResponse();
    }

    /**
     * Create an instance of {@link OrderQueryStatusContract }
     * 
     */
    public OrderQueryStatusContract createOrderQueryStatusContract() {
        return new OrderQueryStatusContract();
    }

    /**
     * Create an instance of {@link VoidInvResponse }
     * 
     */
    public VoidInvResponse createVoidInvResponse() {
        return new VoidInvResponse();
    }

    /**
     * Create an instance of {@link OrderQueryStatus }
     * 
     */
    public OrderQueryStatus createOrderQueryStatus() {
        return new OrderQueryStatus();
    }

    /**
     * Create an instance of {@link AirpQueryStatusBusiness }
     * 
     */
    public AirpQueryStatusBusiness createAirpQueryStatusBusiness() {
        return new AirpQueryStatusBusiness();
    }

    /**
     * Create an instance of {@link PayOutContractResponse }
     * 
     */
    public PayOutContractResponse createPayOutContractResponse() {
        return new PayOutContractResponse();
    }

    /**
     * Create an instance of {@link TPayOut }
     * 
     */
    public TPayOut createTPayOut() {
        return new TPayOut();
    }

    /**
     * Create an instance of {@link AirpSyncResponse }
     * 
     */
    public AirpSyncResponse createAirpSyncResponse() {
        return new AirpSyncResponse();
    }

    /**
     * Create an instance of {@link RefundTicketContractResponse }
     * 
     */
    public RefundTicketContractResponse createRefundTicketContractResponse() {
        return new RefundTicketContractResponse();
    }

    /**
     * Create an instance of {@link TPayResponse }
     * 
     */
    public TPayResponse createTPayResponse() {
        return new TPayResponse();
    }

    /**
     * Create an instance of {@link AutoOut }
     * 
     */
    public AutoOut createAutoOut() {
        return new AutoOut();
    }

    /**
     * Create an instance of {@link QueryAllAirp }
     * 
     */
    public QueryAllAirp createQueryAllAirp() {
        return new QueryAllAirp();
    }

    /**
     * Create an instance of {@link PayOutContract }
     * 
     */
    public PayOutContract createPayOutContract() {
        return new PayOutContract();
    }

    /**
     * Create an instance of {@link PnrMatchAirp }
     * 
     */
    public PnrMatchAirp createPnrMatchAirp() {
        return new PnrMatchAirp();
    }

    /**
     * Create an instance of {@link UserUnsign }
     * 
     */
    public UserUnsign createUserUnsign() {
        return new UserUnsign();
    }

    /**
     * Create an instance of {@link RefundTicketTest }
     * 
     */
    public RefundTicketTest createRefundTicketTest() {
        return new RefundTicketTest();
    }

    /**
     * Create an instance of {@link RefundTicketTestResponse }
     * 
     */
    public RefundTicketTestResponse createRefundTicketTestResponse() {
        return new RefundTicketTestResponse();
    }

    /**
     * Create an instance of {@link QueryAirpolicy }
     * 
     */
    public QueryAirpolicy createQueryAirpolicy() {
        return new QueryAirpolicy();
    }

    /**
     * Create an instance of {@link CancelTicket }
     * 
     */
    public CancelTicket createCancelTicket() {
        return new CancelTicket();
    }

    /**
     * Create an instance of {@link PnrMatchAirpContract }
     * 
     */
    public PnrMatchAirpContract createPnrMatchAirpContract() {
        return new PnrMatchAirpContract();
    }

    /**
     * Create an instance of {@link Change }
     * 
     */
    public Change createChange() {
        return new Change();
    }

    /**
     * Create an instance of {@link PayResponse }
     * 
     */
    public PayResponse createPayResponse() {
        return new PayResponse();
    }

    /**
     * Create an instance of {@link RefundQueryResponse }
     * 
     */
    public RefundQueryResponse createRefundQueryResponse() {
        return new RefundQueryResponse();
    }

    /**
     * Create an instance of {@link ParsePnrMatchAirp }
     * 
     */
    public ParsePnrMatchAirp createParsePnrMatchAirp() {
        return new ParsePnrMatchAirp();
    }

    /**
     * Create an instance of {@link QueryAllAirpResponse }
     * 
     */
    public QueryAllAirpResponse createQueryAllAirpResponse() {
        return new QueryAllAirpResponse();
    }

    /**
     * Create an instance of {@link PnrMatchAirpTestResponse }
     * 
     */
    public PnrMatchAirpTestResponse createPnrMatchAirpTestResponse() {
        return new PnrMatchAirpTestResponse();
    }

    /**
     * Create an instance of {@link AirpSyncLastDateResponse }
     * 
     */
    public AirpSyncLastDateResponse createAirpSyncLastDateResponse() {
        return new AirpSyncLastDateResponse();
    }

    /**
     * Create an instance of {@link AirpQueryStatusBusinessResponse }
     * 
     */
    public AirpQueryStatusBusinessResponse createAirpQueryStatusBusinessResponse() {
        return new AirpQueryStatusBusinessResponse();
    }

    /**
     * Create an instance of {@link ChangeContract }
     * 
     */
    public ChangeContract createChangeContract() {
        return new ChangeContract();
    }

    /**
     * Create an instance of {@link OrderQueryStatusResponse }
     * 
     */
    public OrderQueryStatusResponse createOrderQueryStatusResponse() {
        return new OrderQueryStatusResponse();
    }

    /**
     * Create an instance of {@link QueryFlightResponse }
     * 
     */
    public QueryFlightResponse createQueryFlightResponse() {
        return new QueryFlightResponse();
    }

    /**
     * Create an instance of {@link TPayOutContractResponse }
     * 
     */
    public TPayOutContractResponse createTPayOutContractResponse() {
        return new TPayOutContractResponse();
    }

    /**
     * Create an instance of {@link PayOutResponse }
     * 
     */
    public PayOutResponse createPayOutResponse() {
        return new PayOutResponse();
    }

    /**
     * Create an instance of {@link PnrMatchAirpTest }
     * 
     */
    public PnrMatchAirpTest createPnrMatchAirpTest() {
        return new PnrMatchAirpTest();
    }

    /**
     * Create an instance of {@link TPayOutContract }
     * 
     */
    public TPayOutContract createTPayOutContract() {
        return new TPayOutContract();
    }

    /**
     * Create an instance of {@link BookTicketResponse }
     * 
     */
    public BookTicketResponse createBookTicketResponse() {
        return new BookTicketResponse();
    }

    /**
     * Create an instance of {@link PnrMatchAirpContractResponse }
     * 
     */
    public PnrMatchAirpContractResponse createPnrMatchAirpContractResponse() {
        return new PnrMatchAirpContractResponse();
    }

    /**
     * Create an instance of {@link RefundTicket }
     * 
     */
    public RefundTicket createRefundTicket() {
        return new RefundTicket();
    }

    /**
     * Create an instance of {@link QueryAirpolicyResponse }
     * 
     */
    public QueryAirpolicyResponse createQueryAirpolicyResponse() {
        return new QueryAirpolicyResponse();
    }

    /**
     * Create an instance of {@link AirpSyncLastDate }
     * 
     */
    public AirpSyncLastDate createAirpSyncLastDate() {
        return new AirpSyncLastDate();
    }

    /**
     * Create an instance of {@link PayOut }
     * 
     */
    public PayOut createPayOut() {
        return new PayOut();
    }

    /**
     * Create an instance of {@link CancelTicketTestResponse }
     * 
     */
    public CancelTicketTestResponse createCancelTicketTestResponse() {
        return new CancelTicketTestResponse();
    }

    /**
     * Create an instance of {@link BookTicket }
     * 
     */
    public BookTicket createBookTicket() {
        return new BookTicket();
    }

    /**
     * Create an instance of {@link RefundTicketContract }
     * 
     */
    public RefundTicketContract createRefundTicketContract() {
        return new RefundTicketContract();
    }

    /**
     * Create an instance of {@link CancelTicketResponse }
     * 
     */
    public CancelTicketResponse createCancelTicketResponse() {
        return new CancelTicketResponse();
    }

    /**
     * Create an instance of {@link CancelTicketContract }
     * 
     */
    public CancelTicketContract createCancelTicketContract() {
        return new CancelTicketContract();
    }

    /**
     * Create an instance of {@link ParsePnrBookContract }
     * 
     */
    public ParsePnrBookContract createParsePnrBookContract() {
        return new ParsePnrBookContract();
    }

}
