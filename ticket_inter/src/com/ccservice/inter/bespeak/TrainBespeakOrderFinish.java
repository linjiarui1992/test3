package com.ccservice.inter.bespeak;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.inter.server.Server;

/**
 * 抢票线下出票成功
 * 
 * @author guozhengju 2016-09-19
 */
public class TrainBespeakOrderFinish extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public TrainBespeakOrderFinish() {
		super();
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setHeader("content-type", "text/html;charset=UTF-8");
		PrintWriter out = null;
		JSONObject result = new JSONObject();
		try {
			out = response.getWriter();
			String orderId = request.getParameter("orderId");
			String orderType = request.getParameter("orderType");
			String isSuccess = request.getParameter("isSuccess");
			String failCode = request.getParameter("failCode");
			if ("success".equals(isSuccess)) {// 出票成功
				String orderNum = "";
				JSONArray passengers = new JSONArray();
				String sqlsel = "select O.OrderNumberOnline,O.refusereason,P.Name,T.TrainNo,T.realSeat,T.sealPrice,T.Coach,T.SeatNo,T.ticketNo from TrainOrderOffline O,TrainPassengerOffline P,TrainTicketOffline T where O.Id=P.OrderId and P.Id=T.TrainPid and O.Id="
						+ orderId;
				List list = Server.getInstance().getSystemService()
						.findMapResultBySql(sqlsel, null);
				for (int i = 0; i < list.size(); i++) {
					JSONObject json = new JSONObject();
					Map map = (Map) list.get(i);
					orderNum = map.get("OrderNumberOnline") == null ? "" : map
							.get("OrderNumberOnline").toString();	
					String name = map.get("Name") == null ? "" : map
							.get("Name").toString();
					String trainNum = map.get("TrainNo") == null ? "" : map
							.get("TrainNo").toString();
					String seatType = map.get("realSeat") == null ? "" : map
							.get("realSeat").toString();
					String price = map.get("sealPrice") == null ? "" : map.get(
							"sealPrice").toString();
					String coach = map.get("Coach") == null ? "" : map.get(
							"Coach").toString();
					String seatNo = map.get("SeatNo") == null ? "" : map.get(
							"SeatNo").toString();
					String ticketNo = map.get("ticketNo") == null ? "" : map
							.get("ticketNo").toString();
					json.put("name", name);	//乘客姓名
					json.put("trainNum", trainNum);	//车次
					json.put("seatType", seatType);	//坐席
					json.put("price", price);	//价格
					json.put("coach", coach);	//车厢
					json.put("seatNo", seatNo);	//座位
					json.put("ticketNo", ticketNo);	//票号
					passengers.add(json);
				}

				result.put("isSuccess", true);
				result.put("orderType", orderType);
				result.put("orderNum", orderNum);
				result.put("depTime", "");
				result.put("arrTime", "");
				result.put("msg", "");
				result.put("passengers", passengers);
			} else {
				result.put("isSuccess", false);
				result.put("orderType", orderType);	//0抢票未出票/1抢票成功
				result.put("orderNum", "");
				result.put("depTime", "");
				result.put("arrTime", "");
				result.put("msg", failCode);
				result.put("passengers", "");
			}
		} catch (Exception e) {
			result.put("isSuccess", false);
			result.put("orderType", "");
			result.put("orderNum", "");
			result.put("depTime", "");
			result.put("arrTime", "");
			result.put("msg", "系统错误");
			result.put("passengers", "");
		} finally {
			if (out != null) {
				out.print(result.toString());
				out.flush();
				out.close();
			}
		}

	}

}
