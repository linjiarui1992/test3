package com.ccservice.inter.rabbitmq;

import java.io.IOException;
import java.util.List;
import java.util.Random;

import com.ccservice.Util.db.DBHelper;
import com.ccservice.Util.db.DBSourceEnum;
import com.ccservice.Util.db.DataColumn;
import com.ccservice.Util.db.DataRow;
import com.ccservice.Util.db.DataTable;
import com.ccservice.Util.file.ExceptionUtil;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.b2b2c.base.train.Trainorderchange;
import com.ccservice.inter.server.Server;
import com.ccservice.mq.consumer.ConsumerMaintenance;
import com.ccservice.mq.consumer.RabbitQueueConsumer;
import com.ccservice.rabbitmq.util.PropertyUtil;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.MQPlatformTypeEnum;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.PublicConnectionInfos;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.RabbitmqBean;
import com.ggjs.oauth.client.servlet.AbstractOAuthClientServlet;
import net.tomas.db.safety.uitl.ReflexDBInfoTOObject;

/**
 *
 * 改签确认消费者
 *
 * @author RRRRRR
 * @time 2016年11月16日 下午3:27:38
 */
public class RabbitMQ_TrainConfirmChange extends AbstractOAuthClientServlet {

    private static final long serialVersionUID = 5622624897586147513L;
    private DBSourceEnum sourceEnum = null;//数据源

    @Override
    public void afterInit() {
        try {
            WriteLog.write("DB首次链接记录", "--->开始链接");//开启发下单模式 接收下单消费者发送的需求
            System.out.println("===OAuthClientServlet start begin===");
            long l1 = System.currentTimeMillis();
            int pingTaiType = Integer.parseInt(PropertyUtil.getValue("pingTaiType", "rabbitMQ.properties"));
            sourceEnum = DBSourceEnum.getDBSourceEnumByNo(pingTaiType);
            DataTable getDataTable = DBHelper.GetDataTable("select 1", sourceEnum);
            List<DataRow> getRow = getDataTable.GetRow();
            for (DataRow dataRow : getRow) {
                List<DataColumn> getColumn = dataRow.GetColumn();
                for (DataColumn dataColumn : getColumn) {
                    Object getValue = dataColumn.GetValue();
                    if (getValue != null) {
                        System.out.println("DB首次链接正常sql=[select 1]--->返回=["+getValue+"]");
                        WriteLog.write("DB首次链接记录", "DB首次链接正常sql=[select 1]--->返回=["+getValue+"]");
                    }
                }
            }
        }catch (RuntimeException e) {
            ExceptionUtil.writelogByException("RabbitMQ_TrainConfirmChange_InitDBSourceEnum_Exception",e,"初始化数据源异常");
            return;
        }
        catch (Exception e) {
            System.out.println("---DB首次链接异常---");
            ExceptionUtil.writelogByException("DB首次链接记录", e, "---DB首次链接异常---");
            return;
        }
        System.out.println("改签确认队列RabbitMQ-----开启");
        changeNotice();
    }

    int threadNum = 0;

    private void changeNotice() {
        System.out.println("改签确认消费者队列RabbitMQ-----！！！初始化！！！=" + this.threadNum + "个消费者 ");
        String kt110 = PropertyUtil.getValue("kt110", "rabbitMQ.properties");//空铁|同程 的【消费者TOMCAT编号】
        String queueName = PropertyUtil.getValue("QueueMQ_TrainChange_ConfirmOrder", "rabbitMQ.properties");//下单消费者在mq队列里的名字   "RabbitWaitOrder"线上叫这个
        int pingtaiType = Integer.parseInt(PropertyUtil.getValue("pingTaiType", "rabbitMQ.properties"));//平台类型 1=空铁 2=同程
        int consumerType = PublicConnectionInfos.getConsumerType(PublicConnectionInfos.CHANGECONFIRMORDER);//消费者类型0代表测试 1-13代表自己的业务 1代表下单消费者
        WriteLog.write("消费者启动记录", this.getClass().getSimpleName() + ":kt110:" + kt110 + ":queueName:"
                + PublicConnectionInfos.CHANGECONFIRMORDER + ":pingtaiType:" + pingtaiType + ":consumerType:"
                + consumerType);
        System.out.println("改签确认消费者队列【" + PublicConnectionInfos.CHANGECONFIRMORDER + "】【consumerType:" + consumerType
                + "】【kt110:" + kt110 + "】RabbitMQ-----====平台=" + pingtaiType + "【空铁1,同程2】 ");
        //初始化参数结束
        //监控开始
        //new MonitoringRabbitMQCustomer(kt110, pingtaiType, consumerType).start();//开启下单消费者监控
        new ConsumerMaintenance(PublicConnectionInfos.CHANGECONFIRMORDER,
                MQPlatformTypeEnum.getMQPlatformTypeEnumByType(pingtaiType), kt110) {

            @Override
            public void addCustomerCount(int count, RabbitmqBean rabbitmqBean) {
                String pingtaiString = String.valueOf(pingtaiType);
                for (int i = 0; i < count; i++) {
                    new RabbitQueueConsumer(PublicConnectionInfos.CHANGECONFIRMORDER, pingtaiString,
                            rabbitmqBean.getHost(), rabbitmqBean.getUsername(), rabbitmqBean.getPassword(),
                            "RabbitMQ_TrainConfirmChange_log") {

                        @Override
                        public String execMethod(String notice) {
                            System.out.println(System.currentTimeMillis() + "Rabbit确认改签  接收 ：————————>" + notice);
                            long changeId = Long.valueOf(notice);
                            //确认改签操作
                            if (changeId > 0) {
                                RepOperate(changeId);
                            }
                            return notice + "--->改签确认";
                        }
                    }.start();
                }
            }
        }.start();
        //监控结束
    }

    /**
     *
     * @author RRRRRR
     * @time 2016年11月16日 下午2:42:12
     * @Description 改签确认mq消费者
     */
    private void changeNoticeback() {
        String QueueMQ_TrainChange_WaitOrder = PropertyUtil.getValue("QueueMQ_TrainChange_ConfirmOrder",
                "rabbitMQ.properties"); //
        int type = 1;
        try {
            int threadNum = Integer.parseInt(getInitParameter("threadNum"));
            for (int i = 0; i < threadNum; i++) {
                new RabbitQueueConsumerConfirmChangeV3(QueueMQ_TrainChange_WaitOrder, type).start();
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void RepOperate(long changeId) {
        int random = new Random().nextInt(1000000);
        //改签信息
        Trainorderchange trainOrderChange = getTrainOrderChangeById(changeId);
        if (trainOrderChange == null) {
            WriteLog.write("RabbitMQ_TrainConfirmChange_log_log",changeId + ":未查询到改签订单");
            return;
        }
        //异步改签
        int isAsync = trainOrderChange == null || trainOrderChange.getConfirmIsAsync() == null ? 0 : trainOrderChange
                .getConfirmIsAsync();
        //改签订单不存在、非异步
        if (isAsync != 1 || trainOrderChange.getOrderid() <= 0) {
            return;
        }
        //状态判断
        //int tcstatus = trainOrderChange.getTcstatus();
        int isQuestionChange = trainOrderChange.getIsQuestionChange() == null ? 0 : trainOrderChange
                .getIsQuestionChange();
        float changePrice = trainOrderChange.getTcprice();
        //非等待下单
        if (/*tcstatus != Trainorderchange.CHANGEWAITPAY || */isQuestionChange != 0 || changePrice <= 0) {
            WriteLog.write("重复进入改签确认队列", random + ":改签ID:" + changeId);
            return;
        }
        /*//更新改签
        int C_TCSTATUS = Trainorderchange.CHANGEPAYING;
        int C_STATUS12306 = Trainorderchange.ORDEREDPAYING;
        String updateSql = "update T_TRAINORDERCHANGE set C_TCSTATUS = " + C_TCSTATUS + ", C_STATUS12306 = "
                + C_STATUS12306 + " where ID = " + changeId + " and C_TCSTATUS = " + Trainorderchange.CHANGEWAITPAY;
        //更新结果
        int updateResult = Server.getInstance().getSystemService().excuteAdvertisementBySql(updateSql);
        //更新失败
        if (updateResult != 1) {
            WriteLog.write("RabbitMQ改签占座队列异常", random + ":改签ID:" + changeId + ":更新改签:" + updateResult);
            return;
        }*/
        //异步确认
        Server.getInstance().getTrain12306Service().AsyncChangeConfirm(trainOrderChange);
    }

    private Trainorderchange getTrainOrderChangeById(long changeId) {
        Trainorderchange trainorderchange = new Trainorderchange();
        try {
            DataTable dataTable = DBHelper.GetDataTable("sp_T_TRAINORDERCHANGE_ChangeConfirm_FindTrainOrderChangeByID @id=" + changeId,
                    sourceEnum);
            if (dataTable != null && dataTable.GetRow() != null && dataTable.GetRow().size() == 1) {
                ReflexDBInfoTOObject.setDBDate(trainorderchange,dataTable.GetRow().get(0),"trainorderchangefield"
                        + ".properties");
                return trainorderchange;
            }
        } catch (Exception e) {
            ExceptionUtil.writelogByException("RabbitMQ_TrainConfirmChange_getTrainOrderChangeById_Excetpion",e ,
                    ":改签ID:" + changeId);
        }

        return null;
    }


}
