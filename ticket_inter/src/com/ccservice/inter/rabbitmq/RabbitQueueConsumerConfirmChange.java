package com.ccservice.inter.rabbitmq; 

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang3.SerializationUtils;

import com.ccservice.b2b2c.base.train.Trainorderchange;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.rabbitmq.util.QueueConsumer;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.AMQP.BasicProperties;
/**
 * 
 * 确认改签
 * @author RRRRRR
 * @time 2016年11月17日 上午9:18:40
 */
public class RabbitQueueConsumerConfirmChange extends QueueConsumer implements Runnable,Consumer{
       
    private long changeId;
    
    public RabbitQueueConsumerConfirmChange(String endPointName) throws IOException {
        super(endPointName);
    }
    
    public void handleDelivery(String consumerTag, Envelope env,BasicProperties props, byte[] body) throws IOException {
        Map<?, ?> map = (HashMap<?, ?>)SerializationUtils.deserialize(body);
        try {
            changeId=Long.valueOf(map.get("message number").toString());
            System.out.println("确认改签  接收 ：————————>"+map.get("message number").toString());
        }
        catch (Exception e) {
            changeId = 0;
        }
        //确认改签操作
        if (changeId > 0) {
            RepOperate();
        }
    }

    private void RepOperate() {
        int random = new Random().nextInt(1000000);
        //改签信息
        Trainorderchange trainOrderChange = Server.getInstance().getTrainService().findTrainOrderChangeById(changeId);
        //异步改签
        int isAsync = trainOrderChange == null || trainOrderChange.getConfirmIsAsync() == null ? 0 : trainOrderChange
                .getConfirmIsAsync();
        //改签订单不存在、非异步
        if (isAsync != 1 || trainOrderChange.getOrderid() <= 0) {
            return;
        }
        //状态判断
        int tcstatus = trainOrderChange.getTcstatus();
        int isQuestionChange = trainOrderChange.getIsQuestionChange() == null ? 0 : trainOrderChange
                .getIsQuestionChange();
        float changePrice = trainOrderChange.getTcprice();
        //非等待下单
        if (tcstatus != Trainorderchange.CHANGEWAITPAY || isQuestionChange != 0 || changePrice <= 0) {
            WriteLog.write("重复进入改签确认队列", random + ":改签ID:" + changeId);
            return;
        }
        //更新改签
        int C_TCSTATUS = Trainorderchange.CHANGEPAYING;
        int C_STATUS12306 = Trainorderchange.ORDEREDPAYING;
        String updateSql = "update T_TRAINORDERCHANGE set C_TCSTATUS = " + C_TCSTATUS + ", C_STATUS12306 = "
                + C_STATUS12306 + " where ID = " + changeId + " and C_TCSTATUS = " + Trainorderchange.CHANGEWAITPAY;
        //更新结果
        int updateResult = Server.getInstance().getSystemService().excuteAdvertisementBySql(updateSql);
        //更新失败
        if (updateResult != 1) {
            WriteLog.write("RabbitMQ改签占座队列异常", random + ":改签ID:" + changeId + ":更新改签:" + updateResult);
            return;
        }
        //异步确认
        Server.getInstance().getTrain12306Service().AsyncChangeConfirm(trainOrderChange);
    }
}
