package com.ccservice.inter.rabbitmq; 

import java.io.IOException;
import java.util.Date;
import java.util.UUID;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.time.TimeUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.JobQueryMyOrder;
import com.ccservice.qunar.util.ExceptionUtil;
import com.ccservice.train.mqlistener.MQMethod;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.PublicConnectionInfos;
import com.ccservice.util.mq.util_mq.rabbitmq.customer.IConsumer;
import com.ccservice.util.mq.util_mq.rabbitmq.customer.RabbitMQDefaultCustomer;
import com.ccservice.util.mq.util_mq.rabbitmq.customer.Monitoring.RabbitMQCustomerMonitoringUtil;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.ShutdownSignalException;
/**
 * 审核
 * 
 * @author 之至
 * @time 2016年12月27日 下午7:04:10
 */
public class RabbitQueueConsumerQueryOrderV3 extends RabbitMQDefaultCustomer implements IConsumer {
    private long orderid;

    private long successtime; 

    private int queryno;
    
//    private String queueNameString;

    private int type;

    //消息内容
    private String orderNoticeResult = "";

    public RabbitQueueConsumerQueryOrderV3(String queueNameString, int type) throws IOException {
//        super(queueNameString, type);
//        this.queueNameString = queueNameString;
//        this.type = type;
    }
    
    public RabbitQueueConsumerQueryOrderV3(String name, String pingtaiType, String host, String username, String password) throws IOException {
        super(name, pingtaiType, host, username, password);
    }

    public String logName = this.getClass().getSimpleName();
    @Override
    public void run() {
        UUID uuid = UUID.randomUUID();
        CustomerName = queueNameString + ":" + uuid.toString();//消费者名字随机生成的
        int type = PublicConnectionInfos.getConsumerType(queueNameString);//根据队列名称获取type
        try {
            initCounnection();//初始化链接 【第一步】:获取连接 【第二步】:初始化channel 【第三步】:初始化consumer,并连接到队列
        }
        catch (IOException e1) {
            e1.printStackTrace();
        }
        while (true) {//循环获取消息
            if(!super.connection.isOpen()){//如果连接已经关闭了就关闭
                willClose = true;
                WriteLog.write(this.logName + "-Exception", "连接关了");
            }
            else {
                if (!isCanOrdering(new Date())) {//是否可以下单 
                    try {
                        sleep(1000L);//如果不能下单休息1秒
                    }
                    catch (Exception e) {
                    }
                    continue;
                }
                String msg = "";
                try {
                    msg = getMsgNew();//【第四步】:获取消息
                    String orderNoticeResult = msg;//消息内容
                    System.out.println(TimeUtil.getDateByPattern("yyyy-MM-dd HH:mm:ss") + ":下单当前消费者数量 -- --->"
                            + RabbitMQCustomerMonitoringUtil.ListcustomerNames.size() + "---"
                            + RabbitMQCustomerMonitoringUtil.Mapcustomers.size() + ":接收到消息:" + orderNoticeResult);
                    WriteLog.write("RabbitQueueConsumerCreateOrderV3",
                            TimeUtil.getDateByPattern("yyyy-MM-dd HH:mm:ss") + ":【" + orderNoticeResult + "】");
                    willClose = processTheMessage(orderNoticeResult);//【这一步是进行业务逻辑的方法】去下单的方法挪到了一个方法里了【真正下单的方法返回是否释放该消费者】
                    WriteLog.write("RabbitQueueConsumerCreateOrderV3",
                            TimeUtil.getDateByPattern("yyyy-MM-dd HH:mm:ss") + ":【" + willClose + "】");
                    AckMsg();//【第五步】:确认消息，已经收到
                }
                catch (ShutdownSignalException e) {
                    e.printStackTrace();
                    willClose = true;
                }
                catch (ConsumerCancelledException e) {
                    e.printStackTrace();
                    willClose = true;
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                    willClose = true;
                }
            }
            if (willClose) {//是否即将关闭，这个地方是用来控制消费者数量的
                try {
                    closeMethod(CustomerName);
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }
    
    public void run_back20170302() {
//        System.out.println("下单审核队列RabbitMQ-----开启   数量--->" + Mapcustomers.size());
        while (true) {
            try {
                orderNoticeResult = getNewMessageString(queueNameString);
                channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
                System.out.println("审核队列  接收 ：————————>"+orderNoticeResult);
                WriteLog.write("Rabbit_QueueMQ_QueryOrder", "orderNoticeResult:" + orderNoticeResult);
                query(orderNoticeResult);
            }
            catch (ShutdownSignalException e) {
                ExceptionUtil.writelogByException("Rabbit_QueueMQ_QueryOrder_ShutdownSignalException", e, "");
            }
            catch (ConsumerCancelledException e) {
                ExceptionUtil.writelogByException("Rabbit_QueueMQ_QueryOrder_ConsumerCancelledException", e, "");
            }
            catch (IOException e) {
                ExceptionUtil.writelogByException("Rabbit_QueueMQ_QueryOrder_IOException", e, "");
            }
            catch (InterruptedException e) {
                ExceptionUtil.writelogByException("Rabbit_QueueMQ_QueryOrder_InterruptedException", e, "");
            }
            catch (Exception e) {
                ExceptionUtil.writelogByException("Rabbit_QueueMQ_QueryOrder_Exception", e, "");
            }
            finally{
              
            }
        }
    }

    /**
     * 真正执行的处理审核订单的逻辑
     * 
     * @param orderNoticeResult
     * @time 2017年3月2日 下午7:54:51
     * @author chendong
     */
    private void query(String orderNoticeResult) {
        JSONObject jsonobject = JSONObject.parseObject(orderNoticeResult);
        this.orderid = jsonobject.containsKey("orderid") ? jsonobject.getLongValue("orderid") : 0;
        long intotime = jsonobject.containsKey("intotime") ? jsonobject.getLongValue("intotime") : 0;
        this.successtime = jsonobject.containsKey("successtime") ? jsonobject.getLongValue("successtime") : 0;
        this.queryno = jsonobject.containsKey("queryno") ? jsonobject.getIntValue("queryno") : 0;
        int type = jsonobject.containsKey("type") ? jsonobject.getIntValue("type") : 0;
        WriteLog.write("Rabbit_QueueMQ_QueryOrder", "orderid:" + orderid + "type:" + type);
        if (MQMethod.QUERY == type) {
            //系统配置的审核间隔时间
            new JobQueryMyOrder().gotoQuery(this.orderid, this.successtime, this.queryno);
        }
    
    }

    /**
     * 
     * 处理消息
     * @param orderNoticeResult
     * @time 2017年3月2日 下午7:36:08
     * @author chendong
     */
    public boolean processTheMessage(String msgInfo) {
        boolean willClose = false;//是否即将关闭
        try {
            query(msgInfo);//【具体的实际进行的业务逻辑】
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            consumerStatus = RabbitMQCustomerMonitoringUtil.Mapcustomers.get(CustomerName).isUse();//控制消费者
            if (!consumerStatus) {
                willClose = true;
            }
        }
        return willClose;
    }
}
