package com.ccservice.inter.rabbitmq;

import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import com.ccservice.Util.file.WriteLog;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.util.TimeUtil;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.train.thread.TrainCreateOrderFalse;
import com.ccservice.mq.consumer.ConsumerMaintenance;
import com.ccservice.mq.consumer.RabbitQueueConsumer;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.MQPlatformTypeEnum;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.PublicConnectionInfos;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.RabbitmqBean;
import com.ggjs.config.netty.InitServer;

/**
 * 初始化DB
 * 
 * @parameter
 * @time 2018年3月22日 下午1:04:36
 * @author YangFan
 */
public class RabbitQueueMQ_InitServer extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public void init() throws ServletException {
        System.out.println("开始InitServer");
        InitServer.start();
        System.out.println("结束InitServer");
        orderNtice();
    }

    int threadNum = 0;

    private void orderNtice() {
        System.out.println("假下单占座队列RabbitMQ-----！！！初始化！！！=" + this.threadNum + "个消费者 ");
        String kt110 = PropertyUtil.getValue("kt110", "rabbitMQ.properties");//空铁|同程 的【消费者TOMCAT编号】
        int pingtaiType = Integer.parseInt(PropertyUtil.getValue("pingTaiType", "rabbitMQ.properties"));//平台类型 1=空铁 2=同程
        int consumerType = PublicConnectionInfos.getConsumerType(PublicConnectionInfos.SHAM_CREATEORDER);//消费者类型0代表测试 1-13代表自己的业务 1代表下单消费者
        WriteLog.write("消费者启动记录", this.getClass().getSimpleName() + ":kt110:" + kt110 + ":queueName:"
                + PublicConnectionInfos.SHAM_CREATEORDER + ":pingtaiType:" + pingtaiType + ":consumerType:"
                + consumerType);
        System.out.println("假下单占座队列【" + PublicConnectionInfos.SHAM_CREATEORDER + "】【consumerType:" + consumerType
                + "】【kt110:" + kt110 + "】RabbitMQ-----====平台=" + pingtaiType + "【空铁1,同程2】 ");
        new ConsumerMaintenance(PublicConnectionInfos.SHAM_CREATEORDER,
                MQPlatformTypeEnum.getMQPlatformTypeEnumByType(pingtaiType), kt110) {
            @Override
            public void addCustomerCount(int count, RabbitmqBean rabbitmqBean) {
                String pingtaiString = String.valueOf(pingtaiType);
                for (int i = 0; i < count; i++) {
                    new RabbitQueueConsumer(PublicConnectionInfos.SHAM_CREATEORDER, pingtaiString,
                            rabbitmqBean.getHost(), rabbitmqBean.getUsername(), rabbitmqBean.getPassword(),
                            "sham_CreateOrder_log") {
                        @Override
                        public String execMethod(String notice) {
                            long orderid = Long.valueOf(notice);
                            System.out.println(TimeUtil.dateToString(new Date(), TimeUtil.yyyyMMddHHmmss) + ":准备处理订单:"
                                    + orderid);
                            if (orderid > 0) {
                                new TrainCreateOrderFalse(orderid).createOrderStart(new Customeruser(), false);//执行假下单的逻辑
                            }
                            return notice + "--->下单";
                        }
                    }.start();
                }
            }
        }.start();
        //监控结束
    }
}
