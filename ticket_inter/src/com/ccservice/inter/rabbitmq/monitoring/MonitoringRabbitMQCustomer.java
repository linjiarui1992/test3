package com.ccservice.inter.rabbitmq.monitoring;

//import java.util.Date;
import java.util.List;
import java.util.Map;

import com.ccservice.Util.time.TimeUtil;
import com.ccservice.elong.inter.PropertyUtil;
//import com.ccservice.b2b2c.util.TimeUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.rabbitmq.RabbitMQJobConsumerMonitoringMethod;
import com.ccservice.inter.rabbitmq.RabbitQueueConsumerCannelOrder;
import com.ccservice.inter.rabbitmq.RabbitQueueConsumerConfirmChangeV3;
import com.ccservice.inter.rabbitmq.RabbitQueueConsumerCreateOrderV3;
import com.ccservice.inter.rabbitmq.RabbitQueueConsumerPayExamineChangeV3;
//import com.ccservice.inter.rabbitmq.RabbitQueueConsumerRequestChangeV3;
import com.ccservice.inter.server.Server;
import com.ccservice.qunar.util.ExceptionUtil;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.PublicConnectionInfos;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.RabbitMQConnectsBean;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.RabbitmqBean;
import com.ccservice.util.mq.util_mq.rabbitmq.customer.Monitoring.RabbitMQCustomerMonitoringUtil;

@Deprecated
public class MonitoringRabbitMQCustomer extends MonitoringSupply {

    //是否可以维护消费者数量
    private boolean mainTainConsumerNum = true;

    private String customerTomcatNumber;//消费者tomcat编号 之前的是 kt110表示

    private String pingTaiType;//平台类型 1=空铁;2=同程

    private int consumerType;//消费者类型 1-13 不同的消费者消费者队列的名字

    public MonitoringRabbitMQCustomer() {
    }

    /**
     * 
     * @param customerTomcatNumber 当前tomcat编号
     * @param pingTaiType 平台类型1空铁2同程
     * @param consumerType 消费者类型 //消费者类型 1下单消费者 3审核消费者 4扣款消费者   -13 不同的消费者消费者队列的名字
     */
    public MonitoringRabbitMQCustomer(String customerTomcatNumber, String pingTaiType, int consumerType) {
        this.customerTomcatNumber = customerTomcatNumber;
        this.pingTaiType = pingTaiType;
        this.consumerType = consumerType;
    }

    private int customerCount = 0;//当前tomcat消费者数量

    private int customerCount_DB = -1;//数据库配置的消费者的数量

    private String customerStatus_DB = "0";//当前消费者状态0关1开,

    private long randomL1;

    @Override
    public void run() {
        Double minute = 0.5D;//等几分钟再开始监控
        long sleepTime = (long) (1000 * 60 * minute);
        try {
            String queueName = PublicConnectionInfos.getstrTypeNameStringByConsumerType(this.consumerType + "");
            System.out.println("【" + TimeUtil.gettodaydate(5) + "】:" + queueName + ":消费者监控程序:先睡【" + minute
                    + "】分钟,等消费者都连上了再监控");
            Thread.sleep(sleepTime);
            while (true) {
                try {
                    randomL1 = System.currentTimeMillis();//刷新当次 唯一值
                    RabbitmqBean rabbitmqBean = getRabbitmqBean();//获取当前使用的哪个mq服务的对象
                    InitCustomerCount(rabbitmqBean);//初始化消费者数量
                    updateCustomerCount(rabbitmqBean);//更新消费者的数量
                    //                        SendMessage();
                    //                    //当前消费者数量
                    //                    getConsumerNum();
                    //                    //根据数量进行操作
                    //                    decideConsumerNums();

                    boolean CanOrdering = RabbitMQJobConsumerMonitoringMethod.isCanOrdering();
                    if (CanOrdering) {//是否可以 06:00:00-07:01:30
                        //检测一次
                        Thread.sleep(sleepTime);
                    }
                    else {
                        Thread.sleep(sleepTime * 2);
                        //                        System.out.println(TimeUtil.getDateByPattern("yyyy-MM-dd HH:mm:ss") + ":非工作时间");
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("RabbitMQJobConsumerMonitoringMethod_Exception", e, "run");
        }
    }

    private void SendMessage() {
        String pingtaiName = "1".equals(pingTaiType) ? "空铁" : "同程";
        String strTypeString = PublicConnectionInfos.getstrTypeNameStringByConsumerType(consumerType + "");//获取消费者的类型名字
        String smsContentString = pingtaiName + ":第" + customerTomcatNumber + "号" + strTypeString + "，消费者剩余"
                + RabbitMQCustomerMonitoringUtil.Mapcustomers.size() + "实际应为  "
                + PublicConnectionInfos.RabbitWaitOrder_V;
        WriteLog.write("Rabbit_QueueMQ_短信提醒", TimeUtil.getDateByPattern("yyyy-MM-dd HH:mm:ss") + "发送短信详情 -- "
                + smsContentString);
        sendSMS(smsContentString);
    }

    /**
     * 更新消费者的数量
     * 
     * @time 2017年2月15日 下午2:32:30
     * @author chen
     * @param rabbitmqBean 
     */
    private void updateCustomerCount(RabbitmqBean rabbitmqBean) {
        initDbData(rabbitmqBean);
        if ("1".equals(customerStatus_DB)) {//该消费者tomcat处于开启状态
            if (customerCount_DB == customerCount) {//如果数据库配置的和实际的一样,什么都不操作
            }
            else if (customerCount_DB > customerCount) {//如果数据库配置的比实际的大,说明消费者少了就加消费者
                int addCount = customerCount_DB - customerCount;//即将增加的消费者数量
                addCustomerCount(addCount, rabbitmqBean);//增加消费者
            }
            else if (customerCount_DB < customerCount) {//如果数据库配置的比实际的小,说明消费者多了就减消费者
                shutDownCustomerCount(customerCount - customerCount_DB);//减少消费者
            }
        }
        else {
            shutDownCustomerCount(customerCount);//减少消费者
        }
    }

    /**
     * 增加消费者
     * #TODO 这里必须要加上才能真正添加
     * @param count 增加多少个消费者
     * @time 2017年2月15日 下午2:46:14
     * @author chen
     * @param rabbitmqBean 
     */
    private void addCustomerCount(int count, RabbitmqBean rabbitmqBean) {
        String pingtaiType = PropertyUtil.getValue("pingTaiType", "rabbitMQ.properties");//1=空铁 2=同程
        System.out.println(randomL1 + ":" + pingtaiType + "准备【增加】" + count + "个消费者,增加消费者类型:" + consumerType);
        String host = rabbitmqBean.getHost();
        String username = rabbitmqBean.getUsername();
        String password = rabbitmqBean.getPassword();
        //监控的什么类型就加什么样的消费者
        if (consumerType == 0) {//这是测试消费者
            addRabbitTestCustomer(count, rabbitmqBean);
        }
        if (consumerType == 1) {//这是下单消费者
            addRabbitWaitOrderCustomer(count, rabbitmqBean, PublicConnectionInfos.RABBITWAITORDER, pingtaiType);
        }
        if (consumerType == 14) {//这是下单消费者【 托管】
            addRabbitWaitOrderCustomer(count, rabbitmqBean, PublicConnectionInfos.RABBITWAITORDERTUOGUAN, pingtaiType);
        }
        if (consumerType == 2) {//这是下单排队消费者
            addRabbitTrainOrderWaitCustomer(count, rabbitmqBean, pingtaiType);
        }
        if (consumerType == 3) {//这是审核消费者
            addRabbitQueryOrderCustomer(count, rabbitmqBean, pingtaiType);
        }
        if (consumerType == 4) {//这是扣款消费者 
            addRabbitDeductionCustomer(count, rabbitmqBean, pingtaiType);
        }
        if (consumerType == 5) {//这是申请改签消费者 改签占座
            addRabbitTrainRequestChangeCustomer(count, rabbitmqBean, pingtaiType);
        }
        if (consumerType == 6) {//这是 确认改签消费者 changeConfirmOrder
            for (int i = 0; i < count; i++) {
                try {
                    new RabbitQueueConsumerConfirmChangeV3(PublicConnectionInfos.CHANGECONFIRMORDER, pingtaiType, host,
                            username, password).start();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if (consumerType == 7) {//这是改签审核消费  changePayExamine
            for (int i = 0; i < count; i++) {
                try {
                    new RabbitQueueConsumerPayExamineChangeV3(PublicConnectionInfos.CHANGEPAYEXAMINE, pingtaiType,
                            host, username, password).start();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if (consumerType == 8) {//这是改签排队消费者  Rabbit_ChangeOrder_PaiDui

        }
        if (consumerType == 9) {//这是改签扣款消费者 
            addRabbitGQDeductionCustomer(count, rabbitmqBean, pingtaiType);
        }
        if (consumerType == 10) {//这是 取消订单消费者 
            for (int i = 0; i < count; i++) {
                try {
                    new RabbitQueueConsumerCannelOrder(PublicConnectionInfos.QUEUEMQ_CANCELORDER, pingtaiType, host,
                            username, password).start();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if (consumerType == 11) {//这是 淘宝改签特有消费者
        }
        if (consumerType == 12) {//这是 同程退票消费者
        }
        if (consumerType == 13) {//这是 更新帐号消费者
        }
    }

    /**
     * 这是改签占座消费者
     * 
     * @param count
     * @param rabbitmqBean
     * @param pingtaiType
     * @time 2017年3月12日 下午5:36:11
     * @author chendong
     */
    private void addRabbitTrainRequestChangeCustomer(int count, RabbitmqBean rabbitmqBean, String pingtaiType) {
        for (int i = 0; i < count; i++) {
            try {
                newRabbitTrainRequestChangeByHost(PublicConnectionInfos.CHANGEWAITORDER, pingtaiType, rabbitmqBean);//使用host启动的消费者//新的消费者
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 这是下单排队消费者
     * 
     * @param count
     * @param rabbitmqBean
     * @param pingtaiType
     * @time 2017年3月12日 下午5:36:11
     * @author chendong
     */
    private void addRabbitTrainOrderWaitCustomer(int count, RabbitmqBean rabbitmqBean, String pingtaiType) {
        for (int i = 0; i < count; i++) {
            try {
                newRabbitQueueConsumerWaitLineV3ByHost(
                        PublicConnectionInfos.QUEUEMQ_TRAINORDER_WAITORDER_ORDERID_PAIDUI, pingtaiType, rabbitmqBean);//使用host启动的消费者//新的消费者
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 改签扣款
     * 
     * @param count
     * @param rabbitmqBean
     * @param pingtaiType2
     * @time 2017年3月12日 下午1:16:44
     * @author chendong
     */
    private void addRabbitGQDeductionCustomer(int count, RabbitmqBean rabbitmqBean, String pingtaiType2) {
        for (int i = 0; i < count; i++) {
            try {
                newRabbitQueueConsumerGQDeductionByHost(PublicConnectionInfos.QUEUEMQ_TRAINORDERDEDUCTIONGQ,
                        pingtaiType, rabbitmqBean);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 创建一个扣款的消费者
     * 
     * @param count
     * @param rabbitmqBean
     * @time 2017年3月10日 下午6:10:46
     * @author chendong
     * @param pingtaiType2 
     */
    private void addRabbitDeductionCustomer(int count, RabbitmqBean rabbitmqBean, String pingtaiType) {
        for (int i = 0; i < count; i++) {
            try {
                newRabbitQueueConsumerDeductionByHost(PublicConnectionInfos.RABBIT_QUEUEMQ_DEDUCTION, pingtaiType,
                        rabbitmqBean);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * 增加审核消费者
     * 
     * @param count
     * @param rabbitmqBean
     * @time 2017年3月10日 下午3:24:52
     * @author chendong
     * @param pingtaiType2 
     */
    private void addRabbitQueryOrderCustomer(int count, RabbitmqBean rabbitmqBean, String pingtaiType) {
        for (int i = 0; i < count; i++) {
            try {
                newRabbitQueueConsumerQueryOrderByHost(PublicConnectionInfos.RABBIT_QUEUEMQ_QUERYORDER, pingtaiType,
                        rabbitmqBean);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void addRabbitTestCustomer(int count, RabbitmqBean rabbitmqBean) {
        for (int i = 0; i < count; i++) {
            try {
                int type = 1;//平台   1空铁   2同程
                type = Integer.parseInt(pingTaiType);//平台类型
                String queueNameString = "QUEUE_TEST";//下单消费者在mq队列里的名字
                String host = rabbitmqBean.getHost();
                String username = rabbitmqBean.getUsername();
                String password = rabbitmqBean.getPassword();
                //                new RabbitConsumerDemo(queueNameString, type, host, username, password).start();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 增加n个下单消费者
     * 
     * @param count
     * @time 2017年2月15日 下午2:52:34
     * @author chen
     * @param rabbitmqBean 
     * @param queueName 队列名字
     * @param pingtaiType 
     */
    private void addRabbitWaitOrderCustomer(int count, RabbitmqBean rabbitmqBean, String queueName, String pingtaiType) {
        for (int i = 0; i < count; i++) {
            try {
                //                new RabbitQueueConsumerCreateOrderV3(PublicConnectionInfos.RABBITWAITORDER, type).start();//增加一个对应的消费者；老方法
                newRabbitQueueConsumerCreateOrderV3ByHost(queueName, pingtaiType, rabbitmqBean);//使用host启动的消费者//新的消费者
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 
     * 
     * @param count
     * @time 2017年2月15日 下午2:53:00
     * @author chen
     */
    private void shutDownCustomerCount(int count) {
        System.out.println(randomL1 + ":准备【关闭】" + count + "个消费者,消费者类型:" + consumerType);
        //RabbitMQCustomerMonitoringUtil.shutDownRabbitWaitOrderCustomer(count, consumerType);
        //        //监控的什么类型就加什么样的消费者
        //        if(consumerType==0){//这是测试消费者
        //            RabbitMQCustomerMonitoringUtil.shutDownRabbitWaitOrderCustomer(count,consumerType);
        //        }
        //        if(consumerType==1){//这是下单消费者
        //            RabbitMQCustomerMonitoringUtil.shutDownRabbitWaitOrderCustomer(count,consumerType);
        //        }
    }

    /**
     * 初始化数据库配置的数据
     * 
     * @time 2017年2月15日 下午2:36:25
     * @author chen
     * @param rabbitmqBean 
     */
    private void initDbData(RabbitmqBean rabbitmqBean) {
        String kt110 = customerTomcatNumber;
        List list = getDbListConsumerData(kt110, pingTaiType);//获取库中所存数据，依次：状态,默认数量真实数量
        if (list.size() > 0) {//数据查询完成
            Map map3 = (Map) list.get(0);
            customerStatus_DB = map3.containsKey("Status") ? map3.get("Status").toString() : "1";
            String ConsumerNumString = map3.containsKey("ConsumerNum") ? map3.get("ConsumerNum").toString() : "0";//数据库里配置的消费者的个数
            customerCount_DB = Integer.valueOf(ConsumerNumString);//数据库里配置的消费者的个数
        }
        System.out.println(randomL1 + ":[监控]消费者tomcat编号:" + kt110 + ":[平台类型]:" + pingTaiType);
        System.out.println(randomL1 + ":[监控]当前数据库配置消费者[状态]:" + customerStatus_DB);
        System.out.println(randomL1 + ":[监控]当前:" + rabbitmqBean.getHost() + ":实际[数量]:" + customerCount
                + ":当前数据库配置消费者[数量]:" + customerCount_DB);
        updateDbListConsumerData(kt110, pingTaiType, customerCount);
    }

    /**
     * 初始化消费者的数量
     * 
     * @time 2017年2月15日 下午2:29:20
     * @author chen
     * @param rabbitmqBean 
     */
    private void InitCustomerCount(RabbitmqBean rabbitmqBean) {
        customerCount = 0;
        int num = RabbitMQCustomerMonitoringUtil.ListcustomerNames.size();//当前消费者的总数量
        for (int i = 0; i < num; i++) {
            String consumerNameString = RabbitMQCustomerMonitoringUtil.ListcustomerNames.get(i);//一个消费者分配的唯一的uuid合
            RabbitMQConnectsBean rabbitMQConnectsBean = RabbitMQCustomerMonitoringUtil.Mapcustomers
                    .get(consumerNameString);
            if (rabbitMQConnectsBean != null) {
                //            if (rabbitMQConnectsBean.isUse()) {//如果这个消费者是使用的状态
                int type = rabbitMQConnectsBean.getType();//消费者的队列类型
                if (consumerType == type) {//如果消费者和当前监听的消费者类型一样 类型一样
                    if (rabbitmqBean.getHost().equals(rabbitMQConnectsBean.getHost())) {//并且host也一样,说明是当前和host匹配的消费者。
                        customerCount = customerCount + 1;
                    }
                    else {//否则是不匹配的就关闭它
                        rabbitMQConnectsBean.setUse(false);
                    }
                }
                //}
            }
        }
        System.out.println(randomL1 + ":【" + TimeUtil.gettodaydate(5) + "】" + "当前消费者[类型consumerType:" + consumerType
                + "]数量:" + customerCount);
    }

    //=========XXXXXXXXXXXOOOOOOOOOO以下是老方法

    //上一次维护消费者时间
    //    private String mainTainConsumerLastTime = com.ccservice.b2b2c.util.TimeUtil.dateToString(new Date(), "yyyy-MM-dd HH:mm:ss");
    int type;

    //短信更新时间
    //    protected String smsTimeString = com.ccservice.b2b2c.util.TimeUtil.dateToString(new Date(), com.ccservice.b2b2c.util.TimeUtil.yyyyMMddHHmmss);
    /**
     * 监控方法中下单消费者的监控
     * 
     * @param kt110 消费者编号
     * @param pingTaiType 1=空铁;2=同程
     * @time 2017年1月9日 下午11:22:26
     * @author chen
     * @param ConsumerType 消费者类型,下单消费者, 
     */
    public void waitOrderMethod(String kt110, String pingTaiType, String ConsumerType) {
        String Status = "";//当前消费者状态0关1开,
        int ConsumerNum = 0;//数据库配置的默认数量//默认数量
        int ConsumerNumReal = 0;//当前消费者数量当前真实数量
        List list = getDbListConsumerData(kt110, pingTaiType);//获取库中所存数据，依次：状态,默认数量真实数量
        if (list.size() > 0) {//数据查询完成
            Map map3 = (Map) list.get(0);
            Status = map3.containsKey("Status") ? map3.get("Status").toString() : "1";
            String ConsumerNumString = map3.containsKey("ConsumerNum") ? map3.get("ConsumerNum").toString() : "0";//数据库里配置的消费者的个数
            String ConsumerNumRealString = map3.containsKey("ConsumerNumReal") ? map3.get("ConsumerNumReal").toString()
                    : "0";
            ConsumerNum = Integer.valueOf(ConsumerNumString);//数据库里配置的消费者的个数
            ConsumerNumReal = Integer.valueOf(ConsumerNumRealString);//数据库里当前真实的消费者个数
        }
        System.out.println("当前队列名称--->" + PublicConnectionInfos.RABBITWAITORDER + ":当前消费者数量-->"
                + RabbitMQCustomerMonitoringUtil.RabbitWaitOrder_nums + " 消费者默认数量" + ConsumerNum);
        if (ConsumerNum == 0 && ConsumerNumReal == 0) {//如果都为0，那就修改为默认的数量
            ConsumerNum = PublicConnectionInfos.RabbitWaitOrder_V;//默认消费者数
        }
        else if (RabbitMQCustomerMonitoringUtil.RabbitWaitOrder_nums != ConsumerNumReal) {//如果当前真实的和数据库真实的消费者数不一致,就维护数据库中消费者数量    1m
            String sqlString = "update TrainPingTai set ConsumerNumReal = "
                    + RabbitMQCustomerMonitoringUtil.RabbitWaitOrder_nums + " where PingTai = " + pingTaiType
                    + " and type= " + kt110;
            Server.getInstance().getSystemService().findMapResultBySql(sqlString, null);
        }
        //当前消费者的状态为1 并且 当前消费者数量小于默认值增加
        if ("1".equals(Status) && RabbitMQCustomerMonitoringUtil.RabbitWaitOrder_nums < ConsumerNum) {
            WriteLog.write("RabbitMQJobConsumerMonitoringMethod_decideConsumerNums", "当前队列名称--->"
                    + PublicConnectionInfos.RABBITWAITORDER + "(" + kt110 + ")" + "--->系统配置消费者数量-->" + ConsumerNum
                    + "--->当前消费者数量-->" + RabbitMQCustomerMonitoringUtil.ListcustomerNames.size());
            int poor = (ConsumerNum - RabbitMQCustomerMonitoringUtil.RabbitWaitOrder_nums);//还差多少个消费者
            for (int i = 0; i < poor; i++) {
                try {
                    int type = 1;//平台   1空铁   2同程
                    type = Integer.parseInt(pingTaiType);//
                    new RabbitQueueConsumerCreateOrderV3(PublicConnectionInfos.RABBITWAITORDER, type).start();//增加一个对应的消费者
                }
                catch (Exception e) {
                    ExceptionUtil.writelogByException(
                            "RabbitMQJobConsumerMonitoringMethod_decideConsumerNums_Exception", e);
                }
            }
            WriteLog.write("RabbitMQJobConsumerMonitoringMethod_decideConsumerNums", "当前队列名称--->"
                    + PublicConnectionInfos.RABBITWAITORDER + "(" + kt110 + ")" + "--->添加完毕--->系统配置消费者数量-->"
                    + ConsumerNum + "--->当前消费者数量-->" + RabbitMQCustomerMonitoringUtil.ListcustomerNames.size());
            System.out.println("当前队列名称--->" + PublicConnectionInfos.RABBITWAITORDER + "当前消费者数量 -->   "
                    + RabbitMQCustomerMonitoringUtil.ListcustomerNames.size());
        }
        //如果消费者数量大于默认数量且在消费者的数量维护时间内,且每日有且只维护一次
        else if (mainTainConsumerNum && RabbitMQCustomerMonitoringUtil.RabbitWaitOrder_nums > ConsumerNum) {//大于了
            WriteLog.write("RabbitMQJobConsumerMonitoringMethod_decideConsumerNums", "当前队列名称--->"
                    + PublicConnectionInfos.RABBITWAITORDER + "(" + kt110 + ")" + "--->系统配置消费者数量-->" + ConsumerNum
                    + "--->当前消费者数量-->" + RabbitMQCustomerMonitoringUtil.RabbitWaitOrder_nums);
            int numDifferent = (RabbitMQCustomerMonitoringUtil.RabbitWaitOrder_nums - ConsumerNum);//实际的减去默认的，就是比设置的多的那一部分消费者
            while (true) {
                for (int i = 0; i < numDifferent; i++) {
                    if (RabbitMQCustomerMonitoringUtil.Mapcustomers
                            .get(RabbitMQCustomerMonitoringUtil.ListcustomerNames.get(i)).getKey()
                            .equals(RabbitMQCustomerMonitoringUtil.ListcustomerNames.get(i))) {
                        RabbitMQCustomerMonitoringUtil.Mapcustomers.get(
                                RabbitMQCustomerMonitoringUtil.ListcustomerNames.get(i)).setUse(false);
                    }
                    //                    if (getRabbitWaitOrderTrue_nums() == ConsumerNum) {
                    if (RabbitMQCustomerMonitoringUtil.getConsumerNumTrue_nums(type) == ConsumerNum) {
                        break;
                    }
                }
                mainTainConsumerNum = false;
                //                mainTainConsumerLastTime = com.ccservice.b2b2c.util.TimeUtil.dateToString(new Date(), com.ccservice.b2b2c.util.TimeUtil.yyyyMMddHHmmss);
                break;
            }
            WriteLog.write("RabbitMQJobConsumerMonitoringMethod_decideConsumerNums", "当前队列名称--->"
                    + PublicConnectionInfos.RABBITWAITORDER + "(" + kt110 + ")" + "--->修改完毕");
        }

    }

    public static void main(String[] args) {
        String customerTomcatNumber = "1";
        String pingTaiType = "1";//平台类型1空铁2同程
        int consumerType = 1;//队列类型:如 1下单消费者 2 。。。
        new MonitoringRabbitMQCustomer(customerTomcatNumber, pingTaiType, consumerType).start();
    }
}
