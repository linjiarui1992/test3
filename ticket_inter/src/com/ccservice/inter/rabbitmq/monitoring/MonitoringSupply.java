package com.ccservice.inter.rabbitmq.monitoring;

import java.io.IOException;
import java.util.List;

import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.rabbitmq.RabbitQueueConsumerCreateOrderV3;
import com.ccservice.inter.rabbitmq.RabbitQueueConsumerGQTrainDeductionV3;
import com.ccservice.inter.rabbitmq.RabbitQueueConsumerQueryOrderV3;
import com.ccservice.inter.rabbitmq.RabbitQueueConsumerRequestChangeV3;
import com.ccservice.inter.rabbitmq.RabbitQueueConsumerTrainDeductionV3;
import com.ccservice.inter.rabbitmq.RabbitQueueConsumerWaitLineV3;
import com.ccservice.inter.server.Server;
import com.ccservice.rabbitmq.util.PropertyUtil;
import com.ccservice.rabbitmq.util.RabbitMQCustomer;
import com.ccservice.rabbitmq.util.byhost.RabbitMqUtilNew;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.RabbitmqBean;
import com.ccservice.util.mq.util_mq.rabbitmq.customer.RabbitMQDefaultCustomer;

public class MonitoringSupply extends RabbitMQDefaultCustomer{
    /**
     * 
     * 
     * @param kt110 消费者编号
     * @param pingTaiType 平台名1:空铁2:同程
     * @return
     * @time 2017年2月10日 下午3:19:05
     * @author chen
     */
    public List getDbListConsumerData(String kt110, String pingTaiType) {
        String sql = "select Status,ConsumerNum,ConsumerNumReal from [TrainPingTai] with(nolock)  where PingTai ="
                + pingTaiType + "and type =" + kt110;
        System.out.println(sql);
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        return list;
    }
    
    /**
     * 更新当前实际的消费者的数量
     * 
     * @param kt110
     * @param pingTaiType
     * @param customerCount
     * @time 2017年2月15日 下午5:40:10
     * @author chen
     */
    public void updateDbListConsumerData(String kt110, String pingTaiType, int customerCount) {
        String sqlString = "update TrainPingTai set ConsumerNumReal = "+customerCount+" where PingTai = " + pingTaiType
                + " and type= " + kt110;
        sqlString = "[dbo].[sp_TrainPingTai_update] @ConsumerNumReal="+customerCount+", @PingTai =" + pingTaiType
                + ",@type=" + kt110;
        try {
            Server.getInstance().getSystemService().findMapResultBySql(sqlString, null);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * 获取当前使用的哪个mq服务的对象
     * 
     * @return
     * @time 2017年2月17日 下午2:12:36
     * @author chen
     */
    public RabbitmqBean getRabbitmqBean() {
        String pingTaiType = PropertyUtil.getValue("pingTaiType", "rabbitMQ.properties");
        RabbitmqBean rabbitmqBean = RabbitMqUtilNew.getRabbitmqBean(pingTaiType);
        
//        //获取一个数据库配置的rabbitMq记录
//        List list = Server.getInstance().getSystemService()
//                .findMapResultByProcedure("[dbo].[MQConfiguration_selectUseingMq]");
//        if(list.size()>0){
//            Map map = (Map) list.get(0);
//            String host = map.get("MqHost")==null?"":map.get("MqHost").toString();
//            String username = map.get("UserName")==null?"":map.get("UserName").toString();
//            String password = map.get("PassWord")==null?"":map.get("PassWord").toString();
//            rabbitmqBean = new RabbitmqBean(host, username, password);
//        }
        return rabbitmqBean;
    }
    
    /**
     * 
     * 创建一个下单消费者
     * @param name 队列名
     * @param type 队列名字code类型 1-13
     * @param rabbitmqBean
     * @time 2017年2月20日 下午5:26:43
     * @author chen
     */
    public void newRabbitQueueConsumerCreateOrderV3ByHost(String name, String pingtaiType, RabbitmqBean rabbitmqBean) {
        String host = rabbitmqBean.getHost();
        String username = rabbitmqBean.getUsername();
        String password = rabbitmqBean.getPassword();
        try {
            if (host != null && host != null && host != null) {
                new RabbitQueueConsumerCreateOrderV3(name, pingtaiType, host, username, password).start();
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * 
     * 创建一个审核消费者
     * @param queuename 队列名
     * @param type  队列名字code类型 1-13
     * @param rabbitmqBean 初始化连接的rabbitMQ的基本信息
     * @time 2017年2月20日 下午5:26:43
     * @author chen
     */
    public void newRabbitQueueConsumerQueryOrderByHost(String queuename, String pingtaiType, RabbitmqBean rabbitmqBean) {
        String host = rabbitmqBean.getHost();
        String username = rabbitmqBean.getUsername();
        String password = rabbitmqBean.getPassword();
        try {
//            RabbitQueueConsumerQueryOrderV3 consumerQueryOrder = new RabbitQueueConsumerQueryOrderV3(queryName,type);
            if (host != null && host != null && host != null) {
                new RabbitQueueConsumerQueryOrderV3(queuename, pingtaiType, host, username, password).start();
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * 
     * 创建一个扣款消费者
     * @param queuename 队列名
     * @param type  队列名字code类型 1-13
     * @param rabbitmqBean 初始化连接的rabbitMQ的基本信息
     * @time 2017年2月20日 下午5:26:43
     * @author chen
     */
    public void newRabbitQueueConsumerDeductionByHost(String queuename, String pingtaiType, RabbitmqBean rabbitmqBean) {
        String host = rabbitmqBean.getHost();
        String username = rabbitmqBean.getUsername();
        String password = rabbitmqBean.getPassword();
        try {
            if (host != null && host != null && host != null) {
                new RabbitQueueConsumerTrainDeductionV3(queuename, pingtaiType, host, username, password).start();//扣款的消费者
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * 
     * 创建一个改签扣款消费者
     * @param queuename 队列名
     * @param type  队列名字code类型 1-13
     * @param rabbitmqBean 初始化连接的rabbitMQ的基本信息
     * @time 2017年2月20日 下午5:26:43
     * @author chen
     */
    public void newRabbitQueueConsumerGQDeductionByHost(String queuename, String pingtaiType, RabbitmqBean rabbitmqBean) {
        String host = rabbitmqBean.getHost();
        String username = rabbitmqBean.getUsername();
        String password = rabbitmqBean.getPassword();
        try {
            if (host != null && host != null && host != null) {
                new RabbitQueueConsumerGQTrainDeductionV3(queuename, pingtaiType, host, username, password).start();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * 下单排队消费者
     * 
     * @param queuename
     * @param pingtaiType
     * @param rabbitmqBean
     * @time 2017年3月12日 下午5:33:47
     * @author chendong
     */
    public void newRabbitQueueConsumerWaitLineV3ByHost(String queuename, String pingtaiType, RabbitmqBean rabbitmqBean) {
        String host = rabbitmqBean.getHost();
        String username = rabbitmqBean.getUsername();
        String password = rabbitmqBean.getPassword();
        try {
            if (host != null && host != null && host != null) {
            }
            new RabbitQueueConsumerWaitLineV3(queuename, pingtaiType, host, username, password).start();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * 这是改签占座消费者
     * 
     * @param queuename
     * @param pingtaiType
     * @param rabbitmqBean
     * @time 2017年3月12日 下午5:33:47
     * @author chendong
     */
    public void newRabbitTrainRequestChangeByHost(String queuename, String pingtaiType, RabbitmqBean rabbitmqBean) {
        String host = rabbitmqBean.getHost();
        String username = rabbitmqBean.getUsername();
        String password = rabbitmqBean.getPassword();
        try {
            if (host != null && host != null && host != null) {
                new RabbitQueueConsumerRequestChangeV3(queuename, pingtaiType, host, username, password).start();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static long LastSendSmsTime = System.currentTimeMillis();

    /**
     * 发送短信
     * 
     * @param smsContentString 要发送的短信内容
     * @time 2017年3月2日 上午11:37:16
     * @author chendong
     */
    public void sendSMS(String smsContentString) {
        boolean isCanSend = (System.currentTimeMillis() - MonitoringSupply.LastSendSmsTime) > (5 * 60 * 1000);
        //短信提示每5分钟发送一次
        if (isCanSend) {
            new RabbitMQCustomer().sendSMS(smsContentString);
            MonitoringSupply.LastSendSmsTime = System.currentTimeMillis();
        }
        else {
            //超过3分钟短信数
            WriteLog.write("Rabbit_QueueMQ_短信提醒", "没有超过设定的分钟数不发短信");
        }
    }
    
}
