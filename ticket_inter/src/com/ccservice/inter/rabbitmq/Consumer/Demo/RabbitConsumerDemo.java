package com.ccservice.inter.rabbitmq.Consumer.Demo;

import java.io.IOException;
import java.util.Date;
import java.util.UUID;

import com.ccservice.b2b2c.util.TimeUtil;
//import com.ccservice.elong.inter.PropertyUtil;
//import com.ccservice.inter.rabbitmq.monitoring.MonitoringRabbitMQCustomer;
import com.ccservice.util.mq.util_mq.rabbitmq.customer.RabbitMQDefaultCustomer;
import com.ccservice.util.mq.util_mq.rabbitmq.customer.Monitoring.RabbitMQCustomerMonitoringUtil;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.ShutdownSignalException;

/**
 * rabbitmq消费者demo
 * 一、extends RabbitMQDefaultCustomer
 * 二、写一个带队列名字的构造方法
 * 三、实现run方法
 * 
 * 
 * 
 * 
 * @time 2017年2月20日 下午5:49:10
 * @author chen
 */
public class RabbitConsumerDemo extends RabbitMQDefaultCustomer{
    
    public RabbitConsumerDemo(String queueNameString, String pingtaiType, String host, String username, String password) {
        super(queueNameString, pingtaiType, host, username, password);
    }

    @Override
    public void run() {
        UUID uuid = UUID.randomUUID();
        CustomerName = queueNameString + ":" + uuid.toString();//消费者名字随机生成的
        try {
            initCounnection();//初始化链接 【第一步】:获取连接 【第二步】:初始化channel 【第三步】:初始化consumer,并连接到队列
        }
        catch (IOException e1) {
            e1.printStackTrace();
        }
        while (true) {//循环获取消息
            try {
                String msg = getMsgNew();//【第四步】:获取消息   //消息内容
                System.out.println(TimeUtil.dateToString(new Date(), TimeUtil.yyyyMMddHHmmss) + ":下单当前消费者数量 -- --->"
                        + RabbitMQCustomerMonitoringUtil.Mapcustomers.size() + ":接收到消息:" + msg);
                willClose = logicMethod(msg);//【这一步是进行业务逻辑的方法】去下单的方法挪到了一个方法里了【真正下单的方法返回是否释放该消费者】
                AckMsg();//【第五步】:确认消息，已经收到
            }
            catch (ShutdownSignalException e) {
                e.printStackTrace();
                willClose = true;
            }
            catch (ConsumerCancelledException e) {
                e.printStackTrace();
                willClose = true;
            }
            catch (InterruptedException e) {
                e.printStackTrace();
                willClose = true;
            }
            
            if (willClose) {//是否即将关闭，这个地方是用来控制消费者数量的
                try {
                    closeMethod(CustomerName);
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    
    }

    /**
     * 传入消息内容 获取到该消费者是否准备关闭
     * 
     * @param msgInfo
     * @return
     * @time 2017年2月20日 下午5:58:04
     * @author chen
     */
    private boolean logicMethod(String msgInfo) {
        try {
            System.out.println("休息4秒["+msgInfo+"]");
            Thread.sleep(4000L);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        boolean willClose = !RabbitMQCustomerMonitoringUtil.Mapcustomers.get(CustomerName).isUse();//不使用了，就关闭
        return willClose;
    }
    
    
    
}
