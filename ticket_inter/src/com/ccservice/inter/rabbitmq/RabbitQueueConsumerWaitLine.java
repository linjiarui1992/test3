package com.ccservice.inter.rabbitmq; 

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.SerializationUtils;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.thread.TrainCreateOrderPaiDui;
import com.ccservice.rabbitmq.util.QueueConsumer;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.AMQP.BasicProperties;

/**
 * 
 * 订单排队
 * @author RRRRRR
 * @time 2016年11月18日 下午8:54:05
 */
public class RabbitQueueConsumerWaitLine extends QueueConsumer implements Runnable,Consumer{

    public RabbitQueueConsumerWaitLine(String endPointName) throws IOException {
        super(endPointName);
    }
    
    public void handleDelivery(String consumerTag, Envelope env,BasicProperties props, byte[] body) throws IOException {
        try {
            Map<?, ?> map = (HashMap<?, ?>)SerializationUtils.deserialize(body);
               String  jsonData=map.get("message number").toString();
               System.out.println("Rabbit消费订单排队  接收 ：————————>"+map.get("message number").toString());
                WriteLog.write("Rabbit消费订单排队", System.currentTimeMillis() + ":changeNoticeResult:" + jsonData);
                JSONObject json = JSONObject.parseObject(jsonData); //字符串转为JSONObject
                long trainorderid = json.getLongValue("trainorderid"); //解析JSON对象获取订单id
                boolean isLastPaidui = json.getBooleanValue("isLastPaidui");//解析JSONobject获取是否排队
                TrainCreateOrderPaiDui trainCreateOrderPaiDui = new TrainCreateOrderPaiDui(trainorderid);
                trainCreateOrderPaiDui.startPaiduiResult(isLastPaidui);
        }
        catch (Exception e) {
            WriteLog.write("Rabbit消费订单排队  接收_Exception", e + "--->"+e.getMessage());
        }
    }
}
