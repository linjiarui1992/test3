package com.ccservice.inter.rabbitmq;

import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.db.DBHelper;
import com.ccservice.Util.db.DataRow;
import com.ccservice.Util.db.DataTable;
import com.ccservice.Util.file.ExceptionUtil;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.accountSwitchJob.AccountSwitch;
import com.ccservice.accountSwitchJob.AccountSwitchJob;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.util.TimeUtil;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.train.thread.TrainCreateOrder;
import com.ccservice.mq.consumer.ConsumerMaintenance;
import com.ccservice.mq.consumer.RabbitQueueConsumer;
import com.ccservice.rabbitmq.util.byhost.RabbitMqUtilNew;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.MQPlatformTypeEnum;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.PublicConnectionInfos;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.RabbitmqBean;

/**
 * rabbitmq 下單 消費者自增 且 支持jsp加消费者 
 * 
 * @author 之至
 * @time 2016年12月6日 上午10:48:37
 */
public class RabbitQueueMQ_TrainCreateOrder extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public void init() throws ServletException {
        super.init();
        this.threadNum = initThreadNum();//初始化启动多少个消费者
        orderNtice();
        accountSwitchInit();
    }

    private void accountSwitchInit() {
		//获取数据库中的开关状态
    	boolean accountSwitch  = false;
    	String sql = "GetAccountSwitch";
    	try {
			DataTable dataTable = DBHelper.GetDataTable(sql);
			DataRow dataRow = dataTable.GetRow().get(0);
			String accountSwitchStr = dataRow.GetColumnString("C_VALUE");
			if (!"".equals(accountSwitchStr) && accountSwitchStr != null) {
				 accountSwitch = Integer.valueOf(accountSwitchStr) == 1?true:false;
			}
			AccountSwitch.setAccountSwitch(accountSwitch);
		} catch (Exception e) {
			ExceptionUtil.writelogByException("获取账号开关状态", e, sql);
		}
    	WriteLog.write("消费者启动读取账号获取开关状态", "AccountSwitch>>"+AccountSwitch.isAccountSwitch());
    	String time = "0/30 * * * * ? ";
		JobDetail jobDetail = new JobDetail("AccountSwitchJob", "AccountSwitchJobGroup",
				AccountSwitchJob.class);
		try {
			CronTrigger trigger = new CronTrigger("AccountSwitchJob", "AccountSwitchJobGroup", time);
			Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
			scheduler.scheduleJob(jobDetail, trigger);
			scheduler.start();
		} catch (Exception e) {
			ExceptionUtil.writelogByException("获取账号开关状态JOB异常", e, sql);
		}
	}

	int threadNum = 0;

    private void orderNtice() {
        System.out.println("下单占座队列RabbitMQ-----！！！初始化！！！=" + this.threadNum + "个消费者 ");
        String kt110 = PropertyUtil.getValue("kt110", "rabbitMQ.properties");//空铁|同程 的【消费者TOMCAT编号】
        int pingtaiType = Integer.parseInt(PropertyUtil.getValue("pingTaiType", "rabbitMQ.properties"));//平台类型 1=空铁 2=同程
        int consumerType = PublicConnectionInfos.getConsumerType(PublicConnectionInfos.RABBITWAITORDER);//消费者类型0代表测试 1-13代表自己的业务 1代表下单消费者
        WriteLog.write("消费者启动记录", this.getClass().getSimpleName() + ":kt110:" + kt110 + ":queueName:"
                + PublicConnectionInfos.RABBITWAITORDER + ":pingtaiType:" + pingtaiType + ":consumerType:"
                + consumerType);
        System.out.println("下单占座队列【" + PublicConnectionInfos.RABBITWAITORDER + "】【consumerType:" + consumerType
                + "】【kt110:" + kt110 + "】RabbitMQ-----====平台=" + pingtaiType + "【空铁1,同程2】 ");
        //初始化参数结束

        /* 老的启动消费者的方式 ,弃用改为新的方式，通过监控的方式开关消费者
        MonitoringSupply monitoringSupply = new MonitoringSupply();
        RabbitmqBean rabbitmqBean = getRabbitmqBean();//获取当前使用的哪个mq服务的对象
        System.out.println("下单占座队列【" + queueName + "】RabbitMQ-----====rabbitmqBean=" + rabbitmqBean);
        if (rabbitmqBean != null) {
            System.out.println("下单占座队列【" + queueName + "】RabbitMQ-----====平台=" + rabbitmqBean.toString());
            System.out.println("下单占座队列【" + queueName + "】RabbitMQ-----====准备启动" + threadNum + "个消费者 ");
            for (int i = 0; i < threadNum; i++) {//启动多少个消费者
                //new RabbitQueueConsumerCreateOrderV3(name, type).start();//老的消费实际方法实现
                //新的消费者
                monitoringSupply.newRabbitQueueConsumerCreateOrderV3ByHost(queueName, pingtaiType, rabbitmqBean);//使用host启动的消费者
            }
        }
        else {
            System.out.println("下单占座队列获取MQ信息失败");
        }
        */

        //监控开始
        //new MonitoringRabbitMQCustomer(kt110, pingtaiType, consumerType).start();//开启下单消费者监控
        new ConsumerMaintenance(PublicConnectionInfos.RABBITWAITORDER,
                MQPlatformTypeEnum.getMQPlatformTypeEnumByType(pingtaiType), kt110) {

            @Override
            public void addCustomerCount(int count, RabbitmqBean rabbitmqBean) {
                // TODO Auto-generated method stub
                String pingtaiString = String.valueOf(pingtaiType);
                for (int i = 0; i < count; i++) {
                    new RabbitQueueConsumer(PublicConnectionInfos.RABBITWAITORDER, pingtaiString,
                            rabbitmqBean.getHost(), rabbitmqBean.getUsername(), rabbitmqBean.getPassword(),
                            "RabbitQueueMQ_TrainCreateOrder_log") {

                        @Override
                        public String execMethod(String notice) {
                            // TODO Auto-generated method stub
                            WriteLog.write(
                                    RabbitQueueConsumerCreateOrderV3.class.getSimpleName(),
                                    System.currentTimeMillis() + ":"
                                            + TimeUtil.dateToString(new Date(), TimeUtil.yyyyMMddHHmmss) + ":【"
                                            + notice + "】");
                            long orderid = Long.valueOf(notice);
                            System.out.println(TimeUtil.dateToString(new Date(), TimeUtil.yyyyMMddHHmmss) + ":准备处理订单:"
                                    + orderid);
                            WriteLog.write(RabbitQueueConsumerCreateOrderV3.class.getSimpleName(),
                                    System.currentTimeMillis() + ":orderid:" + orderid);
                            if (orderid > 0) {
                                new TrainCreateOrder(orderid).createOrderStart(new Customeruser(), false);//执行下单的逻辑
                            }
                            return notice + "--->下单";
                        }
                    }.start();
                }
            }
        }.start();
        //监控结束
    }

    /**
     * 获取当前使用的哪个mq服务的对象
     * 
     * @return
     * @time 2017年2月17日 下午2:12:36
     * @author chen
     */
    public RabbitmqBean getRabbitmqBean() {
        String pingTaiType = PropertyUtil.getValue("pingTaiType", "rabbitMQ.properties");
        RabbitmqBean rabbitmqBean = RabbitMqUtilNew.getRabbitmqBean(pingTaiType);

        //        //获取一个数据库配置的rabbitMq记录
        //        List list = Server.getInstance().getSystemService()
        //                .findMapResultByProcedure("[dbo].[MQConfiguration_selectUseingMq]");
        //        if(list.size()>0){
        //            Map map = (Map) list.get(0);
        //            String host = map.get("MqHost")==null?"":map.get("MqHost").toString();
        //            String username = map.get("UserName")==null?"":map.get("UserName").toString();
        //            String password = map.get("PassWord")==null?"":map.get("PassWord").toString();
        //            rabbitmqBean = new RabbitmqBean(host, username, password);
        //        }
        return rabbitmqBean;
    }

    /**
     * 获取到初始化消费者的数量
     * 
     * @return
     * @time 2017年1月10日 上午11:18:38
     * @author chen
     */
    private int initThreadNum() {
        // 此处会引起tomcat 报错 忽略掉 程序正确运行
        String numString = "";
        try {
            numString = getInitParameter("threadNum");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        if (numString == null || "".equals(numString)) {
            threadNum = 1;
        }
        else {
            threadNum = Integer.valueOf(numString);
        }
        return threadNum;
    }
}
