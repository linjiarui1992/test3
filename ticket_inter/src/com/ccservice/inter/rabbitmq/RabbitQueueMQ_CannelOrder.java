package com.ccservice.inter.rabbitmq;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.db.DBHelper;
import com.ccservice.Util.db.DBSourceEnum;
import com.ccservice.Util.db.DataRow;
import com.ccservice.Util.db.DataTable;
import com.ccservice.Util.file.ExceptionUtil;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.b2b2c.atom.service12306.AccountSystem;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.train.TrainStudentInfo;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.train.thread.TrainCreateOrderSupplyMethod;
import com.ccservice.inter.job.train.thread.db.TrainCreateOrderDBMethod;
import com.ccservice.mq.consumer.ConsumerMaintenance;
import com.ccservice.mq.consumer.RabbitQueueConsumer;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.MQPlatformTypeEnum;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.PublicConnectionInfos;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.RabbitmqBean;
import com.ggjs.config.netty.InitServer;

/**
 * 
 * 取消订单队列开启
 * @author 之至
 * @time 2016年11月19日 下午5:34:53
 */
public class RabbitQueueMQ_CannelOrder extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    public void init() throws ServletException {
        super.init();
        System.out.println("取消订单队列开启");
        trainOrderCancel();
    }

    int threadNum = 0;

    private void trainOrderCancel() {
        System.out.println("取消订单队列RabbitMQ-----！！！初始化！！！=" + this.threadNum + "个消费者 ");
        String kt110 = PropertyUtil.getValue("kt110", "rabbitMQ.properties");//空铁|同程 的【消费者TOMCAT编号】
        int pingtaiType = Integer.parseInt(PropertyUtil.getValue("pingTaiType", "rabbitMQ.properties"));//平台类型 1=空铁 2=同程
        int consumerType = PublicConnectionInfos.getConsumerType(PublicConnectionInfos.QUEUEMQ_CANCELORDER);//消费者类型0代表测试 1-13代表自己的业务 1代表下单消费者
        WriteLog.write("消费者启动记录", this.getClass().getSimpleName() + ":kt110:" + kt110 + ":queueName:"
                + PublicConnectionInfos.QUEUEMQ_CANCELORDER + ":pingtaiType:" + pingtaiType + ":consumerType:"
                + consumerType);
        System.out.println("取消订单队列【" + PublicConnectionInfos.QUEUEMQ_CANCELORDER + "】【consumerType:" + consumerType
                + "】【kt110:" + kt110 + "】RabbitMQ-----====平台=" + pingtaiType + "【空铁1,同程2】 ");
        //初始化参数结束
        //监控开始
        //new MonitoringRabbitMQCustomer(kt110, pingtaiType, consumerType).start();//开启下单消费者监控
        new ConsumerMaintenance(PublicConnectionInfos.QUEUEMQ_CANCELORDER,
                MQPlatformTypeEnum.getMQPlatformTypeEnumByType(pingtaiType), kt110) {

            @Override
            public void addCustomerCount(int count, RabbitmqBean rabbitmqBean) {
                String pingtaiString = String.valueOf(pingtaiType);
                for (int i = 0; i < count; i++) {
                    new RabbitQueueConsumer(PublicConnectionInfos.QUEUEMQ_CANCELORDER, pingtaiString,
                            rabbitmqBean.getHost(), rabbitmqBean.getUsername(), rabbitmqBean.getPassword(),
                            "RabbitQueueMQ_CannelOrder_log") {

                        @Override
                        public String execMethod(String notice) {
                            long l1 = System.currentTimeMillis();
                            TrainCreateOrderSupplyMethod TrainCreateOrderSupplyMethod = new TrainCreateOrderSupplyMethod();
                            WriteLog.write("Rabbit取消订单MQ", "进入消费者取消~~");
                            Customeruser use = new Customeruser();
                            boolean cancelIsTrue = false;
                            String orderNoticeResult = notice;
                            WriteLog.write("Rabbit取消订单MQ", l1 + ":" + "MQ传送值：" + orderNoticeResult);
                            JSONObject jsonObject = new JSONObject();
                            jsonObject = JSONObject.parseObject(orderNoticeResult);
                            String extnumber = jsonObject.containsKey("extnumber") ? jsonObject.getString("extnumber")
                                    : "";
                            String trainorderid = jsonObject.containsKey("trainorderid") ? jsonObject
                                    .getString("trainorderid") : "0";
                            //            String freeAccount = jsonObject.containsKey("freeAccount") ? jsonObject.getString("freeAccount") : "false";
                            long orderid = Long.parseLong(trainorderid);
                            Trainorder order = findTrainorder(orderid);
 
                            String result = "";
                            WriteLog.write("Rabbit取消订单MQ",
                                    l1 + ":" + "MQ订单号：" + trainorderid + ",查询订单号：" + order.getId());
                            use = TrainCreateOrderSupplyMethod.getCustomerUserBy12306Account(order, false);//根据订单获取账号
                            WriteLog.write("Rabbit取消订单MQ", l1 + ":" + "查询订单号：" + order.getSupplyaccount() + "账号获取："
                                    + use.getLoginname());
                            int i = 0;
                            do {
                                WriteLog.write("Rabbit取消订单MQ", l1 + ":" + "取消订单参数：票号:" + extnumber + ",订单号:" + orderid);
                                result = new TrainCreateOrderSupplyMethod().cancel12306Order(use, extnumber, orderid,
                                        false);
                                WriteLog.write("Rabbit取消订单MQ", l1 + ":" + "取消订单结果:" + result);
                                if (result.contains("取消订单成功") || "无未支付订单".equals(result)) {
                                    cancelIsTrue = true;
                                }
                                i++;
                                WriteLog.write("Rabbit取消订单MQ", l1 + ":" + trainorderid + "取消失败，消费者取消循环次数：" + i
                                        + "--->取消,返回:" + result);
                            }
                            while (!cancelIsTrue && i < 5);
                            //释放账号
                            if(use.getLoginname()!=null) {
                                TrainCreateOrderSupplyMethod.freeCustomeruser(use, cancelIsTrue ? AccountSystem.FreeNoCare
                                        : AccountSystem.FreeCurrent, AccountSystem.OneFree, AccountSystem.OneCancel,
                                        AccountSystem.NullDepartTime);    
                            }
                            return notice + "--->取消订单";
                        }
                    }.start();
                }
            }
        }.start();
        //监控结束
    }
    public static void main(String[] args) {
        /*Map map = new HashMap(); 
        
        map.put("asdf", 123123);
        System.out.println(map);*/
        
        InitServer.start();
        Trainorder findTrainorder = new RabbitQueueMQ_CannelOrder().findTrainorder(114233512);
//        Trainorder findTrainorder = new RabbitQueueMQ_CannelOrder().findTrainorder(117214962);
        System.out.println(findTrainorder.getId());
    }
    
    private static DBSourceEnum dbSourceEnum = DBSourceEnum.TONGCHENG_DB;
    
    /**
     * @describe 查找订单信息
     */
    public static Trainorder findTrainorder(long orderid) {
        //根据sql 查找唯一的订单信息
        String sql = "[sp_TRAINORDER_selectTrainOrderByOrderId]  @orderId=" + orderid;
        Trainorder trainorder = null;
        try {
            DataTable table = DBHelper.GetDataTable(sql, dbSourceEnum);
            if (table != null) {
                List<DataRow> orderMessages = table.GetRow();
                if (orderMessages != null && !orderMessages.isEmpty()) {
                    TrainCreateOrderDBMethod trainCreateOrderDBMethod = new TrainCreateOrderDBMethod();
                    List<Trainpassenger> trainpassengers = new ArrayList<>();
                    for (DataRow row : orderMessages) {
                        if (trainorder == null) {
                            //**********************************填充 订单信息 开始****************************************************//
                            trainorder = new Trainorder();
                            //订单id
                            trainCreateOrderDBMethod.paddingOrder(trainorder, row);
                            //**********************************填充 订单信息 结束****************************************************//
                        }
                        //**********************************填充 乘客信息 结束****************************************************//
                        if (trainCreateOrderDBMethod.isNotExistTrainPassengers(row, trainpassengers)) {
                            Trainpassenger trainpassenger = new Trainpassenger();
                            //乘客中的订单id
                            trainCreateOrderDBMethod.paddingPassenger(row, trainpassenger);
                            //**********************************填充 乘客信息 结束****************************************************//

                            //**********************************填充 学生信息 开始****************************************************//
                            if (row.GetColumn("SIID").GetValue() != null) {
                                List<TrainStudentInfo> trainStudentInfos = new ArrayList<TrainStudentInfo>();
                                if (trainCreateOrderDBMethod.isNotExistTrainStudents(row, trainStudentInfos)) {
                                    TrainStudentInfo trainStudentInfo = new TrainStudentInfo();
                                    trainCreateOrderDBMethod.paddingStudent(row, trainStudentInfo);
                                    trainStudentInfos.add(trainStudentInfo);
                                }
                                trainpassenger.setTrainstudentinfos(trainStudentInfos);
                            }
                            //**********************************填充 学生信息 结束****************************************************//

                            //**********************************填充 车票信息 开始****************************************************//
                            List<Trainticket> traintickets = new ArrayList<Trainticket>();
                            if (trainCreateOrderDBMethod.isNotExistTrainTickets(row, traintickets)) {
                                Trainticket trainticket = new Trainticket();
                                trainCreateOrderDBMethod.paddingTicket(row, traintickets, trainticket);
                                traintickets.add(trainticket);
                            }
                            trainpassenger.setTraintickets(traintickets);
                            trainpassengers.add(trainpassenger);
                            //**********************************填充 车票信息 结束****************************************************//
                        }
                        trainorder.setPassengers(trainpassengers);
                    }
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            ExceptionUtil.writelogByException("RabbitQueueMQ_CannelOrder_FindTrainorder_Exception", e,
                    orderid + "-->" + sql);
        }
        return trainorder;
    }
    
    public void trainOrderCancel_back() {
        String queryName = PropertyUtil.getValue("QueueMQ_CancelOrder", "rabbitMQ.properties");
        try {
            int threadNum = Integer.parseInt(getInitParameter("threadNum"));
            for (int i = 0; i < threadNum; i++) {
                RabbitQueueConsumerCannelOrder consumerQueryOrder = new RabbitQueueConsumerCannelOrder(queryName);
                Thread thread = new Thread(consumerQueryOrder);
                thread.start();
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
