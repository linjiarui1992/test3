package com.ccservice.inter.rabbitmq;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;





import org.apache.commons.lang3.SerializationUtils;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.server.Server;
import com.ccservice.rabbitmq.util.QueueConsumer;
import com.ccservice.train.qunar.TrainorderDeduction;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.AMQP.BasicProperties;

/**
 * 
 * @author zlx
 * @time 2016年11月18日 上午9:49:06
 * @Description TODO
 *美团云扣款队列  消费者
 */
public class RabbitQueueConsumerTrainDeduction extends QueueConsumer implements Runnable,Consumer{

    private long orderid;

    private int type;

    public RabbitQueueConsumerTrainDeduction(String endPointName) throws IOException {
        super(endPointName);
    }

    public void handleDelivery(String consumerTag, Envelope env, BasicProperties props, byte[] body) throws IOException {
        try {
            Map<?, ?> map = (HashMap<?, ?>) SerializationUtils.deserialize(body);
            String orderNoticeResult = map.get("message number").toString();
            System.out.println("扣款队列  接收 ：————————>"+map.get("message number").toString());
            WriteLog.write("Rabbit_QueueMQ_TrainDeduction", "orderNoticeResult:" + orderNoticeResult);
            JSONObject jsonobject = JSONObject.parseObject(orderNoticeResult);
            this.orderid = jsonobject.containsKey("orderid") ? jsonobject.getLongValue("orderid") : 0;
            this.type = jsonobject.containsKey("type") ? jsonobject.getIntValue("type") : 0;
            WriteLog.write("Rabbit_QueueMQ_TrainDeduction", "orderid:" + this.orderid + "type:" + this.type);
            new TrainorderDeduction(this.orderid, type).tongchengDeduction();
          
            //同程扣款编号   注意配置文件的名称！！！！！！！！！！！！！！！！！
            String kt110 = PropertyUtil.getValue("kt110", "rabbitMQ.properties");
            //平台
            String pingTaiType = PropertyUtil.getValue("pingTaiType", "rabbitMQ.properties");
            List list = Server.getInstance().getSystemService().findMapResultBySql("select Status from [TrainPingTai] with(nolock)  where PingTai ="+pingTaiType+"and type ="+kt110, null);
            Map map1=(Map)list.get(0);
            String StatusString= map1.containsKey("Status")?map1.get("Status").toString():"";
            if (!"".equals(StatusString)&&"0".equals(StatusString)) {
                rmcb.getChannel().close();
                rmcb.getConnection().close();
                System.out.println("关闭消费者-----------");
                return;
            }
        }
        catch (Exception e) {
            WriteLog.write("Rabbit_QueueMQ_TrainDeduction_Exception", "orderid:" + this.orderid + "type:" + this.type+" "+e.getMessage());
        }
    }

}
