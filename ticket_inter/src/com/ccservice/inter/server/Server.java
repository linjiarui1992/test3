﻿package com.ccservice.inter.server;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.atom.service.IAtomService;
import com.ccservice.b2b2c.atom.service.IBussHotelService;
import com.ccservice.b2b2c.atom.service.IELongHotelService;
import com.ccservice.b2b2c.atom.service.IHMHotelService;
import com.ccservice.b2b2c.atom.service.IHotelCacheService;
import com.ccservice.b2b2c.atom.service.IJLHotelService;
import com.ccservice.b2b2c.atom.service.ITaoBaoHotelSerice;
import com.ccservice.b2b2c.atom.service.ITicketSearchService;
import com.ccservice.b2b2c.atom.service.interfacetype.ITrainInterfaceTypeService;
import com.ccservice.b2b2c.atom.service.travelskyhotel.ITravelskyHotelService;
import com.ccservice.b2b2c.atom.service12306.ITrain12306Service;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.service.IAirService;
import com.ccservice.b2b2c.base.service.IB2BAirticketService;
import com.ccservice.b2b2c.base.service.IB2BSystemService;
import com.ccservice.b2b2c.base.service.IHotelService;
import com.ccservice.b2b2c.base.service.IInterhotelService;
import com.ccservice.b2b2c.base.service.IMemberService;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.b2b2c.base.service.ITrainService;
import com.ccservice.b2b2c.policy.ben.RepServerBean;
import com.ccservice.service.ISearchFlightService;

public class Server {
    private static Server server = null;

    private HessianProxyFactory factory = new HessianProxyFactory();

    private String url;

    private String urlAtom;

    private String cnserviceurl;

    private String isAutoPay;

    private String createPnr;

    private String isCreateExtOrder;

    private String isCreateOrderToYeebooking;

    private Map<String, String> dateHashMap = new HashMap<String, String>();

    private List<Customeruser> customeruser12306account = new ArrayList<Customeruser>();

    private int job8000yicount = 0;

    //虚拟值，从0开始，用于取打码REP服务器
    private long DamaRepIdx;

    //虚拟值，从0开始，用于顺序取REP服务器
    private long TrainRepIdx;

    private List<String> budanids = new ArrayList<String>();

    // REP服务器，用于12306下单
    public List<RepServerBean> RepServers = new ArrayList<RepServerBean>();

    public static int regIdx = 0;

    //注册的repurl最后使用时间
    public static Map<String, Long> RegWlfmURLMapLastUseTime = new HashMap<String, Long>();

    //注册的repurl 网络繁忙 次数
    public static Map<String, Integer> RegWlfmURLMapCount = new HashMap<String, Integer>();

    //注册的repurl errorcode 次数
    public static Map<String, Integer> RegErrorcodeURLMapCount = new HashMap<String, Integer>();

    public static Map<String, String> ProxyIpList = new HashMap<String, String>();

    /**
     * adsl是否在线
     */
    public static boolean adslIaonLine = true;

    public static int dangqianheyancount;

    /**
     * 最后拨号时间
     */
    public static Long adslLastDialingTime = 0L;

    public int getJob8000yicount() {
        return job8000yicount;
    }

    public void setJob8000yicount(int job8000yicount) {
        this.job8000yicount = job8000yicount;
    }

    private Server() {

    }

    public static Server getInstance() {
        if (server == null) {

            server = new Server();
        }
        return server;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public IAirService getAirService() {
        try {
            return (IAirService) factory.create(IAirService.class, url
                    + IAirService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public IInterhotelService getInterHotelService() {
        try {
            return (IInterhotelService) factory.create(
                    IInterhotelService.class,
                    url + IInterhotelService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public IHotelService getHotelService() {
        try {
            return (IHotelService) factory.create(IHotelService.class, url
                    + IHotelService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public IHotelCacheService getHotelCacheService() {
        try {
            factory.setOverloadEnabled(true);
            return (IHotelCacheService) factory.create(
                    IHotelCacheService.class, urlAtom
                            + IHotelCacheService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            return null;
        }
    }

    public ITaoBaoHotelSerice getTaoBaoHotelSerice() {
        try {
            return (ITaoBaoHotelSerice) factory.create(
                    ITaoBaoHotelSerice.class, urlAtom
                            + ITaoBaoHotelSerice.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 中航信酒店
     */
    public ITravelskyHotelService getTravelskyHotelService() {
        try {
            factory.setOverloadEnabled(true);
            return (ITravelskyHotelService) factory.create(
                    ITravelskyHotelService.class, urlAtom
                            + ITravelskyHotelService.class.getSimpleName());
        }
        catch (Exception e) {
            return null;
        }
    }

    /**
     * 华闽酒店
     * 
     * @return
     */
    public IHMHotelService getIHMHotelService() {
        try {
            factory.setOverloadEnabled(true);
            return (IHMHotelService) factory.create(IHMHotelService.class,
                    urlAtom + IHMHotelService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public IELongHotelService getIELongHotelService() {
        try {
            factory.setOverloadEnabled(true);
            return (IELongHotelService) factory.create(
                    IELongHotelService.class, urlAtom
                            + IELongHotelService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public ITicketSearchService getTicketSearchService() {
        try {
            return (ITicketSearchService) factory.create(
                    ITicketSearchService.class, urlAtom
                            + ITicketSearchService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public IMemberService getMemberService() {
        try {
            return (IMemberService) factory.create(IMemberService.class, url
                    + IMemberService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public ISystemService getSystemService() {
        try {
            return (ISystemService) factory.create(ISystemService.class, url
                    + ISystemService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public ISystemService getSystemService(String url) {
        try {
            return (ISystemService) factory.create(ISystemService.class, url
                    + ISystemService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * cnserviceurl
     * 
     * @return
     * @time 2014年10月9日 下午5:28:05
     * @author chendong
     */
    public IAtomService getAtomService() {
        try {
            return (IAtomService) factory.create(IAtomService.class,
                    cnserviceurl + IAtomService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * urlAtom
     * 
     * @return
     * @time 2014年10月9日 下午5:27:51
     * @author chendong
     */
    public IAtomService getAtomService2() {
        try {
            return (IAtomService) factory.create(IAtomService.class, urlAtom
                    + IAtomService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public ITrainService getTrainService() {
        try {
            return (ITrainService) factory.create(ITrainService.class, url
                    + ITrainService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            System.out.println("****调用TrainService出现异常：");
            e.printStackTrace();
            return null;
        }
    }

    public ITrainService getTrainService(String url) {
        try {
            return (ITrainService) factory.create(ITrainService.class, url
                    + ITrainService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            System.out.println("****调用TrainService出现异常：");
            e.printStackTrace();
            return null;
        }
    }

    // 51Book接口调用
    public ISearchFlightService getSearchFiveoneFlightService() {
        try {
            return (ISearchFlightService) factory.create(
                    ISearchFlightService.class, cnserviceurl
                            + ISearchFlightService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public IJLHotelService getIJLHotelService() {
        try {
            factory.setOverloadEnabled(true);
            return (IJLHotelService) factory.create(IJLHotelService.class,
                    urlAtom + IJLHotelService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public IBussHotelService getIBussHotelService() {
        try {
            factory.setOverloadEnabled(true);
            return (IBussHotelService) factory.create(IBussHotelService.class,
                    urlAtom + IBussHotelService.class.getSimpleName());
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public ITrain12306Service getTrain12306Service() {
        try {
            return (ITrain12306Service) factory.create(
                    ITrain12306Service.class, urlAtom
                            + ITrain12306Service.class.getSimpleName());
        }
        catch (Exception e) {
            return null;
        }
    }

    public ITrainInterfaceTypeService getInterfaceTypeService() {
        try {
            return (ITrainInterfaceTypeService) factory.create(
                    ITrainInterfaceTypeService.class, urlAtom
                            + ITrainInterfaceTypeService.class.getSimpleName());
        }
        catch (Exception e) {
            return null;
        }
    }

    public String geturlAtom() {
        return urlAtom;
    }

    public void seturlAtom(String urlAtom) {
        this.urlAtom = urlAtom;
    }

    public static Server getServer() {
        return server;
    }

    public static void setServer(Server server) {
        Server.server = server;
    }

    public String getCnserviceurl() {
        return cnserviceurl;
    }

    public void setCnserviceurl(String cnserviceurl) {
        this.cnserviceurl = cnserviceurl;
    }

    public String getIsAutoPay() {
        return isAutoPay;
    }

    public void setIsAutoPay(String isAutoPay) {
        this.isAutoPay = isAutoPay;
    }

    public void setCreatePnr(String createPnr) {
        this.createPnr = createPnr;
    }

    public String getCreatePnr() {
        return createPnr;
    }

    public String getIsCreateExtOrder() {
        return isCreateExtOrder;
    }

    public void setIsCreateExtOrder(String isCreateExtOrder) {
        this.isCreateExtOrder = isCreateExtOrder;
    }

    public String getIsCreateOrderToYeebooking() {
        return isCreateOrderToYeebooking;
    }

    public void setIsCreateOrderToYeebooking(String isCreateOrderToYeebooking) {
        this.isCreateOrderToYeebooking = isCreateOrderToYeebooking;
    }

    public Map<String, String> getDateHashMap() {
        return dateHashMap;
    }

    public void setDateHashMap(Map<String, String> dateHashMap) {
        this.dateHashMap = dateHashMap;
    }

    public IB2BAirticketService getB2BAirticketService() {
        try {
            return (IB2BAirticketService) factory.create(
                    IB2BAirticketService.class, url
                            + IB2BAirticketService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            return null;
        }
    }

    public List<Customeruser> getCustomeruser12306account() {
        return customeruser12306account;
    }

    public void setCustomeruser12306account(
            List<Customeruser> customeruser12306account) {
        this.customeruser12306account = customeruser12306account;
    }

    public List<String> getBudanids() {
        return this.budanids;
    }

    public void setBudanids(List<String> budanids) {
        this.budanids = budanids;
    }

    public void doRequest(final String str) {
        /** 
         * 启动新线程来处理请求 
         */
        new Thread() {
            public void run() {
                //调用处理类来处理请求  

            }
        }.start();
    }

    public long getTrainRepIdx() {
        return TrainRepIdx;
    }

    public void setTrainRepIdx(long trainRepIdx) {
        this.TrainRepIdx = trainRepIdx;
    }

    public long getDamaRepIdx() {
        return DamaRepIdx;
    }

    public void setDamaRepIdx(long damaRepIdx) {
        this.DamaRepIdx = damaRepIdx;
    }

    public List<RepServerBean> getRepServers() {
        return RepServers;
    }

    public void setRepServers(List<RepServerBean> repServers) {
        RepServers = repServers;
    }

    public IB2BSystemService getB2BSystemService() {
        try {
            return (IB2BSystemService) factory.create(IB2BSystemService.class,
                    url + IB2BSystemService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            return null;
        }
    }

}
