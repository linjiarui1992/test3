package com.ccservice.inter.server;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccervice.util.db.DBHelper;
import com.ccervice.util.db.DataRow;
import com.ccervice.util.db.DataTable;


public class ServerMo extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        res.setCharacterEncoding("UTF-8");
        res.setContentType("text/plain; charset=utf-8");
        res.setHeader("content-type", "text/html;charset=UTF-8");
        PrintWriter out = res.getWriter();
        String sqlString = "SELECT * FROM Reg12306MonitorBean with(nolock)";
        DataTable myDt = null;
        try {
            myDt = DBHelper.GetDataTable(sqlString);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        List<DataRow> dataRows = myDt.GetRow();
        if (dataRows.size() > 0) {
            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < dataRows.size(); i++) {
                DataRow datarow = dataRows.get(i);
                String Describe = datarow.GetColumnString("Describe");
                String RegistrationTime =datarow.GetColumnTime("RegistrationTime").toString();
                String RegistrationType = datarow.GetColumnString("RegistrationType");
                String Adsl = datarow.GetColumnString("Adsl");
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("Describe", Describe);
                jsonObject.put("RegistrationTime", RegistrationTime);
                jsonObject.put("RegistrationType", RegistrationType);
                jsonObject.put("Adsl", Adsl);
                jsonArray.add(jsonObject);
            }
            JSONObject jo = new JSONObject();
            jo.put("jsonStr", jsonArray);
            out.write(jo.toString());
            out.flush();
            out.close();
        }
    }
}


