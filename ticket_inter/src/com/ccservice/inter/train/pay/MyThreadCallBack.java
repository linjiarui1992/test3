package com.ccservice.inter.train.pay;

import java.util.HashMap;
import java.util.Map;

import com.ccservice.inter.job.WriteLog;
import com.ccservice.util.httpclient.HttpsClientUtils;

/**
 * 
 * @author wzc
 * 支付异步回调
 *
 */
public class MyThreadCallBack extends Thread {
    int t;

    String payresult;

    String callbackurl;

    long orderid;

    String cmd;

    public MyThreadCallBack(int t, String payresult, String callbackurl, long orderid, String cmd) {
        this.t = t;
        this.payresult = payresult;
        this.callbackurl = callbackurl;
        this.orderid = orderid;
        this.cmd = cmd;
    }

    @Override
    public void run() {
        callBackPayResult(t, payresult, callbackurl, orderid, cmd);
    }

    public void callBackPayResult(int t, String payresult, String callbackurl, long orderid, String cmd) {
        //回调支付结果
        try {
            WriteLog.write("t_paycallback", t + ":cmd:" + payresult);
            Map listdata = new HashMap();
            listdata.put("data", payresult);
            listdata.put("cmd", cmd);
            listdata.put("orderid", "" + orderid);
            String backdata = HttpsClientUtils.posthttpclientdata(callbackurl, listdata, 70000l);
            WriteLog.write("t_paycallback", t + ":cmd：" + backdata);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
