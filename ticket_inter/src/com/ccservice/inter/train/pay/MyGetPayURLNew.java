package com.ccservice.inter.train.pay;

import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.RepServerUtil;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.server.Server;
import com.ccservice.train.mqlistener.MQMethod;
import com.ccservice.train.mqlistener.TrainpayMqMSGUtil;
import com.ccservice.util.httpclient.HttpsClientUtils;

/**
 * 
 * 
 * @time 2015-5-18 15:45
 * @author wzc
 * 获取支付链接线程
 */
public class MyGetPayURLNew {

    private long orderid;//订单id

    private String loginname;//12306帐号

    private String cookieString;//12306登录cookie

    private String callbackurl;//回调地址

    private String mqname;//队列名称

    private int t;//随机数

    private String paymethodtype;//支付方式

    private String extnumber;//12306订单号

    private TrainSupplyMethod trainSupplyMethod;

    private String ordernumber;

    private String password;

    private long ordersource;

    private long urlgetflag;//1 获取链接并支付   0  只获取链接

    private float price;

    public MyGetPayURLNew(long orderid, String loginname, String cookieString, String callbackurl, String mqname,
            int t, String paymethodtype, String extnumber, String ordernumber, String password, long ordersource,
            long urlgetflag, float price) {
        this.orderid = orderid;
        this.loginname = loginname;
        this.cookieString = cookieString;
        this.callbackurl = callbackurl;
        this.mqname = mqname;
        this.t = t;
        this.paymethodtype = paymethodtype;
        this.extnumber = extnumber;
        this.trainSupplyMethod = new TrainSupplyMethod();
        this.ordernumber = ordernumber;
        this.password = password;
        this.ordersource = ordersource;
        this.urlgetflag = urlgetflag;
        this.price = price;
    }

    /**
     * 获取支付链接
     * @return
     */
    public String getPayUrl() {
        String result = "";
        String payurlbefore = getPayurl(orderid, ordersource);
        if (payurlbefore != null && payurlbefore.contains("gateway.do")) {
            result = payurlbefore;
            WriteLog.write("12306获取支付链接_MQ", "二次支付结果:" + result + ",支付方式:" + paymethodtype + ",Extnumber订单号:"
                    + extnumber);
        }
        else {
            if ("1".equals(paymethodtype)) {//获取银联支付链接
                result = orderpaymentunionpayurl(cookieString, loginname, orderid, extnumber, price);
            }
            else if ("2".equals(paymethodtype)) {//获取支付宝支付链接
                result = orderpayment(cookieString, loginname, orderid, extnumber, price);
            }
        }
        if (result != null && !"".equals(result) && (result.contains("gateway.do") || result.contains("Pay.action"))) {
            JSONObject jso = new JSONObject();
            if (urlgetflag == 1) {
                WriteLog.write("12306获取支付链接_MQ", "结果:" + result + ",支付方式:" + paymethodtype + ",Extnumber订单号:"
                        + extnumber);
                jso.put("orderid", orderid);
                jso.put("paytimes", "3");
                jso.put("callbackurl", callbackurl);//回调地址
                jso.put("postdata", result);
                jso.put("paytype", "2");
                jso.put("ordernumber", ordernumber);
                WriteLog.write("12306获取支付链接_MQ_sendpay", t + ":" + ordernumber + ":" + jso.toJSONString());
                new TrainpayMqMSGUtil(MQMethod.ORDERPAY_NAME).sendPayMQmsgPublic(mqname, jso.toJSONString());
                WriteLog.write("12306获取支付链接_MQ", ordernumber + " : " + "获取支付链接并支付");
            }
            else {
                WriteLog.write("12306获取支付链接_MQ", ordernumber + " : " + "获取链接,无需支付");
            }
            try {
                commitData(orderid, ordersource, ordernumber, result, payurlbefore, jso.toJSONString());
            }
            catch (Exception e) {
            }
            return "success";
        }
        return "error";
    }

    /**
     * 
     * @param orderid
     * @param ordersource
     * @param ordernumber
     * @param alipayurl
     * @param callbackurl
     * @return
     */
    @SuppressWarnings("rawtypes")
    public String commitData(long orderid, long ordersource, String ordernumber, String alipayurl, String callbackurl,
            String jsonstr) {
        String wnumber = "";
        if (alipayurl.contains("gateway.do")) {
            wnumber = searWnumber(alipayurl);
        }
        String sql = "select alipayurl from t_trainpayrecord with(nolock) where orderid=" + orderid
                + " and ordersource=" + ordersource;
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        WriteLog.write("12306获取支付链接_MQ_保存数据", ordernumber + " : " + jsonstr);
        if (list.size() <= 0) {
            String insertsql = "insert into T_TRAINPAYRECORD(ORDERID,ORDERSOURCE,ORDERNUMBER,ALIPAYURL,CALLBACKURL,ALIPAYWNUMBER) values ("
                    + orderid
                    + ","
                    + ordersource
                    + ",'"
                    + ordernumber
                    + "','"
                    + alipayurl
                    + "','"
                    + callbackurl
                    + "','" + wnumber + "')";
            WriteLog.write("12306获取支付链接_MQ_保存数据", ordernumber + " : " + insertsql);
            Server.getInstance().getSystemService().findMapResultBySql(insertsql, null);
        }
        else {
            WriteLog.write("12306获取支付链接_MQ_保存数据", ordernumber + " : " + "获取链接,无需支付");
        }
        return "";
    }

    /**
     *交通获取交易金额 
     * @param args
     * @time 2014年12月4日 下午8:01:13
     * @author wzc
     */
    public static String searWnumber(String params) {
        String retrunval = "";
        try {
            Pattern p = Pattern.compile("ord_id_ext=W\\d*");
            Matcher m = p.matcher(params);
            retrunval = "";
            if (m.find()) {
                String count = m.group();
                retrunval = count.replace("ord_id_ext=", "");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return retrunval;
    }

    /**
     * 获取支付链接
     * @param orderid
     * @param ordersource
     * @return
     */
    @SuppressWarnings("rawtypes")
    public String getPayurl(long orderid, long ordersource) {
        String url = "";
        String sql = "select alipayurl from t_trainpayrecord with(nolock) where orderid=" + orderid
                + " and ordersource=" + ordersource;
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        if (list.size() > 0) {
            Map map = (Map) list.get(0);
            url = map.get("alipayurl").toString();
        }
        return url;
    }

    /**
     * 获取银联支付链接 
     * @param cookieString 访问cookie
     * @param trainorderid
     * @param loginname
     * @time 2014年12月6日 下午3:52:46
     * @author fiend
     */
    public String orderpaymentunionpayurl(String cookieString, String loginname, long orderid, String extnumber,
            float price) {
        String mresult = "";
        String result = "";
        Random rand = new Random();
        int randnum = rand.nextInt(1000000);
        Customeruser user = new Customeruser();
        user.setFromAccountSystem(true);
        user.setLoginname(loginname);
        user.setCardnunber(cookieString);
        user.setLogpassword(password);
        try {
            String par = "datatypeflag=29&cookie=" + cookieString + "&extnumber=" + extnumber + "&trainorderid="
                    + orderid + "&price=" + price;
            for (int i = 0; i < 10; i++) {
                String url = RepServerUtil.getRepServer(user, false).getUrl();
                WriteLog.write("12306银联自动支付链接", randnum + ":" + extnumber + ":银联访问地址:" + url + "?" + par);
                try {
                    result = SendPostandGet.submitPost(url, par, "UTF-8").toString();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                WriteLog.write("12306银联自动支付链接", randnum + ":循环次数:" + i + ":" + extnumber + ":银联返回数据:" + result);
                if ("用户未登录".equals(result)) {
                    //登录方法返回cookie
                    par = "datatypeflag=29&cookie=" + cookieString + "&extnumber=" + extnumber + "&trainorderid="
                            + orderid + "&price" + price;
                    WriteLog.write("12306银联自动支付链接", randnum + ":" + extnumber + ":银联访问地址:" + url + "?" + par);
                    try {
                        result = SendPostandGet.submitPost(url, par, "UTF-8").toString();
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                    WriteLog.write("12306银联自动支付链接", "2次登录" + extnumber + ":银联返回数据:" + result);
                }
                else if ("无未支付订单".equals(result)) {
                    WriteLog.write("12306银联自动支付链接", randnum + ":" + "无未支付订单" + extnumber + "");
                    break;
                }
                if (result.indexOf("Pay.action") >= 0) {//获取支付链接成功
                    mresult = result;
                    break;
                }
            }
        }
        catch (Exception e) {
            WriteLog.write("12306银联自动支付链接", randnum + ":" + "访问异常:Job12306GetPayUrl:" + e.getMessage());
        }
        return mresult;
    }

    /**
     * 说明:获取支付宝支付订单页面
     * @param isjointrip 是否为联程
     * @return
     * @time 2014年8月30日 上午11:20:49
     * @author yinshubin
     */
    public String orderpayment(String cookieString, String loginname, long orderid, String extnumber, float price) {
        String resultmsg = "";
        int randomnum = new Random().nextInt(10000);
        Customeruser user = new Customeruser();
        user.setFromAccountSystem(true);
        user.setLoginname(loginname);
        user.setCardnunber(cookieString);
        user.setLogpassword(password);
        try {
            cookieString = URLEncoder.encode(cookieString, "UTF-8");
            String par = "datatypeflag=9&cookie=" + cookieString + "&extnumber=" + extnumber + "&trainorderid="
                    + orderid + "&price" + price;
            long t1 = System.currentTimeMillis();
            String result = "";
            for (int i = 0; i < 10; i++) {
                String url = RepServerUtil.getRepServer(user, false).getUrl();
                WriteLog.write("12306支付宝自动支付链接", randomnum + ":超时机制循环次数：" + i + ":" + extnumber + "支付宝访问地址:" + url);
                result = HttpsClientUtils.gethttpclientdata(url + "?" + par, 30000l);
                WriteLog.write("12306支付宝自动支付链接", randomnum + ":循环次数：" + i + ":" + extnumber + "支付宝返回数据:" + result);
                if ("用户未登录".equals(result)) {
                    //获取订单类型
                    Trainorder trainorder = orderDataByOrderid(orderid, loginname);
                    //判断客人账号
                    user.setCustomerAccount(trainorder.getOrdertype() == 3 || trainorder.getOrdertype() == 4);
                    //以未登录释放
                    trainSupplyMethod.FreeNoLogin(user);
                    //账号系统重新获取
                    user = trainSupplyMethod.getCustomerUserBy12306Account(trainorder, true);
                    if (user.getState() == 1) {//登陆成功
                        par = "datatypeflag=9&cookie=" + cookieString + "&extnumber=" + extnumber + "&trainorderid="
                                + orderid + "&price" + price;
                        WriteLog.write("12306支付宝自动支付链接", randomnum + ":" + extnumber + "支付宝访问地址:" + url + "?" + par);
                        try {
                            result = SendPostandGet.submitPost(url, par, "UTF-8").toString();
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }
                        WriteLog.write("12306支付宝自动支付链接", randomnum + ":" + "2次" + extnumber + "支付宝返回数据:" + result);
                    }
                    else {
                        WriteLog.write("12306支付宝自动支付链接", randomnum + ":" + extnumber + ",登录失败，name:" + loginname);
                    }
                }
                if (result.indexOf("gateway.do") >= 0) {
                    //获取链接成功
                    resultmsg = result;
                    break;
                }
            }
            long t2 = System.currentTimeMillis();
            WriteLog.write("12306支付宝自动支付链接", randomnum + ":获取支付链接用时:" + (t2 - t1));
        }
        catch (Exception e) {
            e.printStackTrace();
            WriteLog.write("12306支付宝自动支付链接", randomnum + ":" + "存储支付信息错误:" + e);
        }
        if (user.isFromAccountSystem()) {
            trainSupplyMethod.FreeNoCare(user);//释放
        }
        return resultmsg;
    }

    @SuppressWarnings("rawtypes")
    private Trainorder orderDataByOrderid(long orderid, String loginname) {
        Trainorder order = new Trainorder();
        try {
            String sql = "SELECT ISNULL(ordertype, 1) ordertype, C_QUNARORDERNUMBER, "
                    + "C_AGENTID, ISNULL(C_INTERFACETYPE, 0) C_INTERFACETYPE "
                    + "FROM T_TRAINORDER WITH (NOLOCK) where ID = " + orderid;
            List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
            //查询成功
            if (list.size() > 0) {
                Map map = (Map) list.get(0);
                order.setId(orderid);
                order.setSupplyaccount(loginname);
                order.setAgentid(Long.parseLong(map.get("C_AGENTID").toString()));
                order.setQunarOrdernumber(map.get("C_QUNARORDERNUMBER").toString());
                order.setOrdertype(Integer.valueOf(map.get("ordertype").toString()));
                order.setInterfacetype(Integer.parseInt(map.get("C_INTERFACETYPE").toString()));
            }
        }
        catch (Exception e) {

        }
        return order;

    }
}
