package com.ccservice.inter.train;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ccervice.db.util.MongoHelper;
import com.mongodb.DBObject;

public class MongoLogic {

    private String collection = "Price";

    /**
     * 根据Key来获取Mongo中的价格
     * 
     * @param keyMap
     * @return
     * @time 2015年9月14日 下午4:41:13
     * @author fiend
     */
    public List<DBObject> SelectPrice(Map<String, Object> keyMap) {
        List<DBObject> listTrainPrice = null;
        try {
            listTrainPrice = MongoHelper.getInstance().find(collection, keyMap);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return listTrainPrice;
    }

    /**
     * 
     * 
     * @param key
     * @param trainTrips
     * @param priceMap
     * @time 2015年9月14日 下午4:38:49
     * @author fiend
     */
    public void UpdatePrice(Map<String, Object> keyMap, Map<String, Object> priceMap) {
        try {
            MongoHelper.getInstance().update(collection, priceMap, keyMap);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据始发到达站及时间拼出Key
     * 
     * @param key
     *            (始发站三字码+到达站三字码+时间yyyy-MM-dd)
     * @param trainTrips
     *            (车次)
     * @param id
     * @time 2015年9月14日 下午4:38:49
     * @author fiend
     * @return
     */
    public Map<String, Object> keyMap(String key, String trainTrips) {
        Map<String, Object> query = new HashMap<String, Object>();
        //始发站三字码+到达站三字码+时间yyyy-MM-dd
        query.put("Key", key);
        //车次
        query.put("TrainTrips", trainTrips);
        return query;
    }

}
