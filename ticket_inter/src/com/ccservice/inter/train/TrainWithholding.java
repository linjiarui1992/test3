package com.ccservice.inter.train;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.hotel.WriteLog;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.train.TrainSupplyMethod;

/**
 * 火车票代扣 
 * @time 2015年3月26日 上午11:13:09
 * @author fiend
 */
public class TrainWithholding extends TrainSupplyMethod {
    //代扣地址
    //    private final String URL_WITHHOLDING = "http://121.40.125.114:19022/ccs_interface/AutopayJSON";

    //代扣类型
    private final String CMD_TRAIN = "TRAIN";

    /**
     * 内部调用代付接口 
     * @param username
     * @param key
     * @param ordernumber
     * @return
     * @time 2015年3月26日 下午1:01:05
     * @author fiend
     */
    public TrainWithholeResult withholding(String username, String key, String ordernumber) {
        String URL_WITHHOLDING = PropertyUtil.getValue("TrainWithholding_URL_WITHHOLDING", "train.properties");
        JSONObject req = new JSONObject();
        req.put("username", username);
        req.put("password", key);
        req.put("ordernum", ordernumber);
        req.put("cmd", this.CMD_TRAIN);
        WriteLog.write("TrainWithholding", URL_WITHHOLDING + req.toString());
        System.out.println(URL_WITHHOLDING + req.toString());
        StringBuffer result = SendPostandGet.submitPost(URL_WITHHOLDING, req.toString(), "UTF-8");
        System.out.println(result);
        WriteLog.write("TrainWithholding", URL_WITHHOLDING + req.toString());
        TrainWithholeResult rwr = new TrainWithholeResult();
        try {
            if (result != null && result.toString().length() > 5) {
                WriteLog.write("TrainWithholding", result.toString());
                JSONObject obj = JSONObject.parseObject(result.toString());
                String Remark = obj.getString("Remark");//返回信息
                String OrderNo = obj.getString("OrderNo");
                String statuscode = obj.getString("statuscode");//支付状态
                rwr.setOrderNo(OrderNo);
                rwr.setRemark(Remark);
                rwr.setStatuscode(statuscode);
            }
            else {
                rwr.setOrderNo(ordernumber);
                rwr.setRemark("C");
                //请求或解析异常
                rwr.setStatuscode("Withholding interface exception");
            }
        }
        catch (Exception e) {
            rwr.setOrderNo(ordernumber);
            rwr.setRemark("C");
            //请求或解析异常
            rwr.setStatuscode("The request or the analysis of abnormal");
        }
        return rwr;
    }

    /**
     * 代付先扣款模式   如果是该模式订单 跟进扣款接口返回结果判定true false  如果不是该模式订单 直接返回true
     * @param orderid   订单ID
     * @param orderstatus 订单状态
     * @param ywtype    业务类型 1-出票  2-改签
     * @return
     * @time 2015年3月26日 下午1:14:14
     * @author fiend
     */
    //    public boolean withHoldingBefore(long orderid, int orderstatus, int ywtype) {
    //        WriteLog.write("代扣", orderid + "");
    //        int interfacetype = getOrderAttribution(orderid);
    //        if (TrainInterfaceMethod.WITHHOLDING_BEFORE == interfacetype) {
    //            WriteLog.write("代扣_BEFORE", "符合先扣款模式------>" + orderid);
    //            TrainWithholeResult trainWithholeResult = withholding("test", "test", orderid + "");
    //            if ("S".equalsIgnoreCase(trainWithholeResult.getRemark())) {
    //                WriteLog.write("代扣成功_BEFORE", "代扣成功------>" + orderid);
    //                writeRC(orderid, "代扣成功", "代扣接口", orderstatus, ywtype);
    //                return true;
    //            }
    //            else if ("F".equalsIgnoreCase(trainWithholeResult.getRemark())) {
    //                WriteLog.write("代扣失败_BEFORE", "代扣失败------>" + orderid);
    //                writeRC(orderid, "代扣失败", "代扣接口", orderstatus, ywtype);
    //                return false;
    //            }
    //            else {
    //                //异常 需要客服后台操作 检查代扣是否成功  成功退款
    //                WriteLog.write("代扣异常_BEFORE", "代扣异常------>" + orderid);
    //                writeRC(orderid, "代扣失败", "代扣接口", orderstatus, ywtype);
    //                return false;
    //            }
    //        }
    //        else {
    //            WriteLog.write("代扣_BEFORE", "不适用------>" + orderid);
    //            return true;
    //        }
    //    }

    /**
     * 代付后扣款模式 
     * @param orderid   订单ID
     * @param orderstatus 订单状态
     * @param ywtype    业务类型 1-出票  2-改签
     * @return
     * @time 2015年3月26日 下午1:14:14
     * @author fiend
     */
    public boolean withHoldingAfter(long orderid, int orderstatus, int ywtype) {
        WriteLog.write("TrainWithholding", orderid + "");
        WriteLog.write("TrainWithholding", "符合后扣款模式------>" + orderid);
        TrainWithholeResult trainWithholeResult = withholding("test", "test", orderid + "");
        if ("S".equalsIgnoreCase(trainWithholeResult.getRemark())) {
            WriteLog.write("TrainWithholding", "代扣成功------>" + orderid);
            writeRC(orderid, "代扣成功", "代扣接口", orderstatus, ywtype);
            return true;
        }
        else if ("F".equalsIgnoreCase(trainWithholeResult.getRemark())) {
            WriteLog.write("TrainWithholding", "代扣失败------>" + orderid);
            writeRC(orderid, "代扣失败", "代扣接口", orderstatus, ywtype);
            return false;
        }
        else {
            //异常 需要客服后台操作 检查代扣是否成功  失败重新扣款
            WriteLog.write("TrainWithholding", "代扣异常------>" + orderid);
            writeRC(orderid, "代扣失败", "代扣接口", orderstatus, ywtype);
            return false;
        }
    }

    public static void main(String[] args) {
        String URL_WITHHOLDING = PropertyUtil.getValue("TrainWithholding_URL_WITHHOLDING", "train.properties");
        System.out.println(URL_WITHHOLDING);
        //        new TrainWithholding().withholding("test", "test", "test");
    }
}
