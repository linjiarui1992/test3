package com.ccservice.inter.job.tcreport;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccervice.util.db.DBHelper;
import com.ccervice.util.db.DataRow;
import com.ccervice.util.db.DataTable;
import com.ccservice.inter.job.WriteLog;

public class TongChengReport {

    public static void main(String[] args) throws ParseException {}

    /**
     * 
     * 
     * @param type(0:发布失败的下拉框id,1:发布成功id,2:未发布状态的下拉框id)
     * @param rebatetime
     * @return
     * @throws ParseException
     * @time 2015年10月27日 上午10:32:32
     * @author zcn
     */
    public String TongChengReportData(String rebatetime) throws ParseException {
        JSONObject tongchengreport = new JSONObject();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//小写的mm表示的是分钟          
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");//小写的mm表示的是分钟          
        String sql = "";
        sql = "SELECT top 100 * FROM TrainReport with(nolock) where   rebatetime>='" + rebatetime
                + "' and publicstate=0";//查询所有未发布数据  
        DataTable datatable = DBHelper.GetDataTable(sql, null);
        JSONArray jsonarray = new JSONArray();
        for (int i = 0; i < datatable.GetRow().size(); i++) {
            try {
                JSONObject jsonObject = new JSONObject();
                DataRow dataRow = datatable.GetRow().get(i);
                System.out.println("查询数据行数：" + datatable.GetRow().size() + ",新版本");
                String supplyname = (String) dataRow.GetColumn("supplyname").GetValue();
                Integer Id = (Integer) dataRow.GetColumn("ID").GetValue();
                String order_no = (String) dataRow.GetColumn("refordernumber").GetValue();
                String ticket_no = (String) dataRow.GetColumn("enumber").GetValue();
                Double amount = Double.parseDouble(dataRow.GetColumn("rebatemoney").GetValue().toString());
                Integer settlement_type = (Integer) dataRow.GetColumn("reporttype").GetValue();
                Integer quantity = (Integer) dataRow.GetColumn("ticketcount").GetValue();
                Date rebatetimes = (Date) dataRow.GetColumn("rebatetime").GetValue();
                String date = sdf.format(rebatetimes);
                String settlement_date = sdf1.format(rebatetimes);
                String account_balance = dataRow.GetColumn("rebatebalance").GetValue() == null ? "0"
                        : dataRow.GetColumn("rebatebalance").GetValue().toString();
                Integer publicstate = (Integer) dataRow.GetColumn("publicstate").GetValue();
                Integer order_type = (Integer) dataRow.GetColumn("TicketType").GetValue();
                String UnionId = (String) dataRow.GetColumn("UnionId").GetValue();
                jsonObject.put("supplyname", supplyname);
                jsonObject.put("Id", Id);
                jsonObject.put("refordernumber", order_no);
                jsonObject.put("enumber", ticket_no);
                jsonObject.put("rebatemoney", amount);
                jsonObject.put("reporttype", settlement_type);
                jsonObject.put("ticketcount", quantity);
                jsonObject.put("rebatetime", date);
                jsonObject.put("rebatebalance", account_balance);
                jsonObject.put("publicstate", publicstate);
                jsonObject.put("trade_date", date);
                jsonObject.put("settlement_date", settlement_date);
                jsonObject.put("order_type", order_type);
                jsonObject.put("UnionId", UnionId);
                WriteLog.write("s数据上传", jsonObject.toJSONString());
                jsonarray.add(jsonObject);
            }
            catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        tongchengreport.put("list", jsonarray);
        return tongchengreport.toJSONString();
    }

    /**
     * 调用接口实现数据上传
     * 
     * @time 2015年10月22日 下午6:53:35
     * @author Administrator
     * @throws ParseException 
     */
    public void uploadData(String rebatetime) throws ParseException {
        String json = TongChengReportData(rebatetime);
        JSONObject report = JSONObject.parseObject(json);
        JSONArray data = report.getJSONArray("list");
        if (data.size() <= 0) {
            try {
                System.out.println("系统休息5分钟……");
                Thread.sleep(300000l);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        else {
            for (int i = 0; i < data.size(); i++) {
                try {
                    JSONObject arrayJson = data.getJSONObject(i);
                    String Id = arrayJson.getString("Id");
                    String order_no = arrayJson.getString("refordernumber");
                    String ticket_no = arrayJson.getString("enumber");
                    String amount = arrayJson.getString("rebatemoney");
                    String settlement_type = arrayJson.getString("reporttype");
                    String quantity = arrayJson.getString("ticketcount");
                    String account_balance = arrayJson.getString("rebatebalance");
                    String trade_date = arrayJson.getString("trade_date");
                    String settlement_date = arrayJson.getString("settlement_date");
                    String order_type = arrayJson.getString("order_type");
                    String UnionId = arrayJson.getString("UnionId");
                    String channel = "9";
                    String md5key = "92188153b16d4c0f9c8a3c6520a95432";
                    String sql = "";
                    try {
                        String result = TongChengReportUtil.TongChengReportResult(order_no, ticket_no, amount,
                                settlement_type, quantity, settlement_date, trade_date, channel, account_balance,
                                md5key, order_type, UnionId);
                        System.out.println("上传结果：" + Id + ":" + result + ",新版本");
                        if ("succeed".equals(result)) {
                            sql = "update TrainReport set publicstate=1 where ID=" + Id + "";
                            DBHelper.executeSql(sql);
                        }
                        else {
                            WriteLog.write("s上传失败", "重置状态：" + "[上传结果：" + Id + ":响应" + result + "]");
                            sql = "update TrainReport set publicstate=0 where ID=" + Id + "";
                            DBHelper.executeSql(sql);
                        }
                    }
                    catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (data.size() < 100) {
                System.out.println("休息一分钟……");
                try {
                    Thread.sleep(60000);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}