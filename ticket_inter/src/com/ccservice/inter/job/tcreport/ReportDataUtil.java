package com.ccservice.inter.job.tcreport;

public class ReportDataUtil {
    private String order_no;

    private String ticket_no;

    private String amount;

    private String settlement_type;

    private String quantity;

    private String trade_date;

    private String settlement_date;

    private String channel;

    private String account_balance;

    private String order_type;

    private String Unique;

    public ReportDataUtil() {
        super();
        // TODO Auto-generated constructor stub
    }

    public String getOrder_type() {
        return order_type;
    }

    public String getUnique() {
        return Unique;
    }

    public void setUnique(String unique) {
        Unique = unique;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }

    public String getOrder_no() {
        return order_no;
    }

    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }

    public String getTicket_no() {
        return ticket_no;
    }

    public void setTicket_no(String ticket_no) {
        this.ticket_no = ticket_no;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount2) {
        this.amount = amount2;
    }

    public String getSettlement_type() {
        return settlement_type;
    }

    public void setSettlement_type(String settlement_type2) {
        this.settlement_type = settlement_type2;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity2) {
        this.quantity = quantity2;
    }

    public String getTrade_date() {
        return trade_date;
    }

    public void setTrade_date(String trade_date2) {

        this.trade_date = trade_date2;

    }

    public String getSettlement_date() {
        return settlement_date;
    }

    public void setSettlement_date(String settlement_date2) {

        this.settlement_date = settlement_date2;

    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getAccount_balance() {
        return account_balance;
    }

    public void setAccount_balance(String account_balance2) {
        this.account_balance = account_balance2;
    }

}
