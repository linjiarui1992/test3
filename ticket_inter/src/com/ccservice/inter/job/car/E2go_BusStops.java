/**
 * 
 */
package com.ccservice.inter.job.car;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.inter.server.Server;

/**
 * http://www.e2go.com.cn/Home/GetBusStops?q=a&limit=50&timestamp=1436351816557
 * @time 2015年7月8日 下午6:39:06
 * @author chendong
 */
public class E2go_BusStops {
    public static void main(String[] args) {
        E2go_BusStops e2go_busstops = new E2go_BusStops();
        System.out.println();
        e2go_busstops.getAllBusStops();
    }

    /**
     * 
     * @time 2015年7月8日 下午6:49:27
     * @author chendong
     */
    private void getAllBusStops() {
        String url = "http://www.e2go.com.cn/Home/GetBusStops?q=";
        for (int i = 0; i < 26; i++) {
            int tempI = i + 97;
            char tempC = (char) tempI;
            String urlt = url;
            urlt += tempC + "&limit=5000&timestamp=" + System.currentTimeMillis();
            String data_url = SendPostandGet.submitGet(urlt);
            System.out.println(urlt);
            System.out.println(data_url);
            JSONArray jsonArray = JSONArray.parseArray(data_url);
            //parseArrayBusStops(jsonArray);
        }
    }

    /**
     * 
     * @time 2015年7月8日 下午6:59:49
     * @author chendong
     */
    private void parseArrayBusStops(JSONArray jsonArray) {
        //        JSONArray jsonArray = JSONArray.parseArray(data_url);
        for (int j = 0; j < jsonArray.size(); j++) {
            JSONObject jsonObject = jsonArray.getJSONObject(j);
            String StopName = jsonObject.getString("StopName");
            String StopId = jsonObject.getString("StopId");
            String Province = jsonObject.getString("Province");
            String City = jsonObject.getString("City");
            System.out.println(StopId + ":" + StopName + ":" + Province + ":" + City);
            String sql = "INSERT INTO [CarStationName] ([StopId],[StopName],[Province],[City],[Address]) VALUES ("
                    + StopId + ",'" + StopName + "','" + Province + "','" + City + "','')";

            ISystemService iSystemService = Server.getInstance().getSystemService(
                    "http://localhost:9001/cn_service/service/");
            String countsql = "select count(1) from CarStationName where StopId=" + StopId;
            try {
                int count = iSystemService.countAdvertisementBySql(countsql);
                if (count == 0) {
                    iSystemService.excuteAdvertisementBySql(sql);
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
