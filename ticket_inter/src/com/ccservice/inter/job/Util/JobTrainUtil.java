/**
 * 
 */
package com.ccservice.inter.job.Util;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.eclipse.jetty.util.ajax.JSON;

import net.sourceforge.pinyin4j.PinyinHelper;

import com.alibaba.fastjson.JSONObject;
import com.ccervice.util.StringUtil;
import com.ccservice.b2b2c.policy.SendPostandGet;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.HttpsUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;

/**
 * 
 * @time 2015年9月6日 上午10:46:32
 * @author chendong
 */
public class JobTrainUtil {

    /**
     * 断开adsl重新连接
     * @time 2015年9月6日 上午10:46:41
     * @author chendong
     */
    public static void reConnectAdsl() {
        //是否使用adsl
        String job12306Registration_useadsl = PropertyUtil.getValue("Job12306Registration_useadsl",
                "train.reg.properties");
        if ("0".equals(job12306Registration_useadsl)) {
            System.out.println("没有使用adsl,不拨号");
            return;
        }
        String Job12306Registration_adslDISCONNECT = PropertyUtil.getValue("Job12306Registration_adslDISCONNECT",
                "train.reg.properties");
        String Job12306Registration_adslCONNECT = PropertyUtil.getValue("Job12306Registration_adslCONNECT",
                "train.reg.properties");
        //最后一次拨号的时间和当前时间是否大于8秒
        boolean istobohao = (System.currentTimeMillis() - Server.adslLastDialingTime) > 8000L;
        if (istobohao) {
            Server.adslLastDialingTime = System.currentTimeMillis();
            if (Server.adslIaonLine) {
                Server.adslIaonLine = false;
                try {
                    ConnectNetWork.executeCmd(Job12306Registration_adslDISCONNECT);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                System.out.println("断开连接");
            }
            //断开ADSL 
            //        try {
            //            Thread.sleep(5000);
            //        }
            //        catch (InterruptedException e1) {
            //            e1.printStackTrace();
            //        }
            if (!Server.adslIaonLine) {//如果不在线才继续执行
                //再连，分配一个新的IP   
                try {
                    ConnectNetWork.executeCmd(Job12306Registration_adslCONNECT);
                    System.out.println("建立连接");
                    WriteLog.write("Job12306Registration_adslIp",
                            SendPostandGet.submitGet("http://ws.jinri.cn/getip.aspx"));
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            Server.adslLastDialingTime = System.currentTimeMillis();
            Server.adslIaonLine = true;
        }
        else {
            System.out.println("距离上次拨号还不足8秒。。。");
        }
    }

    /**
    * 检测 rep是否被封
    * @param repUrl
    * @param proxyPort2 
    * @param proxyHost2 
    * @param useProxy2 
    * @param time
    * @param startcity
    * @param endcity
    * @return
    */
    public static String rep0Method(String repUrl, String useProxy, String proxyHost, String proxyPort) {
        String resultString = "";
        String paramContent = "datatypeflag=0";
        if ("1".equals(useProxy)) {
            paramContent += "&useProxy=" + useProxy + "&proxyHost=" + proxyHost + "&proxyPort=" + proxyPort;
        }
        else {
            paramContent += "&useProxy=" + useProxy;
        }
        Long l1 = System.currentTimeMillis();
        try {
            resultString = SendPostandGet.submitPost(repUrl, paramContent, "utf-8").toString();
        }
        catch (Exception e) {
            resultString = e.getLocalizedMessage();
        }
        //        System.out.println("rep0Method===:(耗时:" + (System.currentTimeMillis() - l1) + ")====>" + repUrl + "?"
        //                + paramContent + ":" + resultString);
        return resultString;
    }

    /**
     * 使用rep登录并返回登录后的结果
     * 
     * @param repUrl
     * @param loginname
     * @param password
     * @return
     * @time 2015年9月9日 上午11:53:41
     * @author chendong
     */
    public static String getCookie(String repUrl, String loginname, String password) {
        String par = "datatypeflag=12&logname=" + loginname + "&logpassword=" + password + "&mobile=";
        String result = "-1";
        for (int i = 0; i < 3; i++) {
            if ("-1".equals(result) || (result.startsWith("失败") && !result.contains("该用户已被暂停使用"))) {
                result = SendPostandGet.submitPost(repUrl, par, "UTF-8").toString();
            }
            else {
                break;
            }
        }
        return result;
    }

    /**
     * 获取这个账号里有多少个乘旅客
     * @return
     * @time 2015年7月29日 下午7:39:29
     * @author chendong
     */
    public static JSONObject getCountString(String repUrl, String cookieString) {
        //        int count = 0;
        String par = "datatypeflag=3&cookie=" + cookieString;
        JSONObject json = new JSONObject();
        try {
            //{"queryMyOrderNoComplete":"","passengercount":16,"msg":""}
            String resultString = SendPostandGet.submitPost(repUrl, par, "UTF-8").toString();
            json = JSONObject.parseObject(resultString);
            //            count = json.getInteger("passengercount");//{"queryMyOrderNoComplete":"","passengercount":1,"msg":"已通过"}
            //            msg = json.getString("msg");
        }
        catch (Exception e) {
            e.printStackTrace();
            //            count = -1;
        }
        return json;
    }

    /**
     * 如果名字没有生僻字返回名字如果有则，转化为拼音后返回
     * 
     * @param name
     * @return
     */
    public static String parseJianPinYinName(String name) {
        String result = "";
        try {
            for (int j = 0; j < name.length(); j++) {
                String pinyin = PinyinHelper.toHanyuPinyinStringArray(name.charAt(j))[0];
                pinyin = pinyin.substring(0, 1);
                result += pinyin;
            }
        }
        catch (Exception e) {
            result = getRandomNum(6, 1);
        }
        return result;
    }

    /**
    * 如果名字没有生僻字返回名字如果有则，转化为拼音后返回
    * 
    * @param name
    * @return
    */
    public static String parsePinYinName(String name) {
        String result = "";
        try {
            for (int j = 0; j < name.length(); j++) {
                String pinyin = PinyinHelper.toHanyuPinyinStringArray(name.charAt(j))[0];
                pinyin = pinyin.substring(0, pinyin.length() - 1);
                result += pinyin;
            }
        }
        catch (Exception e) {
            result = getRandomNum(6, 1);
        }
        return result;
    }

    /**
     * 生成随即密码
     * @param pwd_len 生成的密码的总长度
     * @param type 1数字加字母2纯数字3纯字母
     * @return  密码的字符串
     */
    public static String getRandomNum(int pwd_len, int type) {
        //35是因为数组是从0开始的，26个字母+10个 数字
        final int maxNum = 36;
        int i; //生成的随机数
        int count = 0; //生成的密码的长度
        char[] str = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
                't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
        if (type == 2) {
            str = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
        }
        if (type == 3) {
            str = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
                    'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
        }
        StringBuffer pwd = new StringBuffer("");
        Random r = new Random();
        while (count < pwd_len) {
            //生成随机数，取绝对值，防止 生成负数，
            i = Math.abs(r.nextInt(maxNum)); //生成的数最大为36-1
            if (i >= 0 && i < str.length) {
                pwd.append(str[i]);
                count++;
            }
        }
        return pwd.toString();
    }

    /**
     * 账号是否存在
     * 
     * @param loginname
     * @return true 存在   false不存在
     * @time 2015年9月30日 下午2:50:23
     * @author chendong 
     */
    public static boolean check12306LoginName(String loginname) {
        String resultString = new JSONObject().toJSONString();
        //        {"validateMessagesShowId":"_validatorMessage","status":true,"httpstatus":200,"data":true,"messages":[],"validateMessages":{}}
        for (int j = 0; j < 10; j++) {
            try {
                resultString = HttpsUtil.get("https://kyfw.12306.cn/otn/regist/checkUserName?user_name=" + loginname,
                        "", "utf-8");
            }
            catch (Exception e) {
            }
            System.out.println(loginname + ":用户名检测结果:" + resultString);
            if (resultString == null) {
                reConnectAdsl();
                continue;
            }
            else if (resultString.contains("您的操作频率过快")) {
                reConnectAdsl();//断开adsl重新连接
                continue;
            }
            else {
                break;
            }
        }
        boolean isexist = false;
        try {
            JSONObject jsonObject = JSONObject.parseObject(resultString);
            isexist = jsonObject.getBoolean("data");
        }
        catch (Exception e) {
        }

        return isexist;
    }

    /**
     * 
     * @time 2015年10月19日 下午3:47:04
     * @author chendong
     */
    public static JSONObject initQueryUserInfo(String cookieString) {
        JSONObject jsonObject = new JSONObject();
        Map<String, String> requestproperty = new HashMap<String, String>();
        requestproperty.put("Cookie", cookieString);
        try {
            String resultString = HttpsUtil.post12306("https://kyfw.12306.cn/otn/modifyUser/initQueryUserInfo", "",
                    "utf-8", requestproperty);
            if (resultString.contains("登录 | 客运服务 | 铁路客户服务中心")) {
                jsonObject.put("loginStatus", "未登录");
            }
            else {
                String[] results = resultString.split("info-item");
                for (int i = 1; i < results.length; i++) {
                    String temp_result = results[i];
                    //                    System.out.println(i + "====================================================");
                    //                    System.out.println(temp_result);
                    if (temp_result.contains("手机号码")) {
                        if (temp_result.contains("已通过")) {
                            jsonObject.put("shoujihaoma_heyanstatus", "已通过");
                        }
                        else if (temp_result.contains("未通过")) {
                            jsonObject.put("shoujihaoma_heyanstatus", "未通过");
                        }
                        else if (temp_result.contains("手机核验")) {
                            jsonObject.put("shoujihaoma_heyanstatus", "手机核验");
                        }
                        else {
                            //                        System.out.println(temp_result);
                            jsonObject.put("shoujihaoma_heyanstatus", "其他");
                        }
                        temp_result = StringUtil.getStringByRegex(temp_result, "\\d{11}");
                        if (temp_result.length() == 11) {
                            jsonObject.put("shoujihaoma", temp_result);
                        }
                        else {
                            jsonObject.put("shoujihaoma", "");
                        }
                    }
                    else if (temp_result.contains("核验状态")) {
                        String heyanzhuangtai = "未知";
                        if (temp_result.contains("已通过")) {
                            heyanzhuangtai = "已通过";
                        }
                        else if (temp_result.contains("未通过")) {
                            heyanzhuangtai = "未通过";
                        }
                        else if (temp_result.contains("待核验")) {
                            heyanzhuangtai = "待核验";
                        }
                        else {
                            heyanzhuangtai = "其他";
                            System.out.println(temp_result);
                        }
                        jsonObject.put("heyanzhuangtai", heyanzhuangtai);
                    }
                    else if (temp_result.contains("姓名") && !temp_result.contains("姓名填写规则")
                            && !temp_result.contains("优惠区间")) {
                        System.out.println(i + "====================================================");
                        System.out.println(temp_result);
                        String name = temp_result.substring(temp_result.indexOf("class=\"con\">") + 12);
                        System.out.println(name);
                        name = name.substring(0, name.indexOf("</div>"));
                        System.out.println(name);
                        jsonObject.put("name", name);
                    }
                    else if (temp_result.contains("证件号码") && !temp_result.contains("edit_user_no")) {
                        String idno = temp_result.substring(temp_result.indexOf("class=\"con\">") + 12);
                        idno = idno.substring(0, idno.indexOf("</div>"));
                        jsonObject.put("idno", idno);
                    }
                    //                System.out.println(i + ">>>>>>>>>>>>>>");
                }
            }
        }
        catch (KeyManagementException e) {
            e.printStackTrace();
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    /**
     * 账号是否存在
     * 
     * @param loginname
     * @return true 存在   false不存在
     * @time 2015年9月30日 下午2:50:23
     * @author chendong 
     * @throws Exception 
     */
    public static boolean checkLoginName(String loginname) throws Exception {
        String resultString = HttpsUtil.Get("https://kyfw.12306.cn/otn/regist/checkUserName?user_name=" + loginname);
        //        System.out.println(loginname + ":" + resultString);//zhaofan2uyzwzc1l
        //        {"validateMessagesShowId":"_validatorMessage","status":true,"httpstatus":200,"data":true,"messages":[],"validateMessages":{}}
        boolean isexist = false;
        JSONObject jsonObject = JSONObject.parseObject(resultString);
        try {
            isexist = jsonObject.getBoolean("data");
        }
        catch (Exception e) {
        }
        return isexist;
    }

}
