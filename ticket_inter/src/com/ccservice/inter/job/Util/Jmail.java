package com.ccservice.inter.job.Util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Properties;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Store;

import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.inter.job.WriteLog;

public class Jmail {
    public static void main(String[] args) {
        String str = "-1";
        try {

            //tuuyoo.com=121.40.51.78
            //17gds.com=121.40.205.16
            //mail1.xyz=121.40.241.126
            //59book.xyz=121.40.226.72
            //59love.xyz=121.40.62.200
            //51air.xyz=120.26.83.131
            //59ai.xyz=121.43.117.148
            //aironline.xyz=121.43.225.182
            //                        59book.xyz=121.40.226.72
            str = Jmail.getMail("chendong", "123456", "121.40.62.200", 10L);
            System.out.println(str);
            //            Customeruser customeruser = new Customeruser();
            //            customeruser.setMemberfax("wangaipqar1@tuuyoo.com");//邮箱地址
            //            boolean isAddSuccess = checkMailPath(customeruser, "123.56.102.188");
            //            System.out.println(isAddSuccess);
        }
        catch (Exception e) {
        }
        System.out.println("====================================================");
        //        System.out.println(str);
    }

    /**
     * 检测邮箱地址是否有效如果无效添加到james mail
     * 如果已经有了或者添加采购了就返回true,否则返回false
     * @param customeruser
     * @time 2015年8月20日 下午7:53:17
     * @author chendong
     * @param email_ip 
     */
    public static boolean checkMailPath(Customeruser customeruser, String email_ip) {
        String mailUsername = customeruser.getMemberfax();
        mailUsername = mailUsername.split("@")[0];
        String addresult = adduser(email_ip, mailUsername, "123456");
        //        System.out.println(addresult);
        if (addresult.contains("already exists") || addresult.contains("added")) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * 向JAMES MAIL里添加账号 
     * @param ip ip地址
     * @param mail email地址 的用户名
     * @param mailpassword 密码
     * @return
     * @time 2014年10月22日 下午3:57:57
     * @author yinshubin
     */
    public static String adduser(String ip, String mail, String mailpassword) {
        String result = "";
        try {
            int port = 4555;
            String user = "root";//whzf011843adm
            String password = "root";//5n0wbIrdsMe3
            NetTelnet telnet = new NetTelnet(ip, port, user, password);
            if (telnet.login(user, password)) {
                String cmdcode = "adduser " + mail + " " + mailpassword;
                String r4 = telnet.sendCommand(cmdcode, "added", "Error adding user " + mail);
                if (r4 != null) {
                    result = r4;
                }
            }
            else {
                result = "telnet登录失败";
            }
            telnet.disconnect();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 获取12306激活的邮件的访问地址 
     * 
     * @param mailname 邮箱用户名
     * @param mailpassword 邮箱密码
     * @param host IP
     * @param sleepTime 睡眠多久才开始读取邮箱内容
     * @return
     * @time 2014年10月22日 下午4:05:33
     * @author yinshubin
     */
    public static String getMail(final String mailname, final String mailpassword, String host, Long sleepTime) {
        String result = "";
        // 创建Properties 对象  
        //        Properties props = new Properties();
        //
        //        // 创建邮件会话  
        //        Session session = Session.getDefaultInstance(props, new Authenticator() {
        //            @Override
        //            public PasswordAuthentication getPasswordAuthentication() {
        //                return new PasswordAuthentication(mailname, mailpassword);
        //            }
        //        });

        // 创建一个有具体连接信息的Properties对象  
        Properties props = new Properties();
        props.setProperty("mail.store.protocol", "pop3");
        props.setProperty("mail.pop3.host", host);
        // 使用Properties对象获得Session对象  
        Session session = Session.getInstance(props);
        session.setDebug(false);
        try {
            // 获取邮箱的pop3存储  
            Store store = session.getStore("pop3");
            store.connect(host, mailname, mailpassword);
            // 获取inbox文件  
            Folder folder = store.getFolder("INBOX");
            folder.open(Folder.READ_ONLY);//HOLDS_MESSAGES);// HOLDS_FOLDERS);// READ_WRITE);// READ_ONLY); //打开，打开后才能读取邮件信息  
            // 获取邮件消息  
            Message[] message = folder.getMessages();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                if (message.length == 0) {
                    Thread.sleep(10000L);
                    // 获取邮箱的pop3存储  
                    store = session.getStore("pop3");
                    store.connect(host, mailname, mailpassword);
                    // 获取inbox文件  
                    folder = store.getFolder("INBOX");
                    folder.open(Folder.READ_ONLY);//HOLDS_MESSAGES);// HOLDS_FOLDERS);// READ_WRITE);// READ_ONLY); //打开，打开后才能读取邮件信息  
                    // 获取邮件消息  
                    message = folder.getMessages();
                    baos = new ByteArrayOutputStream();
                }
                if (message.length > 0) {
                    Message message0 = message[0];
                    String content = message0.getContent().toString();
                    //                    System.out.println(content);
                    //                    message[0].writeTo(baos);
                    //                    result = baos.toString();

                    Multipart mp = (Multipart) message0.getContent();
                    int bodyNum = mp.getCount();
                    for (int k = 0; k < bodyNum; k++) {
                        if (mp.getBodyPart(k).isMimeType("text/html")) {
                            result = (String) mp.getBodyPart(k).getContent();
                        }
                    }
                    //                    System.out.println(result);
                }
                else {
                    result = "";
                    System.out.println("没收到邮件啊");
                }
            }
            catch (IOException e) {
                //                e.printStackTrace();
            }
            catch (Exception e) {
                //                e.printStackTrace();
            }
            if (result.contains("https://kyfw.12306.cn/otn//regist/activeAccount?userName")) {
                result = result.split(">https://kyfw.12306.cn/otn//regist/activeAccount\\?userName")[1];
                result = "https://kyfw.12306.cn/otn//regist/activeAccount?userName" + result.split("</a>")[0];
            }
            else {
                result = "no_email";
            }
            // 关闭资源  
            folder.close(false);
            store.close();
        }
        catch (MessagingException e) {
            //            e.printStackTrace();
        }
        catch (Exception e) {
            //            e.printStackTrace();
            WriteLog.write("Job12306Registration_jihuoyouxiang_Exception", mailname + ":" + mailpassword + ":" + host
                    + ":" + sleepTime);
        }
        return result;
    }

    /**
     * 验证邮箱是否存在
     * 
     * @return
     * @time 2015年1月29日 下午1:57:56
     * @author chendong
     */
    public static String verify_email_loginname(String mail, String ip) {
        String result = "";
        try {
            int port = 4555;
            String user = "root";//whzf011843adm
            String password = "root";//5n0wbIrdsMe3
            NetTelnet telnet = new NetTelnet(ip, port, user, password);
            if (telnet.login(user, password)) {
                String r4 = telnet.sendCommand("verify " + mail, "verify", "Error verify user " + mail);
                if (r4 != null) {
                    result = r4;
                }
            }
            else {
                result = "telnet登录失败";
            }
            telnet.disconnect();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * JAMES MAIL删除账号 
     * @param mail 邮箱用户名 
     * @return
     * @time 2014年10月23日 下午6:05:29
     * @author yinshubin
     */
    public static String deluser(String mail, String ip) {
        String result = "";
        try {
            int port = 4555;
            String user = "root";//whzf011843adm
            String password = "root";//5n0wbIrdsMe3
            NetTelnet telnet = new NetTelnet(ip, port, user, password);
            if (telnet.login(user, password)) {
                String r4 = telnet.sendCommand("deluser " + mail, "deleted", "doesn't exist");
                if (r4 != null) {
                    result = r4;
                }
            }
            else {
                result = "telnet登录失败";
            }
            telnet.disconnect();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
