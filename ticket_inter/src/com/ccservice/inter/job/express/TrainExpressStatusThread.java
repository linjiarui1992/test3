package com.ccservice.inter.job.express;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.inter.server.Server;

public class TrainExpressStatusThread extends Thread {

	private String orderid;

	private String expressnum;

	public TrainExpressStatusThread(String orderId, String expressNum) {
		this.orderid = orderId;
		this.expressnum = expressNum;
	}

	@Override
	public void run() {
		if (orderid != null && !"".equals(orderid) && expressnum != null
				&& !"0".equals(expressnum)) {
			String param = "expressno=" + expressnum;
			String url = "http://121.40.241.126:9038/SfExpressHthy/RouteServiceServlet"; // 请求顺丰路由地址
			String sb1 = "";
			String result = SendPostandGet.submitPost(url, param, "UTF-8")
					.toString();
			WriteLog.write("SF_路由信息", ":orderid:" + orderid + ":expressnum:"
					+ expressnum + ":result:" + result);
			int status = 1;
			String takeTime = "";
			try {
				Document document = DocumentHelper.parseText(result);
				Element root = document.getRootElement();
				Element head = root.element("Head");
				Element body = root.element("Body");
				Element routeResponse = body.element("RouteResponse");
				if ("OK".equals(root.elementText("Head"))
						&& routeResponse != null) {
					List elements = routeResponse.elements("Route");
					if (elements.size() > 0) {
						for (int i = 0; i < elements.size(); i++) {
							Element route = (Element) elements.get(i);
							sb1 += route.attributeValue("accept_time") + "DDD"
									+ route.attributeValue("remark") + "BBB";
						}
					}
					if (!"".equals(sb1)) {
						Element route1 = (Element) elements.get(0);
						if (sb1.contains("已签收")) {
							status = 3;
							takeTime = route1.attributeValue("accept_time");
						} else if (sb1.contains("已收取快件")) {
							status = 2;
							takeTime = route1.attributeValue("accept_time");
						}
					}

				}
				insertOrUpdate(takeTime, status, sb1);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		} else if (orderid != null && !"".equals(orderid)
				&& "0".equals(expressnum)) {
			insertOrUpdate("", 1, "");
		}
	}

	public void insertOrUpdate(String takeTime, int expressStatus,
			String routeMsg) {
		try {
			String sql = "SELECT Id FROM TrainExpressOffline WITH (NOLOCK) WHERE OrderId = "
					+ orderid;
			List list = Server.getInstance().getSystemService()
					.findMapResultBySql(sql, null);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String date = sdf.format(new Date());
			if (list.size() > 0) {
				String usql = "UPDATE TrainExpressOffline SET TakeTime = '"
						+ takeTime + "', RefreshTime = '" + date
						+ "', ExpressStatus = '" + expressStatus
						+ "', RouteMsg = '" + routeMsg + "' WHERE OrderId ="
						+ orderid;
				Server.getInstance().getSystemService()
						.findMapResultBySql(usql, null);
			} else {
				String isql = "INSERT INTO TrainExpressOffline(OrderId,TakeTime,RefreshTime,ExpressStatus,RouteMsg) VALUES("
						+ orderid
						+ ",'"
						+ takeTime
						+ "','"
						+ date
						+ "',"
						+ expressStatus + ",'" + routeMsg + "')";
				Server.getInstance().getSystemService()
						.findMapResultBySql(isql, null);
			}
		} catch (Exception e) {
		}
	}
}
