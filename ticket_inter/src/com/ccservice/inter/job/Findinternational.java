package com.ccservice.inter.job;

import java.net.MalformedURLException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.atom.service.IAtomService;
import com.ccservice.b2b2c.base.fflight.AllRouteInfo;
import com.ccservice.b2b2c.base.fflight.Route;
import com.ccservice.b2b2c.base.service.IAirService;
import com.ccservice.b2b2c.base.specialprice.Specialprice;
import com.ccservice.inter.server.Server;

public class Findinternational implements Job {

    @SuppressWarnings("deprecation")
    public static void main(String[] args) throws JobExecutionException {
        //		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        //		
        //		Date date = new Date();
        //		date.setDate(date.getDate()+7);
        //		String s = sdf.format(date);
        //		System.out.println(s);

    }

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        HessianProxyFactory factory = new HessianProxyFactory();
        String serviceurl = Server.getInstance().getUrl();
        IAirService airservice = null;
        String where = "where " + Specialprice.COL_isinternal + "=1";
        List list = Server.getInstance().getAirService().findAllSpecialprice(where, "", -1, 0);
        if (list.size() != 0) {
            String where1 = "delete from " + Specialprice.TABLE + " where " + Specialprice.COL_isinternal + "=1";
            Server.getInstance().getAirService().excuteSpecialpriceBySql(where1);
        }

        try {
            airservice = (IAirService) factory
                    .create(IAirService.class, serviceurl + IAirService.class.getSimpleName());
        }
        catch (MalformedURLException e2) {
            e2.printStackTrace();
        }
        String url = Server.getInstance().geturlAtom();
        IAtomService iAtomService = null;
        try {
            iAtomService = (IAtomService) factory.create(IAtomService.class, url + IAtomService.class.getSimpleName());
        }
        catch (MalformedURLException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        String StartAirportCode = "HKG";
        String EndAirportCode = "ICN";
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        date.setDate(date.getDate() + 14);
        String fromDate = sdf.format(date);

        float f = 0F;
        String[] fromcitys = { "BJS", // 北京
                "PVG", // 上海浦东
                "CAN", // 广州
                "SZX", // 深圳
                "TAO", // 青岛
                "NKG" // 南京
        };
        String[] tocitys = { "PUS", // 釜山
                "MFM", // 澳门
                "ICN", // 首尔
                "SIN", // 新加坡城
                "DME", // 莫斯�?
                "JKF", // 纽约
                "YVR", // 温哥�?
                "BKK", // 曼谷
                "HKG", // 香港
                "TPE", // 台北
                "WLG", // 惠灵�?
                "GVA", // 日内�?
                "BOM", // 孟买
                "CDG", // 巴黎
                "LHR" // 伦敦
        };
        for (int i = 0; i < fromcitys.length; i++) {
            for (int j = 0; j < tocitys.length; j++) {
                StartAirportCode = fromcitys[i];
                EndAirportCode = tocitys[j];
                AllRouteInfo listrouteinfo = iAtomService.interTicketSearch(fromcitys[i], tocitys[j], fromDate, null,
                        "Y", "1");
                List<Route> routes = listrouteinfo.getRoutes();
                Specialprice specialprice = new Specialprice();
                specialprice.setPrice(100000F);
                if (routes != null) {
                    for (Route route : routes) {
                        f = Float.parseFloat(Double.toString(route.getTotalFare()));
                        if (f < specialprice.getPrice()) {
                            specialprice.setStartport(StartAirportCode);
                            specialprice.setArrivalport(EndAirportCode);
                            specialprice.setDiscount(Float.parseFloat(new Integer(new Random().nextInt(50) + 50)
                                    .toString()));
                            specialprice.setStarttime(new Timestamp(System.currentTimeMillis()));
                            specialprice.setPrice(Float.parseFloat(Double.toString(route.getTotalFare())));
                            specialprice.setUpdatetime(new Timestamp(System.currentTimeMillis()));
                            specialprice.setCreatetime(new Timestamp(System.currentTimeMillis()));
                            specialprice.setCreateuser("ADMIN");
                            specialprice.setCreatetime(new Timestamp(System.currentTimeMillis()));
                            specialprice.setModifyuser("ADMIN");
                            specialprice.setModifytime(new Timestamp(System.currentTimeMillis()));
                            specialprice.setIsinternal(1);
                        }
                    }
                }
                try {
                    airservice.createSpecialprice(specialprice);
                }
                catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
