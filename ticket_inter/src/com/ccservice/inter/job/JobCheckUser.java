package com.ccservice.inter.job;

import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;
import com.ccservice.elong.inter.PropertyUtil;

import com.ccservice.inter.job.train.thread.CheckUserThread;

/**
 * 
 * 核对帐号状态并修改成对应状态
 * @time 2016年1月6日 下午6:14:26
 * @author ZhiHong
 */

public class JobCheckUser implements Job {

    public static void main(String[] args) {
    }

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        String dama38 = PropertyUtil.getValue("dama38", "CheckThread.properties");
        if (dama38.equals("1")) {
            new CheckUserThread().start();
        }
        else {
            System.out.println("dama38为0========》Job没开！");
        }
    }

    // 设置调度任务及触发器，任务名及触发器名可用来停止任务或修改任务
    public static void startScheduler(String expr) throws Exception {
        JobDetail jobDetail = new JobDetail("JobCheckUser", "JobCheckUser", JobCheckUser.class);// 任务名，任务组名，任务执行类
        CronTrigger trigger = new CronTrigger("JobCheckUser", "JobCheckUserGroup", expr);// 触发器名，触发器组名
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
        scheduler.scheduleJob(jobDetail, trigger);
        scheduler.start();
    }
}
