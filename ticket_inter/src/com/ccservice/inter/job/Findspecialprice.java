package com.ccservice.inter.job;

import java.net.MalformedURLException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;

import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.atom.service.IAtomService;
import com.ccservice.b2b2c.atom.service.ITicketSearchService;
import com.ccservice.b2b2c.base.flightinfo.FlightInfo;
import com.ccservice.b2b2c.base.flightinfo.FlightSearch;
import com.ccservice.b2b2c.base.service.IAirService;
import com.ccservice.b2b2c.base.specialprice.Specialprice;
import com.ccservice.inter.server.Server;

public class Findspecialprice implements Job {

    @Override
    public void execute(JobExecutionContext context) {
        String where = "where " + Specialprice.COL_isinternal + "=0";
        List list = Server.getInstance().getAirService().findAllSpecialprice(where, "", -1, 0);
        if (list.size() != 0) {
            String where1 = "delete from T_SPECIALPRICE where C_ISINTERNAL=0";
            Server.getInstance().getAirService().excuteSpecialpriceBySql(where1);
        }
        try {
            System.out.println("开始了");
            getSpecialOffer();
            System.out.println("结束了");
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param args
     * @throws MalformedURLException
     */
    @SuppressWarnings("unchecked")
    public void find() throws MalformedURLException {
        String url = Server.getInstance().geturlAtom();
        //
        HessianProxyFactory factory = new HessianProxyFactory();
        ITicketSearchService servier = (ITicketSearchService) factory.create(ITicketSearchService.class, url
                + ITicketSearchService.class.getSimpleName());

        String serviceurl = Server.getInstance().getUrl();
        IAirService airservice = (IAirService) factory.create(IAirService.class,
                serviceurl + IAirService.class.getSimpleName());

        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        date.setDate(date.getDate() + 7);
        String fromDate = sdf.format(date);

        FlightSearch flightSearch = new FlightSearch();
        flightSearch.setFromDate(fromDate);
        // String[] fromcitys =
        // {"PEK","SHA","SZX","TSN","CKG","WUH","TYN","XMN"};
        // String[] tocitys =
        // {"PEK","SHA","SZX","TSN","CKG","WUH","TYN","XMN","NKG","CAN","HGH"};
        String[] fromcitys = { "PEK", "SHA", "SZX", "TSN", "CKG", "WUH", "TYN", "XMN", "CTU" };
        String[] tocitys = { "PEK", "SHA", "SZX", "TSN", "CKG", "CGO", "WUH", "TYN", "XMN", "NKG", "CAN", "BAV", "CTU",
                "CGQ" };
        int no = 0;
        for (int i = 0; i < fromcitys.length; i++) {
            for (int j = 0; j < tocitys.length; j++) {
                Specialprice specialprice1 = new Specialprice();
                specialprice1.setPrice(10000F);
                if (!fromcitys[i].equals(tocitys[j])) {
                    // System.out.println(++no + "" + fromcitys[i] + "==>"
                    // + tocitys[j]);
                    flightSearch.setStartAirportCode(fromcitys[i]);
                    flightSearch.setEndAirportCode(tocitys[j]);
                    List<FlightInfo> listFlightInfo = new ArrayList<FlightInfo>();
                    try {
                        listFlightInfo = servier.findAllFlightinfo(flightSearch);
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                    float f = 0F;
                    System.out.println("插入了2");
                    if (listFlightInfo != null) {
                        for (FlightInfo flightInfo : listFlightInfo) {
                            f = flightInfo.getLowCarbin().getPrice();
                            if (f < specialprice1.getPrice()) {
                                specialprice1.setStartport(fromcitys[i]);
                                specialprice1.setArrivalport(tocitys[j]);
                                specialprice1.setStarttime(flightInfo.getDepartTime());
                                specialprice1.setDiscount(flightInfo.getLowCarbin().getDiscount());
                                specialprice1.setPrice(flightInfo.getLowCarbin().getPrice());
                                specialprice1.setUpdatetime(new Timestamp(System.currentTimeMillis()));
                                specialprice1.setCreatetime(new Timestamp(System.currentTimeMillis()));
                                specialprice1.setCreateuser("ADMIN");
                                specialprice1.setCreatetime(new Timestamp(System.currentTimeMillis()));
                                specialprice1.setModifyuser("ADMIN");
                                specialprice1.setModifytime(new Timestamp(System.currentTimeMillis()));
                                specialprice1.setIsinternal(0);
                            }
                        }
                    }
                }
                try {
                    if (!fromcitys[i].equals(tocitys[j]) && specialprice1.getArrivalport() != null) {
                        airservice.createSpecialprice(specialprice1);
                        System.out.println("插入了");
                    }
                }
                catch (SQLException e) {
                    e.printStackTrace();
                    continue;
                }
            }
        }
    }

    /**
     * @param args
     * @throws MalformedURLException
     */
    @SuppressWarnings("unchecked")
    public void getSpecialOffer() throws MalformedURLException {
        String url = Server.getInstance().geturlAtom();
        //
        HessianProxyFactory factory = new HessianProxyFactory();
        IAtomService iAtomService = (IAtomService) factory.create(IAtomService.class,
                url + IAtomService.class.getSimpleName());

        String serviceurl = Server.getInstance().getUrl();
        IAirService airservice = (IAirService) factory.create(IAirService.class,
                serviceurl + IAirService.class.getSimpleName());

        String[] fromcitys = { "PEK", "SHA", "CAN", "NKG", "XIY", "SZX", "TSN", "CKG", "WUH", "TYN", "XMN", "CTU" };
        String[] tocitys = { "PEK", "SHA", "SZX", "TSN", "CKG", "CGO", "WUH", "TYN", "XMN", "NKG", "CAN", "BAV", "CTU",
                "CGQ" };
        //		int no = 0;
        for (int i = 0; i < fromcitys.length; i++) {
            for (int j = 0; j < tocitys.length; j++) {
                Specialprice specialprice1 = new Specialprice();
                specialprice1.setPrice(10000F);
                if (!fromcitys[i].equals(tocitys[j])) {
                    List<Specialprice> listSpecialprice = new ArrayList<Specialprice>();
                    try {
                        listSpecialprice = iAtomService.getSpecialpriceList("0", fromcitys[i], tocitys[j]);
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                    float f = 0F;
                    if (listSpecialprice != null) {
                        for (Specialprice sp : listSpecialprice) {
                            f = sp.getPrice();
                            System.out.println(fromcitys[i] + "-" + tocitys[j] + "价格：" + sp.getPrice() + "折扣："
                                    + sp.getDiscount() + "出发时间：" + sp.getStarttime());
                            try {
                                Server.getInstance().getAirService().createSpecialprice(sp);
                            }
                            catch (SQLException e) {
                                e.printStackTrace();
                            }
                            //							if (f < specialprice1.getPrice()) {
                            //								specialprice1.setStartport(fromcitys[i]);
                            //								specialprice1.setArrivalport(tocitys[j]);
                            //								specialprice1.setStarttime(sp.getStarttime());
                            //								specialprice1.setDiscount(sp.getDiscount());
                            //								specialprice1.setPrice(sp.getPrice());
                            //								specialprice1.setUpdatetime(new Timestamp(
                            //										System.currentTimeMillis()));
                            //								specialprice1.setCreatetime(new Timestamp(
                            //										System.currentTimeMillis()));
                            //								specialprice1.setCreateuser("ADMIN");
                            //								specialprice1.setCreatetime(new Timestamp(
                            //										System.currentTimeMillis()));
                            //								specialprice1.setModifyuser("ADMIN");
                            //								specialprice1.setModifytime(new Timestamp(
                            //										System.currentTimeMillis()));
                            //								specialprice1.setIsinternal(sp.getIsinternal());
                            //							}
                        }
                    }
                }
                //				try {
                //					if (!fromcitys[i].equals(tocitys[j])&&specialprice1.getArrivalport()!=null) {
                //						airservice.createSpecialprice(specialprice1);
                //					}
                //				} catch (SQLException e) {
                //					e.printStackTrace();
                //					continue;
                //				}
            }
        }
    }
}
