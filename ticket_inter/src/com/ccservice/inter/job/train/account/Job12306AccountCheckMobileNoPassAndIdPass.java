/**
 * 
 */
package com.ccservice.inter.job.train.account;

import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;

import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;

import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.b2b2c.policy.SendPostandGet;
import com.ccservice.b2b2c.policy.TimeUtil;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.JobIndexAgain_RepUtil;
import com.ccservice.inter.job.train.account.thread.Thread12306AccountCheckMobileNoPassAndIdPass;
import com.ccservice.inter.job.train.thread.TrainCreateOrderSupplyMethod;

/**
 * 核验33的账号找到身份通过而手机未通过的账号
 * @time 2015年11月24日21:58:01
 * @author chendong
 */
public class Job12306AccountCheckMobileNoPassAndIdPass extends TrainCreateOrderSupplyMethod implements Job {
    public static void main(String[] args) {
        try {
            Job12306AccountCheckMobileNoPassAndIdPass.startScheduler("0 0/1 7-23 * * ?");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        //        Job12306AccountCheckMobileNoPassAndIdPass job12306AccountCheckMobileNoPassAndIdPass = new Job12306AccountCheckMobileNoPassAndIdPass();
        //        job12306AccountCheckMobileNoPassAndIdPass.reLogintocheckHeyan();
    }

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        //        String Job12306AccountCheckMobileNoPassAndIdPassIsstartUrl = PropertyUtil
        //                .getValue("Job12306AccountCheckMobileNoPassAndIdPassIsstartUrl", "train.checkMobile.properties");
        //        String Isstart = SendPostandGet.submitGet(Job12306AccountCheckMobileNoPassAndIdPassIsstartUrl);
        //        if ("1".equals(Isstart)) {
        reLogintocheckHeyan();
        //        }
        //        else {
        //            System.out.println(TimeUtil.gettodaydate(5) + ":Job12306AccountCheckMobileNoPassAndIdPassIsstartUrl:没开");
        //        }
    }

    /**
     * 获取到待核验的账号去核验
     * @time 2015年6月25日 上午11:19:01
     * @author chendong
     * @param type  0 老核验 1核验手机号
     */
    private void reLogintocheckHeyan() {
        System.out.println(
                Job12306AccountCheckMobileNoPassAndIdPass.class + ":reLogintocheckHeyan:" + TimeUtil.gettodaydate(5));
        String Job12306AccountCheckMobileNoPassAndIdPass_serviceurl = PropertyUtil
                .getValue("Job12306AccountCheckMobileNoPassAndIdPass_serviceurl", "train.checkMobile.properties");
        int selectCount = 50;//一次处理多少个账号
        List customerusers = getCustomerusers(Job12306AccountCheckMobileNoPassAndIdPass_serviceurl, selectCount);
        System.out.println(Job12306AccountCheckMobileNoPassAndIdPass.class + ":customerusers:" + customerusers.size());
        if (customerusers.size() == 0) {
            System.out.println(TimeUtil.gettodaydate(4) + ":没有【Job12306AccountCheckMobileNoPassAndIdPass】的账号了");
        }
        else {
            for (int i = 0; i < customerusers.size(); i++) {
                String repUrl = JobIndexAgain_RepUtil.getInstance().getRepUrl(false);
                Map map = (Map) customerusers.get(i);
                Long id = Long.parseLong(map.get("ID").toString());
                String loginname = map.get("C_LOGINNAME").toString();
                String password = map.get("C_LOGPASSWORD").toString();
                String mobile = map.get("C_MOBILE") == null ? "" : map.get("C_MOBILE").toString();
                WriteLog.write("Job12306AccountCheckMobileNoPassAndIdPass", id + ":" + repUrl + ":" + loginname + ":"
                        + password + ":" + Job12306AccountCheckMobileNoPassAndIdPass_serviceurl);
                System.out.println("开始了:" + id + ":" + repUrl + ":" + loginname + ":" + password + ":mobile:" + mobile);
                new Thread12306AccountCheckMobileNoPassAndIdPass(id, repUrl, loginname, password,
                        Job12306AccountCheckMobileNoPassAndIdPass_serviceurl).start();
            }
        }
    }

    private List getCustomerusers(String serviceurl, int selectCount) {
        ISystemService isystemservice = getISystemService(serviceurl);
        //        String procedureSqlString = " sp_Customeruser_Job12306AccountCheckMobileNoPassAndIdPass_33 ";
        String procedureSqlString = " sp_Customeruser_SelectByIsenable @selectCount=" + selectCount
                + ",@isupdateenable=0,@enable=118,@newenable=118 ";
        List customerusers = isystemservice.findMapResultByProcedure(procedureSqlString);
        return customerusers;
    }

    /**
     * 
     * @time 2015年9月30日 下午3:28:40
     * @author chendong
     * @return 
     */
    private ISystemService getISystemService(String serviceurl) {
        HessianProxyFactory factory = new HessianProxyFactory();
        ISystemService isystemservice = null;
        try {
            isystemservice = (ISystemService) factory.create(ISystemService.class,
                    serviceurl + ISystemService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return isystemservice;
    }

    // 设置调度任务及触发器，任务名及触发器名可用来停止任务或修改任务
    public static void startScheduler(String expr) throws Exception {
        JobDetail jobDetail = new JobDetail("Job12306AccountCheckMobileNew", "Job12306AccountCheckMobileNewGroup",
                Job12306AccountCheckMobileNoPassAndIdPass.class);// 任务名，任务组名，任务执行类
        CronTrigger trigger = new CronTrigger("Job12306AccountCheckMobileNew", "Job12306AccountCheckMobileNewGroup", expr);// 触发器名，触发器组名
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
        scheduler.scheduleJob(jobDetail, trigger);
        scheduler.start();
    }
}
