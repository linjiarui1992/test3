package com.ccservice.inter.job.train.account.thread;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.b2b2c.base.service.ITrainService;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.b2b2c.policy.TimeUtil;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.Util.JobTrainUtil;
import com.ccservice.inter.server.ServerUtil;
import com.ccservice.mobileCode.util.MobileCodeMethod;

/**
 * 核验手机号的Thread
 * 
 * @time 2014年12月27日 上午9:52:42
 * @author chendong
 */
public class Thread12306AccountCheckMobileNewThread extends Thread {
    public static void main(String[] s) {
        String repUrl = PropertyUtil.getValue("repUrl", "train.checkMobile.properties");//"http://localhost:9016/Reptile/traininit";
        String serviceurl = PropertyUtil.getValue("serviceurl", "train.checkMobile.properties");
        String mobileCodeType = PropertyUtil.getValue("mobileCodeType", "train.checkMobile.properties");
        //                {"LoginName":"xialix7ye6","cookieString":"JSESSIONID=0A01D730FCFCC2A54997118285E7F7827F26C866C9; 
        //        BIGipServerotn=819396874.64545.0000; current_captcha_type=Z","UserName":"超级管理","orderid":9937435,"password":"asd123456"}

        Long orderid = 9937435L;
        String loginname = "xialix7ye6";
        String password = "asd123456";
        String cookieString = "JSESSIONID=0A01D730FCFCC2A54997118285E7F7827F26C866C9; BIGipServerotn=819396874.64545.0000; current_captcha_type=Z";
        Thread12306AccountCheckMobileNewThread job12306AccountCheckMobileNewThread = new Thread12306AccountCheckMobileNewThread(
                orderid, loginname, password, repUrl, serviceurl, cookieString, mobileCodeType);
        //        job12306AccountCheckMobileNewThread.start();
        try {
            job12306AccountCheckMobileNewThread.checkMobile();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    String repUrl;

    String loginname;

    String password;

    Long id;

    ISystemService isystemservice;

    public Thread12306AccountCheckMobileNewThread(Long id, String loginname, String password, String repUrl,
            String serviceurl, String cookieString, String mobileCodeType) {
        super();
        this.checkType = 0;
        this.loginname = loginname;
        this.password = password;
        this.id = id;
        this.repUrl = repUrl;
        this.cookieString = cookieString;
        this.mobileCodeType = mobileCodeType;
        this.isystemservice = ServerUtil.getISystemService(serviceurl);
    }

    int checkType;//0老的，1核验页面里的订单里的新的核验

    String orderid;

    public Thread12306AccountCheckMobileNewThread(String orderid, String loginname, String password, String repUrl,
            String serviceurl, String cookieString, String mobileCodeType) {
        super();
        this.checkType = 1;
        this.orderid = orderid;
        this.loginname = loginname;
        this.password = password;
        this.repUrl = repUrl;
        this.cookieString = cookieString;
        this.mobileCodeType = mobileCodeType;
        this.isystemservice = ServerUtil.getISystemService(serviceurl);

    }

    String cookieString;

    String msg;

    String useProxy;

    String mobileCodeType = "8";//1:优码,2:淘码,3:y码

    @Override
    public void run() {
        try {
            checkMobile();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(loginname + ":当前线程结束,Job12306AccountCheckMobileNewThread");

    }

    /**
     * @param foldername 
     * @param cookieString 
     * @time 2015年9月9日 上午11:25:52
     * @author chendong
     
     */
    public void checkMobile() {
        //        https://kyfw.12306.cn/otn/userSecurity/doEditTel
        Long l1 = System.currentTimeMillis();
        WriteLog.write("Job12306AccountCheckMobileNewThread", loginname + ":开始:" + TimeUtil.gettodaydate(5));
        int chongshiCount = 3;
        try {
            chongshiCount = Integer.parseInt(PropertyUtil.getValue("chongshiCount", "train.checkMobile.properties"));
        }
        catch (Exception e) {
        }
        for (int i = 0; i < chongshiCount; i++) {
            String resultString = chuliheyanshoujihao(cookieString, 0, "-1", l1);
            System.out.println(loginname + ":orderid(" + this.orderid + ")处理核验手机号->" + i + "次结果:" + resultString + "->"
                    + TimeUtil.gettodaydate(5));
            if (!"-1".equals(resultString) && !"".equals(resultString)) {
                String[] results = resultString.split("[|]");
                String resultBoolean = results[0];
                if ("true".equals(resultBoolean)) {
                    try {
                        updateDb(results, mobileCodeType);
                        createTrainorderrc(results[1]);
                        break;
                    }
                    catch (Exception e) {
                    }
                }
                else if (resultString.contains("系统忙，请稍后再试") || resultString.contains("您获取验证码短信次数过多")
                        || resultString.contains("网络繁忙")) {
                    System.out.println(resultString);
                    JobTrainUtil.reConnectAdsl();//断开adsl重新连接

                }
                else if (resultString.contains("no_data")) {
                    continue;
                }
                else if (resultString.contains("没有可用号码")) {
                    try {
                        Thread.sleep(2000L);
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    continue;
                }
                else {
                    if (results.length > 2) {
                        String resultStringsss = results[2].toString();
                        try {
                            JSONObject jSONObject = JSONObject.parseObject(resultStringsss);
                            resultStringsss = jSONObject.getJSONObject("data").getString("message");
                            System.out.println(resultStringsss);
                        }
                        catch (Exception e) {
                            System.out.println(resultStringsss);
                            e.printStackTrace();
                        }
                        createTrainorderrc(resultStringsss);

                    }
                    WriteLog.write("Job12306AccountCheckMobileNewThread", loginname + ":heyanResultString:" + resultString);
                    break;
                }
            }
            else {
                continue;
            }
        }
    }

    /**
     * 处理核验手机号
     * 新模式 直接在这里使用rep
     * @time 2015年11月5日14:05:57
     * @param l1 
     * @param resultString 
     * @param string 
     * @param i 
     * @author chendong
     * @time 2015年7月29日 下午8:34:05
     */
    private String chuliheyanshoujihao(String cookieString, int heyancishu, String proxyHost_proxyPort, Long l1) {
        String proxyPar = "";
        if ("1".equals(useProxy)) {
            proxyPar = "&" + getProxyPar(heyancishu, proxyHost_proxyPort);
        }
        String par = "datatypeflag=201&cookie=" + cookieString + "&logpassword=" + password + "&mobileCodeType="
                + mobileCodeType + proxyPar;
        WriteLog.write("Job12306AccountCheckMobileNewThread", loginname + ":" + par);
        String resultString = "-1";
        try {
            MobileCodeMethod mobileCodeMethod = new MobileCodeMethod(mobileCodeType, useProxy, "", "", this.repUrl);
            resultString = mobileCodeMethod.CheckAndChange12306Mobileno(cookieString, this.loginname, this.password,
                    mobileCodeMethod);
        }
        catch (Exception e) {
        }
        System.out.println(loginname + "==>" + resultString);
        WriteLog.write("Job12306AccountCheckMobileNewThread",
                loginname + ":resultString:" + resultString + ":>耗时>" + (System.currentTimeMillis() - l1));
        return resultString;
    }

    /**
     * 处理核验手机号
     * 老的rep模式
     * @param l1 
     * @param resultString 
     * @param string 
     * @param i 
     * @author chendong
     * @time 2015年7月29日 下午8:34:05
     */
    private String chuliheyanshoujihao_rep(String cookieString, int heyancishu, String proxyHost_proxyPort, Long l1) {

        String proxyPar = "";
        if ("1".equals(useProxy)) {
            proxyPar = "&" + getProxyPar(heyancishu, proxyHost_proxyPort);
        }
        String par = "datatypeflag=201&cookie=" + cookieString + "&logpassword=" + password + "&mobileCodeType="
                + mobileCodeType + proxyPar;
        WriteLog.write("Job12306AccountCheckMobileNewThread", loginname + ":url_t:" + this.repUrl + "?" + par);
        int timeout = 240000;
        String resultString = "-1";
        try {
            resultString = SendPostandGet.submitPostTimeOut(this.repUrl, par, "UTF-8", timeout).toString();
        }
        catch (Exception e) {
        }
        System.out.println(loginname + "==>" + resultString);
        WriteLog.write("Job12306AccountCheckMobileNewThread",
                loginname + ":resultString:" + resultString + ":>耗时>" + (System.currentTimeMillis() - l1));

        return resultString;
    }

    /**
     * 
     * @time 2015年10月17日 下午2:11:41
     * @author chendong
     */
    private void createTrainorderrc(String content) {
        if (this.checkType == 1) {
            Trainorderrc rc = new Trainorderrc();
            rc.setOrderid(Long.parseLong(this.orderid));
            rc.setContent(content.toString());
            rc.setStatus(0);
            rc.setCreateuser("CheckMobileListener");
            rc.setYwtype(0);
            String serviceurl_trainorderrc = PropertyUtil.getValue("serviceurl_trainorderrc",
                    "train.checkMobile.properties");
            ITrainService iTrainService = ServerUtil.getTrainService(serviceurl_trainorderrc);
            iTrainService.createTrainorderrc(rc);
        }
    }

    /**
     * 
     * @time 2015年9月11日 下午7:54:31
     * @author chendong
     * @param results 
     * @param mobileCodeType 
     */
    private void updateDb(String[] results, String mobileCodeType) {
        String ticketnumsql = "update T_CUSTOMERUSER set C_ISENABLE=1,C_CARDTYPE='" + mobileCodeType + "'";
        if (results.length > 1) {
            String mobile = results[1];
            ticketnumsql += ",C_MOBILE='" + mobile + "' ";
        }
        if (this.checkType == 0) {
            ticketnumsql += " where ID=" + this.id;
        }
        else {
            ticketnumsql += " where C_LOGINNAME='" + this.loginname + "'";
        }
        int count1 = isystemservice.excuteAdvertisementBySql(ticketnumsql);
        System.out.println(count1 + ":" + ticketnumsql);
        WriteLog.write("Job12306AccountCheckMobileNewThread",
                "loginname:" + loginname + ":" + isystemservice + ":count1:" + count1 + ":ticketnumsql:" + ticketnumsql);
        ticketnumsql = "update CustomeruserCheckMobile set status=2 where loginname='" + this.loginname + "'";
        int count2 = isystemservice.excuteAdvertisementBySql(ticketnumsql);
        System.out.println(count2 + ":" + ticketnumsql);
    }

    /**
     * 获取代理参数
     * @return
     * @time 2015年8月3日 下午2:24:48
     * @author chendong
     * @param heyancishu 
     * @param proxyHost_proxyPort 
     */
    public String getProxyPar(int heyancishu, String proxyHost_proxyPort) {
        String proxyPar = "-1";
        if (heyancishu == 0 && "1".equals(useProxy)) {// && (resultString.indexOf("由于您获取验证码短信次数过多") >= 0)
            String dataString = SendPostandGet
                    .submitGet("http://ip.zdaye.com/?api=20150803200040011100620034949384&port=80&dengji=%B8%DF%C4%E4&"
                            + "gb=2&https=%D6%A7%B3%D6&yys=%D2%C6%B6%AF&ct=3&daochu=1", "GBK");
            WriteLog.write("Job12306AccountCheckMobileNewThread", dataString);
            if (dataString.indexOf("请求过快") < 0) {
                String sql2 = "update T_SYSCONFIG set C_VALUE+='" + dataString + "|' where c_name = 'checkMobileNoProxy'";
                System.out.println("sql2:" + sql2);
            }
        }
        if ("1".equals(useProxy)) {
        }
        return proxyPar;
    }

    /**
    * 如果为空返回 "0"
    * 
    * @param object
    * @return
    * @time 2015年4月15日 下午1:57:54
    * @author chendong
    */
    private String objectisnull(Object object) {
        return object == null ? "0" : object.toString();
    }

}
