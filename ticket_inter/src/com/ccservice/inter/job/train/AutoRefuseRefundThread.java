package com.ccservice.inter.job.train;

import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.sql.Timestamp;
import com.weixin.util.RequestUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;

/**
 * 线程_自动拒绝超时退票，目前针对淘宝线上，超时时间为1小时
 * @author WH
 * @time 2016年12月12日 下午4:39:47
 * @version 1.0
 */

public class AutoRefuseRefundThread extends Thread {

    //订单ID
    private long orderId;

    //车票ID
    private long ticketId;

    //接口类型
    private int interfaceType;

    //拒绝理由
    private int reason = 42;//无退款

    //发车时间
    private String departTime;

    //接口票号
    private String interfaceTicketNo;

    //拒绝退票地址
    private String trainorderNonRefundable;

    //空请求头
    private static final Map<String, String> NullHeader = new HashMap<String, String>();

    //相关参数赋值
    public AutoRefuseRefundThread(long orderId, long ticketId, int interfaceType, String departTime,
            String trainorderNonRefundable, String interfaceTicketNo) {
        this.orderId = orderId;
        this.ticketId = ticketId;
        this.departTime = departTime;
        this.interfaceType = interfaceType;
        this.interfaceTicketNo = interfaceTicketNo;
        this.trainorderNonRefundable = trainorderNonRefundable;
    }

    @SuppressWarnings("rawtypes")
    public void run() {
        //类型为空
        if (interfaceType == 0) {
            //查询类型
            try {
                //if (!ElongHotelInterfaceUtil.StringIsNull(interfaceTicketNo)) {
                //拼接SQL
                String sql = " [TaoBaoRefundInfoQuery]  @sub_biz_order_id='" + interfaceTicketNo + "'";
                //数据入库
                List list = Server.getInstance().getSystemService().findMapResultByProcedure(sql);
                //查询成功
                if (list != null && list.size() > 0) {
                    interfaceType = 6;//淘宝 
                }
                //}
                if (interfaceType == 0 && orderId > 0) {
                    interfaceType = new TrainSupplyMethod().getOrderAttribution(orderId);
                }
            }
            catch (Exception e) {

            }
        }
        //淘宝退票
        if (interfaceType == 6) {
            //记录日志
            WriteLog.write("12306_GT_火车票改签退_超时拒绝", "OrderId:" + orderId + ":TicketId:" + ticketId + ":DepartTime:"
                    + departTime);
            //回调参数
            String url = trainorderNonRefundable + "?trainorderid=" + orderId + "&ticketid=" + ticketId
                    + "&interfacetype=" + interfaceType + "&reason=" + reason + "&responseurl="
                    + trainorderNonRefundable;
            //请求接口
            RequestUtil.get(url, "UTF-8", NullHeader, 30 * 1000);
            //数据入库
            try {
                if (orderId > 0 && haveOrder()) {
                    save();
                }
            }
            catch (Exception e) {

            }
        }
    }

    //存在订单
    @SuppressWarnings("rawtypes")
    private boolean haveOrder() {
        //SQL
        String querySql = "[TrainRefuseRefundData] @Method = 3, @IdNumber = " + orderId;
        //查询
        List list = Server.getInstance().getSystemService().findMapResultByProcedure(querySql);
        //存在
        return list != null && list.size() > 0;
    }

    //数据入库
    @SuppressWarnings("rawtypes")
    private void save() throws Exception {
        //当前日期
        String current = ElongHotelInterfaceUtil.getCurrentDate();
        //当天以后
        if (ElongHotelInterfaceUtil.getSubDays(current, departTime.split(" ")[0]) >= 0) {
            //SQL
            String saveSql;
            //SQL
            String querySql = "[TrainRefuseRefundData] @Method = 1, @IdNumber = " + ticketId;
            //查询
            List list = Server.getInstance().getSystemService().findMapResultByProcedure(querySql);
            //更新
            if (list != null && list.size() == 1) {
                //转换
                Map map = (Map) list.get(0);
                //ID
                long id = Long.parseLong(map.get("ID").toString());
                //时间
                Timestamp time = new Timestamp(System.currentTimeMillis());
                //SQL
                saveSql = "UPDATE TrainRefuseRefund SET CreateTime = '" + time + "' where ID = " + id;
            }
            //新增
            else {
                saveSql = "INSERT INTO TrainRefuseRefund(OrderId, TicketId, InterfaceType) VALUES(" + orderId + ", "
                        + ticketId + ", " + interfaceType + ")";
            }
            //入库
            Server.getInstance().getSystemService().excuteAdvertisementBySql(saveSql);
        }
    }

}