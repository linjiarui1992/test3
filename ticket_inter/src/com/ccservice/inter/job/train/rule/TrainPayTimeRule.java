package com.ccservice.inter.job.train.rule;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.thread.db.TrainCreateOrderDBUtil;
import com.ccservice.inter.server.Server;
import com.ccservice.qunar.util.ExceptionUtil;

/**
 * 支付超时时间规则
 * 
 * @time 2017年5月19日 上午10:38:47
 * @author fiend
 */
public class TrainPayTimeRule {

    /**
     * 存储支付超时时间-订单
     * 
     * @param interfaceOrderNumber
     * @param timeout
     * @time 2017年5月19日 上午10:38:35
     * @author fiend
     */
    public static void savePayTimeOut(String interfaceOrderNumber, String timeout) {
        if (ElongHotelInterfaceUtil.StringIsNull(timeout)) {
            timeout = defaultTimeOut();
            WriteLog.write("TrainPayTimeRule_savePayTimeOut", interfaceOrderNumber + "--->虚拟时间--->" + timeout);
        }
        String sql = " [sp_TrainOrderInfo_insert_PayTimeOut] @interfaceOrderNumber='" + interfaceOrderNumber
                + "', @PayTimeOut='" + timeout + "'";
        try {
            TrainCreateOrderDBUtil.execSql(sql);
            WriteLog.write("TrainPayTimeRule_savePayTimeOut", interfaceOrderNumber + "--->sql--->" + sql);
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("TrainPayTimeRule_savePayTimeOut_Exception", e, interfaceOrderNumber
                    + "--->sql--->" + sql);
        }
    }

    /**
     * 获取支付超时时间-订单
     * 
     * @param interfaceOrderNumber
     * @param dbTimeOut 订单中的订单超时时间trainorder.ordertimeout（比如淘宝就有）
     * @return
     * @time 2017年5月19日 上午10:52:19
     * @author fiend
     */
    @SuppressWarnings("rawtypes")
    public static String getPayTimeOut(String interfaceOrderNumber, String dbTimeOut) {
        String resultTimeout = "";
        String timeout = "";
        String sql = " [sp_TrainOrderInfo_select_PayTimeOut] @interfaceOrderNumber='" + interfaceOrderNumber + "'";
        try {
            List list = Server.getInstance().getSystemService().findMapResultByProcedure(sql);
            Map map = (Map) list.get(0);
            timeout = map.containsKey("PayTimeOut") ? map.get("PayTimeOut").toString() : "";
            WriteLog.write("TrainPayTimeRule_getPayTimeOut", interfaceOrderNumber + "--->sql--->" + sql
                    + "--->timeout--->" + timeout);
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("TrainPayTimeRule_getPayTimeOut_Exception", e, interfaceOrderNumber
                    + "--->sql--->" + sql);
        }
        if (ElongHotelInterfaceUtil.StringIsNull(timeout)) {
            timeout = defaultTimeOut();
            WriteLog.write("TrainPayTimeRule_getPayTimeOut", interfaceOrderNumber + "--->虚拟时间--->" + timeout);
        }
        boolean before = findTheMinimumTime(timeout, dbTimeOut);
        resultTimeout = before ? timeout : dbTimeOut;
        WriteLog.write("TrainPayTimeRule_getPayTimeOut", interfaceOrderNumber + "--->timeout--->" + timeout
                + "--->dbTimeOut--->" + dbTimeOut + "--->before--->" + before + "--->resultTimeout--->" + resultTimeout);
        return resultTimeout;
    }

    /**
     * 默认超时时间-当前时间+29分钟
     * 
     * @return
     * @time 2017年5月19日 上午10:52:35
     * @author fiend
     */
    private static String defaultTimeOut() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                .format(new Date(System.currentTimeMillis() + 29 * 60 * 1000));
    }

    /**
     * 找到最小时间,-这个方法契合逻辑使用，不能当工具用。
     * 
     * @param dateStr1  yyyy-MM-dd HH:mm:ss 严格按照格式请求
     * @param dateStr2  yyyy-MM-dd HH:mm:ss 也可以为空
     * @return  true 前面时间小，false 后面时间小
     * @time 2017年5月19日 上午11:02:47
     * @author fiend
     */
    private static boolean findTheMinimumTime(String timeout, String dbTimeOut) {
        boolean before = true;
        if (!ElongHotelInterfaceUtil.StringIsNull(dbTimeOut)) {
            try {
                before = dateStrComparison_yHdHms(timeout, dbTimeOut);
            }
            catch (ParseException e) {
                ExceptionUtil.writelogByException("TrainPayTimeRule_findTheMinimumTime_ParseException", e, timeout
                        + "--->" + dbTimeOut);
            }
        }
        return before;
    }

    /**
     * 时间比对
     * 
     * @param dateStr1
     * @param dateStr2
     * @return
     * @throws ParseException
     * @time 2017年5月19日 上午11:01:56
     * @author fiend
     */
    private static boolean dateStrComparison_yHdHms(String dateStr1, String dateStr2) throws ParseException {
        return dateStrComparison(dateStr1, dateStr2, "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 时间比对
     * 
     * @param dateStr1
     * @param dateStr2
     * @param simpdateStr1
     * @param simpdateStr2
     * @return
     * @time 2017年5月19日 上午10:59:26
     * @author fiend
     * @throws ParseException 
     */
    private static boolean dateStrComparison(String dateStr1, String dateStr2, String simpdateStr1, String simpdateStr2)
            throws ParseException {
        return new SimpleDateFormat(simpdateStr1).parse(dateStr1).before(
                new SimpleDateFormat(simpdateStr2).parse(dateStr2));
    }
}
