package com.ccservice.inter.job.train.reg;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.train.TrainSupplyMethod;

public class Job12306Registration_New extends TrainSupplyMethod implements Job {
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        String useProxy = PropertyUtil.getValue("Job12306Registration_useProxy", "train.reg.properties");
        String proxyHost = "-1";
        String proxyPort = "-1";
        //1:while 方式 2:定时任务方式
        String damaRuleUrl = PropertyUtil.getValue("Job12306Registration_main_type", "train.reg.properties");
        if ("3".equals(damaRuleUrl)) {

            new Job12306Registration_RegThread("", useProxy, proxyHost, proxyPort).start();
        }
        else {
            System.out.println("不注册啦啦啦");
        }
    }

    public static void main(String[] args) {
        String useProxy = PropertyUtil.getValue("Job12306Registration_useProxy", "train.reg.properties");
        String proxyHost = "-1";
        String proxyPort = "-1";
        new Job12306Registration_RegThread("", useProxy, proxyHost, proxyPort).start();
    }

}
