package com.ccservice.inter.job.train;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;

/**
 *  删除12306账号中不是已通过的乘客
 * @time 2014年8月30日 上午11:44:16
 * @author yinshubin
 */
public class JobDeletePerson extends TrainSupplyMethod implements Job {

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        // TODO Auto-generated method stub
        dodelete();
    }

    /**
     * 说明：遍历customeruser表
     * @time 2014年8月30日 上午11:44:25
     * @author yinshubin
     */
    public void dodelete() {
        String sql = "SELECT * FROM T_CUSTOMERUSER WHERE ID!=14377 AND C_AGENTID="
                + getSysconfigString("qunar_agentid");
        List<Customeruser> customeruserList = Server.getInstance().getMemberService()
                .findAllCustomeruserBySql(sql, -1, 0);
        WriteLog.write("train12306_delete", "当前12306账号总数：" + customeruserList.size());
        for (Customeruser customeruser : customeruserList) {
            //            deletePerson(customeruser.getLoginname(), customeruser.getLogpassword(), customeruser.getCardnunber());
            deletePerson(customeruser.getLoginname(), customeruser.getLogpassword(), customeruser.getCardnunber(),
                    customeruser.getMemberemail());
        }
    }

    /**
     * 说明：根据12306账号cookie 删除该账号中不是已通过的乘客
     * @param logname
     * @param logpassword
     * @param cookie
     * @time 2014年8月30日 上午11:44:34
     * @author yinshubin
     */
    //    public static void deletePerson(String logname, String logpassword, String cookie) {
    //        try {
    //            cookie = URLEncoder.encode(cookie, "UTF-8");
    //            //            String url = JobQunarOrder.getSystemConfig("Reptile_traininit_url");
    //            String url = JobGenerateOrder.reprul();
    //            String par = "datatypeflag=15&logname=" + logname + "&logpassword=" + logpassword + "&cookie=" + cookie;
    //            WriteLog.write("train12306_delete", "当前12306账号：" + logname + ":deletePerson：删除非已通过乘客：" + url + "(:)" + par);
    //            SendPostandGet.submitPost(url, par, "UTF-8").toString();
    //            WriteLog.write("train12306_delete", "当前12306账号：" + logname + ":deletePerson：删除非已通过乘客：完成");
    //        }
    //        catch (UnsupportedEncodingException e) {
    //            WriteLog.write("train12306_delete", "当前12306账号：" + logname + ":deletePerson：删除非已通过乘客：异常：" + e);
    //        }
    //    }

    /**
     * 说明：根据12306账号cookie 删除该账号中不是已通过的乘客
     * @param logname
     * @param logpassword
     * @param cookie
     * @time 2014年8月30日 上午11:44:34
     * @author yinshubin
     */
    public static void deletePerson(String logname, String logpassword, String cookie, String url) {
        try {
            cookie = URLEncoder.encode(cookie, "UTF-8");
            //            String url = JobQunarOrder.getSystemConfig("Reptile_traininit_url");
            //            String url = JobGenerateOrder.reprul();
            String par = "datatypeflag=15&logname=" + logname + "&logpassword=" + logpassword + "&cookie=" + cookie;
            WriteLog.write("train12306_delete", "当前12306账号：" + logname + ":deletePerson：删除非已通过乘客：" + url + "(:)" + par);
            SendPostandGet.submitPost(url, par, "UTF-8").toString();
            WriteLog.write("train12306_delete", "当前12306账号：" + logname + ":deletePerson：删除非已通过乘客：完成");
        }
        catch (UnsupportedEncodingException e) {
            WriteLog.write("train12306_delete", "当前12306账号：" + logname + ":deletePerson：删除非已通过乘客：异常：" + e);
        }
    }

    public static void main(String[] args) {
        //        dodelete();
    }
}
