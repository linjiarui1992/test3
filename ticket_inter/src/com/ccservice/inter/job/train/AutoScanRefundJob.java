package com.ccservice.inter.job.train;

import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.text.SimpleDateFormat;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;
import com.ccservice.inter.server.Server;

/**
 * JOB_扫描超时拒绝的退票，防止退成功，但拒了
 * @author WH
 * @time 2016年12月13日 上午11:31:40
 * @version 1.0
 */

public class AutoScanRefundJob extends TrainSupplyMethod implements Job {

    //每次等待时间，暂定两分钟
    private static final long wait = 2 * 60 * 1000;

    private boolean isNight() {
        //夜间
        boolean isNight = false;
        //判断
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            //当前
            Date current = sdf.parse(sdf.format(new Date()));
            //判断
            isNight = current.before(sdf.parse("06:00"));
        }
        catch (Exception e) {

        }
        //返回
        return isNight;
    }

    public void execute(JobExecutionContext context) throws JobExecutionException {
        while (true) {
            try {
                //非维护时间
                if (!isNight()) {
                    scan();
                }
            }
            catch (Exception e) {

            }
            finally {
                try {
                    Thread.sleep(wait);
                }
                catch (Exception e) {
                }
            }
        }
    }

    @SuppressWarnings("rawtypes")
    private void scan() {
        //所有
        String querySql = "[TrainRefuseRefundData] @Method = 1, @IdNumber = 0";
        //查询
        List list = Server.getInstance().getSystemService().findMapResultByProcedure(querySql);
        //长度
        int ticketSize = list.size();
        //PRINT
        System.out.println(ElongHotelInterfaceUtil.getCurrentTime() + "----扫描超时拒绝的退票----" + ticketSize);
        //有数据
        if (ticketSize > 0) {
            //数据
            Map<Long, List<Map>> datas = new HashMap<Long, List<Map>>();
            //遍历
            for (int i = 0; i < ticketSize; i++) {
                try {
                    //MAP
                    Map map = (Map) list.get(i);
                    //订单ID
                    long orderId = Long.parseLong(map.get("OrderId").toString());
                    //数据正确
                    if (orderId > 0) {
                        //车票数据
                        List<Map> temp = datas.get(orderId);
                        //初始数据
                        if (temp == null) {
                            temp = new ArrayList<Map>();
                        }
                        //添加数据
                        temp.add(map);
                        //添加数据
                        datas.put(orderId, temp);
                    }
                }
                catch (Exception e) {

                }
            }
            //线程池
            ExecutorService threadPool = Executors.newSingleThreadExecutor();
            //遍历
            for (Entry<Long, List<Map>> entry : datas.entrySet()) {
                threadPool.execute(new AutoScanRefundThread(entry.getKey(), entry.getValue()));
            }
            //关闭
            threadPool.shutdown();
        }
    }

}