package com.ccservice.inter.job.train;

import java.util.Map;
import java.util.List;
import java.util.Date;
import org.quartz.Job;
import java.util.HashMap;
import java.util.ArrayList;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import com.weixin.util.RequestUtil;
import java.util.concurrent.Executors;
import org.quartz.JobExecutionContext;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.inter.job.WriteLog;
import org.quartz.JobExecutionException;
import com.ccservice.inter.server.Server;
import java.util.concurrent.ExecutorService;
import com.ccservice.qunar.util.ExceptionUtil;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.policy.ben.RepServerBean;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.atom.service12306.AccountSystem;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;

/**
 * 退款核验
 * @author WH
 */

public class CheckReturnTicketJob extends TrainSupplyMethod implements Job {

    private static final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

    public void execute(JobExecutionContext context) throws JobExecutionException {
        if (isopen()) {
            try {
                //暂停
                stop();
                //退票
                getRefundTickets();
            }
            catch (Exception e) {
                ExceptionUtil.writelogByException("CheckReturnTicketJob_Exception", e);
            }
            finally {
                //开启
                start();
            }
        }
    }

    private void stop() {
        Server.getInstance().getDateHashMap().put("AutoRefundPrice", "0");
    }

    private void start() {
        Server.getInstance().getDateHashMap().put("AutoRefundPrice", "1");
    }

    //判断开关
    private boolean isopen() {
        //标识
        String flag = Server.getInstance().getDateHashMap().get("AutoRefundPrice");
        //返回
        return ElongHotelInterfaceUtil.StringIsNull(flag) || "1".equals(flag);
    }

    //7点5分开始退款
    private boolean rightTime() {
        try {
            //7点5分
            Date start = sdf.parse("06:05:00");
            //当前
            Date current = sdf.parse(sdf.format(new Date()));
            //判断
            if (current.before(start)) {
                return false;
            }
        }
        catch (Exception e) {

        }
        return true;
    }

    @SuppressWarnings("rawtypes")
    private void getRefundTickets() {
        if (!rightTime()) {
            return;
        }
        //退款地址
        String trainRefundPriceUrl = getSysconfigString("trainRefundPrice");
        if (ElongHotelInterfaceUtil.StringIsNull(trainRefundPriceUrl)) {
            return;
        }
        int refundTicketDay = Integer.parseInt(getSysconfigString("RefundTicketDay"));
        if (refundTicketDay < 0) {
            refundTicketDay = -refundTicketDay;
        }
        //退票申请时间
        if (refundTicketDay > 0) {
            refundTicketDay = -refundTicketDay;
        }
        String refundRequestTime = "";
        String currentDate = ElongHotelInterfaceUtil.getCurrentDate();
        try {
            if (refundTicketDay == 0) {
                refundRequestTime = currentDate;
            }
            else {
                refundRequestTime = ElongHotelInterfaceUtil.getAddDate(currentDate, refundTicketDay);
            }
        }
        catch (Exception e) {
            refundRequestTime = currentDate;
        }
        if (ElongHotelInterfaceUtil.StringIsNull(refundRequestTime)) {
            refundRequestTime = currentDate;
        }
        refundRequestTime = refundRequestTime + " 00:00:00";
        //退票个数
        int check_num = Integer.parseInt(getSysconfigString("RefundTicketNumber")) * 2;
        //等待退款查询
        int ts = Trainticket.WAITREFUND;
        String sql = "select top " + check_num + " o.ID orderId, ISNULL(o.C_INTERFACETYPE, 0) interfaceType,"
                + " o.C_EXTNUMBER sequenceNo, o.C_CREATETIME createTime, o.C_SUPPLYACCOUNT userName, t.ID ticketId,"
                + " t.C_TICKETNO ticketNo, t.C_PROCEDURE fee, ISNULL(t.C_TCTICKETNO, '') tcTicketNo,"
                + " ISNULL(t.C_CHANGEID, 0) changeId, ISNULL(t.C_CHANGETYPE, 0) changeType, p.C_NAME passengerName,"
                + " ISNULL(o.ordertype, 1) orderType, o.C_QUNARORDERNUMBER qunarOrderNumber, o.C_AGENTID agentId"
                + " from T_TRAINTICKET t with(nolock)"
                + " inner join T_TRAINPASSENGER p with(nolock) on p.ID = t.C_TRAINPID"
                + " inner join T_TRAINORDER o with(nolock) on o.ID = p.C_ORDERID where t.C_STATUS = " + ts
                + " and t.C_REFUNDREQUESTTIME >= '" + refundRequestTime + "'"
                + " and t.C_ISQUESTIONTICKET = 0 and t.C_REFUNDTYPE = 1"
                + " and t.C_ISAPPLYTICKET in (1, 3) and t.C_PROCEDURE >= 0";
        //退票结果
        List list = new ArrayList();
        //查询数据
        try {
            list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("CheckReturnTicketJob_Exception", e);
        }
        //当前退票数量
        int listSize = list == null ? 0 : list.size();
        //PRINT
        System.out.println(ElongHotelInterfaceUtil.getCurrentTime() + "-----开始审核退票-----" + listSize);
        //更新车票为审核中
        String ids = "";
        //同一订单车票，同时审核
        Map<Long, JSONObject> reqMap = new HashMap<Long, JSONObject>();
        Map<Long, Map<String, Long>> ticketIds = new HashMap<Long, Map<String, Long>>();
        Map<Long, Map<Long, Float>> ticketProcedures = new HashMap<Long, Map<Long, Float>>();
        //循环
        for (int i = 0; i < listSize; i++) {
            long ticketId = 0l;
            try {
                Map map = (Map) list.get(i);
                ticketId = Long.parseLong(map.get("ticketId").toString());
                long orderId = Long.parseLong(map.get("orderId").toString());
                String sequenceNo = map.get("sequenceNo").toString().trim();
                String createTime = map.get("createTime").toString();
                String userName = map.get("userName").toString().trim();
                String ticketNo = map.get("ticketNo").toString().trim();
                String tcTicketNo = map.get("tcTicketNo").toString();
                long changeId = Long.parseLong(map.get("changeId").toString());
                int changeType = Integer.parseInt(map.get("changeType").toString());
                String passengerName = map.get("passengerName").toString();
                float procedure = Float.parseFloat(map.get("fee").toString());
                int interfaceType = Integer.parseInt(map.get("interfaceType").toString());
                int orderType = Integer.parseInt(map.get("orderType").toString());
                String qunarOrderNumber = map.get("qunarOrderNumber").toString();
                long agentId = Long.parseLong(map.get("agentId").toString());
                //请求
                JSONObject json = reqMap.get(orderId);
                if (json == null) {
                    json = new JSONObject();
                }
                json.put("agentId", agentId);
                json.put("userName", userName);
                json.put("orderType", orderType);
                json.put("sequence_no", sequenceNo);
                json.put("interfaceType", interfaceType);
                json.put("passenger_name", passengerName);
                json.put("qunarOrderNumber", qunarOrderNumber);
                json.put("order_date", createTime.split(" ")[0]);
                //车票号
                JSONArray ticket_no = json.getJSONArray("ticket_no");
                if (ticket_no == null) {
                    ticket_no = new JSONArray();
                }
                //当前车票
                String currentNo = ticketNo;
                if (changeId > 0 && (changeType == 1 || changeType == 2)) {
                    currentNo = tcTicketNo;
                }
                //ADD
                ticket_no.add(currentNo);
                //PUT
                json.put("ticket_no", ticket_no);
                //车票ID
                Map<String, Long> tempMap = ticketIds.get(orderId);
                if (tempMap == null) {
                    tempMap = new HashMap<String, Long>();
                }
                tempMap.put(currentNo, ticketId);
                //手续费
                Map<Long, Float> ProcedureMap = ticketProcedures.get(orderId);
                if (ProcedureMap == null) {
                    ProcedureMap = new HashMap<Long, Float>();
                }
                ProcedureMap.put(ticketId, procedure);
                //PUT
                reqMap.put(orderId, json);
                ticketIds.put(orderId, tempMap);
                ticketProcedures.put(orderId, ProcedureMap);
                //车票ID
                ids += ticketId + ",";
            }
            catch (Exception e) {
                QuestionTicket(ticketId, 0);
            }
        }
        if (!ids.endsWith(",")) {
            return;
        }
        //ID
        ids = ids.substring(0, ids.length() - 1);
        //SQL
        sql = "update T_TRAINTICKET set C_REFUNDTYPE = 2 where ID in (" + ids + ") and C_STATUS = " + ts;
        //更新状态为审核中
        boolean UpdateTrue = false;
        //更新车票
        try {
            UpdateTrue = Server.getInstance().getSystemService().excuteAdvertisementBySql(sql) > 0;
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("CheckReturnTicketJob_Exception", e);
        }
        //更新状态成功，开启退款
        if (UpdateTrue) {
            start();
        }
        //失败重审次数
        int CheckRefundCount = Integer.parseInt(getSysconfigString("CheckRefundCount"));
        //小于，默认3次
        if (CheckRefundCount < 1) {
            CheckRefundCount = 3;
        }
        //线程池
        ExecutorService pool = Executors.newFixedThreadPool(reqMap.size());
        //循环
        for (long orderId : reqMap.keySet()) {
            Map<String, Long> tempMap = ticketIds.get(orderId);
            Map<Long, Float> ProcedureMap = ticketProcedures.get(orderId);
            try {
                if (!UpdateTrue) {
                    throw new Exception("Update Error.");
                }
                JSONObject req = reqMap.get(orderId);
                //订单归属
                int interfaceType = req.getIntValue("interfaceType");
                interfaceType = interfaceType > 0 ? interfaceType : getOrderAttribution(orderId);
                if (interfaceType <= 0) {
                    throw new Exception("订单归属类型错误！");
                }
                //线程执行
                pool.execute(new GetRepCheckData(orderId, req, tempMap, ProcedureMap, CheckRefundCount,
                        trainRefundPriceUrl, interfaceType, req.getString("userName"), req.getIntValue("orderType"),
                        req.getInteger("agentId"), req.getString("qunarOrderNumber")));
            }
            catch (Exception e) {
                for (String ticketNo : tempMap.keySet()) {
                    QuestionTicket(tempMap.get(ticketNo), 2);
                }
            }
        }
        //关闭
        pool.shutdown();
    }

    //退票问题
    private void QuestionTicket(long ticketId, int refundType) {
        try {
            if (ticketId > 0) {
                //SQL
                String sql = "update T_TRAINTICKET set C_ISQUESTIONTICKET = " + Trainticket.CHECKQUESTION
                        + " where ID = " + ticketId + " and C_STATUS = " + Trainticket.WAITREFUND
                        + " and C_ISQUESTIONTICKET = 0";
                if (refundType > 0) {
                    sql += " and C_REFUNDTYPE = " + refundType;
                }
                //更新
                Server.getInstance().getSystemService().excuteAdvertisementBySql(sql);
            }
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("CheckReturnTicketJob_Exception", e);
        }
    }
}

class GetRepCheckData extends Thread {

    private long orderId;

    private JSONObject json;

    private int interfacetype;

    private String userName;

    private int CheckRefundCount;

    private String trainRefundPriceUrl;

    private Map<String, Long> ticketIds;//<车票号,车票ID>

    private Map<Long, Float> ticketProcedures;//手续费

    private TrainSupplyMethod supplyMethod;

    private int trainOrderType;

    private long agentId;

    private String qunarOrderNumber;

    private boolean goApp = false; //走手机端

    public GetRepCheckData(long orderId, JSONObject json, Map<String, Long> ticketIds,
            Map<Long, Float> ticketProcedures, int CheckRefundCount, String trainRefundPriceUrl, int interfacetype,
            String userName, int trainOrderType, long agentId, String qunarOrderNumber) {
        this.json = json;
        this.agentId = agentId;
        this.orderId = orderId;
        this.userName = userName;
        this.ticketIds = ticketIds;
        this.interfacetype = interfacetype;
        this.trainOrderType = trainOrderType;
        this.CheckRefundCount = CheckRefundCount;
        this.ticketProcedures = ticketProcedures;
        this.qunarOrderNumber = qunarOrderNumber;
        this.supplyMethod = new TrainSupplyMethod();
        this.trainRefundPriceUrl = trainRefundPriceUrl;
    }

    /**
     * 判断走手机端
     * @author WH
     * @time 2017年7月24日 下午4:09:29
     * @version 1.0
     */
    private boolean goApp(Trainorder order) {
        return supplyMethod.goApp(order, AppBusinessType.REFUND_CHECK, goApp);
    }

    public void run() {
        //获取账号次数
        int get = 0;
        //释放账号次数
        int free = 0;
        //超时，单位：毫秒
        int timeout = 60 * 1000;
        //成功
        List<Long> SuccessList = new ArrayList<Long>();
        //未退成功
        List<Long> RefundFailList = new ArrayList<Long>();
        //获取账号
        Customeruser user = new Customeruser();
        //设置虚拟
        Trainorder order = new Trainorder();
        order.setId(orderId);
        order.setAgentid(agentId);
        order.setSupplyaccount(userName);
        order.setOrdertype(trainOrderType);
        order.setInterfacetype(interfacetype);
        order.setQunarOrdernumber(qunarOrderNumber);
        try {
            get++;
            //暂停一会
            Thread.sleep(5000);
            //走手机端
            goApp = goApp(order);
            //获取账号
            user = supplyMethod.getCustomerUserBy12306Account(order, false, goApp);
        }
        catch (Exception e) {

        }
        //重新获取
        boolean needGetGoApp = true;
        //开始审核
        for (int idx = 0; idx < CheckRefundCount; idx++) {
            try {
                if (user == null) {
                    user = new Customeruser();
                }
                //不获取（未登录时）
                if (!needGetGoApp) {
                    //重置
                    needGetGoApp = true;
                }
                //要获取（第一次循环取初始化账号里的）
                else if (idx > 0) {
                    //走手机端
                    goApp = goApp(order);
                }
                //每次重置
                RefundFailList = new ArrayList<Long>();
                //Cookie
                String cookie = user.getCardnunber();
                json.put("cookie", cookie);//设置请求Cookie
                //REP标识
                String datatypeflag = goApp ? "refundCheckByApp" : "105";
                //请求参数URLEncoder
                String jsonStr = URLEncoder.encode(json.toString(), "UTF-8");
                //Cookie为空
                boolean cookieIsNull = ElongHotelInterfaceUtil.StringIsNull(cookie);
                //获取REP
                RepServerBean rep = cookieIsNull ? new RepServerBean() : RepServerUtil.getRepServer(user, false);
                //请求参数
                String param = "datatypeflag=" + datatypeflag + "&jsonStr=" + jsonStr
                        + supplyMethod.JoinCommonAccountInfo(user, rep);
                //请求REP
                String retdata = cookieIsNull ? "用户未登录" : RequestUtil.post(rep.getUrl(), param, "UTF-8",
                        new HashMap<String, String>(), timeout);
                //用户未登录
                if (!cookieIsNull && retdata.contains("用户未登录") && RepServerUtil.changeRepServer(user)) {
                    //切换REP
                    rep = RepServerUtil.getTaoBaoTuoGuanRepServer(user, false);
                    //类型正确
                    if (rep.getType() == 1) {
                        //重拼参数
                        param = "datatypeflag=" + datatypeflag + "&jsonStr=" + jsonStr
                                + supplyMethod.JoinCommonAccountInfo(user, rep);
                        //重新请求
                        retdata = cookieIsNull ? "用户未登录" : RequestUtil.post(rep.getUrl(), param, "UTF-8",
                                new HashMap<String, String>(), timeout);
                    }
                }
                //用户未登录
                if (Account12306Util.accountNoLogin(retdata, user)) {
                    free++;
                    //释放未登录
                    supplyMethod.FreeNoLogin(user);
                    //非最后一次、非不登录重试，重拿账号
                    if (idx != CheckRefundCount - 1 && !user.isDontRetryLogin()) {
                        //获取次数
                        get++;
                        //走手机端
                        goApp = goApp(order);
                        //已经获取
                        needGetGoApp = false;
                        //获取账号
                        user = supplyMethod.getCustomerUserBy12306Account(order, true, goApp);
                    }
                    //重走
                    continue;
                }
                //解析数据
                JSONObject retobj = JSONObject.parseObject(retdata);
                //获取数据失败
                if (!retobj.getBooleanValue("success")) {
                    continue;
                }
                JSONArray refunds = retobj.getJSONArray("refunds");
                if (refunds == null || refunds.size() == 0) {
                    continue;
                }
                for (int i = 0; i < refunds.size(); i++) {
                    JSONObject refund = refunds.getJSONObject(i);
                    //车票号
                    String ticket_no = refund.getString("ticket_no");
                    //存在结果
                    if (!ElongHotelInterfaceUtil.StringIsNull(ticket_no) && ticketIds.containsKey(ticket_no)) {
                        //车票ID
                        long ticketId = ticketIds.get(ticket_no);
                        //车票状态
                        String ticket_status_name = refund.getString("ticket_status_name");
                        //打印消息
                        System.out.println(ElongHotelInterfaceUtil.getCurrentTime() + "---" + orderId + "---"
                                + ticketId + "---" + ticket_status_name);
                        //已支付、改签票、变更到站票
                        if ("已支付".equals(ticket_status_name) || "改签票".equals(ticket_status_name)
                                || "变更到站票".equals(ticket_status_name)) {
                            //失败ID
                            if (!RefundFailList.contains(ticketId)) {
                                RefundFailList.add(ticketId);
                            }
                        }
                        //退票成功
                        else if (ticket_status_name.contains("已退票") && ticket_status_name.contains("业务流水号")) {
                            //成功ID
                            if (!SuccessList.contains(ticketId)) {
                                SuccessList.add(ticketId);
                            }
                            //移除
                            ticketIds.remove(ticket_no);
                        }
                    }
                }
                if (ticketIds.size() == 0) {
                    break;
                }
                //减少12306状态未变化可能
                if (RefundFailList.size() > 0) {
                    Thread.sleep(10 * 1000);
                }
            }
            catch (Exception e) {
            }
        }
        //失败
        if (ticketIds.size() > 0) {
            String failIds = "";
            String errorIds = "";
            for (String ticket_no : ticketIds.keySet()) {
                //车票ID
                long ticketId = ticketIds.get(ticket_no);
                //失败ID
                if (RefundFailList.contains(ticketId)) {
                    failIds += ticketId + ",";
                }
                //错误ID
                else {
                    errorIds += ticketId + ",";
                }
            }
            //存在失败
            if (failIds.endsWith(",")) {
                //去结尾
                failIds = failIds.substring(0, failIds.length() - 1);
                //重置车票
                RefundFail(failIds);
                //记录日志
                WriteLog.write("CheckRefundFail", orderId + "-->" + failIds);
            }
            //存在错误
            if (errorIds.endsWith(",")) {
                QuestionTicket(errorIds.substring(0, errorIds.length() - 1));
            }
        }
        //成功
        if (SuccessList.size() > 0) {
            String successIds = "";
            for (long ticketId : SuccessList) {
                successIds += ticketId + ",";
            }
            SuccessTicket(successIds.substring(0, successIds.length() - 1));
            //退款回调
            for (long ticketId : SuccessList) {
                //退款参数
                String url = trainRefundPriceUrl + "?trainorderid=" + orderId + "&ticketid=" + ticketId
                        + "&interfacetype=" + interfacetype + "&procedure=" + ticketProcedures.get(ticketId)
                        + "&responseurl=" + trainRefundPriceUrl;
                SendPostandGet.submitGet(url, "UTF-8");
            }
        }
        //次数
        int freeCount = SuccessList.size() + get - free;
        //释放
        if (freeCount > 0) {
            //退票成功已释放REP
            //REP信息置空，不同时释放
            //审核成功、当前取和释放次数一样
            if (get == free) {
                user.setMemberemail("");
            }
            //释放账号
            supplyMethod.FreeUserFromAccountSystem(user, AccountSystem.FreeNoCare, freeCount, AccountSystem.ZeroCancel,
                    AccountSystem.NullDepartTime);
        }
    }

    //退票成功
    private void SuccessTicket(String ticketIds) {
        if (!ElongHotelInterfaceUtil.StringIsNull(ticketIds)) {
            //SQL
            String sql = "update T_TRAINTICKET set C_REFUNDTYPE = 3, C_REFUNDSUCCESSTIME = '"
                    + ElongHotelInterfaceUtil.getCurrentTime() + "' where ID in (" + ticketIds + ")";
            //更新
            Server.getInstance().getSystemService().excuteAdvertisementBySql(sql);
        }
    }

    //退票问题
    private void QuestionTicket(String ticketIds) {
        if (!ElongHotelInterfaceUtil.StringIsNull(ticketIds)) {
            //SQL
            String sql = "update T_TRAINTICKET set C_REFUNDTYPE = 0, C_ISQUESTIONTICKET = " + Trainticket.CHECKQUESTION
                    + " where ID in (" + ticketIds + ") and C_STATUS = " + Trainticket.WAITREFUND;
            //更新
            Server.getInstance().getSystemService().excuteAdvertisementBySql(sql);
        }
    }

    //退票失败，重置
    private void RefundFail(String ticketIds) {
        if (!ElongHotelInterfaceUtil.StringIsNull(ticketIds)) {
            //SQL            
            String sql = "update T_TRAINTICKET set C_REFUNDTYPE = 0, C_ISQUESTIONTICKET = " + Trainticket.NOQUESTION
                    + ", C_STATUS = " + Trainticket.APPLYTREFUND + " where ID in (" + ticketIds + ") and C_STATUS = "
                    + Trainticket.WAITREFUND;
            //更新
            Server.getInstance().getSystemService().excuteAdvertisementBySql(sql);
        }
    }

}