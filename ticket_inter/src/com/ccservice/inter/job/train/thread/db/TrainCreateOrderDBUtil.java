package com.ccservice.inter.job.train.thread.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.PropertyUtil;
import com.ccservice.Util.db.DBHelper;
import com.ccservice.Util.db.DBSourceEnum;
import com.ccservice.Util.db.DataColumn;
import com.ccservice.Util.db.DataRow;
import com.ccservice.Util.db.DataTable;
import com.ccservice.Util.file.ExceptionUtil;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.b2b2c.base.train.TrainStudentInfo;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ggjs.config.netty.InitServer;

/**
 * 下单消费者优化,取消Server 调用db,改用 TrainCreateOrderDBUtil
 * 
 * @author baozz
 *
 */
public class TrainCreateOrderDBUtil {
    static DBSourceEnum dbSourceEnum = getTrainCreateOrderDBSourceEnum();

    public static DBSourceEnum getTrainCreateOrderDBSourceEnum() {
        String value = PropertyUtil.getValue("pingTaiType", "rabbitMQ.properties");
        for (DBSourceEnum dbSourceEnum : DBSourceEnum.values()) {
            if (dbSourceEnum.getNo() == Integer.parseInt(value)) {
                return dbSourceEnum;
            }
        }
        return DBSourceEnum.DEFAULT_DB;
    }

    /**
     * 执行sql
     * 
     * @describe
     * @author baozz
     * @time 2017年12月19日下午3:02:09
     *
     */
    public static void excuteGiftBySql(String sql) {
        execSql(sql);
    }

    public static void main(String[] args) {
        InitServer.start();
        //        Trainorder trainorder = findTrainorder(73419047);
        //        Trainorder trainorder = findTrainorder(73419049);
//        Trainorder trainorder = findTrainorder(73419050);
        Trainorder trainorder = findTrainorder(73419051);
        System.out.println(JSONObject.toJSONString(trainorder));
        System.exit(-1);
    }

    /**
     * 创建rc
     * 
     * @describe
     * @author baozz
     * @time 2017年12月15日下午1:27:44 INSERT INTO [T_TRAINORDERRC]
     *       ([C_ORDERID],[C_TICKETID],[C_STATUS],[C_CREATETIME] ,[C_CREATEUSER]
     *       ,[C_CONTENT],[C_YWTYPE]) VALUES
     *       (#orderid#,#ticketid#,#status#,getdate() ,#createuser#
     *       ,#content#,#ywtype#)
     */
    public static void createTrainorderrc(Trainorderrc rc) {
        // String sql = "INSERT INTO [T_TRAINORDERRC]
        // ([C_ORDERID],[C_TICKETID],[C_STATUS],[C_CREATETIME] ,[C_CREATEUSER]
        // ,[C_CONTENT],[C_YWTYPE]) VALUES
        // (#orderid#,#ticketid#,#status#,getdate(),#createuser#,#content#,#ywtype#";
        // Map<String, Object> valueMap = new HashMap<String, Object>();
        // valueMap.put("C_ORDERID", rc.getOrderid());
        // valueMap.put("C_TICKETID", rc.getTicketid());
        // valueMap.put("C_STATUS", rc.getStatus());
        // valueMap.put("C_CREATETIME", new Timestamp(new Date().getTime()));
        // valueMap.put("C_CREATEUSER", rc.getCreateuser());
        // valueMap.put("C_CONTENT", rc.getContent());
        // valueMap.put("C_YWTYPE", rc.getYwtype());
        // DBHelper.insertGeneratedKey("T_TRAINORDERRC", valueMap, dbSourceEnum);
        String sql = "insertTrainOrderRC @orderId = " + rc.getOrderid() + ", @ticketId = " + rc.getTicketid()
                + " , @status = " + rc.getStatus() + " , @createUser = " + rc.getCreateuser() + " , @content ='"
                + rc.getContent() + "' , @ywType = " + rc.getYwtype();
        execSql(sql);
        // DBHelper.executeSql(sql, dbSourceEnum);
    }

    /**
     * 得到系统属性
     * 
     * @describe
     * @author baozz
     * @time 2017年12月15日下午2:12:58
     *
     */
    public static String getSysconfigString(String name) {
        DataRow row = getDataRow("EXEC [sp_T_SYSCONFIG_selectInfo] @name = '" + name + "'", dbSourceEnum);
        return row == null ? "-1" : row.GetColumnString("C_VALUE");
    }

    /**
     * @describe 查找订单信息
     * @author baozz
     * @time 2017年12月25日上午11:53:39
     */
    public static Trainorder findTrainorder(long orderid) {
        //根据sql 查找唯一的订单信息
        String sql = "findTrainOrderByOrderId @orderId=" + orderid;
        Trainorder trainorder = null;
        try {
            DataTable table = GetDataTable(sql, dbSourceEnum);
            if (table != null) {
                List<DataRow> orderMessages = table.GetRow();
                if (orderMessages != null && !orderMessages.isEmpty()) {
                    TrainCreateOrderDBMethod trainCreateOrderDBMethod = new TrainCreateOrderDBMethod();
                    List<Trainpassenger> trainpassengers = new ArrayList<>();
                    for (DataRow row : orderMessages) {
                        if (trainorder == null) {
                            //**********************************填充 订单信息 开始****************************************************//
                            trainorder = new Trainorder();
                            //订单id
                            trainCreateOrderDBMethod.paddingOrder(trainorder, row);
                            //**********************************填充 订单信息 结束****************************************************//
                        }
                        if (trainCreateOrderDBMethod.isNotExistTrainPassengers(row, trainpassengers)) {
                            //**********************************填充 乘客信息 开始****************************************************//
                            //第一个乘客
                            Trainpassenger trainpassenger = null;
                            if (trainCreateOrderDBMethod.isgroupPassengers(trainorder.getPassengers(), row) == null) {
                                trainpassenger = paddingTrainpassengers(trainCreateOrderDBMethod, trainpassengers, row);
                            }
                            else {
                                if (trainCreateOrderDBMethod.isSamePassenger(row, trainorder.getPassengers())) {
                                    trainpassenger = trainCreateOrderDBMethod.getTrainPassengerByPid(row,
                                            trainorder.getPassengers());
                                }
                                else {
                                    trainpassenger = paddingTrainpassengers(trainCreateOrderDBMethod, trainpassengers,
                                            row);
                                }
                            }
                            //**********************************填充 车票信息 开始****************************************************//
                            List<Trainticket> traintickets = null;
                            if (trainpassenger.getTraintickets() == null) {
                                traintickets = new ArrayList<Trainticket>();
                            }
                            else {
                                traintickets = trainpassenger.getTraintickets();
                            }
                            if (trainCreateOrderDBMethod.isNotExistTrainTickets(row, traintickets)) {
                                Trainticket trainticket = new Trainticket();
                                trainCreateOrderDBMethod.paddingTicket(row, traintickets, trainticket);
                                traintickets.add(trainticket);
                            }
                            trainpassenger.setTraintickets(traintickets);
                            //**********************************填充 车票信息 结束****************************************************//
                            trainorder.setPassengers(trainpassengers);
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("TrainCreateOrderDBUtil_FindTrainorder_Exception", e,
                    orderid + "-->" + sql);
        }
        return trainorder;
    }

    private static Trainpassenger paddingTrainpassengers(TrainCreateOrderDBMethod trainCreateOrderDBMethod,
            List<Trainpassenger> trainpassengers, DataRow row) {
        Trainpassenger trainpassenger = new Trainpassenger();
        //乘客中的订单id
        trainCreateOrderDBMethod.paddingPassenger(row, trainpassenger);
        //**********************************填充 学生信息 开始****************************************************//
        if (row.GetColumn("SIID").GetValue() != null) {
            List<TrainStudentInfo> trainStudentInfos = new ArrayList<TrainStudentInfo>();
            if (trainCreateOrderDBMethod.isNotExistTrainStudents(row, trainStudentInfos)) {
                TrainStudentInfo trainStudentInfo = new TrainStudentInfo();
                trainCreateOrderDBMethod.paddingStudent(row, trainStudentInfo);
                trainStudentInfos.add(trainStudentInfo);
            }
            trainpassenger.setTrainstudentinfos(trainStudentInfos);
        }
        //**********************************填充 学生信息 结束****************************************************//
        trainpassengers.add(trainpassenger);
        return trainpassenger;
    }

    /**
     * @describe 根据sql 查询出一行数据
     * @author baozz
     * @time 2017年12月15日下午2:00:55
     *
     */
    public static DataRow getDataRow(String sql, DBSourceEnum dbSourceEnum) {
        try {
            DataTable dataTable = DBHelper.GetDataTable(sql, dbSourceEnum);
            if (dataTable != null && dataTable.GetRow() != null && dataTable.GetRow().size() > 0) {
                return dataTable.GetRow().get(0);
            }
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("TrainCreateOrderDBUtil_getDataRow_Exception", e, "执行sql:" + sql);
        }
        return null;
    }

    /**
     * @describe 得到一个 DataTable
     * @author baozz
     * @time 2018年1月3日上午9:48:22
     */
    public static DataTable GetDataTable(String sql, DBSourceEnum dbSourceEnum) throws Exception {
        return DBHelper.GetDataTable(sql, dbSourceEnum);
    }

    /**
     * @describe 外界传入枚举进行更新动作
     * @author baozz
     * @time 2017年12月27日下午3:24:40
     */
    public static void execSql(String sql, DBSourceEnum dbSourceEnum) {
        try {
            DBHelper.executeSql(sql, dbSourceEnum);
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("TrainCreateOrderDBUtil_execSql_Exception", e, "执行sql出错:[" + sql + "]");
        }
    }

    /**
     * 修改订单信息
     * 
     * @describe
     * @author baozz
     * @time 2017年12月19日下午3:09:27
     */
    public static void updateTrainorder(Trainorder trainorder) {
        execSql(getUpdateTrain(trainorder));
    }

    /**
     * @describe 得到修改订单的sql语句
     * @author baozz
     * @time 2017年12月25日上午11:25:26
     *
     */
    private static String getUpdateTrain(Trainorder trainorder) {
        StringBuffer sb = new StringBuffer();
        sb.append("UPDATE T_TRAINORDER SET ");
        addtoSb(sb, "C_ORDERSTATUS", trainorder.getOrderstatus());
        addtoSb(sb, "C_PAYMETHOD", trainorder.getPaymethod());
        addtoSb(sb, "C_EXTNUMBER", trainorder.getExtnumber());
        addtoSb(sb, "C_NOTE", trainorder.getNote());
        addtoSb(sb, "C_OPERATEUID", trainorder.getOperateuid());
        addtoSb(sb, "C_TOTALPRICE", trainorder.getOrderprice());
        addtoSb(sb, "C_REFUSEAFFIRM", trainorder.getRefuseaffirm());
        addtoSb(sb, "C_SUPPLYPRICE", trainorder.getSupplyprice());
        addtoSb(sb, "C_SUPPLYPAYWAY", trainorder.getSupplypayway());
        addtoSb(sb, "C_SUPPLYACCOUNT", trainorder.getSupplyaccount());
        addtoSb(sb, "C_SUPPLYTRADENO", trainorder.getSupplytradeno());
        addtoSb(sb, "C_CHANGESUPPLYPRICE", trainorder.getChangesupplyprice());
        addtoSb(sb, "C_CHANGESUPPLYPAYWAY", trainorder.getChangesupplypayway());
        addtoSb(sb, "C_CHANGESUPPLYACCOUNT", trainorder.getChangesupplyaccount());
        addtoSb(sb, "C_CHANGESUPPLYTRADENO", trainorder.getChangesupplytradeno());
        addtoSb(sb, "C_CONTROLNAME", trainorder.getControlname());
        addtoSb(sb, "C_TCPROCEDURE", trainorder.getTcprocedure());
        addtoSb(sb, "C_ISQUESTIONORDER", trainorder.getIsquestionorder());
        addtoSb(sb, "C_PAYSUPPLYSTATUS", trainorder.getPaysupplystatus());
        addtoSb(sb, "C_REFUNDREASON", trainorder.getRefundreason());
        addtoSb(sb, "C_ISPLACEING", trainorder.getIsplaceing());
        addtoSb(sb, "C_CHANGEREFUNDPRICE", trainorder.getChangerefundprice());
        addtoSb(sb, "C_AUTOUNIONPAYURL", trainorder.getAutounionpayurl());
        addtoSb(sb, "C_AUTOUNIONPAYURLSECOND", trainorder.getAutounionpayurlsecond());
        addtoSb(sb, "C_ISCHANGEREFUNDORDER", trainorder.getIschangerefundorder());
        addtoSb(sb, "C_STATE12306", trainorder.getState12306());
        addtoSb(sb, "C_12306USERID", trainorder.getUser12306id());
        addtoSb(sb, "C_INTERFACETYPE", trainorder.getInterfacetype());
        addtoSb(sb, "C_ENREFUNDABLE", trainorder.getEnrefundable());
        addtoSb(sb, "ordertype", trainorder.getOrdertype());
        addtoSb(sb, "ChuPiaoAgentId", trainorder.getChupiaoagentid());
        addtoSb(sb, "ChupiaoTime", trainorder.getChupiaotime());
        addtoSb(sb, "C_TRADENO", trainorder.getTradeno());
        String sql = sb.substring(0, sb.length() - 1) + " where ID= " + trainorder.getId();
        return sql;
    }

    /**
     * @describe 将 key value 形式 填充到 StringBuffer 中 用来组成sql
     * @author baozz
     * @time 2018年1月3日上午9:49:08
     */
    private static void addtoSb(StringBuffer sb, String key, Object value) {
        if (value instanceof String && compareString((String) value)) {
            sb.append(key + " = " + " '" + value + "'" + " ,");
        }
        if (value instanceof Integer && compareInteger((Integer) value)) {
            sb.append(key + " = " + " " + value + " ,");
        }
        if (value instanceof Long && compareLong((Long) value)) {
            sb.append(key + " = " + " " + value + " ,");
        }
        if (value instanceof Float && compareFloat((Float) value)) {
            sb.append(key + " = " + " " + value + " ,");
        }
    }

    /**
     * @describe 执行sql 返回执行的结果
     * @author baozz
     * @time 2018年1月3日上午9:50:03
     */
    public static int execSql(String sql) {
        try {
            return DBHelper.executeSql(sql, dbSourceEnum) ? 1 : 0;
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("TrainCreateOrderDBUtil_execSql_Exception", e, "执行的sql:[" + sql + "]");
            return 0;
        }
    }

    // //TODO 需要得账号 需要进行修改
    // public static Customeruser getcustomeruser(Trainorder trainorder) {
    // Server instance = Server.getInstance();
    // // instance.setUrl("http://121.40.182.167:8080/cn_interface/service/");
    // // instance.seturlAtom("http://121.40.182.167:8080/cn_interface/service/");
    // return instance.getTrain12306Service().getcustomeruser(trainorder);
    // }

    // 返回list<map> 格式的 数据 根据存储过程
    public static List<Map<String, Object>> findMapResultByProcedure(String sql) {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        try {
            DataTable dataTable = DBHelper.GetDataTable(sql, dbSourceEnum);
            if (dataTable.GetRow().size() > 0) {
                for (DataRow row : dataTable.GetRow()) {
                    list.add(transmutationMap(row));
                }
            }
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("TrainCreateOrderDBUtil_findMapResultByProcedure_Exception", e,
                    "执行的sql:" + sql);
        }
        return list;
    }

    // 执行
    public static int excuteTrainNoBySql(String procedure) {
        return execSql(procedure);
    }

    // 根据sql 返回 list<Map> 格式的数据
    public static List<Map<String, Object>> findMapResultBySql(String sql) {
        return findMapResultByProcedure(sql);
    }

    public static int excuteAdvertisementBySql(String sql) {
        return execSql(sql);
    }

    /**
     * 根据sql 得到所有的账号的乘客
     * 
     * @describe
     * @author baozz
     * @time 2017年12月19日下午3:06:29
     *
     */
    public static List findAllCustomerpassengerBySql(String sql, int i, int j) {
        return findMapResultByProcedure(sql);
    }

    /**
     * 查找一个票根据票id
     * 
     * @describe
     * @author baozz
     * @time 2017年12月19日下午3:03:17
     */
    public static Trainticket findTrainticket(long ticketid) {
        String sql = "findTrainTicketByTicketId @ticketId=" + ticketid;
        DataRow row = getDataRow(sql, dbSourceEnum);
        // 火车票
        Trainticket trainticket = new Trainticket();
        // 票id
        trainticket.setId(row.GetColumnInt("ID"));
        trainticket.setTrainpid(row.GetColumnInt("C_TRAINPID"));
        trainticket.setInsurenum(row.GetColumnInt("C_INSURENUM"));
        trainticket.setDeparttime(row.GetColumnString("C_DEPARTTIME"));
        trainticket.setDeparture(row.GetColumnString("C_DEPARTURE"));
        trainticket.setArrival(row.GetColumnString("C_ARRIVAL"));
        trainticket.setTrainno(row.GetColumnString("C_TRAINNO"));
        trainticket.setTickettype(row.GetColumnInt("C_TICKETTYPE"));
        trainticket.setSeattype(row.GetColumnString("C_SEATTYPE"));
        trainticket.setSeatno(row.GetColumnString("C_SEATNO"));
        trainticket.setCoach(row.GetColumnString("C_COACH"));
        trainticket.setPrice(row.GetColumnFloat("C_PRICE"));
        trainticket.setStatus(row.GetColumnInt("C_STATUS"));
        trainticket.setArrivaltime(row.GetColumnString("C_ARRIVALTIME"));
        trainticket.setCosttime(row.GetColumnString("C_COSTTIME"));
        trainticket.setPayprice(row.GetColumnFloat("C_PAYPRICE"));
        trainticket.setProcedure(row.GetColumnFloat("C_PROCEDURE"));
        trainticket.setRefundfailreason(row.GetColumnInt("C_REFUNDFAILREASON"));
        trainticket.setIsapplyticket(row.GetColumnInt("C_ISAPPLYTICKET"));
        trainticket.setTicketno(row.GetColumnString("C_TICKETNO"));
        trainticket.setRefundsuccesstime(row.GetColumnString("C_REFUNDSUCCESSTIME"));
        trainticket.setState12306(row.GetColumnInt("C_STATE12306"));
        trainticket.setInsurorigprice(row.GetColumnFloat("C_INSURORIGPRICE"));
        trainticket.setTctrainno(row.GetColumnString("C_TCTRAINNO"));
        trainticket.setTccoach(row.GetColumnString("C_TCCOACH"));
        trainticket.setTcseatno(row.GetColumnString("C_TCSEATNO"));
        trainticket.setTtcseattype(row.GetColumnString("C_TTCSEATTYPE"));
        trainticket.setTtcdeparttime(row.GetColumnString("C_TTCDEPARTTIME"));
        trainticket.setTcnewprice(row.GetColumnFloat("C_TCNEWPRICE"));
        trainticket.setTcProcedure(row.GetColumnFloat("C_TCPROCEDURE"));
        trainticket.setTcPrice(row.GetColumnFloat("C_TCPRICE"));
        trainticket.setTcticketno(row.GetColumnString("C_TCTICKETNO"));
        trainticket.setChangeid(row.GetColumnInt("C_CHANGEID"));
        trainticket.setChangeType(row.GetColumnInt("C_CHANGETYPE"));
        trainticket.setChangeProcedure(row.GetColumnFloat("C_CHANGEPROCEDURE"));
        trainticket.setTsArrival(row.GetColumnString("C_TSARRIVAL"));
        trainticket.setOldOutTicketDetail(row.GetColumnString("C_OLDOUTTICKETDETAIL"));
        trainticket.setNewOutTicketDetail(row.GetColumnString("C_NEWOUTTICKETDETAIL"));
        trainticket.setRefundPriceQuestion(row.GetColumnInt("C_REFUNDPRICEQUESTION"));
        trainticket.setRefundRequestTime(row.GetColumnString("C_REFUNDREQUESTTIME"));
        trainticket.setRefundType(row.GetColumnInt("C_REFUNDTYPE"));
        trainticket.setIsQuestionTicket(row.GetColumnInt("C_ISQUESTIONTICKET"));
        trainticket.setChangeRequestTime(row.GetColumnString("C_CHANGEREQUESTTIME"));
        trainticket.setRefundTimeStamp(row.GetColumnString("C_REFUNDTIMESTAMP"));
        trainticket.setChangeTimeStamp(row.GetColumnString("C_CHANGETIMESTAMP"));
        trainticket.setInsurinprice(row.GetColumnFloat("C_INSURINPRICE"));
        trainticket.setInsurprice(row.GetColumnFloat("C_INSURPRICE"));
        trainticket.setInsureno(row.GetColumnString("C_INSURENO"));
        trainticket.setInsurorigprice(row.GetColumnFloat("C_INSURORIGPRICE"));
        trainticket.setInterfaceticketno(row.GetColumnString("C_INTERFACETICKETNO"));
        // 票关联乘客
        Trainpassenger trainpassenger = new Trainpassenger();
        trainpassenger.setName(row.GetColumnString("C_NAME"));
        trainpassenger.setOrderid(row.GetColumnLong("OID"));
        trainpassenger.setIdtype(row.GetColumnInt("C_IDTYPE"));
        trainpassenger.setIdnumber(row.GetColumnString("C_IDNUMBER"));
        trainpassenger.setBirthday(row.GetColumnString("C_BIRTHDAY"));
        // 乘客关联订单
        Trainorder trainorder = new Trainorder();
        trainorder.setId(row.GetColumnLong("OID"));
        trainorder.setAgentid(row.GetColumnInt("C_AGENTID"));
        trainorder.setOrdernumber(row.GetColumnString("OORDERNUMBER"));
        trainorder.setCreatetime(row.GetColumnTime("OCREATETIME"));
        trainorder.setPaymethod(row.GetColumnInt("OPAYMETHOD"));
        trainorder.setTradeno(row.GetColumnString("OTRADENO"));
        trainorder.setExtnumber(row.GetColumnString("OEXTNUMBER"));
        trainorder.setQunarOrdernumber(row.GetColumnString("C_QUNARORDERNUMBER"));
        trainorder.setIsquestionorder(row.GetColumnInt("OISQUESTIONORDER"));
        trainorder.setState12306(row.GetColumnInt("OSTATE12306"));
        trainorder.setSupplytradeno(row.GetColumnString("OSUPPLYTRADENO"));
        trainorder.setInterfacetype(row.GetColumnInt("OINTERFACETYPE"));
        trainorder.setContacttel(row.GetColumnString("OCONTACTTEL"));
        trainorder.setOrdertype(row.GetColumnInt("TrainOrderType"));
        trainpassenger.setTrainorder(trainorder);
        trainticket.setTrainpassenger(trainpassenger);
        return trainticket;
    }

    /**
     * row shift Map<String,Object>
     * 
     * @describe
     * @author baozz
     * @time 2017年12月19日下午2:59:57
     *
     */
    private static Map<String, Object> transmutationMap(DataRow row) {
        Map<String, Object> map = new HashMap<String, Object>();
        List<DataColumn> column = row.GetColumn();
        for (DataColumn d : column) {
            map.put(d.GetKey(), d.GetValue());
        }
        return map;
    }

    /**
     * 修改火车票表数据
     * 
     * @describe
     * @author baozz
     * @time 2017年12月19日下午3:02:52
     *
     */
    public static void updateTrainticket(Trainticket trainticket) {
        execSql(getUpdateTicket(trainticket));
    }

    /**
     * @describe
     * @author baozz
     * @time 2018年1月3日上午9:51:52
     *
     */
    private static String getUpdateTicket(Trainticket trainticket) {
        StringBuffer sb = new StringBuffer();
        sb.append("UPDATE T_TRAINTICKET SET ");
        addtoSb(sb, "C_COACH", trainticket.getCoach());
        addtoSb(sb, "C_INSURENO", trainticket.getInsureno());
        addtoSb(sb, "C_SEATNO", trainticket.getSeatno());
        addtoSb(sb, "C_SEATTYPE", trainticket.getSeattype());
        addtoSb(sb, "C_PRICE", trainticket.getPrice());
        addtoSb(sb, "C_PROCEDURE", trainticket.getProcedure());
        addtoSb(sb, "C_INSURINPRICE", trainticket.getInsurinprice());
        addtoSb(sb, "C_STATUS", trainticket.getStatus());
        addtoSb(sb, "C_TCCOACH", trainticket.getTccoach());
        addtoSb(sb, "C_TCSEATNO", trainticket.getTcseatno());
        addtoSb(sb, "C_TTCSEATTYPE", trainticket.getTtcseattype());
        addtoSb(sb, "C_TTCDEPARTTIME", trainticket.getTtcdeparttime());
        addtoSb(sb, "C_TCTRAINNO", trainticket.getTctrainno());
        addtoSb(sb, "C_TCNEWPRICE", trainticket.getTcnewprice());
        addtoSb(sb, "C_TCPRICE", trainticket.getTcPrice());
        addtoSb(sb, "C_TCPROCEDURE", trainticket.getTcProcedure());
        addtoSb(sb, "C_REFUNDFAILREASON", trainticket.getRefundfailreason());
        addtoSb(sb, "C_ISAPPLYTICKET", trainticket.getIsapplyticket());
        addtoSb(sb, "C_REFUNDREQUESTTIME", trainticket.getRefundRequestTime());
        addtoSb(sb, "C_CHANGEREQUESTTIME", trainticket.getChangeRequestTime());
        addtoSb(sb, "C_REFUNDTIMESTAMP", trainticket.getRefundTimeStamp());
        addtoSb(sb, "C_CHANGETIMESTAMP", trainticket.getChangeTimeStamp());
        addtoSb(sb, "C_REFUNDSUCCESSTIME", trainticket.getRefundsuccesstime());
        addtoSb(sb, "C_REFUNDTYPE", trainticket.getRefundType());
        addtoSb(sb, "C_ISQUESTIONTICKET", trainticket.getIsQuestionTicket());
        addtoSb(sb, "C_TICKETNO", trainticket.getTicketno());
        addtoSb(sb, "C_TCTICKETNO", trainticket.getTcticketno());
        addtoSb(sb, "C_STATE12306", trainticket.getState12306());
        addtoSb(sb, "C_CHANGEID", trainticket.getChangeid());
        addtoSb(sb, "C_CHANGETYPE", trainticket.getChangeType());
        addtoSb(sb, "C_REALINSURENO", trainticket.getRealinsureno());
        addtoSb(sb, "C_CHANGEPROCEDURE", trainticket.getChangeProcedure());
        addtoSb(sb, "C_TSARRIVAL", trainticket.getTsArrival());
        addtoSb(sb, "C_OLDOUTTICKETDETAIL", trainticket.getOldOutTicketDetail());
        addtoSb(sb, "C_NEWOUTTICKETDETAIL", trainticket.getNewOutTicketDetail());
        addtoSb(sb, "C_REFUNDPRICEQUESTION", trainticket.getRefundPriceQuestion());
        return sb.substring(0, sb.length() - 1) + " where ID = " + trainticket.getId();
    }

    /**
     * 得到订单接口类型
     * 
     * @describe
     * @author baozz
     * @time 2017年12月19日下午3:02:25
     *
     */
    public static int getTrainInterfaceType(long trainorderid) {
        String sql = "SELECT C_INTERFACETYPE FROM T_INTERFACEACCOUNT WHERE C_AGENTID=(SELECT TOP 1 C_AGENTID FROM T_TRAINORDER WHERE ID="
                + trainorderid + ")";
        WriteLog.write("接口用户判断", trainorderid + "--->" + sql);
        List<Map<String, Object>> list = findMapResultBySql(sql);
        WriteLog.write("接口用户判断", trainorderid + "--->" + list.size());
        if (list.size() > 0) {
            Map<String, Object> map = list.get(0);
            WriteLog.write("接口用户判断", trainorderid + "--->" + map.get("C_INTERFACETYPE").toString());
            return Integer.valueOf(map.get("C_INTERFACETYPE").toString()).intValue();
        }
        return 1;
    }

    /**
     * 比较float
     * 
     * @describe
     * @author baozz
     * @time 2017年12月20日下午12:58:42
     *
     */
    private static boolean compareFloat(float tcprocedure) {
        return tcprocedure > 0;
    }

    /**
     * 比较字符串
     * 
     * @describe
     * @author baozz
     * @time 2017年12月20日下午12:59:02
     *
     */
    private static boolean compareString(String autounionpayurlsecond) {
        return !"".equals(autounionpayurlsecond);
    }

    /**
     * 比较long
     * 
     * @describe
     * @author baozz
     * @time 2017年12月20日下午12:59:15
     *
     */
    private static boolean compareLong(long user12306id) {
        return user12306id > 0;
    }

    /**
     * @describe 比较int 类型
     * @author baozz
     * @time 2017年12月25日上午11:26:08
     *
     */
    private static boolean compareInteger(Integer supplypayway) {
        return supplypayway > 0;
    }

}
