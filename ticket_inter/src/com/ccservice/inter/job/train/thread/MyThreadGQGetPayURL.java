package com.ccservice.inter.job.train.thread;

import java.net.URLEncoder;
import java.util.List;
import java.util.Random;

import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.sysconfig.Sysconfig;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderchange;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.RepServerUtil;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.server.Server;

/**
 * 
 * 
 * @time 2014年12月20日 下午12:52:19
 * @author wzc
 * 获取改签支付链接线程
 */
public class MyThreadGQGetPayURL extends Thread {

    private Trainorder order;//订单

    private Trainorderchange changeorder;//

    private Customeruser user;//12306账户

    private String paymethodtype;//支付方式

    private String url;

    private TrainSupplyMethod trainSupplyMethod;

    /**
     * 
     * @param order 关联订单
     * @param user 关联12306账号
     * @param paymethodtype  支付类型
     */
    public MyThreadGQGetPayURL(Trainorder order, Customeruser user, String paymethodtype, String url,
            Trainorderchange changeorder) {
        this.order = order;
        this.user = user;
        this.paymethodtype = paymethodtype;
        this.url = url;
        this.changeorder = changeorder;
        this.trainSupplyMethod = new TrainSupplyMethod();
    }

    @Override
    public void run() {
        boolean flag = false;
        if ("1".equals(paymethodtype)) {//获取银联支付链接
            flag = orderpaymentunionpayurl(user, order, url, changeorder);
        }
        else if ("2".equals(paymethodtype)) {//获取支付宝支付链接
            flag = orderpayment(user, order, url, changeorder);
        }
        else if ("3".equals(paymethodtype)) {//获取银联支付宝链接

        }
        WriteLog.write("12306获取GQ支付链接",
                "结果:" + flag + ",访问地址:" + url + ",支付方式:" + paymethodtype + ",订单号:" + order.getOrdernumber());
        //        if (!flag) {
        //            Trainorder temporder = Server.getInstance().getTrainService().findTrainorder(order.getId());
        //            if ("1".equals(paymethodtype)) {//获取银联支付链接
        //                if (temporder.getAutounionpayurl() == null) {
        //                    order.setIsquestionorder(Trainorder.PAYINGQUESTION);
        //                    Server.getInstance().getTrainService().updateTrainorder(order);
        //                }
        //            }
        //            else if ("2".equals(paymethodtype)) {//获取支付宝支付链接
        //                if (temporder.getChangesupplytradeno() == null) {
        //                    order.setIsquestionorder(Trainorder.PAYINGQUESTION);
        //                    Server.getInstance().getTrainService().updateTrainorder(order);
        //                }
        //            }
        //        }
        //        else {
        //            if (order.getOrderstatus() == 2 && order.getState12306() == 4) {//下单成功，等待支付
        //                if ("1".equals(paymethodtype)) {//获取银联支付链接
        //
        //                }
        //                else if ("2".equals(paymethodtype)) {//获取支付宝支付链接
        //                    new TrainpayMqMSGUtil(MQMethod.ORDERPAY_NAME).sendPayMQmsg(order, 1, 0);
        //                    WriteLog.write("12306获取支付链接_MQ", order.getOrdernumber() + " : " + "获取支付链接并支付");
        //                }
        //            }
        //        }
    }

    /**
     * 获取银联支付链接 
     * @param cookieString 访问cookie
     * @param trainorderid
     * @param loginname
     * @time 2014年12月6日 下午3:52:46
     * @author fiend
     */
    public boolean orderpaymentunionpayurl(Customeruser user, Trainorder trainorder, String url,
            Trainorderchange changeorder) {
        if (user == null) {
            WriteLog.write("12306银联GQ自动支付链接", "未获取到用户");
            return false;
        }
        String cookieString = (user.getCardnunber() == null ? "" : user.getCardnunber());
        boolean flag = false;
        String extnumber = trainorder.getExtnumber();// 12306订单号
        String result = "";
        Random rand = new Random();
        int randnum = rand.nextInt(1000000);
        try {
            int isjointrip = trainorder.getIsjointtrip() == null ? 0 : trainorder.getIsjointtrip();//是否为联程
            if (isjointrip == 0) {
                if (!flag) {
                    String par = "datatypeflag=29&payurlflag=2&cookie=" + cookieString + "&extnumber=" + extnumber
                            + "&trainorderid=" + trainorder.getId();
                    WriteLog.write("12306银联GQ自动支付链接", randnum + ":" + changeorder.getTcnumber() + ":银联访问地址:" + url
                            + "?" + par);
                    for (int i = 0; i < 10; i++) {
                        url = RepServerUtil.getRepServer(user, false).getUrl();
                        try {
                            result = SendPostandGet.submitPost(url, par, "UTF-8").toString();
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }
                        WriteLog.write("12306银联GQ自动支付链接", randnum + ":循环次数:" + i + ":" + changeorder.getTcnumber()
                                + ":银联返回数据:" + result);
                        if ("用户未登录".equals(result)) {
                            //登录方法返回cookie
                            user = login12306(user, url);
                            if (user.getState() == 1) {//登陆成功
                                cookieString = user.getCardnunber();
                                par = "datatypeflag=29&payurlflag=2&cookie=" + cookieString + "&extnumber=" + extnumber
                                        + "&trainorderid=" + trainorder.getId();
                                WriteLog.write("12306银联GQ自动支付链接", randnum + ":" + changeorder.getTcnumber()
                                        + ":银联访问地址:" + url + "?" + par);
                                try {
                                    result = SendPostandGet.submitPost(url, par, "UTF-8").toString();
                                }
                                catch (Exception e) {
                                    e.printStackTrace();
                                }
                                WriteLog.write("12306银联GQ自动支付链接", "2次登录" + changeorder.getTcnumber() + ":银联返回数据:"
                                        + result);
                            }
                            else {
                                WriteLog.write("12306银联GQ自动支付链接", randnum + ":" + changeorder.getTcnumber()
                                        + ":登录失败，name:" + user.getLoginname() + ",pwd:" + user.getLogpassword());
                            }
                        }
                        else if ("无未支付订单".equals(result)) {
                            WriteLog.write("12306银联GQ自动支付链接", randnum + ":" + "无未支付订单" + changeorder.getTcnumber() + "");
                            break;
                        }
                        if (result.indexOf("Pay.action") >= 0) {
                            break;
                        }
                    }
                    if (result.indexOf("Pay.action") >= 0) {
                        float supplyprice = Float.valueOf(result.split("orderAmount=")[1].split("&customerIp")[0]) / 100;
                        changeorder.setSupplyprice(supplyprice);
                        changeorder.setPayaddress(result);
                        //trainorder.setSupplypayway(1);
                        //trainorder.setAutounionpayurl(result);//更新支付链接
                        if (changeorder.getTcprice() != supplyprice) {
                            changeorder.setIsQuestionChange(2);
                        }
                        Server.getInstance().getTrainService().updateTrainorcerchange(changeorder);
                        WriteLog.write("12306银联GQ自动支付链接", randnum + ":" + changeorder.getTcnumber() + ",银联数据更新完毕");
                        flag = true;//无需二次访问
                    }
                }
            }
            else if (isjointrip == 1) {//联程订单暂时不处理
                WriteLog.write("12306银联GQ自动支付链接", randnum + ":" + changeorder.getTcnumber() + "联程订单不处理");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            WriteLog.write("12306银联GQ自动支付链接", randnum + ":" + "访问异常:Job12306GetPayUrl:" + e.getMessage());
        }
        return flag;
    }

    /**
     * 重新登录12306的公用方法
     * 
     * @param customeruser
     * @return
     * @time 2014年12月22日 下午6:37:16
     * @author chendong
     */
    public Customeruser login12306(Customeruser customeruser, String url) {
        MyThreadLogin myThreadLogin = new MyThreadLogin(customeruser, url);
        customeruser = myThreadLogin.verification12306(customeruser);
        return customeruser;
    }

    /**
     * 说明:获取支付宝支付订单页面
     * @param isjointrip 是否为联程
     * @return
     * @time 2014年8月30日 上午11:20:49
     * @author yinshubin
     */
    public boolean orderpayment(Customeruser user, Trainorder trainorder, String url, Trainorderchange changeorder) {
        boolean resultmsg = false;
        if (user == null) {
            WriteLog.write("12306支付宝GQ自动支付链接", "未获取到用户");
            return false;
        }
        String loginname = user.getLoginname();
        String cookieString = user.getCardnunber();
        int isjointrip = trainorder.getIsjointtrip() == null ? 0 : trainorder.getIsjointtrip();//是否为联程
        String extnumber = trainorder.getExtnumber();// 12306订单号 
        int randomnum = new Random().nextInt(10000);
        try {
            if (isjointrip == 0) {
                cookieString = URLEncoder.encode(cookieString, "UTF-8");
                String par = "datatypeflag=9&payurlflag=2&cookie=" + cookieString + "&extnumber=" + extnumber
                        + "&trainorderid=" + trainorder.getId();
                long t1 = System.currentTimeMillis();
                String result = "";
                for (int i = 0; i < 10; i++) {
                    url = RepServerUtil.getRepServer(user, false).getUrl();
                    result = SendPostandGet.submitPost(url, par, "UTF-8").toString();
                    WriteLog.write("12306支付宝GQ自动支付链接", randomnum + ":循环次数：" + i + ":" + changeorder.getTcnumber()
                            + "支付宝返回数据:" + result);
                    if ("用户未登录".equals(result)) {
                        //登录方法返回cookie
                        user = login12306(user, url);
                        if (user.getState() == 1) {//登陆成功
                            cookieString = user.getCardnunber();
                            par = "datatypeflag=9&payurlflag=2&cookie=" + cookieString + "&extnumber=" + extnumber
                                    + "&trainorderid=" + trainorder.getId();
                            WriteLog.write("12306支付宝GQ自动支付链接", randomnum + ":" + changeorder.getTcnumber() + "支付宝访问地址:"
                                    + url + "?" + par);
                            try {
                                result = SendPostandGet.submitPost(url, par, "UTF-8").toString();
                            }
                            catch (Exception e) {
                                e.printStackTrace();
                            }
                            WriteLog.write("12306支付宝GQ自动支付链接", randomnum + ":" + "2次" + changeorder.getTcnumber()
                                    + "支付宝返回数据:" + result);
                        }
                        else {
                            WriteLog.write("12306支付宝GQ自动支付链接", randomnum + ":" + changeorder.getTcnumber()
                                    + ",登录失败，name:" + user.getLoginname() + ",pwd:" + user.getLogpassword());
                        }
                    }
                    if (result.indexOf("gateway.do") >= 0) {
                        break;
                    }
                }
                long t2 = System.currentTimeMillis();
                WriteLog.write("12306支付宝GQ自动支付链接", randomnum + ":获取支付链接用时:" + (t2 - t1));
                if (result.indexOf("gateway.do") >= 0) {
                    float supplyprice = Float.valueOf(result.split("ord_amt=")[1].split("&ord_cur")[0]);//支付金额
                    String supplytradeno = result.split("ord_id_ext=")[1].split("&ord_name")[0];
                    changeorder.setSupplyprice(supplyprice);
                    changeorder.setSupplytradeno(supplytradeno);
                    changeorder.setPayaddress(result);
                    Server.getInstance().getTrainService().updateTrainorcerchange(changeorder);
                    WriteLog.write("12306支付宝GQ自动支付链接", randomnum + ":" + "存储支付信息正确" + changeorder.getTcnumber() + ","
                            + loginname + "," + extnumber);
                    resultmsg = true;
                }
            }
            else if (isjointrip == 2) {//联程暂时不处理
                WriteLog.write("12306支付宝GQ自动支付链接", randomnum + ":" + changeorder.getTcnumber() + "联程订单不处理");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            WriteLog.write("12306支付宝GQ自动支付链接", randomnum + ":" + "存储支付信息错误:" + e);
        }
        return resultmsg;
    }

    /**
     * 根据sysconfig的name获得value
     * 实时 如果是判断的必须调用实时接口
     * @param name
     * @return
     */
    public String getSysconfigString_(String name) {
        String result = "-1";
        List<Sysconfig> sysoconfigs = Server.getInstance().getSystemService()
                .findAllSysconfig("WHERE C_NAME='" + name + "'", "", -1, 0);
        if (sysoconfigs.size() > 0) {
            result = sysoconfigs.get(0).getValue();
        }
        return result;
    }

    public String getPaymethodtype() {
        return paymethodtype;
    }

    public void setPaymethodtype(String paymethodtype) {
        this.paymethodtype = paymethodtype;
    }

    public Trainorder getOrder() {
        return order;
    }

    public void setOrder(Trainorder order) {
        this.order = order;
    }

    public Customeruser getUser() {
        return user;
    }

    public void setUser(Customeruser user) {
        this.user = user;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
