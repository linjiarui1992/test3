package com.ccservice.inter.job.train.thread;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.TrainInterfaceMethod;
import com.ccservice.inter.job.train.rule.TrainPayTimeRule;
import com.ccservice.inter.job.train.thread.db.TrainCreateOrderDBUtil;
import com.ccservice.inter.server.Server;
import com.ccservice.qunar.util.ExceptionUtil;

/**
 * 假下单逻辑
 * 
 * @parameter
 * @time 2018年3月21日 上午10:08:31
 * @author YangFan
 */
public class TrainCreateOrderFalse extends TrainCreateOrderSupplyMethod {
    protected long trainorderid;

    protected Trainorder trainorder;

    int r1;

    protected String costtime = "";

    protected String departtime = "";

    protected String loginname = "";

    protected float totalPrice = 0f;

    /**
     * 构造
     * @param trainorderid
     */
    public TrainCreateOrderFalse(long trainorderid) {
        this.trainorderid = trainorderid;
    }

    /**
     * 开始创建订单
     * @param from3151CallBack 来源于同程3151回调
     * @param tongcheng3151Account 同程3151预约的账号，通过MQ在cn_interface传递过来的
     * @remark 注意事项：如果from3151CallBack为true，在未走原取账号逻辑前，如果return了，要释放账号[可调freeTongcheng3151Account()方法]！！！！！
     */
    public void createOrderStart(Customeruser customeruser, boolean from3151CallBack) {
        //1.初始化信息
        initOrderInfo();
        //2.拦截
        if (!intercept()) {
            return;
        }
        //3.更新状态、操作记录
        updateStatusAndCreateRc(customeruser);
        //4.拼假参数
        saveTrainOrderInfo();
        //5.回调
        callBack();
    }

    /**
     * 
     * 
     * @time 2018年3月21日 下午3:20:35
     * @author ZhiHong
     */
    public void callBack() {
        String callbackordered = callBackTongChengOrdered(trainorder, "true");
        boolean isordered = true;
        WriteLog.write("JobGenerateOrder_MyThread", r1 + ":订单ID:" + trainorder.getId() + ";12306账号:" + loginname
                + ":refuse:同程:" + isbudanstr + ":结果回调接口返回====>" + callbackordered);
        if (!"success".equalsIgnoreCase(callbackordered)) {
            trainorder.setIsquestionorder(Trainorder.CAIGOUQUESTION);
            tongchengFalseRc(callbackordered, loginname, trainorder, isordered, isbudanstr);
            TrainCreateOrderDBUtil.updateTrainorder(trainorder);
        }
        else {
            tongchengTrueRc(loginname, trainorder, isordered, isbudanstr);
        }
    }

    /**
     * 拦截异常订单
     * 
     * @return
     * @time 2018年3月21日 下午3:10:28
     * @author YangFan
     */
    public boolean intercept() {
        if (this.trainorder == null) {
            WriteLog.write("假下单_拦截", r1 + ":订单ID:" + trainorder.getId() + "-->未找到该订单");
            return false;
        }
        if (this.trainorder.getState12306() == null || Trainorder.WAITORDER != this.trainorder.getState12306()) {
            WriteLog.write("假下单_拦截", r1 + ":订单ID:" + trainorder.getId());
            return false;
        }
        if (this.trainorder.getOrderstatus() > 2) {
            WriteLog.write("假下单_拦截", r1 + ":订单ID:" + trainorder.getId() + "-->订单状态不对");
            createTrainorderrc(this.trainorder.getId(), "订单状态不对", "createOrderStart", this.trainorder.getOrderstatus());
            return false;
        }
        if (!TrainCreateOrderUtil.getNowTime(new Date())) {
            WriteLog.write("假下单_拦截", r1 + ":订单ID:" + trainorder.getId() + "下单时间不对，不予处理");
            return false;
        }
        return true;
    }

    /**
     * 拼假结果，入库
     * 
     * @time 2018年3月21日 上午10:49:28
     * @author ZhiHong
     */
    public void saveTrainOrderInfo() {
        //1.更新票信息
        updatePassengers();
        //2.更新订单信息
        updateTrainOrder();
    }

    /**
     * 更新订单信息     
     * 
     * @time 2018年3月21日 下午3:12:49
     * @author YangFan
     */
    public void updateTrainOrder() {
        String sequence_no = "E" + ((int) ((Math.random() + 1) * 100000000));//12306订单号
        loginname = getRandomString(15);//

        if (isjointrip) {
            if (trainorder.getExtnumber() == null) {
                trainorder.setExtnumber(sequence_no);
            }
            else {
                trainorder.setExtnumber(sequence_no + trainorder.getExtnumber());
            }
            trainorder.setSupplyaccount(loginname);
        }
        else {// 联程票第二段
            if (trainorder.getExtnumber() == null) {
                trainorder.setExtnumber("," + sequence_no);
            }
            else {
                trainorder.setExtnumber(trainorder.getExtnumber() + "," + sequence_no);
            }
        }
        if (TrainInterfaceMethod.HTHY != this.interfacetype) {
            trainorder.setOrderprice(totalPrice);//取订单金额
        }
        if (TrainInterfaceMethod.TONGCHENG == this.interfacetype || TrainInterfaceMethod.MEITUAN == this.interfacetype
                || TrainInterfaceMethod.WITHHOLDING_AFTER == this.interfacetype
                || TrainInterfaceMethod.WITHHOLDING_BEFORE == this.interfacetype
                || TrainInterfaceMethod.TAOBAO == this.interfacetype
                || TrainInterfaceMethod.YILONG1 == this.interfacetype
                || TrainInterfaceMethod.YILONG2 == this.interfacetype) {// 存入订单总价
            try {
                String sql = "UPDATE T_TRAINORDER SET C_EXTORDERCREATETIME='"
                        + new Timestamp(System.currentTimeMillis()) + "' WHERE ID =" + trainorder.getId();
                Server.getInstance().getSystemService().findMapResultBySql(sql, null);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        trainorder.setIsquestionorder(Trainorder.NOQUESTION);
        trainorder.setState12306(Trainorder.ORDEREDWAITPAY);
        String sql = "";
        String arriveDate = conversionTime(costtime, departtime);
        try {
            sql = "sp_TrainOrderInfo_updateArriveDate @ArriveDate = '" + arriveDate + "', @OrderId = '"
                    + trainorder.getQunarOrdernumber() + "'";
            TrainCreateOrderDBUtil.execSql(sql);
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("TrainCreateOrderSupplyMethod_conversionTime_Exception", e, sql);
        }
        try {
            Server.getInstance().getTrainService().updateTrainorder(trainorder);
        }
        catch (Exception e) {
            //数据库同步失败
            e.printStackTrace();
        }
        createTrainorderrc(trainorder.getId(), "下单成功,电子单号:" + sequence_no, "12306", trainorder.getOrderstatus());
    }

    public void updatePassengers() {
        int k = isjointrip ? 0 : 1;
        List<Trainpassenger> trainpassengers = trainorder.getPassengers();
        List<Long> ticketidList = new ArrayList<Long>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String arrivaltime = "";
        //支付超时时间，存一次就够了
        boolean savePayTimeOut = false;
        String coach_no = (int) ((Math.random()) * 13 + 1) + "";//多张票在同一车厢
        for (int j = 0; j < trainpassengers.size(); j++) {
            Trainpassenger trainpassenger = trainpassengers.get(j);// 获取到对象的票的信息
            //12306联程票增加
            List<Trainticket> traintickets = trainpassenger.getTraintickets();
            Trainticket trainticket = traintickets.get(k);
            if (ticketidList.contains(trainticket.getId())) {
                if (traintickets.size() == 2) {
                    trainticket = traintickets.get(1);
                }
            }
            //假占座信息
            int ticket_type_code = trainticket.getTickettype();// 乘客类型(票种)
            boolean is = (int) (Math.random() * 10) >= 5;
            char s = (char) (int) (Math.random() * 26 + 'A');
            int ii = (int) (Math.random() * 10);
            String n1 = ((int) ((Math.random() + 1) * 1000000)) + "";
            String n2 = ((int) ((Math.random() + 1) * 10000000)) + "";
            String nn = n1 + n2 + "";
            String ticket_no = "E" + nn + (is ? s : ii + "");//票号
            String seat_no = (int) ((Math.random()) * 100 + 1) + "";//坐席编号
            String seat_name = (seat_no.length() == 1 ? "00" : "0") + seat_no;//坐席编号name
            String seat_type_name = trainticket.getSeattype();
            Float str_ticket_price_page = Float.valueOf(trainticket.getPrice());
            totalPrice += str_ticket_price_page;
            String hours = (int) ((Math.random()) * 24) + "";
            String minStr = (int) ((Math.random()) * 60) + "";
            departtime = trainticket.getDeparttime().substring(0, 10) + " "
                    + (hours.length() < 2 ? "0" + hours : hours) + ":" + (minStr.length() < 2 ? "0" + minStr : minStr);
            try {//假的历时
                String hourse = (int) ((Math.random() + 1) * 30) + "";//30小时内
                String min = (int) ((Math.random() + 1) * 60) + "";//30小时内
                costtime = (hourse.length() < 2 ? "0" + hourse : hourse) + ":" + (min.length() < 2 ? "0" + min : min);
                arrivaltime = getArrivalTime(departtime, costtime);
            }
            catch (Exception e) {
                ExceptionUtil.writelogByException("updatepassengerfrom12306jsonbypassenger_err", e);
            }
            if (ticket_type_code == 1 || ticket_type_code == 3 || ticket_type_code == 4) {//成人、学生  ，残军

                if (savePayTimeOut) {
                    Date datetime = new Date();
                    datetime.setTime((System.currentTimeMillis() + 30 * 60 * 1000));//超时时间为30分钟后
                    String pay_limit_time = sdf.format(datetime);
                    TrainPayTimeRule.savePayTimeOut(this.trainorder.getQunarOrdernumber(), pay_limit_time);
                    savePayTimeOut = false;
                }
                trainticket.setTicketno(ticket_no);
                trainticket.setTickettype(ticket_type_code);
                trainticket.setCoach(coach_no);
                if ("一等软座".equals(seat_type_name) || "二等软座".equals(seat_type_name) || seat_type_name.contains("动卧")
                        || seat_type_name.contains("动软") || seat_type_name.contains("高级动软")) {
                    trainticket.setSeattype(seat_type_name);
                }
                if (!ElongHotelInterfaceUtil.StringIsNull(seat_name)) {
                    trainticket.setSeatno(seat_name);
                }
                else {
                    if (seat_type_name.contains("卧")) {
                        if (seat_no.endsWith("3")) {
                            trainticket.setSeatno(seat_no.substring(0, seat_no.length() - 1) + "上铺");
                        }
                        else if (seat_no.endsWith("2")) {
                            trainticket.setSeatno(seat_no.substring(0, seat_no.length() - 1) + "中铺");
                        }
                        else {
                            trainticket.setSeatno(seat_no.substring(0, seat_no.length() - 1) + "下铺");
                        }
                    }
                    else {
                        if (seat_no.startsWith("3")) {
                            trainticket.setSeatno("无座");
                        }
                        else {
                            trainticket.setSeatno(seat_no + "号");
                        }
                    }
                }
                trainticket.setPrice(str_ticket_price_page);
                TrainCreateOrderDBUtil.updateTrainticket(trainticket);
                ticketidList.add(trainticket.getId());
            }
            else if (ticket_type_code == 2) {// 儿童
                trainticket.setTicketno(ticket_no);
                trainticket.setTickettype(ticket_type_code);
                trainticket.setCoach(coach_no);
                if ("一等软座".equals(seat_type_name) || "二等软座".equals(seat_type_name) || seat_type_name.contains("动卧")
                        || seat_type_name.contains("动软") || seat_type_name.contains("高级动软")) {
                    trainticket.setSeattype(seat_type_name);
                }
                if (!ElongHotelInterfaceUtil.StringIsNull(seat_name)) {
                    trainticket.setSeatno(seat_name);
                }
                else {
                    if (seat_type_name.contains("卧")) {
                        if (seat_no.endsWith("3")) {
                            trainticket.setSeatno(seat_no.substring(0, seat_no.length() - 1) + "上铺");
                        }
                        else if (seat_no.endsWith("2")) {
                            trainticket.setSeatno(seat_no.substring(0, seat_no.length() - 1) + "中铺");
                        }
                        else {
                            trainticket.setSeatno(seat_no.substring(0, seat_no.length() - 1) + "下铺");
                        }
                    }
                    else {
                        if (seat_no.startsWith("3")) {
                            trainticket.setSeatno("无座");
                        }
                        else {
                            trainticket.setSeatno(seat_no + "号");
                        }
                    }
                }
                trainticket.setPrice(str_ticket_price_page);
                TrainCreateOrderDBUtil.updateTrainticket(trainticket);
                ticketidList.add(trainticket.getId());
            }
        }
        String ticketidsString = "";
        for (int i = 0; i < ticketidList.size(); i++) {
            ticketidsString += ticketidList.get(i);
            if (i != ticketidList.size() - 1) {
                ticketidsString += ",";
            }
        }
        boolean isEnNeedStarttimeEquils = (this.interfacetype != TrainInterfaceMethod.TAOBAO && this.trainorder
                .getOrderstatus() == Trainorder.WAITPAY);//18代表是排队中的订单
        WriteLog.write("历时", this.trainorderid + ":isEnNeedStarttimeEquils:" + isEnNeedStarttimeEquils
                + ":ticketidsString:" + ticketidsString + ":isEnNeedStarttimeEquils:" + isEnNeedStarttimeEquils
                + ":costtime:" + costtime + ":arrivaltime:" + arrivaltime + ":departtime:" + departtime);
        if (isEnNeedStarttimeEquils && isNotNullOrEpt(ticketidsString) && isNotNullOrEpt(costtime)
                && isNotNullOrEpt(arrivaltime) && isNotNullOrEpt(departtime)) {
            String sql = "UPDATE T_TRAINTICKET SET C_ARRIVALTIME='" + arrivaltime + "',C_DEPARTTIME='" + departtime
                    + "',C_COSTTIME='" + costtime + "' WHERE ID IN (" + ticketidsString + ")";
            WriteLog.write("历时", this.trainorderid + ":" + sql);
            TrainCreateOrderDBUtil.execSql(sql);
        }
    }

    /**
     * 更新订单状态，写操作记录
     * 
     * @param customeruser
     * @time 2018年3月21日 上午10:47:58
     * @author YangFan
     */
    public void updateStatusAndCreateRc(Customeruser customeruser) {
        String sql = "UPDATE T_TRAINORDER SET C_STATE12306=2 WHERE ID =" + this.trainorderid;
        TrainCreateOrderDBUtil.excuteGiftBySql(sql);// 获取账号成功以后才把订单状态改为正在下单
        Long l1 = System.currentTimeMillis();
        createTrainorderrc(trainorder.getId(), "开始获取账号", "系统", 1);
        createTrainorderrc(trainorder.getId(),
                "获取账号结束:耗时" + (System.currentTimeMillis() - l1) + ":" + customeruser.getId(), "系统", 1);
        createTrainorderrc(trainorder.getId(), "初始化账号成功", customeruser.getId() + "", Trainticket.WAITPAY);
    }

    /**
     * 初始化数据
     * 
     * @time 2018年3月21日 上午10:48:26
     * @author YangFan
     */
    public void initOrderInfo() {
        //1.查询订单信息
        this.r1 = new Random().nextInt(1000000);
        try {
            this.trainorder = TrainCreateOrderDBUtil.findTrainorder(this.trainorderid);
        }
        catch (Exception e) {
        }
        //2.初始化回调地址
        this.tcTrainCallBack = PropertyUtil.getValue("tcTrainCallBack", "train.properties");
        this.isjointrip = this.trainorder.getIsjointtrip() != 1;
        initBindingPassengers();
    }

    /**
     * 初始化是否下单过程中绑定乘客到DB中
     *
     * @time 2015年12月4日 下午5:28:14
     * @author fiend
     */
    public void initBindingPassengers() {
        try {
            List<Trainpassenger> passengers = this.trainorder.getPassengers();
            for (int i = 0; i < passengers.size(); i++) {
                Trainticket tk = passengers.get(i).getTraintickets().get(0);
                if (tk.getTickettype() == 3) {
                    //包含学生票、儿童的不走下单过程中绑定乘客
                    return;
                }
            }
            TrainCreateOrderDBUtil.execSql(" [sp_TrainOrderBinding_Insert] @OrderId=" + this.trainorderid);
        }
        catch (Exception e) {
            WriteLog.write("Error_initBindingPassengers", "" + this.trainorderid);
            ExceptionUtil.writelogByException("Error_initBindingPassengers", e);
        }
    }

    /**
     * 获取指定位数的随机字符串(包含小写字母、大写字母、数字,0<length)
     * 
     * @param length
     * @return
     * @time 2018年3月21日 下午2:36:29
     * @author YangFan
     */
    public static String getRandomString(int length) {
        //随机字符串的随机字符库
        String KeyString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        StringBuffer sb = new StringBuffer();
        int len = KeyString.length();
        for (int i = 0; i < length; i++) {
            sb.append(KeyString.charAt((int) Math.round(Math.random() * (len - 1))));
        }
        return sb.toString();
    }
}
