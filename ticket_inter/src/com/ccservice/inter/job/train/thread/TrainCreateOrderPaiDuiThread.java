package com.ccservice.inter.job.train.thread;

/**
 */
public class TrainCreateOrderPaiDuiThread extends Thread {
    long trainorderid;

    boolean isLastPaidui;

    public TrainCreateOrderPaiDuiThread(long trainorderid, boolean isLastPaidui) {
        super();
        this.trainorderid = trainorderid;
        this.isLastPaidui = isLastPaidui;
    }

    /* (non-Javadoc)
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() {
        super.run();
        TrainCreateOrderPaiDui trainCreateOrderPaiDui = new TrainCreateOrderPaiDui(this.trainorderid);
        trainCreateOrderPaiDui.startPaiduiResult(isLastPaidui);
    }
}
