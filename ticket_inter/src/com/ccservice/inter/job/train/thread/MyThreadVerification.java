package com.ccservice.inter.job.train.thread;

import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.Random;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.Util.JobTrainUtil;
import com.ccservice.inter.server.Server;

/**
 * 定时访问12306，保持12306账号在线，并将已通过人数同步到数据库
 * 
 * @time 2014年12月27日 上午9:52:42
 * @author chendong
 */
public class MyThreadVerification extends Thread {
    private Customeruser customeruser;

    private String repUrl;

    private int type = 0;

    public MyThreadVerification(Customeruser customeruser, String url) {
        this.customeruser = customeruser;
        this.repUrl = url;
    }

    public MyThreadVerification(Customeruser customeruser, String url, int type) {
        this.customeruser = customeruser;
        this.repUrl = url;
        this.type = type;
    }

    @Override
    public void run() {
        int r1 = new Random().nextInt(1000000);
        Long l1 = System.currentTimeMillis();
        String loginname = customeruser.getLoginname();
        String password = customeruser.getLogpassword();
        String cookie = JobTrainUtil.getCookie(loginname, password, repUrl);
        try {
            String resultString = "-1";
            String ticketnumsql = "";
            String writeString = "";
            String sql_type = "";
            if (cookie.contains("请重新在网上注册新的账户")) {
                //43 [登录提示] 您的用户信息被他人冒用，请重新在网上注册新的账户，为了确保您的购票安全，您还需尽快到就近的办理客运售票业务的铁路车站完成身份核验，谢谢您对12306网站的支持。
                ticketnumsql = "update T_CUSTOMERUSER set C_ISENABLE=43 where id=" + customeruser.getId();
            }
            else if (cookie.contains("JSESSIONID")) {
                cookie = URLEncoder.encode(cookie, "UTF-8");
                rep15Method(cookie);//先把所有未通过的都删除了再获取已通过的人数
                String par = "datatypeflag=3&cookie=" + cookie;
                int count = 0;
                JSONObject json = new JSONObject();

                try {
                    resultString = SendPostandGet.submitPost(this.repUrl, par, "UTF-8").toString();
                    json = JSONObject.parseObject(resultString);
                    count = json.getInteger("passengercount");
                }
                catch (Exception e) {
                    e.printStackTrace();
                    count = -1;
                }

                if (count > 0) {
                    if (this.type == 1 || count < 15) {
                        sql_type = ",C_ISENABLE=1";
                    }
                    ticketnumsql = "update T_CUSTOMERUSER set C_MODIFYTIME='" + new Timestamp(System.currentTimeMillis())
                            + "'" + sql_type + ",C_LOGINNUM=" + count + ",C_DEPTID=5 where ID=" + customeruser.getId();
                    writeString = r1 + ":现有:" + count + ":人:" + customeruser.getLoginname();
                }
                else if (count != -1) {
                    if (this.type == 1) {
                        sql_type = ",C_ISENABLE=1";
                    }
                    ticketnumsql = "update T_CUSTOMERUSER set C_STATE=0" + sql_type + " where ID=" + customeruser.getId();
                    if ("待核验".equals(json.getString("msg"))) {
                        ticketnumsql = "update T_CUSTOMERUSER set C_ISENABLE=17,C_STATE=0 where ID=" + customeruser.getId();
                    }
                    writeString = r1 + ":" + customeruser.getLoginname() + ":已掉线|msg:" + json.getString("msg") + ":regtime:"
                            + customeruser.getCreatetime();
                }
            }
            int count1 = 0;
            if (ticketnumsql != null && ticketnumsql.length() > 0) {
                count1 = Server.getInstance().getSystemService().excuteAdvertisementBySql(ticketnumsql);
            }
            WriteLog.write("MyThreadVerification", writeString + ":现有人数:" + ":ticketnumsql:" + ticketnumsql + ":count1:"
                    + count1 + ":resultString:" + resultString + "========" + this.repUrl);
            System.out.println(writeString + ":" + count1 + ":" + ticketnumsql);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        WriteLog.write("MyThreadVerification",
                r1 + ":" + customeruser.getLoginname() + ":耗时:" + (System.currentTimeMillis() - l1) + ":cookie:" + cookie);
    }

    /**
     * 身份证核验，绑定账号,新身份验证中间方法
     * 
     * @param logname 12306账号  
     * @param logpassword 12306密码
     * @param name 姓名
     * @param id_no 姓名
     * @param email 姓名
     * @return
     * @time 2014年12月19日 下午7:41:51
     * @author chendong
     */
    public String rep15Method(String cookieString) {
        String resultString = "";
        //        repUrl += "?" + paramContent;
        try {
            cookieString = URLEncoder.encode(cookieString, "utf-8");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        String paramContent = "datatypeflag=15&cookie=" + cookieString;
        String repUrl = this.repUrl;
        //        repUrl = "http://localhost:8080/Reptile/traininit";
        resultString = SendPostandGet.submitPost(repUrl, paramContent, "utf-8").toString();
        System.out.println(repUrl);
        System.out.println(paramContent);
        //        resultString = SendPostandGet.submitGet(repUrl, "utf-8");
        return resultString;
    }

    public static void main(String[] s) {
        System.out.println(new Timestamp(System.currentTimeMillis()));
    }
}
