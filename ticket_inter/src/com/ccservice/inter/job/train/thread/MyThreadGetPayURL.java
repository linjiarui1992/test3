package com.ccservice.inter.job.train.thread;

import java.net.URLEncoder;
import java.util.List;
import java.util.Random;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;
import com.ccservice.b2b2c.policy.ben.RepServerBean;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.sysconfig.Sysconfig;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.Account12306Util;
import com.ccservice.inter.job.train.RepServerUtil;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.server.Server;
import com.ccservice.train.mqlistener.MQMethod;
import com.ccservice.train.mqlistener.TrainpayMqMSGUtil;
import com.ccservice.util.httpclient.HttpsClientUtils;

/**
 * 
 * 
 * @time 2014年12月20日 下午12:52:19
 * @author wzc
 * 获取支付链接线程
 */
public class MyThreadGetPayURL extends Thread {

    private Trainorder order;//订单

    private String paymethodtype;//支付方式

    private TrainSupplyMethod trainSupplyMetHod;

    /**
     * 
     * @param order 关联订单
     * @param user 关联12306账号
     * @param paymethodtype  支付类型
     */
    public MyThreadGetPayURL(Trainorder order, String paymethodtype) {
        this.order = order;
        this.paymethodtype = paymethodtype;
        this.trainSupplyMetHod = new TrainSupplyMethod();
    }

    @Override
    public void run() {
        boolean flag = false;
        if ("1".equals(paymethodtype)) {//获取银联支付链接
            flag = orderpaymentunionpayurl(order);
        }
        else if ("2".equals(paymethodtype)) {//获取支付宝支付链接
            flag = orderpayment(order);
        }
        else if ("3".equals(paymethodtype)) {//获取银联支付宝链接

        }
        if (!flag) {
            Trainorder temporder = Server.getInstance().getTrainService().findTrainorder(order.getId());
            if ("1".equals(paymethodtype)) {//获取银联支付链接
                if (temporder.getAutounionpayurl() == null) {
                    order.setIsquestionorder(Trainorder.PAYINGQUESTION);
                    Server.getInstance().getTrainService().updateTrainorder(order);
                }
            }
            else if ("2".equals(paymethodtype)) {//获取支付宝支付链接
                if (temporder.getChangesupplytradeno() == null) {
                    order.setIsquestionorder(Trainorder.PAYINGQUESTION);
                    Server.getInstance().getTrainService().updateTrainorder(order);
                }
            }
        }
        else {
            if (order.getOrderstatus() == 2 && order.getState12306() == 4) {//下单成功，等待支付
                new TrainpayMqMSGUtil(MQMethod.ORDERPAY_NAME).sendPayMQmsg(order, Integer.parseInt(paymethodtype), 0);
                WriteLog.write("12306获取支付链接_MQ", order.getOrdernumber() + " : " + "获取支付链接并支付");
            }
        }
    }

    public static void main(String[] args) throws Exception {
        String url = "http://120.26.71.93:8080/Reptile/traininit";
        String par = "datatypeflag=9&cookie="
                + URLEncoder
                        .encode("JSESSIONID=0A02F04A9825DDB3FE16EC2D2EA5DAA38F92BE629B; BIGipServerotn=1257243146.38945.0000; current_captcha_type=Z",
                                "UTF-8") + "&extnumber=E651117627&trainorderid=1";
        String result = SendPostandGet.submitPost(url, par, "UTF-8").toString();
        System.out.println(result);
    }

    /**
     * 获取银联支付链接 
     * @param cookieString 访问cookie
     * @param trainorderid
     * @param loginname
     * @time 2014年12月6日 下午3:52:46
     * @author fiend
     */
    public boolean orderpaymentunionpayurl(Trainorder trainorder) {
        String result = "";
        boolean flag = false;
        Random rand = new Random();
        int randnum = rand.nextInt(1000000);
        String extnumber = trainorder.getExtnumber();
        //是否为联程
        int isjointrip = trainorder.getIsjointtrip() == null ? 0 : trainorder.getIsjointtrip();
        //账号系统取账号
        Customeruser user = trainSupplyMetHod.getCustomerUserBy12306Account(trainorder, false);
        try {
            if (isjointrip == 0) {
                if (!flag) {
                    for (int i = 0; i < 10; i++) {
                        try {
                            //REP
                            RepServerBean rep = RepServerUtil.getRepServer(user, false);
                            //参数
                            String par = "datatypeflag=29&cookie=" + user.getCardnunber() + "&extnumber=" + extnumber
                                    + "&trainorderid=" + trainorder.getId()
                                    + trainSupplyMetHod.JoinCommonAccountInfo(user, rep);
                            //地址
                            String url = rep.getUrl();
                            //日志
                            WriteLog.write("12306银联自动支付链接", randnum + ":循环次数:" + i + ":" + trainorder.getOrdernumber()
                                    + ":银联访问地址:" + url + "?" + par);
                            //请求
                            result = SendPostandGet.submitPost(url, par, "UTF-8").toString();
                        }
                        catch (Exception e) {

                        }
                        result = ElongHotelInterfaceUtil.StringIsNull(result) ? "" : result;
                        //日志
                        WriteLog.write("12306银联自动支付链接", randnum + ":循环次数:" + i + ":" + trainorder.getOrdernumber()
                                + ":银联返回数据:" + result);
                        //未登录
                        if (Account12306Util.accountNoLogin(result, user)) {
                            //以未登录释放
                            trainSupplyMetHod.FreeNoLogin(user);
                            //账号系统重新获取
                            user = trainSupplyMetHod.getCustomerUserBy12306Account(trainorder, true);
                        }
                        else if ("无未支付订单".equals(result)) {
                            WriteLog.write("12306银联自动支付链接", randnum + ":" + "无未支付订单" + trainorder.getOrdernumber() + "");
                            break;
                        }
                        if (result.indexOf("Pay.action") >= 0) {
                            break;
                        }
                    }
                    if (result.indexOf("Pay.action") >= 0) {
                        if (!(trainorder.getSupplyprice() != null && trainorder.getSupplyprice() > 0)) {
                            float supplyprice = Float.valueOf(result.split("orderAmount=")[1].split("&customerIp")[0]) / 100;
                            trainorder.setSupplyprice(supplyprice);
                        }
                        trainorder.setSupplypayway(1);
                        trainorder.setAutounionpayurl(result);//更新支付链接
                        Server.getInstance().getTrainService().updateTrainorder(trainorder);
                        WriteLog.write("12306银联自动支付链接", randnum + ":" + trainorder.getOrdernumber() + ",银联数据更新完毕");
                        flag = true;//无需二次访问
                    }
                }
            }
            else if (isjointrip == 1) {//联程订单暂时不处理
                WriteLog.write("12306银联自动支付链接", randnum + ":" + trainorder.getOrdernumber() + "联程订单不处理");
            }
        }
        catch (Exception e) {
            WriteLog.write("12306银联自动支付链接", randnum + ":" + "访问异常:Job12306GetPayUrl:" + e.getMessage());
        }
        if (user.isFromAccountSystem()) {
            trainSupplyMetHod.FreeNoCare(user);//释放
        }
        return flag;
    }

    /**
     * 说明:获取支付宝支付订单页面
     * @param isjointrip 是否为联程
     * @return
     * @time 2014年8月30日 上午11:20:49
     */
    public boolean orderpayment(Trainorder trainorder) {
        boolean resultmsg = false;
        int randomnum = new Random().nextInt(10000);
        Customeruser user = trainSupplyMetHod.getCustomerUserBy12306Account(trainorder, false);
        WriteLog.write("12306支付宝自动支付链接", randomnum + ":" + user.getLoginname() + ":cookie:" + user.getCardnunber()
                + ":" + trainorder.getOrdernumber() + ",");
        int isjointrip = trainorder.getIsjointtrip() == null ? 0 : trainorder.getIsjointtrip();//是否为联程
        String extnumber = trainorder.getExtnumber();// 12306订单号 
        try {
            if (isjointrip == 0) {
                long t1 = System.currentTimeMillis();
                String result = "";
                for (int i = 0; i < 5; i++) {
                    //Cookie
                    String cookieString = user.getCardnunber();
                    cookieString = URLEncoder.encode(cookieString, "UTF-8");
                    //REP
                    RepServerBean rep = RepServerUtil.getRepServer(user, false);
                    //参数
                    String par = "datatypeflag=9&cookie=" + cookieString + "&extnumber=" + extnumber + "&trainorderid="
                            + trainorder.getId() + trainSupplyMetHod.JoinCommonAccountInfo(user, rep);
                    //地址
                    String url = rep.getUrl();
                    WriteLog.write("12306支付宝自动支付链接", randomnum + ":超时机制循环次数：" + i + ":" + trainorder.getOrdernumber()
                            + "支付宝访问地址:" + url);
                    result = HttpsClientUtils.gethttpclientdata(url + "?" + par, 30000l);
                    result = ElongHotelInterfaceUtil.StringIsNull(result) ? "" : result;
                    WriteLog.write("12306支付宝自动支付链接", randomnum + ":循环次数：" + i + ":" + trainorder.getOrdernumber()
                            + "支付宝返回数据:" + result);
                    //未登录
                    if (Account12306Util.accountNoLogin(result, user)) {
                        //以未登录释放
                        trainSupplyMetHod.FreeNoLogin(user);
                        //账号系统重新获取
                        user = trainSupplyMetHod.getCustomerUserBy12306Account(trainorder, true);
                    }
                    else if (result.indexOf("gateway.do") >= 0) {
                        break;
                    }
                }
                long t2 = System.currentTimeMillis();
                WriteLog.write("12306支付宝自动支付链接", randomnum + ":获取支付链接用时:" + (t2 - t1));
                if (result.indexOf("gateway.do") >= 0) {
                    float supplyprice = Float.valueOf(result.split("ord_amt=")[1].split("&ord_cur")[0]);
                    trainorder.setSupplyprice(supplyprice);
                    trainorder.setSupplypayway(7);
                    trainorder.setSupplytradeno(result.split("ord_id_ext=")[1].split("&ord_name")[0]);
                    trainorder.setChangesupplytradeno(result);
                    Server.getInstance().getTrainService().updateTrainorder(trainorder);
                    WriteLog.write("12306支付宝自动支付链接", randomnum + ":" + "存储支付信息正确" + trainorder.getOrdernumber() + ","
                            + trainorder.getSupplyaccount() + "," + extnumber);
                    resultmsg = true;
                }
            }
            else if (isjointrip == 2) {//联程暂时不处理
                WriteLog.write("12306支付宝自动支付链接", randomnum + ":" + trainorder.getOrdernumber() + "联程订单不处理");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            WriteLog.write("12306支付宝自动支付链接", randomnum + ":" + "存储支付信息错误:" + e);
        }
        if (user.isFromAccountSystem()) {
            trainSupplyMetHod.FreeNoCare(user);//释放
        }
        return resultmsg;
    }

    /**
     * 根据sysconfig的name获得value
     * 实时 如果是判断的必须调用实时接口
     * @param name
     * @return
     */
    @SuppressWarnings("unchecked")
    public String getSysconfigString_(String name) {
        String result = "-1";
        List<Sysconfig> sysoconfigs = Server.getInstance().getSystemService()
                .findAllSysconfig("WHERE C_NAME='" + name + "'", "", -1, 0);
        if (sysoconfigs.size() > 0) {
            result = sysoconfigs.get(0).getValue();
        }
        return result;
    }

    public String getPaymethodtype() {
        return paymethodtype;
    }

    public void setPaymethodtype(String paymethodtype) {
        this.paymethodtype = paymethodtype;
    }

    public Trainorder getOrder() {
        return order;
    }

    public void setOrder(Trainorder order) {
        this.order = order;
    }

}
