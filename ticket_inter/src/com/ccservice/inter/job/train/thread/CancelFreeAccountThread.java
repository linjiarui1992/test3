package com.ccservice.inter.job.train.thread;

import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.atom.service12306.AccountSystem;

/**
 * @author WH
 * @remark 账号系统释放账号
 */

public class CancelFreeAccountThread extends Thread {

    private String loginname;

    private TrainSupplyMethod method;

    public CancelFreeAccountThread(String loginname) {
        this.loginname = loginname;
        method = new TrainSupplyMethod();
    }

    public void run() {
        Customeruser user = new Customeruser();
        user.setLoginname(loginname);
        user.setFromAccountSystem(true);
        method.freeCustomeruser(user, AccountSystem.FreeNoCare, AccountSystem.OneFree, AccountSystem.OneCancel,
                AccountSystem.NullDepartTime);
    }
}