package com.ccservice.inter.job.train.thread;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;




import javax.jms.JMSException;

import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

import com.alibaba.fastjson.JSONObject;
import com.ccervice.util.ActiveMQUtil;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.b2b2c.policy.TimeUtil;
import com.ccservice.b2b2c.util.ExceptionUtil;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.inter.server.ServerTrainJobUtil;
import com.ccservice.rabbitmq.util.RabbitMQUtil;

/**
 * 订单走排队机制
 */
public class TrainCreateOrderPaiDuiJob implements Job {
    public static String QueuesName = "QueueMQ_trainorder_waitorder_orderid_PaiDui";

    public static void main1(String[] args) {}

    /* (non-Javadoc)
     * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
     */
    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        System.out.println(TimeUtil.gettodaydate(4) + ":开始处理排队订单");
        execute();
    }

    /**
     * 
     * @time 2015年12月11日 下午5:12:51
     * @author chendong
     */
    private void execute() {
        //        String timeString = "-3,-2|-6,-5|-9,-8|-12,-11|-15,-14|-18,-17|-21,-20";
        String todayOrder = "100";
        String timeString = "-3,-2|-6,-5|-9,-8";
        timeString = PropertyUtil.getValue("TrainCreateOrderPaiDuiJobtimeString", "train.TrainCreateOrder.properties");
        todayOrder = PropertyUtil.getValue("TrainCreateOrderPaiDuiJobtimeStringTodayOrderLineupTimeSetting",
                "train.TrainCreateOrder.properties");
        String[] timeStrings = timeString.split("[|]");
        for (int j = 0; j < timeStrings.length; j++) {
            boolean isLast = false;
            if (j == timeStrings.length - 1) {
                isLast = true;
            }
            String stime = timeStrings[j].split(",")[0];
            String etime = timeStrings[j].split(",")[1];
            String procedureSqlString = " [sp_TrainOrderPaiduiData_selectByJob] @stime=" + stime + ",@etime=" + etime
                    + " ";
            List customerusers_map = Server.getInstance().getSystemService()
                    .findMapResultByProcedure(procedureSqlString);
            WriteLog.write("TrainCreateOrderPaiDuiJob", "排队订单:" + stime + ">" + etime + ":" + customerusers_map.size()
                    + ":isLast:" + isLast);
            WriteLog.write("TrainCreateOrderPaiDuiJob",
                    "排队订单:" + stime + ">" + etime + ":" + JSONObject.toJSONString(customerusers_map));
            System.out.println("排队订单:" + stime + ">" + etime + ":" + customerusers_map.size() + ":isLast:" + isLast);
            for (int i = 0; i < customerusers_map.size(); i++) {
                Map map = (Map) customerusers_map.get(i);
                toCheckPaidui(map, isLast, j, todayOrder);
            }
        }

    }

    /**
     * 
     * @param map
     * @time 2015年12月11日 下午5:38:29
     * @author chendong
     * @param boolean isLastPaidui 3:前8-9分钟的订单;2:前5-6分钟的订单;1前2-3分钟的订单
     */
    private void toCheckPaidui(Map map, boolean isLastPaidui, int j, String todayOrder) {
        long r1 = System.currentTimeMillis();
        //      {"Loginname":"hehong8185w5qiz","CheckStatus":0,"CheckCount":0,"Id":7,"LastCheckTime":1449825731447,"StartTime":1449825731447,"CookieString":"JSESSIONID=0A01D96160C2B766B417F08A8AB3E3EAE747018FC7; BIGipServerotn=1641611530.24610.0000; current_captcha_type=Z","TrainOrderId":48595867}
        try {
            System.out.println(JSONObject.toJSONString(map));
            String TrainOrderId = map.get("TrainOrderId").toString();
            long trainorderid = Long.parseLong(TrainOrderId);
            boolean isSendMq = true;
            if (getDepartTimeByIdMthod(trainorderid)) {
                if (Integer.parseInt(todayOrder) == j) {
                    isLastPaidui = true;//如果是当天是最后一次排队
                }
                if (j > Integer.parseInt(todayOrder)) {
                    isSendMq = false;//如果当前下标超过了当天排队的下标,就不扔mq了
                }
                WriteLog.write("TrainCreateOrderPaiDuiJob", trainorderid + ":循环下标j值:" + j + ":todayOrder:" + todayOrder
                        + ":isSendMq:" + isSendMq);
            }
            if (isSendMq) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("trainorderid", trainorderid);
                jsonObject.put("isLastPaidui", isLastPaidui);
                String MQ_URL = PropertyUtil.getValue("activeMQ_url", "train.properties");
                WriteLog.write("TrainCreateOrderPaiDuiJob", TrainOrderId + ":排队订单扔MQ:QueuesName:"
                        + TrainCreateOrderPaiDuiJob.QueuesName + ":" + jsonObject.toJSONString() + ":" + MQ_URL);
//                ActiveMQUtil.sendMessage(MQ_URL, TrainCreateOrderPaiDuiJob.QueuesName, jsonObject.toJSONString());
                RabbitMQUtil.sendOnemessageV2(jsonObject.toJSONString(), "QueueMQ_trainorder_waitorder_orderid_PaiDui");
//                    ActiveMQUtil.sendMessage(MQ_URL, TrainCreateOrderPaiDuiJob.QueuesName, jsonObject.toJSONString());
                //            new TrainCreateOrderPaiDuiThread(trainorderid, isLastPaidui).start();
            }
        }
        catch (Exception e) {
        }
    }

    /**
     * 根据订单id查出出发时间,判断出发时间是否为当天
     * 
     * @param trainorderid
     * @return 是:true , 否:false
     * @time 2016年1月28日 下午2:45:48
     * @author wang.cheng.liang
     */
    private boolean getDepartTimeByIdMthod(long trainorderid) {
        boolean isToday = false;
        try {
            Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(trainorderid);
            List<Trainpassenger> trainpassengers = trainorder.getPassengers();
            WriteLog.write("TrainCreateOrderPaiDuiJob", trainorderid + ":是否有该订单数据:" + trainpassengers.size());
            if (trainpassengers.size() > 0 && trainpassengers.get(0).getTraintickets().size() > 0) {
                List<Trainticket> traintickets = trainpassengers.get(0).getTraintickets();
                String depart = traintickets.get(0).getDeparttime();
                if (!ElongHotelInterfaceUtil.StringIsNull(depart)) {
                    depart = depart.substring(0, 10);
                    String sdfToday = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                    WriteLog.write("TrainCreateOrderPaiDuiJob", trainorderid + ":是否当天:" + depart + "==" + sdfToday);
                    if (sdfToday.equals(depart)) {
                        isToday = true;
                    }
                }
            }
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("TrainCreateOrderPaiDuiJob_Err", e);
        }
        return isToday;

    }

    // 设置调度任务及触发器，任务名及触发器名可用来停止任务或修改任务
    public static void startScheduler(String expr) throws Exception {
        String name = TrainCreateOrderPaiDuiJob.class.getSimpleName() + System.currentTimeMillis();
        String group = TrainCreateOrderPaiDuiJob.class.getSimpleName() + "_Group";
        JobDetail jobDetail = new JobDetail(name, group, TrainCreateOrderPaiDuiJob.class);// 任务名，任务组名，任务执行类
        CronTrigger trigger = new CronTrigger(name, group, expr);// 触发器名，触发器组名
        if (ServerTrainJobUtil.scheduler_TrainCreateOrderPaiDuiJob == null
                || ServerTrainJobUtil.scheduler_TrainCreateOrderPaiDuiJob.isShutdown()) {
            Scheduler scheduler_TrainCreateOrderPaiDuiJob = StdSchedulerFactory.getDefaultScheduler();
            ServerTrainJobUtil.scheduler_TrainCreateOrderPaiDuiJob = scheduler_TrainCreateOrderPaiDuiJob;
            ServerTrainJobUtil.scheduler_TrainCreateOrderPaiDuiJob.scheduleJob(jobDetail, trigger);
            ServerTrainJobUtil.scheduler_TrainCreateOrderPaiDuiJob.start();
        }
    }

    /**
     * 
     * @time 2015年10月19日 下午7:44:25
     * @author chendong
     */
    public static void pauseJob() {
        try {
            if (ServerTrainJobUtil.scheduler_TrainCreateOrderPaiDuiJob != null) {
                ServerTrainJobUtil.scheduler_TrainCreateOrderPaiDuiJob.shutdown();
            }
        }
        catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

}
