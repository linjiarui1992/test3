package com.ccservice.inter.job.train.thread;

import java.net.URLEncoder;
import java.util.Random;

import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.b2b2c.policy.SendPostandGet;
import com.ccservice.b2b2c.policy.ben.RepServerBean;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.Account12306Util;
import com.ccservice.inter.job.train.RepServerUtil;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.server.Server;
import com.ccservice.qunar.util.ExceptionUtil;

/**
 * 下单成功后获取支付链接
 * 
 * @time 2016年5月18日 下午7:41:21
 * @author fiend
 */
public class MyThreadGetPayUrlAfterCreated extends Thread {
    Trainorder trainorder;

    Customeruser customeruser;

    TrainSupplyMethod trainSupplyMethod = new TrainSupplyMethod();

    public MyThreadGetPayUrlAfterCreated(Trainorder trainorder, Customeruser customeruser) {
        this.trainorder = trainorder;
        this.customeruser = customeruser;
    }

    public void run() {
        orderpayment(customeruser);
    }

    /**
     * 说明:获取支付宝支付订单页面
     * @param isjointrip 是否为联程
     * @return
     * @time 2014年8月30日 上午11:20:49
     * @author yinshubin
     */
    public void orderpayment(Customeruser user) {
        int randomnum = new Random().nextInt(10000);
        WriteLog.write(
                "12306支付宝自动支付链接_下单成功后",
                randomnum + ":" + user.getLoginname() + ":cookie:" + user.getCardnunber() + ":"
                        + trainorder.getOrdernumber() + ",");
        if (user == null || user.getCardnunber() == null || user.getCardnunber().equals("")) {
            return;
        }
        String loginname = user.getLoginname();
        int isjointrip = trainorder.getIsjointtrip() == null ? 0 : trainorder.getIsjointtrip();//是否为联程
        String extnumber = trainorder.getExtnumber();// 12306订单号 
        float priceAll = 0;
        for (Trainpassenger trainpassenger : this.trainorder.getPassengers()) {
            for (Trainticket trainticket : trainpassenger.getTraintickets()) {
                priceAll += trainticket.getPrice();
            }
        }
        try {
            if (isjointrip == 0) {
                long t1 = System.currentTimeMillis();
                String result = "";
                for (int i = 0; i < 5; i++) {
                    //Cookie
                    String cookieString = user.getCardnunber();
                    //为空
                    boolean cookieIsNull = ElongHotelInterfaceUtil.StringIsNull(cookieString);
                    //非空
                    if (!cookieIsNull) {
                        cookieString = URLEncoder.encode(cookieString, "UTF-8");
                        //REP
                        RepServerBean rep = RepServerUtil.getRepServer(user, false);
                        //地址
                        String url = rep.getUrl();
                        //参数
                        String par = "datatypeflag=9&cookie=" + cookieString + "&extnumber=" + extnumber
                                + "&trainorderid=" + trainorder.getId() + "&price=" + priceAll
                                + trainSupplyMethod.JoinCommonAccountInfo(user, rep);
                        WriteLog.write("12306支付宝自动支付链接_下单成功后", randomnum + ":超时机制循环次数：" + i + ":par:" + par + ""
                                + trainorder.getOrdernumber() + ":支付宝访问地址:" + url);
                        result = SendPostandGet.submitPostTimeOutFiend(url, par, "UTF-8", 60000).toString();
                        result = ElongHotelInterfaceUtil.StringIsNull(result) ? "" : result;
                        //                    result = HttpsClientUtils.gethttpclientdata(url + "?" + par, 30000l);
                        WriteLog.write("12306支付宝自动支付链接_下单成功后",
                                randomnum + ":循环次数：" + i + ":" + trainorder.getOrdernumber() + "支付宝返回数据:" + result);
                        //切换REP
                        if (result.contains("用户未登录") && RepServerUtil.changeRepServer(user)) {
                            //切换REP
                            rep = RepServerUtil.getTaoBaoTuoGuanRepServer(user, false);
                            //类型正确
                            if (rep.getType() == 1) {
                                //REP地址
                                url = rep.getUrl();
                                //重拼参数
                                par = "datatypeflag=9&cookie=" + cookieString + "&extnumber=" + extnumber
                                        + "&trainorderid=" + trainorder.getId() + "&price=" + priceAll
                                        + trainSupplyMethod.JoinCommonAccountInfo(user, rep);
                                //记录日志
                                WriteLog.write("12306支付宝自动支付链接_下单成功后", randomnum + ":超时机制循环次数：" + i + ":par:" + par
                                        + "" + trainorder.getOrdernumber() + ":切换支付宝访问地址:" + url);
                                //重新请求
                                result = SendPostandGet.submitPostTimeOutFiend(url, par, "UTF-8", 60000).toString();
                                result = ElongHotelInterfaceUtil.StringIsNull(result) ? "" : result;
                                //记录日志
                                WriteLog.write("12306支付宝自动支付链接_下单成功后",
                                        randomnum + ":循环次数：" + i + ":" + trainorder.getOrdernumber() + "支付宝返回数据:"
                                                + result);
                            }
                        }
                    }
                    if (cookieIsNull || Account12306Util.accountNoLogin(result, user)) {
                        //释放
                        if (cookieIsNull) {
                            trainSupplyMethod.FreeNoCare(user);
                        }
                        else {
                            trainSupplyMethod.FreeNoLogin(user);
                        }
                        //等待
                        Thread.sleep(1000);
                        //重拿
                        user = trainSupplyMethod.getCustomerUserBy12306Account(trainorder, true);
                    }
                    else if (result.indexOf("gateway.do") >= 0) {
                        break;
                    }
                    else if (result.indexOf("价格不正确") >= 0) {
                        break;
                    }
                }
                long t2 = System.currentTimeMillis();
                WriteLog.write("12306支付宝自动支付链接_下单成功后", randomnum + ":获取支付链接用时:" + (t2 - t1) + "--->result:" + result);
                if (result.indexOf("gateway.do") >= 0) {
                    float supplyprice = Float.valueOf(result.split("ord_amt=")[1].split("&ord_cur")[0]);
                    trainorder.setSupplyprice(supplyprice);
                    trainorder.setSupplypayway(7);
                    trainorder.setSupplytradeno(result.split("ord_id_ext=")[1].split("&ord_name")[0]);
                    trainorder.setChangesupplytradeno(result);
                    //更新订单
                    String sql = "UPDATE T_TRAINORDER SET  C_SUPPLYPRICE="
                            + Float.valueOf(result.split("ord_amt=")[1].split("&ord_cur")[0])
                            + ",C_SUPPLYPAYWAY=7,C_SUPPLYTRADENO='"
                            + result.split("ord_id_ext=")[1].split("&ord_name")[0] + "',C_CHANGESUPPLYTRADENO='"
                            + result + "'  WHERE ID =" + trainorder.getId();
                    WriteLog.write("12306支付宝自动支付链接_下单成功后", randomnum + ":" + trainorder.getOrdernumber() + "," + sql);
                    Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                    WriteLog.write("12306支付宝自动支付链接_下单成功后", randomnum + ":" + "存储支付信息正确" + trainorder.getOrdernumber()
                            + "," + loginname + "," + extnumber);
                }
            }
            else if (isjointrip == 2) {//联程暂时不处理
                WriteLog.write("12306支付宝自动支付链接_下单成功后", randomnum + ":" + trainorder.getOrdernumber() + "联程订单不处理");
            }
        }
        catch (Exception e) {
            WriteLog.write("12306支付宝自动支付链接_下单成功后_Exception", randomnum + "");
            ExceptionUtil.writelogByException("12306支付宝自动支付链接_下单成功后_Exception", e);
        }
    }
}
