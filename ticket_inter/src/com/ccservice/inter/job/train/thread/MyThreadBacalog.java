package com.ccservice.inter.job.train.thread;

import com.ccservice.inter.job.train.JobBacklogOfOrders;

public class MyThreadBacalog extends Thread {

    private long trainorderid;

    public MyThreadBacalog(long trainorderid) {
        this.trainorderid = trainorderid;
    }

    public long getTrainorderid() {
        return trainorderid;
    }

    @Override
    public void run() {
        new JobBacklogOfOrders().backlogOfOrders(this.trainorderid);
    }

    public void setTrainorderid(long trainorderid) {
        this.trainorderid = trainorderid;
    }

}
