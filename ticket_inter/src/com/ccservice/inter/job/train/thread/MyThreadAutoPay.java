package com.ccservice.inter.job.train.thread;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.List;

import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.sysconfig.Sysconfig;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.b2b2c.ben.Trainform;
import com.ccservice.inter.job.EncryptionUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.JobAutoPayUnion;
import com.ccservice.inter.job.train.JobGenerateOrder;
import com.ccservice.inter.job.train.JobQunarOrder;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.server.Server;

/**
 * 自动支付线程 
 * @time 2014年11月27日 上午11:02:37
 * @author fiend
 */
public class MyThreadAutoPay extends Thread {

    //    private List<Trainorder> orderlist;

    private int uid;

    private boolean isqunar;

    private Trainorder trainorder;

    public MyThreadAutoPay(int uid, boolean isqunar, Trainorder trainorder) {//List<Trainorder> orderlist,
        //        this.orderlist = orderlist;
        this.uid = uid;
        this.isqunar = isqunar;
        this.trainorder = trainorder;
    }

    public void run() {
        //        JobAutoPayAlipay japa = new JobAutoPayAlipay();
        //        japa.topOrder(uid, isqunar);
        //        topOrder(uid, isqunar, trainorder);
        waitPayInformation(uid, trainorder);
        //        japa.allWaitPay(orderlist, uid, isqunar);
    }

    /**
     * 获取订单中第一个匹配可支付订单进行支付
     * @param i
     * @param isqunar
     * @return
     * @time 2014年12月3日 上午9:32:38
     * @author fiend
     */
    public void topOrder(int i, boolean isqunar) {
        Trainform trainform = new Trainform();
        //TODO
        //        trainform.setOrderstatus(Trainorder.ISSUEDWAITPAY);
        trainform.setOrderstatus(Trainorder.ISSUED);
        trainform.setState12306(Trainorder.ORDEREDWAITPAY);
        while (true) {
            if (!iscanpay() && isqunar) {
                new TrainSupplyMethod().fresh(i);
            }
            List<Trainorder> torderList = Server.getInstance().getTrainService().findAllTrainorder(trainform, null);
            Integer before = isqunar ? Integer.valueOf(new TrainSupplyMethod().getSysconfigString("alipaynumber"))
                    : Integer.valueOf(new TrainSupplyMethod().getSysconfigString("alipaynumberyee"));
            int payno = isqunar ? i : i - 9000;
            if (before != null && before > 0) {
                if (torderList.size() > 0) {
                    for (int j = 0; j < torderList.size(); j++) {
                        Trainorder trainorder = torderList.get(j);
                        if (isqunar
                                && trainorder.getAgentid() == Integer.valueOf(new TrainSupplyMethod()
                                        .getSysconfigString("qunar_agentid"))
                                && (int) (trainorder.getId()) % before == payno) {
                            //                            waitPayInformation(trainorder.getId(), i);
                            if (!waitPayInformation(trainorder.getId(), i)) {
                                trainorder.setState12306(Trainorder.ORDEREDPAYFALSE);
                                trainorder.setIsquestionorder(Trainorder.PAYINGQUESTION);
                                Server.getInstance().getTrainService().updateTrainorder(trainorder);
                            }
                            break;
                        }
                        else if (!isqunar) {
                            if (trainorder.getAgentid() != Integer.valueOf(new TrainSupplyMethod()
                                    .getSysconfigString("qunar_agentid"))
                                    && (int) (trainorder.getId()) % before == payno) {
                                //                                waitPayInformation(trainorder.getId(), i);
                                if (!waitPayInformation(trainorder.getId(), i)) {
                                    trainorder.setState12306(Trainorder.ORDEREDPAYFALSE);
                                    trainorder.setIsquestionorder(Trainorder.PAYINGQUESTION);
                                    Server.getInstance().getTrainService().updateTrainorder(trainorder);
                                }
                                break;
                            }
                        }
                    }
                }
                else {
                    noOrder(i);
                    break;
                }
            }
            else {
                noOrder(i);
                break;
            }
        }
    }

    /**
     * 没有订单时公用代码块（刷新，改cofig） 
     * @param i
     * @param isqunar
     * @time 2014年12月3日 上午10:35:49
     * @author fiend
     */
    public void noOrder(int i) {
        try {
            //            JobQunarOrder.changeSystemCofig(new TrainSupplyMethod().getSysconfigStringId("autostatus" + i), "0");
            changeSystemCofig(getSystemConfigId("autostatus" + i), "0");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public long getSystemConfigId(String name) {
        List<Sysconfig> configs = Server.getInstance().getSystemService()
                .findAllSysconfig(" where c_name='" + name + "'", "", -1, 0);
        if (configs != null && configs.size() == 1) {
            Sysconfig config = configs.get(0);
            return config.getId();
        }
        return 0;
    }

    /**
     *  说明：修改获取订单状态 
     * @param scid
     * @param scvalue
     * @time 2014年9月25日 下午12:07:03
     * @author yinshubin
     */
    public void changeSystemCofig(long scid, String scvalue) {
        Sysconfig sysconfig;
        if (scid > 0) {
            sysconfig = Server.getInstance().getSystemService().findSysconfig(scid);
            sysconfig.setId(sysconfig.getId());
            sysconfig.setValue(scvalue);
            Server.getInstance().getSystemService().updateSysconfig(sysconfig);
        }
    }

    //    public List<Trainorder> getOrderlist() {
    //        return orderlist;
    //    }
    //
    //    public void setOrderlist(List<Trainorder> orderlist) {
    //        this.orderlist = orderlist;
    //    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    /**
     * 判定当前秒数 
     * @return
     * @time 2014年11月28日 上午10:54:45
     * @author fiend
     */
    public boolean iscanpay() {
        Calendar ca = Calendar.getInstance();
        int second = ca.get(Calendar.SECOND);//秒
        return second < 50;
    }

    /**
     *  获取待支付订单的信息 
     * @param trainorderid
     * @return
     * @time 2014年11月6日 下午1:58:01
     * @author yinshubin
     */
    public boolean waitPayInformation(long trainorderid, int i) {
        Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(trainorderid);

        String payLinked = trainorder.getChangesupplytradeno() == null
                || trainorder.getChangesupplytradeno().equals("") ? getpayLinked(trainorderid, i) : trainorder
                .getChangesupplytradeno();
        WriteLog.write("JobAutoPayAlipay", "当前订单号：" + trainorderid + ";当前支付链接：" + payLinked);
        if (!payLinked.equals("")) {
            return autoAlipay(payLinked, trainorderid, i);
        }
        return false;
    }

    /**
     *  获取待支付订单的信息 
     * @param trainorderid
     * @return
     * @time 2014年11月6日 下午1:58:01
     * @author yinshubin
     */
    public boolean waitPayInformation(int i, Trainorder trainorder) {
        String payLinked = trainorder.getChangesupplytradeno() == null
                || trainorder.getChangesupplytradeno().equals("") ? getpayLinked(trainorder, i) : trainorder
                .getChangesupplytradeno();
        WriteLog.write("JobAutoPayAlipay", "当前订单号：" + trainorder.getId() + ";当前支付链接：" + payLinked);
        if (!payLinked.equals("")) {
            return autoAlipay(payLinked, trainorder, i);
        }
        return false;
    }

    /**
     * 支付订单 
     * @param alipayurl
     * @param trainorderid
     * @return
     * @time 2014年11月6日 下午1:47:28
     * @author yinshubin
     */
    public boolean autoAlipay(String alipayurl, long trainorderid, int i) {
        Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(trainorderid);
        String url = new TrainSupplyMethod().getSysconfigString("Reptile_traininit_url" + i);
        String autopwd = new TrainSupplyMethod().getSysconfigString("alipaypasswordhy");
        String isqunarorder = "0";
        if (trainorder.getAgentid() == Integer.valueOf(new TrainSupplyMethod().getSysconfigString("qunar_agentid"))) {
            if (i == 0) {
                autopwd = new TrainSupplyMethod().getSysconfigString("alipaypasswordshu");
            }
            else if (i == 1) {
                autopwd = new TrainSupplyMethod().getSysconfigString("alipaypasswordwang");
            }
            isqunarorder = "1";
        }
        try {
            autopwd = URLEncoder.encode(EncryptionUtil.decrypt(autopwd), "UTF-8");
            alipayurl = URLEncoder.encode(alipayurl, "UTF-8");
        }
        catch (UnsupportedEncodingException e1) {
            System.out.println("encode失败");
        }
        String par = "datatypeflag=22&alipayurl=" + alipayurl + "&autopwd=" + autopwd + "&isqunarorder=" + isqunarorder;
        try {
            String result = "false";
            try {
                result = SendPostandGet.submitPost(url, par, "UTF-8").toString();
            }
            catch (Exception e1) {
                System.out.println("访问工具类失败");
            }
            if (result.equals("true")) {
                try {
                    changeOrderStatus(trainorderid);
                }
                catch (Exception e) {
                    System.out.println("修改订单状态失败");
                }
                return true;
            }
        }
        catch (Exception e) {
        }
        return false;
    }

    /**
     * 支付订单 
     * @param alipayurl
     * @param trainorder
     * @param i
     * @return
     * @time 2014年11月6日 下午1:47:28
     * @author yinshubin
     */
    public boolean autoAlipay(String alipayurl, Trainorder trainorder, int i) {
        //        Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(trainorderid);
        String url = new TrainSupplyMethod().getSysconfigString("Reptile_traininit_url" + i);
        String autopwd = new TrainSupplyMethod().getSysconfigString("alipaypasswordhy");
        String isqunarorder = "0";
        if (trainorder.getAgentid() == Integer.valueOf(new TrainSupplyMethod().getSysconfigString("qunar_agentid"))) {
            if (i == 0) {
                autopwd = new TrainSupplyMethod().getSysconfigString("alipaypasswordshu");
            }
            else if (i == 1) {
                autopwd = new TrainSupplyMethod().getSysconfigString("alipaypasswordwang");
            }
            isqunarorder = "1";
        }
        try {
            autopwd = URLEncoder.encode(EncryptionUtil.decrypt(autopwd), "UTF-8");
            alipayurl = URLEncoder.encode(alipayurl, "UTF-8");
        }
        catch (UnsupportedEncodingException e1) {
            System.out.println("encode失败");
        }
        String par = "datatypeflag=22&alipayurl=" + alipayurl + "&autopwd=" + autopwd + "&isqunarorder=" + isqunarorder;
        try {
            String result = "false";
            try {
                result = SendPostandGet.submitPost(url, par, "UTF-8").toString();
            }
            catch (Exception e1) {
                System.out.println("访问工具类失败");
            }
            if (result.equals("true")) {
                try {
                    changeOrderStatus(trainorder);
                }
                catch (Exception e) {
                    System.out.println("修改订单状态失败");
                }
                return true;
            }
        }
        catch (Exception e) {
        }
        return false;
    }

    /**
     * 修改订单状态 
     * @param trainorderid
     * @time 2014年11月6日 下午2:26:33
     * @author yinshubin
     */
    public void changeOrderStatus(long trainorderid) {
        Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(trainorderid);
        trainorder.setId(trainorder.getId());
        trainorder.setOrderstatus(Trainorder.WAITISSUE);
        trainorder.setState12306(Trainorder.ORDEREDPAYING);
        trainorder.setPaysupplystatus(1);
        trainorder.setOperateuid(0L);//出票完成自动解锁
        trainorder.setControlname("");
        Server.getInstance().getTrainService().updateTrainorder(trainorder);
        Trainorderrc rc = new Trainorderrc();
        rc.setOrderid(trainorder.getId());
        rc.setContent("火车票自动支付机器人" + "确认订单支付完成");
        rc.setCreateuser("火车票自动支付机器人");
        rc.setYwtype(1);
        Server.getInstance().getTrainService().createTrainorderrc(rc);
    }

    /**
     * 修改订单状态 
     * @param trainorder
     * @time 2014年11月6日 下午2:26:33
     * @author yinshubin
     */
    public void changeOrderStatus(Trainorder trainorder) {
        //        Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(trainorderid);
        if (trainorder.getQunarOrdernumber() == null || trainorder.getQunarOrdernumber().trim().equals("")) {
            trainorder.setState12306(Trainorder.ORDEREDPAYING);
            Server.getInstance().getTrainService().updateTrainorder(trainorder);
        }
        else {
            trainorder.setId(trainorder.getId());
            //            trainorder.setOrderstatus(Trainorder.ISSUED);
            trainorder.setState12306(Trainorder.ORDEREDPAYING);
            trainorder.setPaysupplystatus(1);
            trainorder.setOperateuid(0L);//出票完成自动解锁
            trainorder.setControlname("");
            Server.getInstance().getTrainService().updateTrainorder(trainorder);
        }
        Trainorderrc rc = new Trainorderrc();
        rc.setOrderid(trainorder.getId());
        rc.setContent("火车票自动支付机器人" + "确认订单支付完成");
        rc.setCreateuser("火车票自动支付机器人");
        rc.setYwtype(1);
        Server.getInstance().getTrainService().createTrainorderrc(rc);
    }

    /**
     * 查询未完成订单支付链接 
     * @param trainorderid
     * @return
     * @time 2014年11月6日 上午11:05:42
     * @author yinshubin
     */
    public String getpayLinked(long trainorderid, int i) {
        String result = "";
        Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(trainorderid);
        String extnumber = trainorder.getExtnumber();
        String cookieString = "";
        String logname = "";
        String logpassword = "";
        Customeruser customeruser = null;
        Trainpassenger trainpassenger = trainorder.getPassengers().get(0);
        String pname = trainpassenger.getName();
        String pidnumber = trainpassenger.getIdnumber();
        int pidtype = trainpassenger.getIdtype();
        String sql = "select * from T_CUSTOMERUSER b"
                + " join T_CUSTOMERPASSENGER a on a.C_CUSTOMERUSERID=b.ID join T_CUSTOMERCREDIT c on a.ID=c.C_REFID where a.C_USERNAME = \'"
                + pname + "\' and c.C_CREDITNUMBER=\'" + pidnumber + "\'" + " and c.C_CREDITTYPEID= " + pidtype
                + " AND a.C_TYPE=4";
        List<Customeruser> customerpassengerList = Server.getInstance().getMemberService()
                .findAllCustomeruserBySql(sql, -1, 0);
        if (customerpassengerList.size() > 0) {
            customeruser = customerpassengerList.get(0);
            cookieString = customeruser.getCardnunber();
            logname = customeruser.getLoginname();
            logpassword = customeruser.getLogpassword();
        }
        else {
            return result;
        }
        try {
            cookieString = URLEncoder.encode(cookieString, "UTF-8");
            String url = new TrainSupplyMethod().getSysconfigString("Reptile_traininit_url" + i);
            String par = "datatypeflag=21&cookie=" + cookieString + "&extnumber=" + extnumber + "&trainorderid="
                    + trainorderid + "&logname=" + logname + "&logpassword=" + logpassword;
            result = SendPostandGet.submitPost(url, par, "UTF-8").toString();
            if (result.indexOf("gateway.do") >= 0) {
                float supplyprice = Float.valueOf(result.split("ord_amt=")[1].split("&ord_cur")[0]);
                trainorder.setSupplyprice(supplyprice);
                trainorder.setSupplypayway(7);
                if (null == trainorder.getQunarOrdernumber() || "".equals(trainorder.getQunarOrdernumber())) {
                    trainorder.setSupplyaccount(customeruser.getLoginname() + "/hy");
                }
                else {
                    trainorder.setSupplyaccount(customeruser.getLoginname() + "/shu");
                }
                trainorder.setSupplytradeno(result.split("ord_id_ext=")[1].split("&ord_name")[0]);
                trainorder.setChangesupplytradeno(result);
                Server.getInstance().getTrainService().updateTrainorder(trainorder);
            }
        }
        catch (Exception e) {
            WriteLog.write("Train12306_order_exception", "订单号：" + trainorderid + ":ajaxorderpayment:" + e);
        }
        return result;
    }

    /**
     * 查询未完成订单支付链接 
     * @param trainorder
     * @param i
     * @return
     * @time 2014年11月6日 上午11:05:42
     * @author yinshubin
     */
    public String getpayLinked(Trainorder trainorder, int i) {
        String result = "";
        //        Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(trainorderid);
        String extnumber = trainorder.getExtnumber();
        String cookieString = "";
        String logname = "";
        String logpassword = "";
        Customeruser customeruser = null;
        Trainpassenger trainpassenger = trainorder.getPassengers().get(0);
        String pname = trainpassenger.getName();
        String pidnumber = trainpassenger.getIdnumber();
        int pidtype = trainpassenger.getIdtype();
        String sql = "select * from T_CUSTOMERUSER b"
                + " join T_CUSTOMERPASSENGER a on a.C_CUSTOMERUSERID=b.ID join T_CUSTOMERCREDIT c on a.ID=c.C_REFID where a.C_USERNAME = \'"
                + pname + "\' and c.C_CREDITNUMBER=\'" + pidnumber + "\'" + " and c.C_CREDITTYPEID= " + pidtype
                + " AND a.C_TYPE=4";
        List<Customeruser> customerpassengerList = Server.getInstance().getMemberService()
                .findAllCustomeruserBySql(sql, -1, 0);
        if (customerpassengerList.size() > 0) {
            customeruser = customerpassengerList.get(0);
            cookieString = customeruser.getCardnunber();
            logname = customeruser.getLoginname();
            logpassword = customeruser.getLogpassword();
        }
        else {
            return result;
        }
        try {
            cookieString = URLEncoder.encode(cookieString, "UTF-8");
            String url = new TrainSupplyMethod().getSysconfigString("Reptile_traininit_url" + i);
            String par = "datatypeflag=21&cookie=" + cookieString + "&extnumber=" + extnumber + "&trainorderid="
                    + trainorder.getId() + "&logname=" + logname + "&logpassword=" + logpassword;
            result = SendPostandGet.submitPost(url, par, "UTF-8").toString();
            if (result.indexOf("gateway.do") >= 0) {
                float supplyprice = Float.valueOf(result.split("ord_amt=")[1].split("&ord_cur")[0]);
                trainorder.setSupplyprice(supplyprice);
                trainorder.setSupplypayway(7);
                if (null == trainorder.getQunarOrdernumber() || "".equals(trainorder.getQunarOrdernumber())) {
                    trainorder.setSupplyaccount(customeruser.getLoginname() + "/hy");
                }
                else {
                    trainorder.setSupplyaccount(customeruser.getLoginname() + "/shu");
                }
                trainorder.setSupplytradeno(result.split("ord_id_ext=")[1].split("&ord_name")[0]);
                trainorder.setChangesupplytradeno(result);
                Server.getInstance().getTrainService().updateTrainorder(trainorder);
            }
        }
        catch (Exception e) {
            WriteLog.write("Train12306_order_exception", "订单号：" + trainorder.getId() + ":ajaxorderpayment:" + e);
        }
        return result;
    }
}
