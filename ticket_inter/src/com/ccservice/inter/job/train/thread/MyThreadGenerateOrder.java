package com.ccservice.inter.job.train.thread;

import java.net.URLEncoder;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.log4j.Logger;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.service12306.bean.TrainOrderReturnBean;
import com.ccservice.b2b2c.base.customerpassenger.Customerpassenger;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.b2b2c.policy.QunarTrainMethod;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.JobQunarOrder;
import com.ccservice.inter.job.train.TrainInterfaceMethod;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.server.Server;

/**
 * 作者:邹远超
 * 日期:2014年9月2日
 */
public class MyThreadGenerateOrder extends Thread {
    Logger logger = Logger.getLogger(this.getClass().getSimpleName());

    private String passengers;//用于下单以及重复获取未完成订单的拼接字符串

    private Trainorder trainorder;//当前订单

    private Customeruser cuser;//当前订单12306账号

    private boolean isjointrip;//是否联程票 true 正常订单,false 联程订单

    private String repUrl;//随机REP的URL

    private String cninterfaceurl;//访问interface的URL

    private long qunar_agentid;//去哪儿AGENTID

    private long dangqianjiekouagentid;//当前系统接口ID

    private int ordermax;//循环下单最大次数

    private long tongcheng_agentid;//同程AGENTID

    private long trainorderid;//当前订单ID

    private String tcTrainCallBack;//同程回调URL

    private String qunarPayurl;//qunar代付URL

    private boolean isbudan;//是否是补单订单

    private int interfacetype;//接口类别

    private int roundnum = 1;//当前下单循环次数

    int r1;

    /**
     * 
     * @param trainorderid 火车票订单id
     * @param isjointrip
     * @param repUrl
     * @param cninterfaceurl
     * @param qunar_agentid 
     * @param dangqianjiekouagentid  当前系统  agentID  0为qunar 1为同程
     * @param ordermax 循环下单次数
     * @param tongcheng_agentid
     * @param tcTrainCallBack
     * @param qunarPayurl
     */
    public MyThreadGenerateOrder(long trainorderid, boolean isjointrip, String repUrl, String cninterfaceurl,
            long qunar_agentid, long dangqianjiekouagentid, int ordermax, long tongcheng_agentid,
            String tcTrainCallBack, String qunarPayurl) {
        this.trainorderid = trainorderid;
        this.isjointrip = isjointrip;
        this.repUrl = repUrl;
        this.cninterfaceurl = cninterfaceurl;
        this.qunar_agentid = qunar_agentid;
        this.dangqianjiekouagentid = dangqianjiekouagentid;
        this.ordermax = ordermax;
        this.tongcheng_agentid = tongcheng_agentid;
        this.tcTrainCallBack = tcTrainCallBack;
        this.qunarPayurl = qunarPayurl;
        r1 = new Random().nextInt(1000000);
    }

    public MyThreadGenerateOrder() {
        super();
    }

    public void run() {
        Long l1 = System.currentTimeMillis();
        //初始化当前类公用参数
        this.trainorder = Server.getInstance().getTrainService().findTrainorder(this.trainorderid);
        WriteLog.write("JobGenerateOrder_MyThread", r1 + ":订单ID:" + trainorder.getId() + ":findtrainorderbyid_time:"
                + (System.currentTimeMillis() - l1) + ":rep:" + this.repUrl);
        isbudanorder();
        getOrderAttribution();
        //获取账号
        this.cuser = Server.getInstance().getTrain12306Service().getcustomeruser(trainorder);
        if (this.cuser.getId() > 0 && this.cuser.getIsenable() == 1) {
            WriteLog.write("JobGenerateOrder_MyThread", r1 + ":订单ID:" + trainorder.getId() + ":获取12306账号成功:"
                    + this.cuser.getLoginname());
            //下单
            boolean iscanordering = true;//是否能下单
            if (iscanordering) {
                String result = "";
                iscanordering = (trainorder.getIsquestionorder() == null || trainorder.getIsquestionorder() == 0)
                        && (trainorder.getExtnumber() == null || ("".equals(trainorder.getExtnumber()) || (trainorder
                                .getExtnumber().indexOf(",") == 0)));
                if (iscanordering) {
                    result = acquisitionParameters();
                    if (result.contains("请尽快支付")) {
                        WriteLog.write("JobGenerateOrder_MyThread", r1 + ":下单成功,当前电子单号:" + result.replace("请尽快支付", ""));
                        if (this.isjointrip) {
                            //下单成功
                            generateSuccess(trainorder.getId(), cuser);
                        }
                    }
                    else if (result.contains("数据库同步失败") || result.contains("12306订单信息不全")) {//同步数据库失败或者是12306订单信息不全
                        trainorder.setIsquestionorder(Trainorder.ORDERINGQUESTION);
                        Server.getInstance().getTrainService().updateTrainorder(trainorder);
                        String content = "下单成功," + result;
                        if (result.contains("数据库同步失败")) {
                            content = "下单失败," + result;
                            refuse(1, "");//如果数据库同步失败就拒单
                        }
                        writetrainorderrc(content, Trainticket.ISSUED);

                    }
                }
            }
        }
        else if (this.cuser.getId() > 0 && this.cuser.getIsenable() == 0) {
            WriteLog.write("JobGenerateOrder_MyThread",
                    r1 + ":订单ID:" + trainorder.getId() + ":身份验证失败:" + this.cuser.getDescription());
            refuse(1, "身份验证失败:" + this.cuser.getDescription());
            freecustomeruser();
        }
        else {
            this.cuser = Server.getInstance().getMemberService().findCustomeruser(62);
            WriteLog.write("JobGenerateOrder_MyThread", r1 + ":订单ID:" + trainorder.getId() + "获取12306账号失败");
            refuse(1, "获取12306账号失败");
        }
        WriteLog.write("JobGenerateOrder_MyThread",
                r1 + ":订单ID:" + trainorder.getId() + ":下单结束:总耗时:" + (System.currentTimeMillis() - l1));
    }

    //TODO===========================================================初始化模块START=================================================
    /**
     * 区分补单或者是占座 
     * @time 2015年1月19日 下午2:29:33
     * @author fiend
     */
    @SuppressWarnings("rawtypes")
    public void isbudanorder() {
        try {
            String sql = "SELECT C_ORDERID FROM T_TRAINORDERTIMEOUT WHERE C_STATE=0 AND C_ORDERID="
                    + trainorder.getId();
            List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
            if (list.size() > 0) {
                this.isbudan = true;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        this.isbudan = false;
    }

    /**
     * 确定订单归属 
     * @time 2015年1月27日 下午7:16:45
     * @author fiend
     */
    private void getOrderAttribution() {
        if (qunar_agentid == this.trainorder.getAgentid() && 0 == dangqianjiekouagentid) {
            this.interfacetype = TrainInterfaceMethod.QUNAR;
        }
        else if (tongcheng_agentid == this.trainorder.getAgentid() && 1 == dangqianjiekouagentid) {
            this.interfacetype = TrainInterfaceMethod.TONGCHENG;
        }
        else {
            this.interfacetype = TrainInterfaceMethod.HTHY;
        }
    }

    //===============================================================初始化模块END=================================================
    //TODO===========================================================下单模块START=================================================
    /**
     * 
     * 说明:获取下单所用的参数
     * 并下单到12306
     * 
     * @param trainorderid
     * @param customeruserId
     * @param cookieString
     * @param loginname
     * @param isjointrip 是否是联程票
     * @return gotoInit();
     * @time 2014年8月30日 上午11:16:03
     * @author yinshubin
     */
    public String acquisitionParameters(/*String url, long trainorderid, Customeruser user, String cookieString,
                                        String loginname*/) {
        WriteLog.write("JobGenerateOrder_MyThread", r1 + ":获取下单所用的参数:" + this.repUrl + ":" + trainorderid + ":"
                + this.cuser.getCardnunber() + ":" + this.cuser.getLoginname() + ":" + this.isjointrip);
        String start_station_name = "";
        String end_station_name = "";
        String passengers = "";
        for (Trainpassenger trainpassenger : this.trainorder.getPassengers()) {
            String pname = trainpassenger.getName();
            String pidnumber = trainpassenger.getIdnumber();
            int pidtype = trainpassenger.getIdtype();
            Trainticket trainticket = trainpassenger.getTraintickets().get(0);
            if (!this.isjointrip) {//联程第2段的车次信息
                trainticket = trainpassenger.getTraintickets().get(1);
            }
            String idtype = "1";
            if (1 == pidtype) {
                idtype = "1";
            }
            if (3 == pidtype) {
                idtype = "B";
            }
            if (4 == pidtype) {
                idtype = "C";
            }
            if (5 == pidtype) {
                idtype = "G";
            }
            String tickettype = trainticket.getTickettype() + "";
            if (null == trainorder.getQunarOrdernumber() || "".equals(trainorder.getQunarOrdernumber())) {
                tickettype = "1";
            }
            else {
                if ("0".equals(tickettype)) {
                    tickettype = "2";
                }
            }
            String passenger = pname + "|" + idtype + "|" + pidnumber + "|" + tickettype + "|"
                    + trainticket.getSeattype() + "|"
                    + Float.valueOf(trainticket.getPayprice()).toString().replace(".", "");
            passengers += passenger + ",";
        }
        this.passengers = passengers.substring(0, passengers.length() - 1);
        WriteLog.write("JobGenerateOrder_MyThread", r1 + ":获取下单id" + trainorderid + "所用的参数:" + passengers + ":"
                + this.cuser.getLoginname());
        Long l2 = System.currentTimeMillis();
        String TCTrainOrdering = TCTrainOrdering(start_station_name, end_station_name);
        WriteLog.write("JobGenerateOrder_MyThread", r1 + ":TCTrainOrdering:返回:" + TCTrainOrdering
                + ":TCTrainOrdering耗时:" + (System.currentTimeMillis() - l2));
        return TCTrainOrdering;
    }

    /**
     * 原来是返回String暂时把结果放到Changesupplytradeno里
     * 如需要可以使用方法getChangesupplytradeno获取
     * @param trainorder
     * @param from_station 出发站三字码,可不传
     * @param to_station 到达站三字码,可不传
     * @param isjointrip 是否是联程票
     * @return
     *>> 格式如:[{"ticket_type":"票类型","price":票价,"zwcode":"座位编码","passenger_id_type_code":"证件类型","passenger_name":"乘客姓名","passenger_id_no":"证件号"}]
     * >> ticket_type:1:成人票,2:儿童票,3:学生票,4:残军票
     * >> price:float类型
     * >> zwcode:9:商务座,P:特等座,M:一等座,O:二等座,6:高级软卧,4:软卧,3:硬卧,2:软座,1:硬座,0:站票[无座]
     * >> passenger_id_type_code:1:二代身份证,C:港澳通行证,G:台湾通 行证,B:护照
     * @time 2014年12月14日 上午10:28:02
     * @author wzc
     */
    public String TCTrainOrdering(/*Customeruser user, Trainorder trainorder,*/String from_station, String to_station) {
        WriteLog.write("JobGenerateOrder_MyThread", r1 + ":s:TCTrainOrdering:" + this.cuser);
        String result = "下单接口访问错误";
        List<Trainpassenger> passengerlist = trainorder.getPassengers();
        int ticketmark = this.isjointrip ? 0 : 1;//0 正常订单,1 联程订单
        String passengersstr = "";
        Trainticket ticket = null;
        if (passengerlist.size() > 0) {
            JSONArray array = new JSONArray();
            for (int i = 0; i < passengerlist.size(); i++) {
                Trainpassenger p = passengerlist.get(i);
                List<Trainticket> tickets = p.getTraintickets();
                if (tickets != null && tickets.size() > 0) {
                    JSONObject obj = new JSONObject();
                    obj.put("passenger_id_no", p.getIdnumber());
                    obj.put("passenger_name", p.getName());
                    obj.put("ticket_type", tickets.get(ticketmark).getTickettype() == 0 ? 2 : tickets.get(ticketmark)
                            .getTickettype());
                    obj.put("price", tickets.get(ticketmark).getPrice());
                    obj.put("zwcode", TrainSupplyMethod.getzwname(tickets.get(ticketmark).getSeattype()));
                    obj.put("passenger_id_type_code", TrainSupplyMethod.getIdtype12306(p.getIdtype()));
                    //TODO 暂时如此使用,如果采购商添加的话 再做修改
                    if (this.dangqianjiekouagentid == 0) {
                        //是否是qunar订单
                        obj.put("isqunarorder", true);
                    }
                    array.add(obj);
                }
            }
            passengersstr = array.toString();
            List<Trainticket> tickets = passengerlist.get(0).getTraintickets();
            if (tickets.size() > 0) {
                ticket = tickets.get(ticketmark);
            }
        }
        if (ticket != null && ticket.getDeparttime().length() >= 10) {
            long t1 = System.currentTimeMillis();
            result = orderRound(ticket, passengersstr, result, t1, from_station, to_station);
            long t2 = System.currentTimeMillis();
            WriteLog.write("JobGenerateOrder_MyThread", r1 + ":trainorderid:" + trainorder.getId()
                    + ":orderRound:下单用时:" + (t2 - t1) + ":" + result);
        }
        else {
            WriteLog.write("JobGenerateOrder_MyThread", r1 + ":乘客票有问题");
        }
        return result;
    }

    /**
     * 循环下单 
     * @param ticket
     * @param passengersstr
     * @param user
     * @param result
     * @param r1
     * @param t1
     * @param from_station
     * @param to_station
     * @param i
     * @return
     * @time 2015年1月3日 下午5:41:26
     * @author chendong
     */
    public String orderRound(Trainticket ticket, String passengersstr/*, Customeruser user*/, String result, long t1,
            String from_station, String to_station) {
        String train_date = ticket.getDeparttime().substring(0, 10);
        Long l1 = System.currentTimeMillis();
        TrainOrderReturnBean returnob = Server
                .getInstance()
                .getTrain12306Service()
                .create12306Order(trainorder.getId(), train_date, from_station, to_station, ticket.getDeparture(),
                        ticket.getArrival(), ticket.getTrainno(), passengersstr, this.cuser);
        String code = returnob.getCode();
        String jsonmsg = returnob.getJson();
        String msg = returnob.getMsg();
        result = msg;
        boolean success = returnob.getSuccess();
        WriteLog.write("JobGenerateOrder_MyThread", r1 + ":TCTrainOrdering:code:" + code + ":msgmsg:" + msg
                + ":orderRound_interface下单用时:" + (System.currentTimeMillis() - l1) + ":jsonmsg:" + jsonmsg);
        if (success) {
            WriteLog.write("JobGenerateOrder_MyThread", r1 + ":当前订单:" + trainorder.getId() + ":循环次数:" + this.roundnum
                    + ":下单耗时:" + (System.currentTimeMillis() - t1) + ":jsonmsg:" + jsonmsg);
            for (int j = 0; j < 5; j++) {
                if (!isall(jsonmsg)) {
                    jsonmsg = getstr();
                }
                else {
                    break;
                }
            }
            if (isall(jsonmsg)) {
                return saveOrderInformation(jsonmsg);
            }
            else {
                //                Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(this.trainorder.getId());
                trainorder.setSupplyaccount(this.cuser.getLoginname());
                trainorder.setIsquestionorder(Trainorder.ORDERINGQUESTION);
                Server.getInstance().getTrainService().updateTrainorder(trainorder);
                return "12306订单信息不全";
            }
        }
        else if (msg.indexOf("账号尚未通过身份信息核验") > 0
                || (msg.indexOf("已订") < 0 && msg.indexOf("不可购票") < 0 /* && msg.indexOf("没有足够的票") < 0*/
                        && msg.indexOf("排队人数现已超过余票数") < 0 && msg.indexOf("本次购票行程冲突") < 0/* && msg.indexOf("已无余票") < 0*/
                        && msg.indexOf("价格发生变化") < 0/* && msg.indexOf("余票不足") < 0*/&& msg.indexOf("非法的席别") < 0
                        && msg.indexOf("您的身份信息未经核验") < 0 && msg.indexOf("当前提交订单用户过多") < 0)) {
            saveTrainorderdc(result);
            WriteLog.write("JobGenerateOrder_MyThread", r1 + ":" + trainorder.getId() + ":orderRound:" + code
                    + ":result:" + result);
            if (result.contains("还在排队中") || result.contains("存在未完成订单")) {
                jsonmsg = getstr();
                WriteLog.write("JobGenerateOrder_MyThread_paidui", r1 + ":" + trainorder.getId() + ":进入排队:0:"
                        + ":jsonmsg:" + jsonmsg + ":result:" + result);
                if (jsonmsg.indexOf("无未支付订单") < 0 && jsonmsg.indexOf("用户未登录") < 0) {
                    //290573:390070:进入排队:0::jsonmsg:无未支付订单:result:提交订单成功，程序自动排队，未获取到订单号，可能情况：1、占座失败；2：超过程序排队时间设定，还在排队中。
                    if ("".equals(jsonmsg)) {
                        jsonmsg = getstr();
                        WriteLog.write("JobGenerateOrder_MyThread_paidui", r1 + ":" + trainorder.getId() + ":进入排队:1:"
                                + ":jsonmsg:" + jsonmsg + ":result:" + result);
                    }
                    if ("".equals(jsonmsg)) {
                        jsonmsg = getstr();
                        WriteLog.write("JobGenerateOrder_MyThread_paidui", r1 + ":" + trainorder.getId() + ":进入排队:2:"
                                + ":jsonmsg:" + jsonmsg + ":result:" + result);
                    }
                    if (jsonmsg.contains("没有足够的票")) {
                        result = jsonmsg;
                        //refuse(trainorder.getId(), 1, user, result);
                        freecustomeruser();
                    }
                    else {
                        for (int j = 0; j < 5; j++) {
                            boolean isall = isall(jsonmsg);
                            if (!isall) {
                                WriteLog.write("JobGenerateOrder_MyThread_paidui", r1 + ":" + trainorder.getId()
                                        + ":排队:" + j + ":次:" + code + ":jsonmsg:" + jsonmsg);
                                try {
                                    Thread.sleep(100L);
                                }
                                catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                jsonmsg = getstr();
                            }
                            else {
                                break;
                            }
                        }
                        if (isall(jsonmsg)) {
                            WriteLog.write("JobGenerateOrder_MyThread_paidui", r1 + ":" + trainorder.getId()
                                    + ":排队成功了:" + jsonmsg);
                            String saveOrderInformation_result = saveOrderInformation(jsonmsg);
                            return saveOrderInformation_result;
                        }
                        else {
                            //                Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(this.trainorder.getId());
                            trainorder.setSupplyaccount(this.cuser.getLoginname());
                            trainorder.setIsquestionorder(Trainorder.ORDERINGQUESTION);
                            Server.getInstance().getTrainService().updateTrainorder(trainorder);
                            refuse(1, result);//
                            return "12306订单信息不全";
                        }

                    }
                }
            }
            WriteLog.write("JobGenerateOrder_MyThread", r1 + ":trainorderid:" + trainorder.getId() + ":i:"
                    + this.roundnum + ":ordermax:" + ordermax + ":msg:" + msg + ":" + jsonmsg);
            if (this.roundnum < this.ordermax) {
                jsonmsg = jsonmsg == null ? "" : jsonmsg;
                if (jsonmsg.contains("没有足够的票") || jsonmsg.contains("无未支付订单") || msg.indexOf("存在未完成订单") > 0
                        || msg.indexOf("订单未支付") > 0 || msg.indexOf("取消次数过多") > 0 || msg.indexOf("账号尚未通过身份信息核验") > 0
                        || msg.indexOf("第三方") > 0 || this.roundnum % 3 == 0) {
                    if (msg.indexOf("取消次数过多") > 0) {//取消次数过多换账号
                        isenableTodaycustomeruser();
                    }
                    if (msg.indexOf("账号尚未通过身份信息核验") > 0) {//您的账号尚未通过身份信息核验
                        isenableforevercustomeruser(msg);
                    }
                    if (msg.indexOf("第三方") > 0 || this.roundnum % 3 == 0) {//账号使用重复切换账号重新下单
                        freecustomeruser();
                    }
                    //准备切换账号
                    changecustomeruer(msg);
                }
                try {
                    Thread.sleep(500L);
                }
                catch (InterruptedException e) {
                }
                this.roundnum = this.roundnum++;
                return orderRound(ticket, passengersstr, result, t1, from_station, to_station);
            }
            else {
                refuse(1, result);
                freecustomeruser();
            }
        }
        else {
            refuse(1, result);
            freecustomeruser();
        }
        return result;
    }

    /**
     * 临时使用 
     * @param customeruser
     * @return
     * @time 2014年12月31日 上午11:21:31
     * @author fiend
     */
    public Customeruser verification12306(Customeruser customeruser) {
        String url = this.repUrl;
        int r1 = new Random().nextInt(10000);
        try {
            String logname = customeruser.getLoginname();
            String logpassword = customeruser.getLogpassword();
            customeruser.setCardtype(null);
            customeruser.setState(0);
            String par = "datatypeflag=12&logname=" + logname + "&logpassword=" + logpassword;
            WriteLog.write("Job12306index_MyThreadLogin", r1 + ":" + logname + ";正在自动登录12306账号:" + logname + ":" + url
                    + "(:)" + par);
            String result = SendPostandGet.submitPost(url, par, "UTF-8").toString();
            WriteLog.write("Job12306index_MyThreadLogin", r1 + ":" + logname + ";正在自动登录12306账号:" + logname + ":"
                    + result);
            if (null != result && !"".equals(result) && result.contains("JSESSIONID")) {
                customeruser.setState(1);
                customeruser.setId(customeruser.getId());
                customeruser.setCardnunber(result);
                customeruser.setMemberemail(url);
                customeruser.setEnname("1");
                customeruser.setModifytime(new Timestamp(System.currentTimeMillis()));
                Server.getInstance().getMemberService().updateCustomeruserIgnoreNull(customeruser);
                WriteLog.write("Job12306index_MyThreadLogin", r1 + ":" + logname + ";正在自动登录12306账号成功:" + logname);
            }
            else {
                WriteLog.write("Job12306index_MyThreadLogin", r1 + ":" + logname + ";正在自动登录12306账号失败:" + logname);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return customeruser;
    }

    /**
    * 重新获取未支付订单
    * @param customeruser
    * @return
    */
    public String getstr() {
        String url = this.repUrl;
        String par = "datatypeflag=18&cookie=" + this.cuser.getCardnunber() + "&passengers=" + this.passengers
                + "&trainorderid=" + this.trainorder.getId();
        WriteLog.write("JobGenerateOrder_MyThread_getstr", r1 + ":订单号:" + this.trainorder.getId()
                + ":获取未完成订单(问题订单使用),调取TrainInit:" + url + "(:)" + par);
        String infodata = "";
        try {
            infodata = SendPostandGet.submitPost(url, par, "UTF-8").toString();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        WriteLog.write("JobGenerateOrder_MyThread_getstr", r1 + ":订单号:" + this.trainorder.getId()
                + ":获取未完成订单(问题订单使用),调取TrainInit:返回:" + infodata);
        return infodata;
    }

    //===========================================================下单模块END=================================================
    //TODO=======================================================账号模块START=================================================
    /**
     * 切换账号 
     * @param msg
     * @time 2015年1月31日 下午3:23:25
     * @author fiend
     */
    private void changecustomeruer(String msg) {
        WriteLog.write("JobGenerateOrder_MyThread", r1 + ":当前订单:" + trainorder.getId() + ":循环次数:" + this.roundnum
                + ":准备切换账号");
        this.cuser = Server.getInstance().getTrain12306Service().getcustomeruser(trainorder);
        WriteLog.write("JobGenerateOrder_MyThread", r1 + ":当前订单:" + trainorder.getId() + ":循环次数:" + this.roundnum
                + ":切换的新账号:" + this.cuser.getLoginname() + ":原因:" + msg);
        if (this.cuser.getId() > 0 && this.cuser.getIsenable() == 1) {
            WriteLog.write("JobGenerateOrder_MyThread", r1 + ":当前订单:" + trainorder.getId() + ":循环次数:" + this.roundnum
                    + ":切换账号成功:" + this.cuser.getLoginname() + "=========>" + this.cuser.getLoginname());
        }

    }

    /**
     * 占用一个账号后的解锁,
     * 支付完成等调用
     * @param cust
     * @time 2014年12月23日 下午6:31:03
     * @author chendong
     */
    public void freecustomeruser() {
        String sql = "UPDATE T_CUSTOMERUSER SET C_ENNAME='1' where C_ENNAME='0' AND C_LOGINNAME = '"
                + this.cuser.getLoginname() + "' ";

        try {
            int count = Server.getInstance().getSystemService().excuteAdvertisementBySql(sql);
            WriteLog.write("Job_MyThread_user", "freecustomeruser:" + count + ":count:" + sql);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 这个账号取消三次后今天不能使用了
     * 支付完成等调用
     * @param cust
     * @time 2014年12月23日 下午6:31:03
     * @author chendong
     */
    public void isenableTodaycustomeruser() {
        String sql = "UPDATE T_CUSTOMERUSER SET C_ENNAME='2' where C_LOGINNAME = '" + this.cuser.getLoginname() + "' ";
        try {
            int count = Server.getInstance().getSystemService().excuteAdvertisementBySql(sql);
            WriteLog.write("Job_MyThread_user", r1 + ":isenableTodaycustomeruser:" + count + ":count:" + sql);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 这个账号由于不能使用了把 isenable 改为0表示禁用了
     * @param cust
     * @time 2014年12月23日 下午6:31:03
     * @author chendong
     */
    public void isenableforevercustomeruser(String description) {
        String sql = "UPDATE T_CUSTOMERUSER SET " + Customeruser.COL_isenable + "='0'," + Customeruser.COL_state
                + "='0'," + Customeruser.COL_description + "='" + description + "' WHERE C_LOGINNAME = '"
                + this.cuser.getLoginname() + "' ";
        try {
            int count = Server.getInstance().getSystemService().excuteAdvertisementBySql(sql);
            WriteLog.write("Job_MyThread_user", r1 + ":isenableforevercustomeruser:" + count + ":count:" + sql);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    //===========================================================账号模块END================================================
    //TODO=======================================================出票模块START================================================

    /**
     * 下单成功处理方案 
     * @param trainorderid
     * @time 2014年12月26日 上午9:02:06
     * @author fiend
     */
    public void generateSuccess(long trainorderid, Customeruser cuser) {
        if (TrainInterfaceMethod.HTHY == this.interfacetype)
            successHTHY();
        else if (TrainInterfaceMethod.QUNAR == this.interfacetype)
            successQUNAR();
        else if (TrainInterfaceMethod.TONGCHENG == this.interfacetype)
            successTONGCHENG();
    }

    private void successQUNAR() {
        if (isallticketseat()) {
            if (ishaveseat()) {
                float differenceprice = getdirrerenceprice();
                if (differenceprice < 0) {
                    refuse(3, "与12306票价不符");
                }
                else {
                    if (qunarPay()) {
                        this.trainorder.setSupplypayway(14);
                        this.trainorder.setSupplytradeno("qunarpaying");
                        this.trainorder.setSupplytradeno(this.trainorder.getSupplytradeno() + "/QUNAR");
                        this.trainorder.setState12306(Trainorder.ORDEREDPAYING);
                        this.trainorder.setIsquestionorder(Trainorder.NOQUESTION);
                        Server.getInstance().getTrainService().updateTrainorder(this.trainorder);
                        writeRC("qunar代付发送成功", "qunar代付", 2);
                    }
                    else {
                        this.trainorder.setSupplypayway(14);
                        this.trainorder.setSupplytradeno("qunarpayfalse");
                        this.trainorder.setState12306(Trainorder.ORDEREDWAITPAY);
                        this.trainorder.setIsquestionorder(Trainorder.NOQUESTION);
                        Server.getInstance().getTrainService().updateTrainorder(this.trainorder);
                        writeRC("<span style='color:red;'>qunar代付发送失败</span>", "qunar代付", 2);
                    }
                }
                //                                        trainIssue(trainorder, trainorder.getSupplyaccount().split("/")[0], cuser);
            }
            else {
                writeRC("该订单包含<span style='color:red;'>无座</span>且客户不要无座", "自动下单", 2);
                refuse(3, null);
                freecustomeruser();
            }
        }
    }

    private void successTONGCHENG() {
        this.trainorder.setState12306(Trainorder.ORDEREDWAITPAY);
        this.trainorder.setIsquestionorder(Trainorder.NOQUESTION);
        Server.getInstance().getTrainService().updateTrainorder(this.trainorder);
        String isbudanstr = "占座";
        if (isbudan) {
            isbudanstr = "补单";
        }
        String callbackordered = "调" + isbudanstr + "接口失败";
        if (this.isbudan) {
            if (isHaveWZ()) {
                if (isThisWP()) {
                    callbackordered = "success";//callBackTongChengBudan(this.trainorder.getId(), "true");
                }
                else {
                    callbackordered = "卧铺补单坐席与原坐席不匹配";
                }
            }
            else {//订单中有无座,调无法出票
                callbackordered = callBackTongChengPayedFalse(this.trainorder);
            }
        }
        else {
            callbackordered = callBackTongChengOrdered(this.trainorder.getId(), "true");
        }
        WriteLog.write("JobGenerateOrder_MyThread",
                r1 + ":订单ID:" + this.trainorder.getId() + ";12306账号:" + this.cuser.getLoginname() + ":refuse:同程:"
                        + isbudanstr + ":结果回调接口返回====>" + callbackordered);
        if (!"success".equalsIgnoreCase(callbackordered)) {
            this.trainorder.setIsquestionorder(Trainorder.CAIGOUQUESTION);
            this.trainorder.setState12306(Trainorder.ORDERFALSE);
            if (isbudan) {
                writetrainorderrc(callbackordered, 2);
                this.trainorder.setOrderstatus(Trainorder.WAITISSUE);
            }
            else {
                writetrainorderrc("占座成功回调失败", 1);
                this.trainorder.setOrderstatus(Trainorder.WAITPAY);
            }
            Server.getInstance().getTrainService().updateTrainorder(this.trainorder);
        }
        else {
            try {
                if (isbudan) {
                    writetrainorderrc("补单成功回调成功", 2);
                    String sql = "UPDATE T_TRAINORDERTIMEOUT SET C_STATE=1 WHERE C_ORDERID =" + this.trainorder.getId();
                    Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                }
                else {
                    writetrainorderrc("占座成功回调成功", 1);
                    String sql = "UPDATE T_TRAINORDERTIMEOUT SET C_STATE=2 WHERE C_ORDERID =" + this.trainorder.getId();
                    Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                    this.trainorder.setOrderstatus(Trainorder.REFUSENOREFUND);
                    Server.getInstance().getTrainService().updateTrainorder(this.trainorder);
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void successHTHY() {
        if (!isallticketseat() || !ishaveseat()) {
            this.trainorder.setState12306(Trainorder.ORDEREDWAITPAY);
            this.trainorder.setIsquestionorder(Trainorder.NOQUESTION);
            Server.getInstance().getTrainService().updateTrainorder(this.trainorder);
            writetrainorderrc("下单成功等待支付", 2);
        }
        else {
            this.trainorder.setIsquestionorder(Trainorder.CAIGOUQUESTION);
            writetrainorderrc("下单成功,请客服检查坐席", 2);
        }

    }

    //===========================================================出票模块END================================================
    //TODO=======================================================存储模块START================================================
    /**
     * 说明:将自动下单成功的订单信息存入数据库
     * @param str
     * @param trainorderid
     * @param loginname
     * @param isjointrip
     * @return  存储结果,电子订单号
     * @time 2014年8月30日 上午11:18:59
     * @author yinshubin
     */
    public String saveOrderInformation(String str/*, long trainorderid, String loginname, boolean isjointrip*/) {
        //        Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(trainorderid);
        try {
            JSONObject jsono = JSONObject.fromObject(str);
            if (jsono.has("data")) {
                JSONObject jsonodata = jsono.getJSONObject("data");
                if (jsonodata.has("orderDBList")) {
                    JSONArray jsonaorderDBList = jsonodata.getJSONArray("orderDBList");
                    for (int j = 0; j < jsonaorderDBList.size(); j++) {//获取所有订单
                        JSONObject jsonoorderDBList = jsonaorderDBList.getJSONObject(j);
                        if (jsonoorderDBList.toString().contains("待支付")) {//拿到那个唯一的待支付订单
                            WriteLog.write("JobGenerateOrder_MyThread", r1 + ":订单号:" + this.trainorderid
                                    + ":下单成功,获取待支付订单信息:" + jsonoorderDBList.toString());
                            String sequence_no = jsonoorderDBList.getString("sequence_no");
                            String order_date = jsonoorderDBList.getString("order_date");
                            Float ticket_total_price_page = Float.valueOf(jsonoorderDBList
                                    .getString("ticket_total_price_page"));
                            String start_time = jsonoorderDBList.getString("start_train_date_page");
                            String train_code = jsonoorderDBList.getString("train_code_page");
                            String from_station = jsonoorderDBList.getJSONArray("from_station_name_page").get(0)
                                    .toString();
                            String to_station = jsonoorderDBList.getJSONArray("to_station_name_page").get(0).toString();
                            String trainno = "";
                            String traintime = "";
                            String startstation = "";
                            String endstation = "";
                            trainno = this.trainorder.getPassengers().get(0).getTraintickets().get(0).getTrainno();
                            traintime = this.trainorder.getPassengers().get(0).getTraintickets().get(0).getDeparttime();
                            startstation = this.trainorder.getPassengers().get(0).getTraintickets().get(0)
                                    .getDeparture();
                            endstation = this.trainorder.getPassengers().get(0).getTraintickets().get(0).getArrival();
                            if (comparisonTraincode(train_code, trainno) && start_time.equals(traintime)
                                    && from_station.equals(startstation) && to_station.equals(endstation)) {
                                if (jsonoorderDBList.has("tickets")) {
                                    JSONArray tickets = jsonoorderDBList.getJSONArray("tickets");
                                    updatepassengerfrom12306jsonbypassenger(trainorder.getPassengers(), tickets,
                                            isjointrip);
                                }
                                if (this.isjointrip) {
                                    if (this.trainorder.getExtnumber() == null) {
                                        this.trainorder.setExtnumber(sequence_no);
                                    }
                                    else {
                                        this.trainorder.setExtnumber(sequence_no + trainorder.getExtnumber());
                                    }
                                    this.trainorder.setSupplyaccount(this.cuser.getLoginname());
                                }
                                else {//联程票第二段
                                    if (this.trainorder.getExtnumber() == null) {
                                        this.trainorder.setExtnumber("," + sequence_no);
                                    }
                                    else {
                                        this.trainorder
                                                .setExtnumber(this.trainorder.getExtnumber() + "," + sequence_no);
                                    }
                                }
                                if (this.trainorder.getAgentid() == this.tongcheng_agentid
                                        && this.dangqianjiekouagentid == 1) {//存入订单总价
                                    this.trainorder.setOrderprice(ticket_total_price_page);
                                    try {
                                        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        //                                        trainorder.setCreatetime(new Timestamp(format.parse(order_date).getTime()));
                                        String sql = "UPDATE T_TRAINORDER SET C_CREATETIME='"
                                                + new Timestamp(format.parse(order_date).getTime())
                                                + "',C_EXTORDERCREATETIME='"
                                                + new Timestamp(format.parse(order_date).getTime()) + "' WHERE ID ="
                                                + this.trainorder.getId();
                                        Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                                    }
                                    catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                                //                                trainorder.setState12306(Trainorder.ORDEREDWAITPAY);
                                trainorder.setIsquestionorder(Trainorder.NOQUESTION);
                                trainorder.setState12306(Trainorder.ORDEREDWAITPAY);
                                Server.getInstance().getTrainService().updateTrainorder(trainorder);
                                String createuser = "12306";
                                if (trainorder.getAgentid() == this.tongcheng_agentid
                                        && this.dangqianjiekouagentid == 1) {
                                }
                                else {
                                    createuser = this.cuser.getLoginname();
                                }
                                writeRC("下单成功,电子单号:" + sequence_no, createuser, trainorder.getOrderstatus());
                                WriteLog.write("JobGenerateOrder_MyThread", r1 + ":订单号:" + trainorderid
                                        + ":下单成功,更改订单信息信息:" + trainorder.getId() + "(:)" + sequence_no);
                                return "请尽快支付" + sequence_no;
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            logger.error("ticket_mythread_saveOrderInformation", e.fillInStackTrace());
            WriteLog.write("JobGenerateOrder_MyThread", r1 + ":订单号:" + trainorderid + ":下单成功,数据库同步失败:" + e);
        }
        WriteLog.write("JobGenerateOrder_MyThread", r1 + ":订单号:" + trainorderid + ":下单成功,数据库同步失败");
        return "下单成功,数据库同步失败";
    }

    /**
     * 根据12306返回的tickets,获取到对应人的票的信息,并且把人放进去
     * 暂不用
     * @param trainpassengers
     * @param tickets
     * @param isjointrip
     * @time 2014年12月29日 下午5:49:51
     * @author chendong
     */
    private void updatepassengerfrom12306jsonbypassenger(List<Trainpassenger> trainpassengers, JSONArray tickets,
            boolean isjointrip) {
        int k = isjointrip ? 0 : 1;
        for (int i = 0; i < tickets.size(); i++) {
            JSONObject jsonoticket = tickets.getJSONObject(i);
            int ticket_type_code = jsonoticket.getInt("ticket_type_code");//乘客类型(票种)
            JSONObject jsonopassengerDTO = jsonoticket.getJSONObject("passengerDTO");
            String ticket_no = jsonoticket.getString("ticket_no");
            String coach_no = jsonoticket.getString("coach_no");
            String seat_name = jsonoticket.getString("seat_name");
            Float str_ticket_price_page = Float.valueOf(jsonoticket.getString("str_ticket_price_page"));
            String passenger_id_no = jsonopassengerDTO.getString("passenger_id_no");
            if (ticket_type_code == 1) {//成人
                for (int j = 0; j < trainpassengers.size(); j++) {
                    Trainpassenger trainpassenger = trainpassengers.get(j);//获取到对象的票的信息
                    Trainticket trainticket = trainpassenger.getTraintickets().get(k);
                    if (trainticket.getTickettype() == 1 && !"".equals(trainpassenger.getIdnumber())
                            && trainpassenger.getIdnumber().toUpperCase().equals(passenger_id_no)) {
                        //                        trainticket = Server.getInstance().getTrainService().findTrainticket(trainticket.getId());
                        trainticket.setTicketno(ticket_no);
                        trainticket.setTickettype(ticket_type_code);
                        trainticket.setCoach(coach_no);
                        trainticket.setSeatno(seat_name);
                        trainticket.setPrice(str_ticket_price_page);
                        Server.getInstance().getTrainService().updateTrainticket(trainticket);
                        break;//找到这个人了就不往下走了,继续找
                    }
                }
            }
            else if (ticket_type_code == 2) {//儿童
                for (int j = 0; j < trainpassengers.size(); j++) {
                    Trainpassenger trainpassenger = trainpassengers.get(j);//获取到对象的票的信息
                    Trainticket trainticket = trainpassenger.getTraintickets().get(k);
                    if (trainticket.getTickettype() == 2 && trainticket.getSeatno() == null
                            && !"".equals(trainpassenger.getIdnumber())
                            && trainpassenger.getIdnumber().toUpperCase().equals(passenger_id_no)) {
                        //                        trainticket = Server.getInstance().getTrainService().findTrainticket(trainticket.getId());
                        trainticket.setTicketno(ticket_no);
                        trainticket.setTickettype(ticket_type_code);
                        trainticket.setCoach(coach_no);
                        trainticket.setSeatno(seat_name);
                        trainticket.setPrice(str_ticket_price_page);
                        Server.getInstance().getTrainService().updateTrainticket(trainticket);
                        break;//找到这个人了就不往下走了,继续找
                    }
                    else {
                        continue;//找不到这个人就继续走
                    }
                }
            }
        }
    }

    //===========================================================出票模块END================================================
    //TODO===========================================================拒单模块START===============================================

    /**
     * 说明:拒单公用方法
     * @param state   1:所购买的车次坐席已无票 2:身份证件已经实名制购票,不能再次购买同日期同车次的车票 3:qunar票价和12306不符
     * @param returnmsg  同程订单下单失败回调 具体原因
     * @time 2014年9月1日 下午2:45:18
     * @author yinshubin
     */
    public void refuse(int state, String returnmsg) {
        WriteLog.write("JobGenerateOrder_MyThread",
                r1 + ":订单ID:" + this.trainorderid + ";12306账号:" + this.cuser.getLoginname() + ":refuse:准备拒单:拒单原因:"
                        + refusereason(state));
        if (TrainInterfaceMethod.HTHY == this.interfacetype)
            refuseHTHY(returnmsg);
        else if (TrainInterfaceMethod.QUNAR == this.interfacetype)
            refuseQUNAR(returnmsg);
        else if (TrainInterfaceMethod.TONGCHENG == this.interfacetype)
            refuseTONGCHENG(returnmsg);
    }

    private void refuseHTHY(String returnmsg) {
        Server.getInstance().getTrainService().refuseTrain(this.trainorderid, this.cuser, this.cninterfaceurl);
        writetrainorderrc(returnmsg, 6);
    }

    private void refuseTONGCHENG(String returnmsg) {
        this.trainorder.setOrderstatus(Trainorder.CANCLED);//先改状态,再调接口
        Server.getInstance().getTrainService().updateTrainorder(this.trainorder);
        writeRC(returnmsg, "12306", 6);
        String isbudanstr = "占座失败";
        if (isbudan) {
            isbudanstr = "补单失败";
        }
        String callbackordered = "调" + isbudanstr + "接口失败";
        if (isbudan) {
            callbackordered = callBackTongChengPayedFalse(this.trainorder);
        }
        else {
            callbackordered = callBackTongChengOrdered(this.trainorder.getId(), returnmsg);
        }
        WriteLog.write("JobGenerateOrder_MyThread",
                r1 + ":订单ID:" + this.trainorder.getId() + ";12306账号:" + this.cuser.getLoginname() + ":refuse:准备调用同程:"
                        + isbudanstr + ":结果回调接口====>" + callbackordered);

        if (!"success".equalsIgnoreCase(callbackordered)) {
            this.trainorder.setIsquestionorder(Trainorder.CAIGOUQUESTION);
            this.trainorder.setState12306(Trainorder.ORDERFALSE);
            if (isbudan) {
                this.trainorder.setOrderstatus(Trainorder.WAITISSUE);
                writeRC(isbudanstr + "回调失败", "12306", Trainorder.WAITISSUE);
            }
            else {
                this.trainorder.setOrderstatus(Trainorder.WAITPAY);
                writeRC(isbudanstr + "回调失败", "12306", Trainorder.WAITPAY);
            }
        }
        else {
            writeRC(isbudanstr + "回调成功", "12306", Trainorder.CANCLED);
            try {
                if (isbudan) {
                    String sql = "UPDATE T_TRAINORDERTIMEOUT SET C_STATE=2 WHERE C_ORDERID =" + trainorder.getId();
                    Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                    this.trainorder.setOrderstatus(Trainorder.CANCLED);
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        Server.getInstance().getTrainService().updateTrainorder(this.trainorder);
    }

    private void refuseQUNAR(String returnmsg) {
        for (Trainpassenger trainpassenger : this.trainorder.getPassengers()) {
            JobQunarOrder.changecustomeruser(trainpassenger.getName(), trainpassenger.getIdnumber(), "1");
        }
        WriteLog.write("JobGenerateOrder_MyThread",
                r1 + ":订单ID:" + this.trainorder.getId() + ";12306账号:" + this.cuser.getLoginname()
                        + ":refuse:准备调用拒单拒单接口");
        if (isrefuse(0)) {
            WriteLog.write("JobGenerateOrder_MyThread", r1 + ":订单ID:" + this.trainorder.getId() + ";12306账号:"
                    + this.cuser.getLoginname() + ":refuse:拒单成功");
            this.trainorder.setOrderstatus(Trainorder.REFUSED);
            try {
                Server.getInstance().getTrainService().refusequnarTrain(this.trainorder, Trainticket.NONISSUEDABLE);
                writetrainorderrc("拒单-无法出票:" + returnmsg, Trainticket.NONISSUEDABLE);
            }
            catch (Exception e) {
                try {
                    Server.getInstance().getTrainService().refusequnarTrain(this.trainorder, Trainticket.NONISSUEDABLE);
                    writetrainorderrc("拒单-无法出票:" + returnmsg, Trainticket.NONISSUEDABLE);
                }
                catch (Exception e1) {
                    this.trainorder.setIsquestionorder(Trainorder.CAIGOUQUESTION);
                    this.trainorder.setOrderstatus(Trainorder.WAITISSUE);
                    writetrainorderrc("拒单-无法出票:" + returnmsg + ":数据库修改失败", Trainticket.NONISSUEDABLE);
                }
            }
        }
        else {
            WriteLog.write("JobGenerateOrder_MyThread",
                    r1 + ":订单ID:" + trainorder.getId() + ";12306账号:" + this.cuser.getLoginname() + ":refuse:拒单失败");
            this.trainorder.setIsquestionorder(Trainorder.CAIGOUQUESTION);
            writetrainorderrc("拒单-失败", Trainticket.WAITISSUE);
        }
        Server.getInstance().getTrainService().updateTrainorder(this.trainorder);
    }

    //===========================================================拒单模块END================================================
    //TODO=======================================================操作记录模块START================================================
    /**
     * 此类公用生成操作记录方法 
     * @param content
     * @param status
     * @time 2015年1月31日 下午3:37:17
     * @author fiend
     */
    private void writetrainorderrc(String content, int status) {
        String createuser = this.cuser.getLoginname();
        if (TrainInterfaceMethod.TONGCHENG == this.interfacetype) {
            createuser = "12306";
        }
        createTrainorderrc(this.trainorderid, content, createuser, status, 1);
    }

    /**
     * 此类公用生成操作记录方法 
     * @param content
     * @param status
     * @time 2015年1月31日 下午3:37:17
     * @author fiend
     */
    private void writeRC(String content, String createuser, int status) {
        createTrainorderrc(this.trainorderid, content, createuser, status, 1);
    }

    /**
     * 创建火车票操作记录
     * 
     * @param trainorderId 火车票订单id
     * @param content 内容
     * @param createuser 用户
     * @param status 状态
     * @time 2015年1月18日 下午5:02:43
     * @author chendong
     */
    private void createTrainorderrc(Long trainorderId, String content, String createuser, int status, int ywtype) {
        Trainorderrc rc = new Trainorderrc();
        rc.setOrderid(trainorderId);
        rc.setContent(content);
        rc.setStatus(status);
        rc.setCreateuser(createuser);
        rc.setYwtype(ywtype);
        Server.getInstance().getTrainService().createTrainorderrc(rc);
    }

    /**
     * 说明:自动下单失败的书写操作记录
     * @param result
     * @param loginname
     * @param trainorder
     * @param isjointrip  是否是联程票
     * @param customeruserId
     * @param orderno 第几次下单
     * @time 2014年8月30日 上午11:42:19
     * @author yinshubin
     */
    public void saveTrainorderdc(String result) {
        //        联程票用
        //        if (!this.isjointrip) {
        //            trainorder.setChangesupplypayway(1);
        //        }
        String content = "";
        if (this.isjointrip) {//true 正常订单,false 联程订单
            if (TrainInterfaceMethod.TONGCHENG == this.interfacetype) {
                content = "12306:第" + this.roundnum + "次:下单失败:" + result;
            }
            else {
                content = this.cuser.getLoginname() + "第" + this.roundnum + "次:下单失败:" + result;
            }
        }
        else {
            content = this.cuser.getLoginname() + ":第二程下单失败:" + result;
        }
        writetrainorderrc(content, 1);
    }

    //    /**
    //     * 同程回调失败后 生成操作记录
    //     * @param result
    //     * @param loginname
    //     * @param trainorder
    //     * @param isordered
    //     * @time 2015年1月4日 下午2:05:46
    //     * @author fiend
    //     */
    //    public void tongchengRc(String result, boolean isordered, boolean iscallbacked) {
    //        String str = isordered ? "---" + isbudanstr + "成功" : "---" + isbudanstr + "失败";
    //        Trainorderrc rc = new Trainorderrc();
    //        rc.setContent(str + "----回调失败:" + result);
    //        rc.setCreateuser("12306");
    //        rc.setOrderid(trainorder.getId());
    //        rc.setYwtype(1);
    //        Server.getInstance().getTrainService().createTrainorderrc(rc);
    //    }
    //
    //    /**
    //     * 同程回调成功后 生成操作记录
    //     * @param loginname
    //     * @param trainorder
    //     * @param isordered
    //     * @time 2015年1月4日 下午2:05:46
    //     * @author fiend
    //     */
    //    public void tongchengTrueRc(String loginname, Trainorder trainorder, boolean isordered, String isbudanstr) {
    //        String str = isordered ? "---" + isbudanstr + "成功" : "---" + isbudanstr + "失败";
    //        Trainorderrc rc = new Trainorderrc();
    //        rc.setContent(str + "----回调成功");
    //        if ("---补单成功".equals(str)) {
    //            rc.setContent(str + "----等待支付后回调");
    //        }
    //        rc.setCreateuser("12306");
    //        rc.setOrderid(trainorder.getId());
    //        rc.setYwtype(1);
    //        Server.getInstance().getTrainService().createTrainorderrc(rc);
    //    }

    //===========================================================操作记录模块END================================================
    //TODO=======================================================接口模块START================================================
    /**
     * qunar代付 
     * @return
     * @time 2015年1月21日 下午5:53:36
     * @author fiend
     */
    public boolean qunarPay() {
        boolean isresult = false;
        String url = this.qunarPayurl;
        JSONObject jso = new JSONObject();
        jso.put("trainorderid", this.trainorderid);
        jso.put("method", "qunartrain_pay");
        try {
            WriteLog.write("JobGenerateOrder_MyThread_qunarPay", r1 + ":url:" + url + ":" + jso.toString());
            String result = SendPostandGet.submitPost(url, jso.toString(), "UTF-8").toString();
            WriteLog.write("JobGenerateOrder_MyThread_qunarPay", r1 + ":" + result);
            if ("success".equalsIgnoreCase(result)) {
                isresult = true;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return isresult;
    }

    /**
     * 说明:调取出票、拒单接口 
     * @param orderid
     * @param state
     * @return
     * @time 2014年9月1日 下午2:45:39
     * @author yinshubin
     */
    public boolean isrefuse(int state) {
        boolean result = false;
        result = QunarTrainMethod.trainIssueOrRefuse(this.trainorderid, state);
        return result;
    }

    /**
     * 同程回调补单结果
     * @param orderid
     * @param returnmsg 回调具体内容  补单成功传true
     * @return
     * @time 2014年01月11日 下午6:20:30
     * @author fiend
     */
    public String callBackTongChengBudan(long orderid, String returnmsg) {
        String result = "false";
        String url = this.tcTrainCallBack;
        try {
            returnmsg = URLEncoder.encode(returnmsg, "utf-8");
        }
        catch (Exception e) {
        }
        JSONObject jso = new JSONObject();
        jso.put("trainorderid", orderid);
        jso.put("method", "train_order_budan_callback");
        jso.put("returnmsg", returnmsg);
        try {
            result = SendPostandGet.submitPost(url, jso.toString(), "UTF-8").toString();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 回调支付失败
     * 
     * @param trainorder
     * @return
     * @time 2014年12月12日 下午2:20:30
     * @author fiend
     */
    public String callBackTongChengPayedFalse(Trainorder trainorder) {
        String result = "false";
        String url = this.tcTrainCallBack;
        JSONObject jso = new JSONObject();
        jso.put("orderid", trainorder.getQunarOrdernumber());
        jso.put("transactionid", trainorder.getOrdernumber());
        jso.put("method", "train_pay_callback");
        jso.put("isSuccess", "N");
        try {
            result = SendPostandGet.submitPost(url, jso.toString(), "UTF-8").toString();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 同程回调占座结果
     * @param i
     * @param orderid
     * @param returnmsg 回调具体内容  占座成功传true
     * @return
     * @time 2014年12月12日 下午2:20:30
     * @author fiend
     */
    public String callBackTongChengOrdered(long orderid, String returnmsg) {
        String result = "false";
        String url = this.tcTrainCallBack;
        returnmsg = returnMsgStr(returnmsg);
        try {
            returnmsg = URLEncoder.encode(returnmsg, "utf-8");
        }
        catch (Exception e) {
        }
        JSONObject jso = new JSONObject();
        jso.put("trainorderid", orderid);
        jso.put("method", "train_order_callback");
        jso.put("returnmsg", returnmsg);
        try {
            WriteLog.write("JobGenerateOrder_MyThread_callBackTongChengOrdered",
                    r1 + ":url:" + url + ":" + jso.toString());
            result = SendPostandGet.submitPost(url, jso.toString(), "UTF-8").toString();
            if ("".equalsIgnoreCase(result.trim())) {
                result = "false";
            }
            WriteLog.write("JobGenerateOrder_MyThread_callBackTongChengOrdered", r1 + ":" + result);
        }
        catch (Exception e) {
            e.printStackTrace();
            result = "false";
        }
        return result;
    }

    //===========================================================接口模块END================================================
    //TODO=======================================================工具模块START================================================

    /**
     * 说明:判断无座 
     * @param trainorder
     * @return
     * @time 2014年9月1日 下午8:32:55
     * @author yinshubin
     */
    public boolean ishaveseat() {
        boolean result = true;
        for (Trainpassenger trainpassenger : this.trainorder.getPassengers()) {
            for (Trainticket trainticket : trainpassenger.getTraintickets()) {
                if ("无座".equals(trainticket.getSeatno())) {
                    WriteLog.write("JobGenerateOrder_MyThread", r1 + ":当前订单:" + this.trainorder.getId()
                            + ":ishaveseat:包含无座");
                    result = false;
                    String extSeat = trainticket.getTcseatno();
                    if (extSeat != null && !"".equals(extSeat)) {
                        try {
                            JSONArray jsa = JSONArray.fromObject(extSeat);
                            for (int i = 0; i < jsa.size(); i++) {
                                JSONObject jso = jsa.getJSONObject(i);
                                if (jso.has("0")) {
                                    result = true;
                                    WriteLog.write("JobGenerateOrder_MyThread", r1 + ":当前订单:" + this.trainorder.getId()
                                            + ":ishaveseat:包含无座并且接受无座");
                                    break;
                                }
                            }
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        return result;
    }

    /**
     * 订单票信息是否完整
     * @param trainorder
     * @return
     * @time 2015年1月5日 上午10:41:13
     * @author fiend
     */
    public boolean isallticketseat() {
        for (Trainpassenger trainpassenger : this.trainorder.getPassengers()) {
            for (Trainticket trainticket : trainpassenger.getTraintickets()) {
                if (trainticket.getSeatno() == null) {
                    WriteLog.write("JobGenerateOrder_MyThread", r1 + ":当前订单:" + trainorder.getId()
                            + ";ishaveseat:因坐席信息不全,无法直接出票");
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 说明:得到订单差价 
     * @return
     * @time 2014年9月1日 下午3:29:14
     * @author yinshubin
     */
    public float getdirrerenceprice() {
        float differenceprice = 0f;
        for (Trainpassenger trainpassenger : this.trainorder.getPassengers()) {
            for (Trainticket trainticket : trainpassenger.getTraintickets()) {
                differenceprice += trainticket.getPayprice() - trainticket.getPrice();
            }
        }
        return differenceprice;
    }

    /**
     * 说明:通过12306账号比对未完成订单
     * @param customeruserId
     * @param pid
     * @param pname
     * @param pidtype
     * @return  boolean
     * @time 2014年8月30日 上午11:15:55
     * @author yinshubin
     */
    @SuppressWarnings("unchecked")
    public boolean isalignmentTrue(long customeruserId, String pid, String pname, int pidtype) {
        boolean result = false;
        String sql = "SELECT * FROM T_CUSTOMERPASSENGER a JOIN T_CUSTOMERCREDIT b ON a.ID=b.C_REFID WHERE a.C_CUSTOMERUSERID="
                + customeruserId
                + " AND a.C_USERNAME='"
                + pname
                + "' AND b.C_CREDITTYPEID="
                + pidtype
                + " AND b.C_CREDITNUMBER='" + pid + "' AND b.C_STAUS=1 AND a.C_STATE=1";
        WriteLog.write("JobGenerateOrder_MyThread", r1 + ":当前的乘客是否在该12306中:SQL:" + sql);
        List<Customerpassenger> customerpassengerList = Server.getInstance().getMemberService()
                .findAllCustomerpassengerBySql(sql, -1, 0);
        WriteLog.write("JobGenerateOrder_MyThread", r1 + ":当前的乘客是否在该12306中:" + customerpassengerList.size());
        if (1 == customerpassengerList.size()) {
            result = true;
        }
        return result;
    }

    /**
     * 获取原订单的WPS 
     * @return
     * @time 2015年1月18日 下午8:07:56
     * @author fiend
     */
    @SuppressWarnings("rawtypes")
    public String getWPS() {
        String sql = "SELECT C_WPS FROM T_TRAINORDERTIMEOUT WHERE C_ORDERID =" + this.trainorder.getId();
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        if (list.size() > 0) {
            Map map = (Map) list.get(0);
            return map.get("C_WPS").toString();
        }
        return null;
    }

    /**
     * 同程补单判断卧铺坐席 
     * @return
     * @time 2015年1月18日 下午7:40:10
     * @author fiend
     */
    public boolean isThisWP() {
        boolean ishaveWP = false;
        int counts = 0;
        int countz = 0;
        int countx = 0;
        //        Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(this.trainorder.getId());
        for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
            for (Trainticket trainticket : trainpassenger.getTraintickets()) {
                if (trainticket.getSeatno().contains("铺")) {
                    ishaveWP = true;
                }
                if (trainticket.getSeatno().contains("上铺")) {
                    counts++;
                }
                if (trainticket.getSeatno().contains("中铺")) {
                    countz++;
                }
                if (trainticket.getSeatno().contains("下铺")) {
                    countx++;
                }
            }
        }
        if (ishaveWP) {
            String wps = counts + "|" + countz + "|" + countx;
            String WPS = getWPS();
            if (WPS == null || "".equals(WPS)) {
                return false;
            }
            if (!wps.equals(WPS)) {
                return false;
            }
        }
        return true;
    }

    /**
     * 处理加工12306返回结果 
     * @param returnmsg
     * @return
     * @time 2015年1月4日 上午11:18:03
     * @author fiend
     */
    public String returnMsgStr(String returnmsg) {
        if (returnmsg.contains("获取12306账号失败") || returnmsg.contains("您的账号尚未通过身份信息核验")
                || returnmsg.contains("获取下单服务器失败")) {
            return "下单失败";
        }
        return returnmsg;
    }

    /**
    /**
     * 时间转换 
     * @param date
     * @param format
     * @return
     * @time 2014年10月9日 下午6:45:34
     * @author yinshubin
     */
    public Timestamp formatStringToTime(String date, String format) {
        try {
            SimpleDateFormat simplefromat = new SimpleDateFormat(format);
            return new Timestamp(simplefromat.parse(date).getTime());

        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @param str
     * @return 是否为null或""
     */
    public boolean isNotNullOrEpt(String str) {
        if (str != null && str.trim().trim().length() > 0) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * 日期转换 
     * @param date
     * @return
     * @time 2014年10月9日 下午6:46:05
     * @author yinshubin
     */
    public String formatchinaMMdd(Timestamp date) {
        return (new SimpleDateFormat("MM月dd日").format(date));
    }

    /**
     * 日期转换 
     * @param date
     * @return
     * @time 2014年10月9日 下午6:46:05
     * @author yinshubin
     */
    public String formatTimestampHHmm(Timestamp date) {
        try {
            return (new SimpleDateFormat("HH:mm").format(date));

        }
        catch (Exception e) {
            return "";
        }
    }

    /**
     * 说明:保存订单信息比对车次 
     * @param traincode
     * @param trainno
     * @return
     * @time 2014年10月6日 上午8:26:05
     * @author yinshubin
     */
    public boolean comparisonTraincode(String traincode, String trainno) {
        boolean result = false;
        if (trainno.contains("/")) {
            String[] trainnos = trainno.split("/");
            for (int i = 0; i < trainnos.length; i++) {
                result = traincode.equals(trainnos[i]);
                if (result) {
                    break;
                }
            }
        }
        else {
            result = traincode.equals(trainno);
        }
        return result;
    }

    /**
     * 说明:根据12306要求,将数据库中日期转变格式
     * @param date
     * @return date
     * @time 2014年8月30日 上午11:18:41
     * @author yinshubin
     */
    public String changeDate(String date) {
        try {
            Date date_result = new Date();
            DateFormat df = new SimpleDateFormat("yy-MM-dd HH:mm");
            date_result = df.parse(date);
            DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
            date = dfm.format(date_result);
        }
        catch (ParseException e1) {
            e1.printStackTrace();
        }
        return date;
    }

    /**
     * 说明:得到拒单原因 
     * @param state
     * @return
     * @time 2014年9月1日 下午2:50:26
     * @author yinshubin
     */
    public static String refusereason(int state) {
        if (state == 1) {
            return "所购买的车次坐席已无票";
        }
        else if (state == 2) {
            return "身份证件已经实名制购票,不能再次购买同日期同车次的车票";
        }
        else if (state == 3) {
            return "qunar票价和12306不符";
        }
        else {
            return "";
        }
    }

    /**
     * 说明:生成下单失败的解决办法
     * @param result
     * @param loginname
     * @param trainorder
     * @return
     * @time 2014年8月30日 上午11:42:56
     * @author yinshubin
     */
    public String saveSolution(String result, String loginname, Trainorder trainorder) {
        String solution = getSolution(result);
        Trainorderrc rc = new Trainorderrc();
        rc.setContent(loginname + "下单失败:" + solution);
        rc.setCreateuser(loginname);
        rc.setOrderid(trainorder.getId());
        rc.setYwtype(1);
        Server.getInstance().getTrainService().createTrainorderrc(rc);
        return "";
    }

    public static String getSolution(String result) {
        if ("下单成功,数据库同步失败".equals(result)) {
            return "请手动查询12306订单";
        }
        else if ("此车次无票".equals(result) || "已买过票了".equals(result)) {
            return "请打电话确认后拒单";
        }
        else {
            return "请手动下单";
        }
    }

    /**
     * 12306返回信息是否齐全
     * @param str
     * @return
     */
    public boolean isall(String str) {
        JSONObject jsono = JSONObject.fromObject(str);
        try {
            if (jsono.has("data")) {
                JSONObject jsonodata = jsono.getJSONObject("data");
                if (jsonodata.has("orderDBList")) {
                    JSONArray jsonaorderDBList = jsonodata.getJSONArray("orderDBList");
                    for (int j = 0; j < jsonaorderDBList.size(); j++) {// 获取所有订单
                        JSONObject jsonoorderDBList = jsonaorderDBList.getJSONObject(j);
                        if (jsonoorderDBList.has("tickets")) {
                            JSONArray tickets = jsonoorderDBList.getJSONArray("tickets");
                            if (tickets.size() == this.trainorder.getPassengers().size()) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            logger.error("isall", e.fillInStackTrace());
        }
        return false;
    }

    /**
     * 同程补单判断无座 
     * @return
     * @time 2015年1月18日 下午7:40:10
     * @author fiend
     */
    public boolean isHaveWZ() {
        //        Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(this.trainorder.getId());
        for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
            for (Trainticket trainticket : trainpassenger.getTraintickets()) {
                if ("无座".equals(trainticket.getSeatno())) {
                    return false;
                }
            }
        }
        return true;
    }
    //===========================================================工具模块END================================================
}
