package com.ccservice.inter.job.train.thread;

import java.util.Date;
import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.db.DBHelper;
import com.ccservice.Util.db.DBSourceEnum;
import com.ccservice.Util.db.DataRow;
import com.ccservice.Util.db.DataTable;
import com.ccservice.Util.file.ExceptionUtil;
import com.ccservice.Util.string.DesUtil;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.train.mqlistener.Method.MemCached;

public class TrainCreateOrderOtherMothed {

    private static DBSourceEnum dbSourceEnum = DBSourceEnum.TONGCHENG_DB;
    
    /**
     * 通过订单ID取第三方账号或Cookie
     */
    public static Customeruser getTrainAccountSrcById(long trainOrderid) {
        Customeruser customeruser = new Customeruser();
        String sql = "[sp_TrainAccountSrc_selectByOrderID] @OrderID = " + trainOrderid;
        DataTable resultset = null;
        try {
            resultset = DBHelper.GetDataTable(sql, dbSourceEnum);
        }
        catch (Exception e1) {
            e1.printStackTrace();
            ExceptionUtil.writelogByException("TrainCreateOrderOtherMothed_err", e1, "getTrainAccountSrcById>>>>>sql=["
                    + sql + "]>>>错误");
        }
        List<DataRow> dataRows = resultset.GetRow();
        if (dataRows.size() > 0) {
            DataRow DataRow = dataRows.get(dataRows.size() - 1);
            //密码
            String PassWord = DataRow.GetColumnString("PassWord");
            //解密
            try {
                PassWord = DesUtil.decrypt(PassWord, "A1B2C3D4E5F60708");
            }
            catch (Exception e) {
            }
            customeruser.setLogpassword(PassWord);
            customeruser.setCardnunber(DataRow.GetColumnString("Cookie"));
            customeruser.setLoginname(DataRow.GetColumnString("UserName"));
            customeruser.setPostalcode(DataRow.GetColumnString("IP12306"));
        }
        return customeruser;
    }

    /**
     * 去数据库查找身份冒用的乘客，将其set进内存
     */
    public static void setIdToMem(Trainorder trainorder) {
        try {
            List<Trainpassenger> passengerList = trainorder.getPassengers();
            String passengerListToStr = JSONObject.toJSONString(passengerList);
            WriteLog.write("查看是否冒用的乘客信息", trainorder.getId() + ":trainorder:" + JSONObject.toJSONString(trainorder));
            WriteLog.write("查看是否冒用的乘客信息", trainorder.getId() + ":乘客List:" + passengerListToStr);
            passengerList = trainorder.getPassengers();
            if (passengerList.size() > 0) {
                for (int i = 0; i < passengerList.size(); i++) {
                    Trainpassenger trainpassenger = passengerList.get(i);
                    if (trainpassenger.getIdentitystatusid() == 315) {
                        String idNumber = trainpassenger.getIdnumber();
                        MemCached.getInstance().add("falsely_" + idNumber, "1", new Date(1000 * 1 * 60 * 60 * 24));
                        WriteLog.write("将该身份证插入内存", ":身份证ID:" + idNumber + ":成功set进内存！");
                    }
                }
            }
        }
        catch (Exception e) {

        }
    }

}
