package com.ccservice.inter.job.train.thread;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import net.sf.json.JSONObject;

import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.base.sysconfig.Sysconfig;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.b2b2c.policy.TimeUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.JobGenerateOrder;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.server.Server;
import com.ccservice.train.mqlistener.MQMethod;
import com.ccservice.train.mqlistener.TrainActiveMQ;

/**
 * 
 * @author wzc
 * 支付宝交易号更新
 *
 */
public class MyThreadAlipayTrade extends Thread {

    TrainSupplyMethod trainSupplyMethod;

    private int paycounttime;//支付次数

    String dangqianagentid;//当前agentid

    String tongchengcallbackurl;//同程回调地址

    private long orderid;

    public MyThreadAlipayTrade(TrainSupplyMethod trainSupplyMethod, int paycounttime, String dangqianagentid,
            String tongchengcallbackurl, long orderid) {
        this.trainSupplyMethod = trainSupplyMethod;
        this.paycounttime = paycounttime;
        this.dangqianagentid = dangqianagentid;
        this.tongchengcallbackurl = tongchengcallbackurl;
        this.orderid = orderid;
    }

    @Override
    public void run() {
        int t = new Random().nextInt(10000);
        try {
            String id = orderid + "";
            Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(orderid);
            if (trainorder.getOrderstatus() == 3) {//如果订单状态为3不用在查
                WriteLog.write("12306支付宝交易号", t + ":" + trainorder.getOrdernumber() + ":已经出票");
            }
            else {
                int paycount = 0;
                try {
                    paycount = (int) trainorder.getTcprocedure();
                }
                catch (Exception e1) {
                }
                String supplyaccount = trainorder.getSupplyaccount();
                String ordernum = trainorder.getOrdernumber();
                String url = "";
                if (supplyaccount.contains("/")) {
                    String unaccount = "alipayurl" + supplyaccount.charAt(supplyaccount.length() - 1) + "";
                    url = getSysconfigString(unaccount);
                }
                String par = "{'ordernum':'" + ordernum + "','cmd':'seachliushuihao','paytype':'1'}";
                WriteLog.write("12306支付宝交易号", t + ":" + par);
                String infodata = SendPostandGet.submitPost(url, par, "UTF-8").toString();
                //支付成功
                //{"status":false,"info":"查询成功","AliTradeNo":"2015011000001000780044253446","AliOrderID":"0110665e7f6474e53fad368014706783",
                //"Amount":1.0,"orderstatus":"支付成功","lastruntime":"2015:01:10 20:05:44","PayRetUrl":"","PayRetParam":""}
                //支付失败
                //{"status":true,"info":"付款时候发送错误:支付操作超时","AliTradeNo":"2015011100001000460042583715",
                //"AliOrderID":"011132d9d41b167023a3267012468463","AliPayUserName":"hthyjk2@126.com","out_trade_no":"","Amount":1.0,"orderstatus":"控件创建","lastruntime":"2015:01:11 14:56:44","PayRetUrl":"","PayRetParam":""}
                WriteLog.write("12306支付宝交易号", t + ":" + infodata + ":" + supplyaccount);
                if (infodata != null && infodata.contains("orderstatus")) {
                    JSONObject obj = JSONObject.fromObject(infodata);
                    String liushuihao = obj.getString("AliTradeNo");
                    String state = "";
                    if (obj.containsKey("orderstatus")) {
                        state = obj.getString("orderstatus");
                    }
                    //查询失败成功信息
                    String msg = "";
                    if (obj.containsKey("info")) {
                        msg = obj.getString("info");
                    }
                    String AliPayUserName = "";
                    if (obj.containsKey("AliPayUserName")) {
                        AliPayUserName = obj.getString("AliPayUserName");//当前支付宝账号
                    }
                    float balance = -1;
                    String Banlancestr = "";
                    if (obj.containsKey("Banlance")) {
                        Banlancestr = obj.getString("Banlance");
                        if (Banlancestr != null && !"".equals(Banlancestr) && !"null".equals(Banlancestr)
                                && "支付成功".equals(state)) {
                            balance = Float.valueOf(Banlancestr);//当前账户余额
                        }
                    }
                    //解析银联参数成功  已经有交易号  必须返回RunOK才能确认已经支付
                    if (liushuihao != null && !"".equals(liushuihao) && liushuihao.length() > 0 && "支付成功".equals(state)) {
                        payclock(AliPayUserName, balance, t);
                        String sqlpaytime = "update T_TRAINREFINFO set C_PAYSUCCESSTIME='" + TimeUtil.gettodaydate(4)
                                + "' where C_ORDERID=" + trainorder.getId();
                        List list = Server.getInstance().getSystemService().findMapResultBySql(sqlpaytime, null);
                        WriteLog.write("12306支付宝交易号", t + ":" + sqlpaytime + ":" + liushuihao);
                        trainorder.setSupplytradeno(liushuihao);
                        trainorder.setAutounionpayurlsecond(AliPayUserName);//使用的银行卡号
                        trainorder.setState12306(Trainorder.ORDEREDPAYED);//更新12306订单为  已支付订单
                        trainorder.setPaysupplystatus(0);
                        trainRC(Long.valueOf(id), "支付成功");
                        Server.getInstance().getTrainService().updateTrainorder(trainorder);
                        WriteLog.write("12306支付宝交易号", t + ":" + trainorder.getOrdernumber() + ":" + msg);
                        new TrainActiveMQ(MQMethod.QUERY, trainorder.getId(), System.currentTimeMillis(),
                                System.currentTimeMillis(), 0).sendqueryMQ();
                        WriteLog.write("12306支付宝交易号", t + ":更新结束");
                    }
                    else if ("支付失败".equals(state) && getpaycontrol(msg)) {//运行错误
                        trainRC(Long.valueOf(id), msg);//保存运行错误信息
                        trainorder.setState12306(Trainorder.ORDEREDPAYFALSE);
                        trainorder.setIsquestionorder(Trainorder.PAYINGQUESTION);
                        Server.getInstance().getTrainService().updateTrainorder(trainorder);
                    }
                    else {
                        try {
                            String sqlpay = "select C_ORDERID,C_SUPPLYPAYTIME from T_TRAINREFINFO where C_ORDERID="
                                    + trainorder.getId();
                            List list = Server.getInstance().getSystemService().findMapResultBySql(sqlpay, null);
                            if (list != null && list.size() > 0) {
                                Map m = (Map) list.get(0);
                                String supplypaytime = m.get("C_SUPPLYPAYTIME").toString();
                                if (getTimeRelayFlag(supplypaytime)) {
                                    paycount++;
                                    trainRC(Long.valueOf(id), "250s未支付,支付需审核：" + paycount);//保存运行错误信息
                                    trainorder.setTcprocedure(paycount);
                                    if (paycount >= paycounttime) {//如果支付次数大于设置值
                                        trainorder.setState12306(Trainorder.ORDEREDPAYFALSE);
                                        trainorder.setIsquestionorder(Trainorder.PAYINGQUESTION);
                                    }
                                    else {
                                        trainorder.setState12306(Trainorder.ORDEREDWAITPAY);
                                    }
                                    Server.getInstance().getTrainService().updateTrainorder(trainorder);
                                }
                                else {//支付中状态
                                    trainorder.setState12306(Trainorder.ORDEREDPAYING);
                                    Server.getInstance().getTrainService().updateTrainorder(trainorder);
                                }
                            }
                            else {
                                paycount++;
                                trainRC(Long.valueOf(id), "未查到支付记录,支付需审核：" + paycount);//保存运行错误信息
                                trainorder.setTcprocedure(paycount);
                                if (paycount >= paycounttime) {//如果支付次数大于设置值
                                    trainorder.setState12306(Trainorder.ORDEREDPAYFALSE);
                                    trainorder.setIsquestionorder(Trainorder.PAYINGQUESTION);
                                }
                                else {
                                    trainorder.setState12306(Trainorder.ORDEREDWAITPAY);
                                }
                                Server.getInstance().getTrainService().updateTrainorder(trainorder);

                            }
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }
                        WriteLog.write("12306支付宝交易号", t + "：更新状态失败：" + ordernum);
                        String updatesql = "update T_TRAINORDER set C_STATE12306=5 where  C_STATE12306=8  and ID="
                                + trainorder.getId();
                        Server.getInstance().getSystemService().findMapResultBySql(updatesql, null);
                        WriteLog.write("12306支付宝交易号", t + "：更新订单审核8状态：" + updatesql);
                    }
                }
                else {
                    trainRC(Long.valueOf(id), "未查到支付记录,支付需审核");//保存运行错误信息
                    trainorder.setState12306(Trainorder.ORDEREDPAYFALSE);
                    trainorder.setIsquestionorder(Trainorder.PAYINGQUESTION);
                    Server.getInstance().getTrainService().updateTrainorder(trainorder);
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 根据返回信息是否确定支付失败
     * @param info
     * @return
     */
    public boolean getpaycontrol(String info) {
        if (info != null && !"".equals(info)) {
            if ("支付失败:CASHIER_ACCESS_GAP_CONTROL_TIP".equals(info)) {
                return false;
            }
            //            else if (info.contains("登录失效")) {
            //                return false;
            //            }
        }
        return true;
    }

    /**
     * 根据sysconfig的name获得value
     * 内存
     * @param name
     * @return
     */
    public String getSysconfigString(String name) {
        String result = "-1";
        try {
            if (Server.getInstance().getDateHashMap().get(name) == null) {
                List<Sysconfig> sysoconfigs = Server.getInstance().getSystemService()
                        .findAllSysconfig("WHERE C_NAME='" + name + "'", "", -1, 0);
                if (sysoconfigs.size() > 0) {
                    result = sysoconfigs.get(0).getValue();
                    Server.getInstance().getDateHashMap().put(name, result);
                }
            }
            else {
                result = Server.getInstance().getDateHashMap().get(name);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 易定行出票 
     * @param trainorder
     * @time 2014年12月26日 上午9:27:51
     * @author fiend
     */
    public void issueYEE(Trainorder trainorder) {
        new JobGenerateOrder().yeeIssue(trainorder.getId(), ordercustomeruser(trainorder));
    }

    /**
     * 通过订单查找下单的12306账号 
     * @param trainorder
     * @return
     * @time 2014年12月25日 下午9:33:36
     * @author fiend
     */
    public long ordercustomeruser(Trainorder trainorder) {
        String sql = "SELECT ID FROM T_CUSTOMERUSER WHERE C_LOGINNAME='" + trainorder.getSupplyaccount().split("/")[0]
                + "'";
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        Map map = (Map) list.get(0);
        return Long.valueOf(map.get("ID").toString());
    }

    /**
     * 根据sysconfig的name获得value
     * 实时 如果是判断的必须调用实时接口
     * @param name
     * @return
     */
    public String getSysconfigString_(String name) {
        String result = "-1";
        List<Sysconfig> sysoconfigs = Server.getInstance().getSystemService()
                .findAllSysconfig("WHERE C_NAME='" + name + "'", "", -1, 0);
        if (sysoconfigs.size() > 0) {
            result = sysoconfigs.get(0).getValue();
        }
        return result;
    }

    /**
     * 判断时间为两分钟
     * @return
     */
    public boolean getTimeRelayFlag(String t) {
        boolean flag = false;
        try {
            Date d1 = new Date();
            Date d2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(t);
            long diff = d1.getTime() - d2.getTime();
            long misecord = diff / 1000;
            if (misecord > 250) {
                flag = true;
            }
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        return flag;
    }

    /**
     * 
     * 
     * @param trainorder
     * @param msg 保存日志信息
     * @time 2014年12月26日 下午1:12:27
     * @author wzc
     */
    public void trainRC(long orerid, String msg) {
        Trainorderrc rc = new Trainorderrc();
        rc.setOrderid(orerid);
        rc.setContent(msg);
        rc.setCreateuser("自动支付");
        rc.setYwtype(1);
        Server.getInstance().getTrainService().createTrainorderrc(rc);
    }

    /**
     * 回调支付成功 
     * @param i
     * @param orderid
     * @return
     * @time 2014年12月12日 下午2:20:30
     * @author fiend
     */
    public String callBackTongChengPayed(int t, long orderid, String tongchengcallbackurl) {
        Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(orderid);
        //        String url = getSysconfigString_("tcTrainCallBack");
        String url = tongchengcallbackurl;
        JSONObject jso = new JSONObject();
        String result = "false";
        jso.put("orderid", trainorder.getQunarOrdernumber());
        jso.put("transactionid", trainorder.getTradeno());
        jso.put("method", "train_pay_callback");
        try {
            WriteLog.write("12306支付宝交易号", t + ":tongcheng_tongzhi_chupiao:" + url + ":" + jso.toString());
            long t1 = System.currentTimeMillis();
            result = SendPostandGet.submitPost(url, jso.toString(), "UTF-8").toString();
            long t2 = System.currentTimeMillis();
            WriteLog.write("12306支付宝交易号", t + ":返回:" + result + ":用时：" + (t2 - t1));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 
     * @param name
     * @return
     */
    public String getSystemConfigbyName(String name) {
        List<Sysconfig> configs = Server.getInstance().getSystemService()
                .findAllSysconfig(" where c_name='" + name + "'", "", -1, 0);
        if (configs != null && configs.size() == 1) {
            Sysconfig config = configs.get(0);
            return config.getValue();
        }
        return "";
    }

    /**
     * 支付预警
     */
    public void payclock(String AliPayUserName, Float balance, int t) {
        try {
            WriteLog.write("12306tz资金账户余额", t + ":" + AliPayUserName + ",余额：" + balance);
            if (balance >= 0) {
                String sqlmoney = "SELECT C_CLOCKMONEY,C_BANLANCE FROM T_PAYACCOUNTINFO WHERE C_ACCOUNT='"
                        + AliPayUserName + "'";
                List yuetixinglistt = Server.getInstance().getSystemService().findMapResultBySql(sqlmoney, null);
                float C_CLOCKMONEY = 20000;
                float C_BANLANCE = 0;
                float remainmoney = 0;
                if (yuetixinglistt.size() > 0) {
                    Map map = (Map) yuetixinglistt.get(0);
                    C_CLOCKMONEY = Float.valueOf(map.get("C_CLOCKMONEY").toString());
                    C_BANLANCE = Float.valueOf(map.get("C_BANLANCE").toString());
                    WriteLog.write("12306tz资金账户余额", t + ":支付前：" + C_BANLANCE + ",支付后：" + balance + ",提醒金额："
                            + C_CLOCKMONEY);
                    if (C_BANLANCE > C_CLOCKMONEY && balance < C_CLOCKMONEY) {
                        String content = AliPayUserName + "账户余额" + balance + ",请尽快充值。";
                        WriteLog.write("12306tz资金账户余额", t + ":" + content);
                        String tetctelphonenum = getSysconfigString_("payremandtel");
                        trainSupplyMethod.sendSmspublic(content, tetctelphonenum, 46);
                    }
                }
                else {
                    String content = "账户数据库没找到支付账号" + AliPayUserName;
                    WriteLog.write("12306tz资金账户余额", t + ":" + content);
                    trainSupplyMethod.sendSmspublic(content, "15811073432", 46);
                }
                String sql = "update T_PAYACCOUNTINFO set C_BANLANCE=" + balance + ",C_LASTUPDATETIME='"
                        + TimeUtil.gettodaydate(4) + "' where C_ACCOUNT='" + AliPayUserName + "'";
                Server.getInstance().getSystemService().findMapResultBySql(sql, null);
            }
        }
        catch (Exception e) {
        }
    }

    public TrainSupplyMethod getTrainSupplyMethod() {
        return trainSupplyMethod;
    }

    public void setTrainSupplyMethod(TrainSupplyMethod trainSupplyMethod) {
        this.trainSupplyMethod = trainSupplyMethod;
    }

    public int getPaycounttime() {
        return paycounttime;
    }

    public void setPaycounttime(int paycounttime) {
        this.paycounttime = paycounttime;
    }

    public String getDangqianagentid() {
        return dangqianagentid;
    }

    public void setDangqianagentid(String dangqianagentid) {
        this.dangqianagentid = dangqianagentid;
    }

    public String getTongchengcallbackurl() {
        return tongchengcallbackurl;
    }

    public void setTongchengcallbackurl(String tongchengcallbackurl) {
        this.tongchengcallbackurl = tongchengcallbackurl;
    }

    public long getOrderid() {
        return orderid;
    }

    public void setOrderid(long orderid) {
        this.orderid = orderid;
    }

}
