package com.ccservice.inter.job.train.thread;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.db.DBHelper;
import com.ccservice.Util.db.DataColumn;
import com.ccservice.Util.db.DataRow;
import com.ccservice.Util.db.DataTable;
import com.ccservice.Util.file.ExceptionUtil;
import com.ccservice.accountSwitchJob.AccountSwitch;
import com.ccservice.b2b2c.atom.service12306.AccountSystem;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.policy.ben.RepServerBean;
import com.ccservice.common.DurationEnum;
import com.ccservice.common.util.DurationEnumUtil;
import com.ccservice.common.util.DurationOperation;
import com.ccservice.control.TrainPropertyMessage;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.RepServerUtil;
import com.ccservice.inter.job.train.TrainInterfaceMethod;
import com.ccservice.inter.job.train.train12306.Train12306StationInfoUtil;
import com.ccservice.inter.job.train.train12306.TrainorderLishiMethod;
import com.ccservice.inter.server.Server;
import com.ccservice.train.mqlistener.Method.OcsMethod;
import com.ccservice.train.util.app.TrainAppSDK.Control;
import com.ccservice.train.util.app.TrainAppSDK.IFindMapFromDb;

/**
 * 作者:邹远超 日期:2014年9月2日
 */

public class TrainCreateOrder extends TrainCreateOrderSupplyMethod {

    public final static Logger logger = Logger.getLogger(TrainCreateOrder.class.getSimpleName());

    public TrainCreateOrder(long trainorderid) {
        this.trainorderid = trainorderid;
    }

    /**
     * 初始化
     * @time 2015年2月12日 上午11:55:34
     * @author fiend
     */
    public void initialization() {
        createTrainorderrc(trainorderid, "开始下单_init_start", "系统", 1);
        this.r1 = new Random().nextInt(1000000);
        // 当前系统 ID 0为qunar 1为同程
        // this.dangqianjiekouagentid =
        // Long.valueOf(getSysconfigString("dangqianjiekouagentid"));
        // long dangqianjiekouagentid = 1;
        // cn_interface地址
        this.cninterfaceurl = getSysconfigString("cninterfaceurl");
        WriteLog.write("JobGenerateOrder_MyThread", r1 + ":订单ID:" + this.trainorderid + ":debug:cninterfaceurl:"
                + cninterfaceurl);
        // 配置循环下单次数
        //this.ordermax = Integer.valueOf(getSysconfigString("ordermax"));
        //this.taobaoNoLoginMax = Integer.valueOf(getSysconfigString("taobaoNoLoginMax"));
        // 配置循环下单次数(错码、重复订单)
        //initOrderMaxErroe();
        // qunar代付接口地址
        this.qunarPayurl = getSysconfigString("qunarPayurl");
        WriteLog.write("JobGenerateOrder_MyThread", r1 + ":订单ID:" + this.trainorderid + ":debug:this.qunarPayurl:"
                + this.qunarPayurl);
        this.isjointrip = true;
        try {
            this.trainorder = Server.getInstance().getTrainService().findTrainorder(this.trainorderid);
        }
        catch (Exception e) {
        }
        WriteLog.write("JobGenerateOrder_MyThread_TrainOrder_JSONObject", r1 + ":订单ID:" + this.trainorderid + ":"
                + com.alibaba.fastjson.JSONObject.toJSONString(trainorder));
        if (this.trainorder != null) {
            WriteLog.write("JobGenerateOrder_MyThread", r1 + ":订单ID:" + this.trainorderid + ":debug:0");
            // 同程接口的callback地址
            // this.tcTrainCallBack = getSysconfigString("tcTrainCallBack");
            this.tcTrainCallBack = PropertyUtil.getValue("tcTrainCallBack", "train.properties");
            WriteLog.write("JobGenerateOrder_MyThread", r1 + ":订单ID:" + this.trainorderid + ":debug:1:"
                    + this.tcTrainCallBack);
            // 是否是其他接口
            // this.isotherjiekou = isOtherJiekou(this.trainorderid);
            try {
                this.interfacetype = this.trainorder.getInterfacetype() != null
                        && this.trainorder.getInterfacetype() > 0 ? this.trainorder.getInterfacetype() : Server
                        .getInstance().getInterfaceTypeService().getTrainInterfaceType(this.trainorderid);
            }
            catch (Exception e1) {
                e1.printStackTrace();
            }
            WriteLog.write("JobGenerateOrder_MyThread", r1 + ":订单ID:" + this.trainorderid + ":debug:interfacetype:"
                    + this.interfacetype);
            //            try {
            //                if (TrainInterfaceMethod.TAOBAO == this.interfacetype) {
            //                    try {
            //                        String create_order_timeout_time_str = getSysconfigString("create_order_timeout_time");
            //                        this.order_timeout_time = Long.valueOf(create_order_timeout_time_str) * 60 * 1000;
            //                    }
            //                    catch (Exception e) {
            //                        this.order_timeout_time = 15 * 60 * 1000L;
            //                    }
            //                }
            //            }
            //            catch (NumberFormatException e) {
            //                e.printStackTrace();
            //            }
            try {
                if (TrainInterfaceMethod.QUNAR == this.interfacetype && "kt".equals(default_pingtaiStr)) {
                    initTrainOrderExtSeat();
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            try {
                int isPhone = 1;
                long agentid = this.trainorder.getAgentid();
                try {
                    isPhone = isPhoneOrder(this.trainorderid) ? 2 : 1;
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                propertys = TrainPropertyMessage.getPropertys(isPhone, 1, agentid, interfacetype, orderType);
                ordermax = getIntValueFromJsonObject(propertys, "ordermax", 4);
                taobaoNoLoginMax = getIntValueFromJsonObject(propertys, "taobaoNoLoginMax", 10);
                WriteLog.write("JobGenerateOrder_MyThread", r1 + ":订单ID:" + this.trainorderid + ":debug:this.ordermax:"
                        + this.ordermax + ":taobaoNoLoginMax:" + this.taobaoNoLoginMax);
                ordermaxerroe = getIntValueFromJsonObject(propertys, "ordermaxerroe", 3);
                WriteLog.write("JobGenerateOrder_MyThread", r1 + ":订单ID:" + this.trainorderid
                        + ":debug:this.ordermaxerroe:" + this.ordermaxerroe);
                order_timeout_time = getLongValueFromJsonObject(propertys, "order_timeout_time", 15 * 60 * 1000L);
                WriteLog.write("JobGenerateOrder_MyThread", r1 + ":订单ID:" + this.trainorderid + ":order_timeout_time:"
                        + this.order_timeout_time);
                ordererrorcodemax = getIntValueFromJsonObject(propertys, "ordererrorcodemax", 2);
            }
            catch (Exception e) {
                WriteLog.write("JobGenerateOrder_MyThread_err", r1 + ":订单ID:" + this.trainorderid + ":给属性重新赋值的时候出错");
            }
            if (this.ordermax < 0) {
                this.ordermax = 4;
            }
            if (this.taobaoNoLoginMax < 0) {
                this.taobaoNoLoginMax = 10;
            }
            this.ordermaxbefore = this.ordermax;
            if (this.ordermaxerroe < 0) {
                this.ordermaxerroe = 3;
            }
            this.ordererrorcodenum = 0;
            // 配置循环下单次数(错码)
            // this.ordererrorcodemax = Integer.valueOf(getSysconfigString("ordererrorcodemax"));
            if (this.ordererrorcodemax < 0) {
                this.ordererrorcodemax = 2;
            }
            WriteLog.write("JobGenerateOrder_MyThread", r1 + ":订单ID:" + this.trainorderid + ":ordererrorcodemax:"
                    + this.ordererrorcodemax);
            initPropertys(propertys);
            createTrainorderrc(trainorderid, "开始下单_init_over", "系统", 1);
        }
        else {
            WriteLog.write("JobGenerateOrder_MyThread_err", r1 + ":订单ID:" + this.trainorderid + ":未找到该订单");
        }
        initBindingPassengers();
        createTrainorderrc(trainorderid, "开始下单_init_end", "系统", 1);
    }

    private void initPropertys(JSONObject propertys) {
        orderMaxQueryRetryPhone = getIntValueFromJsonObject(propertys, "orderMaxQueryRetryPhone",
                orderMaxQueryRetryPhone);
        loopAddSleepTime = getLongValueFromJsonObject(propertys, "loopAddSleepTime", loopAddSleepTime);
        loopAdd2Max = getIntValueFromJsonObject(propertys, "loopAdd2Max", loopAdd2Max);
        loopAdd2ChangeAccountFlag = getBooleanValueFromJsonObject(propertys, "loopAdd2ChangeAccountFlag",
                loopAdd2ChangeAccountFlag);
        loopAddSleepAddTime = getLongValueFromJsonObject(propertys, "loopAddSleepAddTime", loopAddSleepAddTime);
    }

    /**
     * 从json中得到long 值
     * @time:2017年7月27日下午6:01:28
     * @auto:baozz
     * @param propertys
     * @param key
     * @param defaultValue
     * @return
     */
    private boolean getBooleanValueFromJsonObject(JSONObject propertys, String key, boolean defaultValue) {
        try {
            return propertys.containsKey(key) ? propertys.getBooleanValue(key) : defaultValue;
        }
        catch (Exception e) {
            return defaultValue;
        }
    }

    /**
     * 从json中得到long 值
     * @time:2017年7月27日下午6:01:28
     * @auto:baozz
     * @param propertys
     * @param key
     * @param defaultValue
     * @return
     */
    private long getLongValueFromJsonObject(JSONObject propertys, String key, long defaultValue) {
        try {
            return propertys.containsKey(key) ? propertys.getIntValue(key) : defaultValue;
        }
        catch (Exception e) {
            return defaultValue;
        }
    }

    /**
     * 从json中得到int 值
     * @time:2017年7月27日下午6:01:28
     * @auto:baozz
     * @param propertys
     * @param key
     * @param defaultValue
     * @return
     */
    public int getIntValueFromJsonObject(com.alibaba.fastjson.JSONObject propertys, String key, int defaultValue) {
        try {
            return propertys.containsKey(key) ? propertys.getIntValue(key) : defaultValue;
        }
        catch (Exception e) {
            return defaultValue;
        }
    }

    /**
     * 开始创建订单
     * @param from3151CallBack 来源于同程3151回调
     * @param tongcheng3151Account 同程3151预约的账号，通过MQ在cn_interface传递过来的
     * @remark 注意事项：如果from3151CallBack为true，在未走原取账号逻辑前，如果return了，要释放账号[可调freeTongcheng3151Account()方法]！！！！！
     */
    public void createOrderStart(Customeruser tongcheng3151Account, boolean from3151CallBack) {
        boolean isOpenDuration = DurationEnumUtil.getIsOpenDuration();
        this.costTime.setFlag(isOpenDuration);
        DurationOperation durationOperation = new DurationOperation();
        durationOperation.init(this.costTime, this.trainorderid);
        initialization(); // 下单前对象初始化
        if (this.trainorder == null) {
            freeTongcheng3151Account(tongcheng3151Account, from3151CallBack);
            return;
        }
        if (this.trainorder.getState12306() == null || Trainorder.WAITORDER != this.trainorder.getState12306()) {
            freeTongcheng3151Account(tongcheng3151Account, from3151CallBack);
            WriteLog.write("重复进入下单队列", r1 + ":订单ID:" + trainorder.getId());
            return;
        }
        if (this.trainorder.getOrderstatus() > 2) {
            freeTongcheng3151Account(tongcheng3151Account, from3151CallBack);
            WriteLog.write("重复进入下单队列", r1 + ":订单ID:" + trainorder.getId());
            createTrainorderrc(this.trainorder.getId(), "订单状态不对", "createOrderStart", this.trainorder.getOrderstatus());
            return;
        }
        if (!TrainCreateOrderUtil.getNowTime(new Date())) {
            freeTongcheng3151Account(tongcheng3151Account, from3151CallBack);
            WriteLog.write("不符合下单时间", r1 + ":订单ID:" + trainorder.getId());
            reSendTraincreateOrdermessage();// 重新把消息扔进队列里等待下次消费
            return;
        }
        String sql = "UPDATE T_TRAINORDER SET C_STATE12306=2 WHERE ID =" + this.trainorderid;
        Server.getInstance().getSystemService().excuteGiftBySql(sql);// 获取账号成功以后才把订单状态改为正在下单
        String modifyresultMsg = isCanCreate();
        if (modifyresultMsg != null && !"-1".equals(modifyresultMsg)) {
            Customeruser customeruser = new Customeruser();
            customeruser.setLoginname("重复订单判断");
            refuse(trainorderid, 0, customeruser, modifyresultMsg);
            freeTongcheng3151Account(tongcheng3151Account, from3151CallBack);
            return;
        }
        //入口判断 是否允许 第三方下单 允许 直接进第三方下单队列 
        boolean isNeedSendThirdPartMQ = ThirdPartOrderUtil.isNeedSendThirdPartMQ(this.trainorder.getId(),
                this.trainorder.getAgentid());
        if (isNeedSendThirdPartMQ) {
            createTrainorderrc(trainorder.getId(), "进入第三方下单队列", "createOrderStart", trainorder.getOrderstatus());
            return;
        }
        boolean haveYuPiao = true;// getHaveYuPiao();
        if (!haveYuPiao) {//检测余票,没有余票不下单
            String content = "下单失败,余票不足";
            Customeruser user = new Customeruser();
            user.setLoginname("下单系统:余票不足");
            refuse(trainorder.getId(), 1, user, content);
            freeTongcheng3151Account(tongcheng3151Account, from3151CallBack);
            createTrainorderrc(trainorder.getId(), content, user.getLoginname(), Trainticket.ISSUED);
            return;
        }
        Long l1 = System.currentTimeMillis();
        WriteLog.write("JobGenerateOrder_MyThread", r1 + ":订单ID:" + trainorder.getId() + ":findtrainorderbyid_time:"
                + (System.currentTimeMillis() - l1));
        isbudan = isbudanorder();//判断是否是补单的订单
        //验证trainorder中的passenger是否有冒用，若发现冒用，将其set进MemCached
        TrainCreateOrderOtherMothed.setIdToMem(trainorder);
        this.costTime.add(DurationEnum.SAFETY_VERIFICATION);
        WriteLog.write("JobGenerateOrder_MyThread", r1 + ":订单ID:" + trainorder.getId() + ":开始获取账号:");
        //获取到下单使用的账号>>同程3151直接用回调账号
//       ---------------------- start  ---------------------
        //2018-04-23  新增开关，手机端切开关开启状态，则获取不需要登录的账号，否则逻辑不变  TODO
        //从内存获取开关状态
        WriteLog.write("手机端订单获取账号", trainorder.getId()+">>>开始判断");
        String way = new Control().orderWayByorderId(new IFindMapFromDb() {
            @Override
            public List findMapResultByProcedure(String paramString) {
                return findMapResult(paramString);
            }
        }, trainorder.getId(), "create");
        boolean phoneOrder = "2".equals(way)?true:false;
        WriteLog.write("手机端订单获取账号", trainorder.getId()+">>>是否是手机端订单"+phoneOrder);
        Customeruser customeruser ;
        if (phoneOrder && AccountSwitch.isAccountSwitch()) {
			//Customeruser customeruser
        	WriteLog.write("手机端订单获取账号", trainorder.getId()+">>>进入手机端获取账号");
        	Map<String, String> backup = new HashMap<>();
        	backup.put("AccountVirtualCookie", "true");
        	 customeruser = from3151CallBack ? tongcheng3151Account : Server.getInstance()
                    .getTrain12306Service().get12306Account(AccountSystem.OrderAccount, "", false, backup);
        	 WriteLog.write("手机端订单获取账号", trainorder.getId()+">>>手机获取得账号"+JSONObject.toJSONString(customeruser));
		}else{
			 WriteLog.write("手机端订单获取账号",trainorder.getId()+">>>web端获取账号");
			customeruser = from3151CallBack ? tongcheng3151Account : Server.getInstance()
                .getTrain12306Service().getcustomeruser(trainorder);
			WriteLog.write("手机端订单获取账号",trainorder.getId()+">>>web得到的账号"+JSONObject.toJSONString(customeruser));
		}
//        --------------end-------------
        this.costTime.add(DurationEnum.GET_ACCOUNT);
        WriteLog.write("JobGenerateOrder_MyThread",
                r1 + ":订单ID:" + trainorder.getId() + ":获取账号:" + (System.currentTimeMillis() - l1)
                        + ":获取账号:毫秒返回,当前user:" + JSONObject.toJSONString(customeruser));
        //账号可用于下单>>账号未释放、isenable符合下单>>此逻辑不可修改
        boolean isCanUse = customeruser != null && customeruser.getId() > 0 && !customeruser.isCanCreateOrder()
                && TrainCreateOrderUtil.isCanUseByIsenable(customeruser.getIsenable());
        //账号不可用于下单、账号未释放
        if (!isCanUse && customeruser != null && !customeruser.isCanCreateOrder()) {
            FreeNoCare(customeruser);
        }
        //绑定乘客并获取账号成功
        if (isCanUse) {
            createTrainorderrc(trainorder.getId(), "初始化账号成功", customeruser.getId() + "", Trainticket.WAITPAY);
            WriteLog.write("JobGenerateOrder_MyThread", r1 + ":订单ID:" + trainorder.getId() + ":获取12306账号成功:"
                    + customeruser.getLoginname());
            Customeruser cuser = customeruser;
            WriteLog.write("JobGenerateOrder_MyThread", r1 + ":订单ID:" + trainorder.getId() + ":获取12306账号成功:"
                    + customeruser.getLoginname() + ":" + JSONObject.toJSONString(trainorder));
            //可以下单
            if (TrainCreateOrderUtil.getIsCanOrderIng(trainorder)) {
                /**
                 * 注意：如果下单失败，释放账号逻辑写到acquisitionParameters()内部！！！
                 */
                //获取下单所用的参数 并下单到12306
                String result = acquisitionParameters(trainorder.getId(), cuser, cuser.getCardnunber(),
                        cuser.getLoginname());
                /**
                 * 淘宝价钱与12306不符合
                 */
                if (result.contains("请尽快支付")) {
                    WriteLog.write("JobGenerateOrder_MyThread", r1 + ":下单成功,当前电子单号:" + result.replace("请尽快支付", ""));
                    if (this.isjointrip) {//是否联程票 true 正常订单,false 联程订单
                        generateSuccess(trainorder.getId(), customeruser);
                    }
                    /*
                     * else{ refuse(trainorder.getId(), 1, cuser,
                     * "非普通票就拒单");//非普通票就拒单 }
                     */
                }
                else if (result.contains("数据库同步失败") || result.contains("12306订单信息不全")) {// 同步数据库失败或者是12306订单信息不全
                    trainorder.setIsquestionorder(Trainorder.ORDERINGQUESTION);
                    Server.getInstance().getTrainService().updateTrainorder(trainorder);
                    String content = "下单成功," + result;
                    if (result.contains("数据库同步失败")) {
                        content = "下单失败," + result;
                        refuse(trainorder.getId(), 1, cuser, "下单失败");//如果数据库同步失败就拒单
                    }
                    createTrainorderrc(trainorder.getId(), content, customeruser.getLoginname(), Trainticket.ISSUED);
                }
            }
            else {
                //释放账号
                FreeNoCare(customeruser);
                //操作记录
                createTrainorderrc(trainorder.getId(), "是否能下单:不能", "下单系统", trainorder.getOrderstatus());
            }
        }
        else if (JSONObject.toJSONString(customeruser).toString().contains("使用中")
                && "falsely".equals(OcsMethod.getInstance().get(
                        "falsely_" + this.trainorder.getPassengers().get(0).getIdnumber()))) {
            Isfalsely(customeruser, trainorder.getAgentid());
        }
        //客人账号不重试登录[如：途牛托管]
        else if (customeruser != null && customeruser.isDontRetryLogin()) {
            WriteLog.write("JobGenerateOrder_MyThread",
                    r1 + ":订单ID:" + trainorder.getId() + ";12306账号:" + customeruser.getLoginname()
                            + ":refuse:准备拒单:12306原因:" + customeruser.getNationality()
                            + ":customeruser.isDontRetryLogin():" + customeruser.isDontRetryLogin());
            refuse(trainorder.getId(), 1, customeruser, customeruser.getNationality());
        }
        else if (customeruser != null && (customeruser.getId() > 0 && customeruser.getIsenable() == 0)
                || (customeruser.getId() <= 0 && customeruser.getDescription() != null)) {
            WriteLog.write("JobGenerateOrder_MyThread", r1 + ":订单ID:" + trainorder.getId() + ";log001:"
                    + com.alibaba.fastjson.JSONObject.toJSONString(customeruser));
            if (customeruser.getLoginname() == null) {
                customeruser.setLoginname("冒用");
            }
            try {
                if (customeruser.getDescription().contains("身份信息涉嫌被他人冒用")) {
                    com.alibaba.fastjson.JSONObject jsonObject_maoyong = getJsonObject_maoyong(customeruser
                            .getDescription());
                    unmatchedpasslist.add(jsonObject_maoyong);
                    customeruser.setDescription(customeruser.getDescription().split("冒用，")[0] + "冒用，未通过身份核验，"
                            + customeruser.getDescription().split("冒用，")[1]);
                }
            }
            catch (Exception e) {
            }
            if (customeruser.getId() > 0) {
                WriteLog.write("JobGenerateOrder_MyThread", r1 + ":订单ID:" + trainorder.getId() + ":身份验证失败:"
                        + customeruser.getDescription());
            }
            else {
                WriteLog.write("JobGenerateOrder_MyThread", r1 + ":订单ID:" + trainorder.getId() + ":身份验证失败:冒用乘客获取账号失败:"
                        + customeruser.getDescription());
            }
            if (customeruser.getDescription() != null && "12306系统繁忙".equals(customeruser.getDescription())) {
                refuse(trainorder.getId(), 1, customeruser, customeruser.getDescription());
            }
            else if (customeruser.getDescription() != null && customeruser.getDescription().contains("身份信息涉嫌被他人冒用")) {
                refuse(trainorder.getId(), 1, customeruser, customeruser.getDescription());
            }
            else if (customeruser.getDescription() != null
                    && (customeruser.getDescription().contains("用户12306账号登录失败") || customeruser.getDescription()
                            .contains("已满"))
                    && (this.trainorder.getOrdertype() == 3 || this.trainorder.getOrdertype() == 4)) {
                refuse(trainorder.getId(), 1, customeruser, customeruser.getDescription());
            }
            else {
                refuse(trainorder.getId(), 1, customeruser, "身份验证失败:" + customeruser.getDescription());
            }
        }
        else {
            com.alibaba.fastjson.JSONObject jsonObject_fastjson = com.alibaba.fastjson.JSONObject
                    .parseObject(customeruser.getNationality());
            if (jsonObject_fastjson != null && jsonObject_fastjson.getString("error") != null) {
                String error = jsonObject_fastjson.getString("error");
                Customeruser cusre = new Customeruser();
                cusre.setLoginname("第三方账号");
                refuse(trainorderid, 1, cusre, error);
                //                callBackTongChengOrdered(trainorder, error);
            }
            else {
                String sql_state1 = "UPDATE T_TRAINORDER SET C_STATE12306=1 WHERE ID =" + this.trainorderid;
                Server.getInstance().getSystemService().excuteGiftBySql(sql_state1);// 获取账号成功以后才把订单状态改为正在下单
                reSendTraincreateOrdermessage();// 重新把消息扔进队列里等待下次消费
            }
        }
        durationOperation.saveDB(this.costTime, this.trainorderid);
        WriteLog.write("JobGenerateOrder_MyThread",
                r1 + ":订单ID:" + trainorder.getId() + ":下单结束:总耗时:" + (System.currentTimeMillis() - l1));
        removeCanCreate();
    }

    /**
     * 判断是否有余票
     * @return true 有余票 false 无余票
     * @time 2016年1月25日 下午3:23:55
     * @author chendong
     */
    private boolean getHaveYuPiao() {
        boolean haveYuPiao = true;
        //只有是代购的才走这个逻辑
        if (this.orderType == 1) {
            long orderid = trainorder.getId();
            try {
                Trainticket trainticket = trainorder.getPassengers().get(0).getTraintickets().get(0);
                String fromcity = trainticket.getDeparture();
                fromcity = Train12306StationInfoUtil.getThreeByName(fromcity);
                String tocity = trainticket.getArrival();
                tocity = Train12306StationInfoUtil.getThreeByName(tocity);
                String date = trainticket.getDeparttime().substring(0, 10);
                String traincode = trainticket.getTrainno();
                String seattype = trainticket.getSeattype();
                //METHOD
                TrainorderLishiMethod method = new TrainorderLishiMethod();
                //同程线上>>循环1次
                for (int i = 0; i < 1; i++) {
                    //取REP
                    RepServerBean rep = new RepServerBean();
                    //异常捕捉
                    try {
                        //取REP
                        rep = RepServerUtil.dontNeedLoginRepServer();
                        //REP地址
                        String repUrl = rep.getUrl();
                        //校验余票
                        haveYuPiao = method.checkYuPiao(orderid, fromcity, tocity, date, repUrl, traincode, seattype);
                        //存在余票
                        if (haveYuPiao) {
                            break;
                        }
                        //没有余票
                        else {
                            logger.error(i + ":" + haveYuPiao + ":" + orderid + ":" + fromcity + ":" + tocity + ":"
                                    + date + ":" + repUrl + ":" + traincode + ":" + seattype);
                        }
                    }
                    catch (Exception e) {

                    }
                    //释放REP
                    finally {
                        RepServerUtil.freeRepServer(rep);
                    }
                }
            }
            catch (Exception e) {
                logger.error(haveYuPiao + ":" + orderid + ":" + e.fillInStackTrace());
            }
        }
        return haveYuPiao;
    }

    /**
     * 释放同程3151账号
     * @author WH
     * @time 2016年6月23日 上午10:00:30
     * @version 1.0
     * @param user 3151回调的账号
     */
    private void freeTongcheng3151Account(Customeruser tongcheng3151Account, boolean from3151CallBack) {
        //来源于3151且账号未释放
        if (from3151CallBack && tongcheng3151Account != null && !tongcheng3151Account.isCanCreateOrder()) {
            FreeNoCare(tongcheng3151Account);
        }
    }
  
    //返回list<map> 格式的 数据 根据存储过程
    public static List<Map<String, Object>> findMapResult(String sql) {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        try {
            DataTable dataTable = DBHelper.GetDataTable(sql);
            if (dataTable.GetRow().size() > 0) {
                for (DataRow row : dataTable.GetRow()) {
                    list.add(transmutationMap(row));
                }
            }
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("TrainCreateOrder_findMapResultByProcedure_Exception", e,
                    "执行的sql:" + sql);
        }
        return list;
    }
    
    private static Map<String, Object> transmutationMap(DataRow row) {
        Map<String, Object> map = new HashMap<String, Object>();
        List<DataColumn> column = row.GetColumn();
        for (DataColumn d : column) {
            map.put(d.GetKey(), d.GetValue());
        }
        return map;
    }
}