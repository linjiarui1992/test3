package com.ccservice.inter.job.train.thread;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;


import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.policy.SendPostandGet;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;
import com.ccservice.b2b2c.atom.service12306.AccountSystem;
import com.ccservice.b2b2c.atom.service12306.bean.TrainOrderReturnBean;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.policy.ben.RepServerBean;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.Account12306Util;
import com.ccservice.inter.job.train.RepServerUtil;
import com.ccservice.inter.server.Server;
import com.ccservice.qunar.util.ExceptionUtil;
import com.ccservice.train.mqlistener.Method.KongTieRabbtMQMethod;

/**
 * 订单走排队机制
 */
public class TrainCreateOrderPaiDui extends TrainCreateOrderSupplyMethod {

    public static void main(String[] args) {
        String paiduiResult = "<h3>未出票，订单排队中...</h3><p id=\"wait_time_minute\">最新预估等待时间为<span><strong>05</strong>分钟</span>，请耐心等待";
        int waitTime = Integer.valueOf(paiduiResult.split("最新预估等待时间为<span><strong>")[1].split("</strong>分钟</span>")[0]) * 60;
        System.out.println(waitTime);
        //        TrainCreateOrderPaiDui trainCreateOrderPaiDui = new TrainCreateOrderPaiDui(48766774);
        //        trainCreateOrderPaiDui.startPaiduiResult(1);
    }

    //登录名
    String loginname;

    //历时
    String Runtime;

    /**
     * 检测了多少次了排队结果
     */
    int CheckCount;

    /**
    * 
    * @param trainorderid
    * @param cookieString
    * @param loginname
    * @param passengers  陈洪亮|1|130826199312318630|1|硬座|235
    */
    public TrainCreateOrderPaiDui(long trainorderid) {
        this.trainorderid = trainorderid;

    }

    String TrainOrderPaiduiDataId;//TrainOrderPaiduiData表的id

    /**
     * 
     * @time 2015年12月11日 下午12:45:53
     * @author chendong
     */
    @SuppressWarnings("rawtypes")
    private void initData() {
        this.isjointrip = true;
        this.userqu = new Customeruser();
        try {
            this.trainorder = Server.getInstance().getTrainService().findTrainorder(this.trainorderid);
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("TrainCreateOrderPaiDui_initData_Exception", e, "订单号:"+trainorderid);
        }
        this.interfacetype = this.trainorder.getInterfacetype() != null && this.trainorder.getInterfacetype() > 0 ? this.trainorder
                .getInterfacetype() : Server.getInstance().getInterfaceTypeService()
                .getTrainInterfaceType(this.trainorderid);
        this.passengers = getPassengersString(this.trainorder);
        this.r1 = (int) this.trainorderid;
        try {
            queuesleeptime = Long.valueOf(getSysconfigProtected("queuesleeptime", queuesleeptime + ""));
        }
        catch (NumberFormatException e1) {
        }
        try {
            queuesleeptimemax = Long.valueOf(getSysconfigProtected("queuesleeptimemax", queuesleeptimemax + ""));
        }
        catch (NumberFormatException e1) {
        }

        String sql = " sp_TrainOrderPaiduiData_selectByTrainOrderid @TrainOrderId=" + this.trainorder.getId();
        List list = Server.getInstance().getSystemService().findMapResultByProcedure(sql);
        if (list.size() > 0) {
            Map map = (Map) list.get(0);
            this.loginname = map.get("Loginname") == null ? "" : map.get("Loginname").toString();
            this.TrainOrderPaiduiDataId = map.get("Id") == null ? "" : map.get("Id").toString();
            this.Runtime = map.get("Runtime") == null ? "" : map.get("Runtime").toString();
            this.userqu.setLoginname(loginname);
        }
        //名称初始化
        if (ElongHotelInterfaceUtil.StringIsNull(loginname)) {
            this.loginname = trainorder.getSupplyaccount();
        }
        //账号初始化
        this.trainorder.setSupplyaccount(loginname);
        //配置初始化
        this.tcTrainCallBack = PropertyUtil.getValue("tcTrainCallBack", "train.properties");
    }

    //重复获取12306订单列表睡眠时间
    long queuesleeptime = 20000l;

    //重复获取12306订单列表最大执行时间
    long queuesleeptimemax = 10 * 60 * 1000l;

    /**
     * 开始获取排队结果
     * 
     * @time 2015年12月11日 下午3:18:48
     * @author chendong
     * @param type  type 2:前5-6分钟的订单;1前2-3分钟的订单 3:前8-9分钟的订单;
     */
    public void startPaiduiResult(boolean isLastPaidui) {
        long l1 = System.currentTimeMillis();
        System.out.println(this.trainorderid + ":开始排队");
        initData();//初始化排队需要的信息
        checkTrainOrder();//判断是否信息是否相等
        TrainCreateOrderPaiDuiUtil.mapuser.put(this.trainorderid, this.trainorder);//将信息存入Map
        try {
            String falg = PropertyUtil.getValue("isMeituanSeatOrderLastPaiDui", "train.properties");
            if ("1".equals(falg)&&trainorder != null && trainorder.getInterfacetype() == 3 && trainorder.getAgentid() == 67) {
                isLastPaidui = true;
                WriteLog.write("TrainCreateOrderPaiDui_startPaiduiResult",
                        "meituan before seat order is not line up , orderId:" + trainorderid);
            }
        } catch (Exception e) {
            ExceptionUtil.writelogByException("TrainCreateOrderPaiDui_startPaiduiResult_Exception", e,
                    "美团先占座订单不去排队异常:" + trainorderid);
        }
        if (this.trainorder.getState12306() == 18) {
            //订单账号名称
            this.trainorder.setSupplyaccount(loginname);
            //初始化账号
            this.userqu = getCustomerUserBy12306Account(this.trainorder, false);
            //保存操作记录
            createTrainorderrc(this.trainorderid, l1 + ":开始检测排队结果", "排队系统", 1);
            //获取当前时间long
            long nowtime = System.currentTimeMillis();
            WriteLog.write("TrainCreateOrderPaiDui", this.trainorderid + ":" + this.userqu.getLoginname());
            //获取12306订单列表
            String jsonmsg = "";//获取 12306 的订单状态
            if (new TrainCreateOrderCancelTc().isCancel(default_pingtaiStr, trainorder, userqu)) {
                //取消排队的方法 等待写!,遇到排队的时候抓取排队的链接并实现 已完成
                TrainCreateOrderUtil.cancelPaiduiOrder(trainorder, userqu);
                //isCancel()已经取消及释放一次账号
                boolean cancelAndFreed = GoCancelOrder(trainorder, userqu);
                //已经释放REP
                if (cancelAndFreed) {
                    userqu.setMemberemail("");//REP信息置空，不同时释放
                }
                //释放次数>>保存排队数据时没有释放账号
                int freeCount = cancelAndFreed ? AccountSystem.OneFree : AccountSystem.TwoFree;
                //释放账号
                freeCustomeruser(userqu, AccountSystem.FreeNoCare, freeCount, AccountSystem.ZeroCancel,
                        AccountSystem.NullDepartTime);
                //中断返回
                return;
            }

            jsonmsg = get12306Orderinfo(nowtime, this.userqu, this.passengers);
            WriteLog.write("TrainCreateOrderPaiDui", this.trainorderid + "--->jsonmsg--->" + jsonmsg);
            //        if (trainorder.getOrdertimeout().getTime() - System.currentTimeMillis() < 10 * 60 * 1000) {
            //            freeCustomeruser(userqu, AccountSystem.FreeNoCare, AccountSystem.OneFree, AccountSystem.OneCancel,
            //                    AccountSystem.NullDepartTime);
            //            refuse(trainorder.getId(), 1, userqu, "提交订单失败：没有足够的票!");//如果数据库同步失败就拒单
            //            return;
            //        }
            if (jsonmsg.contains("无未支付订单") && isLastPaidui) {
                paiduiRefue(l1, "无未支付订单");
            }
            else if (jsonmsg.contains("没有足够的票")) {
                //                createTrainorderrc(this.trainorderid, l1 + ":没有足够的票", "排队系统", 1);
                paiduiRefue(l1, "没有足够的票");
            }
            else if (jsonmsg.contains("出票失败了")) {
                String failResult = jsonmsg.substring(jsonmsg.indexOf("<span>") + 6, jsonmsg.indexOf("</span>"));
                //                createTrainorderrc(this.trainorderid, l1 + ":" + failResult, "排队系统", 1);
                paiduiRefue(l1, failResult);
            }
            //51133574:i:0:jsonmsg:{"validateMessagesShowId":"_validatorMessage","status":true,"httpstatus":200,"data":{"orderCacheDTO":{"requestId":6090307382416635812,"userId":6500275845028,"number":50,"tourFlag":"dc","requestTime":"2016-01-06 09:07:12","queueOffset":984464785,"queueName":"25_ORDER_SL1_0","trainDate":"2016-01-06 00:00:00","startTime":"1970-01-01 13:55:00","stationTrainCode":"D5092","fromStationCode":"EAY","fromStationName":"西安北","toStationCode":"YWY","toStationName":"延安","status":3,"message":{"message":"您尚有订单未支付，请完成支付后再订票！！","code":"0"},"modifyTime":"2016-01-06 09:07:59","tickets":[{"seatTypeName":"二等座","seatTypeCode":"O","ticketTypeName":"成人票","passengerName":"杜广国","passengerIdTypeName":"二代身份证"}],"waitTime":-2,"waitCount":0,"ticketCount":1,"startTimeString":"2016-01-06 13:55","array_passser_name_page":["杜广国"]},"orderDBList":[{"sequence_no":"E336969178","order_date":"2016-01-06 09:05:24","tick
            else if (jsonmsg.contains("您尚有订单未支付，请完成支付后再订票")) {//如果是这种情况重新扔队列去下单
                updateTrainOrderStatusByTrainOrderid();
                createTrainorderrc(this.trainorderid, l1 + ":[您尚有订单未支付，请完成支付后再订票],重新扔队列去下单", "排队系统", 1);
                //释放账号
                freeCustomeruser(userqu, AccountSystem.FreeNoCare, AccountSystem.TwoFree, AccountSystem.ZeroCancel,
                        AccountSystem.NullDepartTime);
                //重新下单
//                String activeMQ_url = PropertyUtil.getValue("activeMQ_url", "train.TrainCreateOrder.properties");
                new KongTieRabbtMQMethod().activeMQroordering(this.trainorderid);
//                    ActiveMQUtil.sendMessage(activeMQ_url, "QueueMQ_trainorder_waitorder_orderid", this.trainorderid
//                            + "");
            }
            else {
                //12306返回信息是否完全
                String isall = TrainCreateOrderUtil.isallReturnString(this.trainorder, jsonmsg,
                        this.trainorder.getPassengers(), true,trainorder.getIsLC()==1);
                if (isall.startsWith("true")) {
                    createTrainorderrc(this.trainorderid, l1 + ":排队成功", "排队系统", 1);
                    WriteLog.write("历时_排队成功", "OrderId:" + this.trainorder.getId() + " ;Runtime:" + this.Runtime);
                    //满足订单需求开始出票 -排队成功:将自动下单成功的订单信息存入数据库、
                    TrainOrderReturnBean returnobQueue = new TrainOrderReturnBean();
                    returnobQueue.setRuntime(this.Runtime);
                    String resultString = saveOrderInformation(jsonmsg, trainorder.getId(), loginname, this.isjointrip,
                            returnobQueue, userqu);
                    //占座成功
                    if (resultString.contains("请尽快支付")) {
                        WriteLog.write("TrainCreateOrderPaiDui", this.trainorderid + ":" + this.queueticket
                                + "--->排队成功");
                        WriteLog.write("TrainCreateOrderPaiDui", this.trainorderid + ":" + ":下单成功,当前电子单号:"
                                + resultString.replace("请尽快支付", ""));
                        if (this.isjointrip) {//是否联程票 true 正常订单,false 联程订单
                            generateSuccess(trainorder.getId(), userqu);
                        }
                    }
                    else if (resultString.contains("下单成功,数据库同步失败")) {
                        trainorder.setIsquestionorder(Trainorder.ORDERINGQUESTION);
                        Server.getInstance().getTrainService().updateTrainorder(trainorder);
                        String content = "下单失败," + resultString;
                        refuse(trainorder.getId(), 1, userqu, "下单失败");//如果数据库同步失败就拒单
                        createTrainorderrc(trainorder.getId(), content, userqu.getLoginname(), Trainticket.ISSUED);
                    }
                    //保存排队数据前已释放过REP，REP信息置空，不同时释放
                    userqu.setMemberemail("");
                    //释放排队前账号
                    FreeNoCare(userqu);
                }
                else {
                    if (isLastPaidui) {//是否是最后一次检测
                        paiduiRefue(l1, "排队失败");
                    }
                    else {//不是最后一次重新排队
                        WriteLog.write("TrainCreateOrderPaiDui", this.trainorderid + "--->isAll--->" + isall);
                        //释放账号
                        FreeNoCare(userqu);
                        continuePaidui(isall, true, l1, true);//继续下一次排队
                    }
                }
                //        else {
                //            WriteLog.write("TrainCreateOrderPaiDui", this.trainorderid + ":--->" + this.queueticket + "--->超过次数,拒单");
                //            freeCustomeruser(userqu, AccountSystem.FreeNoCare, AccountSystem.OneFree, AccountSystem.OneCancel,
                //                    AccountSystem.NullDepartTime);
                //            refuse(trainorder.getId(), 1, userqu, "提交订单失败：没有足够的票!");//如果数据库同步失败就拒单
                //        }
            }
        }
        else {
            continuePaidui("State12306 不对", false, l1, false);
        }
        TrainCreateOrderPaiDuiUtil.mapuser.remove(trainorderid);
    }

    /**
     * 
     * @time 2016年1月13日 下午8:34:50
     * @author chendong
     */
    private void checkTrainOrder() {
        int count = 0;
        while (true) {
            if (TrainCreateOrderPaiDuiUtil.mapuser.get(trainorderid) == null) {//如果信息为空证明，信息不重复
                if (count > 0) {
                    this.trainorder = Server.getInstance().getTrainService().findTrainorder(this.trainorderid);
                }
                break;
            }
            else {//否则重复信息等十秒
                count++;
                WriteLog.write("TrainCreateOrderPaiDui", this.trainorderid + "--->：订单在循环里循环了--->" + count + "次");
                if (count == 60) {//循环60次还有重复先停止循环记录日志
                    WriteLog.write("TrainCreateOrderPaiDui", this.trainorderid + "--->：订单在循环里循环了--->" + count + "次"
                            + "强制停止");
                    break;
                }
                try {
                    Thread.sleep(10000);//如果信息一样停十秒
                }
                catch (Exception e) {

                }
            }
        }
    }

    /**
     * 继续下一次排队
     * @time 2015年12月13日 上午8:30:36
     * @author chendong
     * @param isall  本次失败原因
     * @param isContinuePaidui  是否继续排队
     * @param l1 
     * @param isUpdate  是否还原状态
     */
    private void continuePaidui(String isall, boolean isContinuePaidui, long l1, boolean isUpdate) {
        if (isUpdate) {
            updateTrainOrderStatus();//排队失败还原状态
        }
        String content = "排队失败:订单信息不全," + isall + ":等待下次检测";
        if (isContinuePaidui) {
        }
        else {
            content = "排队失败:" + isall + ":暂停排队";
        }
        createTrainorderrc(this.trainorderid, l1 + ":" + content, "排队系统", 1);
    }

    /**
     * 排队失败还原状态
     * @time 2016年1月4日 下午12:19:31
     * @author chendong
     */
    public void updateTrainOrderStatus() {
        String sql = "update TrainOrderPaiduiData set CheckStatus=0 where id=" + TrainOrderPaiduiDataId;
        for (int i = 0; i < 5; i++) {
            int sqlCount = Server.getInstance().getSystemService().excuteAdvertisementBySql(sql);
            WriteLog.write("TrainCreateOrderPaiDui", this.trainorderid + ":" + i + ":次:sqlCount:" + sqlCount + ":sql:"
                    + sql);
            if (sqlCount > 0) {//大于零更新成功否则更新失败继续执行5次
                break;
            }
            else {
                try {
                    Thread.sleep(1000L);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
                continue;
            }
        }
    }

    /**
     * 订单状态不对的话就拒单
     * @time 2015年12月11日 下午8:41:40
     * @author chendong
     * @param string 
     */
    private void paiduiRefue(long l1, String string) {
        boolean cancelAndFreed = false;
        //取消订单
        if (!new TrainCreateOrderCancelTc().isCancel(default_pingtaiStr, trainorder, userqu)) {
            createTrainorderrc(this.trainorderid, l1 + ":" + string, "排队系统", 1);
            WriteLog.write("TrainCreateOrderPaiDui", this.trainorderid + ":--->" + this.queueticket + "--->超过次数,拒单:"
                    + string);
            //错误原因
            if (!(string.contains("已订") || string.contains("已购买") || string.contains("行程冲突"))) {
                string = "提交订单失败:12306排队!";
            }
            //拒单操作
            refuse(trainorder.getId(), 1, userqu, string);
            //取消排队
            TrainCreateOrderUtil.cancelPaiduiOrder(trainorder, userqu);
        }
        //isCancel()已经取消及释放一次账号
        else {
            cancelAndFreed = GoCancelOrder(trainorder, userqu);
        }
        //已经释放REP
        if (cancelAndFreed) {
            userqu.setMemberemail("");//REP信息置空，不同时释放
        }
        //释放次数>>保存排队数据时没有释放账号
        int freeCount = cancelAndFreed ? AccountSystem.OneFree : AccountSystem.TwoFree;
        //释放账号
        freeCustomeruser(userqu, AccountSystem.FreeNoCare, freeCount, AccountSystem.ZeroCancel,
                AccountSystem.NullDepartTime);
    }

    /**
     * 获取12306的排队订单的信息
     * @return
     * @time 2015年12月11日 下午12:47:41
     * @author chendong
     * @param nowtime 
     */
    public String get12306Orderinfo(long nowtime, Customeruser userqu, String passengers) {
        //        String jsonmsg = getstr(this.repUrl, cookieString, this.passengers);//获取一次12306的订单状态
        WriteLog.write("TrainCreateOrderPaiDui", this.trainorderid + ":" + ":cardnunber:" + userqu.getCardnunber()
                + ":passengers:" + passengers);
        String jsonmsg = "";//获取一次12306的订单状态
        for (int i = 0; i < 5; i++) {
            jsonmsg = getstr(userqu, passengers);
            WriteLog.write("TrainCreateOrderPaiDui", this.trainorderid + ":i:" + i + ":jsonmsg:" + jsonmsg + ":");
            if ("".equals(jsonmsg) || "{}".equals(jsonmsg)) {//如果获取到的结果是空的换rep重试
                continue;
            }
            else if (Account12306Util.accountNoLogin(jsonmsg, userqu)) {
                //以未登录释放
                FreeNoLogin(userqu);
                //订单账号名称
                this.trainorder.setSupplyaccount(loginname);
                //重拿账号
                userqu = getCustomerUserBy12306Account(this.trainorder, true);
                //账号重置
                this.userqu = userqu;
                //重拿数据
                createTrainorderrc(this.trainorderid, "用户未登录", "排队系统", 1);
                //中断当前
                continue;
            }
            else {
                break;
            }
        }

        WriteLog.write("TrainCreateOrderPaiDui", this.trainorderid + ":" + ":cardnunber:" + userqu.getCardnunber()
                + ":passengers:" + passengers);
        //        while (true) {//这个不要了如果排队失败的话重新还原状态扔队列重新下单
        //            //是否满足订单需求
        //            boolean isAll = TrainCreateOrderUtil.isall(this.trainorder.getId(), jsonmsg,
        //                    this.trainorder.getPassengers());
        //            if (!isAll) {
        //                //            if (!isall1(jsonmsg)) {
        //                //重复获取12306列表时间是否超过规定时间
        //                if (System.currentTimeMillis() - nowtime > queuesleeptimemax
        //                        || trainorder.getOrdertimeout().getTime() - System.currentTimeMillis() < 10 * 60 * 1000) {
        //                    break;
        //                }
        //                try {
        //                    Thread.sleep(queuesleeptime);
        //                }
        //                catch (InterruptedException e) {
        //                    e.printStackTrace();
        //                }
        //                //获取12306订单列表
        //                jsonmsg = getstr(this.repUrl, userqu.getCardnunber(), this.passengers);
        //            }
        //            else {
        //                break;
        //            }
        //        }
        return jsonmsg;
    }

    private String getSysconfigProtected(String key, String protectedString) {
        String dbString = getSysconfigStringbydb(key);
        if (!"-1".equals(dbString)) {
            protectedString = dbString;
        }
        return protectedString;
    }

    /**
     * 修饰表
     * 
     * @param oldmsg
     * @param newmsg
     * @param orderid
     * @param type
     * @time 2015年12月11日 下午12:31:01
     * @author chendong
     */
    @SuppressWarnings("unused")
    private void createModifyMsg(String oldmsg, String newmsg, long orderid, int type) {
        try {
            Timestamp createtime = new Timestamp(System.currentTimeMillis());
            String sql = "INSERT INTO TrainOrderModifyMsg "
                    + "([OldMsg] ,[NewMsg] ,[OrderId] ,[CreateTime] ,[Type] ,[Remark])" + "VALUES " + " ('" + oldmsg
                    + "','" + newmsg + "'," + orderid + ",'" + createtime + "'," + type + ",''  )";
            WriteLog.write("TrainCreateOrder_createModifyMsg", sql);
            Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 
     * @time 2016年1月6日 上午11:35:28
     * @author chendong
     */
    private void updateTrainOrderStatusByTrainOrderid() {
        try {
            //修改12306状态为排队状态
            String sql = "UPDATE T_TRAINORDER SET c_state12306=1 WHERE ID =" + this.trainorderid;
            WriteLog.write("JobGenerateOrder_MyThread_paidui", r1 + ":update:重新下单改状态:sql:" + sql);
            int count = Server.getInstance().getSystemService().excuteAdvertisementBySql(sql);
            WriteLog.write("JobGenerateOrder_MyThread_paidui", r1 + ":update:count:" + count);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 取消12306订单公共方法
     */
    public String cancel12306Order(Customeruser user, String extnumber, long trainorderid) {
        //结果
        String result = "";
        //处理
        try {
            //Cookie
            String cookie = user.getCardnunber();
            //非空
            if (!ElongHotelInterfaceUtil.StringIsNull(extnumber) && !ElongHotelInterfaceUtil.StringIsNull(cookie)) {
                //REP
                RepServerBean rep = RepServerUtil.getRepServer(user, false);
                //参数
                String param = "datatypeflag=10&cookie=" + cookie + "&extnumber=" + extnumber + "&trainorderid="
                        + trainorderid + JoinCommonAccountInfo(user, rep);
                //请求
                result = SendPostandGet.submitPost(rep.getUrl(), param, "UTF-8").toString();
                //记录日志
                WriteLog.write("TrainCreateOrderSupplyMethod_rep10Method_Paidui", trainorderid + "-->" + rep.getUrl()
                        + "-->" + result);
                //未登录
                if (result.contains("用户未登录") && RepServerUtil.changeRepServer(user)) {
                    //切换REP
                    rep = RepServerUtil.getTaoBaoTuoGuanRepServer(user, false);
                    //类型正确
                    if (rep.getType() == 1) {
                        //重拼参数
                        param = "datatypeflag=10&cookie=" + cookie + "&extnumber=" + extnumber + "&trainorderid="
                                + trainorderid + JoinCommonAccountInfo(user, rep);
                        //重新请求
                        result = SendPostandGet.submitPost(rep.getUrl(), param, "UTF-8").toString();
                        //记录日志
                        WriteLog.write("TrainCreateOrderSupplyMethod_rep10Method_paidui",
                                trainorderid + "-->" + rep.getUrl() + "-->" + result);
                    }
                }
            }
        }
        catch (Exception e) {
        }
        //返回结果
        return result;
    }

    /**
     * 取消12306订单公共方法
     */
    @SuppressWarnings("unused")
    private String cancel12306OrderPhone(Customeruser user, String extnumber, long trainorderid) {
        //结果
        String result = "";
        //处理
        try {
            //Cookie
            String cookie = user.getCardnunber();
            //非空
            if (!ElongHotelInterfaceUtil.StringIsNull(extnumber)) {
                //REP
                RepServerBean rep = RepServerUtil.getRepServer(user, false);
                //参数
                String param = "datatypeflag=1010&cookie=" + cookie + "&extnumber=" + extnumber + "&trainorderid="
                        + trainorderid + JoinCommonAccountInfo(user, rep) + JoinCommonAccountPhone(user);
                //请求
                result = SendPostandGet.submitPost(rep.getUrl(), param, "UTF-8").toString();
                //记录日志
                WriteLog.write("TrainCreateOrderSupplyMethod_rep1010Method_paidui", trainorderid + "-->" + rep.getUrl()
                        + "-->" + result);
                //取消订单成功，一天只能取消3次
                if (result.contains("取消订单成功")) {
                    result = "取消订单成功，一天只能取消3次";
                }
                WriteLog.write("TrainCreateOrderSupplyMethod_rep1010Method_paidui", trainorderid + "-->" + result);
            }
        }
        catch (Exception e) {
        }
        //返回结果
        return result;
    }

    /**
     * 将排队结果更新到DB
     * 
     * @param orderId
     * @param jsonmsg
     * @time 2016年4月19日 下午12:11:54
     * @author fiend
     */
    private void freshQueueResult(long orderId, String jsonmsg) {
        try {
            JSONObject jsonObject = JSONObject.parseObject(jsonmsg);
            String paiduiResult = jsonObject.containsKey("paiduiResult") ? jsonObject.getString("paiduiResult") : "";
            if (!ElongHotelInterfaceUtil.StringIsNull(paiduiResult) && paiduiResult.contains("最新预估等待时间为<span><strong>")
                    && paiduiResult.contains("</strong>分钟</span>")) {
                int waitTime = Integer.valueOf(paiduiResult.split("最新预估等待时间为<span><strong>")[1]
                        .split("</strong>分钟</span>")[0]) * 60;
                if (waitTime > 0) {
                    String freshQueueSql = " [sp_TrainOrderQueue_Update] @OrderId=" + orderId + " ,@WaitTime="
                            + waitTime + " ,@Type=1";
                    WriteLog.write("将排队数据刷新DB", orderId + "--->" + freshQueueSql);
                    Server.getInstance().getSystemService().findMapResultByProcedure(freshQueueSql);
                }
            }
        }
        catch (Exception e) {
            WriteLog.write("将排队数据刷新DB_ERROR", orderId + "");
            ExceptionUtil.writelogByException("将排队数据刷新DB_ERROR", e);
        }

    }

    /**
     * 将排队信息删除
     * 
     * @param orderId
     * @param jsonmsg
     * @time 2016年4月19日 下午12:11:54
     * @author fiend
     */
    private void deleteQueueResult(long orderId) {
        try {
            String deleteQueueSql = " [sp_TrainOrderQueue_Delete] @OrderId=" + orderId + " ,@Type=1";
            WriteLog.write("将排队数据删除DB", orderId + "--->" + deleteQueueSql);
            Server.getInstance().getSystemService().findMapResultByProcedure(deleteQueueSql);
        }
        catch (Exception e) {
            WriteLog.write("将排队数据删除DB_ERROR", orderId + "");
            ExceptionUtil.writelogByException("将排队数据删除DB_ERROR", e);
        }

    }
}
