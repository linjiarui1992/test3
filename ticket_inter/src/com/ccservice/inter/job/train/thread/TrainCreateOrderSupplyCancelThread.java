package com.ccservice.inter.job.train.thread;

import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;
import com.ccservice.b2b2c.atom.service12306.AccountSystem;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.b2b2c.policy.SendPostandGet;
import com.ccservice.b2b2c.policy.ben.RepServerBean;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.*;

public class TrainCreateOrderSupplyCancelThread extends Thread implements Runnable {

    Customeruser user;

    String extnumber;

    long trainorderid;

    Trainorder trainorder;

    TrainSupplyMethod TrainSupplyMethod = new TrainSupplyMethod();

    public TrainCreateOrderSupplyCancelThread(Customeruser user, String extnumber, long trainorderid,
            Trainorder trainorder) {

        this.user = user;
        this.extnumber = extnumber;
        this.trainorderid = trainorderid;
        this.trainorder = trainorder;
    }

    /**
    * 说明：取消订单失败 线程  取消订单循环三次
    * @param user
    * @param extnumber
    * @param trainorderid
    * @param trainorder
    * @return
    * @time 2016年4月14日 下午15:30
    * @author zhulixu
    */

    @Override
    public void run() {

        //结果
        String result = "";
        //处理
        try {
            //Cookie
            String cookie = user.getCardnunber();
            //非空
            if (!ElongHotelInterfaceUtil.StringIsNull(extnumber) && !ElongHotelInterfaceUtil.StringIsNull(cookie)) {
                for (int i = 1; i > 0; i++) {
                    //REP
                    RepServerBean rep = RepServerUtil.getRepServer(user, false);
                    //参数
                    String param = "datatypeflag=10&cookie=" + cookie + "&extnumber=" + extnumber + "&trainorderid="
                            + trainorderid + TrainSupplyMethod.JoinCommonAccountInfo(user, rep);
                    //请求
                    result = SendPostandGet.submitPost(rep.getUrl(), param, "UTF-8").toString();
                    //记录日志
                    WriteLog.write("TrainCreateOrderSupplyMethod_rep10Method", trainorderid + "-->" + rep.getUrl()
                            + "-->" + result);
                    //未登录
                    if (result.contains("用户未登录")) {

                        Customeruser userget = new Customeruser();
                        //释放账号
                        TrainSupplyMethod.FreeNoLogin(userget);
                        //重新找回账号
                        userget = TrainSupplyMethod.getCustomerUserBy12306Account(trainorder, false);

                        //重拼参数
                        param = "datatypeflag=10&cookie=" + cookie + "&extnumber=" + extnumber + "&trainorderid="
                                + trainorderid + TrainSupplyMethod.JoinCommonAccountInfo(user, rep);
                        //重新请求
                        result = SendPostandGet.submitPost(rep.getUrl(), param, "UTF-8").toString();
                        //记录日志
                        WriteLog.write("TrainCreateOrderSupplyMethod_rep10Method", trainorderid + "-->" + rep.getUrl()
                                + "-->" + result);

                    }
                    if (result.contains("取消订单成功")) {
                        if (this.trainorder.getOrdertype() != 3 && this.trainorder.getOrdertype() != 4) {
                            Trainorder trainorderelse = new Trainorder();
                            trainorderelse.setId(this.trainorderid);
                            trainorderelse.setSupplyaccount(user.getLoginname());
                            trainorderelse.setOrdertype(this.trainorder.getOrdertype());
                            trainorderelse.setAgentid(this.trainorder.getAgentid());
                            trainorderelse.setInterfacetype(this.trainorder.getInterfacetype());
                            trainorderelse.setQunarOrdernumber(this.trainorder.getQunarOrdernumber());
                            Customeruser userget = TrainSupplyMethod.getCustomerUserBy12306Account(trainorderelse,
                                    false);
                            TrainSupplyMethod.freeCustomeruser(userget, AccountSystem.FreeNoCare,
                                    AccountSystem.OneFree, AccountSystem.OneCancel, AccountSystem.NullDepartTime);

                        }
                        break;
                    }
                    if (result.contains("取消订单失败") || i == 3) {
                        WriteLog.write("取消订单失败", "订单号" + trainorderid);
                        Trainorderrc rc = new Trainorderrc();
                        rc.setContent("取消订单失败，请手动取消" + trainorderid);
                        break;
                    }
                }
            }
        }
        catch (Exception e) {
        }

    }

}
