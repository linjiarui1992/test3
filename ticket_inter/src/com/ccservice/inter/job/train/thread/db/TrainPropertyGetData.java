package com.ccservice.inter.job.train.thread.db;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ccservice.bean.TrainPropertyBean;
import com.ccservice.inter.server.Server;
import com.ccservice.qunar.util.ExceptionUtil;

public class TrainPropertyGetData {
    public List<TrainPropertyBean> getTrainPropertyBeans(String sql) {
        List<TrainPropertyBean> propertyBeans = new ArrayList<TrainPropertyBean>();
        List<Map<String, Object>> rows = null;
        try {
            rows = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
            if (rows != null) {
                for (Map<String, Object> map : rows) {
                    if (map.get("notuse") == null) {
                        String propertyName = (String) map.get("PropertyName");
                        String propertyValue = (String) map.get("PropertyValue");
                        int agentId = (int) map.get("AgentId");
                        int interfaceType = (int) map.get("InterfaceType");
                        int orderType = (int) map.get("OrderType");
                        String remark = (String) map.get("Remark");
                        int propertyType = (int) map.get("PropertyType");
                        int businessType = (int) map.get("BusinessType");
                        int pathwayType = (int) map.get("PathwayType");
                        Timestamp propertyUpdateTime = (Timestamp) map.get("PropertyUpdateTime");
                        String groupName = (String)map.get("GroupName");
                        TrainPropertyBean trainPropertyBean = new TrainPropertyBean(propertyName, propertyValue, agentId, interfaceType,
                                orderType, remark, propertyUpdateTime.toString(), propertyType, businessType,
                                pathwayType,groupName);
                        propertyBeans.add(trainPropertyBean);
                    } 
                }
            }
        }
        catch (Exception e) {
           ExceptionUtil.writelogByException("TrainPropertyMessageJob_getDBMessage_Exception",e, "查询的sql是:" + sql + ",得到的结果是:" + rows);
            propertyBeans = new ArrayList<TrainPropertyBean>();
        }
        return propertyBeans;
    }
}
