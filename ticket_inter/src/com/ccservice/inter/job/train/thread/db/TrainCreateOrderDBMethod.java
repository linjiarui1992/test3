package com.ccservice.inter.job.train.thread.db;

import java.util.ArrayList;
import java.util.List;

import com.ccservice.Util.db.DataRow;
import com.ccservice.b2b2c.base.train.TrainStudentInfo;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;

/**
 * 用于减少dbUtil 的行数
 * 
 * @author baozz
 *
 */
public class TrainCreateOrderDBMethod {
    /**
     * @describe 填充火车票的 票信息
     * @author baozz
     * @time 2017年12月27日下午3:04:59
     */
    public void paddingTicket(DataRow row, List<Trainticket> traintickets, Trainticket trainticket) {
        // 票id
        trainticket.setId(row.GetColumnLong("TID"));
        // 关联乘客id
        trainticket.setTrainpid(row.GetColumnLong("C_TRAINPID"));
        trainticket.setInsurenum(row.GetColumnInt("C_INSURENUM"));
        trainticket.setDeparttime(row.GetColumnString("C_DEPARTTIME"));
        trainticket.setDeparture(row.GetColumnString("C_DEPARTURE"));
        trainticket.setArrival(row.GetColumnString("C_ARRIVAL"));
        trainticket.setArrivaltime(row.GetColumnString("C_ARRIVALTIME"));
        trainticket.setTrainno(row.GetColumnString("C_TRAINNO"));
        trainticket.setTickettype(row.GetColumnInt("C_TICKETTYPE"));
        trainticket.setSeattype(row.GetColumnString("C_SEATTYPE"));
        trainticket.setSeatno(row.GetColumnString("C_SEATNO"));
        trainticket.setCoach(row.GetColumnString("C_COACH"));
        trainticket.setPrice(row.GetColumnFloat("C_PRICE"));
        trainticket.setPayprice(row.GetColumnFloat("C_PAYPRICE"));
        trainticket.setStatus(row.GetColumnInt("C_STATUS"));
        trainticket.setCosttime(row.GetColumnString("C_COSTTIME"));
        trainticket.setInsureno(row.GetColumnString("C_INSURENO"));
        trainticket.setInsurinprice(row.GetColumnFloat("C_INSURINPRICE"));
        trainticket.setInsurprice(row.GetColumnFloat("C_INSURPRICE"));
        trainticket.setInsurorigprice(row.GetColumnFloat("C_INSURORIGPRICE"));
        trainticket.setProcedure(row.GetColumnFloat("C_PROCEDURE"));
        trainticket.setTctrainno(row.GetColumnString("C_TCTRAINNO"));
        trainticket.setTccoach(row.GetColumnString("C_TCCOACH"));
        trainticket.setTcseatno(row.GetColumnString("C_TCSEATNO"));
        trainticket.setTtcseattype(row.GetColumnString("C_TTCSEATTYPE"));
        trainticket.setTtcdeparttime(row.GetColumnString("C_TTCDEPARTTIME"));
        trainticket.setRefundfailreason(row.GetColumnInt("C_REFUNDFAILREASON"));
        trainticket.setSeq(row.GetColumnInt("C_SEQ"));
        trainticket.setRefundsuccesstime(row.GetColumnString("C_REFUNDSUCCESSTIME"));
        trainticket.setTicketno(row.GetColumnString("C_TICKETNO"));
        trainticket.setTcticketno(row.GetColumnString("C_TCTICKETNO"));
        trainticket.setChangeid(row.GetColumnInt("C_CHANGEID"));
        trainticket.setChangeType(row.GetColumnInt("C_CHANGETYPE"));
        trainticket.setTcnewprice(row.GetColumnFloat("C_TCNEWPRICE"));
        trainticket.setTcProcedure(row.GetColumnFloat("C_TCPROCEDURE"));
        trainticket.setTcPrice(row.GetColumnFloat("C_TCPRICE"));
        trainticket.setRefundRequestTime(row.GetColumnString("C_REFUNDREQUESTTIME"));
        trainticket.setRefundType(row.GetColumnInt("C_REFUNDTYPE"));
        trainticket.setIsQuestionTicket(row.GetColumnInt("C_ISQUESTIONTICKET"));
        trainticket.setIsapplyticket(row.GetColumnInt("C_ISAPPLYTICKET"));
        trainticket.setInterfaceticketno(row.GetColumnString("C_INTERFACETICKETNO"));
        trainticket.setChangeRequestTime(row.GetColumnString("C_CHANGEREQUESTTIME"));
        trainticket.setRefundTimeStamp(row.GetColumnString("C_REFUNDTIMESTAMP"));
        trainticket.setChangeTimeStamp(row.GetColumnString("C_CHANGETIMESTAMP"));
        trainticket.setRealinsureno(row.GetColumnString("C_REALINSURENO"));
        trainticket.setChangeProcedure(row.GetColumnFloat("C_CHANGEPROCEDURE"));
        trainticket.setTsArrival(row.GetColumnString("C_TSARRIVAL"));
        trainticket.setOldOutTicketDetail(row.GetColumnString("C_OLDOUTTICKETDETAIL"));
        trainticket.setNewOutTicketDetail(row.GetColumnString("C_NEWOUTTICKETDETAIL"));
        trainticket.setRefundPriceQuestion(row.GetColumnInt("C_REFUNDPRICEQUESTION"));
    }

    /**
     * @describe 填充火车票表的学生信息
     * @author baozz
     * @time 2017年12月27日下午3:04:29
     */
    public void paddingStudent(DataRow row, TrainStudentInfo trainStudentInfo) {
        trainStudentInfo.setId(row.GetColumnLong("SIID"));
        trainStudentInfo.setTrainpid(row.GetColumnLong("Trainpid"));
        trainStudentInfo.setStudentcard(row.GetColumnString("StudentCard"));
        trainStudentInfo.setClasses(row.GetColumnString("Classes"));
        trainStudentInfo.setDepartment(row.GetColumnString("Department"));
        trainStudentInfo.setEductionalsystem(row.GetColumnString("EductionalSystem"));
        trainStudentInfo.setEntranceyear(row.GetColumnString("entranceyear"));
        trainStudentInfo.setFromcity(row.GetColumnString("FromCity"));
        trainStudentInfo.setSchoolname(row.GetColumnString("SchoolName"));
        trainStudentInfo.setSchoolprovince(row.GetColumnString("SchoolProvince"));
        trainStudentInfo.setStudentno(row.GetColumnString("StudentNo"));
        trainStudentInfo.setTocity(row.GetColumnString("ToCity"));
        trainStudentInfo.setSchoolnamecode(row.GetColumnString("SchoolNameCode"));
        trainStudentInfo.setSchoolprovincecode(row.GetColumnString("SchoolProvinceCode"));
        trainStudentInfo.setFromcitycode(row.GetColumnString("FromCityCode"));
        trainStudentInfo.setTocitycode(row.GetColumnString("ToCityCode"));
        trainStudentInfo.setArg1(row.GetColumnLong("Arg1"));
        trainStudentInfo.setArg2(row.GetColumnString("Arg2"));
        trainStudentInfo.setArg3(row.GetColumnLong("Arg3"));
    }

    /**
     * @describe 填充火车票 表的乘客信息
     * @author baozz
     * @time 2017年12月27日下午3:03:59
     */
    public void paddingPassenger(DataRow row, Trainpassenger trainpassenger) {
        trainpassenger.setOrderid(row.GetColumnLong("C_ORDERID"));
        // 乘客主键id
        trainpassenger.setId(row.GetColumnInt("PID"));
        // 乘客名称
        trainpassenger.setName(row.GetColumnString("C_NAME"));
        // 乘客身份证类型
        trainpassenger.setIdtype(row.GetColumnInt("C_IDTYPE"));
        // 乘客身份证号
        trainpassenger.setIdnumber(row.GetColumnString("C_IDNUMBER"));
        // 乘客生日
        trainpassenger.setBirthday(row.GetColumnString("C_BIRTHDAY"));
        trainpassenger.setAduitstatus(row.GetColumnInt("C_ADUITSTATUS"));
        trainpassenger.setPassengerid(row.GetColumnString("C_PASSENGERID"));
    }

    /**
     * @describe 填充火车票表的订单信息
     * @author baozz
     * @time 2017年12月27日下午3:03:15
     */
    public void paddingOrder(Trainorder trainorder, DataRow row) {
        trainorder.setId(row.GetColumnLong("OID"));
        // 自己家的订单号
        trainorder.setOrdernumber(row.GetColumnString("C_ORDERNUMBER"));
        // 订单创建时间
        trainorder.setCreatetime(row.GetColumnTime("C_CREATETIME"));
        // 订单状态
        trainorder.setOrderstatus(row.GetColumnInt("C_ORDERSTATUS"));
        //
        trainorder.setTradeno(row.GetColumnString("C_TRADENO"));
        // 采购商id
        trainorder.setAgentid(row.GetColumnInt("C_AGENTID"));
        //
        trainorder.setCreateuser(row.GetColumnString("C_CREATEUSER"));
        trainorder.setContactuser(row.GetColumnString("C_CONTACTUSER"));
        trainorder.setPaymethod(row.GetColumnInt("C_PAYMETHOD"));
        // 12306订单号
        trainorder.setExtnumber(row.GetColumnString("C_EXTNUMBER"));
        trainorder.setOperateuid(row.GetColumnLong("C_OPERATEUID"));
        trainorder.setContacttel(row.GetColumnString("C_CONTACTTEL"));
        trainorder.setCommission(row.GetColumnFloat("C_COMMISSION"));
        trainorder.setAgentcontact(row.GetColumnString("C_AGENTCONTACT"));
        trainorder.setAgentcontacttel(row.GetColumnString("C_AGENTCONTACTTEL"));
        trainorder.setOrderprice(row.GetColumnFloat("C_TOTALPRICE"));
        trainorder.setAgentprofit(row.GetColumnFloat("C_AGENTPROFIT"));
        trainorder.setRefuseaffirm(row.GetColumnInt("C_REFUSEAFFIRM"));
        trainorder.setNote(row.GetColumnString("C_NOTE"));
        trainorder.setSupplyprice(row.GetColumnFloat("C_SUPPLYPRICE"));
        trainorder.setSupplypayway(row.GetColumnInt("C_SUPPLYPAYWAY"));
        trainorder.setSupplyaccount(row.GetColumnString("C_SUPPLYACCOUNT"));
        trainorder.setSupplytradeno(row.GetColumnString("C_SUPPLYTRADENO"));
        trainorder.setTcprocedure(row.GetColumnFloat("C_TCPROCEDURE"));
        // 采购商的接口订单号
        trainorder.setQunarOrdernumber(row.GetColumnString("C_QUNARORDERNUMBER"));
        trainorder.setRefundreason(row.GetColumnInt("C_REFUNDREASON"));
        trainorder.setIsjointtrip(row.GetColumnInt("C_ISJOINTTRIP"));
        trainorder.setPaysupplystatus(row.GetColumnInt("C_PAYSUPPLYSTATUS"));
        trainorder.setChangesupplyprice(row.GetColumnFloat("C_CHANGESUPPLYPRICE"));
        trainorder.setChangesupplypayway(row.GetColumnInt("C_CHANGESUPPLYPAYWAY"));
        trainorder.setChangesupplyaccount(row.GetColumnString("C_CHANGESUPPLYACCOUNT"));
        trainorder.setChangesupplytradeno(row.GetColumnString("C_CHANGESUPPLYTRADENO"));
        trainorder.setIsquestionorder(row.GetColumnInt("C_ISQUESTIONORDER"));
        trainorder.setChangerefundprice(row.GetColumnFloat("C_CHANGEREFUNDPRICE"));
        trainorder.setAutounionpayurl(row.GetColumnString("C_AUTOUNIONPAYURL"));
        trainorder.setIschangerefundorder(row.GetColumnInt("C_ISCHANGEREFUNDORDER"));
        // 12306订单状态
        trainorder.setState12306(row.GetColumnInt("C_STATE12306"));
        trainorder.setUser12306id(row.GetColumnInt("C_12306USERID"));
        // 订单接口类型
        trainorder.setInterfacetype(row.GetColumnInt("C_INTERFACETYPE"));
        trainorder.setTaobaosendid(row.GetColumnString("C_TAOBAOSENDID"));
        trainorder.setInsureadreess(row.GetColumnString("C_INSUREADREESS"));
        trainorder.setIsplaceing(row.GetColumnInt("C_ISPLACEING"));
        trainorder.setEnrefundable(row.GetColumnInt("C_ENREFUNDABLE"));
        trainorder.setChupiaoagentid(row.GetColumnInt("ChuPiaoAgentId"));
        trainorder.setChupiaotime(row.GetColumnTime("ChuPiaoTime"));
        // 订单类型
        trainorder.setOrdertype(row.GetColumnInt("ordertype"));
        // 订单超时时间
        trainorder.setOrdertimeout(row.GetColumnTime("C_ORDERTIMEOUT"));
        trainorder.setControlname(row.GetColumnString("C_CONTROLNAME"));
        trainorder.setAutounionpayurlsecond(row.GetColumnString("C_AUTOUNIONPAYURLSECOND"));
        // 是否是12306联程票 0 不是 1 是
        trainorder.setIsLC(row.GetColumnInt("C_isLC"));
    }

    /**
     * @describe 判断是否存在已经添加进去的学生
     * @author baozz
     * @time 2017年12月27日上午11:18:43
     */
    public boolean isNotExistTrainStudents(DataRow row, List<TrainStudentInfo> trainStudentInfos) {
        boolean isNotExis = true;
        Long siId = row.GetColumnLong("SIID");
        for (TrainStudentInfo trainStudentInfo : trainStudentInfos) {
            if (trainStudentInfo.getId() == siId) {
                isNotExis = false;
                break;
            }
        }
        return isNotExis;
    }

    /**
     * @describe 判断是否存在已经添加进去的火车票
     * @author baozz
     * @time 2017年12月27日上午11:18:43
     */
    public boolean isNotExistTrainTickets(DataRow row, List<Trainticket> traintickets) {
        boolean isNotExis = true;
        Long tid = row.GetColumnLong("TID");
        for (Trainticket trainticket : traintickets) {
            if (trainticket.getId() == tid) {
                isNotExis = false;
                break;
            }
        }
        return isNotExis;
    }

    /**
     * @describe 判断是否存在已经添加进去的乘客
     * @author baozz
     * @time 2017年12月27日上午11:18:07
     */
    public boolean isNotExistTrainPassengers(DataRow row, List<Trainpassenger> trainpassengers) {
        boolean isNotExis = true;
        Long pid = row.GetColumnLong("PID");
        Long trainpid = row.GetColumnLong("C_TRAINPID");
        for (Trainpassenger trainpassenger : trainpassengers) {
            if (trainpassenger.getId() == pid && trainpid == pid) {
                isNotExis = false;
                break;
            }
        }
        return isNotExis;
    }

    /**
     * @author Baozz
     * @time 2018年1月26日上午10:55:08
     * @describe 判断是否需要第二次创建乘客 true 为不需要 false 为需要
     */

    public boolean isSamePassenger(DataRow row, List<Trainpassenger> passengers) {
        Long pid = row.GetColumnLong("PID");
        boolean falg = true;
        for (Trainpassenger trainpassenger : passengers) {
            if (trainpassenger.getId() != pid) {
                falg = false;
            }
            else {
                falg = true;
            }
        }
        return falg;
    }

    public List<Trainpassenger> isgroupPassengers(List<Trainpassenger> passengers, DataRow row) {
        if (passengers == null) {
            return null;
        }
        Long PID = row.GetColumnLong("PID");
        List<Trainpassenger> trainpassengers = new ArrayList<Trainpassenger>();
        for (Trainpassenger trainpassenger : passengers) {
            if (trainpassenger.getId() == PID) {
                trainpassengers.add(trainpassenger);
            }
        }
        if (trainpassengers.isEmpty()) {
            return null;
        }
        else {
            return trainpassengers;
        }
    }

    public Trainpassenger getTrainPassengerByPid(DataRow row, List<Trainpassenger> passengers) {
        Long PID = row.GetColumnLong("PID");
        for (Trainpassenger trainpassenger : passengers) {
            if (trainpassenger.getId() == PID) {
                return trainpassenger;
            }
        }
        return new Trainpassenger();
    }
}
