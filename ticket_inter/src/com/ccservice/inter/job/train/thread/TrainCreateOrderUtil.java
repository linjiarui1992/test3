/**
 *
 */
package com.ccservice.inter.job.train.thread;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.b2b2c.policy.ben.RepServerBean;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.Util.CallBackPassengerUtil;
import com.ccservice.inter.job.train.RepServerUtil;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.job.train.thread.beanEnum.TrainSeatTypeEnum;
import com.ccservice.qunar.util.ExceptionUtil;
import com.ccservice.train.mqlistener.Method.OcsMethod;

/**
 * 取消排队的方法 等待写!,遇到排队的时候抓取排队的链接并实现
 * @time 2015年10月20日 下午4:45:30
 * @author chendong
 */
public class TrainCreateOrderUtil {

    /**
     * 取消排队的方法
     * @param order
     * @time 2015年10月20日 下午4:45:36
     * @author chendong
     */
    public static String cancelPaiduiOrder(Trainorder trainorder, Customeruser customeruser) {
        //方法
        TrainSupplyMethod method = new TrainSupplyMethod();
        //REP
        RepServerBean rep = RepServerUtil.getRepServer(customeruser, false);
        //地址
        String repUrl = rep.getUrl();
        //参数
        String par = "datatypeflag=32&cookie=" + customeruser.getCardnunber()
                + method.JoinCommonAccountInfo(customeruser, rep);
        WriteLog.write("TrainCreateOrderUtil_cancelPaiduiOrder", trainorder.getId() + ":par:" + par + ":订单号:" + repUrl);
        String infodata = "";
        try {
            infodata = SendPostandGet.submitPost(repUrl, par, "UTF-8").toString();
            //用户未登录
            if (infodata.contains("用户未登录") && RepServerUtil.changeRepServer(customeruser)) {
                //切换REP
                rep = RepServerUtil.getTaoBaoTuoGuanRepServer(customeruser, false);
                //类型正确
                if (rep.getType() == 1) {
                    //REP地址
                    repUrl = rep.getUrl();
                    //重拼参数
                    par = "datatypeflag=32&cookie=" + customeruser.getCardnunber()
                            + method.JoinCommonAccountInfo(customeruser, rep);
                    //记录日志
                    WriteLog.write("TrainCreateOrderUtil_cancelPaiduiOrder", trainorder.getId() + ":par:" + par
                            + ":changeRepServer:" + repUrl);
                    //重新请求
                    infodata = SendPostandGet.submitPost(repUrl, par, "UTF-8").toString();
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            WriteLog.write("TrainCreateOrderUtil_cancelPaiduiOrder",
                    trainorder.getId() + ":e:" + JSONObject.toJSONString(e.getStackTrace()));
        }
        WriteLog.write("TrainCreateOrderUtil_cancelPaiduiOrder", trainorder.getId() + ":infodata:" + infodata);
        return infodata;
    }

    /**
     * 12306返回信息是否齐全
     *
     * @param str
     * @param passengers
     * @param isPaiDui 是否排队
     * @return
     */
    public static boolean isall(Trainorder trainOrder, String str, List<Trainpassenger> passengers, boolean isPaiDui,boolean isLc) {
        String isall = isallReturnString(trainOrder, str, passengers, isPaiDui,isLc);
        if (isall.startsWith("true")) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * 校验未完成订单信息
     */
    public static String checkMyOrderNoComplete(Trainorder trainOrder, String str, List<Trainpassenger> passengers,
                                                boolean isPaiDui) {
        try {
            //改签待支付、变更到站待支付
            if (ElongHotelInterfaceUtil.StringIsNull(str) || str.contains("改签待支付") || str.contains("变更到站待支付")
                    || !str.contains("orderDBList") || !str.contains("tickets")) {
                return "false";
            }
            //解析
            JSONObject json = JSONObject.parseObject(str);
            JSONObject data = json.getJSONObject("data");
            //订单
            JSONArray orderDBList = data.getJSONArray("orderDBList");
            //非唯一
            if (orderDBList == null || orderDBList.size() != 1) {
                return "false";
            }
            //第一个
            JSONObject firstOrder = orderDBList.getJSONObject(0);
            //车票
            JSONArray tickets = firstOrder.getJSONArray("tickets");
            //订单号空
            if (ElongHotelInterfaceUtil.StringIsNull(firstOrder.getString("sequence_no"))) {
                return "false|E字单号为空";
            }
            //票数校验
            if (tickets == null || passengers.size() <= 0 || tickets.size() != passengers.size()) {
                return "false|乘客数量不对";
            }
            //时间早于系统订单，2分钟误差
            /*
            if (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(firstOrder.getString("order_date")).getTime() < (trainOrder
                    .getCreatetime().getTime() - 2 * 60 * 1000)) {
                return "false|12306订单创建时间早于系统订单创建时间";
            }
            */
            Trainticket firstTicket = new Trainticket();
            JSONObject stationTrainDTO = new JSONObject();
            //匹配
            int matchTotal = 0;
            List<String> matchList = new ArrayList<String>();
            //循环车票
            for (int i = 0; i < tickets.size(); i++) {
                JSONObject ticket = tickets.getJSONObject(i);
                //乘客信息
                JSONObject passengerDTO = ticket.getJSONObject("passengerDTO");
                String passenger_name = passengerDTO.getString("passenger_name");
                String passenger_id_no = passengerDTO.getString("passenger_id_no").toUpperCase();
                String passenger_id_type_code = passengerDTO.getString("passenger_id_type_code");
                //座席编码
                String seat_type_code = ticket.getString("seat_type_code");
                //车票类型，1：成人票
                String ticket_type_code = ticket.getString("ticket_type_code");
                //车次信息
                if (i == 0) {
                    stationTrainDTO = ticket.getJSONObject("stationTrainDTO");
                }
                //拼匹配数据
                String keyfalg = passenger_name + "@" + passenger_id_no + "@" + passenger_id_type_code + "@"
                        + seat_type_code + "@" + ticket_type_code;
                //用于匹配本地
                matchList.add(keyfalg);
            }
            //本地系统数据
            for (int i = 0; i < passengers.size(); i++) {
                Trainpassenger localPassenger = passengers.get(i);
                //本地车票
                Trainticket localTicket = localPassenger.getTraintickets().get(0);
                //乘客信息
                String passenger_name = localPassenger.getName();
                String passenger_id_no = localPassenger.getIdnumber().toUpperCase();
                String passenger_id_type_code = TrainSupplyMethod.getIdtype12306(localPassenger.getIdtype());
                //座席编码
                String seat_type_code = TrainSupplyMethod.getzwname(localTicket.getSeattype());
                //车票类型，1：成人票
                String ticket_type_code = String.valueOf(localTicket.getTickettype());
                //车次信息
                if (i == 0) {
                    firstTicket = localTicket;
                }
                //拼匹配数据
                String keyfalg = passenger_name + "@" + passenger_id_no + "@" + passenger_id_type_code + "@"
                        + seat_type_code + "@" + ticket_type_code;
                //匹配数据
                if (matchList.contains(keyfalg)) {
                    matchTotal++;
                    matchList.remove(keyfalg);
                }
            }
            //车次
            String trainCodeLocal = "";
            String trainCode12306 = firstOrder.getString("train_code_page");
            //本地可能多个，T101/T102
            for (String trainNo : firstTicket.getTrainno().split("/")) {
                //12306匹配到本地
                if (trainCode12306.equals(trainNo)) {
                    trainCodeLocal = trainNo;
                    break;
                }
            }
            //发车时间
            String startDateLocal = firstTicket.getDeparttime().substring(0, 10);
            String startDate12306 = firstOrder.getString("start_train_date_page").substring(0, 10);
            //车次比较
            if (!trainCodeLocal.equals(trainCode12306)) {
                return "false|车次不对";
            }
            //日期比较
            if (!startDateLocal.equals(startDate12306)) {
                return "false|日期不对";
            }
            //全匹配
            if (matchList.size() != 0 || matchTotal != passengers.size()) {
                return "false|乘客信息、座席、车票类型存在不对";
            }
            //对比出发及到达站>>北京、北京西等无法识别，暂不处理
            String toStationLocal = firstTicket.getArrival();
            String fromStationLocal = firstTicket.getDeparture();
            String toStation12306 = stationTrainDTO.getString("to_station_name");
            String fromStation12306 = stationTrainDTO.getString("from_station_name");
            //完全匹配上
            if (toStationLocal.equals(toStation12306) && fromStationLocal.equals(fromStation12306)) {
                return "true";
            }
            //直接匹配不上，比对发车时间
            String startTimeLocal = firstTicket.getDeparttime();
            String arriveTimeLocal = firstTicket.getArrivaltime();
            //完全匹配上
            if (!ElongHotelInterfaceUtil.StringIsNull(arriveTimeLocal)
                    && arriveTimeLocal.equals(firstOrder.getString("arrive_time_page"))
                    && startTimeLocal.equals(firstOrder.getString("start_train_date_page"))) {
                return "true";
            }
            else {
                //后续可查12306数据
                return "false|出发或到达站不对";
            }
        }
        catch (Exception e) {

        }
        return "false";
    }

    /**
     * 12306返回信息是否齐全
     * 增加如果匹配不通过是什么原因
     * @param str
     * @return
     */
    public static String isallReturnString(Trainorder trainOrder, String str, List<Trainpassenger> passengers,
                                           boolean isPaiDui,boolean isLC) {
        //结果
        String isall = "true";
        //订单ID
        long trainOrderid = trainOrder.getId();
        //订单类型
        int orderType = trainOrder.getOrdertype();
        //客人账号
        boolean CustomerAccount = orderType == 3 || orderType == 4;
        //记录日志
        String logName = "TrainCreateOrderUtil_isallReturnString";
        WriteLog.write(logName, trainOrderid + ":" + orderType + ":" + str);
        WriteLog.write(logName, trainOrderid + ":" + JSONObject.toJSONString(passengers));
        //客人账号订单
        if (CustomerAccount && isPaiDui) {//客人账号并且排队的
            isall = checkMyOrderNoComplete(trainOrder, str, passengers, isPaiDui);
        }
        else {
            isall = newCheck12306Info(trainOrderid, str, passengers,isLC);
            //            isall = check12306Info2(trainOrderid, str, passengers);
            WriteLog.write(logName, trainOrderid + ":" + isall);
        }

        return isall;
    }

    /**
     * 新的检查对比是否订单信息齐全
     * 这个是最新的殷树斌让用这个，有啥问题都在这个方法的基础上修改。2017年5月15日16:48:14
     * @param trainOrderid
     * @param str
     * @param passengers
     * @return
     */
    private static String newCheck12306Info(long trainOrderid, String str, List<Trainpassenger> passengers,boolean isLC) {
        String isAll = "true";
        //乘客数量
        int passengerNum = passengers.size();
        //循环订单信息
        JSONObject all12306Info = new JSONObject();
        try {
            all12306Info = JSONObject.parseObject(str);
            if (all12306Info != null && !all12306Info.isEmpty()) {
                JSONObject data = all12306Info.containsKey("data") ? all12306Info.getJSONObject("data")
                        : new JSONObject();
                if (!data.isEmpty()) {
                    JSONArray orderDBList = data.containsKey("orderDBList") ? data.getJSONArray("orderDBList")
                            : new JSONArray();
                    if (!orderDBList.isEmpty()) {
                        if (orderDBList.size() == 1) {
                            for (int i = 0; i < orderDBList.size(); i++) {
                                JSONObject orderDBListObject = orderDBList.getJSONObject(i);
                                JSONArray tickets = orderDBListObject.getJSONArray("tickets");
                                //TODO
                                if (tickets.size() != passengerNum && !isLC) {
                                    isAll = "false|出票数与乘客数不对";
                                    break;
                                }
                                if (isLC && tickets.size() != (passengerNum*2)) {
                                    isAll = "false|联程出票数与乘客数不对";
                                    break;
                                }
                                //遍历每张票与乘客对比
                                isAll = compareTicketsInfo(tickets, passengers);
                                if (isAll.startsWith("false")) {
                                    break;
                                }
                            }
                        }
                        else {
                            isAll = "false|orderDBList_Size不对";
                        }
                    }
                    else {
                        isAll = "false|订单信息中orderDBList为NULL或者为空";
                    }
                }
                else {
                    isAll = "false|订单信息中data为NULL或者为空";
                }
            }
            else {
                isAll = "false|订单信息为NULL或者为空";
            }
        }
        catch (Exception e) {
            isAll = "false|对比订单信息异常";
        }

        return isAll;

    }

    /**
     * 对比票的信息
     * @param tickets
     * @param passengers
     * @return
     */
    public static String compareTicketsInfo(JSONArray tickets, List<Trainpassenger> passengers) {
        String compareResult = "true";
        //内部信息对比是否有误
        boolean checkIsFalse = false;
        //对比了几个乘客 理论对比的数量==票数==乘客数
        int campareTicketNum = 0;

        //存放對比的乘客key 乘客id  value 證件號+id
        Hashtable<Long, String> person = new Hashtable<Long, String>();

        //存放對比的票key 乘客id  value ticket_no
        Hashtable<Long, String> campareTicket = new Hashtable<Long, String>();

        for (int j = 0; j < tickets.size(); j++) {
            JSONObject ticket = tickets.getJSONObject(j);
            JSONObject stationTrainDTO = ticket.containsKey("stationTrainDTO") ? ticket
                    .getJSONObject("stationTrainDTO") : new JSONObject();
            JSONObject passengerDTO = ticket.containsKey("passengerDTO") ? ticket.getJSONObject("passengerDTO")
                    : new JSONObject();
            String ticket_no = ticket.containsKey("ticket_no") ? ticket.getString("ticket_no") : "";
            String sequence_no = ticket.containsKey("sequence_no") ? ticket.getString("sequence_no") : "";
            //2016-10-13 00:00:00
            String train_date = ticket.containsKey("train_date") ? ticket.getString("train_date") : "";
            String coach_no = ticket.containsKey("coach_no") ? ticket.getString("coach_no") : "";
            String coach_name = ticket.containsKey("coach_name") ? ticket.getString("coach_name") : "";
            String seat_no = ticket.containsKey("seat_no") ? ticket.getString("seat_no") : "";
            String seat_flag = ticket.containsKey("seat_flag") ? ticket.getString("seat_flag") : "";
            String seat_type_code = ticket.containsKey("seat_type_code") ? ticket.getString("seat_type_code") : "";
            //票类型 成人票
            String ticket_type_name = ticket.containsKey("ticket_type_name") ? ticket.getString("ticket_type_name")
                    : "";
            if (stationTrainDTO.isEmpty() || passengerDTO.isEmpty() || ElongHotelInterfaceUtil.StringIsNull(ticket_no)
                    || ElongHotelInterfaceUtil.StringIsNull(train_date)
                    || ElongHotelInterfaceUtil.StringIsNull(coach_no)
                    || ElongHotelInterfaceUtil.StringIsNull(sequence_no)
                    || ElongHotelInterfaceUtil.StringIsNull(coach_name)
                    || ElongHotelInterfaceUtil.StringIsNull(seat_no) || ElongHotelInterfaceUtil.StringIsNull(seat_flag)
                    || ElongHotelInterfaceUtil.StringIsNull(seat_type_code)
                    || ElongHotelInterfaceUtil.StringIsNull(ticket_type_name)) {
                compareResult = "false|信息不全";
                break;
            }
            //ticket里的车票信息
            //车次
            String station_train_code = stationTrainDTO.containsKey("station_train_code") ? stationTrainDTO
                    .getString("station_train_code") : "";
            //出发站三字码
            String from_station_telecode = stationTrainDTO.containsKey("from_station_telecode") ? stationTrainDTO
                    .getString("from_station_telecode") : "";
            //出发城市
            String from_station_name = stationTrainDTO.containsKey("from_station_name") ? stationTrainDTO
                    .getString("from_station_name") : "";
            //到达站三字码
            String to_station_telecode = stationTrainDTO.containsKey("to_station_telecode") ? stationTrainDTO
                    .getString("to_station_telecode") : "";
            //到达城市
            String to_station_name = stationTrainDTO.containsKey("to_station_name") ? stationTrainDTO
                    .getString("to_station_name") : "";

            //ticket里的乘客信息
            //乘客姓名
            String passenger_name = passengerDTO.containsKey("passenger_name") ? passengerDTO
                    .getString("passenger_name") : "";
            //证件类型code
            String passenger_id_type_code = passengerDTO.containsKey("passenger_id_type_code") ? passengerDTO
                    .getString("passenger_id_type_code") : "";
            //证件类型
            String passenger_id_type_name = passengerDTO.containsKey("passenger_id_type_name") ? passengerDTO
                    .getString("passenger_id_type_name") : "";
            //证件号
            String passenger_id_no = passengerDTO.containsKey("passenger_id_no") ? passengerDTO.getString(
                    "passenger_id_no").toUpperCase() : "";

            if (ElongHotelInterfaceUtil.StringIsNull(station_train_code)
                    || ElongHotelInterfaceUtil.StringIsNull(from_station_telecode)
                    || ElongHotelInterfaceUtil.StringIsNull(from_station_name)
                    || ElongHotelInterfaceUtil.StringIsNull(to_station_telecode)
                    || ElongHotelInterfaceUtil.StringIsNull(to_station_name)
                    || ElongHotelInterfaceUtil.StringIsNull(passenger_name)
                    || ElongHotelInterfaceUtil.StringIsNull(passenger_id_type_code)
                    || ElongHotelInterfaceUtil.StringIsNull(passenger_id_type_name)
                    || ElongHotelInterfaceUtil.StringIsNull(passenger_id_no)) {

                compareResult = "false|信息不全";
                break;
            }

            //对比信息有无
            boolean checkInfoErr = false;
            for (int i = 0; i < passengers.size(); i++) {
                Trainpassenger trainpassenger = passengers.get(i);
                Trainticket trainticket = trainpassenger.getTraintickets().get(0);
                //乘客姓名
                String name = trainpassenger.getName();
                //证件号
                String idNumber = trainpassenger.getIdnumber().toUpperCase();
                ;
                //出发站
                String departure = trainticket.getDeparture();
                //到达站
                String arrival = trainticket.getArrival();
                //出发时间 2016-10-13 00:00
                String departtime = trainticket.getDeparttime();
                //乘客类型 成人
                String tickettypestr = trainticket.getTickettypestr();

                //座席编码
                String seattypecode = TrainSupplyMethod.getzwname(trainticket.getSeattype());

                String trainCode12306 = "";
                //本地可能多个，T101/T102
                for (String trainNo : trainticket.getTrainno().split("/")) {
                    //12306匹配到本地
                    if (station_train_code.equals(trainNo)) {
                        trainCode12306 = trainNo;
                        break;
                    }
                }

                //獲取對比之前的乘客
                String exsitName = person.get(trainpassenger.getId()) == null ? "" : person.get(trainpassenger.getId());

                //獲取對比之後的票號
                String _titcketNo = campareTicket.get(trainpassenger.getId()) == null ? "" : campareTicket
                        .get(trainpassenger.getId());
                //這個票是否判斷過
                boolean istitcketNo = false;

                if (_titcketNo.equals(ticket_no)) {
                    istitcketNo = true;
                }

                //這個乘客是否沒有判斷過
                boolean isNPD = false;

                if ("".equals(exsitName)) {
                    //判斷過
                    isNPD = false;
                }

                if (exsitName.equals(idNumber + trainpassenger.getId())) {
                    isNPD = true;
                }

                //這張票沒有判斷過，乘客沒有判斷過,这张票是否是当前乘客
                if (istitcketNo
                        || isNPD
                        || (!name.equals(passenger_name) || !idNumber.equals(passenger_id_no)
                        || !seatCodeEquals(seat_type_code, trainticket.getSeattype()) || !(ticket_type_name
                        .equals(tickettypestr) || ticket_type_name.contains(tickettypestr)))) {
                    continue;
                }

                //把對比的乘客放進person
                person.put(trainpassenger.getId(), idNumber + trainpassenger.getId());

                //下面对比乘客
                campareTicketNum++;

                //是当前乘客对比车票信息
                if (!station_train_code.equals(trainCode12306)
                        || !train_date.substring(0, 10).equals(departtime.substring(0, 10))) {
                    //当前信息有误
                    checkInfoErr = true;
                    //把對比的乘客id 和 票放進票裡面
                    campareTicket.put(trainpassenger.getId(), ticket_no);
                    break;
                }
                //把對比的乘客id 和 票放進票裡面
                campareTicket.put(trainpassenger.getId(), ticket_no);
                //這個乘客對比完成 直接跳過後續對比 進行下張票對比
                break;
            }

            if (checkInfoErr) {
                compareResult = "false|乘客信息、车次、出发日期、存在不对";
                checkIsFalse = true;
                break;
            }

        }
        if (!checkIsFalse && campareTicketNum != passengers.size() && passengers.size() != 0) {
            compareResult = "false|乘客对比有误";
        }

        return compareResult;
    }

    /**
     * 判断坐席code是否一致
     *
     * @param seatCode12306
     * @param seatTypeDB
     * @return
     * @time 2017年5月31日 下午12:10:25
     * @author fiend
     */
    private static boolean seatCodeEquals(String seatCode12306, String seatTypeDB) {
        //判断席别是否一样
        boolean flag = true;
        try {
            flag = TrainSeatTypeEnum
                    .getTrainSeatTypeEnumBySeatTypeResult(seatTypeDB)
                    .getSeatCodeCreate()
                    .equalsIgnoreCase(
                            TrainSeatTypeEnum.getTrainSeatTypeEnumBySeatCodeResult(seatCode12306).getSeatCodeCreate());
        }
        catch (NullPointerException e) {
            ExceptionUtil.writelogByException("TrainCreateOrderUtil_seatCodeEquals_NullPointerException", e,
                    seatCode12306 + "--->" + seatTypeDB);
        }
        WriteLog.write("TrainCreateOrderUtil_seatCodeEquals", seatCode12306 + "--->" + seatTypeDB + "--->" + flag);
        return flag;
    }
//
//    public static void main(String[] args) {
//        System.out.println(seatCodeEquals("F", "软卧"));
//    }

    /**
     * 把这个方法再写个方法
     * @return
     * @time 2016年4月25日 上午10:52:46
     * @author chendong
     * @param trainOrderid 订单id
     * @param passengers  乘客信息
     * @param str  12306的信息
     */
    private static String check12306Info(long trainOrderid, String str, List<Trainpassenger> passengers) {
        //结果
        String isall = "true";
        try {
            net.sf.json.JSONObject jsono = net.sf.json.JSONObject.fromObject(str);
            if (jsono.has("data")) {
                net.sf.json.JSONObject jsonodata = jsono.getJSONObject("data");
                if (jsonodata.has("orderDBList")) {
                    net.sf.json.JSONArray jsonaorderDBList = jsonodata.getJSONArray("orderDBList");
                    for (int j = 0; j < jsonaorderDBList.size(); j++) {// 获取所有订单
                        net.sf.json.JSONObject jsonoorderDBList = jsonaorderDBList.getJSONObject(j);
                        if (jsonoorderDBList.has("tickets")) {
                            net.sf.json.JSONArray tickets = jsonoorderDBList.getJSONArray("tickets");
                            if (tickets.size() == passengers.size()) {
                                for (Trainpassenger trainpassenger : passengers) {
                                    if (tickets.getJSONObject(0).containsKey("train_date")) {
                                        String traindate12306 = tickets.getJSONObject(0).getString("train_date")
                                                .substring(0, 10);
                                        String traindateString = trainpassenger.getTraintickets().get(0)
                                                .getDeparttime().substring(0, 10);
                                        if (!traindate12306.equals(traindateString)) {
                                            WriteLog.write("TrainCreatOrderUtil_isall", trainOrderid + ":new:"
                                                    + traindate12306 + ":old:" + traindateString);
                                            isall = "false|日期不对";
                                        }
                                    }
                                    else {
                                        isall = "false|没有日期";
                                    }
                                    if (tickets.getJSONObject(0).containsKey("seat_type_code")) {
                                        String seat_type_code = tickets.getJSONObject(0).getString("seat_type_code");
                                        String seattype = trainpassenger.getTraintickets().get(0).getSeattype();
                                        String seat_type_code_db = TrainSupplyMethod.getzwname(seattype);
                                        if (seat_type_code.equalsIgnoreCase(TrainSupplyMethod.getzwname(seattype))
                                                || ("A".equals(seat_type_code) && "6".equals(seat_type_code_db))
                                                || ("7".equals(seat_type_code) && "M".equals(seat_type_code_db))
                                                || ("8".equals(seat_type_code) && "O".equals(seat_type_code_db))
                                                || ("F".equals(seat_type_code) && "4".equals(seat_type_code_db))) {
                                            //A是12306的高级动卧,6是库里的高级软卧,
                                            //7是12306的一等软座,M是库里的一等座,
                                            //8是12306的二等软座,O是库里的二等座,
                                            //F是12306的动卧,4是库里的软卧,
                                            //如果12306是动卧并且客户下的订单是软卧这2个是可以匹配
                                            //判断席别是否一样
                                        }
                                        else {
                                            WriteLog.write("TrainCreatOrderUtil_isall", trainOrderid + ":new:"
                                                    + seat_type_code + ":old:" + seat_type_code_db + ":olddb:"
                                                    + seattype);
                                            //                                            new:8:old:O:olddb:二等座
                                            isall = "false|席别不对";
                                        }
                                    }
                                    else {
                                        isall = "false|没有席别";
                                    }
                                    if (!str.contains(trainpassenger.getName())) {
                                        WriteLog.write("TrainCreatOrderUtil_isall", trainOrderid + ":old:"
                                                + trainpassenger.getName() + ":new:" + str);
                                        isall = "false|乘客姓名不对";
                                    }
                                }
                            }
                            else {
                                isall = "false|乘客数量不对";
                            }
                        }
                        else {
                            isall = "false|没有tickets";
                        }
                    }
                }
                else {
                    isall = "false|没有orderDBList";
                }

            }
            else {
                isall = "false|没有data";
            }
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("ERROR_logName", e);
            isall = "false";
        }
        return isall;
    }

    /**
     * @TODO 12306下单票的信息对比
     * <p>
     * @param trainOrderid  订单id
     * @param str   12306信息
     * @param passengers    乘客信息
     * @return
     * <p>
     * @time:2016年4月25日  下午8:35:58
     * <p>
     * @author  fengfh
     */
    private static String check12306Info2(long trainOrderid, String str, List<Trainpassenger> passengers) {
        //结果
        String isall = "true";
        try {
            net.sf.json.JSONObject jsono = net.sf.json.JSONObject.fromObject(str);
            if (jsono.has("data")) {
                net.sf.json.JSONObject jsonodata = jsono.getJSONObject("data");
                if (jsonodata.has("orderDBList")) {
                    net.sf.json.JSONArray jsonaorderDBList = jsonodata.getJSONArray("orderDBList");
                    for (int j = 0; j < jsonaorderDBList.size(); j++) {// 获取所有订单
                        net.sf.json.JSONObject jsonoorderDBList = jsonaorderDBList.getJSONObject(j);
                        if (jsonoorderDBList.has("tickets")) {
                            net.sf.json.JSONArray tickets = jsonoorderDBList.getJSONArray("tickets");
                            if (tickets.size() == passengers.size()) {
                                //是否需要检查儿童票，如果true，就跳过儿童票；如果false，就需要对比信息
                                boolean needCheckChild = false;
                                goo: for (Trainpassenger tp1 : passengers) {
                                    for (Trainpassenger tp2 : passengers) {
                                        if (tp1.getIdnumber().equals(tp2.getIdnumber()) && tp1.getId() != tp2.getId()) {
                                            needCheckChild = true;
                                            break goo;
                                        }
                                    }
                                }
                                int number = tickets.size();
                                for (int k = 0; k < passengers.size(); k++) {
                                    Trainpassenger trainpassenger = passengers.get(k);
                                    for (int n = 0; n < trainpassenger.getTraintickets().size(); n++) {
                                        String departure = trainpassenger.getTraintickets().get(n).getDeparture();//出发站
                                        String arrival = trainpassenger.getTraintickets().get(n).getArrival();//到达站
                                        String departtime = trainpassenger.getTraintickets().get(n).getDeparttime();//出发时间
                                        String idNumber = trainpassenger.getIdnumber();//证件号
                                        String tickettypestr = trainpassenger.getTraintickets().get(n)
                                                .getTickettypestr();//乘客类型
                                        for (int i = 0; i < tickets.size(); i++) {
                                            String ticketType = tickets.getJSONObject(i).getString("ticket_type_name");
                                            if (tickets.getJSONObject(i).containsKey("stationTrainDTO")) {
                                                String fromStation = tickets.getJSONObject(i)
                                                        .getJSONObject("stationTrainDTO")
                                                        .getString("from_station_name");
                                                String toStation = tickets.getJSONObject(i)
                                                        .getJSONObject("stationTrainDTO").getString("to_station_name");
                                                String startTime = tickets.getJSONObject(i).getString("train_date");
                                                if (tickets.getJSONObject(i).containsKey("passengerDTO")) {
                                                    String idNo = tickets.getJSONObject(i)
                                                            .getJSONObject("passengerDTO").getString("passenger_id_no");
                                                    if (departure.equals(fromStation)
                                                            && arrival.equals(toStation)
                                                            && departtime.substring(0, 10).equals(
                                                            startTime.substring(0, 10))
                                                            && idNumber.equals(idNo)) {
                                                        if (needCheckChild) {
                                                            if ("儿童".equals(tickettypestr) || "儿童票".equals(ticketType)) {
                                                                continue;
                                                            }
                                                        }
                                                        if (tickets.getJSONObject(i).containsKey("train_date")) {
                                                            String traindate12306 = tickets.getJSONObject(i)
                                                                    .getString("train_date").substring(0, 10);
                                                            String traindateString = trainpassenger.getTraintickets()
                                                                    .get(n).getDeparttime().substring(0, 10);
                                                            if (!traindate12306.equals(traindateString)) {
                                                                WriteLog.write("TrainCreatOrderUtil_isall",
                                                                        trainOrderid + ":new:" + traindate12306
                                                                                + ":old:" + traindateString);
                                                                isall = "false|日期不对";
                                                            }
                                                        }
                                                        else {
                                                            isall = "false|没有日期";
                                                        }
                                                        if (tickets.getJSONObject(i).containsKey("seat_type_code")) {
                                                            String seat_type_code = tickets.getJSONObject(i).getString(
                                                                    "seat_type_code");
                                                            String seattype = trainpassenger.getTraintickets().get(n)
                                                                    .getSeattype();
                                                            String seat_type_code_db = TrainSupplyMethod
                                                                    .getzwname(seattype);
                                                            if (seat_type_code.equalsIgnoreCase(TrainSupplyMethod
                                                                    .getzwname(seattype))
                                                                    || ("A".equals(seat_type_code) && "6"
                                                                    .equals(seat_type_code_db))
                                                                    || ("7".equals(seat_type_code) && "M"
                                                                    .equals(seat_type_code_db))
                                                                    || ("8".equals(seat_type_code) && "O"
                                                                    .equals(seat_type_code_db))
                                                                    || ("F".equals(seat_type_code) && "4"
                                                                    .equals(seat_type_code_db))) {
                                                                //A是12306的高级动卧,6是库里的高级软卧,
                                                                //7是12306的一等软座,M是库里的一等座,
                                                                //8是12306的二等软座,O是库里的二等座,
                                                                //F是12306的动卧,4是库里的软卧,
                                                                //如果12306是动卧并且客户下的订单是软卧这2个是可以匹配
                                                                //判断席别是否一样
                                                            }
                                                            else {
                                                                WriteLog.write("TrainCreatOrderUtil_isall",
                                                                        trainOrderid + ":new:" + seat_type_code
                                                                                + ":old:" + seat_type_code_db
                                                                                + ":olddb:" + seattype);
                                                                //                                            new:8:old:O:olddb:二等座
                                                                isall = "false|席别不对";
                                                            }
                                                        }
                                                        else {
                                                            isall = "false|没有席别";
                                                        }
                                                        if (!str.contains(trainpassenger.getName())) {
                                                            WriteLog.write("TrainCreatOrderUtil_isall", trainOrderid
                                                                    + ":old:" + trainpassenger.getName() + ":new:"
                                                                    + str);
                                                            isall = "false|乘客姓名不对";
                                                        }
                                                    }

                                                    else {
                                                        WriteLog.write("TrainCreatOrderUtil_isall", trainOrderid + ":"
                                                                + departure + "--->" + fromStation + "--->" + arrival
                                                                + "--->" + toStation + "--->" + departtime + "--->"
                                                                + startTime.substring(0, 10) + "--->" + idNumber
                                                                + "--->" + idNo);
                                                    }
                                                }
                                                else {
                                                    WriteLog.write("TrainCreatOrderUtil_isall", trainOrderid + ":"
                                                            + "tickets.getJSONObject(" + i + "):no passengerDTO");
                                                }
                                            }
                                            else {
                                                WriteLog.write("TrainCreatOrderUtil_isall", trainOrderid + ":"
                                                        + "tickets.getJSONObject(" + i + "):no stationTrainDTO;");
                                            }
                                        }
                                    }
                                }
                            }
                            else {
                                isall = "false|乘客数量不对";
                            }
                        }
                        else {
                            isall = "false|没有tickets";
                        }
                    }
                }
                else {
                    isall = "false|没有orderDBList";
                }

            }
            else {
                isall = "false|没有data";
            }
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("ERROR_logName", e);
            isall = "false";
        }
        return isall;
    }

    /**
     * 是否能下单
     * @return
     * @time 2015年11月18日 下午4:21:17
     * @author chendong
     * @param trainorder
     */
    public static boolean getIsCanOrderIng(Trainorder trainorder) {
        boolean iscanordering = true;// 是否能下单
        try {
            iscanordering = (trainorder.getIsquestionorder() == null || trainorder.getIsquestionorder() == 0)
                    && (trainorder.getExtnumber() == null || ("".equals(trainorder.getExtnumber()) || (trainorder
                    .getExtnumber().indexOf(",") == 0)));
        }
        catch (Exception e) {
        }
        return iscanordering;
    }

    /**
     * 将不可用的帐号同步推送给同程
     *
     * @param user
     * @param msg
     * @time 2015年11月17日 下午5:49:28
     * @author Administrator
     */
    public static void checkAccountMethod(Customeruser user, String msg) {
        boolean flag = false;
        if (msg.contains("您的身份信息未通过核验")) {
            flag = true;
        }
        else if (msg.contains("您注册的信息与其他用户重复")) {
            flag = true;
        }
        else if (msg.contains("手机核验")) {
            flag = true;
        }
        else if (msg.contains("您注册时的手机号码被多个用户使用")) {
            flag = true;
        }
        else if (msg.contains("未能通过国家身份信息管理权威部门核验")) {
            flag = true;
        }
        if (flag) {
            CallBackPassengerUtil.callBackTongcheng(user, new ArrayList<Trainpassenger>(), 2);
        }
    }

    /**
     * 很抱歉！当前提交订单用户过多，请您稍后重试。
     * 之后再次下单返回已订。
     * 就相当于存在未完成订单！
     *
     * @time 2015年11月19日 下午2:04:09
     * @author fiend
     */
    public static boolean getTooManyUsersSubmit(long trainorderid) {
        try {
            String defaultStr = PropertyUtil.getValue("too_many_users_submit_default_str_", "train.properties");
            String value = OcsMethod.getInstance().get(defaultStr + trainorderid);
            WriteLog.write("too_many_users_submit_get", "订单号:" + trainorderid + "--->" + defaultStr + trainorderid
                    + "--->" + value);
            if (value != null && "true".equals(value)) {
                return true;
            }
        }
        catch (Exception e) {
            WriteLog.write("error_too_many_users_submit_get", "订单号:" + trainorderid);
            ExceptionUtil.writelogByException("error_too_many_users_submit_get", e);
        }
        return false;
    }

    /**
     * 根据返回的信息判断这个订单是否需要重新下单 【需要返回true;不需要返回false】
     * @return
     * @time 2015年11月20日 下午3:06:35
     * @author chendong
     * @param msg
     */
    public static boolean getisReCreateOrderBymsg(String msg, long trainorderid) {
        return getisReCreateOrderBymsg(msg, trainorderid, -1);
    }

    /**
     * 根据返回的信息判断这个订单是否需要重新下单 【需要返回true;不需要返回false】
     * @return
     * @time 2015年11月20日 下午3:06:35
     * @author chendong
     * @param msg
     */
    public static boolean getisReCreateOrderBymsg(String msg, long trainorderid, long agentId) {
        //        窦建国@320922197010251713@
        //        窦建国的身份信息涉嫌被他人冒用，为了保障个人信息安全，
        //        请您持本人和窦建国的身份证件原件(未成年人可持户口本)到就近办理客运售票业务的铁路车站完成身份核验，通过后即可在网上办理购票业务，谢谢。
        boolean isReCreateOrderBymsg = false;
        isReCreateOrderBymsg = (/*(msg.indexOf("已订") < 0 || TrainCreateOrderUtil.getTooManyUsersSubmit(trainorderid))
                                &&*/msg
                .indexOf("不可购票") < 0 /*&& msg.indexOf("排队人数现已超过余票数") < 0 *//*&& msg.indexOf("本次购票行程冲突") < 0*/
                && msg.indexOf("价格发生变化") < 0 && msg.indexOf("非法的席别") < 0
                && msg.indexOf("您的身份信息未经核验") < 0/* && msg.indexOf("当前提交订单用户过多") < 0*/
                /*&& msg.indexOf("余票不足") < 0 && msg.indexOf("没有足够的票") < 0 && msg.indexOf("已无余票") < 0*/
                && msg.indexOf("限制高消费") < 0 && msg.indexOf("座席编码为空") < 0 && msg.indexOf("待核验") < 0
                && msg.indexOf("@证件号码输入有误") < 0 /*&& msg.indexOf("已购买") < 0*/) || msg.indexOf("账号尚未通过身份信息核验") > 0
                || msg.indexOf("联系人尚未通过身份信息核验") > 0
                || (msg.contains("您填写的身份信息有误") && msg.contains("未能通过国家身份信息管理权威部门核验"));
        //美团已订、已购买不重试【fiend.20170617】
        //        if (isReCreateOrderBymsg && (msg.contains("已订") || msg.contains("已购买"))) {
        //            if (agentId == 67) {
        //                isReCreateOrderBymsg = false;
        //            }
        //        }
        return isReCreateOrderBymsg;
    }

    /**
     * 映射帐号状态
     *
     * @param isenable
     * @return
     * @time 2015年11月23日 上午10:50:30
     * @author wcl
     */
    public static int AccountStatus(int isenable) {
        if (isenable == 1) {
            return isenable = 2;
        }
        if (isenable == 33) {
            return isenable = 3;
        }
        if (isenable == 4) {
            return isenable;
        }
        return isenable = 1;
    }

    /**
     * 映射帐号状态
     *
     * @param isenable
     * @return
     * @time 2015年11月23日 上午10:50:30
     * @author wcl
     */
    public static String AccountStatusname(int isenable) {
        String accountStatusname = "其他";
        if (isenable == 1) {
            return accountStatusname = "账号被封";
        }
        if (isenable == 2) {
            return accountStatusname = "可用";
        }
        if (isenable == 3) {
            return accountStatusname = "未绑定手机号";
        }
        if (isenable == 4) {
            return accountStatusname = "该账号当天取消三天，不可使用";
        }
        return accountStatusname;
    }

    /**
     * 根据条件判断是否排队
     * @param ordertype
     * @param result
     * @time 2015年12月11日 上午10:55:45
     * @author chendong
     * @param result 下单返回结果
     * @param interfacetype
     * @param tk
     * @param propertys 
     * @return
     */
    public static boolean ispaidui(int ordertype, int interfacetype, String result, Trainticket tk,
            JSONObject propertys) {
        boolean isPaidui = false;
        isPaidui = result.contains("还在排队中") || result.contains("车次数据正在维护,请稍后再进行取票")
                || (propertys.containsKey("errorMsgIsPaiDui") && propertys.getBooleanValue("errorMsgIsPaiDui"));
        // isPaidui = result.contains("还在排队中") && ordertype != 3 && ordertype !=
        // 4;//客人账号暂不排队，后续再作调整
        // 判断是淘宝的单子且有保险 ，走排队机制 ， 循环下单 [备用]
        // isPaidui = isPaidui && TrainInterfaceMethod.TAOBAO == interfacetype &&
        // tk.getInsurorigprice() != null
        // && tk.getInsurorigprice() == 20f;
        return isPaidui;
    }

    /**
     * 同程补单判断无座
     *
     * @return
     * @time 2015年1月18日 下午7:40:10
     * @author fiend
     */
    public static boolean isHaveWZ(Trainorder trainorder) {
        // Trainorder trainorder =
        // Server.getInstance().getTrainService().findTrainorder(this.trainorder.getId());
        for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
            for (Trainticket trainticket : trainpassenger.getTraintickets()) {
                if ("无座".equals(trainticket.getSeatno())) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 同程补单判断卧铺坐席
     *
     * @return
     * @time 2015年1月18日 下午7:40:10
     * @author fiend
     */
    public static boolean isThisWP(Trainorder trainorder, String WPS) {
        boolean ishaveWP = false;
        int counts = 0;
        int countz = 0;
        int countx = 0;
        // Trainorder trainorder =
        // Server.getInstance().getTrainService().findTrainorder(this.trainorder.getId());
        for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
            for (Trainticket trainticket : trainpassenger.getTraintickets()) {
                if (trainticket.getSeatno().contains("铺")) {
                    ishaveWP = true;
                }
                if (trainticket.getSeatno().contains("上铺")) {
                    counts++;
                }
                if (trainticket.getSeatno().contains("中铺")) {
                    countz++;
                }
                if (trainticket.getSeatno().contains("下铺")) {
                    countx++;
                }
            }
        }
        if (ishaveWP) {
            String wps = counts + "|" + countz + "|" + countx;
            if (WPS == null || "".equals(WPS)) {
                return false;
            }
            if (!wps.equals(WPS)) {
                return false;
            }
        }
        return true;
    }

    /**
     * 说明:接口可调用时间:早05:55晚23:05
     * @param date
     * @return
     * @time 2014年8月30日 下午4:23:20
     * @author yinshubin
     */
    public static boolean getNowTime(Date date) {
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        try {
            Date dateBefor = df.parse("05:55:00");
            Date dateAfter = df.parse("23:05:00");
            Date time = df.parse(df.format(date));
            if (time.after(dateBefor) && time.before(dateAfter)) {
                return true;
            }
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static String getSolution(String result) {
        if ("下单成功,数据库同步失败".equals(result)) {
            return "请手动查询12306订单";
        }
        else if ("此车次无票".equals(result) || "已买过票了".equals(result)) {
            return "请打电话确认后拒单";
        }
        else {
            return "请手动下单";
        }
    }

    /**
     * 是否换账号重试 true:换;false:不换
     * @param jsonmsg
     * @param msg
     * @return
     * @time 2016年1月6日 下午12:33:30
     * @author chendong
     * @param customerAccountNoLogin
     */
    public static boolean isChangeAccount(String jsonmsg, String msg, boolean customerAccountNoLogin) {
        boolean isChangeAccount = jsonmsg.contains("无未支付订单")
                || jsonmsg.contains("sequence_no")
                || msg.indexOf("存在未完成订单") > -1
                || msg.indexOf("订单未支付") > 0
                || msg.indexOf("取消次数过多") > 0
                || msg.indexOf("账号尚未通过身份信息核验") > 0
                || msg.indexOf("您的身份信息正在进行网上核验") > 0
                /*|| msg.indexOf("联系人尚未通过身份信息核验") > 0*/
                /*|| msg.indexOf("第三方") > 0 || msg.indexOf("多次打码失败") > 0 || msg.indexOf("获取动态秘钥失败") > 0*/
                ////            TCTrainOrdering:code::msgmsg:提交订单失败：李丹联系人尚未通过身份信息核验，不可购票，详见《铁路互联网购票身份核验须知》。:orderRound_interface下单用时:4783:jsonmsg:null
                /* || i % 3 == 0 */|| msg.indexOf("手机核验") > 0 || msg.indexOf("您在12306网站注册时填写信息有误") > -1
                || msg.indexOf("您注册的信息与其他用户重复") > -1 || msg.indexOf("您账户中的信息与其他用户重复") > -1
                || msg.indexOf("您注册时的手机号码被多个用户使用") > -1
                || msg.indexOf("您需要提供真实、准确的本人资料，为了保障您的个人信息安全，请您到就近办理客运售票业务的铁路车站完成身份核验") > -1 || msg.contains("已满")
                || msg.contains("未登录") || msg.contains("的身份信息涉嫌被他人冒用") || msg.contains("操作乘车人过于频繁")
                || customerAccountNoLogin || (msg.contains("您填写的身份信息有误") && msg.contains("未能通过国家身份信息管理权威部门核验"));
        return isChangeAccount;
    }

    /**
     * 1@10@3@36@33  可以下单
     *
     * @param isenable
     * @return
     * @time 2016年1月6日 下午8:41:01
     * @author fiend
     */
    public static boolean isCanUseByIsenable(int isenable) {
        if (isenable == 1 || isenable == 10 || isenable == 3 || isenable == 33 || isenable == 36) {
            return true;
        }
        return false;
    }

    /**
     * 比对两个时间相差是否小于5分钟
     *
     * @param start_time    12306的时间
     * @param traintime     数据库的时间
     * @return  true 小于 5minutes; false 大于5minutes;
     * @time 2016年1月15日 下午6:00:15
     * @author w.c.l
     */
    public static boolean thanTimeDifference(String start_time, String traintime) {
        boolean isTime = false;
        if (!ElongHotelInterfaceUtil.StringIsNull(start_time) && !ElongHotelInterfaceUtil.StringIsNull(traintime)) {
            if (start_time.equals(traintime)) {
                isTime = true;
            }
            else {
                try {
                    Date start = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(start_time);
                    Date train = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(traintime);
                    long time = start.getTime() - train.getTime();
                    long minutes = Math.abs(time / (1000 * 60));
                    if (minutes <= 5) {
                        isTime = true;
                    }
                }
                catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
        return isTime;
    }

    /**
     * 比对两个时间是否一致
     *
     * @param start_time    12306的时间
     * @param traintime     数据库的时间
     * @return  true 没区别; false 有区别;
     * @time 2016年6月22日 下午5:35:29
     * @author fiend
     */
    public static boolean TimeEnDifference(String start_time, String traintime) {
        boolean isTime = false;
        if (!ElongHotelInterfaceUtil.StringIsNull(start_time) && !ElongHotelInterfaceUtil.StringIsNull(traintime)) {
            if (start_time.equals(traintime)) {
                isTime = true;
            }
        }
        return isTime;
    }

    /**
     * 判断agentId是否是美团
     *
     * @param agentId
     * @return
     * @time 2016年6月22日 下午5:39:03
     * @author fiend
     */
    public static boolean isMeiTuan(long agentId) {
        String meituanAgentId = "";
        try {
            meituanAgentId = PropertyUtil.getValue("meituanAgentId", "train.properties");
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("TrainCreateOrderUtil_isMeiTuan_ERROR", e);
        }
        return (agentId + "").equals(meituanAgentId);
    }

    /**
     * 比对两个时间是否是同一天
     *
     * @param start_time    12306的时间
     * @param traintime     数据库的时间
     * @return  默认是true，如果不抛异常，不是同一天，才返回false
     * @time 2016年8月15日 下午2:36:22
     * @author fiend
     */
    public static boolean timeEqualsEachDay(String start_time, String traintime) {
        boolean isTime = true;
        if (!ElongHotelInterfaceUtil.StringIsNull(start_time) && !ElongHotelInterfaceUtil.StringIsNull(traintime)) {
            if (!start_time.equals(traintime)) {
                try {
                    start_time = start_time.substring(0, 10);
                    traintime = traintime.substring(0, 10);
                    if (!start_time.equals(traintime)) {
                        isTime = false;
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return isTime;
    }

    public static void main(String[] args) {
       boolean getisReCreateOrderBymsg = TrainCreateOrderUtil.getisReCreateOrderBymsg("排队人数现已超过余票数", 1231);
       System.out.println(getisReCreateOrderBymsg);
    }

}
