package com.ccservice.inter.job.train.thread;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.service12306.AccountSystem;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.sysconfig.Sysconfig;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.b2b2c.policy.QunarTrainMethod;
import com.ccservice.b2b2c.policy.SendPostandGet;
import com.ccservice.b2b2c.policy.ben.RepServerBean;
import com.ccservice.b2b2c.util.ActiveMQUtil;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.JobGenerateOrder;
import com.ccservice.inter.job.train.RepServerUtil;
import com.ccservice.inter.job.train.TrainInterfaceMethod;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.server.Server;
import com.ccservice.qunar.util.ExceptionUtil;
import com.ccservice.train.account.CookieLogic;
import com.ccservice.train.mqlistener.MQMethod;
import com.ccservice.train.mqlistener.TrainActiveMQ;
import com.ccservice.train.util.app.TrainAppSDK.Control;
import com.ccservice.train.util.app.TrainAppSDK.IFindMapFromDb;
import com.ccservice.train.util.app.TrainAppSDK.appMethod.BaseClient;

/**
 * 审核订单12306支付状态线程 
 * @time 2014年12月29日 下午7:06:40
 * @author fiend
 */
public class MyThreadQuery extends Thread {

    //    public static void main(String[] args) {
    //        //Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(50743809);
    //        //System.out.println(JSONObject.toJSONString(trainorder));
    //        //        SendPostandGet.submitPostTimeOut("http://www.qq.com", "", "UTF-8", 1).toString();
    //    }

    TrainSupplyMethod trainSupplyMethod;

    private final int QUERYMYORDERMAX = 10;

    long trainorderid;

    private Trainorder trainorder;

    private int interfacetype;

    private String tongchengcallbackurl;

    private String taobaocallbackurl;

    private String freeType;//String freeType = getSysconfigString_(name)

    private long ordersuccesstime;

    private int queryno;

    private long queryendtime = 7 * 60 * 1000;

    private long queryspacetime = 10 * 1000;

    public MyThreadQuery(long trainorderid, long ordersuccesstime, String tongchengcallbackurl, int queryno,
            String freeType, String taobaocallbackurl) {
        this.trainorderid = trainorderid;
        this.tongchengcallbackurl = tongchengcallbackurl;
        this.ordersuccesstime = ordersuccesstime;
        this.queryno = queryno;
        this.freeType = freeType;
        this.taobaocallbackurl = taobaocallbackurl;
        this.trainSupplyMethod = new TrainSupplyMethod();

    }

    public static String getTime(long ordersuccesstime) {
        SimpleDateFormat yyyyMMddHHmmss = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        String s = yyyyMMddHHmmss.format(ordersuccesstime);
        return s;

    }

    public void run() {
        //        String sql = "SELECT ID FROM T_TRAINORDER WHERE ID =" + this.trainorderid + " AND C_ISPLACEING =1";
        //        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        //        if (list.size() > 0) {
        //            trainSupplyMethod.writeRC(trainorderid, "已扣过钱款,拒绝审核", "审核接口", 2, 1);
        //            return;
        //        }
        try {
            Server.getInstance()
                    .getSystemService()
                    .findMapResultByProcedure(
                            "[TrainOrderQueryTimeOut_insert] @OrderId=" + this.trainorderid + ",@ordersuccesstime='"
                                    + getTime(this.ordersuccesstime) + "'");
        }
        catch (Exception e) {
            WriteLog.write("JobQueryMyOrder_MyThreadQuery", "订单号:" + this.trainorderid + "插入成功时间异常" + e);
        }
        //判断是否客人支付类型
        Boolean resultguest = isNeedPayUrl(false);
        if (resultguest) {
            try {
                String value = PropertyUtil.getValue("queryendtime", "train.properties");
                if (value != null && !"".equals(value)) {
                    queryendtime = Long.valueOf(value.trim());
                }
            }
            catch (Exception e) {
                e.printStackTrace();
                WriteLog.write("JobQueryMyOrder_MyThreadQuery", "订单号:" + this.trainorderid + "客人支付类型获取超时时间异常" + e);
            }
        }
        this.trainorder = Server.getInstance().getTrainService().findTrainorder(this.trainorderid);
        if (this.trainorder.getAgentcontact() != null && "1".equals(this.trainorder.getAgentcontact())) {
            this.freeType = "3";
        }
        WriteLog.write("JobQueryMyOrder_MyThreadQuery",
                "订单号:" + this.trainorderid + ":getIsplaceing:" + trainorder.getIsplaceing());
        if (trainorder.getIsplaceing() != null && trainorder.getIsplaceing() == 1) {
            trainSupplyMethod.writeRC(trainorderid, "已扣过钱款,拒绝审核", "审核接口", 2, 1);
            return;
        }
        //订单信息校验
        if (checkOrderInfo(this.trainorder)) {
            trainSupplyMethod.writeRC(trainorderid, "订单信息二次校验通过", "审核接口", 2, 1);
        }
        else {
            trainSupplyMethod.writeRC(trainorderid, "订单信息二次校验失败", "审核接口", 2, 1);
            trainSupplyMethod.upTrainorder("UPDATE T_TRAINORDER SET C_ISQUESTIONORDER=" + Trainorder.CAIGOUQUESTION
                    + "  WHERE ID=" + this.trainorderid);
            return;
        }
        trainSupplyMethod.upTrainorder("UPDATE T_TRAINORDER SET C_ISQUESTIONORDER=" + Trainorder.NOQUESTION
                + ",C_ISPLACEING=2,C_CONTROLNAME=NULL,C_OPERATEUID=0" + " WHERE ID=" + this.trainorderid);
        trainSupplyMethod.writeRC(this.trainorderid, "开始审核", "自动审核", 2, 1);
        this.interfacetype = getOrderAttribution();//确定订单归属 
        getsysconfigtime();
        if (this.trainorder.getOrderstatus() != Trainorder.ISSUED) {
            String result = queryMyOrder(trainorder);//正式逻辑
            WriteLog.write("JobQueryMyOrder_MyThreadQuery", "订单号:" + trainorder.getId() + ":queryMyOrderAll:" + result);
            orderrc(trainorder, result);//增加个审核结果的整体结果的日志记录
            if ("已支付,可以出票".equals(result)) {
                try {
                    Server.getInstance().getSystemService()
                            .findMapResultByProcedure("[TrainOrderQueryTimeOut_delete] @OrderId=" + this.trainorderid);
                }
                catch (Exception e) {
                    WriteLog.write("JobQueryMyOrder_MyThreadQuery", "订单号:" + trainorder.getId() + "删除异常" + e);
                }
                try {
                    orderrc(trainorder, "12306订单支付成功,已审核");
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                this.trainorder = Server.getInstance().getTrainService().findTrainorder(this.trainorderid);
                if (trainorder.getIsplaceing() != null && trainorder.getIsplaceing() == 1) {
                    trainSupplyMethod.writeRC(trainorderid, "已扣过钱款,拒绝出票", "审核接口", 2, 1);
                }
                else {
                    issue();//出票统一接口 
                }
            }
            else if ("支付账号信息格式有误，缺少>>/<<".equals(result) || "很抱歉，没有对应的12306账号".equals(result)
                    || "很抱歉，联程票查不了".equals(result) || "访问REP出错".equals(result)) {
                trainSupplyMethod.upTrainorder("UPDATE T_TRAINORDER SET  C_ISPLACEING=" + 5 + ",C_ISQUESTIONORDER="
                        + Trainorder.PAYINGQUESTION + " WHERE ID=" + this.trainorderid);
                orderrc(trainorder, result);
            }
            else {
                if (System.currentTimeMillis() - ordersuccesstime < queryendtime) {
                    //trainSupplyMethod.writeRC(this.trainorderid, "12306上未找到该已支付订单", "自动审核", 2, 1);
                    try {
                        Thread.sleep(queryspacetime);
                    }
                    catch (InterruptedException e) {
                    }
                    new TrainActiveMQ(MQMethod.QUERY, this.trainorderid, this.ordersuccesstime,
                            System.currentTimeMillis(), queryno++).sendqueryMQ();
                }
                else {
                    if (resultguest) {
                        String resultCallBackFail = callBackTongChengPayedFail(this.trainorder,
                                this.tongchengcallbackurl);
                        WriteLog.write("审核回调同程出票", "订单号:" + trainorder.getId() + ":客人支付类型超时回调出票失败resultCallBackFail:"
                                + resultCallBackFail);
                    }
                    trainSupplyMethod.upTrainorder("UPDATE T_TRAINORDER SET  C_ISPLACEING=" + 5 + ",C_ISQUESTIONORDER="
                            + Trainorder.PAYINGQUESTION + " WHERE ID=" + this.trainorderid);
                    String rccount = "12306上未找到该已支付订单,请客服到12306官网审核支付情况";
                    if (result != null && "12306账号获取失败".equals(result)) {
                        rccount = result + ",请重新审核,如果受影响订单范围大,请联系值班技术帮助审核";
                    }
                    trainSupplyMethod.writeRC(this.trainorderid, rccount, "自动审核", 2, 1);
                }
            }
        }

    }

    /**
     * 获取系统配置的审核结束时间与审核间隔时间 
     * @time 2015年1月27日 下午8:18:53
     * @author fiend
     */
    private void getsysconfigtime() {
        long sys_queryendtime = Long.valueOf(trainSupplyMethod.getSysconfigString("queryendtime"));
        long sys_queryspacetime = Long.valueOf(trainSupplyMethod.getSysconfigString("queryspacetime"));
        if (sys_queryendtime > 0) {
            this.queryendtime = 60 * 1000 * sys_queryendtime;
        }
        if (sys_queryspacetime > 0) {
            this.queryspacetime = 1000 * sys_queryspacetime;
        }
    }

    /**
     * 确定订单归属 
     * @time 2015年1月27日 下午7:16:45
     * @author fiend
     */
    private int getOrderAttribution() {
        //        long dangqianjiekouagentid = Long.valueOf(trainSupplyMethod.getSysconfigString("dangqianjiekouagentid"));
        //        long tongcheng_agentid = Long.valueOf(trainSupplyMethod.getSysconfigString("tongcheng_agentid"));
        //        long qunar_agentid = Long.valueOf(trainSupplyMethod.getSysconfigString("qunar_agentid"));
        //        if (qunar_agentid == this.trainorder.getAgentid() && 0 == dangqianjiekouagentid) {
        //            this.interfacetype = TrainInterfaceMethod.QUNAR;
        //        }
        //        else if ((tongcheng_agentid == this.trainorder.getAgentid() && 1 == dangqianjiekouagentid)
        //                || trainSupplyMethod.isOtherJiekou(this.trainorderid)) {
        //            this.interfacetype = TrainInterfaceMethod.TONGCHENG;
        //        }
        //        else {
        //            this.interfacetype = TrainInterfaceMethod.HTHY;
        //        }
        this.interfacetype = this.trainorder.getInterfacetype() != null && this.trainorder.getInterfacetype() > 0 ? this.trainorder
                .getInterfacetype() : Server.getInstance().getInterfaceTypeService()
                .getTrainInterfaceType(this.trainorderid);
        return this.interfacetype;
    }

    /**
     * 说明:根据订单号检验12306支付信息
     * @param trainorderid
     * @return 真实支付信息
     * @time 2014年8月30日 上午10:55:40
     * @author yinshubin
     */
    public String queryMyOrder(Trainorder trainorder) {
        String result = "12306出票查询";
        String cookieString = "";
        String pname = "";
        String logname = "";
        String logpassword = "";
        long trainorderid = trainorder.getId();
        Customeruser customeruser = null;
        String extnumber = trainorder.getExtnumber();
        String ticketdate = trainorder.getCreatetime().toString().substring(0, 10);
        if (null != extnumber && !"1".equals(extnumber) && !"0".equals(extnumber)) {
            extnumber = extnumber.trim();
            WriteLog.write("JobQueryMyOrder_MyThreadQuery", "订单号:" + trainorder.getId() + ":queryMyOrder:电子单号是:"
                    + extnumber);
            if (trainorder.getPassengers() != null && trainorder.getPassengers().size() > 0) {
                if (null != trainorder.getSupplyaccount() && !"".equals(trainorder.getSupplyaccount())) {
                    if (trainorder.getOrdertype() == 1 && !trainorder.getSupplyaccount().contains("/")) {
                        //                        result = "账号格式有误，缺少>>/<<";
                        WriteLog.write("JobQueryMyOrder_MyThreadQuery", "订单号:" + trainorder.getId() + ":账号格式有误:"
                                + trainorder.getSupplyaccount());
                        //                        return result;
                    }
                    customeruser = trainSupplyMethod.getCustomerUserBy12306Account(trainorder, false);
                }
                else {
                    result = "很抱歉，没有对应的12306账号";
                    return result;
                }
                if (null != customeruser) {
                    if (customeruser.getLoginname() == null) {
                        result = "12306账号获取失败";
                        return result;
                    }
                    WriteLog.write("JobQueryMyOrder_MyThreadQuery", "订单号:" + trainorderid + ":queryMyOrder:12306账号是:"
                            + customeruser.getLoginname());
                    //已登录
                    if (1 == customeruser.getState()) {
                        if ("".equals(cookieString) || cookieString.equals(customeruser.getCardnunber())) {
                            cookieString = customeruser.getCardnunber();
                            logname = customeruser.getLoginname();
                            logpassword = customeruser.getLogpassword();
                        }
                    }
                    else {
                        customeruser = index12306(trainorder, customeruser);
                        result = customeruser.getCardnunber();
                        logname = customeruser.getLoginname();
                        logpassword = customeruser.getLogpassword();
                        if (result != null && result.contains("JSESSIONID")) {
                            cookieString = result;
                            result = "12306出票查询";
                        }
                        else {
                            result = "很抱歉，12306账号未登录";
                            //账号系统
                            if (customeruser.isFromAccountSystem()) {
                                trainSupplyMethod.FreeNoCare(customeruser);
                            }
                            return result;
                        }
                    }
                }
                else {
                    result = "很抱歉，没有对应的12306账号";
                }
                if ("12306出票查询".equals(result)) {
                    int i = 0;
                    do {
                        if (i > 0) {
                            try {
                                Thread.sleep(800l);
                            }
                            catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        result = queryOrderRep(trainorderid, customeruser, extnumber, ticketdate, pname, logname,
                                logpassword);
                        i++;
                    }
                    while (!"已支付,可以出票".equals(result) && i < QUERYMYORDERMAX);
                    //支付成功
                    if ("已支付,可以出票".equals(result)) {
                        freecustomeruser(customeruser);
                    }
                    else if (result != null && result.contains("用户未登录")) {
                        WriteLog.write("JobQueryMyOrder_MyThreadQuery", "订单号:" + trainorder.getId()
                                + ":queryMyOrder:wzc:电子单号是:" + extnumber + ":未登录释放账号");
                        trainSupplyMethod.FreeNoLogin(customeruser);
                    }
                    //账号系统
                    else if (customeruser.isFromAccountSystem()) {
                        trainSupplyMethod.FreeNoCare(customeruser);
                    }
                }
            }
        }
        return result;
    }

    /**
     * 说明:比对支付数据(暂时只比对是否支付成功)
     * @param result
     * @param extnumber
     * @return 在12306查询到的支付结果
     * @time 2014年8月30日 上午10:57:24
     * @author yinshubin
     */
    public String comparisonDataList(String result, String extnumber) {
        String comparison = "查询失败";
        JSONObject jso = JSONObject.parseObject(result);
        if (jso.containsKey("status") && "true".equals(jso.getString("status"))) {
            if (jso.containsKey("data")) {
                JSONObject jsodata = jso.getJSONObject("data");
                if (jsodata.containsKey("OrderDTODataList") && jsodata.containsKey("order_total_number")) {
                    JSONArray jsonaodl = jsodata.getJSONArray("OrderDTODataList");
                    comparison = getExamineResult(extnumber, comparison, jsonaodl);
                }
            }
        }
        return comparison;
    }

    /**
     * @author Baozz
     * @time 2018年2月5日上午9:33:08
     * @describe 得到审核的结果
     */
    private String getExamineResult(String extnumber, String comparison, JSONArray jsonaodl) {
        if (jsonaodl.size() == 0) {
            comparison = "该电子单号未支付";
        }
        else {
            for (int i = 0; i < jsonaodl.size(); i++) {
                String odlstr = jsonaodl.getJSONObject(i).toString();
                if (odlstr.contains(extnumber)
                        && (odlstr.contains("已支付") || odlstr.contains("已出票") || odlstr.contains("已退票") || odlstr
                                .contains("已改签"))) {
                    comparison = "已支付,可以出票";
                    break;
                }
            }
        }
        return comparison;
    }

    /**
     * 说明:遇到未登录时，将账号登录后再进行检查
     * @param trainorderid
     * @param logname
     * @param logpassword
     * @return  cookie
     * @time 2014年8月30日 上午10:58:40
     * @author yinshubin
     */
    public Customeruser index12306(Trainorder trainorder, Customeruser customeruser) {
        //以未登录释放
        trainSupplyMethod.FreeNoLogin(customeruser);
        //账号系统重新获取
        return trainSupplyMethod.getCustomerUserBy12306Account(trainorder, true);
    }

    /**
     * 访问REP 判断订单是否支付成功
     * 
     * @param url
     * @param trainorderid
     * @param cookieString
     * @param extnumber
     * @param ticketdate
     * @param passengerName
     * @param logname
     * @param logpassword
     * @return
     * @time 2014年12月24日 下午5:02:11
     * @author fiend
     */
    public String queryOrderRep(long trainorderid, Customeruser user, String extnumber, String ticketdate,
            String passengerName, String logname, String logpassword) {
        String cookieString = user.getCardnunber();
        //REP
        RepServerBean rep = RepServerUtil.getRepServer(user, false);
        try {
            cookieString = URLEncoder.encode(cookieString, "UTF-8");
            passengerName = URLEncoder.encode(passengerName, "UTF-8");
        }
        catch (UnsupportedEncodingException e) {
        }
        String url = rep.getUrl();
        //是否走手机端逻辑
        int orderType = trainorder.getOrdertype();//获取到订单类型
        String wayByorderId = new Control().orderWayByorderId(new IFindMapFromDb() {
            @Override
            public List findMapResultByProcedure(String paramString) {
                return Server.getInstance().getSystemService().findMapResultByProcedure(paramString);
            }
        }, trainorderid, "check");
        boolean isPhone = ("1".equals(orderType) || "3".equals(orderType)) && isPhoneOrder(trainorderid);//(代购||托管)&&走手机端
        //拼rep请求参数
        String par = getPram(extnumber, cookieString, ticketdate, passengerName, logname, logpassword, user, rep,
                isPhone);
        WriteLog.write("JobQueryMyOrder_MyThreadQuery", "订单号:" + trainorderid + ";电子单号:" + extnumber
                + ":ajaxQueryMyOrder:检验12306支付信息:" + url + "(:)" + par);
        String result = "-11";
        Long l1 = System.currentTimeMillis();
        try {
            if ("2".equals(wayByorderId)) {
                //app检票口数据入库
                operationWicket(trainorderid, user, result, logname, logpassword, extnumber);
                result = new BaseClient().getCompleteOrderByQueryOrder(logname, logpassword, extnumber);
            }
            else {
                saveWicket(cookieString, logname, logpassword, user, rep, url);
                result = SendPostandGet.submitPostTimeOutFiend(url, par, "UTF-8", 240000).toString();
                //用户未登录
                if ((result.contains("用户未登录") || result.contains("系统繁忙，请稍后重试")) && RepServerUtil.changeRepServer(user)) {
                    //切换REP
                    rep = RepServerUtil.getTaoBaoTuoGuanRepServer(user, false);
                    //类型正确
                    if (rep.getType() == 1) {
                        //REP地址
                        url = rep.getUrl();
                        //重拼参数
                        //拼rep请求参数
                        par = getPram(extnumber, cookieString, ticketdate, passengerName, logname, logpassword, user,
                                rep, isPhone);
                        //记录日志
                        WriteLog.write("JobQueryMyOrder_MyThreadQuery", "订单号:" + trainorderid + ";电子单号:" + extnumber
                                + ":changeRepServer:检验12306支付信息:" + url + "(:)" + par);
                        //重新请求
                        result = SendPostandGet.submitPostTimeOutFiend(url, par, "UTF-8", 60 * 1000).toString();
                    }
                }
            }

        }
        catch (Exception e1) {
            ExceptionUtil.writelogByException("MyThreadQuery_queryOrderRep_Exception", e1, "出现异常==>订单号为:"
                    + trainorderid);
        }
        WriteLog.write("JobQueryMyOrder_MyThreadQuery", "订单号:" + trainorderid + ";途径类型:==>" + wayByorderId
                + "<==;电子单号:" + extnumber + ":ajaxQueryMyOrder:检验12306支付信息:耗时[" + (System.currentTimeMillis() - l1)
                + "]返回结果:" + result);
        if (result != null && result.contains("用户未登录")) {
            return "用户未登录";
        }
        if (result != null && !"-11".equals(result)) {
            try {
                //刷新下cookie
                CookieLogic.getInstance().refresh(result, user);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if ("已支付,可以出票".equals(result)) {
                    return result;
                }
                //存下检票口数据
                return comparisoneach(result, extnumber, isPhone);
            }
            catch (Exception e) {
                return "访问REP出错";
            }
        }
        else {
            return "访问REP出错";
        }
    }

    /**
     * app检票口信息入库
     * 
     * @param trainorderid
     * @param user
     * @param result
     * @param logname
     * @param logpassword
     * @param extnumber
     * @time 2018年4月20日 下午3:15:30
     * @author YangFan
     */
    public void operationWicket(long trainorderid, Customeruser user, String result, String logname,
            String logpassword, String extnumber) {
        String wicketStr = "";
        try {
            wicketStr = new BaseClient().getWicket(logname, logpassword, extnumber);
            if (wicketStr != null && !"".equals(wicketStr) && wicketStr.contains("检票口")) {
                wicketStr = JSONObject.parseObject(wicketStr).getString("wicket").trim();
                insertWicketAndRefresh(wicketStr, result, user);
            }
            WriteLog.write("JobQueryMyOrder_operationWicket", "订单号:" + trainorderid + "--->>返回结果:" + wicketStr);
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("MyThreadQuery_operationWicket_Exception", e, "订单号:" + trainorderid
                    + "--->app返回:" + wicketStr);
        }
    }

    public static void main(String[] args) {
        //        String comparisoneach = new MyThreadQuery().comparisoneach(
        //                "{\"type\":\"list\",\"data\":{\"validateMessagesShowId\":\"_validatorMessage\",\"status\":true,\"httpstatus\":200,\"data\":{\"order_total_number\":\"1\",\"show_catering_button\":true,\"OrderDTODataList\":[{\"sequence_no\":\"EA96323001\",\"order_date\":\"2018-02-05 08:37:01\",\"ticket_totalnum\":2,\"ticket_price_all\":12100,\"cancel_flag\":\"N\",\"resign_flag\":\"1\",\"return_flag\":\"Y\",\"print_eticket_flag\":\"Y\",\"pay_flag\":\"N\",\"pay_resign_flag\":\"N\",\"confirm_flag\":\"N\",\"tickets\":[{\"stationTrainDTO\":{\"trainDTO\":{\"train_no\":\"24000G67110F\"},\"station_train_code\":\"G6711\",\"from_station_telecode\":\"BXP\",\"from_station_name\":\"北京西\",\"start_time\":\"1970-01-01 18:33:00\",\"to_station_telecode\":\"BMP\",\"to_station_name\":\"保定东\",\"arrive_time\":\"1970-01-01 19:14:00\",\"distance\":\"139\"},\"passengerDTO\":{\"passenger_name\":\"何番\",\"passenger_id_type_code\":\"1\",\"passenger_id_type_name\":\"二代身份证\",\"passenger_id_no\":\"511304199009135811\",\"total_times\":\"98\"},\"ticket_no\":\"EA96323001105013A\",\"sequence_no\":\"EA96323001\",\"batch_no\":\"1\",\"train_date\":\"2018-03-05 00:00:00\",\"coach_no\":\"05\",\"coach_name\":\"05\",\"seat_no\":\"013A\",\"seat_name\":\"13A号\",\"seat_flag\":\"0\",\"seat_type_code\":\"O\",\"seat_type_name\":\"二等座\",\"ticket_type_code\":\"1\",\"ticket_type_name\":\"成人票\",\"reserve_time\":\"2018-02-05 08:37:01\",\"limit_time\":\"2018-02-05 08:37:01\",\"lose_time\":\"2018-02-05 09:07:01\",\"pay_limit_time\":\"2018-02-05 09:07:01\",\"ticket_price\":6350,\"print_eticket_flag\":\"Y\",\"resign_flag\":\"1\",\"return_flag\":\"Y\",\"confirm_flag\":\"N\",\"pay_mode_code\":\"N\",\"ticket_status_code\":\"a\",\"ticket_status_name\":\"已支付\",\"cancel_flag\":\"N\",\"amount_char\":0,\"trade_mode\":\"J\",\"start_train_date_page\":\"2018-03-05 18:33\",\"str_ticket_price_page\":\"63.5\",\"come_go_traveller_ticket_page\":\"N\",\"return_deliver_flag\":\"N\",\"deliver_fee_char\":\"\",\"is_need_alert_flag\":false,\"is_deliver\":\"N\",\"dynamicProp\":\"\",\"fee_char\":\"\",\"insure_query_no\":\"\",\"column_nine_msg\":\"\",\"lc_flag\":\"L\",\"integral_pay_flag\":\"N\"},{\"stationTrainDTO\":{\"trainDTO\":{\"train_no\":\"26000G628804\"},\"station_train_code\":\"G6288\",\"from_station_telecode\":\"BMP\",\"from_station_name\":\"保定东\",\"start_time\":\"1970-01-01 20:25:00\",\"to_station_telecode\":\"TXP\",\"to_station_name\":\"天津西\",\"arrive_time\":\"1970-01-01 21:20:00\",\"distance\":\"150\"},\"passengerDTO\":{\"passenger_name\":\"何番\",\"passenger_id_type_code\":\"1\",\"passenger_id_type_name\":\"二代身份证\",\"passenger_id_no\":\"511304199009135811\",\"total_times\":\"98\"},\"ticket_no\":\"EA96323001207011A\",\"sequence_no\":\"EA96323001\",\"batch_no\":\"2\",\"train_date\":\"2018-03-05 00:00:00\",\"coach_no\":\"07\",\"coach_name\":\"07\",\"seat_no\":\"011A\",\"seat_name\":\"11A号\",\"seat_flag\":\"0\",\"seat_type_code\":\"O\",\"seat_type_name\":\"二等座\",\"ticket_type_code\":\"1\",\"ticket_type_name\":\"成人票\",\"reserve_time\":\"2018-02-05 08:37:01\",\"limit_time\":\"2018-02-05 08:37:01\",\"lose_time\":\"2018-02-05 09:07:01\",\"pay_limit_time\":\"2018-02-05 09:07:01\",\"ticket_price\":5750,\"print_eticket_flag\":\"Y\",\"resign_flag\":\"1\",\"return_flag\":\"Y\",\"confirm_flag\":\"N\",\"pay_mode_code\":\"N\",\"ticket_status_code\":\"a\",\"ticket_status_name\":\"已支付\",\"cancel_flag\":\"N\",\"amount_char\":0,\"trade_mode\":\"J\",\"start_train_date_page\":\"2018-03-05 20:25\",\"str_ticket_price_page\":\"57.5\",\"come_go_traveller_ticket_page\":\"N\",\"return_deliver_flag\":\"N\",\"deliver_fee_char\":\"\",\"is_need_alert_flag\":false,\"is_deliver\":\"N\",\"dynamicProp\":\"\",\"fee_char\":\"\",\"insure_query_no\":\"\",\"column_nine_msg\":\"\",\"lc_flag\":\"L\",\"integral_pay_flag\":\"N\"}],\"reserve_flag_query\":\"p\",\"if_show_resigning_info\":\"N\",\"recordCount\":\"1\",\"isNeedSendMailAndMsg\":\"N\",\"array_passser_name_page\":[\"何番\"],\"from_station_name_page\":[\"北京西\",\"保定东\"],\"to_station_name_page\":[\"保定东\",\"天津西\"],\"start_train_date_page\":\"2018-03-05 18:33\",\"start_time_page\":\"20:25\",\"arrive_time_page\":\"21:20\",\"train_code_page\":\"G6288\",\"ticket_total_price_page\":\"121.0\",\"come_go_traveller_order_page\":\"N\",\"canOffLinePay\":\"N\",\"if_deliver\":\"N\",\"insure_query_no\":\"\"}]},\"messages\":[],\"validateMessages\":{}},\"cookie\":\"\"}",
        //                "EA96323001", false);
        //        String comparisoneach1 = new MyThreadQuery().comparisoneach(
        //                "{\"type\":\"list\",\"data\":{\"validateMessagesShowId\":\"_validatorMessage\",\"status\":true,\"httpstatus\":200,\"data\":{\"order_total_number\":\"1\",\"show_catering_button\":true,\"OrderDTODataList\":[{\"sequence_no\":\"E610938791\",\"order_date\":\"2018-02-05 08:59:59\",\"ticket_totalnum\":1,\"ticket_price_all\":33100,\"cancel_flag\":\"N\",\"resign_flag\":\"1\",\"return_flag\":\"Y\",\"print_eticket_flag\":\"Y\",\"pay_flag\":\"N\",\"pay_resign_flag\":\"N\",\"confirm_flag\":\"N\",\"tickets\":[{\"stationTrainDTO\":{\"trainDTO\":{\"train_no\":\"550000122882\"},\"station_train_code\":\"1228\",\"from_station_telecode\":\"SHH\",\"from_station_name\":\"上海\",\"start_time\":\"1970-01-01 20:22:00\",\"to_station_telecode\":\"HLD\",\"to_station_name\":\"葫芦岛\",\"arrive_time\":\"1970-01-01 20:33:00\",\"distance\":\"1760\"},\"passengerDTO\":{\"passenger_name\":\"王朋\",\"passenger_id_type_code\":\"1\",\"passenger_id_type_name\":\"二代身份证\",\"passenger_id_no\":\"211404199706257616\",\"total_times\":\"98\"},\"ticket_no\":\"E6109387911090142\",\"sequence_no\":\"E610938791\",\"batch_no\":\"1\",\"train_date\":\"2018-02-09 00:00:00\",\"coach_no\":\"09\",\"coach_name\":\"09\",\"seat_no\":\"0142\",\"seat_name\":\"14号中铺\",\"seat_flag\":\"0\",\"seat_type_code\":\"3\",\"seat_type_name\":\"硬卧\",\"ticket_type_code\":\"1\",\"ticket_type_name\":\"成人票\",\"reserve_time\":\"2018-02-05 08:59:59\",\"limit_time\":\"2018-02-05 08:59:59\",\"lose_time\":\"2018-02-05 09:29:59\",\"pay_limit_time\":\"2018-02-05 09:29:59\",\"ticket_price\":33100,\"print_eticket_flag\":\"Y\",\"resign_flag\":\"1\",\"return_flag\":\"Y\",\"confirm_flag\":\"N\",\"pay_mode_code\":\"N\",\"ticket_status_code\":\"a\",\"ticket_status_name\":\"已支付\",\"cancel_flag\":\"N\",\"amount_char\":0,\"trade_mode\":\"E\",\"start_train_date_page\":\"2018-02-09 20:22\",\"str_ticket_price_page\":\"331.0\",\"come_go_traveller_ticket_page\":\"N\",\"return_deliver_flag\":\"N\",\"deliver_fee_char\":\"\",\"is_need_alert_flag\":false,\"is_deliver\":\"N\",\"dynamicProp\":\"\",\"fee_char\":\"\",\"insure_query_no\":\"\",\"column_nine_msg\":\"\",\"lc_flag\":\"8\",\"integral_pay_flag\":\"N\"}],\"reserve_flag_query\":\"p\",\"if_show_resigning_info\":\"N\",\"recordCount\":\"1\",\"isNeedSendMailAndMsg\":\"N\",\"array_passser_name_page\":[\"王朋\"],\"from_station_name_page\":[\"上海\"],\"to_station_name_page\":[\"葫芦岛\"],\"start_train_date_page\":\"2018-02-09 20:22\",\"start_time_page\":\"20:22\",\"arrive_time_page\":\"20:33\",\"train_code_page\":\"1228\",\"ticket_total_price_page\":\"331.0\",\"come_go_traveller_order_page\":\"N\",\"canOffLinePay\":\"N\",\"if_deliver\":\"N\",\"insure_query_no\":\"\"}]},\"messages\":[],\"validateMessages\":{}},\"cookie\":\"current_captcha_type=Z; JSESSIONID=5AA73FE9A8FD7DD6935B1B06D9C930F4; BIGipServerotn=753926666.64545.0000; route=c5c62a339e7744272a54643b3be5bf64\"}",
        //                "E610938791", false);
        //        System.out.println(comparisoneach+","+comparisoneach1);
    }

    /**
     * 解析12306返回结果 
     * @param result
     * @param extnumber
     * @param isPhoneOrder
     * @return
     * @time 2014年12月24日 下午5:02:50
     * @author fiend
     */
    public String comparisoneach(String result, String extnumber, boolean isPhoneOrder) {
        JSONObject jso = new JSONObject();
        if (!isPhoneOrder) {//走手机端审核(统一)
            jso = JSONObject.parseObject(result);
        }
        if (jso.containsKey("type") && jso.getString("type").equals("detail") && result.contains("已支付")) {
            result = "已支付,可以出票";
        }
        if (jso.containsKey("type") && jso.getString("type").equals("list")) {
            result = comparisonDataList(jso.getString("data"), extnumber);
        }
        return result;
    }

    /**
     * 易定行出票 
     * @param trainorder
     * @time 2014年12月26日 上午9:27:51
     * @author fiend
     */
    //    public void issueYEE(Trainorder trainorder) {
    //        new JobGenerateOrder().yeeIssue(trainorder.getId(), ordercustomeruser(trainorder));
    //    }

    /**
     * 审核后书写操作记录
     * @param trainorder
     * @param isissure 审核已支付并且出票成功TRUE
     * @time 2014年12月26日 下午12:10:47
     * @author fiend
     */
    public void orderrc(Trainorder trainorder, String Content) {
        Trainorderrc rcrefund = new Trainorderrc();
        rcrefund.setStatus(trainorder.getOrderstatus());
        rcrefund.setContent(Content);
        rcrefund.setCreateuser("12306审核");
        rcrefund.setOrderid(trainorder.getId());
        rcrefund.setYwtype(1);
        try {
            Server.getInstance().getTrainService().createTrainorderrc(rcrefund);
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("MyThreadQuery_orderrc_Exception", e, Content);
        }
    }

    /*下面是公用方法，因为该类没有办法再继承，所以复制过来*/
    /**
     * 根据sysconfig的name获得value
     * 实时 如果是判断的必须调用实时接口
     * @param name
     * @return
     */
    @SuppressWarnings("unchecked")
    public String getSysconfigString_(String name) {
        String result = "-1";
        List<Sysconfig> sysoconfigs = Server.getInstance().getSystemService()
                .findAllSysconfig("WHERE C_NAME='" + name + "'", "", -1, 0);
        if (sysoconfigs.size() > 0) {
            result = sysoconfigs.get(0).getValue();
        }
        return result;
    }

    /**
     * 通过订单查找下单的12306账号 
     * @param trainorder
     * @return
     * @time 2014年12月25日 下午9:33:36
     * @author fiend
     */
    @SuppressWarnings("rawtypes")
    public long ordercustomeruser(Trainorder trainorder) {
        String sql = "SELECT ID FROM T_CUSTOMERUSER WHERE C_LOGINNAME='" + trainorder.getSupplyaccount().split("/")[0]
                + "'";
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        Map map = (Map) list.get(0);
        return Long.valueOf(map.get("ID").toString());
    }

    /**
     * 占用一个账号后的解锁，
     * 支付完成等调用
     * @param cust
     * @time 2014年12月23日 下午6:31:03
     * @author chendong
     */
    public void freecustomeruser(Customeruser cust) {
        if (cust != null && cust.isFromAccountSystem()) {
            int FreeType = AccountSystem.FreeNoCare;
            Timestamp DepartTime = AccountSystem.NullDepartTime;
            if ("2".equals(freeType)) {
                FreeType = AccountSystem.FreeCurrent;
            }
            else if ("3".equals(freeType)) {
                FreeType = AccountSystem.FreeDepart;
                DepartTime = getDeparttime(getDeparttime());
            }
            trainSupplyMethod.FreeUserFromAccountSystem(cust, FreeType, AccountSystem.TwoFree,
                    AccountSystem.ZeroCancel, DepartTime);
            return;
        }
        String sql = "UPDATE T_CUSTOMERUSER SET C_ENNAME='1' where id=" + cust.getId();
        cust.setEnname("1");
        if ("2".equals(this.freeType)) {//如果freeType的类型是2,代表用的模式是这个账号一天只能用一次
            cust.setIsenable(4);
            cust.setState(0);
            sql = "UPDATE T_CUSTOMERUSER SET C_ENNAME='0',C_STATE=0,C_ISENABLE=4 where id=" + cust.getId();
        }
        if ("3".equals(this.freeType)) {//如果freeType的类型是2,代表用的模式是只有该账号所在订单发车后才可以用
            cust.setIsenable(3);//暂时封起来
            cust.setState(0);
            //            update T_CUSTOMERUSER set C_BIRTHDAY='2015-03-13 16:50' where ID=20854
            String Departtime = getDeparttime();
            if (!"-1".equals(Departtime)) {
                Timestamp birthday = getDeparttime(Departtime);
                cust.setBirthday(birthday);
                Departtime = ",C_BIRTHDAY='" + Departtime + "'";
            }
            else {
                Departtime = "";
            }
            sql = "UPDATE T_CUSTOMERUSER SET C_ENNAME='0'" + Departtime + ",C_STATE=0,C_ISENABLE=3 where id="
                    + cust.getId();
        }
        int count = 0;
        try {
            count = Server.getInstance().getSystemService().excuteAdvertisementBySql(sql);
        }
        catch (Exception e) {
            WriteLog.write("自动审核_账号更新", "账号更新异常:" + this.trainorder.getId() + ":sql:" + sql);
            net.sf.json.JSONObject jsonobject = new net.sf.json.JSONObject();
            jsonobject.put("customeruserObject", cust);
            sendMQmessage("update_12306account_err", jsonobject.toString());
        }
        WriteLog.write("JobQueryMyOrder_MyThreadQuery", "订单号:" + trainorderid + ":" + count + ";count:" + sql);
    }

    private Timestamp getDeparttime(String departtime) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis() + 4 * 60 * 60 * 1000);
        try {
            if (departtime.length() >= 16) {
                departtime = departtime.substring(0, 16);
            }
            timestamp = new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(departtime).getTime());
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        return timestamp;

    }

    private String getDeparttime() {
        try {
            return this.trainorder.getPassengers().get(0).getTraintickets().get(0).getDeparttime();
        }
        catch (Exception e) {
            return "-1";
        }
    }

    /**
     * 
     * @param QUEUE_NAME
     * @param message
     * 
     * @time 2015年1月27日 上午11:48:04
     * @author chendong
     */
    public void sendMQmessage(String QUEUE_NAME, String message) {
        String url = this.trainSupplyMethod.getSysconfigString("activeMQ_url");
        //        String url = "tcp://192.168.0.5:61616";
        try {
            ActiveMQUtil.sendMessage(url, QUEUE_NAME, message);
        }
        catch (Exception e) {
        }
    }

    /**
     * 
     * @author 之至
     * @time 2016年12月21日 下午5:53:15
     * @Description 订单信息校验
     * @param trainorder
     * @return
     */
    public boolean checkOrderInfo(Trainorder trainorder) {
        boolean result = true;
        for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
            for (Trainticket trainticket : trainpassenger.getTraintickets()) {
                String seatNoString = trainticket.getSeatno();
                String coachsString = trainticket.getCoach();
                if (null == seatNoString || null == coachsString || "".equals(seatNoString) || "".equals(coachsString)) {
                    result = false;
                }
            }
        }
        return result;
    }

    /**
     * 得到rep请求参数
     * 
     * @param extnumber
     * @param cookieString
     * @param ticketdate
     * @param passengerName
     * @param logname
     * @param logpassword
     * @param user
     * @param rep
     * @param isPhoneOrder
     * @return
     * @time 2017年3月1日 下午6:17:49
     * @author YangFan
     */
    private String getPram(String extnumber, String cookieString, String ticketdate, String passengerName,
            String logname, String logpassword, Customeruser user, RepServerBean rep, boolean isPhoneOrder) {
        String datatypeflag = "26";
        String parm = "";
        if (isPhoneOrder) {//(代购||托管)&&走手机端
            datatypeflag = "2026";
            parm = "datatypeflag=" + datatypeflag + "&orderId=" + extnumber
                    + trainSupplyMethod.JoinCommonAccountPhone(user);//向rep发参数(帐号、密码、startTime)新增加
        }
        else {
            parm = "datatypeflag=" + datatypeflag + "&trainorderid=" + trainorderid + "&extnumber=" + extnumber
                    + "&cookie=" + cookieString + "&ticketdate=" + ticketdate + "&name=" + passengerName + "&logname="
                    + logname + "&logpassword=" + logpassword + trainSupplyMethod.JoinCommonAccountInfo(user, rep);
        }
        WriteLog.write("JobQueryMyOrder_MyThreadQuery", "订单号:" + trainorderid + ";电子单号:" + extnumber + "请求parm：" + parm);
        return parm;
    }

    /**
     * 判断订单是否走手机端审核
     * 
     * @param trainorderid
     * @return
     * @time 2017年2月14日 上午10:24:04
     * @author YangFan
     */
    private static boolean isPhoneOrder(long trainorderid) {
        try {
            //审核订单是否走手机端，判定改天           
            String isPhoneSql = "[TrainRepAction_ChangeActionWay] @OrderId =" + trainorderid + ",@ActionName='check'";
            List list = Server.getInstance().getSystemService().findMapResultByProcedure(isPhoneSql);
            if (list.size() > 0) {
                Map map = (Map) list.get(0);
                if ("1".equals(map.containsKey("IsPhone") ? map.get("IsPhone").toString() : "")) {
                    return true;
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //====================================================回调出票接口开始======================================================================

    /**
     * 保存检票口
     * 
     * @param cookieString
     * @param logname
     * @param logpassword
     * @param user
     * @param rep
     * @time 2017年3月29日 下午7:37:18
     * @author fiend
     */

    private void saveWicket(String cookieString, String logname, String logpassword, Customeruser user,
            RepServerBean rep, String url) {
        try {
            String traino = trainorder.getPassengers().get(0).getTraintickets().get(0).getTrainno();
            //是高铁或者动车
            if (traino.startsWith("G") || traino.startsWith("D")) {
                //没有检票口
                if (trainorder.getWicket() == null || "".equals(trainorder.getWicket())) {
                    String parm = "datatypeflag=99&trainorderid=" + trainorderid + "&cookie=" + cookieString
                            + "&logname=" + logname + "&logpassword=" + logpassword
                            + trainSupplyMethod.JoinCommonAccountInfo(user, rep);
                    WriteLog.write("MyThreadQuery_saveWicket", this.trainorder.getId() + "--->" + url + "--->" + parm);
                    String result = SendPostandGet.submitPostTimeOutFiend(url, parm, "UTF-8", 240000).toString();
                    WriteLog.write("MyThreadQuery_saveWicket", this.trainorder.getId() + "--->" + result);
                    String wicket = "";
                    try {
                        if (result.startsWith("检票口")) {//String类型的格式
                            wicket = result;
                        }
                        else if (result.contains("检票口")) {//兼容json返回格式
                            JSONObject jsonObject = JSONObject.parseObject(result);
                            wicket = jsonObject.getString("wicket");
                        }
                        //执行参数法
                        insertWicketAndRefresh(wicket, result, user);
                    }
                    catch (Exception e) {
                        ExceptionUtil.writelogByException("MyThreadQuery_saveWicket_Exception", e, "orderId:"
                                + this.trainorder.getId());
                    }
                }
            }
        }
        catch (Exception e) {
            //            ExceptionUtil.writelogByException("MyThreadQuery_saveWicket_Exception", e, this.trainorder.getId());
            com.ccservice.qunar.util.ExceptionUtil.writelogByException("MyThreadQuery_saveWicket_Exception", e);
        }
    }

    /**
     * 刷新cookie
     * 存入wicket
     * 
     * @param wicket
     * @param result
     * @param user
     * @time 2017年7月17日 下午7:26:22
     * @author YangFan
     */
    private void insertWicketAndRefresh(String wicket, String result, Customeruser user) {
        try {//刷新cookie
            CookieLogic.getInstance().refresh(result, user);
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("MyThreadQuery_saveWicket_Exception", e, "--->>>刷新cookie异常订单--orderId:"
                    + this.trainorder.getId());
        }
        if ((!"".equals(wicket)) && (wicket != null)) {//有值才存储
            try {
                String procedure = "[sp_T_TRAINORDER__Update_Wicket] @ID=" + trainorder.getId() + ",@Wicket='" + wicket
                        + "'";
                Server.getInstance().getSystemService().findMapResultByProcedure(procedure);
                WriteLog.write("MyThreadQuery_saveWicket", this.trainorder.getId() + "--->wicket：" + wicket);
            }
            catch (Exception e) {
                ExceptionUtil.writelogByException("MyThreadQuery_saveWicket_Exception", e, "--->>>orderId:"
                        + this.trainorder.getId() + "--->>>wicket:" + wicket);
            }
        }
    }

    //====================================================回调出票接口开始======================================================================

    /**
     * 出票统一接口 
     * @return
     * @time 2015年1月27日 上午11:22:02
     * @author fiend
     */
    public void issue() {
        if (TrainInterfaceMethod.HTHY == this.interfacetype) {
            issueYEE();//易定行出票
        }
        else if (TrainInterfaceMethod.QUNAR == this.interfacetype) {
            issueQUNAR();//qunar出票 
        }
        else if ((TrainInterfaceMethod.TONGCHENG == this.interfacetype)
                || (TrainInterfaceMethod.WITHHOLDING_AFTER == this.interfacetype)
                || (TrainInterfaceMethod.WITHHOLDING_BEFORE == this.interfacetype)
                || (TrainInterfaceMethod.MEITUAN == this.interfacetype)
                || (TrainInterfaceMethod.YILONG1 == this.interfacetype)
                || (TrainInterfaceMethod.YILONG2 == this.interfacetype) || (11 == this.interfacetype)) {
            issueTONGCHENG();
        }
        else if (TrainInterfaceMethod.TAOBAO == this.interfacetype) {
            issueTAOBAO();
        }
        else if (TrainInterfaceMethod.TRAIN_BESPEAKTICKET == this.interfacetype) {
            issueBESPEAK();
        }
        else if (12 == this.interfacetype)
            issueQUNARBESPEAK();
    }

    public void issueQUNARBESPEAK() {
        try {
            String QunarBespeakTicketCallBack = PropertyUtil.getValue("qunarCallBackUrl", "train.properties");
            String param = "orderid=" + this.trainorderid + "&opt=CONFIRM";
            WriteLog.write("约票回调_issueQUNARBESPEAK", this.trainorderid + "--->url:" + QunarBespeakTicketCallBack + "?"
                    + param);
            String result = SendPostandGet.submitPost(QunarBespeakTicketCallBack, param, "utf-8").toString();
            WriteLog.write("约票回调_issueQUNARBESPEAK", this.trainorderid + "--->result:" + result);
            com.alibaba.fastjson.JSONObject resultJsonObject = com.alibaba.fastjson.JSONObject.parseObject(result);
            if ((resultJsonObject.containsKey("success")) && (resultJsonObject.getBooleanValue("success"))) {
                new TrainActiveMQ(this.trainorderid, 1).sendDeductionMQ();
                return;
            }
            this.trainSupplyMethod.upTrainorder("UPDATE T_TRAINORDER SET C_ISPLACEING=3,C_ISQUESTIONORDER=3 WHERE ID="
                    + this.trainorderid);
            this.trainSupplyMethod.writeRC(this.trainorder.getId(), "出票:出票失败:" + result, "出票回调接口", 2, 1);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 约票回调
     * 
     * @time 2015年12月4日 下午2:50:23
     * @author wcl
     */
    public void issueBESPEAK() {
        try {
            this.tongchengcallbackurl = PropertyUtil.getValue("Train_BespeakTicketCallBack", "train.properties");
            String map = "orderid=" + this.trainorderid + "&flag=true&refuseMsg=" + URLEncoder.encode("支付成功", "utf-8");
            WriteLog.write("约票回调_issueBESPEAK", "MAP:" + map);
            String result = SendPostandGet.submitPost(tongchengcallbackurl, map, "utf-8").toString();
            WriteLog.write("约票回调_issueBESPEAK", "url:" + tongchengcallbackurl + "?" + map);
            WriteLog.write("约票回调_issueBESPEAK", "result:" + result);
            if ("success".equalsIgnoreCase(result)) {
                //扔队列里去扣款
                new TrainActiveMQ(this.trainorderid, 1).sendDeductionMQ();
                //            for (Trainpassenger trainpassenger : this.trainorder.getPassengers()) {
                //                for (Trainticket trainticket : trainpassenger.getTraintickets()) {
                //                    trainticket.setId(trainticket.getId());
                //                    trainticket.setStatus(Trainticket.ISSUED);
                //                    Server.getInstance().getTrainService().updateTrainticket(trainticket);
                //                }
                //            }
                //            trainSupplyMethod.upTrainorder("UPDATE T_TRAINORDER SET C_ORDERSTATUS=" + Trainorder.ISSUED + " WHERE ID="
                //                    + this.trainorderid);
                //            trainSupplyMethod.writeRC(this.trainorder.getId(), "出票:出票成功", "出票回调接口", 3, 1);
            }
            else {
                trainSupplyMethod.upTrainorder("UPDATE T_TRAINORDER SET C_ISPLACEING=" + 3 + ",C_ISQUESTIONORDER="
                        + Trainorder.CAIGOUQUESTION + " WHERE ID=" + this.trainorderid);
                trainSupplyMethod.writeRC(this.trainorder.getId(), "出票:出票失败:" + result, "出票回调接口", 2, 1);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 
     * 易定行出票 
     * @time 2014年12月26日 上午9:27:51
     * @author fiend
     */
    public void issueYEE() {
        new JobGenerateOrder().yeeIssue(this.trainorder.getId(), ordercustomeruser(this.trainorder));
    }

    /**
     * qunar出票 
     * @time 2014年12月26日 上午9:27:51
     * @author fiend
     */
    public void issueQUNAR() {
        boolean result = false;
        result = QunarTrainMethod.trainIssueOrRefuse(this.trainorderid, 3);
        if (result) {
            for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
                for (Trainticket trainticket : trainpassenger.getTraintickets()) {
                    trainticket.setId(trainticket.getId());
                    trainticket.setStatus(Trainticket.ISSUED);
                    Server.getInstance().getTrainService().updateTrainticket(trainticket);
                }
            }
            trainSupplyMethod.upTrainorder("UPDATE T_TRAINORDER SET C_ORDERSTATUS=" + Trainorder.ISSUED + " WHERE ID="
                    + this.trainorderid);
            trainSupplyMethod.writeRC(this.trainorder.getId(), "出票:出票成功", "QUNAR出票接口", 3, 1);
            new TrainActiveMQ(this.trainorderid, 1).sendDeductionMQ();
        }
        else {
            trainSupplyMethod.upTrainorder("UPDATE T_TRAINORDER SET C_ISPLACEING=" + 3 + ",C_ISQUESTIONORDER="
                    + Trainorder.CAIGOUQUESTION + " WHERE ID=" + this.trainorderid);
            trainSupplyMethod.writeRC(this.trainorder.getId(), "出票:出票失败", "QUNAR出票接口", 2, 1);
        }
    }

    /**
     * TONGCHENG出票 
     * @time 2014年12月26日 上午9:27:51
     * @author fiend
     */
    public void issueTONGCHENG() {
        String callresult = "false";
        boolean isbudan = trainSupplyMethod.isBudanOrder(this.trainorder.getId());
        if (isbudan) {//回调补单成功
            callresult = trainSupplyMethod.callBackTongChengBudan(this.trainorder.getId(), this.tongchengcallbackurl);
            WriteLog.write("自动审核", this.trainorder.getId() + ":" + "回调补单成功" + callresult);
            if ("success".equalsIgnoreCase(callresult)) {
                trainSupplyMethod.changeBudanOrder(trainorder.getId());
            }
        }
        else {//回调支付成功
            callresult = trainSupplyMethod.callBackTongChengPayed(this.trainorder, this.tongchengcallbackurl);
        }
        if ("success".equalsIgnoreCase(callresult)) {
            for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
                for (Trainticket trainticket : trainpassenger.getTraintickets()) {
                    trainticket.setId(trainticket.getId());
                    trainticket.setStatus(Trainticket.ISSUED);
                    Server.getInstance().getTrainService().updateTrainticket(trainticket);
                }
            }
            trainSupplyMethod.upTrainorder("UPDATE T_TRAINORDER SET C_ORDERSTATUS=" + Trainorder.ISSUED + " WHERE ID="
                    + this.trainorderid);
            if (isbudan) {
                trainSupplyMethod.writeRC(this.trainorder.getId(), "补单:补单成功", "补单回调接口", 3, 1);
            }
            else {
                trainSupplyMethod.writeRC(this.trainorder.getId(), "出票:出票成功", "出票回调接口", 3, 1);
            }
            //同程扣款是否成功
            trainSupplyMethod.writeRC(this.trainorder.getId(), "准备调取自动扣款接口", "系统接口", 2, 1);
            //扔到扣款的队列里
            //2后扣款模式 1预付模式
            //出票-->代扣流程
            int type = TrainInterfaceMethod.WITHHOLDING_AFTER == this.interfacetype ? 2 : 1;
            //扔队列里去扣款
            new TrainActiveMQ(this.trainorderid, type).sendDeductionMQ();
        }
        else {
            trainSupplyMethod.upTrainorder("UPDATE T_TRAINORDER SET C_ISPLACEING=" + 3 + ",C_ISQUESTIONORDER="
                    + Trainorder.CAIGOUQUESTION + " WHERE ID=" + this.trainorderid);
            if (isbudan) {
                trainSupplyMethod.writeRC(this.trainorder.getId(), "补单:补单失败:" + callresult, "补单回调接口", 2, 1);
            }
            else {
                trainSupplyMethod.writeRC(this.trainorder.getId(), "出票:出票失败:" + callresult, "出票回调接口", 2, 1);
            }
        }
    }

    /**
     * @param str
     * @return 是否为null或""
     */
    public boolean isNotNullOrEpt(String str) {
        if (str != null && str.trim().trim().length() > 0) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * 时间转换 
     * @param date
     * @param format
     * @return
     * @time 2014年10月9日 下午6:45:34
     * @author yinshubin
     */
    public Timestamp formatStringToTime(String date, String format) {
        try {
            SimpleDateFormat simplefromat = new SimpleDateFormat(format);
            return new Timestamp(simplefromat.parse(date).getTime());

        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 淘宝出票
     * @time 2015年3月31日 上午5:47:01
     * @author fiend
     */
    private void issueTAOBAO() {
        //淘宝回调地址
        //TaobaoTrainInsure.getTaobaoTrainInsure().toubao(trainorder);//投保
        String url = this.taobaocallbackurl + "?order_id=" + this.trainorderid + "&fail_msg=";
        WriteLog.write("TrainCreateOrder_issueTAOBAO", this.trainorderid + ":" + url);
        String taobao_callbackstr = SendPostandGet.submitGet(url, "UTF-8");
        WriteLog.write("TrainCreateOrder_issueTAOBAO", this.trainorderid + ":" + taobao_callbackstr);
        if ("SUCCESS".equals(taobao_callbackstr)) {
            for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
                for (Trainticket trainticket : trainpassenger.getTraintickets()) {
                    trainticket.setId(trainticket.getId());
                    trainticket.setStatus(Trainticket.ISSUED);
                    Server.getInstance().getTrainService().updateTrainticket(trainticket);
                }
            }
            trainSupplyMethod.upTrainorder("UPDATE T_TRAINORDER SET C_ORDERSTATUS=" + Trainorder.ISSUED + " WHERE ID="
                    + this.trainorderid);

            trainSupplyMethod.taobaoSuccessRc(this.trainorderid, true);
            trainSupplyMethod.writeRC(this.trainorder.getId(), "准备调取自动扣款接口", "系统接口", 2, 1);
            new TrainActiveMQ(this.trainorderid, 1).sendDeductionMQ();
        }
        else {
            trainSupplyMethod.upTrainorder("UPDATE T_TRAINORDER SET C_ISQUESTIONORDER=" + Trainorder.CAIGOUQUESTION
                    + " WHERE ID=" + this.trainorderid);
            trainSupplyMethod.taobaoSuccessRc(this.trainorderid, false);
        }
        //淘宝拒单

    }

    //========================================================出票接口结束======================================================================
    /**
     * 判断是否客人支付类型
     * @return
     */
    public boolean isNeedPayUrl(boolean isChange) {
        boolean result = false;
        String sql = "sp_TomasPayUrlInfo_selectByOrderId @OrderId=" + this.trainorderid + ", @ChangeFlag= "
                + (isChange ? 1 : 0);
        List list = Server.getInstance().getSystemService().findMapResultByProcedure(sql);
        if (list != null && list.size() > 0) {
            result = true;
        }
        return result;
    }

    /**
     * 回调同程出票失败
     * @param trainorder2
     * @param tongchengcallbackurl2
     * @return
     */
    public String callBackTongChengPayedFail(Trainorder trainorder, String tongchengcallbackurl) {
        String url = tongchengcallbackurl;
        JSONObject jso = new JSONObject();
        String result = "false";
        jso.put("pkid", trainorder.getId());
        jso.put("orderid", trainorder.getQunarOrdernumber());
        jso.put("transactionid", trainorder.getTradeno());
        jso.put("method", "train_pay_callback");
        jso.put("isSuccess", "N");
        try {
            WriteLog.write("审核回调同程出票", trainorder.getId() + ":tongcheng_tongzhi_chupiao:" + url + ":" + jso.toString());
            long t1 = System.currentTimeMillis();
            result = SendPostandGet.submitPost(url, jso.toString(), "UTF-8").toString();
            long t2 = System.currentTimeMillis();
            WriteLog.write("审核回调同程出票", trainorder.getId() + ":返回:" + result + ":用时：" + (t2 - t1));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;

    }
}
