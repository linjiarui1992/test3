package com.ccservice.inter.job.train.thread.job;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.Util.file.ExceptionUtil;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.bean.TrainPropertyBean;
import com.ccservice.control.TrainPropertyMessage;
import com.ccservice.inter.job.train.thread.db.TrainPropertyGetData;
import com.ggjs.config.netty.InitServer;
import com.ccservice.elong.inter.PropertyUtil;

/**
 * 
 * @author baozz
 *  负责从数据库中读取数据转移到内存中
 */
public class TrainPropertyMessageJob implements Job {
    /**
     * job执行的方法
     */
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        try {
            int isSelectAll = 0;
            if (TrainPropertyMessage.getPropertyBeans().isEmpty()) {
                isSelectAll = 1;
            }
            String storedProcedureName = "getPropertyMessage";
            try {
                storedProcedureName = PropertyUtil.getValue("storedProcedureName", "train.property.properties");
            }
            catch (Exception e) {
            }
            String sql = storedProcedureName+ " @updateTime = '" + new Timestamp(new Date().getTime()).toString()
                    + "',@isSelectAll = " + isSelectAll;
            List<TrainPropertyBean> propertyBeans = new TrainPropertyGetData().getTrainPropertyBeans(sql);
            if (propertyBeans != null && !propertyBeans.isEmpty()) {
                //给内存重新赋值  
                TrainPropertyMessage.setPropertyBeans(propertyBeans);
                //清除缓存
                TrainPropertyMessage.getCachePropertys().clear();
                WriteLog.write("TrainPropertyMessageJob_execute", "得到的数据库信息为:" + propertyBeans);
            }
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("TrainPropertyMessageJob_execute_Exception", e, "job执行时发生异常");
        }

    }
public static void main(String[] args) throws JobExecutionException {
    InitServer.start();
    new TrainPropertyMessageJob().execute(null);
}
}
