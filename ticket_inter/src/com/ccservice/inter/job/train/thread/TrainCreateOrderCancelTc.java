package com.ccservice.inter.job.train.thread;

import java.util.List;
import java.util.Map;

import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.b2b2c.policy.thread.TongChengCancelThread;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.qunar.util.ExceptionUtil;

public class TrainCreateOrderCancelTc {
    /**
     * @author WH
     * @time 2016年7月5日 上午11:34:46
     * @version 1.0
     * @param freeAccount 是否在TongChengCancelThread内部释放账号
     */
    @SuppressWarnings("rawtypes")
    public boolean isCancel(String default_pingtaiStr, Trainorder trainorder, Customeruser userqu, boolean freeAccount) {
        if (default_pingtaiStr != null && "tc".equals(default_pingtaiStr)) {
            try {
                List list = Server.getInstance().getSystemService()
                        .findMapResultByProcedure(" [sp_TrainorderCanceling_Select] @OrderId=" + trainorder.getId());
                if (list.size() > 0) {
                    Map map = (Map) list.get(0);
                    String status = map.get("Status") + "";
                    if ("0".equals(status)) {
                        try {
                            Server.getInstance()
                                    .getSystemService()
                                    .findMapResultBySql(
                                            "UPDATE T_TRAINORDER SET C_STATE12306=" + Trainorder.ORDERFALSE
                                                    + " WHERE ID =" + trainorder.getId(), null);
                        }
                        catch (Exception e) {
                            WriteLog.write("订单取消后修改订单12306状态_ERROR", "" + trainorder.getId());
                            ExceptionUtil.writelogByException("订单取消后修改订单12306状态_ERROR", e);
                        }

                        createTrainorderrc(trainorder.getId(), "订单已被取消", "取消接口", 1);
                        new Thread(new TongChengCancelThread(trainorder, userqu, freeAccount)).start();
                        return true;
                    }
                }
            }
            catch (Exception e) {
                WriteLog.write("订单取消查询DB_ERROR", "" + trainorder.getId());
                ExceptionUtil.writelogByException("订单取消查询DB_ERROR", e);
            }
        }
        return false;
    }

    /**
     * 创建火车票操作记录
     * 
     * @param trainorderId
     *            火车票订单id
     * @param content
     *            内容
     * @param createuser
     *            用户
     * @param status
     *            状态
     * @time 2015年1月18日 下午5:02:43
     * @author chendong
     */
    protected void createTrainorderrc(Long trainorderId, String content, String createuser, int status) {
        try {
            Trainorderrc rc = new Trainorderrc();
            rc.setOrderid(trainorderId);
            rc.setContent(content);
            rc.setStatus(status);
            rc.setCreateuser(createuser);
            rc.setYwtype(1);
            Server.getInstance().getTrainService().createTrainorderrc(rc);
        }
        catch (Exception e) {
            WriteLog.write("操作记录失败", trainorderId + ":content:" + content);
        }
    }
}
