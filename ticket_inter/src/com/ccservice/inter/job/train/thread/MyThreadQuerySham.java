package com.ccservice.inter.job.train.thread;

import java.text.SimpleDateFormat;

import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.TrainInterfaceMethod;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.server.Server;
import com.ccservice.train.mqlistener.TrainActiveMQ;

/**
 * 假支付、审核消费者
 * 更新状态，回调采购商
 * 
 * @parameter
 * @time 2018年3月21日 下午4:58:47
 * @author YangFan
 */
public class MyThreadQuerySham extends Thread {

    private int interfacetype;

    private long ordersuccesstime;

    long trainorderid;

    private Trainorder trainorder;

    private String freeType;//String freeType = getSysconfigString_(name)

    TrainSupplyMethod trainSupplyMethod = new TrainSupplyMethod();

    private String tongchengcallbackurl;//回调接口的地址

    public MyThreadQuerySham(long trainorderid, long ordersuccesstime) {
        this.trainorderid = trainorderid;
        this.ordersuccesstime = ordersuccesstime;
        this.tongchengcallbackurl = trainSupplyMethod.getSysconfigString("tcTrainCallBack");
    }

    public static String getTime(long ordersuccesstime) {
        SimpleDateFormat yyyyMMddHHmmss = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        String s = yyyyMMddHHmmss.format(ordersuccesstime);
        return s;
    }

    public void run() {
        //记录个假的支付记录？？
        trainSupplyMethod.writeRC(trainorderid, "【支付链接消费者开始处理消息】", "自动支付", 2, 1);
        trainSupplyMethod.writeRC(trainorderid, "【发送支付mq成功】", "自动支付", 2, 1);
        trainSupplyMethod.writeRC(trainorderid, "开始支付", "自动支付", 2, 1);
        trainSupplyMethod.writeRC(trainorderid, "支付成功", "自动支付", 2, 1);
        trainSupplyMethod.writeRC(trainorderid, "订单信息二次校验通过", "审核接口", 2, 1);
        try {
            Server.getInstance()
                    .getSystemService()
                    .findMapResultByProcedure(
                            "[TrainOrderQueryTimeOut_insert] @OrderId=" + this.trainorderid + ",@ordersuccesstime='"
                                    + getTime(this.ordersuccesstime) + "'");
        }
        catch (Exception e) {
            WriteLog.write("JobQueryMyOrder_MyThreadQuery", "订单号:" + this.trainorderid + "插入成功时间异常" + e);
        }
        this.trainorder = Server.getInstance().getTrainService().findTrainorder(this.trainorderid);
        if (this.trainorder.getAgentcontact() != null && "1".equals(this.trainorder.getAgentcontact())) {
            this.freeType = "3";
        }
        WriteLog.write("JobQueryMyOrder_MyThreadQuery",
                "订单号:" + this.trainorderid + ":getIsplaceing:" + trainorder.getIsplaceing());
        if (trainorder.getIsplaceing() != null && trainorder.getIsplaceing() == 1) {
            trainSupplyMethod.writeRC(trainorderid, "已扣过钱款,拒绝审核", "审核接口", 2, 1);
            return;
        }
        trainSupplyMethod.upTrainorder("UPDATE T_TRAINORDER SET C_ISQUESTIONORDER=" + Trainorder.NOQUESTION
                + ",C_ISPLACEING=2,C_CONTROLNAME=NULL,C_OPERATEUID=0" + " WHERE ID=" + this.trainorderid);
        trainSupplyMethod.writeRC(this.trainorderid, "开始审核", "自动审核", 2, 1);
        this.interfacetype = getOrderAttribution();//确定订单归属
        if (this.trainorder.getOrderstatus() != Trainorder.ISSUED) {
            try {
                Server.getInstance().getSystemService()
                        .findMapResultByProcedure("[TrainOrderQueryTimeOut_delete] @OrderId=" + this.trainorderid);
            }
            catch (Exception e) {
                WriteLog.write("JobQueryMyOrder_MyThreadQuery", "订单号:" + trainorder.getId() + "删除异常" + e);
            }
            orderrc(trainorder, "12306订单支付成功,已审核");
            issue();//出票统一接口
        }
    }

    public int getOrderAttribution() {
        this.interfacetype = this.trainorder.getInterfacetype() != null && this.trainorder.getInterfacetype() > 0 ? this.trainorder
                .getInterfacetype() : Server.getInstance().getInterfaceTypeService()
                .getTrainInterfaceType(this.trainorderid);
        return this.interfacetype;
    }

    /**
     * 审核后书写操作记录
     * @param trainorder
     * @param isissure 审核已支付并且出票成功TRUE
     * @time 2014年12月26日 下午12:10:47
     * @author fiend
     */
    public void orderrc(Trainorder trainorder, String Content) {
        Trainorderrc rcrefund = new Trainorderrc();
        rcrefund.setStatus(trainorder.getOrderstatus());
        rcrefund.setContent(Content);
        rcrefund.setCreateuser("12306审核");
        rcrefund.setOrderid(trainorder.getId());
        rcrefund.setYwtype(1);
        Server.getInstance().getTrainService().createTrainorderrc(rcrefund);
    }

    /**
     * 出票统一接口
     * @return
     * @time 2015年1月27日 上午11:22:02
     * @author fiend
     */
    public void issue() {
        if (TrainInterfaceMethod.TONGCHENG == this.interfacetype
                || TrainInterfaceMethod.WITHHOLDING_AFTER == this.interfacetype
                || TrainInterfaceMethod.WITHHOLDING_BEFORE == this.interfacetype) {
            issueTONGCHENG();
        }
    }

    /**
     * TONGCHENG出票
     * @time 2014年12月26日 上午9:27:51
     * @author fiend
     */
    public void issueTONGCHENG() {
        String callresult = "false";
        boolean isbudan = trainSupplyMethod.isBudanOrder(this.trainorder.getId());
        if (isbudan) {//回调补单成功
            callresult = trainSupplyMethod.callBackTongChengBudan(this.trainorder.getId(), this.tongchengcallbackurl);
            WriteLog.write("自动审核", this.trainorder.getId() + ":" + "回调补单成功" + callresult);
            if ("success".equalsIgnoreCase(callresult)) {
                trainSupplyMethod.changeBudanOrder(trainorder.getId());
            }
        }
        else {//回调支付成功
            callresult = trainSupplyMethod.callBackTongChengPayed(this.trainorder, this.tongchengcallbackurl);
        }
        if ("success".equalsIgnoreCase(callresult)) {
            for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
                for (Trainticket trainticket : trainpassenger.getTraintickets()) {
                    trainticket.setId(trainticket.getId());
                    trainticket.setStatus(Trainticket.ISSUED);
                    Server.getInstance().getTrainService().updateTrainticket(trainticket);
                }
            }
            trainSupplyMethod.upTrainorder("UPDATE T_TRAINORDER SET C_ORDERSTATUS=" + Trainorder.ISSUED + " WHERE ID="
                    + this.trainorderid);
            if (isbudan) {
                trainSupplyMethod.writeRC(this.trainorder.getId(), "补单:补单成功", "补单回调接口", 3, 1);
            }
            else {
                trainSupplyMethod.writeRC(this.trainorder.getId(), "出票:出票成功", "出票回调接口", 3, 1);
            }
            //同程扣款是否成功
            trainSupplyMethod.writeRC(this.trainorder.getId(), "准备调取自动扣款接口", "系统接口", 2, 1);
            //扔到扣款的队列里
            //2后扣款模式 1预付模式
            //出票-->代扣流程
            int type = TrainInterfaceMethod.WITHHOLDING_AFTER == this.interfacetype ? 2 : 1;
            //扔队列里去扣款
            new TrainActiveMQ(this.trainorderid, type).sendDeductionMQ();
        }
        else {
            trainSupplyMethod.upTrainorder("UPDATE T_TRAINORDER SET C_ISPLACEING=" + 3 + ",C_ISQUESTIONORDER="
                    + Trainorder.CAIGOUQUESTION + " WHERE ID=" + this.trainorderid);
            if (isbudan) {
                trainSupplyMethod.writeRC(this.trainorder.getId(), "补单:补单失败:" + callresult, "补单回调接口", 2, 1);
            }
            else {
                trainSupplyMethod.writeRC(this.trainorder.getId(), "出票:出票失败:" + callresult, "出票回调接口", 2, 1);
            }
        }
    }

}
