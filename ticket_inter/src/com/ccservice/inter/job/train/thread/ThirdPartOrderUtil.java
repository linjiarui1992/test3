package com.ccservice.inter.job.train.thread;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.mq.producer.exception.MQProducerException;
import com.ccservice.mq.producer.method.MQSendMessageMethod;
import com.ccservice.rabbitmq.util.PropertyUtil;

/**
 * 第三方下单工具类
 * @author libaobao
 *
 */
public class ThirdPartOrderUtil {
    //平台类型也是mq服务
    static String mqgroup;

    //系统类型
    static String systemType;

    //可以使用的第三方途径
    static String thirdPart;

    //第三方下单队列名称
    static String createOrderQueue;

    //第三方取消占座队列名
    static String cancleOrderQueue;
    static {
        mqgroup = PropertyUtil.getValue("pingTaiType", "rabbitMQ.properties");
        createOrderQueue = PropertyUtil.getValue("thirdPart_createOrder_queueName", "rabbitMQ.properties");
        cancleOrderQueue = PropertyUtil.getValue("thirdPart_cancleOrder_queueName", "rabbitMQ.properties");
        systemType = PropertyUtil.getValue("system_type", "rabbitMQ.properties");
        thirdPart = PropertyUtil.getValue("third_part", "rabbitMQ.properties");
    }

    /**
    * 判断是否是第三方订单 是则获取第三方订单号
    * @param orderId 订单id
    * @return
    */
    private static boolean isThirdOrder(JSONObject paramJson) {
        boolean isThirdOrder = false;
        Long orderId = paramJson.getLong("OldOrderId");
        WriteLog.write("ThirdPartOrderUtil", orderId + ":isThirdOrder:" + paramJson);
        String sqlProduce = "[dbo].[Select_TRAINORDER_ByOrderId] @orderId = " + orderId;
        List dblist = Server.getInstance().getSystemService().findMapResultByProcedure(sqlProduce);
        HashMap<String, Object> resultMap = (HashMap<String, Object>) dblist.get(0);
        //根据订单表里的 isThirdOrder 1-是 0-否
        if (resultMap != null && resultMap.get("IsThirdOrder") != null && 1 == (int) resultMap.get("IsThirdOrder")) {
            String thirdOrderNumber = (String) resultMap.get("ThirdOrderNumber");
            if (thirdOrderNumber != null) {
                paramJson.put("ThirdOrderNumber", thirdOrderNumber);
                //订单表里 isThirdOrder=1 且 第三方订单号不为空 才断定 为第三方订单号 
                isThirdOrder = true;
            }
        }
        WriteLog.write("ThirdPartOrderUtil", orderId + ":isThirdOrder:" + isThirdOrder);
        return isThirdOrder;
    }

    /**
     * 第三方订单取消占座
     * @param orderId 订单id
     * @return true-发送第三方取消占座队列  false-没有发送
     */
    public static boolean handleThirdPartOrder(Long orderId) {
        WriteLog.write("ThirdPartOrderUtil", orderId + ":handleThirdPartOrder:" + orderId);
        boolean hasHandle = false;
        JSONObject paramJson = new JSONObject();
        paramJson.put("OldOrderId", orderId);
        if (isThirdOrder(paramJson)) {
            //取消占座
            //RabbitMqUtilNew.sendOneRabbitMqMessage(cancleOrderQueue,paramJson.toJSONString(),mqgroup);
            try {
                MQSendMessageMethod.sendOnemessage(paramJson.toJSONString(), cancleOrderQueue,
                        Integer.parseInt(mqgroup));
            }
            catch (NumberFormatException | IOException | MQProducerException e) {
                // TODO Auto-generated catch block
                WriteLog.write(
                        "ThirdPartOrderUtil_handleThirdPartOrder_NumberFormatException|IOException|MQProducerException",
                        "发送消息异常");
            }
            WriteLog.write("ThirdPartOrderUtil", "发送第三方取消占座队列  orderId:" + orderId);
            hasHandle = true;
        }
        WriteLog.write("ThirdPartOrderUtil", orderId + ":handleThirdPartOrder:" + hasHandle);
        return hasHandle;
    }

    /**
    * 判断是否允许使用第三方下单
    * @param agentId
    * @return
    */
    private static boolean getIsThirdStatus(long agentId, long orderId) throws Exception {
        WriteLog.write("ThirdPartOrderUtil", orderId + ":getIsThirdStatus:" + agentId);
        boolean isThirdStatus = false;
        String sqlProduce = "[dbo].[Select_INTERFACEACCOUNT_ByAgentId] @agentId = " + agentId;
        List dblist = Server.getInstance().getSystemService().findMapResultByProcedure(sqlProduce);
        if (dblist == null || dblist.size() == 0 || dblist.get(0) == null) {
            WriteLog.write("ThirdPartOrderUtil", orderId + ":查询是否使用第三方下单时 结果 为空 agentId:" + agentId + "orderId:"
                    + orderId);
            return false;
        }
        HashMap<String, Object> resultMap = (HashMap<String, Object>) dblist.get(0);
        int flag = (int) (resultMap.get("thirdstatus") == null ? 0 : resultMap.get("thirdstatus"));
        if (1 == flag) {
            isThirdStatus = true;
        }
        WriteLog.write("ThirdPartOrderUtil", orderId + ":getIsThirdStatus:" + isThirdStatus);
        return isThirdStatus;
    }

    /**
     * 下单入口判断,第三方下单队列
     * @param trainOrder 火车票订单
     * @return
     */
    public static boolean isNeedSendThirdPartMQ(long orderId, long agentId) {
        WriteLog.write("ThirdPartOrderUtil", orderId + ":" + agentId);
        boolean flag = false;
        try {
            //检查是否允许第三方下单
            boolean check1 = getIsThirdStatus(agentId, orderId);
            //权重分配
            boolean check2 = getWeight(2);
            if (check1 && check2) {
                //发送mq
                sendThirdPartMQ(orderId);
                flag = true;
            }
        }
        catch (Exception e) {
            flag = false;//出错时返回false;
            WriteLog.write("ThirdPartOrderUtil", orderId + ":" + "发送至第三方下单队列时 出错 " + e);
            e.printStackTrace();
        }
        WriteLog.write("ThirdPartOrderUtil", orderId + ":" + flag);
        return flag;
    }

    /**
     * 下单入口和下单失败时发送mq参数
     * @param orderId 订单id
     */
    public static void sendThirdPartMQ(Long orderId) {
        try {
            JSONObject paramJson = new JSONObject();
            JSONArray jarr = JSONArray.parseArray(thirdPart);
            paramJson.put("SystemType", systemType);
            paramJson.put("OldOrderId", orderId);
            paramJson.put("ThirdParty", jarr);
            //RabbitMqUtilNew.sendOneRabbitMqMessage(createOrderQueue, paramJson.toJSONString(), mqgroup);
            MQSendMessageMethod.sendOnemessageUnException(paramJson.toJSONString(), createOrderQueue,
                    Integer.parseInt(mqgroup));
            WriteLog.write("ThirdPartOrderUtil", "发送第三方下单队列成功 orderId:" + orderId);
        }
        catch (Exception e) {
            WriteLog.write("ThirdPartOrderUtil", "发送第三方下单队列时出错  orderId:" + orderId);
            e.printStackTrace();
        }
        //更改订单表 isThirdOrder 为可以使用第三方下单 
        String sqlProduce = "[dbo].[Update_TRAINORDER_IsThirdOrder] @status = 1,@orderId = " + orderId;
        Server.getInstance().getSystemService().findMapResultByProcedure(sqlProduce);
    }

    /**
     * 下单失败时发送mq
     * @param orderId 订单id
     */
    public static boolean failureSendThirdPartMQ(Long orderId) {
        //读取系统设置 是否下单失败时发第三方mq
        boolean flag = true;
        String sqlProduce = "[dbo].[SystemConfigByName] @name = 'ThirdStatus12306'";
        List dblist = Server.getInstance().getSystemService().findMapResultByProcedure(sqlProduce);
        HashMap<String, String> resultMap = (HashMap<String, String>) dblist.get(0);
        //1为是 并附上权重计算
        if (resultMap != null && "1".equals(resultMap.get("value")) && getWeight(2)) {
            sendThirdPartMQ(orderId);
        }
        else {
            flag = false;
        }
        return flag;
    }

    /**
     * 权重计算
     * @param wight12306
     * @param wightdisanfang
     * @return
     */
    private static int getOrderType(int wight12306, int wightdisanfang) {
        int wight12346Status = 1;
        int wightdisanfangStatus = 2;
        int wightAll = wight12306 + wightdisanfang;
        double random1 = Math.random();
        double random2 = (random1 * (wightAll)) % wightAll;
        return (int) (random2) < wight12306 ? wight12346Status : wightdisanfangStatus;
    }

    /**
     * 根据权重南计算比例
     * @param type 1-12306的权重 2-第三方的权重
     * @return  概率返回true和false
     */
    private static boolean getWeight(int type) {
        int wight12306 = 0;
        int wightdisanfang = 0;
        //去数据库读取权重信息......
        String sqlProduce = "[dbo].[TrainCreateOrderWayByText]";
        List dblist = Server.getInstance().getSystemService().findMapResultByProcedure(sqlProduce);
        for (HashMap<String, Object> resultMap : (List<HashMap<String, Object>>) dblist) {
            if (resultMap != null) {
                if ("1".equals(resultMap.get("Text"))) {
                    //weight 为null 设为0
                    wight12306 = (resultMap.get("Weight") == null) ? 0 : (int) resultMap.get("Weight");
                }
                else if ("2".equals(resultMap.get("Text"))) {
                    wightdisanfang = (resultMap.get("Weight") == null) ? 0 : (int) resultMap.get("Weight");
                }
            }
        }
        return getOrderType(wight12306, wightdisanfang) == type;
    }

    public static void main(String[] args) {
        /*Trainorder trainOrder = new Trainorder();
        trainOrder.setAgentid(315);
        trainOrder.setId(73418571);
        int j=0;
        for(int i = 0;i<100;i++){
        	if(isNeedSendThirdPartMQ(trainOrder))j++;
        }
        System.out.println(j);
        StopWatch sw = new StopWatch("ds");
        sw.start("failureSendThirdPartMQ-100");
        int j = 0;
        Trainorder trainOrder = new Trainorder();
        trainOrder.setAgentid(315);
        trainOrder.setId(73418571);
        for(int i = 0;i<100;i++){
        //    		if(failureSendThirdPartMQ(73418572l)) j++;
        	if(isNeedSendThirdPartMQ(trainOrder))j++;
        }
        sw.stop();
        System.out.println(sw.prettyPrint());
        System.out.println(j);*/
        //    	handleThirdPartOrder(73418572l);
        System.out.println(ThirdPartOrderUtil.createOrderQueue);

    }
}
