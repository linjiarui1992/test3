/**
 * 
 */
package com.ccservice.inter.job.train;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.b2b2c.atom.service12306.AccountSystem;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.thread.TrainCreateOrderSupplyMethod;
import com.ccservice.inter.server.Server;
import com.ccservice.qunar.util.ExceptionUtil;

/**
 * 定时查询 账号系统回调淘宝3151是否有回应  
 * @time 2017年3月28日 下午5:00:43
 * @author yuanzc
 */
public class JobTaobaoPersonateOrderCallback extends TrainCreateOrderSupplyMethod implements Job {

    /**
     * 定时查询AcccountWait表 中状态为0 且当前时间大于createTime(createTime为创建时间+15分钟)
     * 查询结果为账号系统超时没有回调淘宝
     */
    @Override
    @SuppressWarnings("rawtypes")
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        System.out.println("------开始处理淘宝3151超时------------");
        //查询数据为当前时间过了设定时间还没有回调淘宝3151  返回 id  result
        List list = findAllTimeOut();
        //如果list不为空,存放的数据为超时未回调淘宝的订单ids
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                Map map = (Map) list.get(i);
                if (map.containsKey("OrderId") && map.containsKey("Result")) {
                    long orderId = Long.parseLong(map.get("OrderId").toString());
                    String result = map.get("Result").toString();//失败原因 
                    WriteLog.write("TAOBAO_3151_TimeOut", orderId + "--->" + result);
                    //根据id查找TrainOrder的orderType,C_CREATETIME
                    Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(orderId);
                    if (trainorder != null) {
                        //记录   拒单  
                        createTrainorderrc(orderId, result, "3151回调淘宝超时", 1);
                        refuse(trainorder, result);
                    }
                }
                else {
                    WriteLog.write("TAOBAO_3151_TimeOut_ERROR", "解析map失败");
                }
            }
        }

    }

    /**
     * 查找所有超时的淘宝3151订单
     * 
     * @return
     * @time 2017年3月31日 上午10:11:42
     * @author fiend
     */
    @SuppressWarnings("rawtypes")
    private List findAllTimeOut() {
        List list = null;
        String sql = "[sp_AccountWait_select_TaobaoCallBackTimeOut]";
        try {
            list = Server.getInstance().getSystemService().findMapResultByProcedure(sql);
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("JobTaobaoPersonateOrderCallback_findAllTimeOut_Exception", e, sql);
        }
        return list;
    }

    /**
     * 淘宝拒单
     * 
     * @param trainorder
     * @param result
     * @time 2017年3月29日 下午4:22:07
     * @author yuanzc
     */
    private void refuse(Trainorder trainorder, String result) {
        trainorder.setOrderstatus(Trainorder.CANCLED);// 先改状态,再调接口
        Server.getInstance().getTrainService().updateTrainorder(trainorder);
        Trainorderrc rc = new Trainorderrc();
        rc.setStatus(Trainticket.WAITISSUE);
        rc.setContent(result);
        rc.setCreateuser("3151回调淘宝超时");
        rc.setOrderid(trainorder.getId());
        rc.setYwtype(1);
        try {
            Server.getInstance().getTrainService().createTrainorderrc(rc);
        }
        catch (Exception e) {
            WriteLog.write("操作记录失败", trainorder.getId() + ":content:淘宝拒单");
        }
        refundTaobao(trainorder, "3151回调淘宝超时", result);
        insertTrain_Order_Seat(trainorder, false);
    }

    /**
     * 下单耗时
     * 
     * @param trainorder
     * @param success
     * @time 2017年3月31日 上午9:42:42
     * @author yuanzc
     */
    private void insertTrain_Order_Seat(Trainorder trainorder, boolean success) {
        long end = System.currentTimeMillis();
        long start = trainorder.getCreatetime().getTime();
        double result = (end - start) / 1000.0D;
        String sql = "dbo.Train_Order_Seat_inset @OrderId ='" + trainorder.getId() + "',@Status=" + ((success) ? 1 : 0)
                + "," + "@CreateTime='"
                + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(trainorder.getCreatetime())
                + "',@ConsumptionTime='" + Double.parseDouble(new DecimalFormat("#.00").format(result)) + "'";
        Server.getInstance().getSystemService().excuteAdvertisementBySql(sql);
    }
}
