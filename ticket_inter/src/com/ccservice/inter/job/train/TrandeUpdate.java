package com.ccservice.inter.job.train;

import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;

import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.inter.job.WriteLog;

/**
 * 
 * 定时更新支付中状态订单
 * @time 2014年12月26日 下午1:06:32
 * @author wzc
 */
public class TrandeUpdate {

    public static void Jobupdatetrade() {
        String num1 = "T141222161730242151,T14123115065689057,T141227123359245029";
        String[] ary = num1.split(",");
        if (ary != null && ary.length > 0) {
            Map<String, String> unionurl = new HashMap<String, String>();
            for (int i = 0; i < ary.length; i++) {
                try {
                    String ordernum = ary[i];
                    String url4 = "http://211.103.207.134:8084/ccs_paymodule/unionpay";
                    String url3 = "http://211.103.207.134:8083/ccs_paymodule/unionpay";
                    String par = "{'ordernum':'" + ordernum + "','cmd':'seachliushuihao'}";
                    String infodata2 = SendPostandGet.submitPost(url4, par, "UTF-8").toString();
                    String infodata3 = SendPostandGet.submitPost(url3, par, "UTF-8").toString();
                    if (infodata2 != null && infodata2.contains("liushuihao")) {
                        JSONObject obj = JSONObject.fromObject(infodata2);
                        String liushuihao = obj.getString("liushuihao");
                        String liu3 = JSONObject.fromObject(infodata3).getString("liushuihao");
                        String state = "";
                        if (obj.containsKey("state")) {
                            state = obj.getString("state");
                        }
                        String msg = "";
                        if (obj.containsKey("msg")) {
                            msg = obj.getString("msg");
                        }
                        System.out.println(ordernum + "-" + liushuihao + "-" + liu3);
                        //解析银联参数成功  已经有交易号  必须返回RunOK才能确认已经支付
                        if (liushuihao != null && !"".equals(liushuihao) && liushuihao.length() > 0 && "RunOK".equals(state)) {
                            //trainorder.setSupplytradeno(liushuihao);
                            //Server.getInstance().getTrainService().updateTrainorder(trainorder);
                        }
                        else {
                            WriteLog.write("12306银联支付", "更新状态失败：" + ordernum);
                        }
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) {
        Jobupdatetrade();
    }
}
