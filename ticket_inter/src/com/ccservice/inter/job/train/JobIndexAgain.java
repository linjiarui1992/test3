package com.ccservice.inter.job.train;

import java.net.MalformedURLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;
import com.ccservice.b2b2c.atom.service12306.TimeUtil;
import com.ccservice.b2b2c.atom.service12306.bean.RepServerBean;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.b2b2c.policy.SendPostandGet;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.reg.Job12306Registration_JihuoThread;
import com.ccservice.inter.job.train.thread.Job12306ReleaseCustomeruserReLogintoCheckHeyanMyThread;
import com.ccservice.inter.job.train.thread.JobIndexAgainCustomeruserMoveTrainpassengerTopassInfo;
import com.ccservice.inter.job.train.thread.MyThreadLogin;
import com.ccservice.inter.server.Server;

/**
 * 
 * 定时保证12306在线账号数量
 * 说明：可用账号不足时，调12306账号自动登录 
 * @time 2014年12月18日 下午5:57:16
 * @author Administrator
 * 
 */
public class JobIndexAgain extends TrainSupplyMethod implements Job {

    public static void main(String[] args) {
        JobIndexAgain JobIndexAgain = new JobIndexAgain();
        //        JobIndexAgain.execute();
        //        JobIndexAgain.execute_mobile(20);
        //        RepServerBean rep = JobIndexAgain.getrepurl();
        //        System.out.println(rep);
        //        checkrepisenable();
        //        JobIndexAgain.moveTrainpassengerTopassInfo();//把trainpassenger 里的数据转移到passinfo 里
        int docount = 0;
        //        do {
        //            if (docount > 0) {
        //                try {
        //                    Thread.sleep(5000L);
        //                }
        //                catch (InterruptedException e) {
        //                    e.printStackTrace();
        //                }
        //            }
        //            docount++;
        //            JobIndexAgain.logintocheckHeyan();
        //        }
        //        while (docount < 1);
        JobIndexAgain.logintocheckHeyan();
    }

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        logintocheckHeyan();//核验身份和核验手机号的定时任务

        //        reTojihuo();//        注册成功后也拿到了激活的链接没有访问成功后重新拿链接去激活然后修改账号状态
        //        if (GoAccountSystem()) {
        //            return;
        //        }
        //        Long l1 = System.currentTimeMillis();
        //        WriteLog.write("JobIndexAgain", "开始了:" + l1);
        //        execute();
        //        //        execute_mobile(50);//增加对发送验证码的支持的新版本
        //        WriteLog.write("JobIndexAgain", "结束了:耗时:" + (System.currentTimeMillis() - l1));
        //        String reslutString = SendPostandGet.submitGet("http://localhost:19010/ticket_inter/test_sq.html");
        //        if ("1".equals(reslutString)) {
        //        }
        //        else {
        //            System.out.println("不核验了");
        //        }

        //        moveTrainpassengerTopassInfo();//把trainpassenger 里的数据转移到passinfo 里
    }

    /**
     * 注册成功后也拿到了激活的链接没有访问成功后重新拿链接去激活然后修改账号状态
     * 重新激活
     * @time 2015年8月31日 下午4:48:47
     * @author chendong
     */
    private void reTojihuo() {
        String JobIndexAgain_reTojihuo = PropertyUtil.getValue("JobIndexAgain_reTojihuo", "train.properties");
        if ("1".equals(JobIndexAgain_reTojihuo)) {//是否开启重新激活
            System.out.println("重新拿链接去激活-重新激活-开启");
            String serviceurl = "";//同程(这个连接地址已经放到了配置文件)  http://121.40.62.200:40000/cn_service/service/
            String JobIndexAgain_LogintocheckHeyan_serviceurl = PropertyUtil.getValue(
                    "JobIndexAgain_LogintocheckHeyan_serviceurl", "train.properties");
            serviceurl = JobIndexAgain_LogintocheckHeyan_serviceurl;
            HessianProxyFactory factory = new HessianProxyFactory();
            ISystemService isystemservice = null;
            try {
                isystemservice = (ISystemService) factory.create(ISystemService.class, serviceurl
                        + ISystemService.class.getSimpleName());
            }
            catch (MalformedURLException e) {
                e.printStackTrace();
            }
            Job12306Registration_JihuoThread job12306Registration_JihuoThread = new Job12306Registration_JihuoThread(
                    isystemservice);
            reTojihuo(job12306Registration_JihuoThread, isystemservice);
        }
    }

    /**
     * 第一次注册没有成功的话重新激活
     * @time 2015年8月31日 下午3:43:36
     * @author chendong
     * @param job12306Registration_JihuoThread 
     * @param isystemservice 
     */
    private void reTojihuo(Job12306Registration_JihuoThread job12306Registration_JihuoThread,
            ISystemService isystemservice) {
        String minid = "0";
        String reLogintocheckHeyanMainid = Server.getInstance().getDateHashMap().get("reLogintocheckHeyanMinid");
        if (reLogintocheckHeyanMainid == null) {
        }
        else {
            minid = reLogintocheckHeyanMainid;
        }
        String dataType = ",@type=3";// 如果是 没有激活 成功的重新获取然后去激活
        String procedureSqlString = "sp_CustomerUser_Job12306ReleaseCustomeruser_reLogintocheckHeyan @minID=" + minid
                + dataType;
        List customerusers = isystemservice.findMapResultByProcedure(procedureSqlString);
        if (customerusers.size() > 0) {
            Map map = (Map) customerusers.get(customerusers.size() - 1);
            Long id = Long.parseLong(map.get("ID").toString());
            //            SELECT top 0 ID,C_LOGINNAME,C_LOGPASSWORD,C_ISENABLE,C_CREATETIME,C_ENNAME,C_LINKOTHER,C_MEMBERFAX
            String linkother = map.get("C_LINKOTHER").toString();
            String logname = map.get("C_LOGINNAME").toString();
            String email = map.get("C_MEMBERFAX").toString();
            int success = job12306Registration_JihuoThread.sendjihuourl(linkother, 0);
            WriteLog.write("Job12306Registration_JihuoThread_reTojihuo", ":激活结果:" + success + ":" + logname + ":"
                    + email);
            String sqlupdatecus = "";
            if (success > 0) {
                sqlupdatecus = "UPDATE T_CUSTOMERUSER SET C_ISENABLE=18,C_CREATETIME = '"
                        + ElongHotelInterfaceUtil.getCurrentTime() + "' WHERE ID=" + id;//激活成功后把这个customeruser改成可用
                int recount = 0;
                try {
                    recount = isystemservice.excuteGiftBySql(sqlupdatecus);
                    //            recount = Server.getServer().getSystemService().excuteGiftBySql(sqlupdatecus);
                }
                catch (Exception e) {
                }
                System.out.println(":激活后修改数据库=>" + recount + ":" + sqlupdatecus);
                WriteLog.write("Job12306Registration_JihuoThread_reTojihuo", ":激活后修改数据库:" + recount + ":"
                        + sqlupdatecus);
            }
            else {
                WriteLog.write("Job12306Registration_JihuoThread_reTojihuo_false", ":success:" + success + ":"
                        + sqlupdatecus + ":" + logname + ":" + email);
            }
        }
        else {
            System.out.println(System.currentTimeMillis() + ":没有了不激活了");
        }
    }

    /**
     * 核验账号 
     * 核验身份和核验手机号的定时任务
     * @time 2015年8月19日 下午1:26:34
     * @author chendong
     */
    private void logintocheckHeyan() {
        //是否使用adsl
        String job12306Registration_useadsl = PropertyUtil.getValue("Job12306Registration_useadsl", "train.properties");

        if ("1".equals(job12306Registration_useadsl)) {
            if (Server.adslIaonLine) {//在线
                System.out.println("账号核验;已经拨号,正常走....");
            }
            else {
                System.out.println("正在拨号,请稍等。。。:" + TimeUtil.gettodaydate(4));
                return;
            }
        }
        String JobIndexAgain_LogintocheckHeyan = PropertyUtil.getValue("JobIndexAgain_LogintocheckHeyan",
                "train.properties");
        if ("1".equals(JobIndexAgain_LogintocheckHeyan)) {//是否开启核验
            String JobIndexAgain_LogintocheckHeyan0 = PropertyUtil.getValue("JobIndexAgain_LogintocheckHeyan0",
                    "train.properties");
            if ("1".equals(JobIndexAgain_LogintocheckHeyan0)) {//开启了老核验身份
                System.out.println(TimeUtil.gettodaydate(4) + "->0 老核验 核验身份 开");
                reLogintocheckHeyan(0);//老核验身份
                //                                reLogintocheckHeyan(2);//-->><<>><<本地临时核验
            }
            String JobIndexAgain_LogintocheckHeyan1 = PropertyUtil.getValue("JobIndexAgain_LogintocheckHeyan1",
                    "train.properties");
            if ("1".equals(JobIndexAgain_LogintocheckHeyan1)) {//开启了核验手机号
                System.out.println(TimeUtil.gettodaydate(4) + "->1 新核验手机号 开");
                reLogintocheckHeyan(1);
            }
        }
        else {
            System.out.println(TimeUtil.gettodaydate(4) + "->未开启核验");
        }
    }

    private void moveTrainpassengerTopassInfo() {
        String key = "moveTrainpassengerTopassInfo_sid_eid_index";
        String key_index = Server.getInstance().getDateHashMap().get(key);
        int i_key_index = 0;
        if (key_index == null) {
            key_index = PropertyUtil.getValue(key, "train.properties");
            i_key_index = Integer.parseInt(key_index);
        }
        else {
            i_key_index = Integer.parseInt(key_index) + 1;
        }
        Server.getInstance().getDateHashMap().put(key, i_key_index + "");
        int duoshaoge = Integer.parseInt(PropertyUtil.getValue("moveTrainpassengerTopassInfo_sid_eid_duoshaoge",
                "train.properties"));
        int sid = i_key_index * duoshaoge;
        int eid = sid + duoshaoge;
        System.out.println("========================" + i_key_index);
        WriteLog.write("JobIndexAgain_moveTrainpassengerTopassInfo", i_key_index + ":" + sid + ":" + eid);
        moveTrainpassengerTopassInfo(sid, eid);//把trainpassenger 里的数据转移到passinfo 里
    }

    /**
     * 把trainpassenger 里的数据转移到passinfo 里
     * 
     * @time 2015年6月24日 下午8:21:52
     * @author chendong
     */
    private void moveTrainpassengerTopassInfo(int sid, int eid) {
        //        String serviceurl_Trainpassenger = "http://localhost:9001/cn_service/service/";//
        //        String serviceurl_passInfo = "http://localhost:9001/cn_service/service/";
        //        String serviceurl_Trainpassenger = "http://121.40.62.200:40000/cn_service/service/";//
        //        String serviceurl_passInfo = "http://121.40.62.200:40000/cn_service/service/";
        String serviceurl_Trainpassenger = PropertyUtil.getValue(
                "moveTrainpassengerTopassInfo_serviceurl_Trainpassenger", "train.properties");//
        String serviceurl_passInfo = PropertyUtil.getValue("moveTrainpassengerTopassInfo_serviceurl_passInfo",
                "train.properties");

        HessianProxyFactory factory = new HessianProxyFactory();
        ISystemService isystemservice_Trainpassenger = null;
        ISystemService isystemservice_passInfo = null;
        try {
            isystemservice_Trainpassenger = (ISystemService) factory.create(ISystemService.class,
                    serviceurl_Trainpassenger + ISystemService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try {
            isystemservice_passInfo = (ISystemService) factory.create(ISystemService.class, serviceurl_passInfo
                    + ISystemService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }

        List customerusers = isystemservice_Trainpassenger
                .findMapResultByProcedure("sp_CustomerUser_JobIndexAgainMoveTrainpassengerTopassInfo @sid=" + sid
                        + ",@eid=" + eid);

        // 创建一个可重用固定线程数的线程池
        int nThread = Integer.parseInt(PropertyUtil.getValue("moveTrainpassengerTopassInfo_sid_eid_nThread",
                "train.properties"));
        ExecutorService pool = Executors.newFixedThreadPool(nThread);
        for (int i = 0; i < customerusers.size(); i++) {
            // 创建实现了Runnable接口对象，Thread对象当然也实现了Runnable接口
            Thread t1 = null;
            Map map = (Map) customerusers.get(i);
            String name = map.get("C_NAME").toString();
            String idnumber = map.get("C_IDNUMBER").toString();
            String aduitstatus = map.get("C_ADUITSTATUS").toString();
            //            String ID = map.get("ID").toString();
            t1 = new JobIndexAgainCustomeruserMoveTrainpassengerTopassInfo(isystemservice_passInfo, name, idnumber,
                    aduitstatus);
            pool.execute(t1);
        }
        // 关闭线程池
        pool.shutdown();
    }

    /**
     * 获取到待核验的账号去核验
     * @time 2015年6月25日 上午11:19:01
     * @author chendong
     * @param type  0 老核验 1核验手机号
     */
    private void reLogintocheckHeyan(int type) {
        if (!"1".equals(getHtmlIsStart())) {
            return;
        }
        try {
            String Job12306ReleaseCustomeruserReLogintoCheckHeyanMyThread_Maxdangqianheyancount = PropertyUtil
                    .getValue("Job12306ReleaseCustomeruserReLogintoCheckHeyanMyThread_Maxdangqianheyancount",
                            "train.properties");
            int Maxdangqianheyancount = Integer
                    .parseInt(Job12306ReleaseCustomeruserReLogintoCheckHeyanMyThread_Maxdangqianheyancount);
            if (Server.dangqianheyancount >= Maxdangqianheyancount) {
                System.out.println("当前(" + Server.dangqianheyancount + ")允许同时最大核验手机号数(" + Maxdangqianheyancount
                        + ")达到最大了,等会再跑。。。");
                return;
            }
        }
        catch (Exception e) {
            System.out.println("报错了");
            return;
        }
        String serviceurl = "";//同程(这个连接地址已经放到了配置文件)  http://121.40.62.200:40000/cn_service/service/
        String JobIndexAgain_LogintocheckHeyan_serviceurl = PropertyUtil.getValue(
                "JobIndexAgain_LogintocheckHeyan_serviceurl", "train.properties");
        serviceurl = JobIndexAgain_LogintocheckHeyan_serviceurl;
        HessianProxyFactory factory = new HessianProxyFactory();
        ISystemService isystemservice = null;
        try {
            isystemservice = (ISystemService) factory.create(ISystemService.class,
                    serviceurl + ISystemService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        String minid = "0";
        String reLogintocheckHeyanMainid = Server.getInstance().getDateHashMap().get("reLogintocheckHeyanMinid");
        if (reLogintocheckHeyanMainid != null) {
            minid = reLogintocheckHeyanMainid;
        }
        String dataType = ",@type=0";
        if (type == 1) {//0 老核验 1核验手机号
            dataType = ",@type=1";
        }
        else if (type == 0) {
            dataType = ",@type=0";//0 老核验
        }
        else if (type == 2) {
            dataType = ",@type=2";//2 本地临时核验
        }
        String procedureSqlString = "sp_CustomerUser_Job12306ReleaseCustomeruser_reLogintocheckHeyan @minID=" + minid
                + dataType;
        List customerusers = isystemservice.findMapResultByProcedure(procedureSqlString);
        if (customerusers.size() == 0) {
            minid = "0";
            procedureSqlString = "sp_CustomerUser_Job12306ReleaseCustomeruser_reLogintocheckHeyan @minID=" + minid
                    + dataType;
            customerusers = isystemservice.findMapResultByProcedure(procedureSqlString);
            Server.getInstance().getDateHashMap().put("reLogintocheckHeyanMinid", minid);
        }
        else {
            Map map = (Map) customerusers.get(customerusers.size() - 1);
            Long id = Long.parseLong(map.get("ID").toString());
            minid = id + "";
            Server.getInstance().getDateHashMap().put("reLogintocheckHeyanMinid", minid);
        }
        System.out.println(System.currentTimeMillis() + ":" + customerusers.size() + ":minid:" + minid);
        for (int i = 0; i < customerusers.size(); i++) {
            Map map = (Map) customerusers.get(i);
            String loginname = map.get("C_LOGINNAME").toString();
            String password = map.get("C_LOGPASSWORD").toString();
            String createtime = map.get("C_CREATETIME").toString();
            Long id = Long.parseLong(map.get("ID").toString());
            //            String url = getrepurl().getUrl();
            //            String url = getrepurl_properties();
            String url = "http://localhost:9016/Reptile/traininit";
            //            String url = Server.getInstance().getTrain12306Service().commonRepServer(true).getUrl();
            new Job12306ReleaseCustomeruserReLogintoCheckHeyanMyThread(loginname, password, createtime, id, url,
                    isystemservice, type).start();
        }
    }

    /**
     * 获取html是否开启
     * @return
     * @time 2015年10月23日 下午10:39:04
     * @author chendong
     */
    private String getHtmlIsStart() {
        String JobIndexAgain_mobileCheckIsstart = "1";
        try {
            String JobIndexAgain_mobileCheckIsstartUrl = PropertyUtil.getValue("JobIndexAgain_mobileCheckIsstartUrl",
                    "train.properties");
            try {
                //                JobIndexAgain_mobileCheckIsstart = SendPostandGet.submitGet(JobIndexAgain_mobileCheckIsstartUrl);
            }
            catch (Exception e) {
            }
            if ("1".equals(JobIndexAgain_mobileCheckIsstart)) {
            }
            else {
                System.out.println("check_mobile_html_没开=>" + TimeUtil.gettodaydate(5)
                        + ":JobIndexAgain_mobileCheckIsstart:" + JobIndexAgain_mobileCheckIsstart);
            }
        }
        catch (Exception e) {
        }
        return JobIndexAgain_mobileCheckIsstart;
    }

    /**
     * 
     * @time 2015年8月19日 下午1:04:19
     * @author chendong
     */
    private String getrepurl_properties() {
        String url = JobIndexAgain_RepUtil.getInstance().getRepUrl(false);
        return url;
    }

    /**
     * 
     * 
     * @return
     * @time 2015年6月26日 下午4:29:18
     * @author Administrator
     */
    private RepServerBean getrepurl_service1() {
        //        String serviceurl = "http://localhost:9001/cn_service/service/";
        String serviceurl = PropertyUtil.getValue("JobIndexAgain_getrepurl_serviceurl_getrepurl", "train.properties");
        RepServerBean rep = new RepServerBean();
        HessianProxyFactory factory = new HessianProxyFactory();
        ISystemService isystemservice = null;
        try {
            isystemservice = (ISystemService) factory.create(ISystemService.class,
                    serviceurl + ISystemService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        List list = isystemservice.findMapResultByProcedure("sp_RepServer_GetOneRep");
        if (list.size() > 0) {
            Map map = (Map) list.get(0);
            long id = Long.parseLong(objectisnull(map.get("ID")));
            //REP名称
            String name = objectisnull(map.get("C_NAME"));
            //REP地址
            String url = objectisnull(map.get("C_URL"));
            //最大订单数
            int maxNumber = Integer.parseInt(objectisnull(map.get("C_MAXNUMBER")));
            //在下订单数
            //int useNumber = Integer.parseInt(map.get("C_USENUMBER").toString());
            //最后交互时间
            Timestamp lastTime = new Timestamp(System.currentTimeMillis());
            //SET
            rep.setId(id);
            rep.setName(name);
            rep.setUrl(url);
            rep.setMaxNumber(maxNumber);
            rep.setUseNumber(0);
            rep.setLastTime(lastTime);
        }
        return rep;

    }

    /**
     * 12306发送验证码的登录线程
     * 
     * @time 2015年5月8日 下午10:37:15
     * @author chendong
     * @param int login_count  登录多少个账号
     */
    private void execute_mobile(int login_count) {
        int getType = 1;
        String toLogin_mobileString = getenableaccountbyMobile(getType);//获得哪些手机号当前状态不是登录中的手机号
        String[] toLogin_mobileStrings = toLogin_mobileString.split(",");
        // 创建一个可重用固定线程数的线程池
        ExecutorService pool = Executors.newFixedThreadPool(120);
        // 创建实现了Runnable接口对象，Thread对象当然也实现了Runnable接口
        Thread t1 = null;
        for (int i = 0; i < toLogin_mobileStrings.length; i++) {
            try {
                Thread.sleep(50L);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
            //            RepServerBean rep = randomIp();
            // 将线程放入池中进行执行
            //            t1 = new MyThreadLogin(toLogin_mobileStrings[i], rep);
            //            pool.execute(t1);
        }
        // 关闭线程池
        pool.shutdown();
    }

    private void execute() {
        int loginnum = 80;//拿常旅客低于多少的人
        String sql = "SELECT COUNT(ID) FROM T_CUSTOMERUSER WITH (NOLOCK) WHERE C_TYPE=4 AND C_STATE=1 AND C_ENNAME='1' AND C_ISENABLE=1 "
                + " AND C_LOGINNUM>0 AND C_LOGINNUM<90 ";
        //获取当前可用账号个数   C_STATE=1（已登录） AND C_ENNAME='1'（正在使用） AND C_ISENABLE=1
        int countAvailable = Server.getInstance().getMemberService().countCustomeruserBySql(sql);
        //        int indexsum = Integer.valueOf(getSysconfigString("indexsum"));
        int indexsum = Integer.valueOf(getSysconfigStringbydb("indexsum"));
        WriteLog.write("JobIndexAgain", "当前可用用下单的账号个数为:" + countAvailable + ":最低可用保证数量:" + indexsum);
        //获取当前可用账号个数如果少于100自动登录
        if (countAvailable < indexsum) {
            exThreadindex(countAvailable, indexsum, loginnum);
        }
    }

    /**
     * /**
     * 说明：12306账号自动登录 
     * 当前可用账号不够的话调用这个方法登录12306账号
     * @param countAvailable    当前可用数
     * @param indexsum          最低可用保证数
     * @param agentid           
     * @time 2015年1月3日 上午12:14:15
     * @author 
     * @param loginnum 
     */
    public void exThreadindex(int countAvailable, int indexsum, int loginnum) {
        int r1 = new Random().nextInt(1000000);
        Long l1 = System.currentTimeMillis();
        List<Customeruser> customerusers = getcustomerusers(countAvailable, indexsum, r1, loginnum);
        WriteLog.write("JobIndexAgain", ":最低可用保证数量:" + indexsum);
        // 创建一个可重用固定线程数的线程池
        ExecutorService pool = Executors.newFixedThreadPool(100);
        // 创建实现了Runnable接口对象，Thread对象当然也实现了Runnable接口
        Thread t1 = null;
        //1'获取待出票订单
        String sql_customeruser = "";
        for (int i = 0; i < customerusers.size(); i++) {
            try {
                Thread.sleep(100L);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
            //            String url = randomIp();
            //            t1 = new MyThreadLogin(customeruser, url);
            //            RepServerBean rep = getrepurl(1);
            Customeruser user1 = customerusers.get(i);
            sql_customeruser += "" + user1.getId();
            if (i < customerusers.size() - 1) {
                sql_customeruser += ",";
            }
            // 将线程放入池中进行执行
            //            t1 = new MyThreadLogin(user1, rep);
            //            pool.execute(t1);
        }
        if (customerusers.size() > 0) {
            try {//把账号改为登陆中（C_STATE=4）的状态
                String sql = "UPDATE T_CUSTOMERUSER SET C_STATE=4 where id in(" + sql_customeruser + ")";
                int icount = Server.getInstance().getSystemService().excuteAdvertisementBySql(sql);
                WriteLog.write("JobIndexAgain", r1 + ":icount:" + icount + ":" + sql);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        WriteLog.write("JobIndexAgain",
                r1 + ":本批次结束:耗时:" + (System.currentTimeMillis() - l1) + ":" + customerusers.size());
        // 关闭线程池
        pool.shutdown();
    }

    /**
     * 
     * 
     * @param countAvailable    当前可用数
     * @param indexsum          最低可用保证数
     * @param agentid  
     * @param r1
     * @return
     * @time 2015年3月12日 下午9:09:55
     * @author chendong
     */
    private List<Customeruser> getcustomerusers(int countAvailable, int indexsum, int r1, int loginnum) {
        indexsum = indexsum - countAvailable;

        String where = "";
        List<Customeruser> customerusers = new ArrayList<Customeruser>();
        //        while (customerusers.size() < indexsum && loginnum < 80) {//从50个开始如果没有就+10加到80
        //            where = " WHERE C_ISENABLE=1 AND C_STATE=0 AND C_LOGINNUM<" + loginnum + " AND C_TYPE=4 AND "
        //                    + Customeruser.COL_createtime + "<'" + gettodayString() + "' ";
        //            customerusers = Server.getInstance().getMemberService()
        //                    .findAllCustomeruser(where, " ORDER BY C_LOGINNUM ASC ", indexsum, 0);
        //            loginnum += 10;
        //        }

        String sql = "select top " + indexsum
                + " ID,C_LOGINNAME,C_LOGPASSWORD,C_MOBILE from T_CUSTOMERUSER with(nolock) "
                + "WHERE C_ISENABLE=1 AND C_TYPE=4 AND C_STATE=0 AND C_LOGINNUM<" + loginnum + " AND C_CREATETIME<'"
                + new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + "' ORDER BY C_LOGINNUM ASC ";
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        for (int i = 0; i < list.size(); i++) {
            Customeruser customeruser = new Customeruser();
            Map map = (Map) list.get(i);
            long id = Long.parseLong(map.get("ID").toString());//ID
            String C_LOGINNAME = map.get("C_LOGINNAME").toString();
            String C_LOGPASSWORD = map.get("C_LOGPASSWORD").toString();
            String C_MOBILE = map.get("C_MOBILE").toString();
            customeruser.setId(id);
            customeruser.setLoginname(C_LOGINNAME);
            customeruser.setLogpassword(C_LOGPASSWORD);
            customeruser.setMobile(C_MOBILE);
            customerusers.add(customeruser);
        }

        /*
         * if (customerusers.size() == 0) {//如果还为0就拿出 一天用一次的或者发车后再用的订单的账号去使用
            where = " WHERE (C_ISENABLE=4 OR C_ISENABLE=3) AND C_STATE=0 AND C_LOGINNUM<" + loginnum
                    + " AND C_TYPE=4 AND " + Customeruser.COL_createtime + "<'" + gettodayString() + "' ";
            customerusers = Server.getInstance().getMemberService()
                    .findAllCustomeruser(where, " ORDER BY C_LOGINNUM ", indexsum, 0);
        }*/
        WriteLog.write("JobIndexAgain", r1 + ":准备登录的账号总数:" + customerusers.size() + ":" + where);
        return customerusers;
    }

    /**
     * 全自动登录,返回cookie
     * 
     * @param logname 12306账号  
     * @param logpassword 12306密码
     * @return
     * @time 2014年12月19日 下午7:41:51
     * @author chendong
     */
    public static String rep12Method(String logname, String logpassword, String repUrl) {
        String resultString = "";
        String paramContent = "datatypeflag=12&logname=" + logname + "&logpassword=" + logpassword
                + "&damarule=2,1,2,4,1";
        resultString = SendPostandGet.submitPost(repUrl, paramContent).toString();
        return resultString;
    }

    /**
     * 获得哪些手机号当前状态不是登录中的手机号
     * 
     * @return
     * @time 2015年5月8日 下午9:45:01
     * @author chendong
     * @param getType  //两种情况
        //1、获取所有正在登录的账号所属的手机号
        //2、获取所有在线的账号所属的手机号
     */
    public String getenableaccountbyMobile(int getType) {
        String allmobileString = PropertyUtil.getValue("sms_mobile", "train.properties") + ",";
        List<String> mobiles = new ArrayList<String>();
        //获取所有正在登陆的账号所属的手机号
        //换句话说是获取当前可以登录的手机号
        //两种情况
        //1、获取所有正在登录的账号所属的手机号
        //2、获取所有在线的账号所属的手机号   包含正在登陆的手机号
        List list = Server.getInstance().getSystemService()
                .findMapResultByProcedure("sp_CustomerUser_JobIndexAgain_getcustomerusers_InTheLogin @" + getType);
        for (int i = 0; i < list.size(); i++) {
            Map map = (Map) list.get(i);
            String mobile = map.get("mobile") == null ? "" : map.get("mobile").toString();
            allmobileString = allmobileString.replace(mobile + ",", "");//把数据库里能查到的的手机号过滤掉，剩下没有的
        }
        allmobileString = allmobileString.substring(0, allmobileString.length() > 0 ? allmobileString.length() - 1
                : allmobileString.length());
        return allmobileString;
    }

}
