package com.ccservice.inter.job.train;

/**
 * APP业务类型
 * @author WH
 * @time 2017年7月24日 下午2:50:27
 * @version 1.0
 */

public class AppBusinessType {

    /**
     * 线上退票
     */
    public static final int REFUND_ONLINE = 100;

    /**
     * （线上）退票审核
     */
    public static final int REFUND_CHECK = 101;

    /**
     * 改签占座
     */
    public static final int CHANGE_CREATE = 102;

    /**
     * 改签确认
     */
    public static final int CHANGE_CONFIRM = 103;

    /**
     * 改签审核
     */
    public static final int CHANGE_CHECK = 104;

    /**
     * 改签排队
     */
    public static final int CHANGE_PAIDUI = 105;

}