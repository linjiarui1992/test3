/**
 * 
 */
package com.ccservice.inter.job.train.temp;

import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;

import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;

import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.service12306.TimeUtil;
import com.ccservice.b2b2c.base.service.ISystemService;

/**
 * 
 * @time 2015年10月16日 下午9:20:07
 * @author chendong
 */
public class JobShenhe implements Job {
    public static void main(String[] args) {
        //        JobShenhe jobShenhe = new JobShenhe();
        //        jobShenhe.execute1();
        try {
            startScheduler("0/5 0/1 7-23 * * ?");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* (non-Javadoc)
     * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
     */
    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        execute1();
    }

    /**
     * 
     * @time 2015年10月16日 下午9:27:37
     * @author chendong
     */
    private void execute1() {
        String sql = "select top 10 ID,C_ORDERSTATUS,C_CREATETIME,C_QUNARORDERNUMBER,C_ORDERNUMBER,C_AGENTID from T_trainorder "
                + "where C_ORDERSTATUS=2 and C_ISQUESTIONORDER=2  and C_ISPLACEING!=2 and C_CREATETIME>'"
                + TimeUtil.gettodaydate(1) + "'  order by id ";
        ISystemService isystemservice = getISystemservice();
        List list = isystemservice.findMapResultBySql(sql, null);
        for (int i = 0; i < list.size(); i++) {
            Map map = (Map) list.get(i);
            long ID = Long.parseLong(objectisnull(map.get("ID")));
            System.out.println(ID);
            //            http://121.199.25.199:45010/ticket_inter/QueryTrainorder.jsp?id=
            //            http://120.26.100.206:49410/ticket_inter/QueryTrainorder.jsp?id=
            String url = "http://121.199.25.199:45010/ticket_inter/QueryTrainorder.jsp?id=" + ID;
            String result = SendPostandGet.submitGet(url, "UTF-8");
            System.out.println(result);
            String qunarorderid = objectisnull(map.get("C_QUNARORDERNUMBER"));
            String ordernumber = objectisnull(map.get("C_ORDERNUMBER"));
            int agentid = Integer.parseInt(map.get("C_AGENTID").toString());
            String callBackUrl = "http://121.199.25.199:35216/cn_interface/tcTrainCallBack";
            try {
                Thread.sleep(1000L);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            JobShenheThread jobShenheThread = new JobShenheThread(ID, new JobShenhe().getISystemservice(),
                    qunarorderid, ordernumber, agentid, callBackUrl);
            jobShenheThread.start();
        }
    }

    /**
     * 如果为空返回 "0"
     * 
     * @param object
     * @return
     * @time 2015年4月15日 下午1:57:54
     * @author chendong
     */
    public String objectisnull(Object object) {
        return object == null ? "0" : object.toString();
    }

    /**
     * 
     * @time 2015年10月16日 下午9:25:02
     * @author chendong
     */
    public ISystemService getISystemservice() {
        ISystemService isystemservice = null;
        //        String serviceurl = "http://121.40.62.200:40000/cn_service/service/";
        String serviceurl = "http://121.199.25.199:9001/cn_service/service/";
        try {
            isystemservice = (ISystemService) new HessianProxyFactory().create(ISystemService.class, serviceurl
                    + ISystemService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return isystemservice;
    }

    // 设置调度任务及触发器，任务名及触发器名可用来停止任务或修改任务
    public static void startScheduler(String expr) throws Exception {
        JobDetail jobDetail = new JobDetail("JobShenhe", "JobShenheGroup", JobShenhe.class);// 任务名，任务组名，任务执行类
        CronTrigger trigger = new CronTrigger("JobShenhe", "JobShenheGroup", expr);// 触发器名，触发器组名
        //        String expr = "1/5 * * * * ? *";
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
        scheduler.scheduleJob(jobDetail, trigger);
        scheduler.start();
    }
}
