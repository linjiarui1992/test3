package com.ccservice.inter.job.train;

import java.util.*;
import java.text.SimpleDateFormat;

import com.ccservice.inter.job.train.reg.Job12306Registration_JihuoThread;
import com.ccservice.inter.server.Server;

public class JiHuoAccount {

    private static SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

    @SuppressWarnings("rawtypes")
    public static void main(String[] args) {
        String sql = "select ID from T_CUSTOMERUSER with(nolock) where C_TYPE=4 and C_ISENABLE=9 order by ID desc";
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        for (int i = 0; i < list.size(); i++) {
            if (isNight()) {
                break;
            }
            try {
                Thread.sleep(2000);
            }
            catch (Exception e) {
            }
            Map map = (Map) list.get(i);
            //            new Job12306Registration_JihuoThread(i, Long.parseLong(map.get("ID").toString())).start();
        }
    }

    //判断当前时间23:30至07:00
    private static boolean isNight() {
        boolean isNight = false;
        try {
            //当前
            Date current = sdf.parse(sdf.format(new Date()));
            //23:30 - 07:00
            Date start = sdf.parse("23:55:00");
            Date end = sdf.parse("07:05:00");
            //判断
            if (current.after(start) || current.before(end)) {
                isNight = true;
            }
        }
        catch (Exception e) {

        }
        return isNight;
    }

}