package com.ccservice.inter.job.train;

import java.util.List;
import java.util.Map;

import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.base.rebaterecord.Rebaterecord;
import com.ccservice.inter.server.Server;

public class RebateRecordtest {
    public static void main(String[] args) {
        String where = " where C_REBATETYPE=5 and C_YEWUTYPE=3 and C_REBATEMEMO like '%退票退款%' ";
        List<Rebaterecord> list = Server.getInstance().getMemberService()
                .findAllRebaterecord(where, "order by id asc ", 10000, 0);
        for (int i = 0; i < list.size(); i++) {
            Rebaterecord record = list.get(i);
            String sql = "select (T.C_PRICE - T.C_PROCEDURE) PRICE,T.ID from T_TRAINTICKET T join T_TRAINPASSENGER P on T.C_TRAINPID=P.ID WHERE T.C_STATUS=10 and P.C_ORDERID="
                    + record.getOrderid();
            List maplist = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
            System.out.println(record.getOrdernumber());
            if (maplist.size() > 0) {
                if (maplist.size() > 1) {
                    boolean flag = false;
                    for (int j = 0; j < maplist.size(); j++) {
                        if (flag) {
                            continue;
                        }
                        Map m = (Map) maplist.get(j);
                        String price = m.get("PRICE").toString();
                        String id = m.get("ID").toString();
                        String sqlt = "select count(id) count from T_REBATERECORD where C_YEWUTYPE=31 and C_ORDERID="
                                + id;
                        List ll = Server.getInstance().getSystemService().findMapResultBySql(sqlt, null);
                        if (ll.size() > 0) {
                            Map llmap = (Map) ll.get(0);
                            String count = llmap.get("count").toString();
                            if (Long.valueOf(count) > 0) {
                                continue;
                            }
                            else {
                                if (record.getRebatemoney().floatValue() == Float.valueOf(price).floatValue()) {
                                    WriteLog.write("交易记录处理",
                                            record.getOrdernumber() + " ：票 " + id + " : 订单：" + record.getOrderid()
                                                    + " : " + price + " : " + record.getRebatemoney());
                                    record.setOrderid(Long.valueOf(id));
                                    record.setYewutype(31);
                                    String updatesql = "update T_REBATERECORD set C_ORDERID=" + Long.valueOf(id)
                                            + ",C_YEWUTYPE=31 where ID=" + record.getId();
                                    System.out.println(updatesql);
                                    Server.getInstance().getSystemService().findMapResultBySql(updatesql, null);
                                    flag = true;
                                }
                            }
                        }
                    }
                }
                else {
                    try {
                        Map m = (Map) maplist.get(0);
                        String price = m.get("PRICE").toString();
                        String id = m.get("ID").toString();
                        if (record.getRebatemoney().floatValue() == Float.valueOf(price).floatValue()) {
                            WriteLog.write("交易记录处理",
                                    record.getOrdernumber() + " ：票 " + id + " : 订单：" + record.getOrderid() + " : "
                                            + price + " : " + record.getRebatemoney());
                            record.setOrderid(Long.valueOf(id));
                            record.setYewutype(31);
                            String updatesql = "update T_REBATERECORD set C_ORDERID=" + Long.valueOf(id)
                                    + ",C_YEWUTYPE=31 where ID=" + record.getId();
                            System.out.println(updatesql);
                            Server.getInstance().getSystemService().findMapResultBySql(updatesql, null);
                        }
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
