package com.ccservice.inter.job;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.ElementHandler;
import org.dom4j.ElementPath;
import org.dom4j.XPath;

import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.atom.server.Server;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.b2b2c.base.hotelimage.Hotelimage;
import com.ccservice.b2b2c.base.roomtype.Roomtype;
import com.ccservice.b2b2c.base.service.IHotelService;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.compareprice.PropertyUtil;

public class JobElongHotelDBElementHandler implements ElementHandler{
		public static String url = Server.getInstance().getUrl();
		public static HessianProxyFactory factory = new HessianProxyFactory();;
		public static IHotelService servier;
		public static ISystemService systemservier;
		static {
			try {
				servier = (IHotelService) factory.create(IHotelService.class, url + IHotelService.class.getSimpleName());
				systemservier = (ISystemService) factory.create(ISystemService.class, url + ISystemService.class.getSimpleName());
			} catch (Exception e) {
			}
		}

		@Override
		public void onEnd(ElementPath path) {
			// TODO Auto-generated method stub
			// 获取当前节点
			Element row = path.getCurrent();
			// System.out.println("row:"+row);
			// 对节点进行操作...
			String tree = path.getPath().substring(26);
			// System.out.println("tree:"+tree);
			if (tree.equals("HotelInfoForIndex")) {
				anayHotel(row);
			}
			// 处理当前节点后,将其从dom树种剪除
			row.detach();
		}

		@Override
		public void onStart(ElementPath path) {
			// TODO Auto-generated method stub

		}

		@SuppressWarnings("unchecked")
		public void anayHotel(Element root){
			//System.out.println("updateHotel..............");
			List<Element> list = root.elements();
			Hotel hotel = new Hotel();
			String isOk = "";
			String hotelCode = "";
			String Modifytime="";
			for (Element e : list) {
				// 酒店是否可用
				if (e.getQName().getName().equals("Isreserve") &&! e.getText().equals("")) {
					isOk = e.getText();
				}
				// 酒店中文名
				if (e.getQName().getName().equals("Hotel_name") && !e.getText().equals("")) {
					hotel.setName(e.getText());
				}
				//酒店英文名
				if(e.getQName().getName().equals("Hotel_name_en") && !e.getText().equals("")){
					hotel.setEnname(e.getText());
				}
				
				//酒店更新时间
				if(e.getQName().getName().equals("Modifytime") && !e.getText().equals("")){
					Modifytime=e.getText();
				}
				
				// 酒店id（酒店在艺龙系统中的唯一标识HotelID）
				if (e.getQName().getName().equals("Hotel_id") && !e.getText().equals("")) {
					hotelCode = e.getText();
					//System.out.println("酒店ID:" + hotelCode);
				}
			}
			PropertyUtil pu=new PropertyUtil();
			int eltime = Integer.parseInt(pu.getValue("eltime"));
			
			Calendar end = GregorianCalendar.getInstance();
			end.add(Calendar.DAY_OF_MONTH, -eltime);
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			//再多加半小时，避免时间差漏掉
			long yestnow =end.getTime().getTime()-1800000;
			long mod=0;
			try {
				mod = sdf.parse(Modifytime).getTime();
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if(mod>yestnow){
				System.out.println("更新时间："+sdf.format(mod));
				System.out.println("昨天时间："+sdf.format(yestnow));
				if (isOk.equals("0")) {
					hotel.setState(3);
					hotel.setSourcetype(1l);
					hotel.setStatedesc("该酒店可用");
					hotel.setHotelcode(hotelCode);
					List<Hotel> hotelList = servier.findAllHotel("where " + Hotel.COL_hotelcode + "='" + hotelCode + "'", "", -1, 0);
					
					if (hotelList.size() > 0) {
						hotel.setId(hotelList.get(0).getId());
						servier.updateHotelIgnoreNull(hotel);
					} else {
						try {
							hotel = servier.createHotel(hotel);
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
					anay(hotel.getHotelcode(), hotel.getId());
				}else{
					
					System.out.println("酒店不可用");
					/**
					 * 删除不可用数据
					 */
					List<Hotel> hotelList = servier.findAllHotel("where " + Hotel.COL_hotelcode + "='" + hotelCode + "'", "", -1, 0);
					if (hotelList.size() > 0) {
						hotel.setId(hotelList.get(0).getId());
						
						String where=" where C_HOTELID='"+hotelList.get(0).getId()+"'";
						List<Roomtype> roomtype = servier.findAllRoomtype(where, "", -1, 0);
						if(roomtype.size()>0){
							for (int i = 0; i < roomtype.size(); i++) {
								System.out.println("删除一条房型");
								servier.deleteRoomstate(roomtype.get(i).getId());
							}
						}
						List<Hotelimage> hotelimage = servier.findAllHotelimage(where, "", -1, 0);
						if(hotelimage.size()>0){
							for (int i = 0; i < hotelimage.size(); i++) {
								System.out.println("删除一条图片信息");
								servier.deleteHotelimage(hotelimage.get(i).getId());
							}
						}
						
						servier.deleteHotel(hotelList.get(0).getId());
				}
			}
			}
			
		}
		@SuppressWarnings("unchecked")
		public static void anay(String hotelCode, long hotelId) {
			String urltemp = "http://114-svc.elong.com/xml/v1.2/perhotelcn/" + hotelCode + ".xml";
			System.out.println("urltemp==" + urltemp);
			try {
				URL url = new URL(urltemp);
				URLConnection con = url.openConnection();
				//设置连接主机超时(单位:毫秒)
				con.setConnectTimeout(120000);
				//设置从主机读取数据超时(单位:毫秒)
				con.setReadTimeout(120000);
				//是否想url输出
				con.setDoOutput(true);
				//设定传送的内容类型是可序列化的对象
				con.setRequestProperty("Content-type", "application/x-java-serialized-object");
				String sCurrentLine;
				StringBuilder sTotalString=new StringBuilder();
				sCurrentLine = "";
				InputStream in = con.getInputStream();
				BufferedReader bf = new BufferedReader(new InputStreamReader(in, "utf-8"));
				while ((sCurrentLine = bf.readLine()) != null) {
					sTotalString.append(sCurrentLine);
				}
				// System.out.println(sTotalString);
				Map map = new HashMap();
				map.put("q1:", "http://api.elong.com/staticInfo");
				org.dom4j.Document document = DocumentHelper.parseText(sTotalString.toString());
				XPath path = document.createXPath("HotelDetail");
				path.setNamespaceURIs(map);
				List nodelist = path.selectNodes(document);
				JobElongHotelBaseUpdate.parseHotelXML(nodelist, hotelId);
				bf.close();
				in.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
}
