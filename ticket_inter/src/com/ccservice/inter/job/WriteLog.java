package com.ccservice.inter.job;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Calendar;

public class WriteLog {

    /**
     * 写日志<br>
     * 
     * 写logString字符串到./log目录下的文件中
     * 
     * @param logString
     *            日志字符串
     * 
     * @author tower
     * 
     */

    public static void write(String fileNameHead, String logString) {
        try {
            String logFilePathName = null;
            Calendar cd = Calendar.getInstance();// 日志文件时间
            int year = cd.get(Calendar.YEAR);
            String month = addZero(cd.get(Calendar.MONTH) + 1);
            String day = addZero(cd.get(Calendar.DAY_OF_MONTH));
            String hour = addZero(cd.get(Calendar.HOUR_OF_DAY));
            String min = addZero(cd.get(Calendar.MINUTE));
            String sec = addZero(cd.get(Calendar.SECOND));
            String mill = addZero(cd.get(Calendar.MILLISECOND));
            String path = "D:/userlog/" + year + month + "/" + day + "/" + hour;
            File fileParentDir = new File(path);// 判断log目录是否存在
            if (!fileParentDir.exists()) {
                fileParentDir.mkdirs();
            }
            if (fileNameHead == null || fileNameHead.equals("")) {
                logFilePathName = path + "/" + year + month + day + ".log";// 日志文件名
            }
            else {
                logFilePathName = path + "/" + fileNameHead + ".log";// 日志文件名
            }
            PrintWriter printWriter = new PrintWriter(new FileOutputStream(logFilePathName, true));// 紧接文件尾写入日志字符串
            String time = "[" + year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec + "." + mill + "] ";
            printWriter.println(time + logString);
            printWriter.flush();
        }
        catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.getMessage();
        }
    }

    /**
     * 
     * @param foldername 文件夹名字
     * @param fileNameHead 文件名
     * @param logString 内容
     */
    public static void write(String foldername, String fileNameHead, String logString) {
        try {
            String logFilePathName = null;
            Calendar cd = Calendar.getInstance();// 日志文件时间
            int year = cd.get(Calendar.YEAR);
            String month = addZero(cd.get(Calendar.MONTH) + 1);
            String day = addZero(cd.get(Calendar.DAY_OF_MONTH));
            String hour = addZero(cd.get(Calendar.HOUR_OF_DAY));
            String min = addZero(cd.get(Calendar.MINUTE));
            String sec = addZero(cd.get(Calendar.SECOND));
            String mill = addZero(cd.get(Calendar.MILLISECOND));
            String path = "D:/userlog/" + year + month + "/" + day + "/" + hour;
            File fileParentDir = new File(path + "/" + foldername);// 判断log目录是否存在
            if (!fileParentDir.exists()) {
                fileParentDir.mkdirs();
            }
            if (fileNameHead == null || fileNameHead.equals("")) {
                logFilePathName = path + "/" + foldername + "/" + year + month + day + ".log";// 日志文件名
            }
            else {
                logFilePathName = path + "/" + foldername + "/" + fileNameHead + ".log";// 日志文件名
            }
            PrintWriter printWriter = new PrintWriter(new FileOutputStream(logFilePathName, true));// 紧接文件尾写入日志字符串
            String time = "[" + year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec + "." + mill + "] ";
            printWriter.println(time + logString);
            printWriter.flush();

        }
        catch (FileNotFoundException e) {
            e.getMessage();
            e.printStackTrace();
        }
    }

    /**
     * 火车票创建订单消费者记录日志的方法
     * @param logString 内容
     * @param orderNumber 关联订单这个是为该订单单独建立的新的文件只记录这个业务
     */
    public static void TrainCreateOrderWrite(String logString, String orderNumber) {
        write("trainCreateOrder", orderNumber, logString);
    }

    /**
     * 整数i小于10则前面补0
     * 
     * @param i
     * 
     * @return
     * 
     * @author tower
     * 
     */
    public static String addZero(int i) {
        if (i < 10) {
            String tmpString = "0" + i;
            return tmpString;
        }
        else {
            return String.valueOf(i);
        }
    }

    public static void main(String[] args) {
        // 前面是文件名字,后面是内容
        write("test", "4444");
    }

}
