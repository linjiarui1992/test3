package com.ccservice.inter.job.zhifubao;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.base.sysconfig.Sysconfig;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;

/**
 * 
 * @author wzc
 * @version 创建时间：2015年6月12日 下午2:43:42
 */
public class PayTradeServlet extends HttpServlet {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        res.setCharacterEncoding("UTF-8");
        res.setContentType("text/plain; charset=utf-8");
        res.setHeader("content-type", "text/html;charset=UTF-8");
        String jsonStr = req.getParameter("jsonStr");
        if (jsonStr != null && jsonStr.length() > 0) {
            WriteLog.write("交易记录cmd", jsonStr);
            JSONObject jso = JSONObject.parseObject(jsonStr);
            String cmd = jso.getString("cmd");
            if ("logToDatabase".equals(cmd)) {//交易记录入库
                WriteLog.write("交易记录cmd", "开始交易记录入库");
                PayLogToDatabase.exelogToDatabase();
            }
            else if ("PayTradetoOrder".equals(cmd)) {//匹配交易记录
                WriteLog.write("交易记录cmd", "开始匹配交易记录");
                String type = jso.getString("type");
                int codetype = 2;
                try {
                    codetype = Integer.parseInt(type);
                }
                catch (NumberFormatException e) {
                }
                String tradeupdateflag = getSysconfigStringbydb("tradeupdateflag");
                if ("1".equals(tradeupdateflag)) {
                    changeSystemCofigbyname("tradeupdateflag", "2");
                    PayTradetoOrder.updateTradeRecordStart(codetype, "", "");
                    changeSystemCofigbyname("tradeupdateflag", "1");
                }
                else {
                    WriteLog.write("交易记录cmd", cmd + ":更新中");
                    System.out.println("交易记录匹配关闭");
                }
            }
            PrintWriter out = res.getWriter();
            out.print("success");
            out.flush();
            out.close();
        }
    }

    /**
     * 根据sysconfig的name获得value
     * 实时 如果是判断的必须调用实时接口
     * @param name
     * @return
     */
    public String getSysconfigStringbydb(String name) {
        String result = "-1";
        List<Sysconfig> sysoconfigs = Server.getInstance().getSystemService()
                .findAllSysconfig("WHERE C_NAME='" + name + "'", "", -1, 0);
        if (sysoconfigs.size() > 0) {
            result = sysoconfigs.get(0).getValue();
        }
        return result;
    }

    /**
     * 修改config的value
     * 
     * @param name
     * @param value
     * @time 2014年12月11日 下午10:18:43
     * @author chendong
     */
    public void changeSystemCofigbyname(String name, String value) {
        Server.getInstance().getSystemService()
                .findMapResultBySql("update T_SYSCONFIG set C_VALUE='" + value + "' where C_NAME='" + name + "'", null);

    }
}
