package com.ccservice.inter.job.zhifubao;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.b2b2c.base.customeragent.Customeragent;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.b2b2c.base.sysconfig.Sysconfig;
import com.ccservice.component.excel.HthyWorkSheet;
import com.ccservice.component.excel.HthyWritableWorkBook;
import com.ccservice.inter.server.Server;

/**
 * 定时生成12306支付报表
 * @author wzc
 * @version 创建时间：2015年10月18日 下午5:35:00
 */
public class Job12306PayRecord implements Job {

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        List<Customeragent> agentlist = Server.getInstance().getMemberService()
                .findAllCustomeragent("where id>=46", "order by id asc ", -1, 0);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, -1);
        String begin_time = sdf.format(cal.getTime());
        String end_time = sdf.format(cal.getTime());
        for (int i = 0; i < agentlist.size(); i++) {
            Customeragent agent = agentlist.get(i);
            try {
                report12306(begin_time, end_time, agent.getId(), agent.getAgentcompanyname());
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * 获取系统配置属性 实时
     */
    public static String getSystemConfig(String name) {
        List<Sysconfig> configs = Server.getInstance().getSystemService()
                .findAllSysconfig("where c_name='" + name + "'", "", -1, 0);
        if (configs != null && configs.size() == 1) {
            Sysconfig config = configs.get(0);
            return config.getValue();
        }
        return "-1";
    }

    public static void main(String[] args) {
        List<Customeragent> agentlist = Server.getInstance().getMemberService()
                .findAllCustomeragent("where id>=46", "order by id asc ", -1, 0);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, -1);
        String begin_time = sdf.format(cal.getTime());
        String end_time = sdf.format(cal.getTime());
        for (int i = 0; i < agentlist.size(); i++) {
            Customeragent agent = agentlist.get(i);
            try {
                new Job12306PayRecord().report12306(begin_time, end_time, agent.getId(), agent.getAgentcompanyname());
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 
     * @param begin_timetemp
     * @param end_timetemp
     * @param tcagentid
     * @param compayname
     * @return
     * @throws Exception
     */
    public void report12306(String begin_timetemp, String end_timetemp, long tcagentid, String compayname)
            throws Exception {
        if (compayname == null || "".equals(compayname)) {
            compayname = tcagentid + "";
        }
        ISystemService service = Server.getInstance().getSystemService();
        List outticketorder12306 = service.findMapResultByProcedure("sp_12306Trainreport @begintime='" + begin_timetemp
                + "',@endtime ='" + end_timetemp + "',@agentid = " + tcagentid + ",@orderstatus=3");
        List<String> titlelist = new ArrayList<String>();
        titlelist.add("预定日期");
        titlelist.add("订单编号");
        titlelist.add("采购商");
        titlelist.add("采购交易号");
        titlelist.add("支付帐号");
        titlelist.add("支付12306金额");
        titlelist.add("支付12306帐号");
        titlelist.add("12306交易号");
        titlelist.add("订单状态");
        String[] titleles = (String[]) titlelist.toArray(new String[titlelist.size()]);
        Map<Integer, Integer> column = new HashMap<Integer, Integer>();
        column.put(1, 100);
        column.put(2, 80);
        column.put(3, 80);
        column.put(4, 60);
        column.put(5, 80);
        column.put(6, 100);
        column.put(7, 80);
        column.put(8, 80);
        int row = 200;
        try {
            row = (outticketorder12306.size()) * 20;
            if (row <= 0) {
                row = 200;
            }
        }
        catch (Exception e2) {
        }
        HthyWritableWorkBook book = HthyWritableWorkBook.getInstanceWrite();
        HthyWorkSheet sheet = book.createHthyWorkSheet("12306火车票报表", titleles.length + 10, row, column);
        sheet.createOneRow(titleles, HthyWorkSheet.CenterBlod);
        if (outticketorder12306 != null && outticketorder12306.size() > 0) {//正常出票
            for (int i = 0; i < outticketorder12306.size(); i++) {
                Map map = (Map) outticketorder12306.get(i);
                String createtime = parseNullData(map.get("C_CREATETIME")).toString();//预定日期
                String ordernumber = parseNullData(map.get("C_ORDERNUMBER")).toString();////我们订单号
                String agentid = parseNullData(map.get("C_AGENTID")).toString();//代理商ID
                String supplyaccount = parseNullData(map.get("C_SUPPLYACCOUNT")).toString();//实际支付宝帐号
                String tradenum = parseNullData(map.get("C_TRADENO")).toString();//采购交易号
                String supplytradeno = parseNullData(map.get("C_SUPPLYTRADENO")).toString();//采购交易号
                String totalprice = parseNullData(map.get("C_TOTALPRICE")).toString();//票数
                String account12306 = parseNullData(map.get("C_AUTOUNIONPAYURLSECOND")).toString();//支付12306帐号
                String orderstate = map.get("C_ORDERSTATUS") == null ? "0" : map.get("C_ORDERSTATUS").toString();//
                sheet.createRow();
                sheet.addCell(createtime);//预定日期
                sheet.addCell(ordernumber);//订单编号
                sheet.addCell(compayname);//采购商
                sheet.addCell(tradenum);//采购交易号
                sheet.addCell(supplyaccount);//支付帐号
                sheet.addCell(totalprice);//支付金额
                sheet.addCell(account12306);//支付12306帐号
                sheet.addCell(supplytradeno);//12306交易号
                sheet.addCell(getOrderstatusstr(Integer.parseInt(orderstate)));//12306交易号
                sheet.rowOver();
            }
        }
        System.gc();
        sheet.sheetOver();
        book.writestr();
        book.closestr();
        String filename = compayname + "12306出票报表" + begin_timetemp + ".xls";
        String path = getSystemConfig("exportreportpath");
        if ("-1".equals(path)) {
            path = "d:";//默认地址
        }
        File f = new File(path + "/report/" + tcagentid + "/");
        if (!f.exists()) {
            f.mkdirs();
        }
        String filepath = "/report/" + tcagentid + "/" + filename;
        File file = new File(path + filepath);
        if (file.exists()) {
            file.delete();
        }
        else {
            file.createNewFile();
        }
        FileUtils.writeStringToFile(file, book.getBookbody().toString(), "gb2312");
    }

    public String getOrderstatusstr(int orderstatus) {
        switch (orderstatus) {
        case 1:
            return "等待支付";
        case 2:
            return "等待出票";
        case 3:
            return "已出票";
        case 4:
            return "拒单-等待退款";
        case 5:
            return "拒单-退款中";
        case 6:
            return "拒单-已退款";
        case 7:
            return "拒单-退款失败";
        case 8:
            return "交易关闭";
        }
        return "";
    }

    /**
     * 解析空数据
     * @param data
     * @return
     */
    public Object parseNullData(Object data) {
        if (data == null) {
            return "";
        }
        return data;
    }
}
