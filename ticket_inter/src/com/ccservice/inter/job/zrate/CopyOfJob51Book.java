package com.ccservice.inter.job.zrate;

import java.rmi.RemoteException;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.axis2.AxisFault;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.ben.FiveonBook;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.test.HttpClient;
import com.liantuo.webservice.version2_0.getpolicydata.GetPolicyDataService_2_0Stub;
import com.liantuo.webservice.version2_0.getpolicydata.GetPolicyDataService_2_0Stub.PolicyData;

public class CopyOfJob51Book implements Job {//
    WriteLog writeLog = new WriteLog();

    FiveonBook fiveonBook = new FiveonBook();

    int pageindex = 1;

    int maxpageindex = 1000;

    @Override
    public void execute(JobExecutionContext context) {
        GetZrateBigIndex();//锟斤拷取锟斤拷锟斤拷锟揭筹拷锟�

        GetZrate(1);//锟斤拷页锟斤拷锟矫接匡拷	
    }

    public void GetZrateBigIndex() {
        try {
            GetPolicyDataService_2_0Stub stub = new GetPolicyDataService_2_0Stub();
            GetPolicyDataService_2_0Stub.GetPolicyData data = new GetPolicyDataService_2_0Stub.GetPolicyData();
            GetPolicyDataService_2_0Stub.GetPolicyDataRequest re = new GetPolicyDataService_2_0Stub.GetPolicyDataRequest();
            GetPolicyDataService_2_0Stub.SecurityCredential sec = new GetPolicyDataService_2_0Stub.SecurityCredential();

            re.setAirline("");
            re.setDeparture("");
            re.setArrival("");
            re.setIsIncludeSpecialPolicy("1");
            re.setIsBusinessUnitPolicy("1");
            re.setPage(1);
            re.setRowPerPage(500);

            sec.setAgencyCode(fiveonBook.getAgentcode());
            String sign = HttpClient.MD5(sec.getAgencyCode() + re.getAirline() + re.getDeparture() + re.getArrival()
                    + re.getPage() + re.getRowPerPage() + re.getIsIncludeSpecialPolicy() + re.getIsBusinessUnitPolicy()
                    + fiveonBook.getSafecode());
            sec.setSign(sign);

            re.setCredential(sec);
            data.setIn0(re);

            GetPolicyDataService_2_0Stub.GetPolicyDataResponse res = stub.getPolicyData(data);

            System.out.println("---" + res.getOut().getReturnCode() + "--" + res.getOut().getReturnMessage());
            System.out.println("锟斤拷页锟斤拷==" + res.getOut().getPageCount());

            writeLog.write("51bookZrate", "锟斤拷页锟斤拷:" + res.getOut().getPageCount());
            //System.out.println("锟斤拷锟斤拷锟斤拷=="+res.getOut().getPolicyDatasList().getPolicyData().length);

            int p = res.getOut().getPageCount();
            maxpageindex = p;

        }
        catch (AxisFault e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void GetZrate(int pageindex) {

        try {

            try {
                if (!fiveonBook.getStaus().equals("0")) {

                    String setsql = " UPDATE T_B2BSEQUENCE SET C_VALUE='NULL' WHERE C_NAME='51ZRATETIME'";
                    Server.getInstance().getSystemService().findMapResultBySql(setsql, null);

                    String setsql2 = " UPDATE T_B2BSEQUENCE SET C_VALUE='1' WHERE C_NAME='51ZRATEID'";
                    Server.getInstance().getSystemService().findMapResultBySql(setsql2, null);

                    int sub = 1;
                    if (pageindex == 1) {
                        Server.getInstance().getAirService()
                                .excuteZrateBySql("delete from " + Zrate.TABLE + " where C_AGENTID=5");
                        sub = 1;
                    }
                    else {

                        sub = pageindex;
                    }

                    for (int a = sub; a <= maxpageindex; a++) {
                        writeLog.write("51bookZrate", "锟斤拷前页锟斤拷:" + a);
                        System.out.println("锟斤拷前页锟斤拷=" + a);
                        pageindex = a;
                        GetPolicyDataService_2_0Stub stub1 = new GetPolicyDataService_2_0Stub();
                        GetPolicyDataService_2_0Stub.GetPolicyData data1 = new GetPolicyDataService_2_0Stub.GetPolicyData();
                        GetPolicyDataService_2_0Stub.GetPolicyDataRequest re1 = new GetPolicyDataService_2_0Stub.GetPolicyDataRequest();
                        GetPolicyDataService_2_0Stub.SecurityCredential sec1 = new GetPolicyDataService_2_0Stub.SecurityCredential();

                        re1.setAirline("");
                        re1.setDeparture("");
                        re1.setArrival("");
                        re1.setIsIncludeSpecialPolicy("1");
                        re1.setIsBusinessUnitPolicy("1");
                        re1.setPage(a);
                        re1.setRowPerPage(500);

                        sec1.setAgencyCode(fiveonBook.getAgentcode());
                        String sign1 = HttpClient.MD5(sec1.getAgencyCode() + re1.getAirline() + re1.getDeparture()
                                + re1.getArrival() + re1.getPage() + re1.getRowPerPage()
                                + re1.getIsIncludeSpecialPolicy() + re1.getIsBusinessUnitPolicy()
                                + fiveonBook.getSafecode());
                        sec1.setSign(sign1);

                        re1.setCredential(sec1);
                        data1.setIn0(re1);

                        GetPolicyDataService_2_0Stub.GetPolicyDataResponse res1 = stub1.getPolicyData(data1);
                        //System.out.println("---"+res.getOut().getReturnCode()+"--"+res.getOut().getReturnMessage());
                        //System.out.println("锟斤拷页锟斤拷=="+res1.getOut().getPageCount());
                        int len = 0;
                        if (res1.getOut().getPolicyDatasList() != null) {
                            if (res1.getOut().getPolicyDatasList().getPolicyData() != null) {
                                if (res1.getOut().getPolicyDatasList().getPolicyData().length > 0) {
                                    len = res1.getOut().getPolicyDatasList().getPolicyData().length;
                                }
                            }
                        }
                        //writeLog.write("51bookZrate", "锟斤拷前页锟斤拷:"+a+".....锟斤拷锟斤拷锟斤拷:"+len);
                        //System.out.println("ien=="+len);

                        for (int b = 0; b < len; b++) {

                            //writeLog.write("51bookZrate", "锟斤拷前页锟斤拷:"+a+".....锟斤拷前锟斤拷锟斤拷:"+b);
                            //index++;
                            //System.out.println("index=="+index);
                            if (res1.getOut().getPolicyDatasList().getPolicyData()[b] != null) {

                                PolicyData aa = res1.getOut().getPolicyDatasList().getPolicyData()[b];
                                //writeLog.write("51bookZrate", "锟斤拷前页锟斤拷:"+a+".....锟斤拷前锟斤拷锟斤拷:"+b+"...锟斤拷细锟斤拷息:"+aa);
                                if (aa != null) {
                                    //System.out.println("锟斤拷锟斤拷="+aa.getCommision()+",锟斤拷锟秸癸拷司锟斤拷锟斤拷="+aa.getAirlineCode()+",锟绞合猴拷锟斤拷="+aa.getFlightNoIncluding()+",锟斤拷锟绞合猴拷锟斤拷="+aa.getFlightNoExclude()+",锟斤拷锟斤拷="+aa.getFlightCourse());
                                    Zrate zrate = new Zrate();
                                    /*String where =" where 1=1 and "+Zrate.COL_outid+" ='"+aa.getId()+"' and "+Zrate.COL_agentid+" =5";
                                    List<Zrate>listz= Server.getInstance().getAirService().findAllZrate(where, " ORDER BY ID DESC", -1, 0);
                                    if(listz.size()>0){
                                    	zrate=listz.get(0);
                                    	
                                    	
                                    }*/

                                    zrate.setCreatetime(new Timestamp(System.currentTimeMillis()));
                                    zrate.setCreateuser("job");
                                    zrate.setModifytime(new Timestamp(System.currentTimeMillis()));
                                    zrate.setModifyuser("job");
                                    zrate.setAgentid(5l);
                                    zrate.setTickettype(1);
                                    zrate.setIsenable(1);

                                    zrate.setOutid(aa.getId() + "");
                                    if (aa.getAirlineCode() != null) {
                                        zrate.setAircompanycode(aa.getAirlineCode());
                                    }
                                    if (aa.getFlightCourse() != null && aa.getFlightCourse().length() > 0
                                            && aa.getFlightCourse().indexOf("-") != -1) {
                                        zrate.setDepartureport(aa.getFlightCourse().split("-")[0]);//锟斤拷锟斤拷锟斤拷
                                        if (zrate.getDepartureport() != null && zrate.getDepartureport().equals("999")) {
                                            if (aa.getDepartureExclude() != null) {
                                                zrate.setDepartureexclude(aa.getDepartureExclude());// 锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷
                                            }
                                        }
                                        zrate.setArrivalport(aa.getFlightCourse().split("-")[1]);//锟斤拷锟斤拷

                                        if (zrate.getArrivalport() != null && zrate.getArrivalport().equals("999")) {
                                            if (aa.getArrivalExclude() != null) {
                                                zrate.setArrivalexclude(aa.getArrivalExclude());// 锟斤拷锟斤不锟斤拷锟斤拷
                                            }
                                        }

                                    }
                                    if (aa.getCommision() > 0) {
                                        zrate.setRatevalue(aa.getCommision());
                                    }
                                    if (aa.getSeatClass() != null) {
                                        zrate.setCabincode(aa.getSeatClass());
                                    }
                                    if (aa.getFlightNoIncluding() != null) {
                                        zrate.setFlightnumber(aa.getFlightNoIncluding());// 锟斤拷锟矫的猴拷锟斤拷
                                    }
                                    if (aa.getFlightNoExclude() != null) {
                                        zrate.setWeeknum(aa.getFlightNoExclude());//锟斤拷锟斤拷锟矫的猴拷锟斤拷
                                    }
                                    if (aa.getFlightCycle() != null) {
                                        zrate.setSchedule(aa.getFlightCycle());//锟斤拷锟斤拷锟斤拷锟斤拷  1234567
                                    }
                                    //aa.getNeedSwitchPNR(); True锟斤拷锟斤拷锟斤拷应锟斤拷锟斤拷要锟斤拷锟斤拷约锟斤拷锟絇NR锟斤拷票 False锟斤拷锟斤拷锟斤拷锟斤拷锟�

                                    //System.out.println("shijian=="+new Timestamp(aa.getStartDate().getTime().getTime()));
                                    //zrate.setBegindate(new Timestamp(aa.getStartDate().getTime().getTime()));
                                    if (aa.getStartDate() != null && aa.getStartDate().getTime() != null) {
                                        zrate.setBegindate(new Timestamp(aa.getStartDate().getTime().getTime()));
                                    }
                                    if (aa.getPrintTicketStartDate() != null
                                            && aa.getPrintTicketStartDate().getTime() != null) {
                                        zrate.setIssuedstartdate(new Timestamp(aa.getPrintTicketStartDate().getTime()
                                                .getTime()));
                                    }
                                    if (aa.getExpiredDate() != null && aa.getExpiredDate().getTime() != null) {
                                        zrate.setIssuedendate(new Timestamp(aa.getExpiredDate().getTime().getTime())); //锟斤拷票锟斤拷锟斤拷时锟斤拷
                                    }
                                    if (aa.getPrintTicketExpiredDate() != null
                                            && aa.getPrintTicketExpiredDate().getTime() != null) {
                                        zrate.setEnddate(new Timestamp(aa.getPrintTicketExpiredDate().getTime()
                                                .getTime())); //锟斤拷锟斤拷锟斤拷效锟节斤拷锟斤拷时锟斤拷

                                    }
                                    //new Timestamp( new SimpleDateFormat("yyyy-MM-dd").p);

                                    //zrate.setIssuedendate(new Timestamp( new SimpleDateFormat("yyyy-MM-dd").parse(aa.getExpiredDate()+"").getTime()));  //锟斤拷票锟斤拷锟斤拷时锟斤拷

                                    //zrate.setEnddate(new Timestamp( new SimpleDateFormat("yyyy-MM-dd").parse(aa.getPrintTicketExpiredDate()+"").getTime()));  //锟斤拷锟斤拷锟斤拷效锟节斤拷锟斤拷时锟斤拷

                                    if (aa.getPolicyType() != null && aa.getPolicyType().equals("B2B")) {
                                        zrate.setTickettype(2);

                                    }
                                    else {

                                        zrate.setTickettype(1);
                                    }

                                    if (aa.getRouteType() != null && aa.getRouteType().equals("OW")) {//锟斤拷锟斤拷  

                                        zrate.setVoyagetype("1");//1锟斤拷锟斤拷,2:锟斤拷3:锟斤拷锟教伙拷锟斤拷

                                    }
                                    else {
                                        zrate.setVoyagetype("2");//1锟斤拷锟斤拷,2:锟斤拷3:锟斤拷锟教伙拷锟斤拷

                                    }

                                    if (aa.getWorkTime() != null && aa.getWorkTime().length() > 0
                                            && aa.getWorkTime().indexOf("-") != -1) {
                                        String worktime = aa.getWorkTime().split("-")[0];
                                        zrate.setWorktime(worktime);
                                        zrate.setAfterworktime(aa.getWorkTime().split("-")[1]);

                                    }
                                    if (aa.getBusinessUnitType() != null) {
                                        //System.out.println("锟斤拷锟斤拷=="+aa.getBusinessUnitType());
                                        if (aa.getBusinessUnitType().equals("0")) {//=0 锟斤拷通锟斤拷锟斤拷 =1 锟斤拷锟斤拷锟斤拷锟斤拷

                                            zrate.setGeneral(1l);
                                            zrate.setZtype("1");
                                        }
                                        else {

                                            zrate.setGeneral(2l);//1,锟斤拷通   2锟竭凤拷
                                            zrate.setZtype("2");
                                        }

                                    }
                                    else {
                                        //System.out.println("锟斤拷锟斤拷==null");
                                        zrate.setGeneral(1l);
                                        zrate.setZtype("1");
                                    }

                                    /*if(aa.getModifyFlag()>0){
                                    	if(aa.getModifyFlag()==1||aa.getModifyFlag()==2){
                                    		
                                    		zrate.setIsenable(1);
                                    	}else{
                                    		
                                    		zrate.setIsenable(0);
                                    	}
                                    	
                                    }*/

                                    zrate.setUsertype("1");
                                    if (aa.getAgencyEfficiency() != null) {
                                        zrate.setSpeed(aa.getAgencyEfficiency());
                                    }
                                    if (aa.getComment() != null) {
                                        zrate.setRemark(aa.getComment());

                                    }

                                    //if(listz.size()>0){
                                    //Server.getInstance().getAirService().updateZrateIgnoreNull(zrate);
                                    //System.out.println("51book全取update=="+zrate.getRatevalue());
                                    //System.out.println("update=="+zrate.getRatevalue());
                                    //}else{

                                    try {
                                        Server.getInstance().getAirService().createZrate(zrate);
                                    }
                                    catch (RuntimeException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }
                                    System.out.println("51book全取add==" + zrate.getRatevalue());
                                    //	}

                                }
                            }

                        }

                    }

                    System.out
                            .println("*********************锟斤拷取锟斤拷伞锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟�");

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Calendar calendar = Calendar.getInstance();
                    String startDate = sdf.format(calendar.getTime());

                    String lasteZrateID = "1";//锟斤拷锟斤拷锟斤拷锟铰糏D
                    String lastUpdateTime = startDate + " 00:00:00";//锟斤拷锟轿革拷锟斤拷时锟斤拷

                    String updsql = " UPDATE T_B2BSEQUENCE SET C_VALUE='" + lastUpdateTime
                            + "' WHERE C_NAME='51ZRATETIME'";
                    Server.getInstance().getSystemService().findMapResultBySql(updsql, null);

                    String updsql2 = " UPDATE T_B2BSEQUENCE SET C_VALUE='" + lasteZrateID
                            + "' WHERE C_NAME='51ZRATEID'";
                    Server.getInstance().getSystemService().findMapResultBySql(updsql2, null);

                    //锟斤拷锟斤拷时锟戒开始,锟斤拷锟矫筹拷锟斤拷锟节革拷锟斤拷锟斤拷
                    String updsq = " UPDATE T_B2BSEQUENCE SET C_VALUE='0' WHERE C_NAME='51ISSTAR'";
                    Server.getInstance().getSystemService().findMapResultBySql(updsq, null);
                    //锟斤拷锟矫斤拷锟斤拷时锟斤拷

                    writeLog.write("51bookZrate", "全取锟斤拷锟�,lastUpdateTime:" + lastUpdateTime + ",lasteZrateID:"
                            + lasteZrateID);

                }
                else {

                    System.out
                            .println("*********************51book锟接口憋拷锟斤拷锟矫★拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷");

                }

            }
            catch (Exception e) {
                System.out.println("51book锟斤拷值锟届常锟斤拷");
                e.printStackTrace();

                WirateErrer(2, e);
                GetZrate(pageindex);

                /*	writeLog.write("51bookZrateQuanQuERRER",e.toString());
                	
                	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd") ;
                	Calendar calendar = Calendar.getInstance();
                	String startDate = sdf.format(calendar.getTime());
                	
                	
                	String lasteZrateID="1";//锟斤拷锟斤拷锟斤拷锟铰糏D
                	String lastUpdateTime=startDate+" 00:00:00";//锟斤拷锟轿革拷锟斤拷时锟斤拷
                	
                	String updsql = " UPDATE T_B2BSEQUENCE SET C_VALUE='"
                		+ lastUpdateTime + "' WHERE C_NAME='51ZRATETIME'";
                	Server.getInstance().getSystemService().findMapResultBySql(
                		updsql, null);
                	
                	String updsql2 = " UPDATE T_B2BSEQUENCE SET C_VALUE='"
                		+ lasteZrateID + "' WHERE C_NAME='51ZRATEID'";
                	Server.getInstance().getSystemService().findMapResultBySql(
                		updsql2, null);
                	
                	//锟斤拷锟斤拷时锟戒开始,锟斤拷锟矫筹拷锟斤拷锟节革拷锟斤拷锟斤拷
                	String updsq = " UPDATE T_B2BSEQUENCE SET C_VALUE='0' WHERE C_NAME='51ISSTAR'";
                		Server.getInstance().getSystemService().findMapResultBySql(updsq, null);
                	//锟斤拷锟矫斤拷锟斤拷时锟斤拷
                */
            }

        }
        catch (Exception e) {

            e.printStackTrace();

            WirateErrer(2, e);
            GetZrate(pageindex);
        }
    }

    public void WirateErrer(int type, Exception e) {//1锟斤拷  2锟届常
        if (type == 1) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Calendar calendar = Calendar.getInstance();
            String startDate = sdf.format(calendar.getTime());

            String lasteZrateID = "1";//锟斤拷锟斤拷锟斤拷锟铰糏D
            String lastUpdateTime = startDate + " 00:00:00";//锟斤拷锟轿革拷锟斤拷时锟斤拷

            String updsql = " UPDATE T_B2BSEQUENCE SET C_VALUE='" + lastUpdateTime + "' WHERE C_NAME='51ZRATETIME'";
            Server.getInstance().getSystemService().findMapResultBySql(updsql, null);

            String updsql2 = " UPDATE T_B2BSEQUENCE SET C_VALUE='" + lasteZrateID + "' WHERE C_NAME='51ZRATEID'";
            Server.getInstance().getSystemService().findMapResultBySql(updsql2, null);

            //锟斤拷锟斤拷时锟戒开始,锟斤拷锟矫筹拷锟斤拷锟节革拷锟斤拷锟斤拷
            String updsq = " UPDATE T_B2BSEQUENCE SET C_VALUE='0' WHERE C_NAME='51ISSTAR'";
            Server.getInstance().getSystemService().findMapResultBySql(updsq, null);
            //锟斤拷锟矫斤拷锟斤拷时锟斤拷
        }
        if (type == 2) {
            System.out.println("^^^^^^^51book锟届常^^^^^^^^^");

            writeLog.write("51bookZrateQuanQuERRER", e.getMessage());
            /*SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd") ;
            Calendar calendar = Calendar.getInstance();
            String startDate = sdf.format(calendar.getTime());
            
            
            String lasteZrateID="1";//锟斤拷锟斤拷锟斤拷锟铰糏D
            String lastUpdateTime=startDate+" 00:00:00";//锟斤拷锟轿革拷锟斤拷时锟斤拷
            
            String updsql = " UPDATE T_B2BSEQUENCE SET C_VALUE='"
            	+ lastUpdateTime + "' WHERE C_NAME='51ZRATETIME'";
            Server.getInstance().getSystemService().findMapResultBySql(
            	updsql, null);
            
            String updsql2 = " UPDATE T_B2BSEQUENCE SET C_VALUE='"
            	+ lasteZrateID + "' WHERE C_NAME='51ZRATEID'";
            Server.getInstance().getSystemService().findMapResultBySql(
            	updsql2, null);
            
            //锟斤拷锟斤拷时锟戒开始,锟斤拷锟矫筹拷锟斤拷锟节革拷锟斤拷锟斤拷
            String updsq = " UPDATE T_B2BSEQUENCE SET C_VALUE='0' WHERE C_NAME='51ISSTAR'";
            	Server.getInstance().getSystemService().findMapResultBySql(updsq, null);
            //锟斤拷锟矫斤拷锟斤拷时锟斤拷
            */}
    }

}
