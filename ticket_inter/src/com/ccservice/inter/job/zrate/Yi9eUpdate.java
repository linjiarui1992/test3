package com.ccservice.inter.job.zrate;

import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.b2b2c.base.eaccount.Eaccount;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.test.HttpClient;
import com.speed.iesales.service.Rate;
import com.speed.iesales.service.RateDel;
import com.speed.iesales.service.SyncRateDelRequest;
import com.speed.iesales.service.SyncRateDelResponse;
import com.speed.iesales.service.SyncRateRequest;
import com.speed.iesales.service.SyncRateResponse;
import com.speed.iesales.service.impl.SyncRateServiceNewImplService;

public class Yi9eUpdate implements Job {
    String PageSize = "300";

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        Eaccount eaccount = Server.getInstance().getSystemService().findEaccount(15);
        deleteZrate(eaccount);
        updateZrate(eaccount);
        System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
    }

    public void updateZrate(Eaccount eaccount) {
        String Username = eaccount.getUsername();
        String Saftcode = eaccount.getPwd();
        //数据库中下标为0的为删除接口用下标为1的为更新接口用
        String StrategyId = eaccount.getLastzid().split("[|]")[1];
        String UpdateTime = eaccount.getUpdatetime().split("[|]")[1];
        System.out.println();
        System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        System.out.println(StrategyId + ":" + UpdateTime);
        SyncRateServiceNewImplService syncRateServiceNewImplService = new SyncRateServiceNewImplService();
        SyncRateRequest syncRateRequest = new SyncRateRequest();
        String Aircomp2C = "";
        String Appcode = "19";
        String MinDiscount = "2";
        String PsgType = "3";
        String RateType = "3";
        String Sign = "GetSyncRate" + Aircomp2C + RateType + PsgType + StrategyId + UpdateTime + MinDiscount + PageSize
                + Username + Appcode + Saftcode;
        //		WriteLog.write("19EUpdateZrate", "0.1:" + Sign);
        try {
            Sign = HttpClient.MD5(Sign);
            //			WriteLog.write("19EUpdateZrate", "0.2:" + Sign);
        }
        catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        syncRateRequest.setAircomp2C(Aircomp2C);
        syncRateRequest.setAppcode(Appcode);
        syncRateRequest.setMinDiscount(MinDiscount);
        syncRateRequest.setPageSize(PageSize);
        syncRateRequest.setPsgType(PsgType);
        syncRateRequest.setRateType(RateType);
        syncRateRequest.setSign(Sign);
        syncRateRequest.setStrategyId(StrategyId);
        syncRateRequest.setUpdateTime(UpdateTime);
        syncRateRequest.setUsername(Username);
        SyncRateResponse syncRateResponse = syncRateServiceNewImplService.getSyncRateServiceNewImplPort().getSyncRate(
                syncRateRequest);
        if ("S".equals(syncRateResponse.getCode())) {
            String lastStrategyId = syncRateResponse.getLastStrategyId();
            int pageCount = syncRateResponse.getPageCount();
            String updateTime = syncRateResponse.getUpdateTime();
            List<Rate> rateList = syncRateResponse.getRateList();
            System.out.println("rateList.size:" + rateList.size());
            WriteLog.write("19EUpdateZrate", "S:" + lastStrategyId + ":" + updateTime + ":size:" + rateList.size()
                    + ":" + pageCount);
            createZrateByList(rateList);
            eaccount.setLastzid(eaccount.getLastzid().split("[|]")[0] + "|" + lastStrategyId);
            eaccount.setUpdatetime(eaccount.getUpdatetime().split("[|]")[0] + "|" + updateTime);
            Server.getInstance().getSystemService().updateEaccount(eaccount);
        }
        else {
            System.out.println(syncRateResponse.getMessage());
            WriteLog.write("19EUpdateZrate", "ERROR:" + syncRateResponse.getMessage());
        }
    }

    private static void createZrateByList(List<Rate> rateList) {
        List<Zrate> addZrates = new ArrayList<Zrate>();
        for (int i = 0; i < rateList.size(); i++) {
            Rate rate = rateList.get(i);
            Zrate zrate = new Zrate();
            zrate.setModifyuser("19E");
            zrate.setCreateuser("19E");
            zrate.setUsertype("1");
            zrate.setAgentid(13l);
            zrate.setIsenable(1);
            zrate.setZtype("1");
            zrate.setType(1);
            zrate.setCreatetime(new Timestamp(System.currentTimeMillis()));
            zrate.setModifytime(new Timestamp(System.currentTimeMillis()));

            zrate.setOutid(rate.getStrategyId() + "");
            zrate.setDepartureport(rate.getFromport3C());
            zrate.setArrivalport(rate.getToport3C());
            zrate.setAircompanycode(rate.getAircomp2C());
            if (!"***".equals(rate.getFlightnoFit())) {
                zrate.setFlightnumber(rate.getFlightnoFit());
            }
            zrate.setWeeknum(rate.getFlightnoNotFit());
            zrate.setVoyagetype(rate.getRoutetype() + "");
            zrate.setRatevalue(rate.getBackrate().floatValue());
            zrate.setCabincode(rate.getFlightclass());
            zrate.setBegindate(new Timestamp(rate.getSdate().toGregorianCalendar().getTimeInMillis()));
            zrate.setEnddate(new Timestamp(rate.getEdate().toGregorianCalendar().getTimeInMillis()));
            zrate.setWorktime(rate.getWorktime().split("[-]")[0]);
            zrate.setAfterworktime(rate.getWorktime().split("[-]")[1]);
            zrate.setOnetofiveworktime(rate.getWorktime().split("[-]")[0]);
            zrate.setOnetofiveaftertime(rate.getWorktime().split("[-]")[1]);
            zrate.setGeneral(Long.parseLong(rate.getRatetype()));
            int tickettype = rate.getStrategyType().intValue();
            if (tickettype == 0)
                tickettype = 2;
            else if (tickettype == 1)
                tickettype = 1;
            zrate.setTickettype(tickettype);
            zrate.setRemark(rate.getChngretmemo());
            zrate.setOnetofivewastetime(rate.getVoidtime());
            zrate.setWeekendwastetime(rate.getVoidtime());
            zrate.setSpeed(rate.getThespeed().equals("0") ? "10" : rate.getThespeed());
            addZrates.add(zrate);
        }
        if (addZrates.size() > 0) {
            Server.getInstance().getAirService().createZrateList(addZrates);
        }
    }

    public void deleteZrate(Eaccount eaccount) {
        String Username = eaccount.getUsername();
        String Saftcode = eaccount.getPwd();
        //数据库中下标为0的为删除接口用下标为1的为更新接口用
        String StrategyId = eaccount.getLastzid();
        StrategyId = StrategyId.split("[|]")[0];
        String UpdateTime = eaccount.getUpdatetime();
        UpdateTime = UpdateTime.split("[|]")[0];
        SyncRateServiceNewImplService syncRateServiceNewImplService = new SyncRateServiceNewImplService();
        SyncRateDelRequest syncRateDelRequest = new SyncRateDelRequest();
        syncRateDelRequest.setUpdateTime(UpdateTime);
        syncRateDelRequest.setDeleteStrategyId(StrategyId);
        syncRateDelRequest.setPageSize(PageSize);
        syncRateDelRequest.setUsername(Username);
        String Appcode = "19";
        syncRateDelRequest.setAppcode(Appcode);
        String Sign = "GetSyncRateDel" + UpdateTime + StrategyId + PageSize + Username + Appcode + Saftcode;
        //		WriteLog.write("19EDeleteZrate", "0.1:" + Sign);
        try {
            Sign = HttpClient.MD5(Sign);
            //			WriteLog.write("19EDeleteZrate", "0.2:" + Sign);
        }
        catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        syncRateDelRequest.setSign(Sign);
        SyncRateDelResponse syncRateDelResponse = syncRateServiceNewImplService.getSyncRateServiceNewImplPort()
                .getSyncRateDel(syncRateDelRequest);
        if ("S".equals(syncRateDelResponse.getCode())) {
            String lastStrategyId = syncRateDelResponse.getDeleteStrategyId();
            String updateTime = syncRateDelResponse.getUpdateTime();
            List<RateDel> rateList = syncRateDelResponse.getRateList();
            WriteLog.write("19EDeleteZrate", "S:" + lastStrategyId + ":" + updateTime + ":size:" + rateList.size());
            String deleteWhere = "DELETE FROM T_ZRATE WHERE 1=2 ";
            for (int i = 0; i < rateList.size(); i++) {
                deleteWhere += " OR C_OUTID ='" + rateList.get(i).getStrategyId() + "'";
            }
            WriteLog.write("19EDeleteZrate", "D:" + deleteWhere);
            int delete = Server.getInstance().getAirService().excuteZrateBySql(deleteWhere);
            WriteLog.write("19EDeleteZrate", "D:" + delete);
            eaccount.setLastzid(lastStrategyId + "|" + eaccount.getLastzid().split("[|]")[1]);
            eaccount.setUpdatetime(updateTime + "|" + eaccount.getUpdatetime().split("[|]")[1]);
            Server.getInstance().getSystemService().updateEaccount(eaccount);
        }
        else {
            System.out.println(syncRateDelResponse.getMessage());
            WriteLog.write("19EDeleteZrate", "ERROR:" + syncRateDelResponse.getMessage());
        }
    }

}
