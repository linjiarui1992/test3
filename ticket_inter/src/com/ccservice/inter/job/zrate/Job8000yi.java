package com.ccservice.inter.job.zrate;

import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net._8000yi.websvr.newply.webinterface.plyintefaceservice_asmx.PlyIntefaceServiceStub;

import org.apache.axiom.om.OMElement;
import org.apache.axis2.AxisFault;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

import com.ccservice.b2b2c.base.eaccount.Eaccount;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.UtilMethod;
import com.ccservice.b2b2c.policy.ben.BaQianYiBook;
import com.ccservice.inter.job.DBUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;

/**
 * 此类为全取8000翼的政策的定时任务
 * @author chendong	update:2013-4-20
 *
 */
public class Job8000yi implements Job {
    BaQianYiBook baQianYiBook = new BaQianYiBook();

    @Override
    public void execute(JobExecutionContext context) {
        deleteZrate();
        updateZrateNew();
    }

    ////获取已经删除政策，此接口在调用 AlreadyDisposalSynchronizationNew_All
    //方法之前调用（第一次调用的时候不要调用，从第二次调用更新政策方法开始调
    //用；注：删除政策返回数据为倒序，按时间倒序
    private static void deleteZrate() {
        try {
            Eaccount eaccount = Server.getInstance().getSystemService().findEaccount(3);
            String datetime = eaccount.getNourl();
            PlyIntefaceServiceStub stub = new PlyIntefaceServiceStub();
            PlyIntefaceServiceStub.GetDeletePolicy getDeletePolicy = new PlyIntefaceServiceStub.GetDeletePolicy();
            PlyIntefaceServiceStub.AlreadyDisposalSynchronizationNew_All alreadyDisposalSynchronizationNew_All = new PlyIntefaceServiceStub.AlreadyDisposalSynchronizationNew_All();
            String deleteWhere = "DELETE FROM T_ZRATE WHERE 1=2 ";
            getDeletePolicy.setName("bjhthy");
            getDeletePolicy.setPwd("bjhthy");
            getDeletePolicy.setDatetime(datetime);
            PlyIntefaceServiceStub.GetDeletePolicyResponse response = stub.getDeletePolicy(getDeletePolicy);
            OMElement e = response.getGetDeletePolicyResult().getExtraElement();
            Iterator<OMElement> it = e.getChildElements();
            int i = 0;
            while (it.hasNext()) {
                OMElement o = it.next();
                Iterator<OMElement> oit = o.getChildElements();
                while (oit.hasNext()) {
                    OMElement ot = oit.next(); // 单条政策
                    Iterator<OMElement> rit = ot.getChildElements();
                    while (rit.hasNext()) {
                        OMElement rt = rit.next();
                        String value = rt.getText();
                        String name = rt.getLocalName();
                        if (name.equals("plyid")) {
                            deleteWhere += " OR C_OUTID ='" + value + "'";
                        }
                        if (name.equals("Time") && i == 0) {
                            datetime = value;
                            i++;
                        }
                    }
                }
            }
            WriteLog.write("8000YI_UPDATE_DEL", datetime + "=" + eaccount.getNourl());
            eaccount.setNourl(datetime);
            Server.getInstance().getSystemService().updateEaccount(eaccount);
            int delete = Server.getInstance().getAirService().excuteZrateBySql(deleteWhere);
            WriteLog.write("8000YI_DEL", delete + ":" + deleteWhere);
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e1) {
            e1.printStackTrace();
        }
    }

    public void updateZrateNew() {
        String datetime = "2011-1-1 00:00:00";
        String lasttime = "";
        try {
            List<Zrate> addZrates = new ArrayList<Zrate>();
            String sql = "SELECT C_VALUE AS PICKTIME FROM T_B2BSEQUENCE WHERE C_NAME='8000YI_ZRATETIME' AND C_VALUE IS NOT NULL";
            List clist = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
            if (clist.size() > 0) {
                Map m = (Map) clist.get(0);
                datetime = m.get("PICKTIME").toString();
            }
            String sqlsql = "SELECT C_VALUE AS PICKTIME FROM T_B2BSEQUENCE WHERE C_NAME='8000YI_ZRATE' AND C_VALUE = 1";
            List clist1 = Server.getInstance().getSystemService().findMapResultBySql(sqlsql, null);
            if (clist1.size() > 0) {
                System.out.println("8000yi正在进行上次全取:" + Server.getInstance().getJob8000yicount());
                Server.getInstance().setJob8000yicount(Server.getInstance().getJob8000yicount() + 1);
                if (Server.getInstance().getJob8000yicount() >= 3) {
                    Server.getInstance().getSystemService()
                            .excuteSysconfigBySql("UPDATE T_B2BSEQUENCE SET C_VALUE=0 WHERE C_NAME='8000YI_ZRATE'");
                }
                return;
            }
            System.out.println(new SimpleDateFormat("HH:mm:ss").format(new Date()) + "***8000翼全取时间：" + datetime);
            lasttime = datetime;
            PlyIntefaceServiceStub stub = new PlyIntefaceServiceStub();
            PlyIntefaceServiceStub.AlreadyDisposalSynchronizationNew_All alreadyDisposalSynchronizationNew_All = new PlyIntefaceServiceStub.AlreadyDisposalSynchronizationNew_All();
            alreadyDisposalSynchronizationNew_All.setDatetime(datetime);
            alreadyDisposalSynchronizationNew_All.setName(baQianYiBook.getUsername8000());
            alreadyDisposalSynchronizationNew_All.setPwd(baQianYiBook.getPassword8000());
            PlyIntefaceServiceStub.AlreadyDisposalSynchronizationNew_AllResponse response = stub
                    .alreadyDisposalSynchronizationNew_All(alreadyDisposalSynchronizationNew_All);
            if (response.getAlreadyDisposalSynchronizationNew_AllResult() == null) {
                String updsqlsql = " UPDATE T_B2BSEQUENCE SET C_VALUE=0 WHERE C_NAME='8000YI_ZRATE'";
                Server.getInstance().getSystemService().findMapResultBySql(updsqlsql, null);
                return;
            }
            String updsqlsql1 = " UPDATE T_B2BSEQUENCE SET C_VALUE=1 WHERE C_NAME='8000YI_ZRATE'";
            Server.getInstance().getSystemService().findMapResultBySql(updsqlsql1, null);
            OMElement e = response.getAlreadyDisposalSynchronizationNew_AllResult().getExtraElement();
            Iterator<OMElement> it = e.getChildElements();
            DateFormat fromat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+08:00");
            int tempint = 0;
            int updateint = 0;
            int delete = 0;
            String deleteWhere = "DELETE FROM T_ZRATE WHERE 1=2 ";
            while (it.hasNext()) {
                OMElement o = it.next();
                Iterator<OMElement> oit = o.getChildElements();
                while (oit.hasNext()) {
                    OMElement ot = oit.next(); // 单条政策
                    Iterator<OMElement> rit = ot.getChildElements();
                    Zrate zrate = new Zrate();
                    zrate.setModifyuser("8000YI");
                    zrate.setCreateuser("8000YI");
                    zrate.setUsertype("1");
                    zrate.setAgentid(3l);
                    zrate.setIsenable(1);
                    zrate.setGeneral(1L);
                    zrate.setZtype("1");
                    try {
                        while (rit.hasNext()) {
                            OMElement rt = rit.next();
                            String value = rt.getText();
                            String name = rt.getLocalName();
                            if ("ErrInfo".equals(name)) {
                                WriteLog.write("8000YIZRATE",
                                        baQianYiBook.getUsername8000() + ":" + baQianYiBook.getPassword8000());
                                WriteLog.write("8000YIZRATE", name + ":" + value);
                                System.out.println(name + ":" + value);
                                break;
                            }
                            if (!value.isEmpty()) {
                                if (name.equals("A1")) {
                                    zrate.setOutid(value);
                                }
                                if (name.equals("A2")) {
                                    zrate.setDepartureport(value);
                                }
                                if (name.equals("A3")) {
                                    zrate.setArrivalport(value);
                                }
                                if (name.equals("A4")) {
                                    zrate.setAircompanycode(value);
                                }
                                if (name.equals("A5")) {
                                    zrate.setFlightnumber(value);
                                    zrate.setType(1);
                                }
                                if (name.equals("A6")) {
                                    zrate.setWeeknum(value);
                                    zrate.setType(2);
                                }
                                if (name.equals("A7")) {// 返回1表示单程，返回2表示往返，返回3表示单程往返
                                    zrate.setVoyagetype(value);

                                }
                                if (name.equals("A8")) {
                                    zrate.setRatevalue(Float.parseFloat(value));
                                }
                                if (name.equals("A9")) {
                                    zrate.setCabincode(value);
                                }
                                if (name.equals("A10")) {
                                    zrate.setBegindate(new Timestamp(fromat.parse(value).getTime()));
                                }
                                if (name.equals("A11")) {
                                    zrate.setEnddate(new Timestamp(fromat.parse(value).getTime()));
                                }
                                if (name.equals("A12")) {
                                    try {
                                        String[] times = value.replace("|", ",").split(",");
                                        zrate.setWorktime(times[0]);
                                        zrate.setAfterworktime(times[1]);
                                        zrate.setOnetofiveworktime(times[0]);
                                        zrate.setOnetofiveaftertime(times[1]);
                                    }
                                    catch (Exception ex) {
                                        System.out.println("A12");
                                        ex.printStackTrace();
                                    }
                                }
                                if (name.equals("A13")) {
                                    try {
                                        String[] times = value.replace("|", ",").split(",");
                                        zrate.setOnetofiveworktime(times[0]);
                                        zrate.setOnetofiveaftertime(times[1]);
                                    }
                                    catch (Exception ex) {
                                        System.out.println("A13");
                                        ex.printStackTrace();
                                    }
                                }
                                if (name.equals("A14")) {
                                    zrate.setGeneral(1l);
                                    if ("0".equals(value)) {
                                        zrate.setZtype("1");
                                    }
                                    try {
                                        if (value.length() > 1) {

                                        }
                                        else {
                                            zrate.setIstype(0L);
                                        }
                                    }
                                    catch (Exception exx) {
                                        exx.printStackTrace();
                                    }
                                }
                                if (name.equals("A15")) {

                                }
                                if (name.equals("A16")) {
                                    if ("BSP".equals(value)) {
                                        zrate.setTickettype(1);
                                    }
                                    else {
                                        zrate.setTickettype(2);
                                    }
                                }
                                if (name.equals("A17")) {
                                    zrate.setRemark(value.trim() + "|" + zrate.getRemark());
                                }
                                if (name.equals("A18")) {

                                }
                                if (name.equals("A19")) {
                                    zrate.setOnetofivewastetime(value.replace('|', '-'));
                                }
                                if (name.equals("A20")) {
                                    zrate.setWeekendwastetime(value.replace('|', '-'));
                                }
                                if (name.equals("A21")) {
                                    zrate.setSchedule(value);
                                }
                                if (name.equals("A22")) {
                                    if ("可用".equals(value)) {
                                        zrate.setIsenable(1);
                                    }
                                    else {
                                        zrate.setIsenable(0);
                                    }
                                }
                                if (name.equals("A23")) {
                                    lasttime = value;
                                }
                                if (name.equals("A24")) {
                                    zrate.setAgentcode(value);
                                }
                            }
                        }

                        zrate.setCreatetime(new Timestamp(System.currentTimeMillis()));
                        zrate.setModifytime(new Timestamp(System.currentTimeMillis()));
                        deleteWhere += " OR C_OUTID ='" + zrate.getOutid() + "'";
                        if (zrate.getIsenable().equals(1)) {
                            if (zrate.getRatevalue() != null && zrate.getRatevalue() > 2.5) {
                                addZrates.add(zrate);
                            }
                        }
                    }
                    catch (Exception e2) {
                        System.out.println(e2.getMessage());
                        e2.printStackTrace();
                    }
                }
            }
            WriteLog.write("8000YI_UPDATE_DEL", delete + ":" + deleteWhere);
            delete = Server.getInstance().getAirService().excuteZrateBySql(deleteWhere);
            if (addZrates.size() > 0) {
                tempint = Server.getInstance().getAirService().createZrateList(addZrates);
            }
            System.out.println(new SimpleDateFormat("HH:mm:ss").format(new Date()) + "******" + datetime + "提取完成××");
            Server.getInstance().setJob8000yicount(0);
            System.out.println("[" + lasttime + "]");
            if (lasttime.length() > 0) {
                String updsql = " UPDATE T_B2BSEQUENCE SET C_VALUE='"
                        + lasttime
                        + "' WHERE C_NAME='8000YI_ZRATETIME';UPDATE T_B2BSEQUENCE SET C_VALUE=0 WHERE C_NAME='8000YI_ZRATE'";
                Server.getInstance().getSystemService().findMapResultBySql(updsql, null);
            }
            else {

                String updsqlsql = "UPDATE T_B2BSEQUENCE SET C_VALUE=0 WHERE C_NAME='8000YI_ZRATE'";
                Server.getInstance().getSystemService().findMapResultBySql(updsqlsql, null);
            }
        }
        catch (AxisFault e) {
            try {
                String[] lasttimes = lasttime.split("[.]");
                int tempint = Integer.parseInt(lasttimes[1]) + 3;
                lasttime = lasttimes[0] + "." + tempint;
                String updsql1 = " UPDATE T_B2BSEQUENCE SET C_VALUE='"
                        + lasttime
                        + "' WHERE C_NAME='8000YI_ZRATETIME';UPDATE T_B2BSEQUENCE SET C_VALUE=0 WHERE C_NAME='8000YI_ZRATE'";
                Server.getInstance().getSystemService().findMapResultBySql(updsql1, null);
            }
            catch (Exception e2) {
                e2.printStackTrace();
                // TODO: handle exception
            }
        }
        catch (RemoteException e) {
            try {
                String[] lasttimes = lasttime.split(".");
                int tempint = Integer.parseInt(lasttimes[1]) + 3;
                lasttime = lasttimes[0] + "." + tempint;
                String updsql1 = " UPDATE T_B2BSEQUENCE SET C_VALUE='"
                        + lasttime
                        + "' WHERE C_NAME='8000YI_ZRATETIME';UPDATE T_B2BSEQUENCE SET C_VALUE=0 WHERE C_NAME='8000YI_ZRATE'";
                Server.getInstance().getSystemService().findMapResultBySql(updsql1, null);
            }
            catch (Exception e2) {
                e2.printStackTrace();
            }
            e.printStackTrace();
        }
    }

    /**
     * 8000yi更新特殊政策
     */
    public static void updateLastUpdateSpecial() {
        List<Zrate> addZrates = new ArrayList<Zrate>();
        try {
            PlyIntefaceServiceStub stub = new PlyIntefaceServiceStub();
            PlyIntefaceServiceStub.LastUpdateSpecial lastUpdateSpecial = new PlyIntefaceServiceStub.LastUpdateSpecial();
            lastUpdateSpecial.setDatetime("2013-08-05 17:07:00");
            lastUpdateSpecial.setName("bjhthy");
            lastUpdateSpecial.setPwd("bjhthy");
            PlyIntefaceServiceStub.LastUpdateSpecialResponse response = stub.lastUpdateSpecial(lastUpdateSpecial);
            OMElement e = response.getLastUpdateSpecialResult().getExtraElement();
            System.out.println(e.toString());
            Iterator<OMElement> it = e.getChildElements();
            DateFormat fromat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+08:00");
            String lasttime = "";
            while (it.hasNext()) {
                OMElement o = it.next();
                Iterator<OMElement> oit = o.getChildElements();
                while (oit.hasNext()) {
                    OMElement ot = oit.next(); // 单条政策
                    Iterator<OMElement> rit = ot.getChildElements();
                    Zrate zrate = new Zrate();
                    zrate.setModifyuser("8000YI");
                    zrate.setCreateuser("8000YI");
                    zrate.setUsertype("1");
                    zrate.setAgentid(3l);
                    zrate.setIsenable(1);
                    zrate.setGeneral(2L);
                    zrate.setZtype("2");
                    try {
                        while (rit.hasNext()) {
                            OMElement rt = rit.next();
                            String value = rt.getText();
                            String name = rt.getLocalName();
                            if (!value.isEmpty()) {
                                if (name.equals("A1")) {
                                    zrate.setOutid(value);
                                }
                                if (name.equals("A2")) {
                                    zrate.setDepartureport(value);
                                }
                                if (name.equals("A3")) {
                                    zrate.setArrivalport(value);
                                }
                                if (name.equals("A4")) {
                                    zrate.setAircompanycode(value);
                                }
                                if (name.equals("A5")) {
                                    zrate.setFlightnumber(value);
                                    zrate.setType(1);
                                }
                                if (name.equals("A6")) {
                                    zrate.setWeeknum(value);
                                    zrate.setType(2);
                                }
                                if (name.equals("A7")) {// 返回1表示单程，返回2表示往返，返回3表示单程往返
                                    zrate.setVoyagetype(value);

                                }
                                if (name.equals("A8")) {
                                    zrate.setRatevalue(Float.parseFloat(value));
                                }
                                if (name.equals("A9")) {
                                    zrate.setCabincode(value);
                                }
                                if (name.equals("A10")) {
                                    zrate.setBegindate(new Timestamp(fromat.parse(value).getTime()));
                                }
                                if (name.equals("A11")) {
                                    zrate.setEnddate(new Timestamp(fromat.parse(value).getTime()));
                                }
                                if (name.equals("A12")) {
                                    try {
                                        String[] times = value.replace("|", ",").split(",");
                                        zrate.setWorktime(times[0]);
                                        zrate.setAfterworktime(times[1]);
                                        zrate.setOnetofiveworktime(times[0]);
                                        zrate.setOnetofiveaftertime(times[1]);
                                    }
                                    catch (Exception ex) {
                                        ex.printStackTrace();
                                    }
                                }
                                if (name.equals("A13")) {
                                    try {
                                        String[] times = value.replace("|", ",").split(",");
                                        zrate.setOnetofiveworktime(times[0]);
                                        zrate.setOnetofiveaftertime(times[1]);
                                    }
                                    catch (Exception ex) {
                                        ex.printStackTrace();
                                    }
                                }
                                if (name.equals("A14")) {
                                    zrate.setGeneral(1l);
                                    if ("0".equals(value)) {
                                        zrate.setZtype("1");
                                    }
                                    try {
                                        if (value.length() > 1) {

                                        }
                                        else {
                                            zrate.setIstype(0L);
                                        }
                                    }
                                    catch (Exception exx) {
                                        exx.printStackTrace();
                                    }
                                }
                                if (name.equals("A15")) {

                                }
                                if (name.equals("A16")) {
                                    if ("BSP".equals(value)) {
                                        zrate.setTickettype(1);
                                    }
                                    else {
                                        zrate.setTickettype(2);
                                    }
                                }
                                if (name.equals("A17")) {
                                    zrate.setRemark(value.trim() + "|" + zrate.getRemark());
                                }
                                if (name.equals("A18")) {

                                }
                                if (name.equals("A19")) {
                                    zrate.setOnetofivewastetime(value.replace('|', '-'));
                                }
                                if (name.equals("A20")) {
                                    zrate.setWeekendwastetime(value.replace('|', '-'));
                                }
                                if (name.equals("A21")) {
                                    zrate.setSchedule(value);
                                }
                                if (name.equals("A22")) {
                                    if ("可用".equals(value)) {
                                        zrate.setIsenable(1);
                                    }
                                    else {
                                        zrate.setIsenable(0);
                                    }
                                }
                                if (name.equals("A23")) {
                                    lasttime = value;
                                }
                                if (name.equals("A24")) {
                                    zrate.setAgentcode(value);
                                }
                            }
                        }
                        zrate.setCreatetime(new Timestamp(System.currentTimeMillis()));
                        zrate.setModifytime(new Timestamp(System.currentTimeMillis()));
                        if (zrate.getIsenable().equals(1)) {
                            if (zrate.getRatevalue() > 0) {
                                addZrates.add(zrate);
                            }
                        }
                    }
                    catch (Exception e2) {
                        UtilMethod.writeEx("Job8000yi", e2);
                        e2.printStackTrace();
                    }
                }
            }
        }
        catch (AxisFault e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void updateZrateOld() {
        if (!baQianYiBook.getStaus().equals("0")) {
            try {
                try {
                    String datetime = "2011-1-1 00:00:00";
                    String sql = "SELECT C_VALUE AS PICKTIME FROM T_B2BSEQUENCE WHERE C_NAME='ZRATETIME' AND C_VALUE IS NOT NULL";
                    List clist = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                    if (clist.size() > 0) {
                        Map m = (Map) clist.get(0);
                        datetime = m.get("PICKTIME").toString();
                    }
                    String sqlsql = "SELECT C_VALUE AS PICKTIME FROM T_B2BSEQUENCE WHERE C_NAME='T_ZRATE' AND C_VALUE = 1";
                    List clist1 = Server.getInstance().getSystemService().findMapResultBySql(sqlsql, null);
                    if (clist1.size() > 0) {
                        System.out.println("8000yi正在进行上次全取:" + Server.getInstance().getJob8000yicount());
                        Server.getInstance().setJob8000yicount(Server.getInstance().getJob8000yicount() + 1);
                        if (Server.getInstance().getJob8000yicount() >= 60) {
                            Server.getInstance()
                                    .getSystemService()
                                    .excuteSysconfigBySql(
                                            "SELECT C_VALUE AS PICKTIME FROM T_B2BSEQUENCE WHERE C_NAME='T_ZRATE' AND C_VALUE = 0");
                        }
                        return;
                    }
                    System.out
                            .println(new SimpleDateFormat("HH:mm:ss").format(new Date()) + "***8000翼全取时间：" + datetime);
                    PlyIntefaceServiceStub stub = new PlyIntefaceServiceStub();
                    PlyIntefaceServiceStub.AlreadyDisposalSynchronizationNew service = new PlyIntefaceServiceStub.AlreadyDisposalSynchronizationNew();
                    service.setDatetime(datetime);
                    service.setName(baQianYiBook.getUsername8000());
                    service.setPwd(baQianYiBook.getPassword8000());
                    PlyIntefaceServiceStub.AlreadyDisposalSynchronizationNewResponse result = stub
                            .alreadyDisposalSynchronizationNew(service);

                    if (result.getAlreadyDisposalSynchronizationNewResult() == null) {
                        String updsqlsql = " UPDATE T_B2BSEQUENCE SET C_VALUE=0 WHERE C_NAME='T_ZRATE'";
                        Server.getInstance().getSystemService().findMapResultBySql(updsqlsql, null);
                        return;
                    }
                    String updsqlsql1 = " UPDATE T_B2BSEQUENCE SET C_VALUE=1 WHERE C_NAME='T_ZRATE'";
                    Server.getInstance().getSystemService().findMapResultBySql(updsqlsql1, null);
                    OMElement e = result.getAlreadyDisposalSynchronizationNewResult().getExtraElement();
                    Iterator<OMElement> it = e.getChildElements();
                    DateFormat fromat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+08:00");
                    String lasttime = "";
                    while (it.hasNext()) {
                        OMElement o = it.next();
                        Iterator<OMElement> oit = o.getChildElements();
                        String deleteWhere = "DELETE FROM T_ZRATE WHERE 1=2 ";
                        int tempint = 0;
                        int delint = 0;
                        while (oit.hasNext()) {
                            try {
                                OMElement ot = oit.next(); // 单条政策
                                Iterator<OMElement> rit = ot.getChildElements();
                                Zrate zrate = new Zrate();
                                zrate.setModifyuser("8000YI");
                                zrate.setCreateuser("8000YI");
                                zrate.setAgentid(3l);
                                zrate.setIsenable(1);

                                while (rit.hasNext()) {

                                    OMElement rt = rit.next();
                                    String value = rt.getText();
                                    String name = rt.getLocalName();
                                    if (!value.isEmpty()) {
                                        if (name.equals("A1")) {
                                            zrate.setOutid(value);
                                        }
                                        if (name.equals("A2")) {
                                            zrate.setDepartureport(value);
                                        }
                                        if (name.equals("A3")) {
                                            zrate.setArrivalport(value);
                                        }
                                        if (name.equals("A4")) {
                                            zrate.setAircompanycode(value);
                                        }
                                        if (name.equals("A5")) {
                                            zrate.setFlightnumber(value);
                                            zrate.setType(1);
                                        }
                                        if (name.equals("A6")) {
                                            zrate.setWeeknum(value);
                                            zrate.setType(2);
                                        }
                                        if (name.equals("A7")) {// 返回1表示单程，返回2表示往返，返回3表示单程往返
                                            zrate.setVoyagetype(value);

                                        }
                                        if (name.equals("A8")) {
                                            zrate.setRatevalue(Float.parseFloat(value));
                                        }
                                        if (name.equals("A9")) {
                                            zrate.setCabincode(value);
                                        }
                                        if (name.equals("A10")) {
                                            zrate.setBegindate(new Timestamp(fromat.parse(value).getTime()));
                                        }
                                        if (name.equals("A11")) {
                                            zrate.setEnddate(new Timestamp(fromat.parse(value).getTime()));
                                        }
                                        if (name.equals("A12")) {
                                            try {
                                                String[] times = value.replace("|", ",").split(",");
                                                zrate.setWorktime(times[0]);
                                                zrate.setAfterworktime(times[1]);
                                                zrate.setOnetofiveworktime(times[0]);
                                                zrate.setOnetofiveaftertime(times[1]);
                                            }
                                            catch (Exception ex) {
                                                System.out.println("A12");
                                                ex.printStackTrace();
                                            }
                                        }
                                        if (name.equals("A13")) {
                                            try {
                                                String[] times = value.replace("|", ",").split(",");
                                                zrate.setOnetofiveworktime(times[0]);
                                                zrate.setOnetofiveaftertime(times[1]);
                                            }
                                            catch (Exception ex) {
                                                System.out.println("A13");
                                                ex.printStackTrace();
                                            }
                                        }
                                        if (name.equals("A14")) {
                                            zrate.setGeneral(1l);
                                            if ("0".equals(value)) {
                                                zrate.setZtype("1");
                                            }
                                            try {
                                                if (value.length() > 1) {

                                                }
                                                else {
                                                    zrate.setIstype(0L);
                                                }
                                            }
                                            catch (Exception exx) {
                                                exx.printStackTrace();
                                            }
                                        }
                                        if (name.equals("A15")) {

                                        }
                                        if (name.equals("A16")) {
                                            if (value.equals("BSP")) {
                                                zrate.setTickettype(1);
                                            }
                                            else {
                                                zrate.setTickettype(2);
                                            }
                                        }
                                        if (name.equals("A17")) {
                                            zrate.setRemark(value.trim() + "|" + zrate.getRemark());
                                        }
                                        if (name.equals("A18")) {

                                        }
                                        if (name.equals("A19")) {
                                            zrate.setOnetofivewastetime(value.replace('|', '-'));
                                        }
                                        if (name.equals("A20")) {
                                            zrate.setWeekendwastetime(value.replace('|', '-'));
                                        }
                                        if (name.equals("A21")) {
                                            zrate.setSchedule(value);
                                        }
                                        if (name.equals("A22")) {
                                            // System.out.println(value);
                                            if (value != null) {
                                                if ("可用".equals(value)) {
                                                    zrate.setIsenable(1);
                                                }
                                                else {
                                                    zrate.setIsenable(0);
                                                }
                                            }
                                            else {
                                                zrate.setIsenable(0);
                                            }
                                        }
                                        if (name.equals("A23")) {
                                            lasttime = value;
                                        }
                                    }
                                }
                                String outid = zrate.getOutid();
                                if (zrate.getIsenable().equals(1)) {
                                    String sql2 = "DELETE  FROM T_ZRATE WHERE C_OUTID ='" + outid + "'";
                                    Server.getInstance().getSystemService().findMapResultBySql(sql2, null);
                                    DBUtil.insertZrate(zrate);
                                    tempint++;
                                }
                                else {
                                    deleteWhere += " OR C_OUTID ='" + outid + "'";
                                    delint++;
                                }
                                zrate = null;
                            }
                            catch (Exception e2) {
                                e2.printStackTrace();
                            }

                        }

                        Server.getInstance().getSystemService().findMapResultBySql(deleteWhere, null);
                        System.out.println("insert:" + tempint);
                        System.out.println("delete:" + delint);
                    }

                    System.out.println(new SimpleDateFormat("HH:mm:ss").format(new Date()) + "******" + datetime
                            + "提取完成××");
                    System.out.println("[" + lasttime + "]");
                    if (lasttime.length() > 0) {
                        String updsql = " UPDATE T_B2BSEQUENCE SET C_VALUE='" + lasttime + "' WHERE C_NAME='ZRATETIME'";
                        Server.getInstance().getSystemService().findMapResultBySql(updsql, null);
                        String updsqlsql = " UPDATE T_B2BSEQUENCE SET C_VALUE=0 WHERE C_NAME='T_ZRATE'";
                        Server.getInstance().getSystemService().findMapResultBySql(updsqlsql, null);
                    }
                    else {
                        String updsqlsql = " UPDATE T_B2BSEQUENCE SET C_VALUE=0 WHERE C_NAME='T_ZRATE'";
                        Server.getInstance().getSystemService().findMapResultBySql(updsqlsql, null);
                    }
                }
                catch (Exception e) {
                    System.out.println("8000翼赋值异常：");
                    String updsqlsql = " UPDATE T_B2BSEQUENCE SET C_VALUE=0 WHERE C_NAME='T_ZRATE'";
                    Server.getInstance().getSystemService().findMapResultBySql(updsqlsql, null);
                    e.printStackTrace();
                }

            }
            catch (Exception e) {
                System.out.println("^^^^^^^8000异常^^^^^^^^^");
                String updsqlsql = " UPDATE T_B2BSEQUENCE SET C_VALUE=0 WHERE C_NAME='T_ZRATE'";
                Server.getInstance().getSystemService().findMapResultBySql(updsqlsql, null);
                e.printStackTrace();
            }
        }
        else {
            System.out.println("----------8000yi接口被禁用--------------------");
        }
    }

    /**
     * 用于更新政策如果更新成功则返回更新的数量
     * @param sql
     * @return
     */
    public int updateZrate(Zrate zrate) {
        String sql = "UPDATE T_ZRATE SET C_DEPARTUREPORT='" + zrate.getDepartureport() + "',C_ARRIVALPORT='"
                + zrate.getArrivalport() + "',C_CABINCODE='" + zrate.getCabincode() + "',C_RATEVALUE="
                + zrate.getRatevalue() + ",C_MODIFYTIME='" + zrate.getModifytime().toLocaleString() + "',C_REMARK='"
                + zrate.getRemark() + "',C_AIRCOMPANYCODE='" + zrate.getAircompanycode() + "',C_TICKETTYPE='"
                + zrate.getTickettype() + "',C_GENERAL='" + zrate.getGeneral() + "',C_BEGINDATE='"
                + zrate.getBegindate().toLocaleString() + "',C_ENDDATE='" + zrate.getEnddate().toLocaleString()
                + "',C_AFTERWORKTIME='" + zrate.getAfterworktime() + "',C_WORKTIME='" + zrate.getWorktime()
                + "',C_SPEED='" + zrate.getSpeed() + "',C_ONETOFIVEWASTETIME='" + zrate.getOnetofivewastetime()
                + "',C_VOYAGETYPE='" + zrate.getVoyagetype() + "',C_USERTYPE='" + zrate.getUsertype()
                + "' WHERE C_OUTID='" + zrate.getOutid() + "'";
        //		WriteLog.write("8000YIZRATE", sql);
        return Server.getInstance().getAirService().excuteZrateBySql(sql);
    }

    public static String getGzipdata(String time) {
        String result = "-1";
        try {
            PlyIntefaceServiceStub stub = new PlyIntefaceServiceStub();
            PlyIntefaceServiceStub.AlreadyDisposalSynchronizationNew_All_GZip alreadyDisposalSynchronizationNew_All_GZip = new PlyIntefaceServiceStub.AlreadyDisposalSynchronizationNew_All_GZip();
            alreadyDisposalSynchronizationNew_All_GZip.setDatetime("2013-01-01 00:00:00.000");
            alreadyDisposalSynchronizationNew_All_GZip.setName("bjhthy");
            alreadyDisposalSynchronizationNew_All_GZip.setPwd("bjhthy");
            PlyIntefaceServiceStub.AlreadyDisposalSynchronizationNew_All_GZipResponse response = stub
                    .alreadyDisposalSynchronizationNew_All_GZip(alreadyDisposalSynchronizationNew_All_GZip);
            result = response.getAlreadyDisposalSynchronizationNew_All_GZipResult();
            System.out.println(result);
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static void main(String[] args) {
        boolean a = true;
        if (a == true) {

        }
        try {
            Job8000yi Job8000yi = new Job8000yi();
            Job8000yi.updateZrateNew();
        }
        catch (Exception e2) {
            System.out.println(e2.toString());
            e2.printStackTrace();
        }
    }
}
