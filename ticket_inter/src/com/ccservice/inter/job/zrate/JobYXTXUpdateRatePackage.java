package com.ccservice.inter.job.zrate;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.dom4j.DocumentException;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.ben.YeeXingBook;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.test.MD5Util;
import com.yeexing.webservice.service.IBEService;

public class JobYXTXUpdateRatePackage implements Job {
    YeeXingBook yeeXing = new YeeXingBook();

    @Override
    public void execute(JobExecutionContext context) {
        if (yeeXing.getStaus().equals("1")) {
            IBEService serv = new IBEService();
            try {
                String sqltime = "SELECT C_VALUE AS PICKTIME FROM T_B2BSEQUENCE WHERE C_NAME='YIXINGTIME' AND C_VALUE IS NOT  NULL";
                List clisttime = Server.getInstance().getSystemService().findMapResultBySql(sqltime, null);
                if (clisttime.size() == 0) {
                    System.out.println("�������»�ûȫ����ȡ,�ȴ�ȫ����ȡ������ͬ���ӿ�");
                    return;
                }
                String sqlsql = "SELECT C_VALUE AS PICKTIME FROM T_B2BSEQUENCE WHERE C_NAME='YIXINGISSTAR' AND C_VALUE = 1";
                List clist = Server.getInstance().getSystemService().findMapResultBySql(sqlsql, null);
                if (clist.size() > 0) {
                    System.out.println("�����������ڽ����ϴ�ͬ��");
                    return;
                }
                else {
                    String sqlsql1 = "update T_B2BSEQUENCE set C_VALUE = '1' WHERE C_NAME='YIXINGISSTAR'";
                    Server.getInstance().getSystemService().findMapResultBySql(sqlsql1, null);

                    String lastUpdateTime = getLastUpdateTime();
                    // String lastUpdateTime = "2012-07-05 17:16:26";
                    System.out.println(lastUpdateTime);
                    if (!lastUpdateTime.equals("-1")) {
                        String userName = yeeXing.getUsername();
                        String sign = MD5Util.MD5(URLEncoder.encode(lastUpdateTime + userName + yeeXing.getKey(),
                                "UTF-8").toUpperCase());
                        String xml = serv.getIBEServiceHttpPort().airpSync(userName, lastUpdateTime, sign);
                        WriteLog.write("yeexing����", xml);
                        org.dom4j.Document doc = org.dom4j.DocumentHelper.parseText(xml);
                        org.dom4j.Element root = doc.getRootElement();
                        String is_success = root.elementText("is_success");
                        if (is_success.equals("T")) {
                            org.dom4j.Element discounts = root.element("discounts");
                            List list = discounts.elements("discount");
                            Iterator it = list.iterator();
                            while (it.hasNext()) {
                                Zrate zrate = new Zrate();
                                zrate.setAgentid(4L);
                                zrate.setCreateuser("job");
                                zrate.setCreatetime(new Timestamp(new Date().getTime()));
                                zrate.setModifytime(new Timestamp(new Date().getTime()));
                                zrate.setModifyuser("job");
                                zrate.setIsenable(1);
                                zrate.setUsertype("1");

                                org.dom4j.Element elmt = (org.dom4j.Element) it.next();
                                String outid = elmt.attributeValue("plcid");
                                zrate.setOutid(outid);
                                String where = "  where 1=1 and " + Zrate.COL_outid + " ='" + zrate.getOutid()
                                        + "' and " + Zrate.COL_agentid + " =5";
                                List<Zrate> listz = Server.getInstance().getAirService()
                                        .findAllZrate(where, " ORDER BY ID DESC", -1, 0);
                                if (listz.size() > 0) {
                                    zrate = listz.get(0);
                                }
                                String departureport = elmt.attributeValue("orgCity");
                                zrate.setDepartureport(departureport);
                                String arrivalport = elmt.attributeValue("dstCity");
                                zrate.setArrivalport(arrivalport);
                                String aircompanycode = elmt.attributeValue("airComp");
                                zrate.setAircompanycode(aircompanycode);
                                String flightnumber = elmt.attributeValue("flight");
                                if (!flightnumber.equals("-")) {
                                    zrate.setFlightnumber(flightnumber);
                                }
                                String weeknum = elmt.attributeValue("flightNPermit");
                                if (!weeknum.equals("-")) {
                                    zrate.setWeeknum(weeknum);
                                }
                                String cabincode = elmt.attributeValue("cabin");
                                zrate.setCabincode(cabincode);
                                Float ratevalue = Float.parseFloat(elmt.attributeValue("disc"));
                                zrate.setRatevalue(ratevalue);
                                String extReward = elmt.attributeValue("extReward");
                                zrate.setAddratevalue(Float.parseFloat(extReward));
                                Timestamp begindate = new Timestamp(new SimpleDateFormat("yyyy-MM-dd").parse(
                                        elmt.attributeValue("startDate")).getTime());
                                zrate.setBegindate(begindate);
                                Timestamp enddate = new Timestamp(new SimpleDateFormat("yyyy-MM-dd").parse(
                                        elmt.attributeValue("endDate")).getTime());
                                zrate.setEnddate(enddate);
                                String tickettype = elmt.attributeValue("tickType");// 1=BSP,2=B2B
                                if (tickettype.equals("1")) {
                                    zrate.setTickettype(2);
                                }
                                else {
                                    zrate.setTickettype(1);
                                }
                                String changePnr = elmt.attributeValue("changePnr");
                                zrate.setIschange(Long.parseLong(changePnr));
                                Long general = Long.parseLong(elmt.attributeValue("isSphigh")) + 1;
                                zrate.setGeneral(general);
                                zrate.setZtype(general + "");
                                String remark = elmt.attributeValue("memo");
                                zrate.setRemark(remark);
                                String plcType = elmt.attributeValue("plcType");// ��ǰ�����1λ�ǵ��̵�2λ�����س̵�3λ�����س̵Ϳ�����Ϊ1��������Ϊ0
                                for (int i = 0; i < plcType.length(); i++) {
                                    if (plcType.charAt(i) == '1') {
                                        int t = i + 1;
                                        zrate.setVoyagetype(t + "");
                                    }
                                }
                                // String dayLimit =
                                // elmt.attributeValue("dayLimit");
                                String userType = elmt.attributeValue("userType");
                                zrate.setUsertype(userType);
                                String workTime = elmt.attributeValue("workTime");
                                zrate.setWorktime(workTime.split("[-]")[0]);
                                zrate.setAfterworktime(workTime.split("[-]")[1]);
                                Integer isenable = Integer.parseInt(elmt.attributeValue("plcEnable"));
                                zrate.setIsenable(isenable);
                                // String paytype =
                                // elmt.attributeValue("paytype");
                                // String modifyTime =
                                // elmt.attributeValue("modifyTime");
                                if (zrate.getIsenable().equals("0")) {
                                    String sql2 = "DELETE  FROM T_ZRATE WHERE C_OUTID ='" + zrate.getOutid() + "'";
                                    Server.getInstance().getAirService().excuteTeamapplyBySql(sql2);
                                    System.out.println("��������delete:" + zrate.getOutid());
                                }
                                else if (zrate.getIsenable().equals(1) && listz.size() == 0) {
                                    System.out.println("��������insert:" + zrate.getOutid());
                                    Server.getInstance().getAirService().createZrate(zrate);
                                }
                                else if (listz.size() > 0 && listz.size() > 0) {
                                    System.out.println("��������update:" + zrate.getOutid());
                                    Server.getInstance().getAirService().updateZrateIgnoreNull(zrate);
                                }
                            }
                        }
                        else {
                            sqlsql1 = "update T_B2BSEQUENCE set C_VALUE = '0' WHERE C_NAME='YIXINGISSTAR'";
                            Server.getInstance().getSystemService().findMapResultBySql(sqlsql1, null);
                            return;
                        }
                    }
                    sqlsql1 = "update T_B2BSEQUENCE set C_VALUE = '0' WHERE C_NAME='YIXINGISSTAR'";
                    Server.getInstance().getSystemService().findMapResultBySql(sqlsql1, null);
                    System.out.println("�������±��θ��½���:" + new Date());
                }
            }
            catch (Exception e) {
                String sqlsql1 = "update T_B2BSEQUENCE set C_VALUE = '0' WHERE C_NAME='YIXINGISSTAR'";
                Server.getInstance().getSystemService().findMapResultBySql(sqlsql1, null);
                e.printStackTrace();
            }
        }
        else {
            System.out.println("�������½ӿڱ�����");
        }
    }

    /**
     * �������ͬ��ʱ��
     * 
     * @return
     */
    public String getLastUpdateTime() {
        IBEService serv = new IBEService();
        String userName = yeeXing.getUsername();
        String sign = "";
        try {
            sign = MD5Util.MD5(URLEncoder.encode(userName + yeeXing.getKey(), "UTF-8").toUpperCase());
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        System.out.println("������ȡ���ͬ��ʱ��ķ���");
        String result = serv.getIBEServiceHttpPort().airpSyncLastDate(userName, sign);
        WriteLog.write("��������", "lastTime:" + result);
        String time = "";
        try {
            org.dom4j.Document doc = org.dom4j.DocumentHelper.parseText(result);
            org.dom4j.Element root = doc.getRootElement();
            String is_success = root.elementText("is_success");
            if (is_success.equals("T")) {
                time = root.elementText("lastModifyTime");
            }
            else {
                time = "-1";
            }
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }
        return time;
    }
}
