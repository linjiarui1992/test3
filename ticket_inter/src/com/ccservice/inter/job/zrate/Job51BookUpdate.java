package com.ccservice.inter.job.zrate;

import java.rmi.RemoteException;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.axis2.AxisFault;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

import com.ccservice.b2b2c.base.eaccount.Eaccount;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.UtilMethod;
import com.ccservice.b2b2c.policy.ben.FiveonBook;
import com.ccservice.inter.job.DBUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.test.HttpClient;
import com.liantuo.webservice.client.SynchronizePolicyDataService_2_0Stub;
import com.liantuo.webservice.client.SynchronizePolicyDataService_2_0Stub.ModifiedPolicyData;
import com.webservice.fiveonebookv3.SyncPolicyServiceImpl_1_0ServiceStub;

public class Job51BookUpdate implements Job {
    static int rowsPerPage = 1000;

    static DateFormat fromat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public void execute(JobExecutionContext context) {
        fiveone3_0updatezrate();
    }

    public static void main(String[] args) {
    }

    public static void fiveone3_0updatezrate() {
        Eaccount eaccout = Server.getInstance().getSystemService().findEaccount(12);
        try {
            SyncPolicyServiceImpl_1_0ServiceStub stub = new SyncPolicyServiceImpl_1_0ServiceStub();
            SyncPolicyServiceImpl_1_0ServiceStub.SyncPolicyE syncPolicyE = new SyncPolicyServiceImpl_1_0ServiceStub.SyncPolicyE();
            SyncPolicyServiceImpl_1_0ServiceStub.SyncPolicy syncPolicy = new SyncPolicyServiceImpl_1_0ServiceStub.SyncPolicy();
            SyncPolicyServiceImpl_1_0ServiceStub.SyncPolicyRequest syncPolicyRequest = new SyncPolicyServiceImpl_1_0ServiceStub.SyncPolicyRequest();
            String agencyCode = eaccout.getUsername();
            String safecode = eaccout.getPassword();
            String lastPolicyId = eaccout.getLastzid();
            String lastUpdateTime = eaccout.getUpdatetime();
            int needSpePricePolicy = 1;
            int needSpeRulePolicy = 1;
            syncPolicyRequest.setAgencyCode(agencyCode);
            syncPolicyRequest.setLastPolicyId(lastPolicyId);
            syncPolicyRequest.setLastUpdateTime(lastUpdateTime);
            syncPolicyRequest.setNeedSpePricePolicy(needSpePricePolicy);
            syncPolicyRequest.setNeedSpeRulePolicy(needSpeRulePolicy);
            syncPolicyRequest.setRowsPerPage(rowsPerPage);
            String sign = agencyCode + lastPolicyId + lastUpdateTime + needSpePricePolicy + needSpeRulePolicy
                    + rowsPerPage + safecode;
            sign = HttpClient.MD5(sign);
            syncPolicyRequest.setSign(sign);

            syncPolicy.setRequest(syncPolicyRequest);
            syncPolicyE.setSyncPolicy(syncPolicy);
            SyncPolicyServiceImpl_1_0ServiceStub.SyncPolicyResponseE responseE = stub.syncPolicy(syncPolicyE);
            SyncPolicyServiceImpl_1_0ServiceStub.SyncPolicyReply policyReply = responseE.getSyncPolicyResponse()
                    .get_return();
            policyReply.getLastGmtModified();
            policyReply.getLastPolicyId();
            System.out.println(lastPolicyId + ":" + lastUpdateTime + ":" + policyReply.getReturnCode() + ":"
                    + policyReply.getReturnMessage());
            if (policyReply.getReturnCode().equals("S")) {
                SyncPolicyServiceImpl_1_0ServiceStub.WsPolicyData[] wsPolicyDatas = policyReply.getPolicyDatasList();
                String deleteWhere = "DELETE FROM T_ZRATE WHERE 1=2 ";
                List<Zrate> zrates = new ArrayList<Zrate>();
                for (int i = 0; i < wsPolicyDatas.length; i++) {
                    try {
                        Zrate zrate = new Zrate();
                        SyncPolicyServiceImpl_1_0ServiceStub.WsPolicyData wsPolicyData = wsPolicyDatas[i];
                        //起飞三字码
                        zrate.setDepartureport(wsPolicyData.getFlightCourse().split("[-]")[0]);
                        //到达三字码
                        zrate.setArrivalport(wsPolicyData.getFlightCourse().split("[-]")[1]);
                        if (wsPolicyData.getFlightNoIncluding() != null
                                && wsPolicyData.getFlightNoIncluding().length() > 0) {
                            //适用航班号
                            zrate.setFlightnumber(wsPolicyData.getFlightNoIncluding());
                        }
                        //仓位代码
                        zrate.setCabincode(wsPolicyData.getSeatClass());
                        //返点
                        zrate.setRatevalue(wsPolicyData.getCommisionPoint());
                        zrate.setCreateuser("51BOOK3.0_update");
                        zrate.setCreatetime(new Timestamp(System.currentTimeMillis()));
                        zrate.setRemark(wsPolicyData.getComment());
                        zrate.setIsenable(1);
                        zrate.setAircompanycode(wsPolicyData.getAirlineCode());
                        zrate.setAgentid(5L);
                        zrate.setTickettype(wsPolicyData.getPolicyType().equals("B2B") ? 2 : 1);
                        zrate.setGeneral((long) wsPolicyData.getProductType());
                        //开始结束时间
                        zrate.setBegindate(new Timestamp(fromat.parse(wsPolicyData.getStartDate()).getTime()));
                        zrate.setEnddate(new Timestamp(fromat.parse(wsPolicyData.getExpiredDate()).getTime()));
                        zrate.setOutid(wsPolicyData.getPolicyId() + "");
                        deleteWhere += " OR C_OUTID ='" + zrate.getOutid() + "'";
                        zrate.setAfterworktime(wsPolicyData.getWorkTime().split("[-]")[1]);
                        zrate.setWorktime(wsPolicyData.getWorkTime().split("[-]")[0]);//08:00-23:59
                        zrate.setSpeed(wsPolicyData.getTicketSpeed().split("分钟")[0]);
                        zrate.setOnetofivewastetime(wsPolicyData.getVtWorkingTime());
                        zrate.setZtype("1");
                        zrate.setVoyagetype(wsPolicyData.getRouteType().equals("OW") ? "1" : "2");
                        zrate.setUsertype("1");
                        //
                        zrate.setIschange((long) wsPolicyData.getNeedSwitchPNR());
                        //不适用的航班号
                        if (wsPolicyData.getFlightNoExclude() != null && wsPolicyData.getFlightNoExclude().length() > 0) {
                            //适用航班号
                            zrate.setWeeknum(wsPolicyData.getFlightNoExclude());
                        }
                        if (wsPolicyData.getPolicyStatus() == 1) {
                            zrates.add(zrate);
                        }
                    }
                    catch (Exception e) {

                    }
                }
                eaccout.setLastzid(policyReply.getLastPolicyId());
                eaccout.setUpdatetime(policyReply.getLastGmtModified());
                WriteLog.write("51BOOK_DELETE", deleteWhere);
                Server.getInstance().getAirService().excuteZrateBySql(deleteWhere);
                if (zrates.size() > 0) {
                    Server.getInstance().getAirService().createZrateList(zrates);
                    System.out.println(zrates.size() + "-" + eaccout.getLastzid() + "-" + eaccout.getUpdatetime());
                }
                WriteLog.write("51BOOK_UPDATE",
                        zrates.size() + "-" + eaccout.getLastzid() + "-" + eaccout.getUpdatetime());
                Server.getInstance().getSystemService().updateEaccount(eaccout);
                System.out.println("--------------------");
            }
            else {
                System.out.println(policyReply.getReturnCode() + ":" + policyReply.getReturnMessage());
                WriteLog.write("51BOOK_UPDATE_ERR", policyReply.getReturnCode() + ":" + policyReply.getReturnMessage());
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }

    public static String isnull(String value) {
        return value == null ? "NULL" : value;
    }

    public void fiveone2_0updatezrate() {
        int RowPerPage = 500;
        FiveonBook fiveonBook = new FiveonBook();
        System.out.println(fiveonBook.getStaus());
        if (!fiveonBook.getStaus().equals("0")) {// 0,禁用 1,启用
            String sqlisstar = "SELECT C_VALUE AS PICKISSTAR FROM T_B2BSEQUENCE WHERE C_NAME='51ISSTAR' AND C_VALUE =0";
            // 1 表示正在更新 0表示更新完了
            List isstar = Server.getInstance().getSystemService().findMapResultBySql(sqlisstar, null);
            if (isstar.size() == 0) {
                System.out.println("51book上次更新还没结束,等待....................");
                return;
            }

            sqlisstar = "SELECT C_VALUE AS PICKISSTAR FROM T_B2BSEQUENCE WHERE C_NAME='T_ZRATE' AND C_VALUE =0";
            // 1 表示正在更新 0表示更新完了
            isstar = Server.getInstance().getSystemService().findMapResultBySql(sqlisstar, null);
            if (isstar.size() == 0) {
                System.out.println("8000yi上次更新还没结束,等待....................");
                return;
            }

            sqlisstar = "SELECT C_VALUE AS PICKISSTAR FROM T_B2BSEQUENCE WHERE C_NAME='JINRIISSTAR' AND C_VALUE =0";
            // 1 表示正在更新 0表示更新完了
            isstar = Server.getInstance().getSystemService().findMapResultBySql(sqlisstar, null);
            if (isstar.size() == 0) {
                System.out.println("jinri上次更新还没结束,等待....................");
                return;
            }

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Calendar calendar = Calendar.getInstance();
            String startDate = sdf.format(calendar.getTime());
            long agentid = 5;
            String lasteZrateID = "1";// 最后条记录ID
            String lastUpdateTime = startDate + " 00:00:00";// 最后次更新时间
            String sqltime = "SELECT C_VALUE AS PICKTIME FROM T_B2BSEQUENCE WHERE C_NAME='51ZRATETIME' AND C_VALUE IS NOT  NULL";
            List clisttime = Server.getInstance().getSystemService().findMapResultBySql(sqltime, null);
            if (clisttime.size() == 0) {
                String updsq1 = " UPDATE T_B2BSEQUENCE SET C_VALUE='0' WHERE C_NAME='51ISSTAR'";
                Server.getInstance().getSystemService().findMapResultBySql(updsq1, null);
                System.out.println("还没全部获取,等待全部获取后启动同步接口");
                return;
            }
            Date date23 = new Date();
            date23.setHours(23);
            date23.setMinutes(10);
            Date date5 = new Date();
            date5.setHours(1);
            String JINRINUM = "0";
            String uptime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
            // 是在23:30之前和4:00之后
            boolean realTime = !new Date().before(date5) && !new Date().after(date23);
            String uptime11 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
            System.out.println(uptime11 + ":" + realTime);
            if (!realTime) {
                System.out.println("在23:10之前和1:00之后才取");
                return;
            }
            // 设置时间开始,设置成正在更新中
            String updsq = " UPDATE T_B2BSEQUENCE SET C_VALUE='1' WHERE C_NAME='51ISSTAR'";
            Server.getInstance().getSystemService().findMapResultBySql(updsq, null);
            if (clisttime.size() > 0) {
                Map m = (Map) clisttime.get(0);
                lastUpdateTime = m.get("PICKTIME").toString();
            }

            String sql2 = "SELECT C_VALUE AS PICKID FROM T_B2BSEQUENCE WHERE C_NAME='51ZRATEID' AND C_VALUE IS NOT NULL";
            List clist2 = Server.getInstance().getSystemService().findMapResultBySql(sql2, null);
            if (clist2.size() > 0) {
                Map m = (Map) clist2.get(0);
                lasteZrateID = m.get("PICKID").toString();
            }
            System.out.println("------------------51book本次更新开始------------");
            System.out.println(lastUpdateTime + "----" + lasteZrateID);
            try {
                SynchronizePolicyDataService_2_0Stub stub = new SynchronizePolicyDataService_2_0Stub();
                SynchronizePolicyDataService_2_0Stub.SynchronizePolicyData data1 = new SynchronizePolicyDataService_2_0Stub.SynchronizePolicyData();
                SynchronizePolicyDataService_2_0Stub.SynchronizePolicyDataRequest re1 = new SynchronizePolicyDataService_2_0Stub.SynchronizePolicyDataRequest();
                SynchronizePolicyDataService_2_0Stub.SecurityCredential sec1 = new SynchronizePolicyDataService_2_0Stub.SecurityCredential();
                re1.setAirline("");// 航空公司
                re1.setDeparture("");// 出发地三字码
                re1.setArrival("");// 到当地三字码
                re1.setLastUpdateTime(lastUpdateTime);// 最后更新时间,格式：yyyy-MM-dd
                // HH:mm:ss
                // 值为上一次调用接口返回数据最后一条的修改时间，全取政策后第一次调用时值为当天的00：00：00
                re1.setReturnRecordCount(RowPerPage);// 一次返回记录数
                re1.setOutOfTownCity(0);// 异地政策 0：不管本地还是异地，全部的1：出发是异地的
                // 2：抵达是异地3：出发和抵达是异地的4：出发是本地的5：抵达是本地的6：出发和抵达是本地的
                re1.setPolicyId(lasteZrateID);// 最后更新时间所关联的那条政策Id值为上一次调用接口返回数据最后一条的id，全取政策后第一次调用时值为“1”
                re1.setIsBusinessUnitPolicy("1");// 特殊政策 =0 不包括 =1包括
                re1.setParam1("1");// =1 包括特价政策 =0 不包括特价政策 =2 只要特价政策
                sec1.setAgencyCode(fiveonBook.getAgentcode());
                String sign1 = HttpClient.MD5(sec1.getAgencyCode() + re1.getAirline() + re1.getDeparture()
                        + re1.getArrival() + re1.getLastUpdateTime() + re1.getReturnRecordCount()
                        + re1.getOutOfTownCity() + re1.getPolicyId() + re1.getIsBusinessUnitPolicy() + re1.getParam1()
                        + fiveonBook.getSafecode());
                sec1.setSign(sign1);
                re1.setCredential(sec1);
                data1.setIn0(re1);
                SynchronizePolicyDataService_2_0Stub.SynchronizePolicyDataResponse res1 = stub
                        .synchronizePolicyData(data1);
                res1 = stub.synchronizePolicyData(data1);
                System.out.println("---" + res1.getOut().getReturnCode() + "--" + res1.getOut().getReturnMessage());
                int len = 0;
                if (res1.getOut().getModifiedPolicyDataList().getModifiedPolicyData() != null) {
                    len = res1.getOut().getModifiedPolicyDataList().getModifiedPolicyData().length;
                }
                String deleteWhere = "DELETE FROM T_ZRATE WHERE 1=2 ";
                int tempint = 0;
                int updateint = 0;
                for (int a = 0; a < len; a++) {

                    try {
                        ModifiedPolicyData aa = res1.getOut().getModifiedPolicyDataList().getModifiedPolicyData()[a];
                        if (aa != null) {
                            Zrate zrate = new Zrate();
                            zrate.setAgentid(agentid);
                            if (aa.getFlightCourse() != null && aa.getFlightCourse().length() > 0
                                    && aa.getFlightCourse().indexOf("-") != -1) {
                                zrate.setDepartureport(aa.getFlightCourse().split("-")[0]);// 出发机场
                                if (zrate.getDepartureport() != null && zrate.getDepartureport().equals("999")) {
                                    if (aa.getDepartureExclude() != null) {
                                        zrate.setDepartureexclude(aa.getDepartureExclude());// 出发不适用
                                    }
                                }

                                zrate.setArrivalport(aa.getFlightCourse().split("-")[1]);// 到达
                                if (zrate.getArrivalport() != null && zrate.getArrivalport().equals("999")) {
                                    if (aa.getArrivalExclude() != null) {
                                        zrate.setArrivalexclude(aa.getArrivalExclude());// 到达不适用
                                    }
                                }
                            }
                            if (aa.getFlightNoIncluding() != null && aa.getFlightNoIncluding().length() > 3) {
                                zrate.setFlightnumber(aa.getFlightNoIncluding());// 适用的航班
                            }
                            if (aa.getFlightNoExclude() != null && aa.getFlightNoExclude().length() > 3) {
                                zrate.setWeeknum(aa.getFlightNoExclude());// 不适用的航班
                            }
                            if (aa.getSeatClass() != null && aa.getSeatClass().length() >= 1) {
                                zrate.setCabincode(aa.getSeatClass());
                            }
                            if (aa.getCommision() > 0) {
                                zrate.setRatevalue(aa.getCommision());
                            }
                            zrate.setCreatetime(new Timestamp(System.currentTimeMillis()));
                            zrate.setCreateuser("job");
                            zrate.setModifytime(new Timestamp(System.currentTimeMillis()));
                            zrate.setModifyuser("job");

                            if (aa.getPrintTicketStartDate() != null && aa.getPrintTicketStartDate().getTime() != null) {
                                zrate.setIssuedstartdate(new Timestamp(aa.getPrintTicketStartDate().getTime().getTime()));

                            }
                            if (aa.getExpiredDate() != null && aa.getExpiredDate().getTime() != null) {
                                zrate.setIssuedendate(new Timestamp(aa.getExpiredDate().getTime().getTime())); // 出票结束时间
                            }
                            if (aa.getExpiredDate() != null && aa.getExpiredDate().getTime() != null) {

                            }
                            zrate.setRemark(aa.getComment());

                            if (aa.getModifyFlag() > 0) {
                                if (aa.getModifyFlag() == 1 || aa.getModifyFlag() == 2) {
                                    zrate.setIsenable(1);
                                }
                                else {
                                    zrate.setIsenable(0);
                                }

                            }
                            zrate.setAircompanycode(aa.getAirlineCode());
                            if (aa.getPolicyType() != null && aa.getPolicyType().equals("B2B")) {
                                zrate.setTickettype(2);

                            }
                            else {

                                zrate.setTickettype(1);
                            }

                            if (aa.getBusinessUnitType() != null) {

                                if (aa.getBusinessUnitType().equals("0")) {// =0
                                    // 普通政策
                                    // =1
                                    // 特殊政策
                                    zrate.setGeneral(1l);
                                    zrate.setZtype("1");
                                }
                                else {
                                    zrate.setGeneral(2l);// 1,普通 2高反
                                    zrate.setZtype("2");
                                }
                            }
                            else {
                                zrate.setGeneral(1l);
                                zrate.setZtype("1");
                            }
                            if (aa.getStartDate() != null && aa.getStartDate().getTime() != null) {
                                zrate.setBegindate(new Timestamp(aa.getStartDate().getTime().getTime()));
                            }

                            if (aa.getPrintTicketExpiredDate() != null
                                    && aa.getPrintTicketExpiredDate().getTime() != null) {
                                zrate.setEnddate(new Timestamp(aa.getPrintTicketExpiredDate().getTime().getTime())); // 政策有效期结束时间
                            }
                            zrate.setOutid(aa.getId() + "");

                            if (aa.getWorkTime() != null && aa.getWorkTime().length() > 0
                                    && aa.getWorkTime().indexOf("-") != -1) {
                                String worktime = aa.getWorkTime().split("-")[0];
                                zrate.setWorktime(worktime);
                                zrate.setAfterworktime(aa.getWorkTime().split("-")[1]);
                            }

                            zrate.setSpeed(aa.getAgencyEfficiency());

                            if (aa.getRouteType() != null && aa.getRouteType().equals("OW")) {// 单程
                                zrate.setVoyagetype("1");// 1单程,2:往返，3:单程或往返
                            }
                            else {
                                zrate.setVoyagetype("2");// 1单程,2:往返，3:单程或往返
                            }
                            zrate.setSchedule(aa.getFlightCycle());// 航班周期
                            zrate.setUsertype("1");
                            try {
                                if (zrate.getIsenable().equals(1)) {
                                    if (zrate.getRatevalue() != null) {
                                        if (updateZrate(zrate) == 0) {
                                            DBUtil.insertZrate(zrate);
                                            tempint++;
                                        }
                                        else {
                                            updateint++;
                                        }
                                    }
                                }
                                else {
                                    deleteWhere += " OR C_OUTID ='" + zrate.getOutid() + "'";
                                }
                            }
                            catch (Exception e) {
                                UtilMethod.writeEx(this.getClass().getSimpleName(), e);
                                e.printStackTrace();
                            }
                            lasteZrateID = zrate.getOutid();
                            if (aa.getGmtModified() != null) {
                                lastUpdateTime = new Timestamp(aa.getGmtModified().getTime().getTime()).toString();
                            }
                            zrate = null;
                        }
                    }
                    catch (Exception e) {
                        UtilMethod.writeEx(this.getClass().getSimpleName(), e);
                        e.printStackTrace();
                    }
                }
                System.out.println("insert:" + tempint);
                System.out.println("updateint:" + updateint);
                Server.getInstance().getSystemService().findMapResultBySql(deleteWhere, null);
                String updsql = " UPDATE T_B2BSEQUENCE SET C_VALUE='" + lastUpdateTime + "' WHERE C_NAME='51ZRATETIME'";
                Server.getInstance().getSystemService().findMapResultBySql(updsql, null);
                String updsql2 = " UPDATE T_B2BSEQUENCE SET C_VALUE='" + lasteZrateID + "' WHERE C_NAME='51ZRATEID'";
                Server.getInstance().getSystemService().findMapResultBySql(updsql2, null);

                // 设置时间开始,设置成已经更新完
                String updsql22 = " UPDATE T_B2BSEQUENCE SET C_VALUE='0' WHERE C_NAME='51ISSTAR'";
                Server.getInstance().getSystemService().findMapResultBySql(updsql22, null);
                // 设置结束时间
                SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                System.out.println(sdf1.format(new Date().getTime()) + lastUpdateTime + ":" + lasteZrateID);
                System.out.println("------------------51book本次更新结束------------");
            }
            catch (Exception e) {
                // 设置时间开始,设置成已经更新完
                String updsql = " UPDATE T_B2BSEQUENCE SET C_VALUE='0' WHERE C_NAME='51ISSTAR'";
                // 设置结束时间
                updsql += ";UPDATE T_B2BSEQUENCE SET C_VALUE='" + lastUpdateTime + "' WHERE C_NAME='51ZRATETIME'";
                updsql = ";UPDATE T_B2BSEQUENCE SET C_VALUE='" + lasteZrateID + "' WHERE C_NAME='51ZRATEID'";
                Server.getInstance().getSystemService().findMapResultBySql(updsql, null);

                e.printStackTrace();
            }

        }
        else {

            System.out.println("-----------------51book接口被禁用------------------");
        }
    }

    /**
     * 用于更新政策如果更新成功则返回更新的数量
     * @param sql
     * @return
     */
    public int updateZrate(Zrate zrate) {
        String sql = "UPDATE T_ZRATE SET C_DEPARTUREPORT='" + zrate.getDepartureport() + "',C_ARRIVALPORT='"
                + zrate.getArrivalport() + "',C_CABINCODE='" + zrate.getCabincode() + "',C_RATEVALUE="
                + zrate.getRatevalue() + ",C_MODIFYTIME='" + zrate.getModifytime().toLocaleString() + "',C_REMARK='"
                + zrate.getRemark() + "',C_AIRCOMPANYCODE='" + zrate.getAircompanycode() + "',C_TICKETTYPE='"
                + zrate.getTickettype() + "',C_GENERAL='" + zrate.getGeneral() + "',C_BEGINDATE='"
                + zrate.getBegindate().toLocaleString() + "',C_ENDDATE='" + zrate.getEnddate().toLocaleString()
                + "',C_AFTERWORKTIME='" + zrate.getAfterworktime() + "',C_WORKTIME='" + zrate.getWorktime()
                + "',C_SPEED='" + zrate.getSpeed() + "',C_ONETOFIVEWASTETIME='" + zrate.getOnetofivewastetime()
                + "',C_VOYAGETYPE='" + zrate.getVoyagetype() + "',C_USERTYPE='" + zrate.getUsertype()
                + "' WHERE C_OUTID='" + zrate.getOutid() + "'";
        WriteLog.write("51BOOKZRATE", sql);
        return Server.getInstance().getAirService().excuteZrateBySql(sql);
    }

    public void excuSql(String sql) {
        /*
         * String url = "http://localhost:8080/cn_service/service/";
         * 
         * try { HessianProxyFactory factory = new HessianProxyFactory();
         * IAirService servier = (IAirService) factory.create(IAirService.class,
         * url + IAirService.class.getSimpleName()) ;
         * servier.excuteZrateBySql(sql); } catch (MalformedURLException e) { //
         * TODO Auto-generated catch block e.printStackTrace(); }
         * 
         */
        System.out.println("51book执行add操作开始");
        // System.out.println(sql);
        Server.getInstance().getAirService().excuteZrateBySql(sql);
        System.out.println("51book执行add操作结束");
    }
}
