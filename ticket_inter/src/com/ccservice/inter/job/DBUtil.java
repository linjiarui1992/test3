package com.ccservice.inter.job;

import java.sql.Timestamp;

import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.inter.server.Server;

public class DBUtil {
    public static void insertZrate(Zrate zrate) {
        try {
            String insertsql = "INSERT INTO T_ZRATE (C_DEPARTUREPORT,C_ARRIVALPORT,C_ISENABLE,C_AIRCOMPANYCODE,"
                    + "C_FLIGHTNUMBER,C_WEEKNUM,C_VOYAGETYPE,C_RATEVALUE,C_CABINCODE,C_BEGINDATE,"
                    + "C_ENDDATE,C_WORKTIME,C_AFTERWORKTIME,C_GENERAL,C_TICKETTYPE,"
                    + "C_REMARK,C_OUTID,C_CREATETIME,C_MODIFYTIME,C_AGENTID,C_USERTYPE,C_ONETOFIVEWASTETIME,C_WEEKENDWASTETIME,C_SPEED) VALUES('"
                    + zrate.getDepartureport()
                    + "','"
                    + zrate.getArrivalport()
                    + "',1,'"
                    + zrate.getAircompanycode()
                    + "',"
                    + (zrate.getFlightnumber() == null ? "NULL" : "'" + zrate.getFlightnumber() + "'")
                    + ",'"
                    + zrate.getWeeknum()
                    + "','"
                    + zrate.getVoyagetype()
                    + "',"
                    + zrate.getRatevalue()
                    + ",'"
                    + zrate.getCabincode()
                    + "','"
                    + zrate.getBegindate().toLocaleString()
                    + "','"
                    + zrate.getEnddate().toLocaleString()
                    + "','"
                    + zrate.getWorktime()
                    + "','"
                    + zrate.getAfterworktime()
                    + "',"
                    + (zrate.getGeneral() == null ? 1 : zrate.getGeneral())
                    + ","
                    + zrate.getTickettype()
                    + ","
                    + (zrate.getRemark() == null ? "NULL" : "'" + zrate.getRemark() + "'")
                    + ",'"
                    + zrate.getOutid()
                    + "','"
                    + new Timestamp(System.currentTimeMillis()).toLocaleString()
                    + "','"
                    + new Timestamp(System.currentTimeMillis()).toLocaleString()
                    + "',"
                    + zrate.getAgentid()
                    + ",'"
                    + zrate.getUsertype()
                    + "',"
                    + (zrate.getOnetofivewastetime() == null ? "NULL" : "'" + zrate.getOnetofivewastetime() + "'")
                    + ","
                    + (zrate.getWeekendwastetime() == null ? "NULL" : "'" + zrate.getWeekendwastetime() + "'")
                    + ","
                    + (zrate.getSpeed() == null ? "NULL" : "'" + zrate.getSpeed() + "'")
                    + ")";
            //			System.out.println(insertsql);
            Server.getInstance().getSystemService().findMapResultBySql(insertsql, null);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void updateZrate(Zrate zrate) {
        try {
            String updataZrateString = "UPDATE T_ZRATE SET C_DEPARTUREPORT='"
                    + zrate.getDepartureport()
                    + "',C_ARRIVALPORT='"
                    + zrate.getArrivalport()
                    + "',C_AIRCOMPANYCODE='"
                    + zrate.getAircompanycode()
                    + "',C_FLIGHTNUMBER="
                    + (zrate.getFlightnumber() == null ? "NULL" : "'" + zrate.getFlightnumber() + "'")
                    + ",C_WEEKNUM='"
                    + zrate.getWeeknum()
                    + "',C_VOYAGETYPE='"
                    + zrate.getVoyagetype()
                    + "',C_RATEVALUE="
                    + zrate.getRatevalue()
                    + ",C_CABINCODE='"
                    + zrate.getCabincode()
                    + "',C_BEGINDATE='"
                    + zrate.getBegindate().toLocaleString()
                    + "',C_ENDDATE='"
                    + zrate.getEnddate().toLocaleString()
                    + "',C_MODIFYTIME='"
                    + new Timestamp(System.currentTimeMillis()).toLocaleString()
                    + "',C_WORKTIME='"
                    + zrate.getWorktime()
                    + "',C_AFTERWORKTIME='"
                    + zrate.getAfterworktime()
                    + "',C_GENERAL="
                    + (zrate.getGeneral() == null ? 1 : zrate.getGeneral())
                    + ",C_TICKETTYPE="
                    + zrate.getTickettype()
                    + ",C_REMARK="
                    + (zrate.getRemark() == null ? "NULL" : "'" + zrate.getRemark() + "'")
                    + " WHERE C_OUTID='"
                    + zrate.getOutid()
                    + "'";
            Server.getInstance().getSystemService().findMapResultBySql(updataZrateString, null);
        }
        catch (Exception ex1) {
            ex1.printStackTrace();
        }
    }
}
