package com.ccservice.inter.job.Visa;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.ctrip.capturedata.ICripVisa;
import com.ccservice.ctrip.thread.CtripVisaThread;

public class JobCtripVisa implements Job {

    private String url = "http://localhost:8080/Reptile/service/";

    private HessianProxyFactory factory = new HessianProxyFactory();

    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        System.out.println("=============抓取携程签证数据开始=============");
        try {
            String allVisaData = "";
            ICripVisa service = (ICripVisa) factory.create(ICripVisa.class, url + ICripVisa.class.getSimpleName());
            String ctripAllVisaData = service.allAnalyticalDataOfVisa();
            if (ctripAllVisaData != null && !ctripAllVisaData.equals("") && !ctripAllVisaData.equals("FAIL")) {
                allVisaData = ctripAllVisaData;
            }
            if (allVisaData != null && !allVisaData.equals("") && !allVisaData.equals("FAIL")) {
                JSONObject jsonObject = JSONObject.fromObject(allVisaData.toString());
                JSONArray ctripVisaAllData = jsonObject.getJSONArray("ctripVisaAllData");
                if (ctripVisaAllData != null && ctripVisaAllData.size() > 0) {
                    ExecutorService executorService = Executors.newFixedThreadPool(ctripVisaAllData.size());
                    for (int i = 0; i < ctripVisaAllData.size(); i++) {
                        JSONObject ctripVisaData = JSONObject.fromObject(ctripVisaAllData.get(i));
                        String guoJiaMingCheng = ctripVisaData.getString("guoJiaMingCheng");
                        JSONArray detailInfo = ctripVisaData.getJSONArray("detailInfo");
                        if (detailInfo != null && detailInfo.size() > 0) {
                            List detailInfoList = JSONArray.toList(detailInfo);
                            executorService.execute(new CtripVisaThread(guoJiaMingCheng, detailInfo));
                        }
                    }
                    executorService.shutdown();
                }
            }
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
