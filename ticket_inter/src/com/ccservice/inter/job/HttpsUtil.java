package com.ccservice.inter.job;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * HTTPS POST/GET 
 * @time 2014年8月30日 下午3:01:10
 * @author yinshubin
 */
public class HttpsUtil {

    private static class TrustAnyTrustManager implements X509TrustManager {
        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[] {};
        }
    }

    private static class TrustAnyHostnameVerifier implements HostnameVerifier {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    }

    /**
     * https post方式请求服务器。传入MAP
     * @param url
     * @param content
     * @param charset
     * @param requestproperty
     * @return
     * @throws NoSuchAlgorithmException
     * @throws KeyManagementException
     * @throws IOException
     * @time 2014年8月30日 下午2:57:46
     * @author yinshubin
     */
    public static String post12306(String url, String content, String charset, Map<String, String> requestproperty)
            throws NoSuchAlgorithmException, KeyManagementException, IOException {
        StringBuffer responseMessage = null;
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, new TrustManager[] { new TrustAnyTrustManager() }, new java.security.SecureRandom());

        URL console = new URL(url);
        URLConnection con = console.openConnection();
        //        con.addRequestProperty("JSESSIONID", cookiestring);
        //        con.addRequestProperty("Cookie", cookiestring);
        Iterator iter = requestproperty.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry<String, String> entry = (Map.Entry<String, String>) iter.next();
            con.addRequestProperty(entry.getKey(), entry.getValue());
        }
        HttpsURLConnection conn = (HttpsURLConnection) con;
        conn.setSSLSocketFactory(sc.getSocketFactory());
        conn.setHostnameVerifier(new TrustAnyHostnameVerifier());
        conn.setDoOutput(true);
        conn.connect();
        DataOutputStream out = new DataOutputStream(conn.getOutputStream());
        out.write(content.getBytes(charset));
        // 刷新、关闭
        out.flush();
        out.close();
        InputStream is = conn.getInputStream();
        if (is != null) {
            int charCount = -1;
            BufferedReader br = null;
            responseMessage = new StringBuffer();
            String ContentEncoding = conn.getHeaderField("Content-Encoding");
            if ("gzip".equals(ContentEncoding)) {
                br = new BufferedReader(new InputStreamReader(new GZIPInputStream(is), "UTF-8"));
            }
            else {
                br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            }
            try {
                while ((charCount = br.read()) != -1) {
                    responseMessage.append((char) charCount);
                }
            }
            catch (Exception e) {
            }
            //            List<String> s = conn.getHeaderFields().get("Set-Cookie");
            //            try {
            //                String cookiestring = getCookie(s.toString());
            //                WriteLog.write("train12306_order", "访问gateway.do获取的Set-Cookie:" + s);
            //                return responseMessage.toString() + "拿到新Cookie" + cookiestring;
            //            }
            //            catch (Exception e) {
            //            }
        }
        return responseMessage.toString();
    }

    /**
     * java.net实现 HTTP或HTTPs GET方法提交,变体
     * @param strUrl
     * @return
     * @time 2014年8月30日 下午2:59:53
     * @author yinshubin
     */
    public static String Get12306(String strUrl, Map<String, String> requestproperty) {
        StringBuffer responseMessage = null;
        URLConnection connection = null;
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, new TrustManager[] { new TrustAnyTrustManager() }, new java.security.SecureRandom());

            URL url = new URL(strUrl);
            connection = url.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(false);
            Iterator iter = requestproperty.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry<String, String> entry = (Map.Entry<String, String>) iter.next();
                connection.addRequestProperty(entry.getKey(), entry.getValue());
            }
            HttpsURLConnection conn = (HttpsURLConnection) connection;
            conn.setSSLSocketFactory(sc.getSocketFactory());

            conn.setHostnameVerifier(new TrustAnyHostnameVerifier());
            conn.setDoOutput(true);
            InputStream is = conn.getInputStream();
            if (is != null) {
                int charCount = -1;
                BufferedReader br = null;
                responseMessage = new StringBuffer();
                String ContentEncoding = conn.getHeaderField("Content-Encoding");
                if ("gzip".equals(ContentEncoding)) {
                    br = new BufferedReader(new InputStreamReader(new GZIPInputStream(is), "UTF-8"));
                }
                else {
                    br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                }
                try {
                    while ((charCount = br.read()) != -1) {
                        responseMessage.append((char) charCount);
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return responseMessage.toString();
    }

    /**
     * https post方式请求服务器
     * @param url
     * @param content
     * @param charset
     * @param cookiestring
     * @return
     * @throws NoSuchAlgorithmException
     * @throws KeyManagementException
     * @throws IOException
     * @time 2014年8月30日 下午2:58:26
     * @author yinshubin
     */
    public static String post(String url, String content, String charset, String cookiestring)
            throws NoSuchAlgorithmException, KeyManagementException, IOException {
        StringBuffer responseMessage = new StringBuffer();
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, new TrustManager[] { new TrustAnyTrustManager() }, new java.security.SecureRandom());
        URL console = new URL(url);
        URLConnection con = console.openConnection();
        con.addRequestProperty("JSESSIONID", cookiestring);
        con.addRequestProperty("Cookie", cookiestring);
        HttpsURLConnection conn = (HttpsURLConnection) con;
        conn.setSSLSocketFactory(sc.getSocketFactory());
        conn.setHostnameVerifier(new TrustAnyHostnameVerifier());
        conn.setDoOutput(true);
        conn.connect();
        DataOutputStream out = new DataOutputStream(conn.getOutputStream());
        out.write(content.getBytes(charset));
        // 刷新、关闭
        out.flush();
        out.close();
        InputStream is = conn.getInputStream();
        if (is != null) {
            int charCount = -1;
            BufferedReader br = null;
            String ContentEncoding = conn.getHeaderField("Content-Encoding");
            if ("gzip".equals(ContentEncoding)) {
                br = new BufferedReader(new InputStreamReader(new GZIPInputStream(is), "UTF-8"));
            }
            else {
                br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            }
            try {
                while ((charCount = br.read()) != -1) {
                    responseMessage.append((char) charCount);
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return responseMessage.toString();
    }

    public static String get(String url, String cookiestring, String charset) throws Exception {
        //SSL
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, new TrustManager[] { new TrustAnyTrustManager() }, new java.security.SecureRandom());
        //RETURN
        StringBuffer res = new StringBuffer();
        InputStream in = null;
        HttpsURLConnection con = null;
        BufferedReader reader = null;
        try {
            con = (HttpsURLConnection) (new URL(url)).openConnection();
            con.setDoOutput(true);
            con.setUseCaches(false);
            if (cookiestring != null && !"".equals(cookiestring.trim())) {
                con.addRequestProperty("JSESSIONID", cookiestring);
                con.addRequestProperty("Cookie", cookiestring);
            }
            con.setSSLSocketFactory(sc.getSocketFactory());
            con.setHostnameVerifier(new TrustAnyHostnameVerifier());
            con.connect();
            in = con.getInputStream();
            //判断是否压缩
            String ContentEncoding = con.getHeaderField("Content-Encoding");
            if ("gzip".equalsIgnoreCase(ContentEncoding)) {
                reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(in), charset));
            }
            else {
                reader = new BufferedReader(new InputStreamReader(in, charset));
            }
            String lineTxt = null;
            while ((lineTxt = reader.readLine()) != null) {
                res.append(lineTxt);
            }
        }
        catch (Exception e) {
            res = new StringBuffer();
        }
        finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            }
            catch (Exception e) {
            }
            try {
                if (in != null) {
                    in.close();
                }
            }
            catch (Exception e) {
            }
        }
        return res.toString();
    }

    public static String getProxy(String url, String cookiestring, String charset, String proxyHost, int proxyPort)
            throws Exception {
        // 使用java.net.Proxy类设置代理IP
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost, proxyPort));
        //SSL
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, new TrustManager[] { new TrustAnyTrustManager() }, new java.security.SecureRandom());
        //RETURN
        StringBuffer res = new StringBuffer();
        InputStream in = null;
        HttpsURLConnection con = null;
        BufferedReader reader = null;
        try {
            con = (HttpsURLConnection) (new URL(url)).openConnection(proxy);
            con.setDoOutput(true);
            con.setUseCaches(false);
            if (cookiestring != null && !"".equals(cookiestring.trim())) {
                con.addRequestProperty("JSESSIONID", cookiestring);
                con.addRequestProperty("Cookie", cookiestring);
            }
            con.setSSLSocketFactory(sc.getSocketFactory());
            con.setHostnameVerifier(new TrustAnyHostnameVerifier());
            con.connect();
            in = con.getInputStream();
            //判断是否压缩
            String ContentEncoding = con.getHeaderField("Content-Encoding");
            if ("gzip".equalsIgnoreCase(ContentEncoding)) {
                reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(in), charset));
            }
            else {
                reader = new BufferedReader(new InputStreamReader(in, charset));
            }
            String lineTxt = null;
            while ((lineTxt = reader.readLine()) != null) {
                res.append(lineTxt);
            }
        }
        catch (Exception e) {
            res = new StringBuffer();
        }
        finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            }
            catch (Exception e) {
            }
            try {
                if (in != null) {
                    in.close();
                }
            }
            catch (Exception e) {
            }
        }
        return res.toString();
    }

    /**
     * https post方式请求服务器。传入MAP
     * @param url
     * @param content
     * @param charset
     * @param requestproperty
     * @return
     * @throws NoSuchAlgorithmException
     * @throws KeyManagementException
     * @throws IOException
     * @time 2014年8月30日 下午2:57:46
     * @author yinshubin
     */
    public static String post1(String url, String content, String charset, Map<String, String> requestproperty)
            throws NoSuchAlgorithmException, KeyManagementException, IOException {
        StringBuffer responseMessage = null;
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, new TrustManager[] { new TrustAnyTrustManager() }, new java.security.SecureRandom());

        URL console = new URL(url);
        URLConnection con = console.openConnection();
        //        con.addRequestProperty("JSESSIONID", cookiestring);
        //        con.addRequestProperty("Cookie", cookiestring);
        Iterator iter = requestproperty.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry<String, String> entry = (Map.Entry<String, String>) iter.next();
            con.addRequestProperty(entry.getKey(), entry.getValue());
        }
        HttpsURLConnection conn = (HttpsURLConnection) con;
        conn.setSSLSocketFactory(sc.getSocketFactory());
        conn.setHostnameVerifier(new TrustAnyHostnameVerifier());
        conn.setDoOutput(true);
        conn.connect();
        DataOutputStream out = new DataOutputStream(conn.getOutputStream());
        out.write(content.getBytes(charset));
        // 刷新、关闭
        out.flush();
        out.close();
        InputStream is = conn.getInputStream();
        if (is != null) {
            //            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            //            byte[] buffer = new byte[1024];
            //            int len = 0;
            //            while ((len = is.read(buffer)) != -1) {
            //                outStream.write(buffer, 0, len);
            //            }
            //            is.close();

            int charCount = -1;
            BufferedReader br = null;
            responseMessage = new StringBuffer();
            String ContentEncoding = conn.getHeaderField("Content-Encoding");
            if ("gzip".equals(ContentEncoding)) {
                br = new BufferedReader(new InputStreamReader(new GZIPInputStream(is), "UTF-8"));
            }
            else {
                br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            }
            try {
                while ((charCount = br.read()) != -1) {
                    responseMessage.append((char) charCount);
                }
            }
            catch (Exception e) {
            }
            List<String> s = conn.getHeaderFields().get("Set-Cookie");
            try {
                String cookiestring = getCookie(s.toString());
                return responseMessage.toString() + "拿到新Cookie" + cookiestring;
            }
            catch (Exception e) {
            }
        }
        return responseMessage.toString();
    }

    /**
     * https post方式请求服务器。传入MAP
     * @param url
     * @param content
     * @param charset
     * @param requestproperty
     * @return
     * @throws NoSuchAlgorithmException
     * @throws KeyManagementException
     * @throws IOException
     * @time 2014年8月30日 下午2:57:46
     * @author yinshubin
     */
    public static String post2(String url, String content, String charset, Map<String, String> requestproperty)
            throws NoSuchAlgorithmException, KeyManagementException, IOException {
        StringBuffer responseMessage = null;
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, new TrustManager[] { new TrustAnyTrustManager() }, new java.security.SecureRandom());

        URL console = new URL(url);
        URLConnection con = console.openConnection();
        //        con.addRequestProperty("JSESSIONID", cookiestring);
        //        con.addRequestProperty("Cookie", cookiestring);
        Iterator iter = requestproperty.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry<String, String> entry = (Map.Entry<String, String>) iter.next();
            con.addRequestProperty(entry.getKey(), entry.getValue());
        }
        HttpsURLConnection conn = (HttpsURLConnection) con;
        conn.setSSLSocketFactory(sc.getSocketFactory());
        conn.setHostnameVerifier(new TrustAnyHostnameVerifier());
        conn.setDoOutput(true);
        conn.connect();
        DataOutputStream out = new DataOutputStream(conn.getOutputStream());
        out.write(content.getBytes(charset));
        // 刷新、关闭
        out.flush();
        out.close();
        InputStream is = conn.getInputStream();
        if (is != null) {
            //            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            //            byte[] buffer = new byte[1024];
            //            int len = 0;
            //            while ((len = is.read(buffer)) != -1) {
            //                outStream.write(buffer, 0, len);
            //            }
            //            is.close();

            int charCount = -1;
            BufferedReader br = null;
            responseMessage = new StringBuffer();
            String ContentEncoding = conn.getHeaderField("Content-Encoding");
            if ("gzip".equals(ContentEncoding)) {
                br = new BufferedReader(new InputStreamReader(new GZIPInputStream(is), "UTF-8"));
            }
            else {
                br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            }
            try {
                while ((charCount = br.read()) != -1) {
                    responseMessage.append((char) charCount);
                }
            }
            catch (Exception e) {
            }
        }
        return responseMessage.toString();
    }

    /**
     * java.net实现 HTTP或HTTPs GET方法提交
     * @param strUrl
     * @return
     * @time 2014年8月30日 下午2:59:27
     * @author yinshubin
     */
    public static String Get(String strUrl) {
        URLConnection connection = null;
        BufferedReader reader = null;
        String str = null;
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, new TrustManager[] { new TrustAnyTrustManager() }, new java.security.SecureRandom());

            URL url = new URL(strUrl);
            connection = url.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(false);
            HttpsURLConnection conn = (HttpsURLConnection) connection;
            conn.setSSLSocketFactory(sc.getSocketFactory());

            conn.setHostnameVerifier(new TrustAnyHostnameVerifier());
            conn.setDoOutput(true);
            reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String lines;
            StringBuffer linebuff = new StringBuffer("");
            try {
                while ((lines = reader.readLine()) != null) {
                    linebuff.append(lines);
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            str = linebuff.toString();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            try {
                reader.close();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return str;
    }

    /**
     * java.net实现 HTTP或HTTPs GET方法提交,变体
     * @param strUrl
     * @return
     * @time 2014年8月30日 下午2:59:53
     * @author yinshubin
     */
    public static String Get1(String strUrl) {
        StringBuffer responseMessage = null;
        URLConnection connection = null;
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, new TrustManager[] { new TrustAnyTrustManager() }, new java.security.SecureRandom());

            URL url = new URL(strUrl);
            connection = url.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(false);
            HttpsURLConnection conn = (HttpsURLConnection) connection;
            conn.setSSLSocketFactory(sc.getSocketFactory());

            conn.setHostnameVerifier(new TrustAnyHostnameVerifier());
            conn.setDoOutput(true);
            InputStream is = conn.getInputStream();
            if (is != null) {
                int charCount = -1;
                BufferedReader br = null;
                responseMessage = new StringBuffer();
                String ContentEncoding = conn.getHeaderField("Content-Encoding");
                if ("gzip".equals(ContentEncoding)) {
                    br = new BufferedReader(new InputStreamReader(new GZIPInputStream(is), "UTF-8"));
                }
                else {
                    br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                }
                try {
                    while ((charCount = br.read()) != -1) {
                        responseMessage.append((char) charCount);
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return responseMessage.toString();
    }

    /**
     * 说明：拼接COOKIE
     * @param str
     * @return
     * @time 2014年8月30日 下午2:57:08
     * @author yinshubin
     */
    public static String getCookie(String str) {
        if (str.contains("BIGipServerotn")) {
            String[] strs = str.split("; path=/,");
            strs[0] = strs[0].substring(1, strs[0].length());
            if (strs[1].contains("current_captcha_type")) {
                String[] strs2 = strs[1].split("; Path=/,");
                String[] strs3 = strs2[1].split(";");
                str = strs3[0] + "; " + strs[0] + ";" + strs2[0];
            }
            else {
                String[] strs2 = strs[1].split(";");
                strs2[0] = strs2[0].substring(1, strs2[0].length());
                str = strs2[0] + "; " + strs[0];
            }
        }
        else if (str.contains("JSESSIONID")) {
            //            [JSESSIONID=9EF9F5BE7618AF2E207FD54C95CC3B9D; Path=/otn]
            String[] strs = str.split(";");
            str = strs[0].substring(1, strs[0].length());
        }
        return str;
    }

}
