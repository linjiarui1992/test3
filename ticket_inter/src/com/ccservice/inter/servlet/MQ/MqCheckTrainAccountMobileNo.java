package com.ccservice.inter.servlet.MQ;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.policy.TimeUtil;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.Util.JobTrainUtil;
import com.ccservice.inter.job.train.JobIndexAgain_RepUtil;
import com.ccservice.inter.job.train.account.Account12306Server;
import com.ccservice.inter.job.train.account.thread.Thread12306AccountCheckMobileNewThread;
import com.ccservice.inter.server.ServerQueueMQ;

/**
 * 
 * @time 2015年10月17日 下午1:45:42
 * @author chendong
 */
public class MqCheckTrainAccountMobileNo implements MessageListener {
    public static void main(String[] args) {
        String brokerURL = "tcp://120.26.100.206:61616/";
        //        String brokerURL = "tcp://121.199.25.199:61616/";
        String name = "MQ_Account_checkMobileNo?consumer.prefetchSize=1";
        initMessageConsumer(1, brokerURL, name);
        //        JSONObject jsonObject_init = JobTrainUtil.initQueryUserInfo(
        //                "JSESSIONID=0A01D96360C6C900B4CF93210F4DD4F3B3558A5CEE; BIGipServerotn=1675165962.24610.0000; current_captcha_type=Z");
        //        System.out.println(jsonObject_init);
        //        String brokerURL = "tcp://192.168.0.5:61616/";
        //        String name = "MQ_Account_checkMobileNo?consumer.prefetchSize=1";
        //        String name = "chendong_test1?consumer.prefetchSize=1";
        //        MqCheckTrainAccountMobileNo.initMessageConsumer(2, brokerURL, name);
        try {
            //            ServerQueueMQ.MqCheckTrainAccountMobileNoList.get(0).close();
            //            ServerQueueMQ.MqCheckTrainAccountMobileNoList.remove(0);
            //            ServerQueueMQ.MqCheckTrainAccountMobileNoList.get(0).close();
            //            ServerQueueMQ.MqCheckTrainAccountMobileNoList.remove(0);
            //            ServerQueueMQ.MqCheckTrainAccountMobileNoList.get(0).close();
            //            ServerQueueMQ.MqCheckTrainAccountMobileNoList.remove(0);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMessage(Message message) {
        String messageString = getMessage(message);
        System.out.println(TimeUtil.gettodaydate(5) + ":" + messageString);
        if (messageString != null) {
            try {
                JSONObject jsonObject = JSONObject.parseObject(messageString);
                String loginname = jsonObject.getString("LoginName");
                String password = jsonObject.getString("password");
                String cookieString = jsonObject.getString("cookieString");
                String orderid = jsonObject.getString("orderid");
                checkMobileThread(loginname, password, cookieString, orderid);
            }
            catch (Exception e) {
            }
        }
    }

    /**
     * 
     * @time 2015年10月17日 下午2:02:43
     * @author chendong
     * @param orderid 
     * @param cookieString 
     * @param password 
     * @param loginname 
     */
    private void checkMobileThread(String loginname, String password, String cookieString, String orderid) {
        //        String repUrl = PropertyUtil.getValue("repUrl", "train.checkMobile.properties");//"http://localhost:9016/Reptile/traininit";
        String repUrl = JobIndexAgain_RepUtil.getInstance().getRepUrl(false);
        String serviceurl = PropertyUtil.getValue("serviceurl", "train.checkMobile.properties");
        String mobileCodeType = PropertyUtil.getValue("mobileCodeType", "train.checkMobile.properties");
        JSONObject jsonObject_init = JobTrainUtil.initQueryUserInfo(cookieString);
        //                JSESSIONID=0A01D96360C6C900B4CF93210F4DD4F3B3558A5CEE; BIGipServerotn=1675165962.24610.0000; current_captcha_type=Z
        if (jsonObject_init.toJSONString().contains("未登录")) {
            cookieString = JobTrainUtil.getCookie(repUrl, loginname, password);
        }
        System.out.println(loginname + ":>" + jsonObject_init);
        Thread12306AccountCheckMobileNewThread job12306AccountCheckMobileNewThread = new Thread12306AccountCheckMobileNewThread(
                orderid, loginname, password, repUrl, serviceurl, cookieString, mobileCodeType);
        //        job12306AccountCheckMobileNewThread.start();
        try {
            job12306AccountCheckMobileNewThread.checkMobile();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        Account12306Server.dangqiancheckMobileCount--;
        System.out.println(loginname + ":当前线程结束,总核验线程数:" + Account12306Server.dangqiancheckMobileCount);

    }

    /**
     * 
     * @return
     * @time 2015年10月17日 下午1:53:07
     * @author chendong
     * @param message 
     */
    private String getMessage(Message message) {
        String messageString = null;
        try {
            messageString = ((TextMessage) message).getText();
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
        return messageString;
    }

    /**
     * 初始化N个消费者
     * 
     * @param CosumerCount 多少个消费者
     * @param brokerURL "tcp://192.168.0.5:61616/"
     * @param name "chendong_test1?consumer.prefetchSize=1"
     * @time 2015年10月17日 下午2:52:33
     * @author chendong
     */
    public static int initMessageConsumer(int CosumerCount, String brokerURL, String name) {
        //        String brokerURL = "tcp://192.168.0.5:61616/";
        ConnectionFactory cf = new ActiveMQConnectionFactory(brokerURL);
        Connection conn = null;
        try {
            conn = cf.createConnection();
            Destination destination = new ActiveMQQueue(name);
            for (int i = 0; i < CosumerCount; i++) {
                Session session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
                MessageConsumer consumer = session.createConsumer(destination);
                ServerQueueMQ.MqCheckTrainAccountMobileNoList.add(consumer);
                consumer.setMessageListener(new MqCheckTrainAccountMobileNo());
            }
            conn.start();
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
        return ServerQueueMQ.MqCheckTrainAccountMobileNoList.size();
    }
}
