package com.ccservice.inter.servlet;

import java.util.Properties;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;

import com.aliyun.openservices.ons.api.Action;
import com.aliyun.openservices.ons.api.ConsumeContext;
import com.aliyun.openservices.ons.api.Consumer;
import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.MessageListener;
import com.aliyun.openservices.ons.api.ONSFactory;
import com.aliyun.openservices.ons.api.PropertyKeyConst;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.inter.server.ServerALiQueueMQ;
import com.ccservice.inter.server.ServerQueueMQ;
import com.ccservice.train.mqlistener.TrainALiCreateOrderMessageListener;
import com.ccservice.train.mqlistener.TrainCreateOrderMessageListener;

/**
 * http://192.168.0.5:8161/admin/
 * @time 2015年1月24日 下午6:02:07
 * @author chendong
 */
@SuppressWarnings("serial")
public class QueueMQ_TrainCreateOrder extends HttpServlet {

    private String mqaddress = "";// MQ地址

    private String mqusername = "";// MQ 用户名

    private String isstart = "";// 是否开启

    private int createordernum = 153;

    // 阿里云初始化参数 zhangqifei
    private static String topicName = PropertyUtil.getValue("topicName", "train.properties");// topicName

    private static String consumerId = PropertyUtil.getValue("consumerId", "train.properties");

    private static String accessKey = PropertyUtil.getValue("AccessKey", "train.properties");

    private static String secretKey = PropertyUtil.getValue("SecretKey", "train.properties");

    @Override
    public void init() throws ServletException {
        super.init();
        this.mqaddress = this.getInitParameter("mqaddress");
        this.mqusername = this.getInitParameter("mqusername");
        this.isstart = this.getInitParameter("isstart");

        String temp_createordernum = this.getInitParameter("createordernum");
        try {
            this.createordernum = Integer.parseInt(temp_createordernum);
        }
        catch (Exception e) {
            this.createordernum = 100;
        }
        if ("1".equals(isstart)) {
            System.out.println("下单队列:开启");
            String type = PropertyUtil.getValue("type", "train.properties");
            int temp = Integer.parseInt(type);
            if (temp == 0) {
                orderNotice();
            }
            else if (temp == 1) {
                orderNoticeALi();
            }
        }
    }

    public void orderNotice() {
        ConnectionFactory cf = new ActiveMQConnectionFactory(mqaddress);
        Connection conn = null;
        Session session = null;
        try {
            conn = cf.createConnection();
            Destination destination = new ActiveMQQueue(mqusername);
            for (int i = 0; i < this.createordernum; i++) {
                session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
                MessageConsumer consumer = session.createConsumer(destination);
                consumer.setMessageListener(new TrainCreateOrderMessageListener());
            }
            conn.start();
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
    }

    public void orderNoticeALi() {
        Properties properties = new Properties();
        properties.put(PropertyKeyConst.ConsumerId, consumerId);// 您在MQ控制台创建的Consumer
        properties.put(PropertyKeyConst.AccessKey, accessKey);// 鉴权用AccessKey，在阿里云服务器管理控制台创建
        properties.put(PropertyKeyConst.SecretKey, secretKey);// 鉴权用SecretKey，在阿里云服务器管理控制台创建
        Consumer consumer = ONSFactory.createConsumer(properties);
        for (int i = 0; i < this.createordernum; i++) {
            consumer.subscribe(topicName, "*", new TrainALiCreateOrderMessageListener());
        }
        consumer.start();
    }

    /**
     * 初始化N个消费者
     * 
     * @param CosumerCount
     *            多少个消费者
     * @param brokerURL
     *            "tcp://192.168.0.5:61616/"
     * @param name
     *            "chendong_test1?consumer.prefetchSize=1"
     * @time 2015年10月17日 下午2:52:33
     * @author chendong
     */
    public static int initMessageConsumer(int CosumerCount, String brokerURL, String name) {
        String type = PropertyUtil.getValue("type", "train.properties");
        int temp = Integer.parseInt(type);
        if (temp == 0) {
            return initMessageConsumerActive(CosumerCount, brokerURL, name);
        }
        else if (temp == 1) {
            return initMessageConsumerALi(CosumerCount, brokerURL, name);
        }
        else {
            return 0;
        }
    }

    /**
     * Active初始化N个消费者
     * 
     * @param CosumerCount
     *            多少个消费者
     * @param brokerURL
     *            "tcp://192.168.0.5:61616/"
     * @param name
     *            "chendong_test1?consumer.prefetchSize=1"
     * @time 2015年10月17日 下午2:52:33
     * @author chendong
     */
    public static int initMessageConsumerActive(int CosumerCount, String brokerURL, String name) {
        ConnectionFactory cf = new ActiveMQConnectionFactory(brokerURL);
        Connection conn = null;
        try {
            conn = cf.createConnection();
            Destination destination = new ActiveMQQueue(name);
            for (int i = 0; i < CosumerCount; i++) {
                Session session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
                MessageConsumer consumer = session.createConsumer(destination);
                ServerQueueMQ.MqTrainCreateOrderList.add(consumer);
                consumer.setMessageListener(new TrainCreateOrderMessageListener());
            }
            conn.start();
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
        return ServerQueueMQ.MqTrainCreateOrderList.size();
    }

    /**
     * 清理掉消费者 
     * @param CloseCosumerCount
     *            多少个消费者
     * @time 2015年12月10日16:59:34
     * @author chendong
     */
    public static int closeMessageConsumer(int CloseCosumerCount) {
        String type = PropertyUtil.getValue("type", "train.properties");
        int temp = Integer.parseInt(type);
        if (temp == 1) {
            return closeMessageConsumerALi(CloseCosumerCount);
        }
        else {
            return closeMessageConsumerActive(CloseCosumerCount);
        }
    }

    /**
     * ALi初始化N个消费者
     * 
     * @param CosumerCount
     *            多少个消费者
     * @param brokerURL
     *            "tcp://192.168.0.5:61616/"
     * @param name
     *            "chendong_test1?consumer.prefetchSize=1"
     * @time 2015年10月17日 下午2:52:33
     * @author chendong
     */
    public static int initMessageConsumerALi(int CosumerCount, String brokerURL, String name) {
        Properties properties = new Properties();
        properties.put(PropertyKeyConst.ConsumerId, consumerId);// 您在MQ控制台创建的Consumer
        properties.put(PropertyKeyConst.AccessKey, accessKey);// 鉴权用AccessKey，在阿里云服务器管理控制台创建
        properties.put(PropertyKeyConst.SecretKey, secretKey);// 鉴权用SecretKey，在阿里云服务器管理控制台创建
        Consumer consumer = ONSFactory.createConsumer(properties);
        try {
            for (int i = 0; i < CosumerCount; i++) {
                ServerALiQueueMQ.MqTrainCreateOrderList.add(consumer);
                consumer.subscribe(topicName, "*", new TrainALiCreateOrderMessageListener());
            }
            consumer.start();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(ServerALiQueueMQ.MqTrainCreateOrderList.size());
        return ServerALiQueueMQ.MqTrainCreateOrderList.size();
    }

    /**
     * Active清理掉消费者
     * 
     * @param CloseCosumerCount
     *            多少个消费者
     * @time 2015年12月10日16:59:34
     * @author chendong
     */
    public static int closeMessageConsumerActive(int CloseCosumerCount) {
        for (int i = 0; i < CloseCosumerCount; i++) {
            if (ServerQueueMQ.MqTrainCreateOrderList.size() > 0) {
                try {
                    for (int j = 0; j < ServerQueueMQ.MqTrainCreateOrderList.size(); j++) {
                        MessageConsumer messageConsumer = ServerQueueMQ.MqTrainCreateOrderList.get(j);
                        TrainALiCreateOrderMessageListener messageListener = (TrainALiCreateOrderMessageListener) messageConsumer
                                .getMessageListener();
                        if (messageListener.isUse()) {
                            continue;
                        }
                        else {
                            // System.out.println("用了吗?" +
                            // messageListener.isUse());
                            // String MessageSelector =
                            // messageConsumer.getMessageSelector();
                            messageConsumer.close();
                            // System.out.println(MessageSelector);
                            ServerQueueMQ.MqTrainCreateOrderList.remove(j);
                            break;
                        }
                    }
                }
                catch (JMSException e) {
                    e.printStackTrace();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ServerQueueMQ.MqTrainCreateOrderList.size();
    }

    /**
     * ALi清理掉消费者
     * 
     * @param CloseCosumerCount
     *            多少个消费者
     * @time 2015年12月10日16:59:34
     * @author chendong
     */
    public static int closeMessageConsumerALi(int CloseCosumerCount) {
        // for (int i = 0; i < CloseCosumerCount; i++) {
        // if (ServerALiQueueMQ.MqTrainCreateOrderList.size() > 0) {
        // try {
        // for (int j = 0; j < ServerALiQueueMQ.MqTrainCreateOrderList
        // .size(); j++) {
        Consumer consumer = ServerALiQueueMQ.MqTrainCreateOrderList.get(0);
        consumer.unsubscribe(topicName);
        // if (messageListener.isUse()) {
        // continue;
        // } else {
        // // System.out.println("用了吗?" +
        // // messageListener.isUse());
        // // String MessageSelector =
        // // messageConsumer.getMessageSelector();
        // consumer.start();
        // // System.out.println(MessageSelector);
        // ServerALiQueueMQ.MqTrainCreateOrderList.remove(j);
        // break;
        // }
        // }
        // } catch (JMSException e) {
        // e.printStackTrace();
        // } catch (Exception e) {
        // e.printStackTrace();
        // }
        // }
        // }
        return ServerQueueMQ.MqTrainCreateOrderList.size();
    }

    public static void main(String[] args) {
        Properties properties = new Properties();
        properties.put(PropertyKeyConst.ConsumerId, consumerId);
        properties.put(PropertyKeyConst.AccessKey, accessKey);
        properties.put(PropertyKeyConst.SecretKey, secretKey);
        System.out.println(consumerId);
        System.out.println(accessKey);
        System.out.println(secretKey);
        Consumer consumer = ONSFactory.createConsumer(properties);
        System.out.println(topicName);
        consumer.subscribe(topicName, "*", new MessageListener() {
            public Action consume(Message message, ConsumeContext context) {
                System.out.println("Receive: " + message);
                return Action.CommitMessage;
            }
        });
        consumer.start();
        System.out.println("Consumer Started");
    }
}
