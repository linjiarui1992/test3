package com.ccservice.inter.servlet;

import java.util.Random;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.inter.server.Server;

/**
 * http://192.168.0.5:8161/admin/
 * 
 * @time 2015年1月24日 下午6:02:07
 * @author chendong
 */
public class QueueMQ_Customeruser12306 extends HttpServlet {
    private String isstart;

    private String mqaddress = "";// MQ地址

    private String mqusername = "";// MQ 用户名

    private String orderNoticeResult = "";// 纵横天地从MQ中接收到得信息

    @Override
    public void init() throws ServletException {
        super.init();
        mqaddress = this.getInitParameter("mqaddress");
        mqusername = this.getInitParameter("mqusername");
        this.isstart = this.getInitParameter("isstart");
        if ("1".equals(this.isstart)) {
            System.out.println("12306账号update失败处理:开启");
            orderNotice();
        }
    }

    public void orderNotice() {
        ConnectionFactory cf = new ActiveMQConnectionFactory(mqaddress);
        Connection conn = null;
        Session session = null;
        try {
            conn = cf.createConnection();
            session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Destination destination = new ActiveMQQueue(mqusername);
            MessageConsumer consumer = session.createConsumer(destination);
            conn.start();
            consumer.setMessageListener(new MessageListener() {
                @Override
                public void onMessage(Message message) {
                    try {
                        int r1 = new Random().nextInt(10000000);
                        orderNoticeResult = ((TextMessage) message).getText();
                        JSONObject jsonobject = JSONObject.parseObject(orderNoticeResult);
                        String type = jsonobject.getString("type");//1更新12306账号状态
                        //                        Customeruser cust = jsonobject.getObject("customeruserObject", Customeruser.class);
                        JSONObject customeruserObject = jsonobject.getJSONObject("customeruserObject");
                        String enname = customeruserObject.getString("enname");
                        long id = customeruserObject.getLongValue("id");
                        String sql = "UPDATE T_CUSTOMERUSER SET C_ENNAME='" + enname + "' where id=" + id;
                        int counti = Server.getInstance().getSystemService().excuteAdvertisementBySql(sql);
                        System.out.println("err_user_id:" + id + ":enname:" + enname + ":count_i:" + counti);
                    }
                    catch (JMSException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {

        JSONObject jsonobject = JSONObject
                .parseObject("{'customeruserObject':{'agentid':47,'cardnunber':'JSESSIONID=895BCD940FF80A0A78C5C7149C9FB3E6; BIGipServerotn=1742274826.64545.0000; current_captcha_type=C','createtime':1422753560813,'createuser':'HTHY','description':'添加乘客林宇杰未通过身份效验450821199707013412','enname':'1','id':149813,'interticketnum':0,'isenable':0,'limitinterticketnum':0,'limitticketnum':0,'loginname':'tc000106076','loginnum':4,'logpassword':'asd123456','memberemail':'http://10.252.128.88:8080/Reptile/traininit','memberfax':'tc000106147@mlzg99.com','membermobile':'430682196311201032','membername':'袁神祥','modifytime':1422834226923,'modifyuser':'HTHY','state':1,'ticketnum':1041691,'type':4}}");
        String type = jsonobject.getString("type");//1更新12306账号状态
        Customeruser cust = jsonobject.getObject("customeruserObject", Customeruser.class);
        Server.getInstance().getMemberService().updateCustomeruser(cust);
    }
}
