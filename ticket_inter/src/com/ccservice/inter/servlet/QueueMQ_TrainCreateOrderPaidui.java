package com.ccservice.inter.servlet;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.servlet.http.HttpServlet;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;

import com.ccservice.inter.server.ServerQueueMQ;
import com.ccservice.train.mqlistener.TrainCreateOrderPaiduiMessageListener;

/**
 * http://192.168.0.5:8161/admin/
 * 
 * 
 * /ticket_inter/WebContent/mq/MqTrainCreateOrderPaidui.jsp?type=100
 * @time 2015年12月28日 下午17:38:07
 * @author fanchangwei
 */
@SuppressWarnings("serial")
public class QueueMQ_TrainCreateOrderPaidui extends HttpServlet {
    public static void main(String[] args) {
        String brokerURL = "tcp://120.26.100.206:61616/";
        String name = "QueueMQ_trainorder_waitorder_orderid_PaiDui?consumer.prefetchSize=1";
        MessageConsumer(1, brokerURL, name);
    }

    /**
    * 消费者排队
    * @param CosumerCount 多个排队消费者
    * @param brokerURL "tcp://192.168.0.5:61616/"
    * @time 2015年12月28日 下午17:38:00
    * @author fanchangwei
    */
    private static final long serialVersionUID = 1L;

    public static int MessageConsumer(int CosumerCount, String brokerURL, String name) {
        ConnectionFactory cf = new ActiveMQConnectionFactory(brokerURL);
        Connection conn = null;
        try {
            conn = cf.createConnection();
            Destination destination = new ActiveMQQueue(name);
            for (int i = 0; i < CosumerCount; i++) {
                Session session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
                MessageConsumer consumer = session.createConsumer(destination);
                ServerQueueMQ.MqTrainCreateOrderListpaidui.add(consumer);
                consumer.setMessageListener(new TrainCreateOrderPaiduiMessageListener());//监听
            }
            conn.start();
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
        return ServerQueueMQ.MqTrainCreateOrderListpaidui.size();
    }

    /**
     * 清理掉消费者
     * 
     * @param CloseCosumerCount 多少个消费者
     * @time 2015年12月10日16:59:34
     * @author chendong
     */
    public static int closeMessageConsumer(int CloseCosumerCount) {
        for (int i = 0; i < CloseCosumerCount; i++) {
            if (ServerQueueMQ.MqTrainCreateOrderListpaidui.size() > 0) {
                try {
                    ServerQueueMQ.MqTrainCreateOrderListpaidui.get(0).close();
                    ServerQueueMQ.MqTrainCreateOrderListpaidui.remove(0);
                }
                catch (JMSException e) {
                    e.printStackTrace();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ServerQueueMQ.MqTrainCreateOrderListpaidui.size();
    }

}
