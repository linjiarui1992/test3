package com.ccservice.inter.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ccservice.inter.job.WriteLog;
import com.ccservice.train.order.CallBackMethodResultAliPayResult;
import com.ccservice.train.order.CallBackMethodResultUnionPayResult;
import com.ccservice.train.order.CallBackPayUrlResult;
import com.ccservice.train.order.PayCallBackMethod;

/**
 * wzc
 * 支付处理回调类
 */
public class PayCallBack_TrainPay extends HttpServlet {

    private static final long serialVersionUID = 1L;

    private PayCallBackMethod paycallbackmethod;

    private CallBackMethodResultAliPayResult callbackmethodresultAlipay;

    private CallBackMethodResultUnionPayResult callBackMethodResultUnion;

    private CallBackPayUrlResult callbackpayurlresult;

    @Override
    public void init() throws ServletException {
        paycallbackmethod = new PayCallBackMethod();
        callbackmethodresultAlipay = new CallBackMethodResultAliPayResult();
        callBackMethodResultUnion = new CallBackMethodResultUnionPayResult();
        callbackpayurlresult = new CallBackPayUrlResult();
    }

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = null;
        response.setCharacterEncoding("UTF-8");
        request.setCharacterEncoding("UTF-8");
        int t = new Random().nextInt(100000);
        out = response.getWriter();
        String data = request.getParameter("data");
        String cmd = request.getParameter("cmd");
        String orderid = request.getParameter("orderid");
        WriteLog.write("pay_TrainPay_paycallback_servlet",
                "[" + request.getRemoteAddr() + "]" + t + ",orderid=" + orderid + ":cmd=" + cmd + ":请求:data=" + data);
        String backstr = "";
        if (data != null) {
            if ("paymothod".equals(cmd)) {//支付地址回调
                backstr = paycallbackmethod.parseOrderMethod(data, t);
            }
            else if ("payresult".equals(cmd)) {//支付结果回调
                long orderidlong = Long.valueOf(orderid);
                String payset = request.getParameter("payset");
                String paymethodtype = request.getParameter("paymethodtype");
                // 真实的支付方式, 1民生 2 支付宝，3工商 4 手机支付  5 中国银行  6 交通银行
                String payMethodNew = request.getParameter("payMethodNew") == null ? "1" : request.getParameter("payMethodNew");
                if (paymethodtype != null && "1".equals(paymethodtype)) {
                    backstr = callBackMethodResultUnion.parsePayResult(data, orderidlong, t, payset, paymethodtype, payMethodNew);
                }
                else {
                    backstr = callbackmethodresultAlipay.parsePayResult(data, orderidlong, t, payset, paymethodtype, payMethodNew);
                }
            }
            else if ("payurl".equals(cmd)) {//支付链接回调
                long orderidlong = Long.valueOf(orderid);
                backstr = callbackpayurlresult.parsePayUrlResult(data, orderidlong, t);
            }
            else if ("timeout".equals(cmd)) {//超时处理
                long orderidlong = Long.valueOf(orderid);
                backstr = callbackpayurlresult.payTimeOut(data, orderidlong, t);

            }
        }
        WriteLog.write("pay_TrainPay_paycallback_servlet", t + ":" + cmd + ":响应:" + backstr);
        out.print(backstr);
        out.flush();
        out.close();
    }
}
