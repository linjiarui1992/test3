package com.ccservice.inter.servlet;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;





import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.callback.SendPostandGet;
import com.ccservice.b2b2c.base.mailaddress.MailAddress;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainOrderOffline.TrainOrderOffline;
import com.ccservice.b2b2c.base.trainTicketOffline.TrainTicketOffline;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.b2b2c.base.trainpassengerOffline.TrainPassengerOffline;
import com.ccservice.b2b2c.util.DBoperationUtil;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.offline.util.DelieveUtils;
import com.ccservice.qunar.util.ExceptionUtil;

/**
 * 同程抢票转线下_下单 
 * wangdiao 
 * 2016-12-1
 * 
 */
public class OfflineTicketNumberOrder extends Thread {
	private static final long serialVersionUID = 1L;

	private String certNoValue, certTypeValue, nameValue, birthday,
			trainEndTimeValue, seatValue_Key, ticketNewId,
			trainNoValue, trainStartTimeValue,extSeatValue;
	private String ticketId = "";
	private int ticketTypeValue;// 用于判断票种 类型
	private float ticketPayValue = 0f;//
	private DBoperationUtil dt = new DBoperationUtil();
	JSONObject jsonStr = new JSONObject();
	public OfflineTicketNumberOrder(JSONObject jsonStr) {
		this.jsonStr = jsonStr;
	}

	/**
	 * 传入的参数进行入库
	 * "arrStation": "呼和浩特",//到达站
	 * "dptStation": "乌海",//出发站
	 * "startDateTime": "2016-05-26 07:10:00",//出发时间
	 * "endDateTime": "2016-05-26 12:42:00",//到达时间
	 * "trainNo":"G106",//车次
	 * "orderDate": "2016-11-26 17:22:36",//下单时间
	 * "orderNo": "qccfs161126172236efd",//订单号
	 * "endTime":"2016-11-30 18:00",//截止出票时间	
	 * "transportAddress": "内蒙古自治区乌海市海勃湾区方圆新村B区8号楼三单元302",//邮寄地址
	 * "transportName": "杨文勇",//邮寄联系人
	 * "transportPhone": "13848319564"//邮寄联系电话
	 * @param jsonStr
	 * @author wangdiao
	 * 
	 */
	@Override
	public void run() {
		WriteLog.write("同程抢票线下下单_JSON", "json:" + jsonStr);
		String dptStationValue = jsonStr.getString("dptStation");// 始发站
		String arrStationValue = jsonStr.getString("arrStation");// 终点站
		String orderDate = jsonStr.getString("orderDate");// 下单时间
		String orderNoValue = jsonStr.getString("orderNo");// 接口订单号
		String endTime = jsonStr.getString("endTime");// 出票截至时间
		String transportAddress = jsonStr.getString("transportAddress");// 邮寄地址
		String transportName = jsonStr.getString("transportName");// 邮寄联系人
		String transportPhone = jsonStr.getString("transportPhone");// 邮寄联系电话
		
			trainNoValue = jsonStr.getString("trainNo");// 火车编号（车次）
			String costTime = "";
			trainStartTimeValue = jsonStr.getString("startDateTime");// 火车出发时间
			DateFormat df = new SimpleDateFormat("yy-MM-dd HH:mm");
			Date date_trainStartTime = new Date();
			try {
				date_trainStartTime = df.parse(trainStartTimeValue);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			trainEndTimeValue = jsonStr.getString("endDateTime");// 火车到达时间
			Date date_trainEndTime = new Date();

			try {
				date_trainEndTime = df.parse(trainEndTimeValue);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			long endMilliSeconds = date_trainEndTime.getTime();
			DateFormat dfm = new SimpleDateFormat("HH:mm");
			trainEndTimeValue = dfm.format(date_trainEndTime);

			long startMilliSeconds = date_trainStartTime.getTime();
			int costtime = (int) ((endMilliSeconds - startMilliSeconds) / 1000);
			costTime = String.valueOf(costtime / (60 * 60)) + ":"
					+ String.valueOf((costtime % (60 * 60)) / 60);// 历时
			/**
			 * 获取 乘客信息
			 */
			JSONArray passengers = jsonStr.getJSONArray("passengers");// 乘客信息
			Trainorder trainorder = new Trainorder();// 创建订单
			trainorder.setOrdertype(2);// 订单类型（1、线上 2、线下）默认为1
			List<Trainpassenger> trainplist = new ArrayList<Trainpassenger>();
			for (int i = 0; i < passengers.size(); i++) {
				List<Trainticket> traintickets = new ArrayList<Trainticket>();
				JSONObject infopassengers = passengers.getJSONObject(i);
				certNoValue = infopassengers.getString("certNo");// 乘车人证件号码
				// 判断证件类型： 1：二代身份证；C：港澳通行证；G：台湾通行证；B：护照
				certTypeValue = infopassengers.getString("certType");// 乘车人证件种类
				ticketId = infopassengers.getString("ticketId");// 乘客id
				if ("护照".equals(certTypeValue)) {
					certTypeValue = "3";
				} else if ("二代身份证".equals(certTypeValue)) {
					certTypeValue = "1";
				} else if ("港澳通行证".equals(certTypeValue)) {
					certTypeValue = "4";
				} else if ("台湾通行证".equals(certTypeValue)) {
					certTypeValue = "5";
				}
				if (certNoValue.length() == 18) {
					String year = certNoValue.substring(6, 10);
					String month = certNoValue.substring(10, 12);
					String day = certNoValue.substring(12, 14);
					birthday = year + "-" + month + "-" + day;
				}
				nameValue = infopassengers.getString("name");// 乘车人姓名
				if (infopassengers.containsKey("ticketType")) {
					// 票种类型，1：成人票；0：儿童票 ；2:学生票；3:残军票
					String tType = infopassengers.getString("ticketType");
					if ("儿童票".equals(tType)) {
						ticketTypeValue = 0;
					} else if ("成人票".equals(tType)) {
						ticketTypeValue = 1;
					} else if ("学生票".equals(tType)) {
						ticketTypeValue = 2;
					} else if ("残军票".equals(tType)) {
						ticketTypeValue = 3;
					}
				} else {
					ticketTypeValue = 1;
				}
				ticketPayValue = (float) infopassengers.getDoubleValue("price");// 获取票价
				seatValue_Key = infopassengers.getString("seatType");// 坐席类型
				
				// 订单存入数据库订单表
				Trainticket trainticket = new Trainticket();
				Trainpassenger trainpassenger = new Trainpassenger();
				trainticket.setArrival(arrStationValue);// 存入终点站
				trainticket.setDeparture(dptStationValue);// 存入始发站
				trainticket.setStatus(Trainticket.WAITISSUE);
				trainticket.setTrainno(trainNoValue);//火车编号
				trainticket.setSeq(0);// 存入订单类型
				trainticket.setTickettype(ticketTypeValue);// 存入票种类型
				trainticket.setSeattype(seatValue_Key);// 坐席--------------------------
				trainticket.setPrice(ticketPayValue);// 火车票价格
				trainticket.setArrivaltime(trainEndTimeValue);// 火车到达时间
				trainticket.setTrainno(trainNoValue); // 火车编号
				trainticket.setSeattype(infopassengers.getString("seatType"));// 车票坐席
				trainticket.setTicketno(infopassengers.getString("ticketId"));
				trainticket.setDeparttime(trainStartTimeValue+":00");// 火车出发日期
				trainticket.setIsapplyticket(1);
				trainticket.setRefundfailreason(0);
				trainticket.setCosttime(costTime);// 历时
				trainticket.setInsurenum(0);
				trainticket.setInsurprice(0f);// 采购支付价
				trainticket.setInsurorigprice(0f);// 保险原价
				traintickets.add(trainticket);
				// 存入乘客信息
				trainpassenger.setIdnumber(certNoValue);// 存入乘车人证件号码
				trainpassenger.setIdtype(Integer.parseInt(certTypeValue));// 存入乘车人证件种类
				trainpassenger.setName(nameValue);// 存入乘车人姓名
				trainpassenger.setBirthday(birthday);
				trainpassenger.setChangeid(0);
				trainpassenger.setAduitstatus(1);
				trainpassenger.setTraintickets(traintickets);
				trainplist.add(trainpassenger);
			}
			trainorder.setTaobaosendid(seatValue_Key);
			trainorder.setOrdertype(2);// 订单类型（1、线上 2、线下）默认为1
			 //纸质票类型(0普通,1团体,2下铺,3靠窗,4连坐)
            trainorder.setPaymethod(0);
			trainorder.setQunarOrdernumber(orderNoValue);// 接口订单号
			trainorder.setContactuser(transportName);// 邮寄联系人
			trainorder.setContacttel(transportPhone);// 邮寄电话
			trainorder.setCreateuid(68l);// 自定义createUid,同城_线下多车次下单
			trainorder.setInsureadreess(transportAddress);// 邮寄地址
			trainorder.setIsjointtrip(0);
			trainorder.setAgentid(489l);// agentId=489,同cheng下单默认给虎门代售点
			trainorder.setState12306(Trainorder.WAITORDER);
			trainorder.setOrderstatus(Trainorder.WAITISSUE);
			trainorder.setAgentprofit(0f);
			trainorder.setCommission(0f);
			trainorder.setTcprocedure(0f);
			trainorder.setInterfacetype(2);
			trainorder.setOrderprice(ticketPayValue*trainplist.size());//价格乘以车票数量
			trainorder.setCreateuser("同程抢票_线下");
			Timestamp orderCreateTime = Timestamp.valueOf(orderDate);
			trainorder.setCreatetime(orderCreateTime);// 下单时间
			trainorder.setPassengers(trainplist);
			trainorder.setTicketcount(trainplist.size());// 车票数量
			try {
				WriteLog.write("同程抢票线下",trainorder.getQunarOrdernumber() + "(:)"+ trainorder.getId());
				//去线下订单下单
					WriteLog.write("同程抢票_线下", "线下");
					trainorderofflineadd(trainorder);
			} catch (Exception e) {
				WriteLog.write("MyThreadQunarOrder_LadanException_Error",jsonStr.toString());
				ExceptionUtil.writelogByException("MyThreadQunarOrder_LadanException_Error", e);
			}
		}
	/**
	 * 添加火车票线下订单
	 */
	public void trainorderofflineadd(Trainorder trainorder) {
		TrainOrderOffline trainOrderOffline = new TrainOrderOffline();
		// 有合适的出票点，分配
		String addresstemp = trainorder.getInsureadreess();
		// 通过订单邮寄地址匹配出票点
		String agentidtemp = "497";
		//distribution2(addresstemp);//同城没有代售点，指定497虎门代售点
		 WriteLog.write("线下火车票分单log记录","地址:"+addresstemp+"----->agentId:"+agentidtemp);
		trainOrderOffline.setAgentId(Long.valueOf(agentidtemp));// 同城_线下多车次下单
		trainOrderOffline.setPaystatus(1);
		trainOrderOffline.setTradeNo("");
		trainOrderOffline.setCreateUId(trainorder.getCreateuid());// //createUid
		trainOrderOffline.setCreateUser(trainorder.getCreateuser());// createuser
		trainOrderOffline.setContactUser(trainorder.getContactuser());// Contactuser
		trainOrderOffline.setContactTel(trainorder.getContacttel());//Contacttel
		trainOrderOffline.setOrderPrice(trainorder.getOrderprice());// 订单价格
		trainOrderOffline.setAgentProfit(trainorder.getAgentprofit());// 
		trainOrderOffline.setOrdernumberonline(trainorder.getQunarOrdernumber());// 接口订单号
		trainOrderOffline.setTicketCount(trainorder.getTicketcount());// 票数 
		trainOrderOffline.setPaperType(0);//纸质票类型 默认为	0 普通
		trainOrderOffline.setExtSeat(trainorder.getTaobaosendid());//获取坐席
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		trainOrderOffline.setOrderTimeOut(Timestamp.valueOf(sdf1.format(new Date())));
		Timestamp startTime = new Timestamp(new Date().getTime());
		trainOrderOffline.setOrderTimeOut(startTime);
		String sp_TrainOrderOffline_insert = dt.getCreateTrainorderOfficeProcedureSql(trainOrderOffline);
		WriteLog.write("TrainOrderOffline_insert_保存订单存储过程",sp_TrainOrderOffline_insert);
		List list = Server.getInstance().getSystemService().findMapResultByProcedure(sp_TrainOrderOffline_insert);
		Map map = (Map) list.get(0);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String orderid = map.get("id").toString();
		// 通过出票点和邮寄地址获取预计到达时间results
		String delieveStr = DelieveUtils.getDelieveStr(agentidtemp,
				trainorder.getInsureadreess());//
		String updatesql = "UPDATE TrainOrderOffline SET expressDeliver ='"
				+ delieveStr + "' WHERE ID=" + orderid;
		WriteLog.write("TrainOrderOfflineOffline_保存快递时效信息", updatesql);
		Server.getInstance().getSystemService()
				.excuteAdvertisementBySql(updatesql);
		// [TrainOrderOfflineRecord]表插入数据
		String procedureRecord = "sp_TrainOrderOfflineRecord_insert @FKTrainOrderOfflineId="+ orderid
				+ ",@ProviderAgentid="+ agentidtemp
				+ ",@DistributionTime='"+ sdf.format(new Date())
				+ "',@ResponseTime='',@DealResult=0,@RefundReason=0,@RefundReasonStr=''";
		WriteLog.write("sp_TrainOrderOfflineRecord_insert_记录表存储过程",procedureRecord);
		Server.getInstance().getSystemService().findMapResultByProcedure(procedureRecord);
		// 火车票线下订单邮寄信息
		MailAddress address = new MailAddress();
		address.setMailName(trainorder.getContactuser());
		address.setMailTel(trainorder.getContacttel());
		address.setPrintState(0);
		String insureaddress = addresstemp;
		String[] splitadd = insureaddress.split(",");
		address.setPostcode("100000");
		address.setAddress(trainorder.getInsureadreess());
		address.setOrderid(Integer.parseInt(orderid));
		address.setCreatetime(trainorder.getCreatetime());
		Date date = new Date();
		Timestamp timestamp = new Timestamp(date.getTime());
		address.setCreatetime(timestamp);
		String sp_MailAddress_insert = dt.getMailAddressProcedureSql(address);
		WriteLog.write("TrainOrderOfflineMailAddress_insert_保存邮寄地址存储过程",sp_MailAddress_insert);
		Server.getInstance().getSystemService().findMapResultByProcedure(sp_MailAddress_insert);
		for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
			// 火车票乘客线下TrainPassengerOffline
			TrainPassengerOffline trainPassengerOffline = new TrainPassengerOffline();
			trainPassengerOffline.setorderid(Long.parseLong(orderid));
			trainPassengerOffline.setname(trainpassenger.getName());
			trainPassengerOffline.setidtype(trainpassenger.getIdtype());
			trainPassengerOffline.setidnumber(trainpassenger.getIdnumber());
			trainPassengerOffline.setbirthday(trainpassenger.getBirthday());
			/**
			 * 保存乘客信息到乘客信息表中
			 */
			String sp_TrainPassengerOffline_insert = dt.getCreateTrainpassengerOfficeProcedureSql(trainPassengerOffline);
			WriteLog.write("TrainOrderOfflinePassengerOffline_insert_保存乘客存储过程",
					sp_TrainPassengerOffline_insert);
			List listP = Server.getInstance().getSystemService()
					.findMapResultByProcedure(sp_TrainPassengerOffline_insert);
			Map map2 = (Map) listP.get(0);
			String trainPid = map2.get("id").toString();//获取乘客信息表中的ID
			for (Trainticket ticket : trainpassenger.getTraintickets()) {
				// 线下火车票TrainTicketOffline
				TrainTicketOffline trainTicketOffline = new TrainTicketOffline();
				trainTicketOffline.setTrainPid(Long.parseLong(trainPid));
				trainTicketOffline.setOrderid(Long.parseLong(orderid));
				trainTicketOffline.setDepartTime(Timestamp.valueOf(ticket.getDeparttime()));
				trainTicketOffline.setDeparture(ticket.getDeparture());
				trainTicketOffline.setArrival(ticket.getArrival());
				trainTicketOffline.setTrainno(ticket.getTrainno());
				trainTicketOffline.setTicketType(ticket.getTickettype());
				trainTicketOffline.setSeatType(ticket.getSeattype());
				trainTicketOffline.setPrice(ticket.getPrice());
				trainTicketOffline.setCostTime(ticket.getCosttime());
				trainTicketOffline.setStartTime(ticket.getDeparttime().substring(11, 16));
				trainTicketOffline.setArrivalTime(ticket.getArrivaltime());
				trainTicketOffline.setSubOrderId(ticket.getTicketno());
				/**
				 * 保存车票信息到车票信息表中
				 */
				String sp_TrainTicketOffline_insert = dt.getCreateTrainticketOfficeProcedureSql(trainTicketOffline);
				WriteLog.write("TrainOrderOfflineTicketOffline_insert_保存车票存储过程",sp_TrainTicketOffline_insert);
				Server.getInstance().getSystemService().findMapResultByProcedure(sp_TrainTicketOffline_insert);
			}
		}
	}

	public String getDelieveStr(String agengId, String address) {
		String results = "";
		String fromcode = "010";
		String tocode = getExpressCodes(address);
		String time1 = "10:00:00";
		String time2 = "18:00:00";

		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		String dates = sdf.format(new Date());
		String sql1 = "SELECT fromcode,time1,time2 from TrainOrderAgentTimes where agentId="
				+ agengId;
		List list = Server.getInstance().getSystemService()
				.findMapResultBySql(sql1, null);
		if (list.size() > 0) {
			Map map = (Map) list.get(0);
			fromcode = map.get("fromcode").toString();
			time1 = map.get("time1").toString();
			time2 = map.get("time2").toString();
		}
		String realTime = getRealTimes(dates, time1, time2);
		String urlString = PropertyUtil.getValue("expressDeliverUrl",
				"train.properties");
		String param = "times=" + realTime + "&fromcode=" + fromcode
				+ "&tocode=" + tocode;
		WriteLog.write("TrainOrderOfflineOffline_保存快递时效信息", "agengId="
				+ agengId + "------->" + "address=" + address + "---------->"
				+ urlString + "?" + param);
		String result = SendPostandGet.submitPost(urlString, param, "UTF-8")
				.toString();
		WriteLog.write("TrainOrderOfflineOffline_保存快递时效信息", "address="
				+ address + "---------->result:" + result);
		try {
			Document document = DocumentHelper.parseText(result);
			Element root = document.getRootElement();
			Element head = root.element("Head");
			Element body = root.element("Body");
			if ("OK".equals(root.elementText("Head"))) {
				if (result.contains("deliver_time")) {
					Element deliverTmResponse = body
							.element("DeliverTmResponse");
					Element deliverTm = deliverTmResponse.element("DeliverTm");
					String business_type_desc = deliverTm
							.attributeValue("business_type_desc");
					String deliver_time = deliverTm
							.attributeValue("deliver_time");
					String business_type = deliverTm
							.attributeValue("business_type");
					results = "如果" + realTime + "正常发件。快递类型为:"
							+ business_type_desc + "。快递预计到达时间:" + deliver_time
							+ "。以上时效为预计到达时间，仅供参考，精准时效以运单追踪结果中的“预计到达时间”为准。";
				} else {
					results = "获取快递时间失败！请上官网核验快递送达时间。";
				}
			}
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return results;
	}

	/**
	 * 获取取快递时间
	 * 
	 * @param dates
	 * @param time1
	 * @param time2
	 * @return
	 */
	public String getRealTimes(String dates, String time1, String time2) {
		String result = "";
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String realDates = sdf1.format(new Date());
		try {
			Date date0 = sdf.parse(dates);
			Date date1 = sdf.parse(time1);
			Date date2 = sdf.parse(time2);
			if (date0.before(date1)) {
				result = (realDates.substring(0, 10) + " " + sdf.format(date1));
			} else if (date0.after(date1) && date0.before(date2)) {
				result = (realDates.substring(0, 10) + " " + sdf.format(date2));
			} else if (date0.after(date2)) {
				Date ds = getDate(new Date());
				String nextd = sdf1.format(ds);
				result = (nextd.substring(0, 10) + " " + sdf.format(date1));
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return result;
	}

	public Date getDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, 1);
		Date date1 = new Date(calendar.getTimeInMillis());
		return date1;
	}

	/**
	 * 通过存储过程获取乘客地址的citycode
	 * 
	 * @param address
	 * @return
	 */
	public String getExpressCodes(String address) {
		String procedure = "sp_TrainOfflineExpress_getCode @address='"
				+ address + "'";
		List list = Server.getInstance().getSystemService()
				.findMapResultByProcedure(procedure);
		String cityCode = "010";
		if (list.size() > 0) {
			Map map = (Map) list.get(0);
			cityCode = map.get("CityCode").toString();
		}
		return cityCode;
	}
}
