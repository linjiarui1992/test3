package com.ccservice.inter.servlet;

import java.util.List;

import com.ccservice.Util.PropertyUtil;
import com.ccservice.Util.db.DBHelper;
import com.ccservice.Util.db.DBSourceEnum;
import com.ccservice.Util.db.DataColumn;
import com.ccservice.Util.db.DataRow;
import com.ccservice.Util.db.DataTable;
import com.ccservice.Util.file.ExceptionUtil;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.Util.http.SendPostandGet;
import com.ggjs.oauth.client.servlet.AbstractOAuthClientServlet;

public class OAuthClientInitServlet extends AbstractOAuthClientServlet {

	@Override
	public void afterInit() {
		try {
            WriteLog.write("DB首次链接记录", "--->开始链接");//开启发下单模式 接收下单消费者发送的需求
            System.out.println("===OAuthClientServlet start begin===");
            long l1 = System.currentTimeMillis();
            DataTable getDataTable = DBHelper.GetDataTable("select 1", DBSourceEnum.KONGTIE_DB);
            List<DataRow> getRow = getDataTable.GetRow();
            for (DataRow dataRow : getRow) {
                List<DataColumn> getColumn = dataRow.GetColumn();
                for (DataColumn dataColumn : getColumn) {
                    Object getValue = dataColumn.GetValue();
                    if (getValue != null) {
                        System.out.println("DB首次链接正常sql=[select 1]--->返回=["+getValue+"]");
                        WriteLog.write("DB首次链接记录", "DB首次链接正常sql=[select 1]--->返回=["+getValue+"]");
                    }
                }
            }
            WriteLog.write("DB首次链接记录", "自加载DubboInitServelt-->首次链接结束，耗时：" + (System.currentTimeMillis() - l1) + "ms");//开启发下单模式 接收下单消费者发送的需求
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("---DB首次链接异常---");
            ExceptionUtil.writelogByException("DB首次链接记录", e, "---DB首次链接异常---");
        }
		int sleepTime = 1;
		while (true) {
			try {
				SendPostandGet.submitGet(PropertyUtil.getValue("InitServlet",
						"InitUrl.properties"));
			} catch (Exception e) {
				if (e.getMessage().contains("405")) {
					break;
				}
			}
			System.out.println("初始化-Servlet异常-睡眠:" + sleepTime + "秒");
			try {
				Thread.sleep(sleepTime * 1000);
			} catch (InterruptedException e1) {
			}
			sleepTime++;
		}
	}

}
