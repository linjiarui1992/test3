package com.ccservice.inter.servlet;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;

import com.ccservice.train.mqlistener.TrainorderBudanRemoveIdMessageListener;

/**
 * 下单队列开始消费补单的时候，从缓存中清除对应订单ID
 * @time 2015年3月13日 下午3:57:23
 * @author fiend
 */
@SuppressWarnings("serial")
public class QueueMQ_BudanRemoveId extends HttpServlet {

    private String mqaddress = "";// MQ地址

    private String mqusername = "";// MQ 用户名

    private String isstart = "";// 是否开启

    @Override
    public void init() throws ServletException {
        super.init();
        this.mqaddress = this.getInitParameter("mqaddress");
        this.mqusername = this.getInitParameter("mqusername");
        this.isstart = this.getInitParameter("isstart");
        if ("1".equals(isstart)) {
            System.out.println("清除缓存中订单ID:开启");
            orderNotice();
        }
    }

    public void orderNotice() {
        ConnectionFactory cf = new ActiveMQConnectionFactory(mqaddress);
        Connection conn = null;
        Session session = null;
        try {
            conn = cf.createConnection();
            session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Destination destination = new ActiveMQQueue(mqusername);
            MessageConsumer consumer = session.createConsumer(destination);
            consumer.setMessageListener(new TrainorderBudanRemoveIdMessageListener());
            conn.start();
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
