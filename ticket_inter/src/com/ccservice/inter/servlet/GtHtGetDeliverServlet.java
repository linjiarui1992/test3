package com.ccservice.inter.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.offline.util.DelieveUtils;
import com.ccservice.offline.util.ExceptionTCUtil;
import com.ccservice.offline.util.HttpUtil;
import com.ccservice.offline.util.TrainOrderOfflineUtil;
import com.tenpay.util.MD5Util;

public class GtHtGetDeliverServlet extends HttpServlet {
    private static final String LOGNAME = "高铁管家请求快递时效接口";

    private int r1 = new Random().nextInt(10000000);

	private static final long serialVersionUID = 1L;
       
    public GtHtGetDeliverServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		String timeStamp=request.getParameter("timeStamp");//时间戳 2017-10-19 15:00:32
	    String partnerName=request.getParameter("partnerName");//gaotieguanjia
	    String messageIdentity=request.getParameter("messageIdentity");//807C972168D04D6E8127B168B4627726
	    String Address = request.getParameter("Address");//湖北省宜昌市西陵区青岛路21号中南装备公司
	    String TicketTime=request.getParameter("TicketTime");//2017-10-21 13:53:00.0
	    
	    //正式环境是不需要这个步骤的，有可能高铁管家做了专门的配置处理
	    //Address = new String(Address.getBytes("ISO-8859-1"),"UTF-8"); 
	    
	    WriteLog.write("高铁管家请求快递时效接口", "timeStamp:"+timeStamp+",partnerName:"+partnerName+";messageIdentity:"+messageIdentity+";Address:"+Address+";TicketTime:"+TicketTime);
	    
	    String sql = "SELECT keys from TrainOfflineAgentKey where partnerName='"+partnerName+"'";
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        
        String key="";
        String code="";
        String msg="";
        String deliverTime="";//2017-10-21 12:00:00,2017-10-21 12:00:00 - 到达日期
        String ifpaytime="";//寄件时间 - 根据代售点的配置信息匹配的寄件时间
        String deliverDescription="";//配送费用￥20,2017-10-19 17:00:00前完成支付,2017-10-21 12:00:00前送到！
        
        String expressType = "SF";
        
        if(list.size()>0){
//        	if(true){
            Map map=(Map)list.get(0);
            key=map.get("keys").toString();
        		
            String md5s=MD5Util.MD5Encode(partnerName+timeStamp+key, "UTF-8").toUpperCase();
            if(messageIdentity.equals(md5s)){//验证通过
        		code="0";
            	
        		//String agentid=distribution2(Address);

                String createUid = "57";
                //String createUser = "高铁管家";
                
                String agentid = "414";
                try {
                    agentid = TrainOrderOfflineUtil.distribution(Address, Integer.valueOf(createUid));
                }
                catch (Exception e) {
                    WriteLog.write(LOGNAME + "-接口访问异常", r1 + "-无法获取到分单代售点:请联系技术处理");
                    //幂等的设计和实现
                    ExceptionTCUtil.handleTCException(e);
                }
                
                //String result=getDelieveStr(agentid,Address);
                
                JSONObject jj=DelieveUtils.getDelieveStr2GT(agentid, Address);
                
                String sf=jj.getString("results");//配送费用￥20,2017-10-19 17:00:00前完成支付,2017-10-21 12:00:00前送到！

                ifpaytime=jj.getString("realTime");
                deliverTime=jj.getString("totime");//2017-10-21 12:00:00,2017-10-21 12:00:00 - 到达日期

                WriteLog.write("高铁管家请求快递时效接口", "agentid:"+agentid+",jj:"+jj);
                
                if (deliverTime.equals("0") || deliverTime.equals("1") || deliverTime.equals("2")) {//不拦截
                    //匹配新的快递时效的机制
                    deliverTime = TrainOrderOfflineUtil.ungetDelieveTime(Address, agentid, LOGNAME, r1);
                }

                //strResult = "配送费用￥20," + realTime + "前完成支付," + deliver_time + "前送到！";
                //配送费用￥20,2017-10-19 17:00:00前完成支付,2017-10-21 12:00:00前送到！
                if (sf.contains("暂无快递")) {
                    sf = sf.substring(sf.indexOf("-")+1);
                    sf = "配送费用￥20," + sf + "前完成支付," + deliverTime + "前送到！";
                }
                
                //String jd=getJDExpressDelivery(agentid, Address);
                
                deliverDescription = sf;
                
                /*if (choose(sf, jd)) {
                    expressType = "JD";
                    deliverTime = jd;
                    deliverDescription = sf.split(",")[1] + jd + "前送达！";
                }*/
                
                /*if ("".equals(deliverTime)) {
                    deliverTime = "目前无具体到达时间";
                }*/

            	WriteLog.write(LOGNAME, r1 + "Address:"+Address+"--->返回信息="+deliverDescription);
            	msg="获取配送时效,价格成功";
            }else{
                code="1";
                msg="账号核验失败！";
            }
        }else{
            code="1";
            msg="账号核验失败！";
        }

        if (code.equals("1")) {
            WriteLog.write(LOGNAME + "-账号核验失败", r1 + "请联系技术处理");
        }
        
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        JSONObject responsejson =new JSONObject();
        responsejson.put("code", code);
        responsejson.put("msg", msg);
        responsejson.put("ifpaytime", ifpaytime);
        
        //responsejson.put("deliverTime", deliverTime.split(",")[0]);
        responsejson.put("deliverTime", deliverTime);
        
        responsejson.put("expressType", expressType);
        responsejson.put("arriveTime", deliverDescription);
        responsejson.put("price", 20);

        WriteLog.write(LOGNAME, r1 + "Address:"+Address+"--->反馈信息="+responsejson.toJSONString());
        
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.append(responsejson.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }
	}
	
	/**
	 * 获取出票点ID
	 * @param agengId
	 * @param address
	 * @return
	 * @throws ParseException 
	 */
	/*public static String getGTDelieveStr(String agengId,String address) throws ParseException{
    	//String urlString=PropertyUtil.getValue("expressDeliverUrl", "train.properties");
    	
		WriteLog.write("高铁请求时效temp", "results="+jbb.toJSONString());
    	return jbb.toJSONString();
    }*/
	
	/**jbb.toJSONString()
	 * 获取某一天
	 * @param dateString
	 * @param i
	 * @return
	 * @throws ParseException
	 */
	/*public static String getNextDay(String dateString, int i) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = sdf.parse(dateString);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, -i);
        date = calendar.getTime();
        return sdf.format(date);
    }*/
	
	/**
     * 获取取快递时间
     * @param dates
     * @param time1
     * @param time2
     * @return
     */
    /*public static String getRealTimes(String dates,String time1,String time2){
        String result="";
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String realDates=sdf1.format(new Date());
        try {
            Date date0 = sdf.parse(dates);
            Date date1 = sdf.parse(time1);
            Date date2 = sdf.parse(time2);
            if(date0.before(date1)){
                result=(realDates.substring(0, 10)+" "+sdf.format(date1));
            }else if(date0.after(date1) && date0.before(date2)){
                result=(realDates.substring(0, 10)+" "+sdf.format(date2));
            }else if(date0.after(date2)){
                Date ds=getDate(new Date());
                String nextd=sdf1.format(ds);
                result=(nextd.substring(0, 10)+" "+sdf.format(date1));
            }
        }
        catch (ParseException e) {
            e.printStackTrace();
        } 
        return result;
    }*/
    
    /*public static Date getDate(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        Date date1 = new Date(calendar.getTimeInMillis());
        return date1;
    }*/
    
    /**
     * 通过存储过程获取乘客地址的citycode
     * @param address
     * @return
     */
    /*public static String getExpressCodes(String address){
    	String procedure="sp_TrainOfflineExpress_getCode @address='"+address+"'";
    	List list = Server.getInstance().getSystemService().findMapResultByProcedure(procedure);
    	String cityCode="010";
    	if(list.size()>0){
    		Map map=(Map)list.get(0);
    		cityCode=map.get("CityCode").toString();
    	}
    	return cityCode;
    }*/
    
    /**
     * 获取京东时效
     * @param agentId
     * @param address
     * @return
     */
	/*public String getJDExpressDelivery(String agentId,String address){
		String cityName = getCityByAgentId(agentId);
	    SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
	    String url="http://120.26.100.206:12345/JD/jdisacept";//正式jdisacept  jdgetkey
	    int max=10000;
        int min=1000;
        Random random = new Random();
        int s = random.nextInt(max)%(max-min+1) + min;
	    String orderid="T"+sdf.format(new Date())+""+s;
	    String sendCode = getProvinceCode(cityName.split("_")[0], cityName.split("_")[1]);
	    String senderprovinceid = "1";
	    String sendercityid = "72";
	    if (sendCode != null && !"".equals(sendCode)) {
            senderprovinceid = sendCode.split("_")[0];
            sendercityid = sendCode.split("_")[1];
           
        }
	    try {
            address = URLEncoder.encode(address, "utf-8");
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String param="address="+address+"&orderid="+orderid+"&senderprovinceid="+senderprovinceid+"&sendercityid="+sendercityid;
        WriteLog.write("TrainOrderOfflineOffline_保存快递时效信息", "JD-address:"+address+";orderid:"+orderid+";senderprovinceid:"+senderprovinceid+";sendercityid:"+sendercityid);
        String resultjson=SendPostandGet.submitPost(url, param, "UTF-8").toString();
        WriteLog.write("TrainOrderOfflineOffline_保存快递时效信息", "JD-address:"+address+";orderid:"+orderid+";senderprovinceid:"+senderprovinceid+";sendercityid:"+sendercityid+";resultjson:"+resultjson);
        JSONObject json=JSONObject.parseObject(resultjson);
        String responses=json.getString("jingdong_etms_range_check_responce");
        JSONObject jsonres=JSONObject.parseObject(responses);
        String resultInfo=jsonres.getString("resultInfo");
        JSONObject jsonone=JSONObject.parseObject(resultInfo);
        String rcode=jsonone.getString("rcode");
        String result="";
        if("100".equals(rcode)){
            String agingName=jsonone.getString("agingName");
            String aging=jsonone.getString("aging");
            if(!"0".equals(aging)){
                SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat sdf2=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date istoday = new Date();
                String today = sdf1.format(istoday);
                today += " 17:00:00";
                Date theday = new Date();
				try {
					theday = sdf2.parse(today);
				} catch (ParseException e) {
				}
                if (istoday.getTime() > theday.getTime()) {
                	result=sdf1.format(getDateAfter(new Date(),Integer.parseInt(aging)+1))+" 18:00:00";
				} else {
					result=sdf1.format(getDateAfter(new Date(),Integer.parseInt(aging)))+" 18:00:00";
				}
            }
        }
        return result;
	}*/
	
	/**
	 * 
	 * @param d
	 * @param day
	 * @return
	 */
	/*public Date getDateAfter(Date d, int day) {  
        Calendar now = Calendar.getInstance();  
        now.setTime(d);  
        now.set(Calendar.DATE, now.get(Calendar.DATE) + day);  
        return now.getTime();  
    }*/
	
    /**
     * 通过agentId拿对应的省市
     * @param agentId
     * @return
     */
    /*public String getCityByAgentId(String agentId){
    	agentId="363";
    	String provinceName = "北京";
    	String cityName = "朝阳区";
    	String sql = "SELECT ProvinceName,CityName,RegionName FROM T_CUSTOMERAGENT WHERE ID = '" + agentId + "'";
    	List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
    	if(list.size()>0){
            Map map=(Map)list.get(0);
            provinceName=map.get("ProvinceName").toString();
            cityName=map.get("CityName").toString();
            String regionName = map.get("RegionName").toString();
            if (provinceName.equals(cityName.replace("市", ""))) {
            	cityName = regionName;
			}
        }
    	WriteLog.write("高铁管家新版快递路由分配订单", "agentId=" + agentId + ";city=" + provinceName + "_" + cityName);
    	return provinceName + "_" + cityName;
    }*/
    
    /**
	 * jd获取省市code
	 * @param province
	 * @param city
	 * @param random
	 * @return
	 */
	/*public String getProvinceCode(String province, String city) {
        String provinces = province.replace("省", "").replace("市", "");
        String privinceCode = "";
        String cityCode = "";
        String parnetid = "";
        String sendCode = "";
        String psql = "SELECT areaid FROM JDprovince WITH (NOLOCK) WHERE areaname = '"
                + provinces + "'";
        List plist = Server.getInstance().getSystemService()
                .findMapResultBySql(psql, null);
        if (plist.size() > 0) {
            Map map = (Map) plist.get(0);
            privinceCode = map.get("areaid").toString();
        }

        String csql = "SELECT areaid,parnetid FROM JDprovince WITH (NOLOCK) WHERE areaname = '"
                + city + "'";
        List clist = Server.getInstance().getSystemService()
                .findMapResultBySql(csql, null);
        if (clist.size() > 0) {
            Map map = (Map) clist.get(0);
            cityCode = map.get("areaid").toString();
            parnetid = map.get("parnetid").toString();
        }
        if (!"".equals(privinceCode) && privinceCode.equals(parnetid)) {
            sendCode = privinceCode + "_" + cityCode;
        }
        return sendCode;
    }*/
	
	/*public boolean choose(String sf, String jd){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		boolean flag = false;
		if (jd.contains("18:00")) {
			try {
				Date sfdate = sdf.parse(sf.split(",")[2]);
				Date jddate = sdf.parse(jd);
				if (jddate.getTime() < sfdate.getTime()) {
					flag = true;
				}
			} catch (ParseException e) {
			}
		}
		return flag;
	}*/

    /*public static void main(String[] args) {
        String ss="2016-10-17 20:00:00,2016-10-17 20:00:00";
        System.out.println(ss.split(",")[0]);
//      try {
//            System.out.println(getNextDay(ss.substring(0, 10),-2)+" "+ss.substring(11, 19));
//        }
//        catch (ParseException e) {
//            e.printStackTrace();
//        }
    }*/

    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        HttpUtil httpUtil = new HttpUtil();

        String partnerName= "gaotieguanjia";//
        /*String timeStamp0=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());//
        String timeStamp=new SimpleDateFormat("yyyy-MM-dd%20HH:mm:ss").format(new Date());*///

        String timeStamp=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());//
        
        String key="";
        
        String sql = "SELECT keys from TrainOfflineAgentKey where partnerName='"+partnerName+"'";
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        Map map=(Map)list.get(0);
        key=map.get("keys").toString();
        
        //String messageIdentity=MD5Util.MD5Encode(partnerName+timeStamp0+key, "UTF-8").toUpperCase();//807C972168D04D6E8127B168B4627726
        String messageIdentity=MD5Util.MD5Encode(partnerName+timeStamp+key, "UTF-8").toUpperCase();//807C972168D04D6E8127B168B4627726
        
        /*String data = "{\"timeStamp\":\""+timeStamp+"\","
                + "\"partnerName\":\""+partnerName+"\","
                + "\"messageIdentity\":\""+messageIdentity+"\","
                + "\"Address\":\"湖北省宜昌市西陵区青岛路21号中南装备公司\","
                + "\"TicketTime\":\"2017-10-21 13:53:00.0\"}";*/

        //String Address="湖北省宜昌市西陵区青岛路21号中南装备公司";
        //String Address="新疆维吾尔自治区乌鲁木齐市天山区拜城县";
        String Address="拜城县";
        
        //String TicketTime="2017-10-21%2013:53:00.0";
        String TicketTime="2017-10-21 13:53:00.0";
        
        //String data = "?timeStamp="+timeStamp+"&partnerName="+partnerName+"&messageIdentity="+messageIdentity+"&Address="+Address+"&TicketTime="+TicketTime;//1: 开始配送, 2: 已送达
        //System.out.println(timeStamp.replace(" ", "%20"));
        String data = "?timeStamp="+URLEncoder.encode(timeStamp, "UTF-8")+"&partnerName="+partnerName+"&messageIdentity="+messageIdentity
                +"&Address="+URLEncoder.encode(Address, "UTF-8")+"&TicketTime="+URLEncoder.encode(TicketTime, "UTF-8");//1: 开始配送, 2: 已送达

        System.out.println(data);
        
        //本地测试地址
        //String url = "http://localhost:8097/ticket_inter/GtHtGetDeliverServlet";
        //测试环境地址
        //String url = "http://121.40.226.72:9007/ticket_inter/GtHtGetDeliverServlet";
        //String url = "http://peisong.test.51kongtie.com/ticket_inter/GtHtGetDeliverServlet";
        
        //正式环境地址
        //String url = "http://121.40.241.126:9010/ticket_inter/GtHtGetDeliverServlet";
        //String url = "http://ws.peisong.51kongtie.com/ticket_inter/GtHtGetDeliverServlet";
        
        String url = "http://121.40.241.126:9065/ticket_inter/GtHtGetDeliverServlet";
        
        System.out.println(httpUtil.doGet(url, data));
        //System.out.println(httpUtil.doGetUTF8(url, data));

        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间： " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间： " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间： " + runTimeS / 60 + "min：" + runTimeS % 60 + "s");//分
    }
    
}
