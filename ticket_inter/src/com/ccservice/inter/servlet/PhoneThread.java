package com.ccservice.inter.servlet;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.callback.SendPostandGet;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.atom.hotel.TaobaoHotelInterfaceUtil;
import com.ccservice.b2b2c.base.mailaddress.MailAddress;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainOrderOffline.TrainOrderOffline;
import com.ccservice.b2b2c.base.trainTicketOffline.TrainTicketOffline;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.b2b2c.base.trainpassengerOffline.TrainPassengerOffline;
import com.ccservice.b2b2c.util.BaiDuMapApi;
import com.ccservice.b2b2c.util.DBoperationUtil;
import com.ccservice.b2b2c.util.ExceptionUtil;
import com.ccservice.b2b2c.util.db.DBHelper;
import com.ccservice.b2b2c.util.db.DataRow;
import com.ccservice.b2b2c.util.db.DataTable;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.inter.server.Server;
import com.ccservice.qunar.train.MyThreadQunarOrderOffline;

/*
 * 线下火车票拉票线程
 * ren 2016.04.21
 * */
public class PhoneThread extends Thread {
    private String arrStationValue, dptStationValue, seatValue_Key, orderNoValue, certNoValue, certTypeValue,
            nameValue, seatValue, trainEndTimeValue, trainNoValue, trainStartTimeValue, birthday;

    private float ticketPayValue, seatValue_Value = 0;

    private int ticketTypeValue;

    private String json;

    public PhoneThread(String json) {
        this.json = json;
    }

    public void run() {
        getorder(json);
    }

    public void getorder(String json) {
        JSONObject jb = JSONObject.parseObject(json);
        JSONArray jsonarr = jb.getJSONArray("data");
        JSONObject info = (JSONObject) jsonarr.get(0);
        WriteLog.write("TelTrainOffline_AddThread开始创建订单", "info=" + info);
        try {
            String main_order_id = info.getString("orderNo");// 电话订票订单号
            String sql = "SELECT * FROM TrainOrderOffline WHERE OrderNumberOnline='" + main_order_id + "'";
            List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
            WriteLog.write("电话订票lists_logs", "防止订单重复list" + list.size());
            if (list.size() == 0) {
                float total_price = info.getFloat("ticketPay");// 订单金额
                Trainorder trainorder = new Trainorder();// 创建订单
                trainorder.setQunarOrdernumber(main_order_id);// 电话订票订单号
                String dptStationValue = info.getString("dptStation");// 出发站
                String arrStationValue = info.getString("arrStation");// 到达站
                String trainNo = info.getString("trainNo");// 到达站
                String Auftragsnummer = info.getString("DB_Auftragsnummer");// 取票号
                JSONArray passengers = info.getJSONArray("passengers");
                List<Trainpassenger> trainplist = new ArrayList<Trainpassenger>();
                for (int i = 0; i < passengers.size(); i++) {
                    List<Trainticket> traintickets = new ArrayList<Trainticket>();
                    JSONObject infotickets = passengers.getJSONObject(i);
                    String certNoValue = infotickets.getString("certNo");// 乘车人证件号码
                    String certificate_type = infotickets.getString("certType");// 证件类型，0:身份证
                                                                                // 1:护照
                                                                                // 4:港澳通行证
                                                                                // 5:台湾通行证
                    String trainStartTimeValue = "0";// 出发时间
                    String nameValue = infotickets.getString("name");// 乘客姓名
                    int passenger_type = Integer.parseInt(infotickets.getString("ticketType"));

                    Trainticket trainticket = new Trainticket();
                    Trainpassenger trainpassenger = new Trainpassenger();
                    trainticket.setArrival(arrStationValue);// 存入终点站
                    trainticket.setDeparture(dptStationValue);// 存入始发站
                    trainticket.setStatus(Trainticket.WAITISSUE);
                    trainticket.setTickettype(passenger_type);// 存入票种类型
                    trainticket.setPayprice(0);
                    trainticket.setIsapplyticket(1);
                    trainticket.setRefundfailreason(0);
                    trainticket.setCosttime("0");// 历时
                    trainticket.setTrainno(trainNo);// 车次号
                    traintickets.add(trainticket);
                    trainpassenger.setIdnumber(certNoValue);// 存入乘车人证件号码
                    trainpassenger.setIdtype(Integer.parseInt(certificate_type));// 存入乘车人证件种类
                    trainpassenger.setName(nameValue);// 存入乘车人姓名
                    //					trainpassenger.setBirthday(birthday);
                    trainpassenger.setChangeid(0);
                    trainpassenger.setAduitstatus(1);
                    trainpassenger.setTraintickets(traintickets);
                    trainplist.add(trainpassenger);
                }
                trainorder.setContactuser(info.getString("transportName"));
                trainorder.setContacttel(info.getString("transportPhone"));// 线下票收件电话
                trainorder.setTicketcount(trainplist.size());
                trainorder.setCreateuid(58l);
                trainorder.setInsureadreess(info.getString("transportAddress"));
                trainorder.setOrderprice(total_price);
                trainorder.setAgentid(0);
                trainorder.setPassengers(trainplist);
                trainorder.setCreateuser("电话订票");
                trainorderofflineadd(trainorder, Auftragsnummer);
            }
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("Tel_Add_Thread线程异常", e);
            WriteLog.write("Tel_Add_Thread线程异常", "e:" + e);
        }

    }

    private DBoperationUtil dt = new DBoperationUtil();

    public void trainorderofflineadd(Trainorder trainorder, String Auftragsnummer) {
        TrainOrderOffline trainOrderOffline = new TrainOrderOffline();
        // 有合适的出票点，分配
        String addresstemp = trainorder.getInsureadreess();
        String agentidtemp = distribution2(addresstemp);
        WriteLog.write("线下火车票Tel分单log记录", "地址:" + addresstemp + "----->agentId:" + agentidtemp);
        trainOrderOffline.setAgentId(Long.valueOf(agentidtemp));// ok
        trainOrderOffline.setPaystatus(1);
        trainOrderOffline.setTradeNo(Auftragsnummer);//电话订票的取票号
        trainOrderOffline.setCreateUId(trainorder.getCreateuid());// ok
        trainOrderOffline.setCreateUser(trainorder.getCreateuser());// ok
        trainOrderOffline.setContactUser(trainorder.getContactuser());// ok
        trainOrderOffline.setContactTel(trainorder.getContacttel());// ok
        trainOrderOffline.setOrderPrice(trainorder.getOrderprice());// ok
        trainOrderOffline.setAgentProfit(0f);// ok
        trainOrderOffline.setOrdernumberonline(trainorder.getQunarOrdernumber());// ok
        trainOrderOffline.setTicketCount(trainorder.getTicketcount());// ok
        trainOrderOffline.setPaperType(0);
        trainOrderOffline.setPaperBackup(0);
        trainOrderOffline.setPaperLowSeatCount(0);
        trainOrderOffline.setExtSeat("无");
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        trainOrderOffline.setOrderTimeOut(Timestamp.valueOf(sdf1.format(new Date())));
        Timestamp startTime = new Timestamp(new Date().getTime());
        trainOrderOffline.setOrderTimeOut(startTime);
        String sp_TrainOrderOffline_insert = dt.getCreateTrainorderOfficeProcedureSql(trainOrderOffline);
        WriteLog.write("TrainOrderOffline_insert_保存订单存储过程", sp_TrainOrderOffline_insert);
        List list = Server.getInstance().getSystemService().findMapResultByProcedure(sp_TrainOrderOffline_insert);
        Map map = (Map) list.get(0);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String orderid = map.get("id").toString();
        // 通过出票点和邮寄地址获取预计到达时间results
        String delieveStr = getDelieveStr(agentidtemp, trainorder.getInsureadreess());//
        String updatesql = "UPDATE TrainOrderOffline SET expressDeliver ='" + delieveStr + "' WHERE ID=" + orderid;
        WriteLog.write("TrainOrderOfflineOffline_保存快递时效信息", updatesql);
        Server.getInstance().getSystemService().excuteAdvertisementBySql(updatesql);
        // [TrainOrderOfflineRecord]表插入数据
        String procedureRecord = "sp_TrainOrderOfflineRecord_insert @FKTrainOrderOfflineId=" + orderid
                + ",@ProviderAgentid=" + agentidtemp + ",@DistributionTime='" + sdf.format(new Date())
                + "',@ResponseTime='',@DealResult=0,@RefundReason=0,@RefundReasonStr=''";
        WriteLog.write("sp_TrainOrderOfflineRecord_insert_记录表存储过程", procedureRecord);
        Server.getInstance().getSystemService().findMapResultByProcedure(procedureRecord);
        // 火车票线下订单邮寄信息
        MailAddress address = new MailAddress();
        address.setMailName(trainorder.getContactuser());
        address.setMailTel(trainorder.getContacttel());
        address.setPrintState(0);
        address.setPostcode("100000");
        address.setAddress(trainorder.getInsureadreess());
        address.setOrderid(Integer.parseInt(orderid));
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        address.setCreatetime(timestamp);
        String sp_MailAddress_insert = dt.getMailAddressProcedureSql(address);
        WriteLog.write("TrainOrderOfflineMailAddress_insert_保存邮寄地址存储过程", sp_MailAddress_insert);
        Server.getInstance().getSystemService().findMapResultByProcedure(sp_MailAddress_insert);

        for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
            // 火车票乘客线下TrainPassengerOffline
            TrainPassengerOffline trainPassengerOffline = new TrainPassengerOffline();
            trainPassengerOffline.setorderid(Long.parseLong(orderid));
            trainPassengerOffline.setname(trainpassenger.getName());
            trainPassengerOffline.setidtype(trainpassenger.getIdtype());
            trainPassengerOffline.setidnumber(trainpassenger.getIdnumber());
            trainPassengerOffline.setbirthday(trainpassenger.getBirthday());
            String sp_TrainPassengerOffline_insert = dt
                    .getCreateTrainpassengerOfficeProcedureSql(trainPassengerOffline);
            WriteLog.write("TrainOrderOfflinePassengerOffline_insert_保存乘客存储过程", sp_TrainPassengerOffline_insert);
            List listP = Server.getInstance().getSystemService()
                    .findMapResultByProcedure(sp_TrainPassengerOffline_insert);
            Map map2 = (Map) listP.get(0);
            String trainPid = map2.get("id").toString();
            for (Trainticket ticket : trainpassenger.getTraintickets()) {
                // 线下火车票TrainTicketOffline
                TrainTicketOffline trainTicketOffline = new TrainTicketOffline();
                trainTicketOffline.setTrainPid(Long.parseLong(trainPid));
                trainTicketOffline.setOrderid(Long.parseLong(orderid));
                trainTicketOffline.setDepartTime(Timestamp.valueOf("1990-01-01 00:00:00"));
                trainTicketOffline.setDeparture(ticket.getDeparture());
                trainTicketOffline.setArrival(ticket.getArrival());
                trainTicketOffline.setTrainno(ticket.getTrainno());
                trainTicketOffline.setTicketType(ticket.getTickettype());
                trainTicketOffline.setSeatType("无");
                trainTicketOffline.setPrice(0f);
                //				trainTicketOffline.setCostTime(ticket.getCosttime());
                //				trainTicketOffline.setStartTime(ticket.getDeparttime()
                //						.substring(11, 16));
                //				trainTicketOffline.setArrivalTime(ticket.getArrivaltime());
                //				trainTicketOffline.setSubOrderId(ticket.getTicketno());
                String sp_TrainTicketOffline_insert = dt.getCreateTrainticketOfficeProcedureSql(trainTicketOffline);
                WriteLog.write("TrainOrderOfflineTicketOffline_insert_保存车票存储过程", sp_TrainTicketOffline_insert);
                Server.getInstance().getSystemService().findMapResultByProcedure(sp_TrainTicketOffline_insert);
            }
        }
    }

    /**
     * 根据数据库设置匹配出票点
     * 
     * @param address1
     * @return
     */
    public static String distribution2(String address1) {
        boolean flag = false;
        // 默认出票点
        String sql1 = "SELECT agentId FROM TrainOfflineMatchAgent WHERE status=2 AND createUid=" + "52";
        List list1 = Server.getInstance().getSystemService().findMapResultBySql(sql1, null);
        String agentId = "378";
        if (list1.size() > 0) {
            Map map = (Map) list1.get(0);
            agentId = map.get("agentId").toString();
        }
        // 程序自动分配出票点
        String sql2 = "SELECT * FROM TrainOfflineMatchAgent WHERE status=1 AND createUid=" + "52";
        List list2 = Server.getInstance().getSystemService().findMapResultBySql(sql2, null);
        for (int i = 0; i < list2.size(); i++) {
            Map mapp = (Map) list2.get(i);
            String provinces = mapp.get("provinces").toString();
            String agentid = mapp.get("agentId").toString();
            String[] add = provinces.split(",");
            for (int j = 0; j < add.length; j++) {
                if (address1.startsWith(add[j])) {
                    agentId = agentid;
                    flag = true;
                }
                if (flag) {
                    break;
                }
            }
            if (flag) {
                break;
            }
        }
        WriteLog.write("去哪qclt新版分配订单", "agentId=" + agentId + ";address1=" + address1);
        return agentId;
    }

    public static String getDelieveStr(String agengId, String address) {
        String results = "";
        String fromcode = "010";
        String tocode = getExpressCodes(address);
        String time1 = "10:00:00";
        String time2 = "18:00:00";

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String dates = sdf.format(new Date());
        String sql1 = "SELECT fromcode,time1,time2 from TrainOrderAgentTimes where agentId=" + agengId;
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql1, null);
        if (list.size() > 0) {
            Map map = (Map) list.get(0);
            fromcode = map.get("fromcode").toString();
            time1 = map.get("time1").toString();
            time2 = map.get("time2").toString();
        }
        String realTime = getRealTimes(dates, time1, time2);
        String urlString = PropertyUtil.getValue("expressDeliverUrl", "train.properties");
        String param = "times=" + realTime + "&fromcode=" + fromcode + "&tocode=" + tocode;
        WriteLog.write("TrainOrderOfflineOffline_保存快递时效信息", "agengId=" + agengId + "------->" + "address=" + address
                + "---------->" + urlString + "?" + param);
        String result = SendPostandGet.submitPost(urlString, param, "UTF-8").toString();
        try {
            Document document = DocumentHelper.parseText(result);
            Element root = document.getRootElement();
            Element head = root.element("Head");
            Element body = root.element("Body");
            if ("OK".equals(root.elementText("Head"))) {
                Element deliverTmResponse = body.element("DeliverTmResponse");
                Element deliverTm = deliverTmResponse.element("DeliverTm");
                String business_type_desc = deliverTm.attributeValue("business_type_desc");
                String deliver_time = deliverTm.attributeValue("deliver_time");
                String business_type = deliverTm.attributeValue("business_type");
                results = "如果" + realTime + "正常发件。快递类型为:" + business_type_desc + "。快递预计到达时间:" + deliver_time
                        + "。以上时效为预计到达时间，仅供参考，精准时效以运单追踪结果中的“预计到达时间”为准。";
            }
        }
        catch (DocumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return results;
    }

    /**
     * 获取取快递时间
     * @param dates
     * @param time1
     * @param time2
     * @return
     */
    public static String getRealTimes(String dates, String time1, String time2) {
        String result = "";
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String realDates = sdf1.format(new Date());
        try {
            Date date0 = sdf.parse(dates);
            Date date1 = sdf.parse(time1);
            Date date2 = sdf.parse(time2);
            if (date0.before(date1)) {
                result = (realDates.substring(0, 10) + " " + sdf.format(date1));
            }
            else if (date0.after(date1) && date0.before(date2)) {
                result = (realDates.substring(0, 10) + " " + sdf.format(date2));
            }
            else if (date0.after(date2)) {
                Date ds = getDate(new Date());
                String nextd = sdf1.format(ds);
                result = (nextd.substring(0, 10) + " " + sdf.format(date1));
            }
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Date getDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        Date date1 = new Date(calendar.getTimeInMillis());
        return date1;
    }

    /**
     * 通过存储过程获取乘客地址的citycode
     * @param address
     * @return
     */
    public static String getExpressCodes(String address) {
        String procedure = "sp_TrainOfflineExpress_getCode @address='" + address + "'";
        List list = Server.getInstance().getSystemService().findMapResultByProcedure(procedure);
        String cityCode = "010";
        if (list.size() > 0) {
            Map map = (Map) list.get(0);
            cityCode = map.get("CityCode").toString();
        }
        return cityCode;
    }
}
