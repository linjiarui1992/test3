package com.ccservice.inter.servlet;



/**
 * 同程抢票转线下-下单
 * 此接口接到订单后，在进行入库处理
 * wangdiao
 * 2017-01-04
 */
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;





import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.inter.job.WriteLog;

public class TrainBespeakAddOrder extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public TrainBespeakAddOrder() {
		super();
	}

	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
			request.setCharacterEncoding("UTF-8");
			response.setCharacterEncoding("UTF-8");
			response.setHeader("content-type", "text/html;charset=UTF-8");
			String jsonStr = request.getParameter("jsonStr");
			JSONObject result = new JSONObject();
			PrintWriter out = null;
			WriteLog.write("同程抢票-线下单", "param：" + jsonStr);
			try {
				out = response.getWriter();
				if (jsonStr != null && !"".equals(jsonStr)) {
					boolean jsonIstrue = false;// json数据是否正确
					JSONObject json=new JSONObject();
					try {
						json = JSONObject.parseObject(jsonStr);
						 jsonIstrue = true;
					} catch (Exception e) {
						e.printStackTrace();
					}
					if(jsonIstrue){
						//json数据验证通过
						boolean passCheck = checkJsonDate(json);
						if(passCheck){
							// 通用参数通过验证了
							if (json == null || json.isEmpty()) {
								result.put("success", false);
								result.put("msg", "参数为空");
							} else {
								JSONArray datas = json.getJSONArray("data");
								WriteLog.write("同程抢票-线下单", "data：" + datas);
								if (datas == null ) {
									result.put("success", false);
									result.put("msg", "参数错误");
								} else {
									for (int i = 0; i < datas.size(); i++) { 
										/**进行入库下单处理开始**/
										new OfflineTicketNumberOrder(datas.getJSONObject(i)).start();
										/**进行入库下单处理结束**/
									}
									result.put("success", true);
									result.put("msg", "请求数据已接收");
								}
							}
						}
						else{
							result.put("success", false);
							result.put("msg", "数据参数缺失 || 存在数据为空");
						}
					}
					else{
						result.put("success", false);
						result.put("msg","json数据格式错误");
					}
					
				} else {
					result.put("success", false);
					result.put("msg", "参数为空");
				}
			} catch (Exception e) {
				result.put("success", false);
				result.put("msg", "系统错误");
			} finally {
				if (out != null) {
					out.print(result.toString());
					out.flush();
					out.close();
					}
			}
		}
	 /**
	  * 检查jsonObject 数据参数是否缺失
	  * @param jsonObject
	  * @return		true 是  false 否
	  */
    private boolean checkJsonDate(JSONObject jsonObject) {
        boolean pass = false;
        if (jsonObject.containsKey("data") && inspectionParameter(jsonObject)) {
            pass = true;
        }
        return pass;
    }
    /**
     * 检验参数的是否在空的数据
     * @param jsonObject
     * @return   true  false
     */
	private boolean inspectionParameter(JSONObject jsonObject) {
		boolean  result=false;
		JSONArray datas = jsonObject.getJSONArray("data");
		for (int i = 0; i < datas.size(); i++) { 
			if(datas.getJSONObject(i).containsKey("arrStation") && datas.getJSONObject(i).containsKey("dptStation") && datas.getJSONObject(i).containsKey("trainNo")  && datas.getJSONObject(i).containsKey("orderNo")  && datas.getJSONObject(i).containsKey("passengers") )
			{
				result=true;
			}
		}
		return result;
	}
    
}
