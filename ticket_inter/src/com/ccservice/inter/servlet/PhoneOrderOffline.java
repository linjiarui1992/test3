package com.ccservice.inter.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.inter.server.Server;

public class PhoneOrderOffline extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public PhoneOrderOffline() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		String jsonStr=request.getParameter("jsonStr");
		WriteLog.write("电话订票拉单json", "jsonStr=" + jsonStr);
		String result="false";
		JSONObject jb=JSONObject.parseObject(jsonStr); 
		String odn=null;
		String dan=null;
		JSONArray jsonarr=	jb.getJSONArray("data");
		if(jsonarr.size()>0){
			JSONObject jsonOb=(JSONObject)jsonarr.get(0);
			odn=jsonOb.getString("orderNo");
			dan=jsonOb.getString("DB_Auftragsnummer");			//取票号
		}
		String sql = "SELECT * FROM TrainOrderOffline WHERE OrderNumberOnline='"
				+ odn + "'";
		List list = Server.getInstance().getSystemService()
				.findMapResultBySql(sql, null);
	    JSONObject jsonObject = new JSONObject();
		if(odn!=null && !"".equals(odn) && list.size()==0){
			if(dan!=null && !"".equals(dan)){//判断接单成功
				result="true";
				jsonObject.put("orderNo", odn);
				jsonObject.put("orderStatus", result);
				WriteLog.write("拉票信息成功", jsonObject.toString());
				new PhoneThread(jsonStr).start();	//进入线程   开始下单操作
				response.getWriter().print(jsonObject.toString());
			}
		}else{
			jsonObject.put("orderNo", odn);
			jsonObject.put("orderStatus", result);
			WriteLog.write("拉票信息失败", jsonObject.toString());
			response.getWriter().print(jsonObject.toString());
		}
	}

}
