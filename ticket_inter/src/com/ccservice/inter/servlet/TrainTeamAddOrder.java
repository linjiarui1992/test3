package com.ccservice.inter.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.train.qunar.QunarOrderMethod;

/**
 * 团队票下单
 * @author guozhengju
 *
 */
public class TrainTeamAddOrder extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 
    public TrainTeamAddOrder() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        String jsonStr=request.getParameter("jsonStr");
        WriteLog.write("线下收到团队票订单", "jsonStr="+jsonStr);
        JSONObject jsonObject=JSONObject.parseObject(jsonStr);
        if (jsonObject.containsKey("data")) {
            String data1 = jsonObject.getString("data");
            JSONArray data = JSONArray.parseArray(data1);
            WriteLog.write("JobQunarOrderOffline", jsonObject.toString());
            String orderNos = allOrderNos(data);
            String orderNonos = allOrderNonos(data);
            WriteLog.write("JobQunarOrderOffline", "orderNos:" + orderNos);
            JSONObject jsonresult=new JSONObject();
            jsonresult.put("orderNumber", orderNonos);
            if (!"".equals(orderNos.trim())) {
                List<QunarOrderMethod> listqom = allOldOrders(orderNos);
                for (int i = 0; i < data.size(); i++) {
                    JSONObject info = data.getJSONObject(i);
                    String orderNo = info.getString("orderNo");
                    boolean isoldorder = false;
                    for (int j = 0; j < listqom.size(); j++) {
                        QunarOrderMethod qunarordermethod = (QunarOrderMethod) listqom.get(j);
                        if ((orderNo != null) && (orderNo.equals(qunarordermethod.getQunarordernumber()))) {
                            qunarordermethod.setOrderjson(info);
//                            isoldorder = true;
                            break;
                        }
                    }
                    if (!isoldorder) {
                        QunarOrderMethod qunarordermethod = new QunarOrderMethod();
                        qunarordermethod.setOrderjson(info);
                        TrainTeamAddOrderDB trainTeamAddOrderDB=new TrainTeamAddOrderDB();
                        trainTeamAddOrderDB.exThread(qunarordermethod);
                        jsonresult.put("msg", "success");
                    }else{
                        jsonresult.put("msg", "fail");
                    }
                }
            }
            response.setContentType("text/plain; charset=utf-8");
            PrintWriter out = response.getWriter();
            out.println(jsonresult);
            out.flush();
            out.close();
        }
        
	}
	
	 public String allOrderNos(JSONArray data) {
	        String orderNos = "";
	        for (int a = 0; a < data.size(); a++) {
	            JSONObject info = data.getJSONObject(a);
	            String orderNo = info.getString("orderNo");
	            orderNos = orderNos + "'" + orderNo + "'";
	            if (a < data.size() - 1) {
	                orderNos = orderNos + ",";
	            }
	        }
	        return orderNos;
	    }
	public String allOrderNonos(JSONArray data) {
        String orderNos = "";
        for (int a = 0; a < data.size(); a++) {
            JSONObject info = data.getJSONObject(a);
            String orderNo = info.getString("orderNo");
            orderNos = orderNos + "" + orderNo + "";
            if (a < data.size() - 1) {
                orderNos = orderNos + ",";
            }
        }
        return orderNos;
    }
	 public List<QunarOrderMethod> allOldOrders(String orderNos) {
	        List<QunarOrderMethod> listqom = new ArrayList();
	        String sql_trainorder = "SELECT OrderNumberOnline,Id FROM TrainOrderOffline  WHERE OrderNumberOnline in (" +

	        orderNos + ")";
	        List list = Server.getInstance().getSystemService().findMapResultBySql(sql_trainorder, null);
	        for (int i = 0; i < list.size(); i++) {
	            QunarOrderMethod qunarordermethod = new QunarOrderMethod();
	            Map map = (Map) list.get(i);
	            qunarordermethod.setQunarordernumber(map.get("OrderNumberOnline").toString());
	            qunarordermethod.setId(Long.valueOf(map.get("Id").toString()).longValue());
	            listqom.add(qunarordermethod);
	        }
	        return listqom;
	    }
}
