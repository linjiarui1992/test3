package com.ccservice.inter.servlet;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;

import com.ccservice.inter.train.listener.TrainCreatedFreshMongoMessageListener;

@SuppressWarnings("serial")
public class QueueMQ_TrainCreatedFreshMongo extends HttpServlet {

    private String mqaddress = "";// MQ地址

    private String mqusername = "";// MQ 用户名

    private String isstart = "";// 是否开启

    private int createordernum = 70;

    @Override
    public void init() throws ServletException {
        super.init();
        this.mqaddress = this.getInitParameter("mqaddress");
        this.mqusername = this.getInitParameter("mqusername");
        this.isstart = this.getInitParameter("isstart");
        String temp_createordernum = this.getInitParameter("createordernum");
        try {
            this.createordernum = Integer.parseInt(temp_createordernum);
        }
        catch (Exception e) {
            this.createordernum = 70;
        }
        if ("1".equals(isstart)) {
            System.out.println("下单成功更新Mongo队列:开启");
            orderNotice();
        }
    }

    public void orderNotice() {
        ConnectionFactory cf = new ActiveMQConnectionFactory(mqaddress);
        Connection conn = null;
        Session session = null;
        try {
            conn = cf.createConnection();
            Destination destination = new ActiveMQQueue(mqusername);
            for (int i = 0; i < this.createordernum; i++) {
                session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
                MessageConsumer consumer = session.createConsumer(destination);
                consumer.setMessageListener(new TrainCreatedFreshMongoMessageListener());
            }
            conn.start();
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
