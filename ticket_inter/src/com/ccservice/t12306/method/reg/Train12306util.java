package com.ccservice.t12306.method.reg;

import java.util.Map;
import java.io.FileReader;
import java.net.URLEncoder;
import java.io.UnsupportedEncodingException;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.mobileCode.util.RequestUtil;


public class Train12306util {
    /**
     * 登录
     */
    public static int LOGIN = 1;

    /**
     * 查询余票
     */
    public static int LEFUTICKET = 2;

    /**
     * 进入提交订单页
     */
    public static int INITDC = 3;
    /**
     * 12306动态JS值
     * @param dynamicJs，动态JS变化值，如：lnghsns
     * @param secondDynamicParam 第二次动态请求参数
     */
    public static Dynamic12306Result getDynamic(String dynamicJs, Map<String, String> oldHeader,
            String secondDynamicParam) throws Exception {
        Dynamic12306Result result = new Dynamic12306Result();
        //JS动态地址
        dynamicJs = "https://kyfw.12306.cn/otn/dynamicJs/" + dynamicJs;
        //获取动态JS内容
        String content = RequestUtil.get(dynamicJs, SupplyMethod.UTF8, oldHeader, 0);
        if (RequestUtil.StringIsNull(content)) {
            result.setKey("-1");
            result.setValue("-1");
            return result;
        }
        if (content.contains("$(document).ready")) {
            content = content.substring(content.indexOf("$(document).ready"));
        }
        //url :'/otn/dynamicJs/
        if (content.contains("/otn/dynamicJs/")) {
            if (RequestUtil.StringIsNull(secondDynamicParam)) {
                secondDynamicParam = "";
            }
            //url: '/otn/dynamicJs/svxkjqe'
            String newDynamicJs = content.split("/otn/dynamicJs/")[1];
            newDynamicJs = newDynamicJs.substring(0, newDynamicJs.indexOf("'"));
            newDynamicJs = "https://kyfw.12306.cn/otn/dynamicJs/" + newDynamicJs;
            //重新获取请求头
            Map<String, String> newHeader = RequestUtil.getRequestHeader(true, false, false);
            //PUT
            newHeader.put(SupplyMethod.HeaderContentLength, String.valueOf(secondDynamicParam.length()));
            newHeader.put(SupplyMethod.HeaderCookie, oldHeader.get(SupplyMethod.HeaderCookie));
            newHeader.put(SupplyMethod.HeaderReferer, oldHeader.get(SupplyMethod.HeaderReferer));
            newHeader.put(SupplyMethod.HeaderContentType, SupplyMethod.HeaderContentTypeValueUTF8);
            //POST
            RequestUtil.post(newDynamicJs, secondDynamicParam, SupplyMethod.UTF8, newHeader, 0);
        }
        if (content.contains("function gc()")) {
            content = content.split("function gc()")[1];
            content = content.substring(content.indexOf("'") + 1);
            String key = content.substring(0, content.indexOf("'"));
            //执行JS，获取value
            ScriptEngineManager engineManager = new ScriptEngineManager();
            ScriptEngine engine = engineManager.getEngineByName("JavaScript");
            engine.eval(new FileReader(Train12306util.class.getResource("12306.js").getPath()));
            Invocable inv = (Invocable) engine;
            String encrypt = String.valueOf(inv.invokeFunction("encrypt", "1111", key));
            String bin216 = String.valueOf(inv.invokeFunction("bin216", encrypt));
            String value = String.valueOf(inv.invokeFunction("encode32", bin216));
            //返回
            result.setKey(key);
            result.setValue(value);
        }
        if (RequestUtil.StringIsNull(result.getKey()) || RequestUtil.StringIsNull(result.getValue())) {
            result.setKey("-1");
            result.setValue("-1");
        }
        return result;
    }

    /**
     * 说明：截取HTML文件公用类 
     * @param str
     * @param str1
     * @return
     * @time 2014年8月30日 下午2:45:45
     * @author yinshubin
     */
    public static String strTools(String str, String str1) {
        if (str.contains(str1 + "\" value=\"") && str.contains("\">")) {
            String[] strs1 = str.split(str1 + "\" value=\"");
            String[] strs2 = strs1[1].split("\">");
            str1 = strs2[0];
        }
        try {
            str1 = URLEncoder.encode(str1, "UTF-8");
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return str1;
    }

    /**
     * 获取12306验证码的链接地址
     * login
     */
    private static String verificationUrl = "https://kyfw.12306.cn/otn/passcodeNew/getPassCodeNew?module=login&rand=sjrand";

    /**
     * 12306下单的时候需要的验证码
     * passenger
     */
    private static String verificationUrl_createorder = "https://kyfw.12306.cn/otn/passcodeNew/getPassCodeNew?module=passenger&rand=randp";

    private static String verificationUrl_registration = "https://kyfw.12306.cn/otn/passcodeNew/getPassCodeNew?module=regist&rand=sjrand";

    /**
     * 获取验证码下载下来并返回那个链接地址,如果type是登录，则返回地址+cookie 如果图片大小不对，返回-1
     * 
     * @param cookieString
     * @param type 登录|注册|
     * @param imageurl 图片的访问地址
     * @param dirpath  图片的存放的本地地址
     * @return 
     * @time 2014年8月30日 下午2:27:52
     * @author yinshubin
     */
    public static String verification(String cookieString, String type, String imageurl, String dirpath) {
        WriteLog.write("rep_12306_999_T12306util_verification", "HttpsUtil:verification:" + cookieString);
        WriteLog.write("rep_12306_999_T12306util_verification", type + ":verification:" + imageurl + ":" + dirpath);
        String cookieBefore = cookieString;
        String resultString = "";
        long currentTime = System.currentTimeMillis();
        if ("登录".equals(type) || "注册".equals(type)) {
            currentTime -= (int) (5000000 * Math.random() + 1);
        }
        String fileName = currentTime + ".jpg";
        if ("登录".equals(type)) {//登录
            String verificationUrlLogin = verificationUrl + "&" + currentTime;
            cookieString = HttpsUtil.Get_picture(verificationUrlLogin, dirpath, fileName, cookieString);
        }
        else if ("登录new".equals(type)) {
            //TODO 登录用到短信new
            String verificationUrlLogin = "http://localhost:28080/AccountManager/otn/passcodeNew/getPassCodeNew"
                    + "?module=login&rand=sjrand&" + currentTime;
            System.out.println(verificationUrlLogin);
            cookieString = HttpsUtil.Get_pictureHttp(verificationUrlLogin, dirpath, fileName, cookieString);
            cookieString += "EncryptionNDY2MjY0=ODZhM2ZkMTU0MzBkYWQ1OQ==";
        }
        else if ("注册".equals(type)) {
            String verificationUrlregistration = verificationUrl_registration + "&" + currentTime;
            cookieString = HttpsUtil.Get_picture(verificationUrlregistration, dirpath, fileName, "1");
        }
        else {//下单
            String verificationUrlcreatorder = verificationUrl_createorder + "&" + currentTime;
            cookieString = HttpsUtil.Get_picture(verificationUrlcreatorder, dirpath, fileName, cookieString);
        }
        resultString = imageurl + "/" + fileName;
        if ("-1".equals(cookieString)) {
            return cookieString;
        }
        if (null != cookieBefore && !(cookieBefore.equals(cookieString))) {
            resultString += cookieString;
            return resultString;
        }
        return resultString;
    }

    /**
     * 12306加密 
     * @param Encryption
     * @param type
     * @param cookie
     * @return
     * @throws Exception
     * @time 2014年12月18日 下午3:04:30
     * @author fiend
     */
    public static String getEncryption(String Encryption, int type, String cookie) throws Exception {
        Encryption = Encryption.split("dynamicJs/")[1].split("\"")[0];
        Map<String, String> hmap = null;
        if (Train12306util.LOGIN == type) {
            hmap = HttpsUtil.Spelling12306Map(false, "0", false, "https://kyfw.12306.cn/otn/login/init", cookie);
        }
        else if (Train12306util.LEFUTICKET == type) {
            hmap = HttpsUtil.Spelling12306MapGet(false, "https://kyfw.12306.cn/otn/leftTicket/init", cookie);
        }
        else if (Train12306util.INITDC == type) {
            hmap = HttpsUtil.Spelling12306MapGet(false, "https://kyfw.12306.cn/otn/confirmPassenger/initDc", cookie);
        }
        Dynamic12306Result dynamic12306result = Train12306util.getDynamic(Encryption, hmap, "");
        return "Encryption" + dynamic12306result.getKey() + "=" + dynamic12306result.getValue();
    }
}
