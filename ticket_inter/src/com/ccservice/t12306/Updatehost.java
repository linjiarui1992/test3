package com.ccservice.t12306;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 自动修改服务器的host文件里的12306的指向ip
 * 使用方法 :http://url/Reptile/updatehost?12306ip=118.186.8.222
 * @time 2015年4月8日 上午11:35:00
 * @author chendong
 */
public class Updatehost extends HttpServlet implements javax.servlet.Servlet {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String hostpath;

    @Override
    public void init() throws ServletException {
        super.init();
        this.hostpath = this.getInitParameter("hostpath");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int r1 = new Random().nextInt(10000);//获取一个随机数，用来匹配日志记录的这个流程
        //业务类型
        resp.setHeader("Access-Control-Allow-Origin", "*");
        resp.setHeader("contentType", "text/html; charset=utf-8"); //通知浏览器服务器发送的数据格式是text/html，并要求浏览器使用utf-8进行解码。
        resp.setCharacterEncoding("UTF-8");
        req.setCharacterEncoding("UTF-8");
        String data = req.getParameter("12306ip");
        String result = "-1";
        if (data != null && isboolIp(data)) {
            updatehostmethod(data);
        }
        else {
            result = "ip_err";
        }
        //print返回数据
        PrintWriter out;
        try {
            out = resp.getWriter();
            out.print(result);
            out.flush();
            out.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**  
    * 判断是否为合法IP  
    * @return the ip  
    */
    public boolean isboolIp(String ipAddress) {
        String ip = "([1-9]|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])(\\.(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])){3}";
        Pattern pattern = Pattern.compile(ip);
        Matcher matcher = pattern.matcher(ipAddress);
        return matcher.matches();
    }

    private void updatehostmethod(String newip) {
        //        hostpath = "C:\\Windows\\System32\\drivers\\etc\\hosts";
        try {
            copyFile(hostpath, hostpath + "_back_auto");//备份文件
            String data = readFileandEditfile("C:\\Windows\\System32\\drivers\\etc\\hosts", "utf-8", newip);//读取内容并替换最新的12306,hostip
            cleanfile(hostpath);//清空文件
            PrintStream(hostpath, data);//写入新的内容
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cleanfile(String hostpath) throws IOException {
        File f = new File(hostpath);
        FileWriter fw = new FileWriter(f);
        fw.write("");
        fw.close();
    }

    /**
     * 写入文件内容
     * 
     * @param hostpath 文件路径
     * @param data 写入内容
     * @time 2015年4月8日 下午12:34:41
     * @author chendong
     */
    public void PrintStream(String hostpath, String data) {
        try {
            FileOutputStream out = new FileOutputStream(hostpath);
            PrintStream p = new PrintStream(out);
            p.println(data);
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Updatehost updatehost = new Updatehost();
        updatehost.updatehostmethod("122.70.142.160");

    }

    /**
     * 读取文件内容,如果那一行是12306的host则修改为最新的ip
     * 
     * @param filePath
     * @param charsetName
     * @param new_12306ip 新的12306的ip
     * @return
     * @throws Exception
     * @time 2015年4月8日 上午11:54:43
     * @author chendong
     */
    public static String readFileandEditfile(String filePath, String charsetName, String new_12306ip) throws Exception {
        File file = new File(filePath);
        FileInputStream fis = new FileInputStream(file);
        BufferedReader br = new BufferedReader(new InputStreamReader(fis, charsetName));
        String fileContent = "";
        String temp = "";
        while ((temp = br.readLine()) != null) {
            //            System.out.println(temp);
            if (temp.indexOf("12306") >= 0) {
                temp = new_12306ip + " kyfw.12306.cn";
            }
            fileContent = fileContent + temp + "\n";
        }
        br.close();
        fis.close();
        return fileContent;
    }

    /**
     * 复制一个文件
     * 
     * @param src 老文件路径
     * @param dest 新文件路径
     * @throws IOException
     * @time 2015年4月8日 下午12:35:58
     * @author chendong
     */
    public static void copyFile(String src, String dest) throws IOException {
        FileInputStream in = new FileInputStream(src);
        File file = new File(dest);
        if (!file.exists())
            file.createNewFile();
        FileOutputStream out = new FileOutputStream(file);
        int c;
        byte buffer[] = new byte[1024];
        while ((c = in.read(buffer)) != -1) {
            for (int i = 0; i < c; i++)
                out.write(buffer[i]);
        }
        in.close();
        out.close();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }

    public String getHostpath() {
        return hostpath;
    }

    public void setHostpath(String hostpath) {
        this.hostpath = hostpath;
    }

}
