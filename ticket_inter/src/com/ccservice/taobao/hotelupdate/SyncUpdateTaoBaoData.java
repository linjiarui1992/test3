package com.ccservice.taobao.hotelupdate;

import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.HashMap;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import java.text.SimpleDateFormat;

import com.ccservice.inter.server.Server;
import com.ccservice.b2b2c.base.roomtype.Roomtype;
import com.ccservice.taobao.util.TaoBaoUpdateUtil;
import com.ccservice.b2b2c.base.hmhotelprice.Hmhotelprice;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;

/**
 * 更新淘宝数据 
 * @author WH
 */

public class SyncUpdateTaoBaoData {

    public static void sync(String data) throws Exception {
        SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //获取符合条件的淘宝卖家
        Map<String, JSONObject> buyer = TaoBaoUpdateUtil.loadbuyer();
        if (buyer.size() == 0) {
            System.out.println(timeFormat.format(new Date()) + "**********无符合条件淘宝卖家，中断**********");
            return;
        }
        StringBuffer buf = new StringBuffer();
        for (String agentId : buyer.keySet()) {
            buf.append(agentId + ",");
        }
        String agentIds = buf.toString();
        agentIds = agentIds.substring(0, agentIds.length() - 1);
        //深捷旅JSON数据
        data = URLDecoder.decode(data, "utf-8");
        //JSON
        JSONObject json = JSONObject.fromObject(data);
        //解析
        String roomId = json.getString("jlRoomId");
        JSONArray jlDatas = json.getJSONArray("jlDatas");
        int size = jlDatas.size();
        //无价格了
        if (size == 0) {
            productDown(roomId, agentIds, buyer);
        }
        else {
            productUpdate(roomId, agentIds, buyer, jlDatas, size, timeFormat);
        }
    }

    //更新商品
    private static void productUpdate(String roomId, String agentIds, Map<String, JSONObject> buyer, JSONArray jlDatas,
            int size, SimpleDateFormat timeFormat) {
        JSONObject first = jlDatas.getJSONObject(0);
        //深捷旅酒店ID
        String hotelId = first.getString("hotelId");
        String jlHotelName = first.getString("hotelName");
        int roomtypeId = first.getInt("roomtypeId");//房型ID
        String roomtypeName = first.getString("roomtypeName");//房型名称
        if (Integer.parseInt(hotelId) <= 0 || roomtypeId <= 0) {
            return;
        }
        //根据捷旅房型ID查询淘宝商品信息
        Map<String, JSONObject> goods = TaoBaoUpdateUtil.loadgoods(hotelId, roomId, agentIds);
        if (goods.size() == 0) {
            System.out.println(timeFormat.format(new Date()) + "~~~~~" + jlHotelName + "，" + roomtypeName
                    + "，没查询到淘宝商品，中断~~~~~");
            return;
        }
        //封装深捷旅价格为本地格式
        Map<String, List<Hmhotelprice>> prices = jlDataToLocalPrice(jlDatas, size);
        //可能性小
        if (prices == null || prices.size() == 0) {
            //下架
            productDown(roomId, agentIds, buyer);
            return;
        }
        //循环卖家商品
        for (String agentId : goods.keySet()) {
            try {
                JSONObject good = goods.get(agentId);
                long localHotelId = Long.parseLong(good.getString("hotelId"));
                String hotelName = good.getString("hotelName");
                long localRoomId = Long.parseLong(good.getString("roomId"));
                String roomName = good.getString("roomName");
                int bed = Integer.parseInt(good.getString("bed"));
                String prod = good.getString("prod");
                //String bf = good.getString("bf");
                String trId = good.getString("trId");
                long gid = Long.parseLong(good.getString("gid"));
                //String status = good.getString("status");
                double addprice = Double.parseDouble(good.getString("addprice"));
                //String modifytime = good.getString("modifytime");
                //房型
                Roomtype roomtype = new Roomtype();
                roomtype.setId(localRoomId);
                //酒店商品名称
                String title = hotelName.trim() + " - " + roomName.trim();
                if (title.length() > 60) {
                    title = roomName.trim();
                }
                roomtype.setName(title);
                roomtype.setBed(bed);
                roomtype.setHotelid(localHotelId);
                //获取价格
                List<Hmhotelprice> priceList = prices.get(prod);
                //取第一个套餐价格
                if (priceList == null || priceList.size() == 0) {
                    for (String key : prices.keySet()) {
                        priceList = prices.get(key);
                        break;
                    }
                }
                List<Hmhotelprice> newList = new ArrayList<Hmhotelprice>();
                for (Hmhotelprice price : priceList) {
                    price.setHotelid(localHotelId);
                    price.setRoomtypeid(localRoomId);
                    double tempPirce = price.getPrice();
                    //加价
                    if (addprice > 0) {
                        //0：不加价；1：固定值；2：比例
                        String mode = String.valueOf(addprice).substring(0, 1);
                        //实际加价
                        double realAddprice = Double.parseDouble(String.valueOf(addprice).substring(1));
                        if ("1".equals(mode)) {
                            tempPirce = ElongHotelInterfaceUtil.add(tempPirce, realAddprice);
                        }
                        else if ("2".equals(mode)) {
                            Double tempprice = ElongHotelInterfaceUtil.multiply(tempPirce, realAddprice * 0.01);
                            tempPirce = ElongHotelInterfaceUtil.add(tempPirce, tempprice.intValue());
                        }
                        price.setPrice(tempPirce);
                    }
                    newList.add(price);
                }
                newList = priceOptimize(newList);
                //agentName
                String agentName = buyer.get(agentId).getString("agentName");
                //sessionKey
                String sessionKey = buyer.get(agentId).getString("sessionKey");
                //请求淘宝
                reqTaoBao(gid, roomtype, newList, sessionKey, trId, agentName, hotelName, roomName);
            }
            catch (Exception e) {
            }
        }
    }

    private static void reqTaoBao(long gid, Roomtype roomtype, List<Hmhotelprice> prices, String sessionKey,
            String trId, String agentName, String hotelName, String roomName) throws Exception {
        SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //request taobao
        String json = Server.getInstance().getTaoBaoHotelSerice()
                .productUpdate(gid, roomtype, prices, null, sessionKey);
        JSONObject obj = JSONObject.fromObject(json);
        if (obj.containsKey("hotel_room_update_response")) {
            obj = obj.getJSONObject("hotel_room_update_response");
            obj = obj.getJSONObject("room");
            long status = obj.getLong("status");
            String statusStr = "";
            if (status == 1) {
                statusStr = "上架";
            }
            else if (status == 2) {
                statusStr = "下架";
            }
            else if (status == 3) {
                statusStr = "删除";
            }
            else {
                statusStr = String.valueOf(status);
            }
            System.out.println(timeFormat.format(new Date()) + "  " + agentName + "，" + hotelName + "，" + roomName
                    + "，商品更新成功，状态为" + statusStr);
        }
        else if (obj.containsKey("error_response")) {
            JSONObject errorObj = obj.getJSONObject("error_response");
            String code = errorObj.containsKey("sub_code") ? errorObj.getString("sub_code") : "";//错误码
            String desc = errorObj.containsKey("sub_msg") ? errorObj.getString("sub_msg") : "";//错误描述
            //商品已删除、商品查询失败
            if (("isv.room-error:ROOM_DELETE".equals(code) && desc.equals(gid + "商品已删除"))
                    || ("isv.ic-error:ROOM_GET_ERROR".equals(code) && "商品查询失败".equals(desc))) {
                String delSql = "update T_TAOBAOROOMTYPE set C_IID=null,C_GID=null,C_GOODSSTATUS=null,C_GODDSCREATEUSERID=null,"
                        + "C_GOODSCREATETIME=null,C_GOODSMODIFYTIME=null,C_LOCALPROD=null,C_LOCALBF=null,C_ADDPRICE=null where ID = "
                        + trId;// + " and C_TAOBAOTABLEID = " + taoBaoRoomType.getTaobaotableid();
                Server.getInstance().getSystemService().findMapResultBySql(delSql, null);
                System.out.println(timeFormat.format(new Date()) + "  " + agentName + "，" + roomName + "，商品查询失败，置空商品。");
            }
        }
    }

    //本地价格优化为淘宝规定数据
    private static List<Hmhotelprice> priceOptimize(List<Hmhotelprice> prices) throws Exception {
        //第一个价格
        Hmhotelprice firstPrice = prices.get(0);
        //最后有价格日期
        String lastDate = "";
        //某天重复，后续不传
        String currentDate = ElongHotelInterfaceUtil.getCurrentDate();
        Map<String, Hmhotelprice> tempMap = new LinkedHashMap<String, Hmhotelprice>();//key：日期
        for (Hmhotelprice p : prices) {
            String key = p.getStatedate();
            //商品价格重复、重复的关房
            if (tempMap.containsKey(key)) {
                Hmhotelprice repeat = tempMap.get(key);//第一个重复数据
                repeat.setRoomstatus(1l);//关房
                tempMap.put(key, repeat);//覆盖第一个重复
                break;
            }
            lastDate = key;
            tempMap.put(key, p);
        }
        //取连续价格
        List<Hmhotelprice> newList = new ArrayList<Hmhotelprice>();
        //有价格天数
        int days = ElongHotelInterfaceUtil.getSubDays(currentDate, lastDate) + 1;
        for (int i = 0; i < days; i++) {
            String current = ElongHotelInterfaceUtil.getAddDate(currentDate, i);
            //存在价格
            if (tempMap.containsKey(current)) {
                newList.add(tempMap.get(current));
            }
            //不存在价格
            else {
                Hmhotelprice temp = TaoBaoUpdateUtil.copyprice(firstPrice);
                temp.setRoomstatus(1l);//关房
                temp.setStatedate(current);
                newList.add(temp);
            }
        }
        if (newList.size() == 0) {
            Hmhotelprice temp = TaoBaoUpdateUtil.copyprice(firstPrice);
            temp.setRoomstatus(1l);//关房
            temp.setStatedate(currentDate);
            newList.add(temp);
        }
        if (newList.size() == 1) {
            Hmhotelprice temp = TaoBaoUpdateUtil.copyprice(firstPrice);
            temp.setRoomstatus(1l);//关房
            temp.setStatedate(ElongHotelInterfaceUtil.getAddDate(currentDate, 1));
            newList.add(temp);
        }
        return newList;
    }

    //封装深捷旅价格为本地格式
    private static Map<String, List<Hmhotelprice>> jlDataToLocalPrice(JSONArray jlDatas, int size) {
        //key: prod
        Map<String, List<Hmhotelprice>> prices = new HashMap<String, List<Hmhotelprice>>();
        for (int i = 0; i < size; i++) {
            //解析
            JSONObject room = jlDatas.getJSONObject(i);
            if (!room.containsKey("roomPriceDetail")) {
                continue;
            }
            //客户类型、市场：11 所有市场 12 中国大陆市场 13 日本市场 14 香港市场 15 俄罗斯市场 16 澳门市场，先看不适用市场，为空再看适用市场
            String noacceptcustomer = room.containsKey("noacceptcustomer") ? room.getString("noacceptcustomer") : "";
            if ("11".equals(noacceptcustomer) || "12".equals(noacceptcustomer)) {
                continue;
            }
            //价格明细
            JSONArray roomPriceDetail = room.getJSONArray("roomPriceDetail");
            int priceSize = roomPriceDetail.size();
            for (int j = 0; j < priceSize; j++) {
                //解析价格
                JSONObject price = roomPriceDetail.getJSONObject(j);
                //11:现付，12:预付
                String pricingtype = price.getString("pricingtype");
                //货币
                String currency = price.getString("currency");
                if (!"12".equals(pricingtype) || !"RMB".equals(currency)) {
                    continue;
                }
                //预订条款类型:提前订房、指定日期前、连住晚数、指定时间段，11：提前预订 12：指定日期前订 13：连住晚数 14：指定时间段能订
                int termtype = price.containsKey("termtype") ? price.getInt("termtype") : 0;
                if (termtype == 11 || termtype == 12 || termtype == 13 || termtype == 14) {
                    continue;
                }
                //同行标准价
                double preeprice = price.getDouble("preeprice");
                if (preeprice <= 0) {
                    continue;
                }
                //日期，"night": "2014-10-01 12:00:00"
                String night = price.getString("night").split(" ")[0];
                //套餐ID
                String ratetype = price.getString("ratetype");
                String ratetypename = price.getString("ratetypename");
                //早餐
                String bf = price.getString("includebreakfastqty2");
                if (ElongHotelInterfaceUtil.StringIsNull(ratetype) || ElongHotelInterfaceUtil.StringIsNull(bf)) {
                    continue;
                }
                //当前可售房间数量，大于0表示可即时确认；等于0表示需要等待确认；小于0表示满房 
                long roomnum = price.getLong("qtyable");
                //房态 0：开房；1：满房
                long roomstatus = roomnum < 0 ? 1 : 0;
                //封装
                Hmhotelprice localPrice = new Hmhotelprice();
                localPrice.setStatedate(night);
                localPrice.setPrice(preeprice);
                localPrice.setProd(ratetype);
                localPrice.setRatetype(ratetypename);
                localPrice.setYuliuNum(roomnum);
                localPrice.setRoomstatus(roomstatus);
                localPrice.setBf(TaoBaoUpdateUtil.bfToLocal(bf));
                //当前套餐价格
                List<Hmhotelprice> priceList = prices.get(ratetype);
                if (priceList == null) {
                    priceList = new ArrayList<Hmhotelprice>();
                }
                //ADD
                priceList.add(localPrice);
                prices.put(ratetype, priceList);
            }
        }
        return prices;
    }

    //无价格、商品下架
    private static void productDown(String roomId, String agentIds, Map<String, JSONObject> buyer) {
        SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //Print
        System.out.println(timeFormat.format(new Date()) + "=====" + roomId + "，深捷旅房型无价格=====");
        //根据捷旅房型ID查询淘宝商品信息
        Map<String, JSONObject> goods = TaoBaoUpdateUtil.loadgoods("", roomId, agentIds);
        for (String agentId : goods.keySet()) {
            JSONObject obj = buyer.get(agentId);
            String agentName = obj.getString("agentName");
            String sessionKey = obj.getString("sessionKey");
            //商品
            JSONObject good = goods.get(agentId);
            String hotelName = good.getString("hotelName");
            String roomName = good.getString("roomName");
            //            String status = good.getString("status");
            //已是下架状态
            //            if ("2".equals(status)) {
            //                System.out.println(timeFormat.format(new Date()) + "  " + agentName + "，" + hotelName + "，" + roomName
            //                        + "[ID：" + roomId + "]，无价格，商品已下架，无需请求淘宝。");
            //            }
            //上架状态，进行下架
            //            else {
            String trId = good.getString("trId");
            String gid = good.getString("gid");
            TaoBaoUpdateUtil.taobaoDown(trId, gid, sessionKey, agentName, hotelName, roomName, timeFormat);
            //            }
        }
    }
}