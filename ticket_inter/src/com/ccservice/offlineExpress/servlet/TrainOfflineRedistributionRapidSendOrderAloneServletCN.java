package com.ccservice.offlineExpress.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.quartz.JobDetail;
import org.quartz.SchedulerException;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.PropertyUtil;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.offline.dao.MailAddressDao;
import com.ccservice.offline.dao.TrainOrderOfflineDao;
import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.util.ExceptionTCUtil;
import com.ccservice.offline.util.ExceptionTNUtil;
import com.ccservice.offline.util.SchedulerUtil;
import com.ccservice.offline.util.TrainOrderOfflineUtil;
import com.ccservice.offlineExpress.service.TrainOfflineRedistributionRapidSendOrderAloneOvertime;
import com.ccservice.offlineExpress.service.TrainOfflineRedistributionRapidSendSecondOrderAloneOvertime;
import com.ccservice.offlineExpress.uupt.UUptService;

/**
 * @className: com.ccservice.offlineExpress.servlet.TrainOfflineRedistributionRapidSendServletCN
 * @description: TODO - 
 * 
 * 这个接口需要处理以下事情
 * 
 * 需要满足二次及多次下单的匹配处理
 * 
 * 需要更新相关的邮寄信息-最终全部默认以顺丰进行回调
 * 
 * 需要更新相关的快递时效展示信息和相关位置的快递的逻辑以及相关订单的闪送标识，由代售点后期手动完成分单
 * 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年11月13日 下午3:58:26 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOfflineRedistributionRapidSendOrderAloneServletCN extends HttpServlet {
    private static final String LOGNAME = "闪送单独下单请求接口-CN发起";

    private int r1 = new Random().nextInt(10000000);

    private TrainOrderOfflineDao trainOrderOfflineDao = new TrainOrderOfflineDao();

    private MailAddressDao mailAddressDao = new MailAddressDao();

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();

        String orderId = request.getParameter("orderId");//系统表的主键的订单表的ID
        String userid = request.getParameter("userid");//当前系统的登录用户的ID - 用于记录操作日志

        Long orderIdl = Long.valueOf(orderId);
        Integer useridi = Integer.valueOf(userid);

        //记录请求信息日志
        WriteLog.write(LOGNAME, r1 + ":闪送单独下单请求信息-orderId-->" + orderIdl + ",userid-->" + useridi);

        //记录日志
        TrainOrderOfflineUtil.CreateBusLogAdminOtherDealResult(orderIdl, useridi, "发起闪送单独下单请求", 222);

        JSONObject result = rapidSendOrderAloneCN(orderIdl, useridi);

        WriteLog.write(LOGNAME, r1 + ":闪送单独下单请求结果-result" + result);

        out.print(result.toJSONString());
        out.flush();
        out.close();
    }

    private JSONObject rapidSendOrderAloneCN(Long orderIdl, Integer useridi) {
        JSONObject resResult = new JSONObject();

        WriteLog.write(LOGNAME,
                r1 + ":闪送单独下单请求-进入到方法-rapidSendOrderAloneCN:orderIdl-->" + orderIdl + ",useridi-->" + useridi);

        if (orderIdl == null || orderIdl == 0L) {
            resResult.put("success", "false");
            String errorMsg = "传入的订单号有误，请排查";
            resResult.put("msg", errorMsg);
            TrainOrderOfflineUtil.CreateBusLogAdminOtherDealResult(orderIdl, useridi, "闪送单独下单失败-" + errorMsg, 222);
            return resResult;
        }

        TrainOrderOffline trainOrderOffline = null;
        try {
            trainOrderOffline = trainOrderOfflineDao.findTrainOrderOfflineById(orderIdl);
        }
        catch (Exception e) {
            return ExceptionTCUtil.handleTCException(e);
        }

        if (trainOrderOffline == null) {
            resResult.put("success", "false");
            String errorMsg = "该订单不存在，请排查";
            resResult.put("msg", errorMsg);
            TrainOrderOfflineUtil.CreateBusLogAdminOtherDealResult(orderIdl, useridi, "闪送单独下单失败-" + errorMsg, 222);
            return resResult;
        }

        //一般不会出现上述原因

        String orderNumber = trainOrderOffline.getOrderNumber();
        resResult.put("orderid", orderNumber);

        /*if (trainOrderOffline.getAgentId() != 1) {
            resResult.put("success", "false");
            String errorMsg = "该订单并非待分配订单，请勿操作次按钮";
            resResult.put("msg", errorMsg);
            TrainOrderOfflineUtil.CreateBusLogAdminOtherDealResult(orderIdl, useridi, "闪送单独下单失败-" + errorMsg, 222);
            return resResult;
        }*/

        //由于出现了 同程的二次进单的代码，因而，在闪送-锁单的操作中，需要 判定，该单是否已经做过二次下单的操作
        Boolean isSecondRapidSend = TrainOrderOfflineUtil.getIsSecondOrderRapidSend(trainOrderOffline.getOrderNumber());//判断闪送单是否是二次订单 - 

        UUptService uuptService = new UUptService();
        JSONObject responseJson = new JSONObject();
        JSONObject data = new JSONObject();
        data.put("orderId", orderIdl);
        try {
            if (isSecondRapidSend) {//二次下单
                TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "闪送单独下单-发起闪送二次下单请求");
                responseJson = uuptService.operate(LOGNAME, "uupt", r1, "addOrderAgain", data, responseJson);
            }
            else {//一次取消
                TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "闪送单独下单-发起闪送下单请求");
                responseJson = uuptService.operate(LOGNAME, "uupt", r1, "addOrder", data, responseJson);
            }
        }
        catch (Exception e) {
            ExceptionTNUtil.handleTNException(e);
        }
        //记录请求信息日志
        WriteLog.write(LOGNAME, r1 + ":闪送单独下单请求反馈信息-responseJson" + responseJson);

        /**
         * 
        {"message":"成功","result":{"partnerOrderNumber":"U37908001710310949679202216","message":"订单发布成功","orderNumber":"","success":true},"success":true}
         * 
        {"message":"请求发生异常","result":"","success":false}
        
        下单成功之后，进行价格的设计展示，让客服进行闪送单的价格获取的二次判定 - 但是此处的接口不反馈价格的相关信息，所以必须确保重新分配的代售点的名称必须保持一致
         * 
         */
        //Boolean responseB = responseJson.getBooleanValue("success");
        Boolean responseB = false;
        JSONObject responseJsonResult = new JSONObject();
        if (responseJson.getBooleanValue("success")) {
            responseJsonResult = responseJson.getJSONObject("result");
            responseB = responseJsonResult.getBooleanValue("success");
        }

        if (responseB) {//闪送下单成功
            //需要更新相关的快递时效展示信息和相关位置的快递的逻辑以及相关订单的闪送标识，由代售点后期手动完成分单

            /**
             * 需要更新相关的快递时效展示信息以及相关订单的闪送标识
             * 
             * 20171023-新增需求 - 是否是闪送订单  0: 普通订单,1: 闪送订单 - 默认值是 0 通过快递类型进行判定 - 目前只有邮寄票会是闪送订单
             * 
             * 快递信息描述,例如："如果2017-08-16 18:00:00正常发件。快递类型为:标准快递。快递预计到达时间:2017-08-17 18:00:00,2017-08-17 18:00:00。以上时效为预计到达时间，仅供参考，精准时效以运单追踪结果中的“预计到达时间”为准。"
             */

            //获取UU跑腿的快递时效信息并做下一步处理
            /*Integer agentid = trainOrderOffline.getAgentId();
            String totalAddress = "";
            try {
                totalAddress = mailAddressDao.findADDRESSCITYNAMEByORDERID(orderIdl).getADDRESS();
            }
            catch (Exception e2) {
                WriteLog.write(LOGNAME, r1 + ":邮寄信息获取失败-e2:" + e2);
            
                ExceptionTNUtil.handleTNException(e2);
            }
            String delieveStr = DelieveUtils.getUUptDelieveStr(String.valueOf(agentid), totalAddress);//
            trainOrderOfflineDao.updateIsRapidSendExpressDeliverById(orderIdl, 1, delieveStr);*///

            //下单成功之后，需要做快递单号的修改 - //0-顺丰 - 1-EMS 2-宅急送 3-京东 - 99配送到站 10-闪送【UU跑腿】
            String ExpressNum = responseJson.getJSONObject("result").getString("partnerOrderNumber");
            WriteLog.write(LOGNAME, r1 + ":闪送下单成功-更改闪送快递单号-ExpressNum:" + ExpressNum + ",orderId:" + orderIdl);
            mailAddressDao.updateExpressNumByORDERID(orderIdl.intValue(), ExpressNum);

            /**
             * 如果想模拟成为同步反馈的结果，需要判定是否收到异步的锁单反馈 - 且不允许再次锁单 - 需要加上一个标志位
             * 
             * 途牛的锁票的异步转同步的内部等待时间 - 以s为单位
             */
            String LockRapidSendWait = PropertyUtil.getValue("TrainTongChengOfflineLockRapidSendWait",
                    "Train.GuestAccount.properties");

            //毫秒数
            Long overtime = new Date().getTime() + Integer.valueOf(LockRapidSendWait) * 60 * 1000;

            String lockWaitDateTime = TrainOrderOfflineUtil.getTimestrByTime(overtime);
            //记录日志
            TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi,
                    "闪送单独下单请求成功，等待UU跑腿异步反馈结果，目前等待超时的设置时间是:" + LockRapidSendWait + "min，超时后自动访问UU跑腿取消订单的接口，不涉及其它状态值的变动");

            //此处另起JOB，完成5分钟之后的拒单操作

            String year = lockWaitDateTime.substring(0, 4);
            String month = lockWaitDateTime.substring(5, 7);
            String day = lockWaitDateTime.substring(8, 10);
            String hour = lockWaitDateTime.substring(11, 13);
            String minute = lockWaitDateTime.substring(14, 16);
            String second = lockWaitDateTime.substring(17, 19);

            //"40 17 14 25 08 ? 2017"
            String cronExpression = second + " " + minute + " " + hour + " " + day + " " + month + " ? " + year;

            //System.out.println(cronExpression);

            WriteLog.write(LOGNAME, r1 + ":闪送单独下单请求启动定时任务-cronExpression" + cronExpression);

            //实际的锁单请求中 - 可能会出现 8 min 内，原有的锁票的任务 还未执行的情况下，发起了后续的锁单请求的交互结果 - 所以此处的定时任务的判定逻辑需要做相关的修改

            /**
             * 后期涉及的东西
             * 要求在自动发起取消的同时，结束相关位置的定时任务 - 正常流程中排除了下述 jobDetail!=null 的情况的出现
             */
            try {
                String jobFlag = "TrainOfflineRedistributionRapidSendOrderAloneOvertime";
                JobDetail jobDetail = SchedulerUtil.getScheduler()
                        .getJobDetail(jobFlag + "Job" + trainOrderOffline.getId(), jobFlag + "JobGroup");
                if (jobDetail != null) {
                    WriteLog.write(LOGNAME, r1 + ":闪送单独下单请求启动定时任务-jobDetail:" + jobDetail.getFullName());

                    resResult.put("success", "false");
                    resResult.put("msg", "闪送单独下单请求的定时取消任务已存在，处理失败");
                    return resResult;
                }
                else {
                    WriteLog.write(LOGNAME, r1 + ":闪送单独下单请求启动定时任务-jobDetail:" + null);

                    //创建一个定时任务，并在指定的时间点进行启动
                    try {
                        if (isSecondRapidSend) {//如果是二次订单的话，需要走二次取消的相关逻辑
                            SchedulerUtil.startLockScheduler(orderIdl, useridi, LockRapidSendWait, cronExpression,
                                    jobFlag, TrainOfflineRedistributionRapidSendSecondOrderAloneOvertime.class);
                        }
                        else {//一次取消
                            SchedulerUtil.startLockScheduler(orderIdl, useridi, LockRapidSendWait, cronExpression,
                                    jobFlag, TrainOfflineRedistributionRapidSendOrderAloneOvertime.class);
                        }
                    }
                    catch (Exception e) {
                        return ExceptionTCUtil.handleTCException(e);
                    }

                    TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "闪送单独下单请求成功");

                    resResult.put("success", "true");
                    resResult.put("msg", "闪送单独下单请求成功");
                    return resResult;
                }
            }
            catch (SchedulerException e1) {
                TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "闪送单独下单请求失败，原因异常，请联系技术处理");//记录日志

                resResult.put("success", "true");
                resResult.put("msg", "闪送单独下单请求-定时任务出现异常");
                return resResult;
            }
        }
        else {//闪送下单失败 - 不更改任何内容 - 系统问题
            String errorMsg = responseJsonResult.getString("message");
            if (errorMsg == null || "".equals(errorMsg)) {
                errorMsg = responseJson.getString("message");
            }
            errorMsg = "闪送单独下单-该订单UU跑腿下单失败:" + errorMsg + "-请联系技术处理";
            TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, errorMsg);

            resResult.put("success", "false");
            resResult.put("msg", errorMsg);
            return resResult;
        }
    }

    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        Long orderIdl = 1290449L;
        Integer useridi = 62;

        TrainOfflineRedistributionRapidSendOrderAloneServletCN trainOfflineRedistributionRapidSendOrderAloneServletCN = new TrainOfflineRedistributionRapidSendOrderAloneServletCN();
        JSONObject result = trainOfflineRedistributionRapidSendOrderAloneServletCN.rapidSendOrderAloneCN(orderIdl,
                useridi);

        System.out.println(result);

        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间： " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间： " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间： " + runTimeS / 60 + "min：" + runTimeS % 60 + "s");//分
    }
}
