package com.ccservice.offlineExpress.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.offline.dao.MailAddressDao;
import com.ccservice.offline.dao.T_CUSTOMERAGENTDao;
import com.ccservice.offline.dao.TrainOrderOfflineDao;
import com.ccservice.offline.dao.TrainTicketOfflineDao;
import com.ccservice.offline.domain.MailAddress;
import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.util.DelieveUtils;
import com.ccservice.offline.util.ExceptionTCUtil;
import com.ccservice.offline.util.TrainOrderOfflineUtil;

/**
 * @className: com.ccservice.offlineExpress.servlet.TrainOfflineRedistributionRapidSendServletCN
 * @description: TODO - 
 * 
 * 这个接口需要处理以下事情
 * 
 * 1、判定是否是闪送单， - 目前只针对淘宝进行相关的测试
 * 
 *      如果不是，做反馈，在平台做提示
 *      
 *      如果是，需要直接修改标识之后做判定 - 
 *      【
 *          现有流程是，由客服询问最优匹配的代售点去询问是否有票，如果有票的话，
 *          
 *          点击新的闪送下单按钮，替换数据库中的相关字段标识，下单成功之后，指定代售点的分配，由客服手工操作完成
 *          
 *      】
 * 
 * 2、
 *      只要是分配到了订单未分配的单子，都可以点击该按钮进行该单进行相关的判定和提示
 * 
 * 直接分配给代售点
 * 
 * 闪送标识分配，还原相关的反馈字段标识
 * 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年11月13日 下午3:58:26 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOfflineRedistributionRapidSendServletCN extends HttpServlet {
    private static final String LOGNAME = "重新分配的闪送逻辑请求接口-CN发起";

    private int r1 = new Random().nextInt(10000000);

    private TrainOrderOfflineDao trainOrderOfflineDao = new TrainOrderOfflineDao();

    private MailAddressDao mailAddressDao = new MailAddressDao();

    private TrainTicketOfflineDao trainTicketOfflineDao = new TrainTicketOfflineDao();

    private T_CUSTOMERAGENTDao t_CUSTOMERAGENTDao = new T_CUSTOMERAGENTDao();

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();

        String orderId = request.getParameter("orderId");//系统表的主键的订单表的ID
        String userid = request.getParameter("userid");//当前系统的登录用户的ID - 用于记录操作日志

        Long orderIdl = Long.valueOf(orderId);
        Integer useridi = Integer.valueOf(userid);

        //记录请求信息日志
        WriteLog.write(LOGNAME, r1 + ":重新分配的闪送逻辑请求信息-orderId-->" + orderIdl + ",userid-->" + useridi);

        //记录日志
        TrainOrderOfflineUtil.CreateBusLogAdminOtherDealResult(orderIdl, useridi, "发起闪送匹配请求", 222);

        JSONObject result = matchRapidSendCN(orderIdl, useridi);

        WriteLog.write(LOGNAME, r1 + ":重新分配的闪送逻辑请求结果-result" + result);

        out.print(result.toJSONString());
        out.flush();
        out.close();
    }

    private JSONObject matchRapidSendCN(Long orderIdl, Integer useridi) {
        JSONObject resResult = new JSONObject();

        WriteLog.write(LOGNAME,
                r1 + ":重新分配的闪送逻辑请求-进入到方法-matchRapidSendCN:orderIdl-->" + orderIdl + ",useridi-->" + useridi);

        if (orderIdl == null || orderIdl == 0L) {
            resResult.put("success", "false");
            String errorMsg = "传入的订单号有误，请排查";
            resResult.put("msg", errorMsg);
            TrainOrderOfflineUtil.CreateBusLogAdminOtherDealResult(orderIdl, useridi, "闪送匹配失败-" + errorMsg, 222);
            return resResult;
        }

        TrainOrderOffline trainOrderOffline = null;
        try {
            trainOrderOffline = trainOrderOfflineDao.findTrainOrderOfflineById(orderIdl);
        }
        catch (Exception e) {
            return ExceptionTCUtil.handleTCException(e);
        }

        if (trainOrderOffline == null) {
            resResult.put("success", "false");
            String errorMsg = "该订单不存在，请排查";
            resResult.put("msg", errorMsg);
            TrainOrderOfflineUtil.CreateBusLogAdminOtherDealResult(orderIdl, useridi, "闪送匹配失败-" + errorMsg, 222);
            return resResult;
        }

        //一般不会出现上述原因

        String orderNumber = trainOrderOffline.getOrderNumber();
        resResult.put("orderid", orderNumber);

        if (trainOrderOffline.getAgentId() != 1) {
            resResult.put("success", "false");
            String errorMsg = "该订单并非待分配订单，请勿操作次按钮";
            resResult.put("msg", errorMsg);
            TrainOrderOfflineUtil.CreateBusLogAdminOtherDealResult(orderIdl, useridi, "闪送匹配失败-" + errorMsg, 222);
            return resResult;
        }

        MailAddress mailAddress = null;
        try {
            mailAddress = mailAddressDao.findADDRESSCITYNAMEByORDERID(orderIdl);
        }
        catch (Exception e1) {
            resResult = ExceptionTCUtil.handleTCException(e1);
            String errorMsg = "邮寄信息对象无法获取";
            resResult.put("msg", errorMsg);
            TrainOrderOfflineUtil.CreateBusLogAdminOtherDealResult(orderIdl, useridi, "闪送匹配失败-" + errorMsg, 222);
            return resResult;
        }

        if (mailAddress == null) {
            resResult.put("success", "false");
            String errorMsg = "该订单的邮寄信息获取失败";
            resResult.put("msg", errorMsg);
            TrainOrderOfflineUtil.CreateBusLogAdminOtherDealResult(orderIdl, useridi, "闪送匹配失败-" + errorMsg, 222);
            return resResult;
        }

        //是 出发时间,格式:2017-08-24 13:15:00 - yyyy-MM-dd HH:mm:ss
        String departTime = null;
        try {
            departTime = trainTicketOfflineDao.findDepartTimeByOrderId(orderIdl);
        }
        catch (Exception e1) {
            resResult = ExceptionTCUtil.handleTCException(e1);
            String errorMsg = "订单的发车日期无法获取";
            resResult.put("msg", errorMsg);
            TrainOrderOfflineUtil.CreateBusLogAdminOtherDealResult(orderIdl, useridi, "闪送匹配失败-" + errorMsg, 222);
            return resResult;
        }

        if (departTime == null) {
            resResult.put("success", "false");
            resResult.put("msg", "该订单的发车日期获取失败");
            return resResult;
        }

        String isRapidSendDepartTime = departTime.substring(0, departTime.indexOf("."));

        String totalAddress = mailAddress.getADDRESS();
        JSONObject isRapidSendResult = TrainOrderOfflineUtil.isRapidSend(orderNumber, totalAddress, null,
                isRapidSendDepartTime);

        Boolean isRapidSendB = isRapidSendResult.getBooleanValue("isRapidSend");
        if (!isRapidSendB) {// 0: 普通订单,1: 闪送订单
            resResult.put("success", "false");
            String errorMsg = "该订单不符合闪送业务要求";
            resResult.put("msg", errorMsg);
            TrainOrderOfflineUtil.CreateBusLogAdminOtherDealResult(orderIdl, useridi, "闪送匹配失败-" + errorMsg, 222);
        }
        else {
            /**
             * result.put("isRapidSend", true);
            result.put("need_paymoney", need_paymoney);
            result.put("agentId", agentId);
             * 
             */
            String agentId = isRapidSendResult.getString("agentId");

            /**
             * 闪送匹配成功之后，直接分配给相关的代售点 - 
             * 
             * 更新相关的闪送单的标识 - 
             * 更新闪送单的快递时效
             */

            //获取UU跑腿的快递时效信息并做下一步处理
            String delieveStr = DelieveUtils.getUUptDelieveStr(agentId, totalAddress);//
            trainOrderOfflineDao.updateAgentIdIsRapidSendAndCallbackExpressDeliverById(orderIdl,
                    Integer.valueOf(agentId), 1, delieveStr);

            WriteLog.write(LOGNAME,
                    r1 + ":闪送单独下单的代售点的信息:" + agentId + ",邮寄的目的地址:" + totalAddress + ",UU跑腿快递时效:" + delieveStr);

            //更新快递类型为闪送 - //0-顺丰 - 1-EMS 2-宅急送 3-京东 - 99配送到站 10-闪送【UU跑腿】
            mailAddressDao.updateExpressAgentByORDERID(orderIdl.intValue(), 10);

            String agentName = null;
            try {
                agentName = t_CUSTOMERAGENTDao.findC_AGENTCOMPANYNAMEByID(Double.valueOf(agentId));
            }
            catch (Exception e) {
                resResult = ExceptionTCUtil.handleTCException(e);
                String errorMsg = "代售点的名称获取失败";
                resResult.put("msg", errorMsg);
                TrainOrderOfflineUtil.CreateBusLogAdminOtherDealResult(orderIdl, useridi, "闪送匹配失败-" + errorMsg, 222);
                return resResult;
            }

            if (agentName == null) {
                agentName = "未知采购，请联系技术处理";
            }

            resResult.put("success", "true");
            resResult.put("agentId", agentId);
            resResult.put("agentName", agentName);

            Double rapidSendPrice = isRapidSendResult.getDouble("need_paymoney");
            resResult.put("rapidSendPrice", rapidSendPrice);

            String errorMsg = "agentId-" + agentId + ",agentName-" + agentName + ",rapidSendPrice-" + rapidSendPrice;
            resResult.put("msg", errorMsg);
            TrainOrderOfflineUtil.CreateBusLogAdminOtherDealResult(orderIdl, useridi, "闪送匹配成功-" + errorMsg, 222);

            TrainOrderOfflineUtil.CreateBusLogAdminOtherDealResult(orderIdl, useridi,
                    "该闪送单已分配给最优匹配出票点并更新了快递时效信息和快递类型，请抓紧联系代售点锁单出票", 222);
        }
        return resResult;
    }

    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        Long orderIdl = 1290487L;
        Integer useridi = 62;

        TrainOfflineRedistributionRapidSendServletCN trainOfflineRedistributionRapidSendServletCN = new TrainOfflineRedistributionRapidSendServletCN();
        JSONObject result = trainOfflineRedistributionRapidSendServletCN.matchRapidSendCN(orderIdl, useridi);

        System.out.println(result);

        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间： " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间： " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间： " + runTimeS / 60 + "min：" + runTimeS % 60 + "s");//分
    }
}
