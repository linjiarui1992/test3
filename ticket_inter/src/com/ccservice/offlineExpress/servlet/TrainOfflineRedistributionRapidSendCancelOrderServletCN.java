package com.ccservice.offlineExpress.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.quartz.JobDetail;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.offline.dao.MailAddressDao;
import com.ccservice.offline.dao.TrainOrderOfflineDao;
import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.util.ExceptionTCUtil;
import com.ccservice.offline.util.ExceptionTNUtil;
import com.ccservice.offline.util.SchedulerUtil;
import com.ccservice.offline.util.TrainOrderOfflineUtil;
import com.ccservice.offlineExpress.uupt.UUptService;

/**
 * @className: com.ccservice.offlineExpress.servlet.TrainOfflineRedistributionRapidSendServletCN
 * @description: TODO - 
 * 
 * 这个接口需要处理以下事情
 * 
 * 需要满足二次及多次下单的匹配处理
 * 
 * 需要更新相关的邮寄信息-最终全部默认以顺丰进行回调
 * 
 * 需要更新相关的快递时效展示信息和相关位置的快递的逻辑以及相关订单的闪送标识，由代售点后期手动完成分单
 * 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年11月13日 下午3:58:26 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOfflineRedistributionRapidSendCancelOrderServletCN extends HttpServlet {
    private static final String LOGNAME = "闪送单独下单的取消接口-CN发起";

    private int r1 = new Random().nextInt(10000000);

    private TrainOrderOfflineDao trainOrderOfflineDao = new TrainOrderOfflineDao();

    private MailAddressDao mailAddressDao = new MailAddressDao();

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();

        String orderId = request.getParameter("orderId");//系统表的主键的订单表的ID
        String userid = request.getParameter("userid");//当前系统的登录用户的ID - 用于记录操作日志

        Long orderIdl = Long.valueOf(orderId);
        Integer useridi = Integer.valueOf(userid);

        //记录请求信息日志
        WriteLog.write(LOGNAME, r1 + ":闪送单独下单的取消信息-orderId-->" + orderIdl + ",userid-->" + useridi);

        //记录日志
        TrainOrderOfflineUtil.CreateBusLogAdminOtherDealResult(orderIdl, useridi, "发起闪送单独下单的取消", 222);

        JSONObject result = rapidSendCancelOrderCN(orderIdl, useridi);

        WriteLog.write(LOGNAME, r1 + ":闪送单独下单的取消结果-result" + result);

        out.print(result.toJSONString());
        out.flush();
        out.close();
    }

    private JSONObject rapidSendCancelOrderCN(Long orderIdl, Integer useridi) {
        JSONObject resResult = new JSONObject();

        WriteLog.write(LOGNAME,
                r1 + ":闪送单独下单的取消-进入到方法-rapidSendOrderAloneCN:orderIdl-->" + orderIdl + ",useridi-->" + useridi);

        if (orderIdl == null || orderIdl == 0L) {
            resResult.put("success", "false");
            String errorMsg = "传入的订单号有误，请排查";
            resResult.put("msg", errorMsg);
            TrainOrderOfflineUtil.CreateBusLogAdminOtherDealResult(orderIdl, useridi, "闪送单独下单的取消失败-" + errorMsg, 222);
            return resResult;
        }

        TrainOrderOffline trainOrderOffline = null;
        try {
            trainOrderOffline = trainOrderOfflineDao.findTrainOrderOfflineById(orderIdl);
        }
        catch (Exception e) {
            return ExceptionTCUtil.handleTCException(e);
        }

        if (trainOrderOffline == null) {
            resResult.put("success", "false");
            String errorMsg = "该订单不存在，请排查";
            resResult.put("msg", errorMsg);
            TrainOrderOfflineUtil.CreateBusLogAdminOtherDealResult(orderIdl, useridi, "闪送单独下单的取消失败-" + errorMsg, 222);
            return resResult;
        }

        //一般不会出现上述原因

        String orderNumber = trainOrderOffline.getOrderNumber();
        resResult.put("orderid", orderNumber);

        Boolean isSecondRapidSend = TrainOrderOfflineUtil
                .getIsSecondCancelRapidSend(trainOrderOffline.getOrderNumber());//判断闪送单是否是二次取消订单 - 

        UUptService uuptService = new UUptService();
        JSONObject responseJson = new JSONObject();

        JSONObject data = new JSONObject();
        data.put("orderId", orderIdl);

        try {
            if (isSecondRapidSend) {//二次取消
                TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "发起闪送单独下单的二次取消请求");
                data.put("cancelReason", "线下票闪送订单出票失败发起二次取消");
                responseJson = uuptService.operate(LOGNAME, "uupt", r1, "cancelOrderAgain", data, responseJson);
            }
            else {//一次取消
                TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "闪送单独下单的取消请求");
                data.put("cancelReason", "线下票闪送订单出票失败发起取消");
                responseJson = uuptService.operate(LOGNAME, "uupt", r1, "cancelOrder", data, responseJson);
            }
        }
        catch (Exception e) {
            ExceptionTNUtil.handleTNException(e);
        }
        //记录请求信息日志
        WriteLog.write(LOGNAME, r1 + ":闪送单独下单的取消反馈信息-responseJson" + responseJson);

        /**
         * 
        {"message":"成功","result":{"partnerOrderNumber":"U37908001710310949679202216","message":"订单发布成功","orderNumber":"","success":true},"success":true}
         * 
        {"message":"请求发生异常","result":"","success":false}
         * 
         */
        //Boolean responseB = responseJson.getBooleanValue("success");
        Boolean responseB = false;
        JSONObject responseJsonResult = new JSONObject();
        if (responseJson.getBooleanValue("success")) {
            responseJsonResult = responseJson.getJSONObject("result");
            responseB = responseJsonResult.getBooleanValue("success");
        }

        if (responseB) {
            TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "闪送单独下单的取消成功");

            //判断正常的锁单流程中是否存在了有待处理的闪送单的定时取消任务 - 有的话，做清空动作
            String jobFlag = "TrainOfflineRedistributionRapidSendOrderAloneOvertime";
            try {
                JobDetail jobDetail = SchedulerUtil.getScheduler()
                        .getJobDetail(jobFlag + "Job" + trainOrderOffline.getId(), jobFlag + "JobGroup");
                if (jobDetail != null) {
                    SchedulerUtil.cancelCancelLockTicketScheduler(orderIdl, jobFlag);

                    TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "闪送单独下单的自动取消定时任务删除成功");
                }
            }
            catch (Exception e) {
                ExceptionTNUtil.handleTNException(e);
            }

            //重置相关的邮寄对象的信息

            /*TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "闪送单独下单-重置快递类型");
            
            mailAddressDao.updateMailAddressExpress(orderIdl.intValue(), "0", 0);
            
            //默认获取顺丰的快递时效并做相关的更新操作
            //为了兼容后期的自动取消业务，还需重置闪送业务的回调已反馈的状态字段的标识
            TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "闪送单独下单-重置订单的闪送类型区分、反馈标识字段及顺丰的快递时效的显示");
            
            Integer agentid = trainOrderOffline.getAgentId();
            String totalAddress = "";
            try {
                totalAddress = mailAddressDao.findADDRESSCITYNAMEByORDERID(orderIdl).getADDRESS();
            }
            catch (Exception e2) {
                WriteLog.write(LOGNAME, r1 + ":邮寄信息获取失败-e2:" + e2);
            
                ExceptionTCUtil.handleTCException(e2);
            }
            String delieveStr = DelieveUtils.getDelieveStr(String.valueOf(orderIdl), totalAddress);//
            trainOrderOfflineDao.updateIsRapidSendAndCallbackExpressDeliverById(orderIdl, 0, delieveStr);*///

            //取消订单的同时需要还原闪送反馈的字段 - 方便后期闪送下单的自动取消任务的执行 - 但是后期的自动下单在第一个点位已经完成了相关的字段的初始化的操作 - 所以此处略去

            resResult.put("success", "true");
            resResult.put("msg", "闪送单独下单的取消成功");
        }
        else {
            String errorMsg = responseJsonResult.getString("message");
            if (errorMsg == null || "".equals(errorMsg)) {
                errorMsg = responseJson.getString("message");
            }
            errorMsg = "闪送单独下单的取消失败:" + errorMsg + "-请联系技术处理";
            TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, errorMsg);

            resResult.put("success", "false");
            resResult.put("msg", errorMsg);
        }
        return resResult;
    }

    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        Long orderIdl = 1290449L;
        Integer useridi = 62;

        TrainOfflineRedistributionRapidSendCancelOrderServletCN trainOfflineRedistributionRapidSendCancelOrderServletCN = new TrainOfflineRedistributionRapidSendCancelOrderServletCN();
        JSONObject result = trainOfflineRedistributionRapidSendCancelOrderServletCN.rapidSendCancelOrderCN(orderIdl,
                useridi);

        System.out.println(result);

        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间： " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间： " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间： " + runTimeS / 60 + "min：" + runTimeS % 60 + "s");//分
    }
}
