/**
 * 版权所有, 空铁无忧
 * Author: 火车票 H5-微信端 项目开发组
 * copyright: 2017
 */
package com.ccservice.offlineExpress.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.PropertyUtil;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.offline.dao.MailAddressDao;
import com.ccservice.offline.dao.TrainOrderOfflineDao;
import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.util.ExceptionTCUtil;
import com.ccservice.offline.util.SendPostandGetUtil;
import com.ccservice.offline.util.TrainOrderOfflineUtil;

/**
 * @className: com.ccservice.tuniu.train.servlet.TrainTuNiuOfflineOrderServlet
 * @description: TODO - 
 * 
 * 线下票 - 闪送已取件-其它采购的邮寄的单独的逻辑的兼容接口 - 由CN平台发起
 * 
 * 用于正常的流程中回填快递信息
 * 
 * 虽说只有在闪送单才会进入此流程，但是也需要进行相关位置的拦截健壮判定
 * 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月15日 上午9:34:25 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOfflineRapidSendMailServletCN extends HttpServlet {
    private static final String LOGNAME = "闪送已取件-其它采购的邮寄的单独的逻辑的兼容接口-CN发起";

    private int r1 = new Random().nextInt(10000000);

    private TrainOrderOfflineDao trainOrderOfflineDao = new TrainOrderOfflineDao();

    private MailAddressDao mailAddressDao = new MailAddressDao();

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");

        BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
        String line = null;
        StringBuilder sb = new StringBuilder();
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }

        //将资料解码  
        String reqBody = sb.toString();
        //System.out.println(reqBody);

        //记录请求信息日志
        WriteLog.write(LOGNAME, r1 + ":闪送已取件-其它采购的邮寄的单独的逻辑的兼容接口请求信息-reqBody-->" + reqBody);

        JSONObject requestBody = JSONObject.parseObject(reqBody);

        Long orderId = requestBody.getLong("orderId");
        Integer useridi = requestBody.getInteger("userid");

        //记录请求信息日志
        WriteLog.write(LOGNAME, r1 + ":闪送已取件-其它采购的邮寄的单独的逻辑的兼容信息-orderId-->" + orderId + ",useridi-->" + useridi);

        //记录日志
        TrainOrderOfflineUtil.CreateBusLogAdmin(orderId, useridi, "发起闪送已取件-其它采购的邮寄请求");

        JSONObject result = rapidSendMailCN(orderId, useridi);

        WriteLog.write(LOGNAME, r1 + ":闪送已取件-其它采购的邮寄的单独的逻辑的兼容结果-result" + result);

        PrintWriter out = response.getWriter();

        out.print(result.toJSONString());
        out.flush();
        out.close();
    }

    private JSONObject rapidSendMailCN(Long orderIdl, Integer useridi) {
        JSONObject resResult = new JSONObject();

        WriteLog.write(LOGNAME, r1 + ":闪送已取件-其它采购的邮寄的单独的逻辑的兼容-进入到方法-rapidSendMailCN");

        if (orderIdl == null || orderIdl == 0L) {
            resResult.put("success", "false");

            String errorMsg = "传入的订单号有误，请排查";
            resResult.put("msg", errorMsg);
            TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, errorMsg);

            return resResult;
        }

        TrainOrderOffline trainOrderOffline = null;
        try {
            trainOrderOffline = trainOrderOfflineDao.findTrainOrderOfflineById(orderIdl);
        }
        catch (Exception e) {
            return ExceptionTCUtil.handleTCException(e);
        }

        if (trainOrderOffline == null) {
            resResult.put("success", "false");

            String errorMsg = "该订单不存在，请排查";
            resResult.put("msg", errorMsg);
            TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, errorMsg);

            return resResult;
        }

        Integer isRapidSend = trainOrderOffline.getIsRapidSend();//20171023-新增需求 - 是否是闪送订单  0: 普通订单,1: 闪送订单 - 默认值是 0 //通过快递类型进行判定 - 目前只有邮寄票会是闪送订单
        if (isRapidSend == null) {
            isRapidSend = 0;
        }
        if (isRapidSend != 1) {
            resResult.put("success", "false");

            String errorMsg = "该订单非闪送订单，请勿操作此按钮或联系客服处理";
            resResult.put("msg", errorMsg);
            TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, errorMsg);

            return resResult;
        }

        //根据不同采购走不同的相关邮寄流程
        Integer createUId = trainOrderOffline.getCreateUId();

        if (createUId == 56) {//淘宝发件回调
            String elongxml = PropertyUtil.getValue("rapidSendMailTaobaoRequestXml", "Train.GuestAccount.properties");
            String urlString = elongxml + "TaoBaoTrainOfflineExpressServlet";

            //先做快递类型的标识的还原 - 顺丰-0 - 闪送-10   //0-顺丰 - 1-EMS 2-宅急送 3-京东 - 99配送到站 10-闪送【UU跑腿】
            String paramContent = "orderid=" + orderIdl;

            WriteLog.write(LOGNAME, r1 + "请求地址参数:" + urlString + "?" + paramContent);

            mailAddressDao.updateExpressAgentByORDERID(orderIdl.intValue(), 0);
            TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "发起淘宝邮寄请求-临时修改快递类型为顺丰");

            String resulturl = SendPostandGetUtil.submitPost(urlString, paramContent, "UTF-8").toString();
            WriteLog.write(LOGNAME, r1 + "请求地址参数:" + urlString + "?" + paramContent + ",返回结果:" + resulturl);

            /**
             * 
            [2017-11-16 09:19:19.894] 请求地址参数:http://121.41.171.147:30002/cn_interface/TaoBaoTrainOfflineExpressServlet?orderid=1455622
            [2017-11-16 09:19:20.300] 请求地址参数:http://121.41.171.147:30002/cn_interface/TaoBaoTrainOfflineExpressServlet?orderid=1455622,返回结果:
             * 
             */
            if (resulturl == null || !"true".equals(resulturl)) {
                resResult.put("success", "false");
                String errorMsg = "发起淘宝邮寄请求失败，请联系技术处理";
                resResult.put("msg", errorMsg);
                TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, errorMsg);
            }
            else {
                mailAddressDao.updateExpressAgentByORDERID(orderIdl.intValue(), 10);

                resResult.put("success", "true");
                String errorMsg = "发起淘宝邮寄请求成功-还原快递类型为UU";
                resResult.put("msg", errorMsg);
                TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, errorMsg);
            }
        }

        //记录请求信息日志
        WriteLog.write(LOGNAME, r1 + ":闪送已取件-其它采购的邮寄的单独的逻辑的兼容反馈信息-resResult" + resResult);

        return resResult;
    }

}
