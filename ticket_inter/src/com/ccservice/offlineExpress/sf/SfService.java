package com.ccservice.offlineExpress.sf;

import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.callback.SendPostandGet;
import com.ccservice.Util.PropertyUtil;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.util.ExceptionUtil;
import com.ccservice.offlineExpress.util.ServiceUtil;

/**
 * SF快递务分类
 * 
 * @time 2017年10月23日 下午4:09:14
 * @author liujun
 */
public class SfService extends ServiceUtil {

    /**
     * 操作入口
     * 
     * @time 2017年10月23日 下午4:09:03
     * @author liujun
     */
    public JSONObject operate(String logName, String expressName, long random, String method, JSONObject data,
            JSONObject responseJson) throws Exception {
        // 获取快递路由
        if ("getExpressRoute".equals(method)) {
            responseJson = getExpressRoute(logName, expressName, random, method, data, responseJson);
        }
        // 返回
        return responseJson;
    }

    /**
     * 获取快递路由
     * 
     * @time 2017年10月23日 下午4:10:19
     * @author liujun
     */
    private static JSONObject getExpressRoute(String logName, String expressName, long random, String method,
            JSONObject dataJson, JSONObject responseJson) {
        String message = "成功";
        boolean success = true;
        String result = "";
        String urlString = PropertyUtil.getValue("expressRouteUrl", "train.properties");
        String expressNum = dataJson.getString("expressNum");
        String param = "expressno=" + expressNum;
        String reqResult = SendPostandGet.submitPost(urlString, param, "UTF-8").toString();
        WriteLog.write(logName, random + "express:" + expressNum + "urlString:" + urlString + "-->param:" + param
                + "-->reqResult:" + reqResult);
        try {
            Document document = DocumentHelper.parseText(reqResult);
            Element root = document.getRootElement();
            Element head = root.element("Head");
            Element body = root.element("Body");
            Element routeResponse = body.element("RouteResponse");
            if ("OK".equals(root.elementText("Head")) && routeResponse != null) {
                responseJson.put("success", true);
                responseJson.put("message", "成功");
                List elements = routeResponse.elements("Route");
                if (elements.size() > 0) {
                    JSONArray jsonArray = new JSONArray();
                    for (int i = 0; i < elements.size(); i++) {
                        JSONObject jsonObject = new JSONObject();
                        Element route = (Element) elements.get(i);
                        jsonObject.put("acceptTime", route.attributeValue("accept_time"));
                        jsonObject.put("acceptAddress", route.attributeValue("accept_address"));
                        jsonObject.put("remark", route.attributeValue("remark"));
                        jsonArray.add(jsonObject);
                    }
                    result = jsonArray.toString();
                }
            }
        }
        catch (DocumentException e) {
            message = "失败";
            success = false;
            // 记录异常日志
            ExceptionUtil.writelogByException(logName, e, String.valueOf(random));
        }
        //统一返回结果
        responseJson.put("result", result);
        responseJson.put("success", success);
        responseJson.put("message", message);
        return responseJson;
    }
}
