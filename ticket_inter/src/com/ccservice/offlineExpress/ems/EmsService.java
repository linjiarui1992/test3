package com.ccservice.offlineExpress.ems;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.offlineExpress.util.ServiceUtil;

/**
 * EMS快递务分类
 * 
 * @time 2017年10月23日 下午4:09:14
 * @author liujun
 */
public class EmsService extends ServiceUtil {

    /**
     * 操作入口
     * 
     * @time 2017年10月23日 下午4:09:03
     * @author liujun
     */
    public JSONObject operate(String logName, String expressName, long random, String method, JSONObject data,
            JSONObject responseJson) throws Exception {
        // 根据大客户号、业务类型获取详情单号
        if ("getExpressNum".equals(method)) {
            responseJson = getExpressNum(logName, expressName, random, method, data, responseJson);
        }
        // 获取快递路由
        else if ("getExpressRoute".equals(method)) {
            responseJson = getExpressRoute(logName, expressName, random, method, data, responseJson);
        }
        // 详情单打印信息更新到EMS自助服务系统
        else if ("updatePrintDatas".equals(method)) {
            responseJson = updatePrintDatas(logName, expressName, random, method, data, responseJson);
        }
        // 返回
        return responseJson;
    }

    /**
     * 根据大客户号、业务类型获取详情单号
     * 
     * @time 2017年10月23日 下午4:09:54
     * @author liujun
     */
    private static JSONObject getExpressNum(String logName, String expressName, long random, String method,
            JSONObject dataJson, JSONObject responseJson) {
        // 接口信息
        JSONObject data = new JSONObject();
        data.put("sysAccount", dataJson.getString("sysAccount"));
        // 返回
        String reqResult = reqParam(logName, random, dataJson, method, expressName);
        responseJson = JSONObject.parseObject(reqResult);
        return responseJson;
    }

    /**
     * 获取快递路由
     * 
     * @time 2017年10月23日 下午4:10:19
     * @author liujun
     */
    private static JSONObject getExpressRoute(String logName, String expressName, long random, String method,
            JSONObject dataJson, JSONObject responseJson) {
        // 接口信息
        JSONObject data = new JSONObject();
        // 参数拼接
        data.put("expressNum", dataJson.getString("expressNum"));// 快递单号
        // 返回
        String reqResult = reqParam(logName, random, dataJson, method, expressName);
        responseJson = JSONObject.parseObject(reqResult);
        return responseJson;
    }

    /**
     * 详情单打印信息更新到EMS自助服务系统
     * 
     * @time 2017年10月23日 下午5:31:58
     * @author liujun
     */
    private JSONObject updatePrintDatas(String logName, String expressName, long random, String method, JSONObject data,
            JSONObject responseJson) {
        // TODO Auto-generated method stub
        return null;
    }
}
