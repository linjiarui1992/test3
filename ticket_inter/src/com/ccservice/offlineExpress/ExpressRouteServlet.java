package com.ccservice.offlineExpress;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.util.ExceptionUtil;
import com.ccservice.offlineExpress.ems.EmsService;
import com.ccservice.offlineExpress.sf.SfService;
import com.ccservice.offlineExpress.util.CommonUtil;
import com.ccservice.offlineExpress.util.MD5Util;
import com.ccservice.offlineExpress.util.RequestStreamUtil;
import com.ccservice.offlineExpress.uupt.UUptService;

public class ExpressRouteServlet extends HttpServlet implements Servlet {

    private static final long serialVersionUID = 9108457457749806705L;

    //日志名称
    private static final String logName = "线下火车票_同程获取快递路由";

    @Override
    @SuppressWarnings("restriction")
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }

    @Override
    @SuppressWarnings("restriction")
    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        //随机
        int random = CommonUtil.randomNum();
        //编码
        req.setCharacterEncoding("utf-8");
        //结果
        JSONObject responseJson = new JSONObject();
        try {
            // 请求参数转换
            JSONObject param = RequestStreamUtil.reqToJson(req);
            //日志
            WriteLog.write(logName, random + "---req---" + param);
            //请求体
            JSONObject requestParam = JSONObject.parseObject(param.getString("requestParam"));
            //请求快递公司
            String expressName = param.getString("expressName");
            //日志
            WriteLog.write(logName, random + "---快递代理商---" + expressName + "---requestParam---" + requestParam);
            //数字签名
            String sign = requestParam.containsKey("sign") ? requestParam.getString("sign") : "";
            //请求参数
            String data = requestParam.containsKey("data") ? requestParam.getString("data") : "";
            //请求时间
            String reqtime = requestParam.containsKey("reqtime") ? requestParam.getString("reqtime") : "";
            // 
            String check = MD5Util.MD5(expressName + reqtime, "utf-8");
            //请求参数非空
            if (CommonUtil.StringIsEmpty(data) || CommonUtil.StringIsEmpty(reqtime) || CommonUtil.StringIsEmpty(sign)) {
                responseJson.put("success", false);//失败
                responseJson.put("message", "必要请求参数缺失");
                responseJson.put("result", "");
            }
            //签名不一致
            else if (!check.equals(sign)) {
                responseJson.put("success", false);//失败
                responseJson.put("message", "签名验证失败");
                responseJson.put("result", "");
            }
            else {
                //解析
                JSONObject dataJson = JSONObject.parseObject(data);
                //SF
                if ("sf".equals(expressName)) {
                    responseJson = new SfService().operate(logName, expressName, random, "getExpressRoute", dataJson,
                            responseJson);
                    WriteLog.write(logName, random + "---responseJson---" + responseJson);
                }
                //EMS
                if ("ems".equals(expressName)) {
                    responseJson = new EmsService().operate(logName, expressName, random, "getExpressRoute", dataJson,
                            responseJson);
                }
                //uu跑腿
                if ("uupt".equals(expressName)) {
                    responseJson = new UUptService().operate(logName, expressName, random, "getExpressRoute", dataJson,
                            responseJson);
                }
            }
        }
        catch (Exception e) {
            responseJson.put("success", false);//失败
            responseJson.put("message", "请求发生异常");
            responseJson.put("result", "");
            //记录异常日志
            ExceptionUtil.writelogByException(logName, e, String.valueOf(random));
        }
        finally {
            try {
                res.setCharacterEncoding("utf-8");
                res.setContentType("application/json;charset=utf-8");
                //输出
                res.getWriter().write(responseJson.toString());
                //日志
                WriteLog.write(logName, random + "---res---" + responseJson);
            }
            catch (Exception e) {
            }
        }
    }
}
