package com.ccservice.offlineExpress.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.PropertyUtil;
import com.ccservice.b2b2c.atom.component.WriteLog;

public class ServiceUtil {
    // 日志名称
    private static final String logName = "线下火车票_ticket_inter请求接口";

    // 响应超时时间
    public final static Integer timeOut = 60000;

    // 编码格式
    private final static String charset = "UTF-8";

    // 请求地址
    private final static String reqUrl = PropertyUtil.getValue("offlineExpress_url", "train.properties");

    // 回调地址
    public final static String callbackUrl = PropertyUtil.getValue("uupt_callback", "train.properties");

    // 推送方式（0 开放订单，1指定跑男，2商户绑定的跑男）默认传0即可，测试订单传2
    public final static String pushType = PropertyUtil.getValue("uupt_pushType", "train.properties");

    // 取件是否给我打电话 1需要 0不需要
    public final static String callmeWithtake = PropertyUtil.getValue("uupt_callmeWithtake", "train.properties");

    /**
     * 请求并返回结果
     * 
     * @time 2017年10月23日 下午4:31:04
     * @author liujun
     */
    public static String reqParam(String logName, long random, JSONObject data, String method, String expressName) {
        String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        JSONObject requestParam = new JSONObject();
        requestParam.put("data", data);
        requestParam.put("method", method);
        requestParam.put("reqtime", time);
        requestParam.put("sign", MD5Util.MD5(method + time, charset));
        // 请求参数
        JSONObject req = new JSONObject();
        req.put("expressName", expressName);
        req.put("requestParam", requestParam);
        //请求日志
        WriteLog.write(logName, random + "----请求----" + req.toJSONString());
        // 请求
        String result = RequestUtil.post(reqUrl, req.toJSONString(), charset, new HashMap<String, String>(), timeOut);
        //响应日志
        WriteLog.write(logName, random + "----返回----" + result);
        // 返回结果
        return result;
    }
}
