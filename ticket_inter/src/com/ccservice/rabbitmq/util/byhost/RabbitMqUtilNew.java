package com.ccservice.rabbitmq.util.byhost;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.qunar.util.ExceptionUtil;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.RabbitmqBean;
import com.ccservice.util.mq.util_mq.rabbitmq.customer.RabbitMQSupplyPool;
import com.ccservice.util.mq.util_mq.rabbitmq.customer.RabbitMqUtil;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.MissedHeartbeatException;
import com.rabbitmq.client.ShutdownSignalException;

public class RabbitMqUtilNew {

    /**
     * 获取当前使用的哪个mq服务的对象
     * @param mqGroup 1空铁组 2 同程组 3支付组
     * @return
     * @time 2017年2月17日 下午2:12:36
     * @author chen
     */
    public static RabbitmqBean getRabbitmqBean(String mqGroup) {
        RabbitmqBean rabbitmqBean = null;
        String sql = "[dbo].[MQConfiguration_selectUseingMq] @MqGroup=" + mqGroup;
        System.out.println(sql);
        //获取一个数据库配置的rabbitMq记录
//        System.out.println("RabbitMqUtilNew:"+Server.getInstance().getSystemService());
        List list = Server.getInstance().getSystemService().findMapResultByProcedure(sql);
        if (list.size() > 0) {
            Map map = (Map) list.get(0);
            String host = map.get("MqHost") == null ? "" : map.get("MqHost").toString();
            String username = map.get("UserName") == null ? "" : map.get("UserName").toString();
            String password = map.get("PassWord") == null ? "" : map.get("PassWord").toString();
            rabbitmqBean = new RabbitmqBean(host, username, password);
        }
        return rabbitmqBean;
    }
    
    /**
     * 发送一个rabbitMq的消息
     * 
     * @param queueName 消息队列的名字
     * @param msginfo 消息的内容
     * @param mqGroup 1空铁组 2 同程组 3支付组
     * @time 2017年3月3日 下午4:38:06
     * @author chendong
     * @throws IOException 
     */
    public static void sendOneRabbitMqMessage(String queueName, String msginfo,String mqGroup){
        long r1 = System.currentTimeMillis();
        WriteLog.write("RabbitMqUtilNew发送消息_1", queueName + ":" + msginfo);
        String key = "rabbitmqBean"+mqGroup;
        RabbitmqBean rabbitmqBean = RabbitMQSupplyPool.mapRabbitmqBean.get(key);
        if (rabbitmqBean == null || (System.currentTimeMillis()
                - rabbitmqBean.getLastUpdateMqHostTime()) > RabbitMQSupplyPool.updateRabbitMqhostTime) {//如过对象为空或者最后更新时间超过了最大更新时间就重新读取数据库的最新的mq信息
            rabbitmqBean = RabbitMqUtilNew.getRabbitmqBean(mqGroup);
            RabbitMQSupplyPool.mapRabbitmqBean.put(key, rabbitmqBean);
        }
        String host = rabbitmqBean.getHost();
        String userName = rabbitmqBean.getUsername();
        String passWord = rabbitmqBean.getPassword();
        // 创建一个新的消息队列服务器实体的连接
        Connection connection = getMqConnection(host, userName, passWord,queueName,msginfo);
        //建立链接
        try {
            RabbitMqUtil.sendMsgByConnection(connection, queueName, msginfo);
        }
        catch (ShutdownSignalException e) {
            e.printStackTrace();
            ExceptionUtil.writelogByException("RabbitMqUtilNew-exception", e, "run=" +queueName+":"+ msginfo);
        }
        catch (ConsumerCancelledException e) {
            e.printStackTrace();
            ExceptionUtil.writelogByException("RabbitMqUtilNew-exception", e, "run=" +queueName+":"+ msginfo);
        }
        catch (MissedHeartbeatException e) {
            e.printStackTrace();
            ExceptionUtil.writelogByException("RabbitMqUtilNew-exception", e, "run=" +queueName+":"+ msginfo);
        }
        catch (IOException e) {
            e.printStackTrace();
            ExceptionUtil.writelogByException("RabbitMqUtilNew-exception", e, "run=" +queueName+":"+ msginfo);
        }
        catch (Exception e) {
            e.printStackTrace();
            ExceptionUtil.writelogByException("RabbitMqUtilNew-exception", e, "run=" +queueName+":"+ msginfo);
        }
        //发送消息
        WriteLog.write("RabbitMqUtilNew发送消息_1", queueName + ":发送结束:" + msginfo);
    }
    
    private static Connection getMqConnection(String host, String userName, String passWord, String queueName,
            String msginfo) {
        Connection connection = null;
        for (int i = 0; i < 3; i++) {
            try {
                connection = RabbitMqUtil.getRabbitMqConnection(host, userName, passWord);
                break;
            }
            catch (Exception e) {
                e.printStackTrace();
                ExceptionUtil.writelogByException("RabbitMqUtilNew-exception", e,
                        i+"次host:" + host + ":userName:" + userName + ":userName:" + passWord);
                try {
                    connection = RabbitMqUtil.getRabbitMqConnection(host, userName, passWord, true);
                    break;
                }
                catch (IOException e1) {
                    e1.printStackTrace();
                    ExceptionUtil.writelogByException("RabbitMqUtilNew-exception", e,
                            i+"次host:" + host + ":userName:" + userName + ":userName:" + passWord);
                }
            } 
        }
        return connection;
    }

    public static void main(String[] args) {
        String host = "211.159.152.139";
        String username = "admin";
        String password = "@FL@op7SW4%KxCoU";
      /*  try {
			Connection connection = RabbitMqUtil.getRabbitMqConnection(host, username, password);
			RabbitMqUtil.sendMsgByConnection(connection, "queue_test_1", "hello");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
      try {
            //            new RabbitMqUtilNew().getRabbitmqBean("1");
//            System.out.println(Server.getInstance().getSystemService());
//            for (int i = 0; i < 10; i++) {
                //                RabbitMqUtilNew.sendOneRabbitMqMessage("QUEUE_TEST", i+"","0");
                // 创建一个新的消息队列服务器实体的连接
//                Connection connection = RabbitMqUtil.getRabbitMqConnection(host, username, password);//建立链接
                RabbitMqUtilNew.sendOneRabbitMqMessage("queue_test_2", "hello2211111111111112", "4");
//            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

}
