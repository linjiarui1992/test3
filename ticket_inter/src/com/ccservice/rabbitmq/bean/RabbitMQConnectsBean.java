package com.ccservice.rabbitmq.bean;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.QueueingConsumer;

/**
 * 一个消费者的对象
 * 
 * @time 2017年2月10日 下午5:20:59
 * @author chen
 */
public class RabbitMQConnectsBean {
    /**
     *  1----13  依次
     *  下单消费者队列名称 RabbitWaitOrder
                        下单排队消费者队列名称 QueueMQ_trainorder_waitorder_orderid_PaiDui
                        审核消费者队列名称 Rabbit_QueueMQ_QueryOrder
                        扣款消费者队列名称 Rabbit_QueueMQ_Deduction
                        申请改签消费者队列名称 changeWaitOrder
                         确认改签消费者队列名称 changeConfirmOrder
                        改签审核消费者队列名称 changePayExamine
                        改签排队消费者队列名称 Rabbit_ChangeOrder_PaiDui
                        改签扣款消费者队列名称 QueueMQ_TrainorderDeductionGq
                        取消消费者队列名称 QueueMQ_CancelOrder
                        淘宝改签特有消费者队列名称 TB_Change_Order
                        同程退票消费者队列名称 QueueMQ_TrainTicket_RefundTicket
                        更新帐号消费者队列名称 QueueMQ_12306AccountSystem_ExcuteUpdate
     */
    private int type;//类型     

    private Connection connection;//   目前链接中先不放值

    private boolean use;//是否正在使用

    private String key;//key   确认唯一一个链接

    private Channel channel;// 通道

    private QueueingConsumer consumer;

    public QueueingConsumer getConsumer() {
        return consumer;
    }

    public void setConsumer(QueueingConsumer consumer) {
        this.consumer = consumer;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public boolean isUse() {
        return use;
    }

    public void setUse(boolean use) {
        this.use = use;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

}
