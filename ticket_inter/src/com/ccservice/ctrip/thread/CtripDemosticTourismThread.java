package com.ccservice.ctrip.thread;

import java.net.MalformedURLException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.atom.server.Server;
import com.ccservice.b2b2c.base.travelcity.TravelCity;
import com.ccservice.b2b2c.base.tripline.Tripline;
import com.ccservice.b2b2c.base.tripnode.Tripnode;
import com.ccservice.b2b2c.base.tripprice.TripPrice;
import com.ccservice.b2b2c.base.triprange.Triprange;
import com.ccservice.ctrip.domestictourism.ICtripDomesticTourism;

public class CtripDemosticTourismThread implements Runnable {

    private String url = "http://localhost:8080/Reptile/service/";

    private HessianProxyFactory factory = new HessianProxyFactory();

    private Tripline tripline = new Tripline();

    private TripPrice tripprice = new TripPrice();

    private Tripnode tripnode = new Tripnode();

    private Triprange triprange = new Triprange();

    private TravelCity travelcity;

    private List<TravelCity> arriveCityList;

    private String fenYe;

    public CtripDemosticTourismThread(TravelCity travelcity, List<TravelCity> arriveCityList, String fenYe) {
        this.travelcity = travelcity;
        this.arriveCityList = arriveCityList;
        this.fenYe = fenYe;
    }

    @Override
    public void run() {
        try {
            if (travelcity != null && arriveCityList != null && arriveCityList.size() > 0) {
                for (int i = 0; i < arriveCityList.size(); i++) {
                    fetchingListPageData(travelcity.getCityOutId(), fenYe, arriveCityList.get(i).getCityNo2(),
                            arriveCityList.get(i).getCityNo1(), travelcity.getCityPinYin(), arriveCityList.get(i)
                                    .getCityPinYin());
                }
            }
        }
        catch (Exception e) {
        }
    }

    public void fetchingListPageData(String startCityId, String fenYe, String arriveCityNo2, String arriveCityNo1,
            String startCityPinYin, String arriveCityPinYin) {
        try {
            ICtripDomesticTourism service = (ICtripDomesticTourism) factory.create(ICtripDomesticTourism.class, url
                    + ICtripDomesticTourism.class.getSimpleName());
            String dynamicString = "whole-" + startCityId.trim() + "B126P" + fenYe.trim() + "Z" + arriveCityNo2.trim()
                    + "-D-" + arriveCityNo1.trim() + "-" + startCityPinYin.trim() + "-" + arriveCityPinYin.trim() + "";
            //System.out.println("动态地址==========="+dynamicString);
            String tourRouteList = service.fetchingListPageData(dynamicString);
            if (tourRouteList != null && !tourRouteList.equals("FAIL") && !tourRouteList.equals("")) {
                JSONObject tourRouteListObject = JSONObject.fromObject(tourRouteList);
                JSONArray tourRouteListArray = tourRouteListObject.getJSONArray("tourRouteList");
                for (int i = 0; i < tourRouteListArray.size(); i++) {
                    JSONObject tourRoute = JSONObject.fromObject(tourRouteListArray.get(i));
                    String imgAddress = tourRoute.getString("imgAddress");
                    String tourType = tourRoute.getString("tourType");
                    String tourLineTitle = tourRoute.getString("tourLineTitle");
                    JSONArray tourLineDecInfoArray = tourRoute.getJSONArray("tourLineDecInfo");
                    if (tourLineDecInfoArray != null && tourLineDecInfoArray.size() > 0) {
                        for (int j = 0; j < tourLineDecInfoArray.size(); j++) {
                            JSONObject tourLineDecInfoObject = JSONObject.fromObject(tourLineDecInfoArray.get(j));
                            String smallTourLineTitleLink = tourLineDecInfoObject.getString("smallTourLineTitleLink");
                            String detailsPage = "";
                            if (smallTourLineTitleLink != null && !smallTourLineTitleLink.equals("")) {
                                String str = "";
                                if (!tourType.contains("鸿鹄")) {
                                    str = analyticalDetailsPage(smallTourLineTitleLink);
                                }
                                if (str != null && !str.equals("FAIL") && !tourRouteList.equals("")) {
                                    detailsPage = str;
                                }
                            }
                            String smallTourLineTitle = tourLineDecInfoObject.getString("smallTourLineTitle");
                            String tourDec = tourLineDecInfoObject.getString("tourDec");
                            String goDays = tourLineDecInfoObject.getString("goDays");
                            String tourPrice = tourLineDecInfoObject.getString("tourPrice");
                            /*********************************列表页面向数据插入数据开始*******************************************/
                            tripline.setName(tourLineTitle + "*" + smallTourLineTitle);
                            tripline.setCreateuser("admin");
                            tripline.setCreatetime(new Timestamp(System.currentTimeMillis()));
                            tripline.setModifyuser("admin");
                            tripline.setModifytime(new Timestamp(System.currentTimeMillis()));
                            tripline.setDescription(tourDec);
                            tripline.setPredesc("");
                            tripline.setCityid(Long.valueOf(startCityId));
                            tripline.setStartrange(goDays.replaceAll("班期：", ""));
                            tripline.setCustomeragentid(Long.valueOf("46"));
                            tripline.setStartdate("");
                            tripline.setUcode(123L);
                            tripline.setLanguage(0);
                            tripline.setImage(imgAddress);
                            tripline.setEndcityid(Long.valueOf(arriveCityNo2));
                            if (tourType.contains("团队")) {
                                tripline.setTypeid(6L);
                            }
                            else if (tourType.contains("自由")) {
                                tripline.setTypeid(7L);
                            }
                            else if (tourType.contains("半自助")) {
                                tripline.setTypeid(8L);
                            }
                            else if (tourType.contains("私家")) {
                                tripline.setTypeid(10L);
                            }
                            else if (tourType.contains("鸿鹄")) {
                                tripline.setTypeid(9L);
                            }
                            tripline.setLongname(tourLineTitle + "*" + smallTourLineTitle);
                            tripline.setCreateuserid(62L);
                            if (tourPrice != null && !tourPrice.equals("")) {
                                tripline.setAdultprice(Float.valueOf(tourPrice));
                            }
                            String tianShu = "";
                            if (detailsPage != null && !detailsPage.equals("")) {
                                JSONObject detailsPageObject = JSONObject.fromObject(detailsPage);
                                JSONArray routeArrangeInfoArray = detailsPageObject.getJSONArray("routeArrangeInfo");
                                if (routeArrangeInfoArray != null && routeArrangeInfoArray.size() > 0) {
                                    tianShu = String.valueOf(routeArrangeInfoArray.size());
                                }
                            }
                            tripline.setTripdays(tianShu);
                            tripline.setPrebookdays("1");
                            tripline.setTraffic("");
                            tripline.setBuydesc("");
                            tripline.setOrganization(7L);
                            tripline.setLocalData(1);
                            this.tripline = Server.getInstance().getTripService().createTripline(tripline);
                            /*********************************列表页面向数据插入数据结束*******************************************/
                            /*********************************详细信息页面向数据插入数据开始*******************************************/
                            if (detailsPage != null && !detailsPage.equals("")) {
                                JSONObject detailsPageObject = JSONObject.fromObject(detailsPage);
                                String imgNameAndAddress = detailsPageObject.getString("imgNameAndAddress");
                                if (imgNameAndAddress != null && !imgNameAndAddress.equals("")) {
                                    String[] imgNameAndAddressArray = imgNameAndAddress.split("@^^@");
                                    if (imgNameAndAddressArray != null && imgNameAndAddressArray.length > 0) {
                                        String sql = "";
                                        for (int k = 0; k < imgNameAndAddressArray.length; k++) {
                                            sql += "insert into T_SCENICSPOT(C_TRIPLINEID,C_NAME,C_IMAGE,C_CREATEUSER,C_CREATETIME,C_MODIFYUSER,C_MODIFYTIME,C_REGIONID,C_LANGUAGE,C_CREATEUSERID) "
                                                    + "values("
                                                    + this.tripline.getId()
                                                    + ",'"
                                                    + tourLineTitle
                                                    + "*"
                                                    + smallTourLineTitle
                                                    + "','"
                                                    + imgNameAndAddressArray[k]
                                                    + "','admin','"
                                                    + new Timestamp(System.currentTimeMillis())
                                                    + "','admin','"
                                                    + new Timestamp(System.currentTimeMillis())
                                                    + "',"
                                                    + this.tripline.getId() + ",0,62);";
                                        }
                                        if (sql != null && !sql.equals("")) {
                                            Server.getInstance().getTripService().excuteScenicspotBySql(sql);
                                        }
                                    }
                                }
                                String calendarDateAndPrice = detailsPageObject.getString("calendarDateAndPrice");
                                if (calendarDateAndPrice != null && !calendarDateAndPrice.equals("")) {
                                    String[] calendarDateAndPriceArr = calendarDateAndPrice.split("@##@");
                                    if (calendarDateAndPriceArr != null && calendarDateAndPriceArr.length > 0) {
                                        for (int a = 0; a < calendarDateAndPriceArr.length; a++) {
                                            String dateAndPrice = calendarDateAndPriceArr[a];
                                            if (dateAndPrice != null && !dateAndPrice.equals("")) {
                                                String[] dateAndPriceArr = dateAndPrice.split("@&&@");
                                                if (dateAndPriceArr != null && dateAndPriceArr.length == 2) {
                                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                                    String calendarDate = dateAndPriceArr[0].trim();
                                                    Date dateOfCalendar = sdf.parse(calendarDate);
                                                    String calendarPrice = dateAndPriceArr[1].trim();
                                                    if (!calendarPrice.contains("实时计价")) {
                                                        //str1  str2  str3  必须这样转换，否则字符串中的数据不能正常转换
                                                        String str1 = calendarPrice.substring(2);
                                                        String str2 = str1.replace("起", "");
                                                        String str3 = str2.replace(",", "");
                                                        tripprice.setAdultPrice(Integer.valueOf(str3.trim()));
                                                        tripprice.setChildrenPrice(Integer.valueOf(str3.trim()) / 2);
                                                        tripprice.setChildrenpricest("暂无描述");
                                                        tripprice.setSingleRoomPrice(0);
                                                        tripprice.setTriplineid(this.tripline.getId());
                                                        tripprice.setTripDate(sdf.format(dateOfCalendar));
                                                        Server.getInstance().getTripService()
                                                                .createTripPrice(tripprice);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                JSONArray routeArrangeInfoArray = detailsPageObject.getJSONArray("routeArrangeInfo");
                                if (routeArrangeInfoArray != null && routeArrangeInfoArray.size() > 0) {
                                    for (int b = 0; b < routeArrangeInfoArray.size(); b++) {
                                        JSONObject routeArrangeInfoObj = JSONObject.fromObject(routeArrangeInfoArray
                                                .get(b));
                                        String tripName = routeArrangeInfoObj.getString("tripName");
                                        String routeArrange = routeArrangeInfoObj.getString("routeArrange");
                                        triprange.setTriplineid(this.tripline.getId());
                                        triprange.setName(tripName);
                                        triprange.setCreateuser("admin");
                                        triprange.setCreateuserid(62L);
                                        triprange.setCreatetime(new Timestamp(System.currentTimeMillis()));
                                        triprange.setModifyuser("admin");
                                        triprange.setModifytime(new Timestamp(System.currentTimeMillis()));
                                        triprange.setDescription(routeArrange);
                                        triprange.setUcode(123L);
                                        triprange.setLanguage(0);
                                        Server.getInstance().getTripService().createTriprange(triprange);
                                    }
                                }
                                tripnode.setTriplineid(this.tripline.getId());
                                tripnode.setCreateuser("admin");
                                tripnode.setCreateuserid(62L);
                                tripnode.setCreatetime(new Timestamp(System.currentTimeMillis()));
                                tripnode.setModifyuser("admin");
                                tripnode.setModifytime(new Timestamp(System.currentTimeMillis()));
                                tripnode.setUcode(123L);
                                tripnode.setLanguage(0);
                                tripnode.setName("退款说明");
                                tripnode.setType(1);
                                tripnode
                                        .setContent("旅游度假产品，请用户在跟供应商预约的时候，就出示验证码。一旦消费码验证成功，视为订单已消费，已消费订单无法办理退款且不支持任何变更。在使用有效期内未消费可以全额退款，有效期外，不予退款。");
                                Server.getInstance().getTripService().createTripnode(tripnode);
                                if (tripline.getDescription() != null && !tripline.getDescription().equals("")) {
                                    tripnode.setName("线路特色");
                                    tripnode.setType(2);
                                    tripnode.setContent(tripline.getDescription());
                                    Server.getInstance().getTripService().createTripnode(tripnode);
                                }
                                String managerRecommended = detailsPageObject.getString("managerRecommended");
                                if (managerRecommended != null && !managerRecommended.equals("")) {
                                    tripnode.setName("产品详情");
                                    tripnode.setType(3);
                                    tripnode.setContent(managerRecommended);
                                    Server.getInstance().getTripService().createTripnode(tripnode);
                                }
                                String feiYongBaoHan = detailsPageObject.getString("feiYongBaoHan");
                                if (feiYongBaoHan != null && !feiYongBaoHan.equals("")) {
                                    tripnode.setName("费用说明");
                                    tripnode.setType(5);
                                    tripnode.setContent(feiYongBaoHan);
                                    Server.getInstance().getTripService().createTripnode(tripnode);
                                }
                                String yuDingXuZhi = detailsPageObject.getString("yuDingXuZhi");
                                if (yuDingXuZhi != null && !yuDingXuZhi.equals("")) {
                                    tripnode.setName("使用说明");
                                    tripnode.setType(6);
                                    tripnode.setContent(yuDingXuZhi);
                                    Server.getInstance().getTripService().createTripnode(tripnode);
                                }
                                String chuXingJingGao = detailsPageObject.getString("chuXingJingGao");
                                if (chuXingJingGao != null && !chuXingJingGao.equals("")) {
                                    tripnode.setName("注意事项");
                                    tripnode.setType(7);
                                    tripnode.setContent(chuXingJingGao);
                                    Server.getInstance().getTripService().createTripnode(tripnode);
                                }
                            }
                            /*********************************详细信息页面向数据插入数据结束*******************************************/
                            this.tripline.setId(0);
                        }
                    }
                }
            }
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public String analyticalDetailsPage(String oppositeLinkAddress) {
        try {
            ICtripDomesticTourism service = (ICtripDomesticTourism) factory.create(ICtripDomesticTourism.class, url
                    + ICtripDomesticTourism.class.getSimpleName());
            return service.analyticalDetailsPage(oppositeLinkAddress);
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return "FAIL";
    }
}
