package com.ccservice.jielv.hotel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.ccservice.b2b2c.base.hmhotelprice.JLPriceResult;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.b2b2c.base.hotelgooddata.HotelGoodData;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.inter.server.Server;
/**
 * 
 * @author wzc
 * 比价实体工具类
 *
 */
public class JLComProc{
	/**
	 * 加载华闽数据
	 * @param hotel 酒店对象
	 * @param start 加载数据起始日期
	 * @param end   加载数据结束日期
	 * @param dayHMData 承载数据的每天数据
	 * @throws Exception
	 */
	public void loadroomtype(Hotel hotel, String start, String end,Map<Long, List<JLPriceResult>> dayJLData) throws Exception {
		System.out.println("酒店名称："+hotel.getName());
		List<JLPriceResult> result = Server.getInstance().getIJLHotelService().getHotelPrice(hotel.getHotelcode(), start, end);
		System.out.println("酒店名称22222："+hotel.getName());
		if (result.size()>0) {
			dayJLData.put(hotel.getId(), result);
		}
	}
	/**
	 * 讲优势数据写入数据库
	 * @param gooddata
	 */
	public void writeDataDB(List<HotelGoodData> gooddata){
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Set<Long> hotelids=new HashSet<Long>();
		
		for (int i = 0; i < gooddata.size() ; i++) {
			HotelGoodData good=gooddata.get(i);
			good.setUpdatetime(sdf1.format(new Date(System.currentTimeMillis())));
			good.setId(0l);
			
			String wherex = "where C_HOTELID=" + good.getHotelid()
			+ " and C_ROOMTYPEID=" + good.getRoomtypeid()
			+ "  and C_DATENUM='" + good.getDatenum()
			+ "' and c_sorucetype=6";
			
			List<HotelGoodData> gd = Server.getInstance().getHotelService().findAllHotelGoodData(wherex, "", -1, 0);
			
			PropertyUtil pu=new PropertyUtil();
			if(gd.size()>0){
				if(gd.get(0).getRoomstatus()==1){
					good.setRoomstatus(1l);
				}
				hotelids.add(good.getHotelid());
				Server.getInstance().getSystemService().findMapResultBySql(
						"delete from T_HOTELGOODDATA where c_hotelid=" + good.getHotelid()+ " and C_ROOMTYPEID=" + good.getRoomtypeid()+ "  and C_DATENUM='" + good.getDatenum()+ "' and c_sorucetype=6", null);
				System.out.println("删除………………");
			}
			
			good.setOpenclose(Integer.parseInt(pu.getValue("openclose")));// 0是关房
			good.setSorucetype("6");
			//特殊处理
			if(good.getProfit()!=null){
				if(good.getProfit()>0&&good.getProfit()<20){
					good.setRoomflag("2");
					//good.setRoomstatus(0l);//开房
					//good.setShijiprice(good.getBaseprice()+20);
				}
				if(good.getProfit()>=20){
					good.setRoomflag("1");
				}
			}else{
				System.out.println(good.getProfit());
			}
			
			if(good.getShijiprice()<20){
				good.setShijiprice(0l);
			}
			System.out.println(good);
			String str="[dbo].[sp_insertgooddata] @hotelid = "+
			good.getHotelid()+",@hotelname = N'"+
			good.getHotelname()+"',@roomtypeid = "+
			good.getRoomtypeid()+",@roomtypename = N'"+
			good.getRoomtypename()+"',@shijiprice = "+
			good.getShijiprice()+",@baseprice = "+
			good.getBaseprice()+",	@sealprice = "+
			good.getSealprice()+",@profit = "+
			good.getProfit()+",@roomstatus = "+
			good.getRoomstatus()+",@yuliunum = "+
			good.getYuliunum()+",@datenum = N'"+
			good.getDatenum()+"',	@minday = "+
			good.getMinday()+",@openclose = "+
			good.getOpenclose()+",@beforeday = "+
			good.getBeforeday()+",@contractid = N'"+
			good.getContractid()+"',@contractver = N'"+
			good.getContractver()+"',@prodid = N'"+
			good.getProdid()+"',@bfcount = "+
			good.getBfcount()+",@sorucetype = N'"+
			good.getSorucetype()+"',@agentname = N'"+
			good.getAgentname()+"',@updatetime = N'"+
			good.getUpdatetime()+"',@roomflag=N'"+
			good.getRoomflag()+"',@cityid=N'"+
			good.getCityid()+"',@jlkeyid=N'"+
			good.getJlkeyid()+"',@jltime=N'"+
			good.getJltime()+"',@jlft=N'"+
			good.getJlft()+"',@jlf=N'"+
			good.getJlf()+"',@jlroomtypeid=N'"+
			good.getJlroomtypeid()+"',@allotmenttype=N'"+
			good.getAllotmenttype()+"',@ratetype=N'"+
			good.getRatetype()+"',@ratetypeid=N'"+
			good.getRatetypeid()+"'";
			List result = Server.getInstance().getSystemService().findMapResultByProcedure(str);
		}
	}
}
