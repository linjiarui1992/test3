package com.ccservice.jielv.hotel;
/**
 * 定时增加一天数据
 * by sefvang
 */
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import com.ccservice.b2b2c.base.city.City;
import com.ccservice.b2b2c.base.hmhotelprice.JLPriceResult;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.b2b2c.base.hotelgooddata.HotelGoodData;
import com.ccservice.b2b2c.base.roomtype.Roomtype;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.inter.server.Server;

public class JielvModifyPrice implements Job {
	
	private JLComProc proc = null;// 比价工具对象
	
	private Map<Long, List<JLPriceResult>> dayJLData = new HashMap<Long, List<JLPriceResult>>();
	
	public static void main(String[] args) {
		JielvModifyPrice dp=new JielvModifyPrice();
		dp.test();
	}

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		JielvModifyPrice dp=new JielvModifyPrice();
		dp.test();
	}

	/**
	 * 构造器，实例化比价对象
	 */
	public JielvModifyPrice() {
		proc = new JLComProc();
	}


	public void test() {
		Calendar start = GregorianCalendar.getInstance();
		start.add(Calendar.DAY_OF_MONTH, -1);
		Calendar end = GregorianCalendar.getInstance();
		end.add(Calendar.DAY_OF_MONTH, 30);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String started = sdf.format(start.getTime()); 
		String ended = sdf.format(end.getTime());
		long time1 = System.currentTimeMillis();
		PropertyUtil pu=new PropertyUtil();
		Float profit = Float.parseFloat(pu.getValue("profit1"));// 1000-
		String where="  where C_JLCODE is not null ";
		List<City> citys=Server.getInstance().getHotelService().findAllCity(where, "", -1, 0);
		int cnum = citys.size();
		
		
		for (City city : citys) {
			String wheres = " where  c_star>20 and c_sourcetype=6 and c_state=3 ";
			wheres += " and c_cityid=" + city.getId();
			List<Hotel> hotels = Server.getInstance().getHotelService()
					.findAllHotel(wheres, "order by id", -1, 0);
			int k = hotels.size();
			
			for (int j = 0; j < hotels.size(); j++) {
				System.out.println("城市："+city.getName());
				System.out.println("城市数量："+cnum--);
				System.out.println("酒店数量：" + (k--));
				long time3 = System.currentTimeMillis();
				System.out.println("酒店名称：" + hotels.get(j).getName());
				// 入基础数据
				work(hotels.get(j), started, ended, dayJLData, profit);
				long time4 = System.currentTimeMillis();
				System.out.println("用时：" + (time4 - time3) / 1000 + "s");
			}
			
		}
		
		long time2 = System.currentTimeMillis();
		System.out.println("用时：" + (time2 - time1) / 1000 / 60);
	}

	public void work(Hotel hotel, String started, String ended,
			Map<Long, List<JLPriceResult>> dayJLData, Float profit) {
		List<HotelGoodData> HotelGoodDatas = new ArrayList<HotelGoodData>();
		try {
			proc.loadroomtype(hotel, started, ended, dayJLData);
			if (dayJLData != null) {
				List<JLPriceResult> result = dayJLData.get(hotel.getId());// 华闽加载的数据结果
				if (result != null) {

					label: for (JLPriceResult priceResult : result) {
						PropertyUtil pu=new PropertyUtil();
						Float profit25 = Float.parseFloat(pu.getValue("profit2"));// 1000-2000
						Float profit2 = Float.parseFloat(pu.getValue("profit3"));// 2000+
						String allot = priceResult.getAllot();
						String bf = priceResult.getBreakfast();
						String hname = priceResult.getHotelName();
						String pp = priceResult.getPprice();
						String rt = priceResult.getRoomtype();
						String sdate = priceResult.getStayDate();
						HotelGoodData good = new HotelGoodData();
						good.setHotelid(hotel.getId());
						good.setBfcount(Long.valueOf(bf));
						good.setContractid(hotel.getHotelcode2());
						good.setJlkeyid(priceResult.getJlkeyid());
						good.setCityid(String .valueOf(hotel.getCityid()));
						good.setJltime(priceResult.getJltime());
						
						good.setAllotmenttype(priceResult.getAllotmenttype());
						good.setRatetype(priceResult.getRatetype());
						good.setRatetypeid(priceResult.getRatetypeid());
						good.setJlroomtypeid(priceResult.getRoomtypeid());
						
						SimpleDateFormat sdf = new SimpleDateFormat(
								"yyyy-MM-dd");
						Date qdate = sdf.parse(sdate);
						String xdate = sdf.format(qdate);
						good.setDatenum(xdate);
						good.setHotelname(hname);
						if (priceResult.getLeadTime() != null
								&& !"".equals(priceResult.getLeadTime())
								&& !"null".equals(priceResult.getLeadTime())) {
							good.setBeforeday(Long.parseLong(priceResult
									.getLeadTime()));
						} else {
							good.setBeforeday(0l);
						}
						if (priceResult.getMinDay() != null
								&& !"".equals(priceResult.getMinDay())
								&& !"null".equals(priceResult.getMinDay())) {
							good.setMinday(Long.parseLong(priceResult
									.getMinDay()));
						} else {
							good.setMinday(1l);
						}

						good.setOpenclose(Integer.parseInt(pu.getValue("openclose")));
						String where = "  where C_HOTELID=" + hotel.getId()
								+ " and C_NAME='" + rt + "'";
						List<Roomtype> typeid = Server.getInstance()
								.getHotelService().findAllRoomtype(where, "",
										-1, 0);
						
						good.setRoomtypename(rt+"("+priceResult.getRatetype()+")");
						if (typeid.size() > 0) {
							good.setRoomtypeid(typeid.get(0).getId());
							good.setYuliunum(Long.parseLong(priceResult
									.getFangliang()));
						
							Double baseprice = Double.parseDouble(pp);
							int fee = 0;
							int lirun = 0;
							Double shijiprice = 0d;
							if (profit < 1) {
								if (baseprice < 1000) {
									fee = (int) (baseprice * 0.01 + baseprice * profit) + 12;
									lirun = (int) (baseprice * profit);
									shijiprice = baseprice + fee;
								} else if (baseprice > 1000 && baseprice < 2000) {
									fee = (int) (baseprice * 0.01 + baseprice * profit25) + 12;
									lirun = (int) (baseprice * profit25);
									shijiprice = baseprice + fee;
								} else if (baseprice > 2000) {
									fee = (int) (baseprice * 0.01 + baseprice * profit2) + 12;
									lirun = (int) (baseprice * profit2);
									shijiprice = baseprice + fee;
								}
							} else {
								fee = (int) (baseprice * 0.01)
										+ profit.intValue() + 12;
								shijiprice = baseprice + profit + fee;
							}

							good.setBaseprice((long) (Double.parseDouble(pp)));
							good.setProfit((long) lirun);
							good.setShijiprice(shijiprice.longValue());
							
							// 11为现付
							if ("11".equals(priceResult.getPricetypr())) {
								System.out.println("现付的房型");
								continue label;
							}
							//状态为查的数据
							if("null".equals(allot)&&Integer.parseInt(priceResult.getFangliang())<=0||
									"".equals(allot)&&Integer.parseInt(priceResult.getFangliang())<=0||
									allot==null&&Integer.parseInt(priceResult.getFangliang())<=0){
								good.setRoomstatus(1l);
								HotelGoodDatas.add(good);
								good.setJlft(allot);
								good.setJlf(priceResult.getFangliang());
								System.out.println("查");
								continue label;
							}

							//没有价格
							if ("0".equals(pp)) {
								good.setRoomstatus(1l);
								HotelGoodDatas.add(good);
								good.setJlft(allot);
								good.setJlf(priceResult.getFangliang());
								System.out.println("没有价格");
								continue label;
							}
							
							//待确认的
							if(!"16".equals(allot)
									&&!"12".equals(allot)
									&&Integer.parseInt(priceResult.getFangliang())==0){
								good.setRoomstatus(1l);
								good.setJlft(allot);
								good.setJlf(priceResult.getFangliang());
								System.out.println("待确认房");
								continue label;
							}
							
							//及时确认
							if (!"16".equals(allot)
									&&Integer.parseInt(priceResult.getFangliang())>0
									||"12".equals(allot)) {
								good.setYuliunum(1l);
								good.setRoomstatus(0l);
								good.setJlft(allot);
								good.setJlf(priceResult.getFangliang());
								System.out.println("及时确认房");
							} 
							//满房
							if("16".equals(allot)){
								good.setRoomstatus(1l);
								HotelGoodDatas.add(good);
								good.setJlft(allot);
								good.setJlf(priceResult.getFangliang());
								continue label;
							}
							good.setJlft(allot);
							good.setJlf(priceResult.getFangliang());
							System.out.println("正常确认房");
							good.setRoomstatus(0l);
							HotelGoodDatas.add(good);
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		proc.writeDataDB(HotelGoodDatas);
	}

}
