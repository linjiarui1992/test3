package com.ccservice.jielv.hotelupdate;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
/**
 * 第一次更新捷旅基础数据job-1
 */
public class UpdateDataJob1 implements Job{

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		System.out.println("开始更新-捷旅-第一次更新捷旅基础数据job...");
		JLUpdateJob.updateDataJob();
		System.out.println("结束更新--捷旅-第一次更新捷旅基础数据job...");
	}

}
