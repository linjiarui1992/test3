package com.ccservice.mixdata;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import com.ccervice.huamin.update.PHUtil;
import com.ccservice.b2b2c.atom.server.Server;
import com.ccservice.b2b2c.base.landmark.Landmark;

/**
 芒果网地标
{
    "gptypeId": 26,
    "provinceName": "S山东",
    "ID": 23088,
    "cityCode": "TNA",
    "cityName": "济南",
    "operationDate": "2011-09-08T00:00:00",
    "operationer": "IT",
    "address": "中国山东省济南市历下区文化东路73号山东师范大学",
    "baiduLongitude": null,
    "isActive": null,
    "longitude": 117.04977,
    "operationerId": "0",
    "latitude": 36.65515,
    "gisId": 23088,
    "name": "山东师范大学 ",
    "sortNum": 999,
    "baiduLatitude": null,
    "seqNo": 23088
}
 */

public class FindManGuoCoordinate {
    public static void main(String[] args) throws Exception {
        String url = "http://hotel.mangocity.com/asyhotelCityGitSearch.shtml";
        String json = PHUtil.submitPost(url, "").toString();
        JSONObject obj = JSONObject.fromObject(json);
        obj = obj.getJSONObject("resultMap");
        //景点
        JSONArray array = obj.getJSONArray("scenic");
        for (int i = 0; i < array.size(); i++) {
            JSONObject o = array.getJSONObject(i);
            String cityName = o.getString("cityName");
            if ("北京".equals(cityName) || "上海".equals(cityName) || "广州".equals(cityName) || "深圳".equals(cityName)) {
                String id = "MangGuo_" + o.getString("ID");
                String name = o.getString("name");
                Double longitude = o.getDouble("longitude");
                Double latitude = o.getDouble("latitude");

                System.out.println(cityName + "---" + name);

                Landmark landmark = new Landmark();
                landmark.setName(name);
                landmark.setCityid(getCityIdByName(cityName));
                landmark.setType("85");
                landmark.setLanguage(0);
                landmark.setLat(latitude);
                landmark.setLng(longitude);
                landmark.setElongmarkid(id);
                Server.getInstance().getHotelService().createLandmark(landmark);
            }
        }
        //大学
        array = obj.getJSONArray("university");
        for (int i = 0; i < array.size(); i++) {
            JSONObject o = array.getJSONObject(i);
            String cityName = o.getString("cityName");
            if ("北京".equals(cityName) || "上海".equals(cityName) || "广州".equals(cityName) || "深圳".equals(cityName)) {
                String id = "MangGuo_" + o.getString("ID");
                String name = o.getString("name");
                Double longitude = o.getDouble("longitude");
                Double latitude = o.getDouble("latitude");

                System.out.println(cityName + "---" + name);

                Landmark landmark = new Landmark();
                landmark.setName(name);
                landmark.setCityid(getCityIdByName(cityName));
                landmark.setType("86");
                landmark.setLanguage(0);
                landmark.setLat(latitude);
                landmark.setLng(longitude);
                landmark.setElongmarkid(id);
                Server.getInstance().getHotelService().createLandmark(landmark);
            }
        }
        //医院
        array = obj.getJSONArray("hospital");
        for (int i = 0; i < array.size(); i++) {
            JSONObject o = array.getJSONObject(i);
            String cityName = o.getString("cityName");
            if ("北京".equals(cityName) || "上海".equals(cityName) || "广州".equals(cityName) || "深圳".equals(cityName)) {
                String id = "MangGuo_" + o.getString("ID");
                String name = o.getString("name");
                Double longitude = o.getDouble("longitude");
                Double latitude = o.getDouble("latitude");

                System.out.println(cityName + "---" + name);

                Landmark landmark = new Landmark();
                landmark.setName(name);
                landmark.setCityid(getCityIdByName(cityName));
                landmark.setType("87");
                landmark.setLanguage(0);
                landmark.setLat(latitude);
                landmark.setLng(longitude);
                landmark.setElongmarkid(id);
                Server.getInstance().getHotelService().createLandmark(landmark);
            }
        }
    }

    private static long getCityIdByName(String name) {
        long cityid = 0l;
        if ("北京".equals(name)) {
            cityid = 101;//北京
        }
        else if ("上海".equals(name)) {
            cityid = 102;//上海
        }
        else if ("广州".equals(name)) {
            cityid = 732;//广州
        }
        else if ("深圳".equals(name)) {
            cityid = 734;//深圳
        }
        return cityid;
    }
}
