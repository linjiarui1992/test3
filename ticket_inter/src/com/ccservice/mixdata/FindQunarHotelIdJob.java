package com.ccservice.mixdata;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.compareprice.PropertyUtil;

public class FindQunarHotelIdJob implements Job{

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		PropertyUtil pu=new PropertyUtil();
		int type=Integer.valueOf(pu.getValue("qunarIdType"));
		if(type==1){
			try {
				new FindQunarHotelId().updateQunarHotelAllId();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			try {
				int sourcetype=Integer.valueOf(pu.getValue("qunarIdsourcetype"));
				new FindQunarHotelId().updateQunarHotelId(sourcetype);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
