package com.ccservice.mixdata;

import java.util.*;
import java.net.URLEncoder;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.ccervice.huamin.update.PHUtil;
import com.ccservice.b2b2c.base.city.City;
import com.ccservice.b2b2c.atom.server.Server;
import com.ccservice.b2b2c.base.province.Province;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;

/**
 * 去哪儿城市编码
 */

@SuppressWarnings("unchecked")
public class FindQunarCityCode {
	public static void main(String[] args) {
		List<City> citys = Server.getInstance().getHotelService().findAllCity("where C_TYPE = 1 and C_QUNARCODE is null and C_NAME is not null", "order by ID", -1, 0);
		for(City c:citys){
			try {
				String QunarCityCode = FindQunarCityCode.getQunarCityCode(c);
				if(!ElongHotelInterfaceUtil.StringIsNull(QunarCityCode)){
					c.setQunarcode(QunarCityCode);
					Server.getInstance().getHotelService().updateCityIgnoreNull(c);
				}
			} catch (Exception e) {
			}
		}
	}
	//本地省份
	private static Map<Long,String> LocalProvinces = new HashMap<Long, String>(); //省份ID，省份名称
	//加载省份
	static{
		List<Province> provinces = Server.getInstance().getHotelService().findAllProvince("", "", -1, 0);
		for(Province p:provinces){
			LocalProvinces.put(p.getId(), p.getName());
		}
	}
	//请求去哪儿
	public static String getQunarCityCode(City city) throws Exception{
		String ret = "";
		
		String cityname = city.getName();
		if("黄南藏族自治州".equals(cityname))cityname = "黄南";
		if("海南州".equals(cityname))cityname = "海南藏族自治州";
		if("凉山州".equals(cityname))cityname = cityname.substring(0, cityname.length()-1);
		if("阿坝州".equals(cityname))cityname = cityname.substring(0, cityname.length()-1);
		if("甘孜州".equals(cityname))cityname = cityname.substring(0, cityname.length()-1);
		if("红河州".equals(cityname))cityname = cityname.substring(0, cityname.length()-1);
		if("海北州".equals(cityname))cityname = cityname.substring(0, cityname.length()-1);
		if("海西州".equals(cityname))cityname = cityname.substring(0, cityname.length()-1);
		if("海东州".equals(cityname))cityname = cityname.substring(0, cityname.length()-1);
		if("德宏州".equals(cityname))cityname = cityname.substring(0, cityname.length()-1);
		if("黔南州".equals(cityname))cityname = cityname.substring(0, cityname.length()-1);
		if("黔西南州".equals(cityname))cityname = cityname.substring(0, cityname.length()-1);
		if("黔东南州".equals(cityname))cityname = cityname.substring(0, cityname.length()-1);
		if("迪庆州".equals(cityname))cityname = cityname.substring(0, cityname.length()-1);
		if("果洛州".equals(cityname))cityname = cityname.substring(0, cityname.length()-1);
		
		String url = "http://hs.qunar.com/api/hs/city/typeahead?city="+URLEncoder.encode(cityname, "utf-8");
		String json = PHUtil.submitPost(url, "").toString();
		
		System.out.println(city.getId() + "---" + city.getName() + "---" + json);
		
		//解析JSON
		//{"ret":true,"city":"北京","data":[{"c":"北京","o":"beijing_city","p":"北京-中国","t":0}]}
		JSONObject qunar = JSONObject.fromObject(json);
		JSONArray data = qunar.getJSONArray("data");
		if(data!=null && data.size()>0){
			for(int i = 0 ; i < data.size() ; i++){
				JSONObject d = data.getJSONObject(i);
				String c = d.getString("c");//去哪儿城市名称
				String o = d.getString("o");//去哪儿城市ID
				String p = d.getString("p").contains("-") ? d.getString("p").split("-")[1] : d.getString("p");//去哪儿省份
				if(ElongHotelInterfaceUtil.StringIsNull(c) || 
						ElongHotelInterfaceUtil.StringIsNull(o) ||
							ElongHotelInterfaceUtil.StringIsNull(p)){
					continue;
				}
				List<City> LocalCityList = Server.getInstance().getHotelService().findAllCity("where C_QUNARCODE = '"+o+"'", "", -1, 0);
				if(LocalCityList!=null && LocalCityList.size()>0){
					continue;
				}
				if(p.equals(getQunarProvince(LocalProvinces.get(city.getProvinceid())))){//同一个省份、名称相同
					if(c.endsWith("县") || c.endsWith("区") || c.endsWith("市") || c.endsWith("镇")){
						c = c.substring(0, c.length()-1);
					}
					if(cityname.endsWith("县") || cityname.endsWith("区") || cityname.endsWith("市") || cityname.endsWith("镇")){
						cityname = cityname.substring(0, cityname.length()-1);
					}
					if(c.equals(cityname)){
						ret = o;
						break;
					}
					if("阿拉善盟".equals(c) && "阿拉善".equals(city.getName())){
						ret = o;
						break;
					}
					if("旅顺口".equals(c) && "旅顺".equals(city.getName())){
						ret = o;
						break;
					}
					if("景宁畲族自治县".equals(c) && "景宁".equals(city.getName())){
						ret = o;
						break;
					}
					if("襄樊".equals(c) && "襄阳".equals(city.getName())){
						ret = o;
						break;
					}
				}
				if("重庆".equals(c) && c.equals(city.getName()) && p.equals("中国")){
					ret = o;
					break;
				}
			}
		}
		
		return ret;
	}
	//本地省份转去哪儿省份
	private static String getQunarProvince(String ProvinceName){
		String ret = "";
		if("北京".equals(ProvinceName) || "上海".equals(ProvinceName) ||
				"天津".equals(ProvinceName) || "香港".equals(ProvinceName) || "澳门".equals(ProvinceName)){
			ret = "中国";
		}else if("内蒙古".equals(ProvinceName)){
			ret = "内蒙古自治区";
		}else if("广西".equals(ProvinceName)){
			ret = "广西壮族自治区";
		}else if("西藏".equals(ProvinceName)){
			ret = "西藏自治区";
		}else if("西藏".equals(ProvinceName)){
			ret = "西藏自治区";
		}else if("宁夏".equals(ProvinceName)){
			ret = "宁夏回族自治区";
		}else if("新疆".equals(ProvinceName)){
			ret = "新疆维吾尔自治区";
		}else if("重庆".equals(ProvinceName)){
			ret = "重庆";
		}else if("台湾".equals(ProvinceName)){
			ret = "台湾";
		}else{
			ret = ProvinceName + "省";
		}
		return ret;
	}
}
