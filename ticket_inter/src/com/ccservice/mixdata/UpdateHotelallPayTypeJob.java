package com.ccservice.mixdata;

import java.util.List;
import java.util.ArrayList;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.inter.server.Server;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.elong.inter.DateSwitch;
import com.ccservice.b2b2c.base.hotelall.Hotelall;

/**
 * 更新正式酒店的支付方式
 * @author WH
 */
public class UpdateHotelallPayTypeJob implements Job{
	@SuppressWarnings("unchecked")
	public void execute(JobExecutionContext context) throws JobExecutionException {
		long start = System.currentTimeMillis();
		//正式酒店
		String sql = "where ID in (select distinct C_ZSHOTELID from T_HOTEL where C_PAYTYPE in (1,2) and C_ZSHOTELID is not null)";
		List<Hotelall> hotelalls = Server.getInstance().getHotelService().findAllHotelall(sql, "", -1, 0);
		if(hotelalls!=null && hotelalls.size()>0){
			for(Hotelall hotelall:hotelalls){
				//临时酒店
				String tempsql = "where C_PAYTYPE in (1,2) and C_ZSHOTELID = " + hotelall.getId();
				List<Hotel> hotels = Server.getInstance().getHotelService().findAllHotel(tempsql, "", -1, 0);
				//正式酒店支付方式
				Long paytype = null;
				if(hotels!=null && hotels.size()>0){
					List<Long> paytypelist = new ArrayList<Long>();
					for(Hotel temph:hotels){
						Long temppaytype = temph.getPaytype();
						if(temppaytype!=null && !paytypelist.contains(temppaytype)){
							paytypelist.add(temppaytype);
						}
					}
					if(paytypelist.size()==1){
						paytype = paytypelist.get(0);
					}else if(paytypelist.size()>1){
						paytype = 3l;//现、预付
					}
				}
				boolean flag = false;
				if((hotelall.getPaytype()==null && paytype!=null) || !hotelall.getPaytype().equals(paytype)){
					flag = true;
				}
				if(flag){
					String a = hotelall.getPaytype()==null ? "空" : Long.toString(hotelall.getPaytype());
					String b = paytype==null ? "空" : Long.toString(paytype);
					
					hotelall.setPaytype(paytype);
					Server.getInstance().getHotelService().updateHotelall(hotelall);//更新正式酒店信息
					
					System.out.println("更新正式酒店"+hotelall.getName()+"["+hotelall.getId()+"]的支付方式：由" + a + "更新为" + b);
				}else{
					System.out.println("正式酒店"+hotelall.getName()+"["+hotelall.getId()+"]的支付方式无需更新");
				}
			}
		}
		long end = System.currentTimeMillis();
		System.out.println("正式酒店支付方式更新-----耗时-----" + DateSwitch.showTime(end - start));
	}
}
