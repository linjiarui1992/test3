package com.ccservice.mixdata;

import java.util.List;

import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.b2b2c.base.roomtypeofficial.RoomTypeOfficial;
import com.ccservice.inter.server.Server;

/**
 * 正式房型与正式酒店关联程序
 * @author wzc
 *
 */
public class RoomTypeMixConn {
	public static void main(String[] args) {
		ConHotelRoomtype();
	}
	/**
	 * 正式房型与正式酒店关联程序
	 */
	public static void ConHotelRoomtype(){
		List<Hotel> hotels=Server.getInstance().getHotelService().findAllHotel("where c_zshotelid is not null and id in (select c_hotelid from t_roomtype where c_zshotelid is null group by c_hotelid)", "", -1, 0);
		int k=hotels.size();
		for (int i = 0; i < hotels.size(); i++) {
			Hotel hotel=hotels.get(i);
			System.out.println("剩余酒店："+(k--));
			List<RoomTypeOfficial> roomtypes=Server.getInstance().getHotelService().findAllRoomTypeOfficial("where c_hotelid="+hotel.getId(), "", -1, 0);
			for (int j = 0; j < roomtypes.size(); j++) {
				RoomTypeOfficial roomtype=roomtypes.get(j);
				roomtype.setZshotelid(hotel.getZshotelid());
				Server.getInstance().getHotelService().updateRoomTypeOfficialIgnoreNull(roomtype);
			}
		}
	}
}
