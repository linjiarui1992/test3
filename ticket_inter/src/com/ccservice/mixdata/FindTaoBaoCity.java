package com.ccservice.mixdata;

import java.util.*;
import java.net.URLDecoder;
import java.net.URLEncoder;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import com.ccervice.huamin.update.PHUtil;
import com.ccservice.b2b2c.base.city.City;
import com.ccservice.b2b2c.atom.server.Server;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;

public class FindTaoBaoCity {

    private static String cityUrl = "http://kezhan.trip.taobao.com/citysuggest.do?q=";

    //http://kezhan.trip.taobao.com/ajax/suggest/autoSuggestion.htm?_ksTS=1402561279013_2778&callback=jsonp2779&city=&cityName=%E5%8C%97%E4%BA%AC&callback=&q=%E7%8E%89%E6%B3%89%E8%B7%AF&_input_charset=utf-8&type=0&hotelType=0

    public static void main(String[] args) throws Exception {
        int all = 0;
        List<City> citys = Server.getInstance().getHotelService()
                .findAllCity("where C_TYPE = 1 and C_HTCODE is null", "ORDER BY C_SORT", -1, 0);
        for (City c : citys) {
            String q = c.getName().trim();
            if (q.length() > 2 && (q.endsWith("市") || q.endsWith("县") || q.endsWith("州"))) {
                q = q.substring(0, q.length() - 1);
            }
            String url = cityUrl + URLEncoder.encode(q, "gbk");
            String json = PHUtil.submitGet(url).toString();
            if (!ElongHotelInterfaceUtil.StringIsNull(json) && json.contains("\"c\"")) {
                json = json.substring(1);
                json = json.substring(0, json.length() - 1);
                JSONObject obj = JSONObject.fromObject(json);
                JSONArray ary = obj.getJSONArray("result");
                obj = ary.getJSONObject(0);
                String id = obj.getString("c").trim();
                String name = obj.getString("t").trim();
                name = URLDecoder.decode(name, "gbk").split("_")[0];
                System.out.println(c.getName() + "---" + id + "---" + name);
                c.setHtcode(id);
                Server.getInstance().getHotelService().updateCityIgnoreNull(c);
            }
        }
    }
}
