package com.ccservice.mixdata;

import com.ccervice.huamin.update.PHUtil;
import com.ccservice.newelong.util.UpdateElDataUtil;

/**
 * 获取去哪儿mixKey
 * @author WH
 */
public class GetQunarMixKey {
	/**
	 * @param cityurl 去哪儿城市ID
	 */
	public static String get(String cityurl) throws Exception{
		String mixKey = "";
		
		String fromDate = UpdateElDataUtil.getCurrentDate();
		String toDate = UpdateElDataUtil.getAddDate(fromDate, 1);
		String strUrl = "http://hotel.qunar.com/city/" + cityurl + "/#fromDate=" + fromDate + "&toDate=" + toDate + "&from=hotellist&QHFP=ZSL_A491C891&bs=&bc=";
		System.out.println("MixKey失效，重新POST请求去哪儿MixKey：" + strUrl);
		String html = PHUtil.submitPost(strUrl, "").toString();
		int start = html.indexOf("<span id=\"eyKxim\" style=\"display:none\">");
		html = html.substring(start);
		html = html.substring(html.indexOf(">") + 1);
		mixKey = html.substring(0, html.indexOf("<"));
		
		return mixKey;
	}
}
