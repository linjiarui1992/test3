package com.ccservice.qunar.update;

import java.util.*;
import org.quartz.Job;
import net.sf.json.JSONObject;
import org.quartz.JobExecutionContext;
import com.gargoylesoftware.htmlunit.*;
import org.quartz.JobExecutionException;
import com.ccservice.inter.server.Server;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.b2b2c.base.hotelall.Hotelall;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;

/**去哪儿酒店状态同步*/

public class SyncQunarHotelStatus implements Job {

    @SuppressWarnings("unchecked")
    public void execute(JobExecutionContext context) throws JobExecutionException {
        long start = System.currentTimeMillis();
        System.out.println(ElongHotelInterfaceUtil.getCurrentTime() + " ---> 开始同步去哪儿酒店状态...");

        //关闭日志输出
        org.apache.commons.logging.LogFactory.getFactory().setAttribute("org.apache.commons.logging.Log",
                "org.apache.commons.logging.impl.NoOpLog");

        //代理IP
        String ip = PropertyUtil.getValue("AgentIp");
        ip = ElongHotelInterfaceUtil.StringIsNull(ip) ? "localhost" : ip.trim();

        //实例化WebClient
        WebClient webClient = null;
        if ("localhost".equals(ip)) {
            webClient = new WebClient(BrowserVersion.getDefault());
        }
        else {
            webClient = new WebClient(BrowserVersion.getDefault(), ip.split(":")[0], Integer.parseInt(ip.split(":")[1]));
        }

        webClient.getOptions().setTimeout(30000);
        webClient.getOptions().setJavaScriptEnabled(true);
        webClient.getCookieManager().setCookiesEnabled(true);
        webClient.setAjaxController(new NicelyResynchronizingAjaxController());

        webClient.getOptions().setCssEnabled(false);
        webClient.getOptions().setActiveXNative(false);
        webClient.getOptions().setAppletEnabled(false);
        webClient.getOptions().setThrowExceptionOnScriptError(false);
        webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);

        webClient.addRequestHeader("Accept",
                "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        webClient.addRequestHeader("Accept-Encoding", "gzip,deflate,sdch");
        webClient.addRequestHeader("Accept-Language", "zh-CN,zh;q=0.8,en;q=0.6,zh-TW;q=0.4");
        webClient.addRequestHeader("Cache-Control", "max-age=0");
        webClient.addRequestHeader("Connection", "keep-alive");
        webClient.addRequestHeader("User-Agent",
                "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1976.2 Safari/537.36");

        List<Hotelall> hotels = Server.getInstance().getHotelService().findAllHotelall("", "", -1, 0);
        int size = hotels.size();

        int count = 0;
        int total = 300;
        StringBuffer ids = new StringBuffer();
        Map<String, Long> idMap = new HashMap<String, Long>();

        for (int i = 0; i < size; i++) {
            Hotelall hotel = hotels.get(i);
            try {
                String qid = hotel.getQunarId();
                if (ElongHotelInterfaceUtil.StringIsNull(qid)) {
                    continue;
                }

                System.out.println(hotel.getName());

                ids.append(qid).append(",");
                idMap.put(qid, hotel.getId());
                count++;

                if (count == total || i == size - 1) {
                    String seqs = ids.toString();
                    seqs = seqs.substring(0, seqs.length() - 1);

                    String url = "http://hotel.qunar.com/price/historyView.jsp?seqs=" + seqs + "&callback=XQScript_5";

                    String ret = "";
                    try {
                        ret = webClient.getPage(url).getWebResponse().getContentAsString();
                    }
                    catch (Exception e) {
                        JavaScriptPage page = webClient.getPage(url);
                        ret = page.getWebResponse().getContentAsString();
                    }

                    if (!ElongHotelInterfaceUtil.StringIsNull(ret) && ret.contains("os")) {
                        ret = ret.substring(ret.indexOf("{"));
                        ret = ret.substring(0, ret.lastIndexOf("}") + 1);
                        JSONObject result = JSONObject.fromObject(ret);
                        Iterator<String> keys = result.keys();
                        while (keys.hasNext()) {
                            String key = keys.next();
                            JSONObject obj = result.getJSONObject(key);
                            if (obj.containsKey("os")) {
                                String os = obj.getString("os");
                                int state = "营业中".equals(obj.getString("os")) ? 3 : 0;//状态，营业中
                                Double pr = obj.containsKey("pr") ? obj.getDouble("pr") : 0d;//最低价
                                String sql = "update T_HOTELALL set C_STATE = " + state + ", C_STATEDESC = '" + os
                                        + "', C_STARTPRICE = " + pr + " where ID = " + idMap.get(key) + ";\n";
                                System.out.println(sql);
                                Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                            }
                        }
                    }

                    if (i != size - 1) {
                        System.out.println("休息5秒...");
                        Thread.sleep(1000 * 5);
                    }
                }
            }
            catch (Exception e) {

            }
            finally {
                if (count == total || i == size - 1) {
                    count = 0;
                    ids = new StringBuffer();
                    idMap = new HashMap<String, Long>();
                }
            }
        }
        long end = System.currentTimeMillis();
        System.out.println(ElongHotelInterfaceUtil.getCurrentTime() + " ---> 结束同步去哪儿酒店状态，消耗时间：" + (end - start) / 1000
                + "秒。");
    }

}