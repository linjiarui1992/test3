package com.ccservice.qunar.update;

import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import com.ccservice.inter.server.Server;

/**
 * 平均每个服务器酒店个数，获取城市ID
 * @author WH
 */

public class GetCityIdsByAvgHotelCount {

    @SuppressWarnings("rawtypes")
    public static void main(String[] args) {
        //总共服务器数量，8个服务器，大概一周可以更新完
        int serverNum = 8;
        //酒店总数
        String sql = "select COUNT(*) TOTAL from T_HOTELALL where C_STATE = 3 and C_STARTPRICE > 0";
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        Map tm = (Map) list.get(0);
        int hotelTotal = Integer.parseInt(tm.get("TOTAL").toString());
        //每个城市酒店数
        sql = "select C_CITYID ID,COUNT(*) TOTAL from T_HOTELALL where C_STATE = 3 and C_STARTPRICE > 0 group by C_CITYID";
        list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        //城市总数
        int citySize = list.size();
        //每个服务器酒店数
        int everySize = hotelTotal / serverNum;
        System.out.println("酒店总数：" + hotelTotal + "；城市总数：" + citySize + "；每个服务器酒店数：" + everySize);
        //城市ID
        List<String> ids = new ArrayList<String>();
        //当前累计总数
        int currentTotal = 0;
        for (int i = 0; i < citySize; i++) {
            if (currentTotal >= everySize || i == citySize - 1) {
                System.out.println(ids.toString());
                //reset
                currentTotal = 0;
                ids = new ArrayList<String>();
            }
            Map map = (Map) list.get(i);
            //城市ID
            String cId = map.get("ID").toString();
            //城市酒店数量
            int cTotal = Integer.parseInt(map.get("TOTAL").toString());
            //累加
            currentTotal += cTotal;
            ids.add(cId);
        }
    }

}