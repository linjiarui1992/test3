package com.ccservice.qunar.cache;

import java.util.Map;
import java.net.URLEncoder;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.ccervice.huamin.update.PHUtil;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;

public class ThreadServerForSaveCache extends Thread {

    private String qunarId;

    private Map<String, JSONArray> roomMap;

    public ThreadServerForSaveCache(String qunarId, Map<String, JSONArray> roomMap) {
        this.qunarId = qunarId;
        this.roomMap = roomMap;
    }

    public void run() {
        try {
            String CacheServerUrl = PropertyUtil.getValue("CacheServerUrl");
            if (ElongHotelInterfaceUtil.StringIsNull(CacheServerUrl) || roomMap == null || roomMap.size() == 0
                    || ElongHotelInterfaceUtil.StringIsNull(qunarId)) {
                return;
            }
            //JSON串，用于异步向服务器存缓存
            JSONArray json = new JSONArray();
            //ForEach
            for (String roomName : roomMap.keySet()) {
                JSONObject obj = new JSONObject();
                obj.put("roomName", roomName);
                obj.put("roomArray", roomMap.get(roomName));
                json.add(obj.toString());
            }
            JSONObject obj = new JSONObject();
            obj.put("qid", qunarId);
            obj.put("data", json.toString());
            //请求缓存服务器
            PHUtil.submitPost(CacheServerUrl, URLEncoder.encode(obj.toString(), "utf-8"));
        }
        catch (Exception e) {
        }
    }

    public String getQunarId() {
        return qunarId;
    }

    public void setQunarId(String qunarId) {
        this.qunarId = qunarId;
    }

    public Map<String, JSONArray> getRoomMap() {
        return roomMap;
    }

    public void setRoomMap(Map<String, JSONArray> roomMap) {
        this.roomMap = roomMap;
    }

}