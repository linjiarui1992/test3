package com.ccservice.qunar.cache;

import com.gargoylesoftware.htmlunit.*;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

public class CheckAgentIp {

    public static void main(String[] args) {
        print("183.203.8.148:8080");
        print("111.13.109.54:80");
        print("183.221.217.125:8123");
        print("59.79.41.112:8585");
        print("221.176.14.72:80");
        print("59.175.137.83:88");
        print("221.10.102.203:82");
        print("61.234.123.64:8080");
        print("183.230.53.116:8123");
        print("183.221.160.247:8123");
        print("112.25.137.10:8123");
    }

    public static void print(String ip) {
        System.out.print(ip + " ---> ");
        //关闭日志输出
        org.apache.commons.logging.LogFactory.getFactory().setAttribute("org.apache.commons.logging.Log",
                "org.apache.commons.logging.impl.NoOpLog");
        //实例化WebClient
        WebClient webClient = new WebClient(BrowserVersion.getDefault(), ip.split(":")[0], Integer.parseInt(ip
                .split(":")[1]));
        //设置
        webClient.getOptions().setCssEnabled(false);
        webClient.getOptions().setActiveXNative(false);
        webClient.getOptions().setAppletEnabled(false);
        webClient.getOptions().setRedirectEnabled(false);
        webClient.getOptions().setJavaScriptEnabled(false);
        webClient.getCookieManager().setCookiesEnabled(false);
        webClient.getOptions().setThrowExceptionOnScriptError(false);
        webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
        //请求头
        webClient.addRequestHeader("Accept",
                "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        webClient.addRequestHeader("Accept-Encoding", "gzip,deflate,sdch");
        webClient.addRequestHeader("Accept-Language", "zh-CN,zh;q=0.8");
        webClient.addRequestHeader("Connection", "keep-alive");
        String UserAgent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.122 Safari/537.36";
        webClient.addRequestHeader("User-Agent", UserAgent);
        //请求
        try {
            HtmlPage page = webClient.getPage("http://1111.ip138.com/ic.asp");
            String html = page.asText();
            System.out.println(html.substring(html.indexOf("您的IP是：["), html.indexOf("]") + 1));
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}