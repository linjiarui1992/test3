package com.ccservice.qunar.cache;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import com.ccservice.elong.inter.PropertyUtil;

/**
 * 缓存去哪儿酒店价格
 * @author WH
 */

public class CatchQunarHotelPriceJob implements Job {

    public void execute(JobExecutionContext context) throws JobExecutionException {
        int CacheDataType = Integer.parseInt(PropertyUtil.getValue("CacheDataType"));
        String print = CacheDataType == 1 ? "更新去哪儿缓存价格" : "缓存去哪儿酒店价格";
        System.out.println("开始" + print + "...");
        CatchQunarHotelPrice.cache(CacheDataType);
        System.out.println("结束" + print + "...");
    }

    public static void main(String[] args) {
        int CacheDataType = Integer.parseInt(PropertyUtil.getValue("CacheDataType"));
        String print = CacheDataType == 1 ? "更新去哪儿缓存价格" : "缓存去哪儿酒店价格";
        System.out.println("开始" + print + "...");
        CatchQunarHotelPrice.cache(CacheDataType);
        System.out.println("结束" + print + "...");
    }

}