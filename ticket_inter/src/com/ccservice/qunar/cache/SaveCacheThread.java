package com.ccservice.qunar.cache;

import java.util.Map;
import java.util.List;
import java.util.HashMap;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.ccservice.inter.server.Server;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlDivision;
import com.gargoylesoftware.htmlunit.html.HtmlListItem;

/**
 * 保存缓存线程
 * @author WH
 */

public class SaveCacheThread extends Thread {
    private int type;

    private long start;

    private HtmlPage page;

    private String hotelName;

    private WebClient webClient;

    private String qunarHotelId;

    public SaveCacheThread(WebClient webClient, HtmlPage page, String hotelName, String qunarHotelId, long start,
            int type) {
        this.type = type;
        this.start = start;
        this.page = page;
        this.hotelName = hotelName;
        this.webClient = webClient;
        this.qunarHotelId = qunarHotelId;
    }

    public void run() {
        try {
            reqQunar(webClient, page, hotelName, qunarHotelId, start, type);
        }
        catch (Exception e) {
        }
    }

    public void reqQunar(WebClient webClient, HtmlPage page, String hotelName, String qunarHotelId, long start, int type)
            throws Exception {
        //缓存Map
        Map<String, JSONArray> roomMap = new HashMap<String, JSONArray>();
        //解析
        HtmlDivision roomTool = (HtmlDivision) page.getElementById("roomTool");
        List<HtmlListItem> lis = roomTool.getHtmlElementsByTagName("li");
        for (HtmlListItem li : lis) {
            try {
                String liName = li.toString();
                //展开报价
                if (!liName.contains("similar-expand")) {
                    HtmlElement div = (HtmlElement) li.getFirstElementChild();
                    div.click();
                }
                String roomName = "";
                String bedName = "";
                String roomDesc = "";
                //价格列表
                Iterable<DomElement> childs = li.getChildElements();
                for (DomElement ele : childs) {
                    try {
                        String eleName = ele.toString();
                        //表头
                        if ("".equals(roomName) && eleName.contains("type-title js-room-title")) {
                            HtmlDivision roomTitle = (HtmlDivision) ele;
                            List<HtmlElement> spans = roomTitle.getElementsByTagName("span");
                            for (HtmlElement span : spans) {
                                //房型名称
                                if (span.toString().contains("type-name js-p-name")) {
                                    roomName = span.asText();
                                    System.out.println("==========" + hotelName + " ===== " + roomName + "==========");
                                }
                                //床型
                                else if (span.toString().contains("roomtxt js-roomtxt")) {
                                    //房型描述
                                    StringBuffer buf = new StringBuffer();
                                    List<HtmlElement> cites = span.getElementsByTagName("cite");
                                    if (cites != null && cites.size() > 0) {
                                        for (HtmlElement cite : cites) {
                                            String citeTxt = cite.asText();
                                            if (ElongHotelInterfaceUtil.StringIsNull(citeTxt)) {
                                                continue;
                                            }
                                            if (!citeTxt.endsWith("；")) {
                                                citeTxt = citeTxt + "；";
                                            }
                                            buf.append(citeTxt);
                                        }
                                        roomDesc = buf.toString();
                                        if (roomDesc.endsWith("；")) {
                                            roomDesc = roomDesc.substring(0, roomDesc.length() - 1);
                                        }
                                    }
                                    //床型名称
                                    bedName = span.asText();
                                    if (bedName.contains("大床") && bedName.contains("双床")) {
                                        bedName = "大床/双床";
                                    }
                                    else if (bedName.contains("大床")) {
                                        bedName = "大床";
                                    }
                                    else if (bedName.contains("双床")) {
                                        bedName = "双床";
                                    }
                                    else if (bedName.contains("单人床")) {
                                        bedName = "单人床";
                                    }
                                    else {
                                        bedName = "其他";
                                    }
                                }
                            }
                        }
                        //价格
                        else if (eleName.contains("type-title-inner")) {
                            if ("".equals(roomName)) {
                                roomName = "其他";
                            }
                            if ("".equals(bedName)) {
                                bedName = "其他";
                            }
                            HtmlDivision roomPrice = (HtmlDivision) ele;
                            roomPrice = (HtmlDivision) roomPrice.getFirstChild();
                            Iterable<DomElement> priceEles = roomPrice.getChildElements();
                            //查看其他报价
                            for (DomElement priceEle : priceEles) {
                                try {
                                    String priceEleName = priceEle.toString();
                                    if (priceEleName.contains("more-quote js-room-more")
                                            && !priceEleName.contains("display:none")) {
                                        HtmlDivision moreDiv = (HtmlDivision) priceEle;
                                        moreDiv.click();
                                        break;
                                    }
                                }
                                catch (Exception e) {
                                }
                            }
                            //价格列表
                            for (DomElement priceEle : priceEles) {
                                try {
                                    String priceEleName = priceEle.toString();
                                    if (priceEleName.contains("similar-type-agent-list")) {
                                        priceEle = priceEle.getFirstElementChild();
                                        Iterable<DomElement> agents = priceEle.getChildElements();
                                        for (DomElement agent : agents) {
                                            String agentId = "wiqunarqta2";
                                            String agentName = "去哪儿酒店直销";
                                            String paytype = "";//0：现付；1：预付
                                            double price = 0d;//真实价格
                                            double priceratio = 0d;//价格比例
                                            double fanyong = 0d;//返佣
                                            String roombf = "";//套餐，如提前，暂不取
                                            int roomstatus = 1;//房态 1 开房；-1 关房；-2 休息中
                                            String hashcode = "";//暂不取
                                            int danbaoflag = 0;//0：不担保；1：担保
                                            String bfstr = "无早";//早餐
                                            String webstr = "无";//宽带
                                            String agentRoomName = "";//代理房型名称
                                            //价格明细
                                            DomElement priceDetail = agent.getFirstElementChild();
                                            //源码
                                            String html = priceDetail.asXml();
                                            //价格Table元素
                                            HtmlElement priceTable = (HtmlElement) priceDetail.getFirstElementChild();
                                            //代理ID及支付方式：payment=0&CPCB=wiqunarqcw8
                                            //代理ID
                                            String key = "CPCB=";
                                            if (html.contains(key)) {
                                                String temphtml = html.substring(html.indexOf(key) + key.length());
                                                temphtml = temphtml.substring(0, temphtml.indexOf("&"));
                                                agentId = temphtml;
                                            }
                                            //支付方式
                                            key = "payment=1&";
                                            if (html.contains(key) || html.contains("预付") || html.contains("预先支付")) {
                                                paytype = "1";
                                            }
                                            else {
                                                key = "payment=0&";
                                                if (html.contains(key) || html.contains("返现") || html.contains("需担保")) {
                                                    paytype = "0";
                                                }
                                                else {
                                                    paytype = "1";
                                                }
                                            }
                                            //Table Td
                                            List<HtmlElement> priceTds = priceTable.getElementsByTagName("td");
                                            for (HtmlElement priceTd : priceTds) {
                                                String priceTdStr = priceTd.asXml();
                                                //代理房型名称
                                                if (priceTdStr.contains("js-order-detail clrfix order-detail")) {
                                                    HtmlElement agentRoomEle = (HtmlElement) priceTd
                                                            .getFirstElementChild();
                                                    agentRoomName = agentRoomEle.asText();
                                                    if (!ElongHotelInterfaceUtil.StringIsNull(agentRoomName)) {
                                                        if (agentRoomName.contains("-")) {
                                                            agentRoomName = agentRoomName.split("-")[0].trim();
                                                        }
                                                    }
                                                    else {
                                                        agentRoomName = "";
                                                    }
                                                }
                                                //代理名称
                                                if (priceTdStr.contains("js-logo-img")) {
                                                    HtmlElement img = priceTd.getElementsByTagName("img").get(0);
                                                    agentName = img.asXml();
                                                    key = "alt=\"";
                                                    agentName = agentName.substring(agentName.indexOf(key)
                                                            + key.length());
                                                    agentName = agentName.substring(0, agentName.indexOf("\""));
                                                }
                                                //早餐
                                                else if (priceTdStr.contains("js-breakfast-yes breakfast-yes")) {
                                                    HtmlElement bf = (HtmlElement) priceTd.getFirstElementChild();
                                                    bfstr = bf.asText();
                                                    if (!bfstr.contains("早")) {
                                                        bfstr = "无早";
                                                    }
                                                }
                                                //宽带
                                                else if (priceTdStr.contains("js-you-inter you-inter")) {
                                                    HtmlElement web = (HtmlElement) priceTd.getFirstElementChild();
                                                    webstr = web.asText();
                                                    if (!"无".equals(webstr) && !"免费".equals(webstr)
                                                            && !"收费".equals(webstr)) {
                                                        webstr = "无";
                                                    }
                                                }
                                                //担保
                                                else if ("0".equals(paytype) && priceTdStr.contains("需担保")) {
                                                    danbaoflag = 1;
                                                }
                                                //价格
                                                else if (priceTdStr.contains("count_pr")
                                                        && priceTdStr.contains("final-price")) {
                                                    HtmlElement priceDiv = (HtmlElement) priceTd.getFirstElementChild();
                                                    priceDiv = priceDiv.getElementsByTagName("p").get(1);
                                                    String priceStr = priceDiv.asText();
                                                    String fanyongStr = "0";
                                                    if (!priceStr.startsWith("¥")) {
                                                        continue;
                                                    }
                                                    priceStr = priceStr.substring(1);
                                                    if (priceStr.contains("加税费")) {
                                                        priceStr = priceStr.substring(0, priceStr.indexOf("¥"));
                                                    }
                                                    else if (priceStr.contains("立减")) {
                                                        priceStr = priceStr.substring(0, priceStr.indexOf("¥"));
                                                    }
                                                    //现付返佣
                                                    if ("0".equals(paytype) && priceStr.contains("返")) {
                                                        fanyongStr = priceStr.substring(priceStr.indexOf("返") + 2);
                                                        priceStr = priceStr.substring(priceStr.indexOf("¥") + 1,
                                                                priceStr.indexOf("返"));
                                                    }
                                                    try {
                                                        price = Double.parseDouble(priceStr);
                                                        fanyong = Double.parseDouble(fanyongStr);
                                                    }
                                                    catch (Exception e) {
                                                        System.out.println("ERROR-----" + priceStr);
                                                        price = 0d;
                                                    }
                                                }
                                                //房态
                                                else if (priceTdStr.contains("btn-book") && priceTdStr.contains("订完")) {
                                                    roomstatus = -1;
                                                }
                                            }

                                            if (price <= 0) {
                                                continue;
                                            }

                                            System.out.println(hotelName + " / " + roomName + " / " + bedName + " / "
                                                    + agentId + " / " + agentName + " / "
                                                    + ("1".equals(paytype) ? "Advance" : "现付") + " / " + bfstr + " / "
                                                    + webstr + " / " + price + " / "
                                                    + (roomstatus == 1 ? "开房" : "Close"));

                                            JSONObject roomObj = new JSONObject();
                                            roomObj.put("bedName", bedName);
                                            roomObj.put("agentId", agentId);
                                            roomObj.put("agentName", agentName);
                                            roomObj.put("paytype", paytype);
                                            roomObj.put("bfstr", bfstr);
                                            roomObj.put("webstr", webstr);
                                            roomObj.put("danbaoflag", danbaoflag);
                                            roomObj.put("price", price);
                                            roomObj.put("fanyong", fanyong);
                                            roomObj.put("roomstatus", roomstatus);
                                            roomObj.put("priceratio", priceratio);
                                            roomObj.put("roombf", roombf);
                                            roomObj.put("hashcode", hashcode);
                                            roomObj.put("agentRoomName", agentRoomName);
                                            roomObj.put("roomDesc", roomDesc);

                                            JSONArray roomArray = roomMap.get(roomName);
                                            if (roomArray == null) {
                                                roomArray = new JSONArray();
                                            }
                                            roomArray.add(roomObj.toString());
                                            roomMap.put(roomName, roomArray);

                                        }
                                    }
                                }
                                catch (Exception e) {
                                }
                            }
                        }
                    }
                    catch (Exception e) {
                    }
                }
            }
            catch (Exception e) {
            }
        }

        //缓存
        if (roomMap.size() > 0) {
            //上传缓存到服务器
            new ThreadServerForSaveCache(qunarHotelId, roomMap).start();
            //本地存缓存
            Server.getInstance().getHotelCacheService().getQunarHotelPrice(qunarHotelId, roomMap, type);
        }

        //关闭
        webClient.closeAllWindows();

        long end = System.currentTimeMillis();
        System.out.println(hotelName + "，结束解析源文件，消耗时间：" + (end - start) + "毫秒");
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public HtmlPage getPage() {
        return page;
    }

    public void setPage(HtmlPage page) {
        this.page = page;
    }

    public WebClient getWebClient() {
        return webClient;
    }

    public void setWebClient(WebClient webClient) {
        this.webClient = webClient;
    }

    public String getQunarHotelId() {
        return qunarHotelId;
    }

    public void setQunarHotelId(String qunarHotelId) {
        this.qunarHotelId = qunarHotelId;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

}