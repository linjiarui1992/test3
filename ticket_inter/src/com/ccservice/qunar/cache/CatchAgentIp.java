package com.ccservice.qunar.cache;

import java.util.List;
import java.util.Random;
import java.util.ArrayList;

import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;

public class CatchAgentIp {

    //1：http://ip.zdaye.com/ ； 2：http://proxy.com.ru/gaoni/；3：http://www.xici.net.co/nn
    private static int flag = Integer.parseInt(PropertyUtil.getValue("CatchIpWebNo"));

    public static List<String> getIp() {
        //http://ip.zdaye.com/
        if (flag == 1) {
            return Ip_Zdaye_Com();
        }
        //http://proxy.com.ru/gaoni/
        else if (flag == 2) {
            return Proxy_Com_Ru();
        }
        //http://www.xici.net.co/nn
        else if (flag == 3) {
            return Xici_NetCo(new Random().nextInt(3) + 1);
        }
        else {
            return new ArrayList<String>();
        }
    }

    private static List<String> Xici_NetCo(int idx) {
        List<String> ips = new ArrayList<String>();
        try {
            String url = "http://www.xici.net.co/nn/" + idx;
            //关闭日志输出
            org.apache.commons.logging.LogFactory.getFactory().setAttribute("org.apache.commons.logging.Log",
                    "org.apache.commons.logging.impl.NoOpLog");
            //实例化WebClient
            WebClient webClient = new WebClient(BrowserVersion.getDefault());
            //设置
            webClient.getOptions().setCssEnabled(false);
            webClient.getOptions().setActiveXNative(false);
            webClient.getOptions().setAppletEnabled(false);
            webClient.getOptions().setRedirectEnabled(false);
            webClient.getOptions().setJavaScriptEnabled(false);
            webClient.getCookieManager().setCookiesEnabled(false);
            webClient.getOptions().setThrowExceptionOnScriptError(false);
            webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
            //请求头
            webClient.addRequestHeader("Accept",
                    "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
            webClient.addRequestHeader("Accept-Encoding", "gzip,deflate,sdch");
            webClient.addRequestHeader("Accept-Language", "zh-CN,zh;q=0.8");
            webClient.addRequestHeader("Cache-Control", "max-age=0");
            webClient.addRequestHeader("Connection", "keep-alive");
            String UserAgent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.122 Safari/537.36";
            webClient.addRequestHeader("User-Agent", UserAgent);
            //请求
            HtmlPage page = webClient.getPage(url);
            //解析
            DomElement iplist = page.getElementById("ip_list");
            List<HtmlElement> trs = iplist.getElementsByTagName("tr");
            for (HtmlElement tr : trs) {
                try {
                    if (!tr.asText().contains("高匿")) {
                        continue;
                    }
                    String ip = "";
                    int port = 0;
                    List<HtmlElement> tds = tr.getElementsByTagName("td");
                    for (int i = 0; i < tds.size(); i++) {
                        HtmlElement td = tds.get(i);
                        String content = td.asText();
                        if (i == 2 && content.split("\\.").length == 4) {
                            ip = content;
                        }
                        if (i == 3 && !ElongHotelInterfaceUtil.StringIsNull(ip)) {
                            port = Integer.parseInt(content.trim());
                        }
                    }
                    if (!ElongHotelInterfaceUtil.StringIsNull(ip) && port > 0) {
                        ips.add(ip + ":" + port);
                    }
                }
                catch (Exception e) {
                }
            }
        }
        catch (Exception e) {
        }
        return ips;
    }

    private static List<String> Ip_Zdaye_Com() {
        List<String> ips = new ArrayList<String>();
        try {
            String url = "http://ip.zdaye.com/?ip=&port=&adr=&checktime=10%B7%D6%D6%D3%C4%DA&sleep=3%C3%EB%C4%DA&cunhuo=&px=&nport=&nadr=&gb=2&dengji=%B8%DF%C4%E4&fgl=90%D2%D4%C9%CF&login=&daochu=&api=&ct=1000";
            //关闭日志输出
            org.apache.commons.logging.LogFactory.getFactory().setAttribute("org.apache.commons.logging.Log",
                    "org.apache.commons.logging.impl.NoOpLog");
            //实例化WebClient
            WebClient webClient = new WebClient(BrowserVersion.getDefault());
            //设置
            webClient.getOptions().setTimeout(1000 * 60);
            webClient.getOptions().setRedirectEnabled(true);
            webClient.getOptions().setJavaScriptEnabled(true);
            webClient.getCookieManager().setCookiesEnabled(true);
            webClient.setAjaxController(new NicelyResynchronizingAjaxController());

            webClient.getOptions().setCssEnabled(false);
            webClient.getOptions().setActiveXNative(false);
            webClient.getOptions().setAppletEnabled(false);
            webClient.getOptions().setThrowExceptionOnScriptError(false);
            webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
            //请求头
            webClient.addRequestHeader("Accept",
                    "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
            webClient.addRequestHeader("Accept-Encoding", "gzip,deflate,sdch");
            webClient.addRequestHeader("Accept-Language", "zh-CN,zh;q=0.8");
            webClient.addRequestHeader("Cache-Control", "max-age=0");
            webClient.addRequestHeader("Connection", "keep-alive");
            webClient.addRequestHeader("Referer", "http://ip.zdaye.com/");
            String UserAgent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.122 Safari/537.36";
            webClient.addRequestHeader("User-Agent", UserAgent);
            //请求
            HtmlPage page = webClient.getPage(url);
            //等待加载JS
            webClient.waitForBackgroundJavaScript(1000 * 3);
            //解析
            List<DomElement> tables = page.getElementsByTagName("table");
            for (DomElement table : tables) {
                String txt = table.asXml();
                if (txt.contains("ctable") && txt.contains("IP地址") && txt.contains("端口")) {
                    List<HtmlElement> trs = table.getElementsByTagName("tr");
                    for (HtmlElement tr : trs) {
                        try {
                            String ip = "";
                            int port = 0;
                            List<HtmlElement> tds = tr.getElementsByTagName("td");
                            for (int i = 0; i < tds.size(); i++) {
                                HtmlElement td = tds.get(i);
                                String content = td.asText();
                                if (i == 0 && content.split("\\.").length == 4) {
                                    ip = content;
                                }
                                else if (i == 1 && !ElongHotelInterfaceUtil.StringIsNull(ip)) {
                                    port = Integer.parseInt(content.trim());
                                }
                            }
                            if (!ElongHotelInterfaceUtil.StringIsNull(ip) && port > 0) {
                                ips.add(ip + ":" + port);
                            }
                        }
                        catch (Exception e) {
                        }
                    }
                }
            }
        }
        catch (Exception e) {
        }
        return ips;
    }

    private static List<String> Proxy_Com_Ru() {
        List<String> ips = new ArrayList<String>();
        try {
            String url = "http://proxy.com.ru/gaoni/";
            //关闭日志输出
            org.apache.commons.logging.LogFactory.getFactory().setAttribute("org.apache.commons.logging.Log",
                    "org.apache.commons.logging.impl.NoOpLog");
            //实例化WebClient
            WebClient webClient = new WebClient(BrowserVersion.getDefault());
            //设置
            webClient.getOptions().setCssEnabled(false);
            webClient.getOptions().setActiveXNative(false);
            webClient.getOptions().setAppletEnabled(false);
            webClient.getOptions().setRedirectEnabled(false);
            webClient.getOptions().setJavaScriptEnabled(false);
            webClient.getCookieManager().setCookiesEnabled(false);
            webClient.getOptions().setThrowExceptionOnScriptError(false);
            webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
            //请求头
            webClient.addRequestHeader("Accept",
                    "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
            webClient.addRequestHeader("Accept-Encoding", "gzip,deflate,sdch");
            webClient.addRequestHeader("Accept-Language", "zh-CN,zh;q=0.8");
            webClient.addRequestHeader("Cache-Control", "max-age=0");
            webClient.addRequestHeader("Connection", "keep-alive");
            String UserAgent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.122 Safari/537.36";
            webClient.addRequestHeader("User-Agent", UserAgent);
            //请求
            HtmlPage page = webClient.getPage(url);
            //解析
            List<DomElement> tables = page.getElementsByTagName("table");
            for (DomElement table : tables) {
                String txt = table.asXml();
                if (txt.contains("代理服务器IP") && txt.contains("端口") && txt.contains("代理类型") && txt.contains("高度匿名")) {
                    List<HtmlElement> trs = table.getElementsByTagName("tr");
                    for (HtmlElement tr : trs) {
                        try {
                            String ip = "";
                            int port = 0;
                            List<HtmlElement> tds = tr.getElementsByTagName("td");
                            for (int i = 0; i < tds.size(); i++) {
                                HtmlElement td = tds.get(i);
                                String content = td.asText();
                                if (i == 1 && content.split("\\.").length == 4) {
                                    ip = content;
                                }
                                if (i == 2 && !ElongHotelInterfaceUtil.StringIsNull(ip)) {
                                    port = Integer.parseInt(content.trim());
                                }
                            }
                            if (!ElongHotelInterfaceUtil.StringIsNull(ip) && port > 0) {
                                ips.add(ip + ":" + port);
                            }
                        }
                        catch (Exception e) {
                        }
                    }
                }
            }
        }
        catch (Exception e) {
        }
        return ips;
    }

    public static int getFlag() {
        return flag;
    }

    public static void setFlag(int flag) {
        CatchAgentIp.flag = flag;
    }
}