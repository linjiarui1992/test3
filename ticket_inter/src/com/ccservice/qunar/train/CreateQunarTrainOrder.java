package com.ccservice.qunar.train;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.atom.server.Server;
import com.ccservice.b2b2c.base.mailaddress.MailAddress;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainOrderOffline.TrainOrderOffline;
import com.ccservice.b2b2c.base.trainTicketOffline.TrainTicketOffline;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.b2b2c.base.trainpassengerOffline.TrainPassengerOffline;
import com.ccservice.b2b2c.util.BaiDuMapApi;
import com.ccservice.b2b2c.util.DBoperationUtil;

/**
 * @author 作者 guozhengju:
 * @version 创建时间：2015年8月11日 下午3:47:16 类说明
 */
public class CreateQunarTrainOrder {

    private DBoperationUtil dt = new DBoperationUtil();

    public void createTrainOrderOffline(Trainorder trainorder) {
        WriteLog.write("CreateQunarTrainOrder", "CreateQunarTrainOrder");
        trainorderofflineadd(trainorder);
    }

    /**
     * 添加火车票线下订单
     */
    public void trainorderofflineadd(Trainorder trainorder) {
        // 创建火车票线下订单TrainOrderOffline
        TrainOrderOffline trainOrderOffline = new TrainOrderOffline();
        // 有合适的出票点，分配
        //        String addresstemp = "河北省,石家庄市,长安区,就业服务大厦";
        String addresstemp = trainorder.getInsureadreess();
        if (addresstemp.contains("省") && addresstemp.contains("市")) {
            String province = addresstemp.substring(0, addresstemp.indexOf("省"));
            String city = addresstemp.substring(addresstemp.indexOf("省") + 1, addresstemp.indexOf("市"));
            String region = addresstemp.substring(addresstemp.indexOf("市") + 1);
            addresstemp = province + "," + city + "," + region;
        }
        // 通过订单邮寄地址匹配出票点
        BaiDuMapApi bd = new BaiDuMapApi();
        String agentidtemp = bd.getAgentidByInsuraddress(getAllListByAdd(addresstemp, "-1"), addresstemp);
        //        String agentidtemp = "378";
        trainOrderOffline.setAgentId(Long.parseLong(agentidtemp));//ok
        trainOrderOffline.setCreateUId(trainorder.getCreateuid());//ok
        trainOrderOffline.setCreateUser(trainorder.getCreateuser());//ok
        trainOrderOffline.setContactUser(trainorder.getContactuser());//ok
        trainOrderOffline.setContactTel(trainorder.getContacttel());//ok
        trainOrderOffline.setOrderPrice(trainorder.getOrderprice());//ok
        trainOrderOffline.setAgentProfit(trainorder.getAgentprofit());//ok
        trainOrderOffline.setOrderNumber(trainorder.getOrdernumber());//ok
        trainOrderOffline.setTicketCount(trainorder.getTicketcount());//ok
        trainOrderOffline.setPaperType(trainorder.getPaymethod());
        trainOrderOffline.setPaperBackup(trainorder.getSupplypayway());
        trainOrderOffline.setPaperLowSeatCount(trainorder.getRefuseaffirm());
        String sp_TrainOrderOffline_insert = dt.getCreateTrainorderOfficeProcedureSql(trainOrderOffline);
        List list = Server.getInstance().getSystemService().findMapResultByProcedure(sp_TrainOrderOffline_insert);
        Map map = (Map) list.get(0);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String orderid = map.get("id").toString();
        // [TrainOrderOfflineRecord]表插入数据
        String procedureRecord = "sp_TrainOrderOfflineRecord_insert @FKTrainOrderOfflineId=" + orderid
                + ",@ProviderAgentid=" + agentidtemp + ",@DistributionTime='" + sdf.format(new Date())
                + "',@ResponseTime='',@DealResult=0,@RefundReason=0";
        Server.getInstance().getSystemService().findMapResultByProcedure(procedureRecord);
        // 火车票线下订单邮寄信息
        MailAddress address = new MailAddress();
        address.setMailName(trainorder.getContactuser());
        address.setMailTel(trainorder.getContacttel());
        address.setPrintState(0);
        // String insureaddress=trainorder.getInsureadreess();
        //        String insureaddress = "郑宇凤^^^河北^^^石家庄市^^^长安区^^^河北省石家庄市长安区裕华东路99号就业服务大厦^^^050000^^^18819411570^^^";
        String insureaddress = addresstemp;
        String[] splitadd = insureaddress.split(",");
        address.setPostcode("100000");
        address.setAddress(trainorder.getInsureadreess());
        address.setProvinceName(splitadd[1]);
        address.setCityName(splitadd[2]);
        address.setRegionName(splitadd[3]);
        address.setTownName("桥东区政府");
        address.setOrderid(Integer.parseInt(orderid));
        address.setCreatetime(trainorder.getCreatetime());
        String sp_MailAddress_insert = dt.getMailAddressProcedureSql(address);
        Server.getInstance().getSystemService().findMapResultByProcedure(sp_MailAddress_insert);

        for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
            // 火车票乘客线下TrainPassengerOffline
            TrainPassengerOffline trainPassengerOffline = new TrainPassengerOffline();
            trainPassengerOffline.setorderid(Long.parseLong(orderid));
            trainPassengerOffline.setname(trainpassenger.getName());
            trainPassengerOffline.setidtype(trainpassenger.getIdtype());
            trainPassengerOffline.setidnumber(trainpassenger.getIdnumber());
            trainPassengerOffline.setbirthday(trainpassenger.getBirthday());
            String sp_TrainPassengerOffline_insert = dt
                    .getCreateTrainpassengerOfficeProcedureSql(trainPassengerOffline);
            List listP = Server.getInstance().getSystemService()
                    .findMapResultByProcedure(sp_TrainPassengerOffline_insert);
            Map map2 = (Map) listP.get(0);
            String trainPid = map2.get("id").toString();
            for (Trainticket ticket : trainpassenger.getTraintickets()) {
                // 线下火车票TrainTicketOffline
                TrainTicketOffline trainTicketOffline = new TrainTicketOffline();
                trainTicketOffline.setTrainPid(Long.parseLong(trainPid));
                trainTicketOffline.setOrderid(Long.parseLong(orderid));
                trainTicketOffline.setDepartTime(Timestamp.valueOf(ticket.getDeparttime() + ":00"));
                trainTicketOffline.setDeparture(ticket.getDeparture());
                trainTicketOffline.setArrival(ticket.getArrival());
                trainTicketOffline.setTrainno(ticket.getTrainno());
                trainTicketOffline.setTicketType(ticket.getTickettype());
                trainTicketOffline.setSeatType(ticket.getSeattype());
                trainTicketOffline.setPrice(ticket.getPrice());
                trainTicketOffline.setCostTime(ticket.getCosttime());
                trainTicketOffline.setStartTime(ticket.getDeparttime().substring(11, 16));
                trainTicketOffline.setArrivalTime(ticket.getArrivaltime());
                String sp_TrainTicketOffline_insert = dt.getCreateTrainticketOfficeProcedureSql(trainTicketOffline);
                Server.getInstance().getSystemService().findMapResultByProcedure(sp_TrainTicketOffline_insert);
            }
        }
    }

    public List getAllListByAdd(String address, String orderId) {
        // address = "北京,北京市,昌平区,沙河镇";
        String[] ss = address.split(",");
        String procedure = "sp_findAgentid_byAddress @ProvinceName='" + ss[0] + "',@CityName='" + ss[1]
                + "',@RegionName='" + ss[2] + "',@TownName='" + ss[3] + "',@OrderId='" + orderId + "'";
        List list = Server.getInstance().getSystemService().findMapResultByProcedure(procedure);
        return list;
    }

    public static void main(String[] args) {

    }

}
