package com.ccservice.qunar.train;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.interticket.HttpClient;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.train.qunar.QunarOrderMethod;

public class JobQunarOrderOfflineYglt implements Job{
	
	private long qcyg_agentid;
	
	private String qunarPayurl;

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		 String isqunaracquisition = null;
	        long syscfid = 0L;
	        SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	        WriteLog.write("JobQunarOffline_Test", sdFormat.format(new Date()));
	        String merchantCode=PropertyUtil.getValue("ygltmerchantCode", "train.properties");
	        String HMAC=PropertyUtil.getValue("ygltHMAC", "train.properties");
	        try {
	            String url = HttpClient
	                    .httpget(
	                            "http://api.pub.train.qunar.com/api/pub/QueryOrders.do?&merchantCode="+merchantCode+"&type=WAIT_TICKET&HMAC="+HMAC,
	                            "UTF-8");
	            WriteLog.write("JobQunarOrderOffline_json拉单json_yglt", "url:"+"http://api.pub.train.qunar.com/api/pub/QueryOrders.do?&merchantCode="+merchantCode+"&type=WAIT_TICKET&HMAC="+HMAC+"--->josn:" + url);
	            Classification(JSONObject.parseObject(url));
	        }
	        catch (Exception e) {
	            e.printStackTrace();
	        }
	}
	
	public void Classification(JSONObject jsonObject) {
        if (jsonObject.containsKey("data")) {
            String data1 = jsonObject.getString("data");
            JSONArray data = JSONArray.parseArray(data1);
            WriteLog.write("JobQunarOrderOffline", jsonObject.toString());
            String orderNos = allOrderNos(data);
            WriteLog.write("JobQunarOrderOffline", "orderNos:" + orderNos);
            if (!"".equals(orderNos.trim())) {
                List<QunarOrderMethod> listqom = allOldOrders(orderNos);
                for (int i = 0; i < data.size(); i++) {
                    JSONObject info = data.getJSONObject(i);
                    String orderNo = info.getString("orderNo");
                    boolean isoldorder = false;
                    for (int j = 0; j < listqom.size(); j++) {
                        QunarOrderMethod qunarordermethod = (QunarOrderMethod) listqom.get(j);
                        if ((orderNo != null) && (orderNo.equals(qunarordermethod.getQunarordernumber()))) {
                            qunarordermethod.setOrderjson(info);
                                                        isoldorder = true;
                            break;
//                                                        exThread(qunarordermethod);
                        }
                    }
                    if (!isoldorder) {
                        QunarOrderMethod qunarordermethod = new QunarOrderMethod();
                        qunarordermethod.setOrderjson(info);
                        exThread(qunarordermethod);
                    }
                }
            }
        }
    }
	
	// 设置调度任务及触发器，任务名及触发器名可用来停止任务或修改任务
    public static void startScheduler(String expr) throws Exception {
    	System.out.println("StartygltQunarLadan");
        JobDetail jobDetail = new JobDetail("JobQunarOrderOfflineYglt", "JobQunarOrderOfflineYgltGroup",
        		JobQunarOrderOfflineYglt.class);// 任务名，任务组名，任务执行类
        CronTrigger trigger = new CronTrigger("JobQunarOrderOfflineYglt","JobQunarOrderOfflineYgltGroup",
                expr);// 触发器名，触发器组名
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
        scheduler.scheduleJob(jobDetail, trigger);
        scheduler.start();
    }
    
	public void exThread(QunarOrderMethod qunarordermethod) {
        ExecutorService pool = Executors.newFixedThreadPool(1);

        Thread t1 = null;

        t1 = new MyThreadQunarOrderOfflineYglt(qunarordermethod, this.qcyg_agentid, this.qunarPayurl);
        pool.execute(t1);

        pool.shutdown();
//        WriteLog.write("JobQunarOrderOffline", "qunar订单号:" + qunarordermethod.getQunarordernumber() + "线程关闭");
    }
	
	public String allOrderNos(JSONArray data) {
        String orderNos = "";
        for (int a = 0; a < data.size(); a++) {
            JSONObject info = data.getJSONObject(a);
            String orderNo = info.getString("orderNo");
            orderNos = orderNos + "'" + orderNo + "'";
            if (a < data.size() - 1) {
                orderNos = orderNos + ",";
            }
        }
        return orderNos;
    }
	public List<QunarOrderMethod> allOldOrders(String orderNos) {
        List<QunarOrderMethod> listqom = new ArrayList();
        String sql_trainorder = "SELECT OrderNumberOnline,Id FROM TrainOrderOffline  WHERE OrderNumberOnline in (" +

        orderNos + ")";
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql_trainorder, null);
        for (int i = 0; i < list.size(); i++) {
            QunarOrderMethod qunarordermethod = new QunarOrderMethod();
            Map map = (Map) list.get(i);
            qunarordermethod.setQunarordernumber(map.get("OrderNumberOnline").toString());
            qunarordermethod.setId(Long.valueOf(map.get("Id").toString()).longValue());
            listqom.add(qunarordermethod);
        }
        return listqom;
    }
	public static void main(String[] args) {
		String url = "{\"data\":[{\"arrStation\":\"徐州\",\"dptStation\":\"长春\",\"extSeat\":[{\"5\":337},{\"6\":371}],\"isPaper\":1,\"orderDate\":\"2015-11-29 19:38:21\",\"orderNo\":\"qcltw1511291938215bf\",\"orderType\":0,\"paperBackup\":0,\"paperLowSeatCount\":0,\"paperType\":0,\"passengers\":[{\"certNo\":\"320305196603040045\",\"certType\":\"1\",\"name\":\"裴君\",\"ticketType\":\"1\"}],\"seat\":{\"7\":388},\"ticketPay\":388,\"trainEndTime\":\"2015-12-06 15:12\",\"trainNo\":\"K551/K554\",\"trainStartTime\":\"2015-12-05 17:45\",\"transportAddress\":\"江苏省徐州市泉山区刘场小区26号楼4单元101\",\"transportName\":\"裴君\",\"transportPhone\":\"13952188506\"},{\"arrStation\":\"郑州\",\"dptStation\":\"无锡\",\"extSeat\":[{\"5\":200},{\"6\":206}],\"isPaper\":1,\"orderDate\":\"2015-11-29 20:04:13\",\"orderNo\":\"qcltw1511292004137d5\",\"orderType\":0,\"paperBackup\":0,\"paperLowSeatCount\":0,\"paperType\":0,\"passengers\":[{\"certNo\":\"410704196405250524\",\"certType\":\"1\",\"name\":\"李玉霞\",\"ticketType\":\"1\"},{\"certNo\":\"410902196303014051\",\"certType\":\"1\",\"name\":\"李志郑\",\"ticketType\":\"1\"}],\"seat\":{\"7\":214},\"ticketPay\":428,\"trainEndTime\":\"2015-12-04 07:40\",\"trainNo\":\"K152/K153\",\"trainStartTime\":\"2015-12-03 18:25\",\"transportAddress\":\"江苏省无锡市南长区南湖大道855号扬名科技创业中心2001室华夏幸福\",\"transportName\":\"李川阁\",\"transportPhone\":\"15152255050\"}],\"ret\":true,\"total\":2}";
		JobQunarOrderOfflineYglt jobQunarOrderOfflineYglt = new JobQunarOrderOfflineYglt();
		jobQunarOrderOfflineYglt.Classification(JSONObject.parseObject(url));
	}
}
