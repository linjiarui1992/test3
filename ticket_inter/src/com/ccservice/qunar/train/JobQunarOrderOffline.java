package com.ccservice.qunar.train;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.interticket.HttpClient;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.JobQunarOrder;
import com.ccservice.inter.server.Server;
import com.ccservice.train.qunar.QunarOrderMethod;
import com.tenpay.util.MD5Util;

public class JobQunarOrderOffline implements Job {

    static Log log = LogFactory.getLog(JobQunarOrder.class);

    private final String order_url = "http://api.pub.train.qunar.com/api/pub/QueryOrders.do?&merchantCode=qcltw&type=WAIT_TICKET&HMAC=6E4AC943A5F93A485C2F6A9E136A1FFF";

    static String arrStationValue;

    static String dptStationValue;

    static String extSeatValue;

    static String extSeat_Key;

    static String seatValue_Key = null;

    static String orderDateValue;

    static String orderNoValue;

    static String certNoValue;

    static String certTypeValue = "";

    static String nameValue = null;

    static String seatValue;

    static String arrStationValue1;

    static String dptStationValue1;

    static String extSeatValue1;

    static String trainEndTimeValue;

    static String trainNoValue;

    static String trainStartTimeValue;

    static String arrStationValue_lc;

    static String dptStationValue_lc;

    static String extSeatValue_lc;

    static String seatValue_lc;

    static String trainEndTimeValue_lc;

    static String trainNoValue_lc;

    static String trainStartTimeValue_lc;

    static String birthday;

    static float ticketPayValue;

    static float seatValue_Value = 0.0F;

    static float extSeat_Value;

    static float ticketPayValue_lc = 0.0F;

    static int orderTypeValue;

    static int ticketTypeValue;

    static int seqValue;

    static int isjointtrip = 0;

    private long qcyg_agentid;

    private String qunarPayurl;

    WriteLog writeLog = new WriteLog();

    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        //    	System.out.println("executeexecute");
        //    	WriteLog.write("JobQunarOrderOffline_json拉单json", "josn:" + "executeexecute");
        String isqunaracquisition = null;
        long syscfid = 0L;
        SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        WriteLog.write("JobQunarOffline_Test", sdFormat.format(new Date()));
        String merchantCode = PropertyUtil.getValue("qcltmerchantCode", "train.properties");
        String HMAC = PropertyUtil.getValue("qcltHMAC", "train.properties");
        try {
            String url = HttpClient.httpget("http://api.pub.train.qunar.com/api/pub/QueryOrders.do?&merchantCode="
                    + merchantCode + "&type=WAIT_TICKET&HMAC=" + HMAC, "UTF-8");
            //        try {
            //            String url = HttpClient
            //                    .httpget(
            //                            "http://api.pub.train.qunar.com/api/pub/QueryOrders.do?&merchantCode=qcltw&type=WAIT_TICKET&HMAC=6E4AC943A5F93A485C2F6A9E136A1FFF",
            //                            "UTF-8");
            WriteLog.write("JobQunarOrderOffline_json拉单json_qclt", "josn:" + url);
            Classification(JSONObject.parseObject(url));
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void Classification(JSONObject jsonObject) {
        //        getCommon();
        if (jsonObject.containsKey("data")) {
            String data1 = jsonObject.getString("data");
            JSONArray data = JSONArray.parseArray(data1);
            WriteLog.write("JobQunarOrderOffline", jsonObject.toString());
            String orderNos = allOrderNos(data);
            WriteLog.write("JobQunarOrderOffline", "orderNos:" + orderNos);
            if (!"".equals(orderNos.trim())) {
                List<QunarOrderMethod> listqom = allOldOrders(orderNos);
                for (int i = 0; i < data.size(); i++) {
                    JSONObject info = data.getJSONObject(i);
                    String orderNo = info.getString("orderNo");
                    boolean isoldorder = false;
                    for (int j = 0; j < listqom.size(); j++) {
                        QunarOrderMethod qunarordermethod = (QunarOrderMethod) listqom.get(j);
                        if ((orderNo != null) && (orderNo.equals(qunarordermethod.getQunarordernumber()))) {
                            qunarordermethod.setOrderjson(info);
                            //                                                        isoldorder = true;
                            break;
                            //                                                        exThread(qunarordermethod);
                        }
                    }
                    if (!isoldorder) {
                        QunarOrderMethod qunarordermethod = new QunarOrderMethod();
                        //                        qunarordermethod.setQunarordernumber(orderNo);
                        //                        qunarordermethod.setId(0L);
                        //                        qunarordermethod.setOrderstatus(2);
                        //                        qunarordermethod.setCreatetime("");
                        //                        qunarordermethod.setIsquestionorder(0);
                        //                        qunarordermethod.setState12306(1);
                        qunarordermethod.setOrderjson(info);
                        //                        qunarordermethod.setInterfaceType(2);
                        exThread(qunarordermethod);
                    }
                }
            }
        }
    }

    // 设置调度任务及触发器，任务名及触发器名可用来停止任务或修改任务
    public static void startScheduler(String expr) throws Exception {
        System.out.println("StartqcltQunarLadan");
        JobDetail jobDetail = new JobDetail("JobQunarOrderOffline", "JobQunarOrderOfflineGroup",
                JobQunarOrderOffline.class);// 任务名，任务组名，任务执行类
        CronTrigger trigger = new CronTrigger("JobQunarOrderOffline", "JobQunarOrderOfflineGroup", expr);// 触发器名，触发器组名
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
        scheduler.scheduleJob(jobDetail, trigger);
        scheduler.start();
    }

    public static void main1(String[] args) {
        String str_toMD5 = "27316DD7742B441CB705D210FCA8FC0AxcslwWAIT_TICKET";
        String str_MD5 = MD5Util.MD5Encode(str_toMD5, "UTF-8").toUpperCase();
        System.out.println(str_MD5);
    }

    public void exThread(QunarOrderMethod qunarordermethod) {
        ExecutorService pool = Executors.newFixedThreadPool(1);

        Thread t1 = null;
        //        WriteLog.write(
        //                "JobQunarOrder",
        //                "qunar订单号:"
        //                        + qunarordermethod.getQunarordernumber()
        //                        + "===当前订单ID:"
        //                        + (qunarordermethod.getId() == 0L ? "===不存在" : new StringBuilder(String
        //                                .valueOf(qunarordermethod.getId())).append("===当前订单状态:")
        //                                .append(qunarordermethod.getOrderstatus()).append("===当前订单问题订单状态:")
        //                                .append(qunarordermethod.getIsquestionorder()).append("===当前订单12306状态:")
        //                                .append(qunarordermethod.getState12306()).append("===当前订单创建时间")
        //                                .append(qunarordermethod.getCreatetime()).toString()));

        t1 = new MyThreadQunarOrderOffline(qunarordermethod, this.qcyg_agentid, this.qunarPayurl);
        pool.execute(t1);

        pool.shutdown();
        //        WriteLog.write("JobQunarOrderOffline", "qunar订单号:" + qunarordermethod.getQunarordernumber() + "线程关闭");
    }

    public String allOrderNos(JSONArray data) {
        String orderNos = "";
        for (int a = 0; a < data.size(); a++) {
            JSONObject info = data.getJSONObject(a);
            String orderNo = info.getString("orderNo");
            orderNos = orderNos + "'" + orderNo + "'";
            if (a < data.size() - 1) {
                orderNos = orderNos + ",";
            }
        }
        return orderNos;
    }

    public List<QunarOrderMethod> allOldOrders(String orderNos) {
        List<QunarOrderMethod> listqom = new ArrayList();
        String sql_trainorder = "SELECT OrderNumberOnline,Id FROM TrainOrderOffline  WHERE OrderNumberOnline in (" +

        orderNos + ")";
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql_trainorder, null);
        //        WriteLog.write("JobQunarOrder", "sql_trainorder:" + sql_trainorder);
        //        WriteLog.write("JobQunarOrder", "list.size:" + list.size());
        for (int i = 0; i < list.size(); i++) {
            QunarOrderMethod qunarordermethod = new QunarOrderMethod();
            Map map = (Map) list.get(i);
            qunarordermethod.setQunarordernumber(map.get("OrderNumberOnline").toString());
            qunarordermethod.setId(Long.valueOf(map.get("Id").toString()).longValue());
            //            qunarordermethod.setOrderstatus(Integer.valueOf(map.get("C_ORDERSTATUS").toString()).intValue());
            //            qunarordermethod.setCreatetime(map.get("C_CREATETIME").toString());
            //            qunarordermethod.setIsquestionorder(Integer.valueOf(map.get("C_ISQUESTIONORDER").toString()).intValue());
            //            try {
            //                qunarordermethod.setState12306(Integer.valueOf(map.get("C_STATE12306").toString()).intValue());
            //            }
            //            catch (NumberFormatException e) {
            //                qunarordermethod.setState12306(0);
            //            }
            //            qunarordermethod.setOrderjson(new JSONObject());
            listqom.add(qunarordermethod);
        }
        return listqom;
    }

    public static void main(String[] args) {
        String url = "{\"data\":[{\"arrStation\":\"河唇\",\"dptStation\":\"昆明\",\"extSeat\":[{\"5\":261.5},{\"6\":270.5}],\"extSeatMap\":[{\"硬卧上\":261.5},{\"硬卧中\":270.5}],\"isPaper\":1,\"orderDate\":\"2016-08-10 18:08:43\",\"orderNo\":\"yglcw160810180843f02\",\"orderType\":0,\"paperBackup\":0,\"paperLowSeatCount\":0,\"paperType\":11,\"passengers\":[{\"certNo\":\"440822197404115919\",\"certType\":\"1\",\"name\":\"黄廉统\",\"ticketType\":\"1\"}],\"seat\":{\"7\":279.5},\"seatMap\":{\"硬卧下\":279.5},\"ticketPay\":279.5,\"trainEndTime\":\"2016-08-30 10:19\",\"trainNo\":\"K232\",\"trainStartTime\":\"2016-08-29 16:15\",\"transportAddress\":\"云南省昆明市五华区威远街金鹰B座五楼星期五美发沙龙\",\"transportName\":\"黄廉统\",\"transportPhone\":\"13888260138\"},{\"arrStation\":\"昆明\",\"dptStation\":\"大理\",\"extSeat\":[],\"extSeatMap\":[],\"isPaper\":1,\"orderDate\":\"2016-08-10 18:39:18\",\"orderNo\":\"yglcw16081018391874a\",\"orderType\":0,\"paperBackup\":0,\"paperLowSeatCount\":0,\"paperType\":0,\"passengers\":[{\"certNo\":\"150121198601017227\",\"certType\":\"1\",\"name\":\"王秀利\",\"ticketType\":\"1\"},{\"certNo\":\"150124198604190112\",\"certType\":\"1\",\"name\":\"周文杰\",\"ticketType\":\"1\"},{\"certNo\":\"150121198601017227\",\"certType\":\"1\",\"name\":\"王秀利\",\"ticketType\":\"0\"},{\"certNo\":\"150121198601017227\",\"certType\":\"1\",\"name\":\"王秀利\",\"ticketType\":\"0\"}],\"seat\":{\"1\":64},\"seatMap\":{\"硬座\":64},\"ticketPay\":256,\"trainEndTime\":\"2016-08-13 19:20\",\"trainNo\":\"K6144\",\"trainStartTime\":\"2016-08-13 12:50\",\"transportAddress\":\"云南省大理白族自治州大理市大理江湖驿栈全海景精品主题客栈\",\"transportName\":\"王秀利\",\"transportPhone\":\"13674819333\"}],\"ret\":true,\"total\":2}";
        //String url = "{\"data\":[{\"arrStation\":\"乌鲁木齐南\",\"dptStation\":\"昆明\",\"extSeat\":[],\"extSeatMap\":[],\"isPaper\":1,\"orderDate\":\"2016-08-16 19:08:48\",\"orderNo\":\"yglcw16081619084857a\",\"orderType\":0,\"paperBackup\":0,\"paperLowSeatCount\":1,\"paperType\":3,\"passengers\":[{\"certNo\":\"650103199502030610\",\"certType\":\"1\",\"name\":\"盛祥宇\",\"ticketType\":\"1\"}],\"seat\":{\"1\":391.5},\"seatMap\":{\"硬座\":391.5},\"ticketPay\":391.5,\"trainEndTime\":\"2016-08-23 10:56\",\"trainNo\":\"K1502\",\"trainStartTime\":\"2016-08-20 18:55\",\"transportAddress\":\"云南省昆明市官渡区官渡区新亚洲体育城五环锋尚G6栋\",\"transportName\":\"盛祥宇\",\"transportPhone\":\"13608803141\"},{\"arrStation\":\"蒙自北\",\"dptStation\":\"昆明\",\"extSeat\":[],\"extSeatMap\":[],\"isPaper\":1,\"orderDate\":\"2016-08-16 18:51:40\",\"orderNo\":\"yglcw160816185140383\",\"orderType\":0,\"paperBackup\":1,\"paperLowSeatCount\":0,\"paperType\":4,\"passengers\":[{\"certNo\":\"532231197712091717\",\"certType\":\"1\",\"name\":\"朱文高\",\"ticketType\":\"1\"},{\"certNo\":\"530129199604051718\",\"certType\":\"1\",\"name\":\"朱学委\",\"ticketType\":\"1\"},{\"certNo\":\"532231197610201727\",\"certType\":\"1\",\"name\":\"马小英\",\"ticketType\":\"1\"}],\"seat\":{\"1\":49.5},\"seatMap\":{\"硬座\":49.5},\"ticketPay\":148.5,\"trainEndTime\":\"2016-08-23 12:00\",\"trainNo\":\"K9812\",\"trainStartTime\":\"2016-08-23 07:50\",\"transportAddress\":\"云南省昆明市官渡区城区民航路400号城投大厦水汇国际\",\"transportName\":\"左绍湘\",\"transportPhone\":\"15288346127\"},{\"arrStation\":\"关林\",\"dptStation\":\"昆明\",\"extSeat\":[{\"5\":408},{\"6\":449}],\"extSeatMap\":[{\"硬卧上\":408},{\"硬卧中\":449}],\"isPaper\":1,\"orderDate\":\"2016-08-16 19:02:04\",\"orderNo\":\"yglcw1608161902044a7\",\"orderType\":0,\"paperBackup\":0,\"paperLowSeatCount\":1,\"paperType\":2,\"passengers\":[{\"certNo\":\"410381196607136587\",\"certType\":\"1\",\"name\":\"武菊芳\",\"ticketType\":\"1\"}],\"seat\":{\"7\":436},\"seatMap\":{\"硬卧下\":436},\"ticketPay\":449,\"trainEndTime\":\"2016-09-01 03:11\",\"trainNo\":\"K2286\",\"trainStartTime\":\"2016-08-30 18:18\",\"transportAddress\":\"云南省昆明市盘龙区溪畔丽景8-1808\",\"transportName\":\"袁腾飞\",\"transportPhone\":\"13888675665\"}],\"ret\":true,\"total\":3}";
        //String url="{\"data\":[{\"arrStation\":\"长春\",\"dptStation\":\"开封\",\"extSeat\":[{\"5\":353,\"硬卧上\":353},{\"6\":389,\"硬卧中\":389}],\"extSeatMap\":[{},{}],\"isPaper\":1,\"orderDate\":\"2016-06-02 13:45:08\",\"orderNo\":\"qcjzs1606021345085cd\",\"orderType\":0,\"paperBackup\":0,\"paperLowSeatCount\":1,\"paperType\":2,\"passengers\":[{\"certNo\":\"220724199102241456\",\"certType\":\"1\",\"name\":\"高航\",\"ticketType\":\"1\"}],\"seat\":{\"7\":406},\"seatMap\":{\"硬卧下\":406},\"ticketPay\":406,\"trainEndTime\":\"2016-06-06 15:07\",\"trainNo\":\"K926\",\"trainStartTime\":\"2016-06-05 16:20\",\"transportAddress\":\"河南省开封市通许县大岗李乡田寨村\",\"transportName\":\"高航\",\"transportPhone\":\"13364388454\"},{\"arrStation\":\"青岛北\",\"dptStation\":\"商丘\",\"extSeat\":[{\"5\":194,\"硬卧上\":194},{\"6\":200,\"硬卧中\":200}],\"extSeatMap\":[{},{}],\"isPaper\":1,\"orderDate\":\"2016-06-02 13:19:26\",\"orderNo\":\"qcjzs160602131926267\",\"orderType\":0,\"paperBackup\":0,\"paperLowSeatCount\":2,\"paperType\":2,\"passengers\":[{\"certNo\":\"411402199610104011\",\"certType\":\"1\",\"name\":\"刘星辰\",\"ticketType\":\"1\"},{\"certNo\":\"411403199706227522\",\"certType\":\"1\",\"name\":\"周婕敏\",\"ticketType\":\"1\"}],\"seat\":{\"7\":208},\"seatMap\":{\"硬卧下\":208},\"ticketPay\":416,\"trainEndTime\":\"2016-06-10 11:42\",\"trainNo\":\"K1636\",\"trainStartTime\":\"2016-06-09 23:59\",\"transportAddress\":\"河南省商丘市睢阳区睢阳大道六中分校宏源小区\",\"transportName\":\"刘星辰\",\"transportPhone\":\"18736753962\"}],\"ret\":true,\"total\":2}";
        //String url="{\"data\":[{\"arrStation\":\"天津南\",\"dptStation\":\"北京南\",\"extSeat\":[],\"extSeatMap\":[],\"isPaper\":1,\"orderDate\":\"2016-06-30 11:17:00\",\"orderNo\":\"xcslw16063011170015f\",\"orderType\":0,\"paperBackup\":0,\"paperLowSeatCount\":0,\"paperType\":3,\"passengers\":[{\"certNo\":\"370724198811211442\",\"certType\":\"1\",\"name\":\"错过后悔\",\"ticketType\":\"1\"},{\"certNo\":\"42070019800209792X\",\"certType\":\"1\",\"name\":\"彭娇芳\",\"ticketType\":\"1\"}],\"seat\":{\"4\":54.5},\"seatMap\":{\"二等座\":54.5},\"ticketPay\":109,\"ticketToStationNo\":\"9496\",\"trainEndTime\":\"2016-07-06 19:18\",\"trainNo\":\"G269\",\"trainStartTime\":\"2016-07-06 18:35\",\"transportAddress\":\"四川省成都市金牛区成都站广场售票大厅背面西南铁旅门市1楼（长城宾馆斜对面20米）\",\"transportName\":\"发广告\",\"transportPhone\":\"13301108360\"},{\"arrStation\":\"北京\",\"dptStation\":\"天津\",\"extSeat\":[],\"extSeatMap\":[],\"isPaper\":1,\"orderDate\":\"2016-07-01 18:38:21\",\"orderNo\":\"xcslw16070118382116f\",\"orderType\":0,\"paperBackup\":0,\"paperLowSeatCount\":0,\"paperType\":0,\"passengers\":[{\"certNo\":\"370724198811211442\",\"certType\":\"1\",\"name\":\"错过后悔\",\"ticketType\":\"1\"}],\"seat\":{\"1\":21.5},\"seatMap\":{\"硬座\":21.5},\"ticketPay\":21.5,\"trainEndTime\":\"2016-07-07 08:50\",\"trainNo\":\"K4730\",\"trainStartTime\":\"2016-07-07 05:13\",\"transportAddress\":\"新疆维吾尔自治区阿拉尔市其它区但是分到发地方\",\"transportName\":\"第三方\",\"transportPhone\":\"18612137499\"},{\"arrStation\":\"北京南\",\"dptStation\":\"天津\",\"extSeat\":[],\"extSeatMap\":[],\"isPaper\":1,\"orderDate\":\"2016-07-05 11:55:06\",\"orderNo\":\"xcslw160705115506179\",\"orderType\":0,\"paperBackup\":0,\"paperLowSeatCount\":0,\"paperType\":0,\"passengers\":[{\"certNo\":\"452223199008107016\",\"certType\":\"1\",\"name\":\"蔡小强\",\"ticketType\":\"1\"}],\"seat\":{\"4\":54.5},\"seatMap\":{\"二等座\":54.5},\"ticketPay\":54.5,\"trainEndTime\":\"2016-07-07 06:53\",\"trainNo\":\"C2002\",\"trainStartTime\":\"2016-07-07 06:18\",\"transportAddress\":\"安徽省蚌埠市蚌山区大方\",\"transportName\":\"蔡小强\",\"transportPhone\":\"18629539185\"},{\"arrStation\":\"北京南\",\"dptStation\":\"天津\",\"extSeat\":[],\"extSeatMap\":[],\"isPaper\":1,\"orderDate\":\"2016-07-05 16:33:47\",\"orderNo\":\"xcslw16070516334717b\",\"orderType\":0,\"paperBackup\":0,\"paperLowSeatCount\":0,\"paperType\":0,\"passengers\":[{\"certNo\":\"452223199008107016\",\"certType\":\"1\",\"name\":\"蔡小强\",\"ticketType\":\"1\"}],\"seat\":{\"4\":54.5},\"seatMap\":{\"二等座\":54.5},\"ticketPay\":54.5,\"trainEndTime\":\"2016-07-07 06:53\",\"trainNo\":\"C2002\",\"trainStartTime\":\"2016-07-07 06:18\",\"transportAddress\":\"重庆重庆市北碚区防守打法毒贩夫妇萨顶顶顺丰大发发发生地方多少分法萨芬地方都是\",\"transportName\":\"蔡小强\",\"transportPhone\":\"18629539185\"},{\"arrStation\":\"天津\",\"dptStation\":\"北京南\",\"extSeat\":[],\"extSeatMap\":[],\"isPaper\":1,\"orderDate\":\"2016-07-04 15:02:20\",\"orderNo\":\"xcslw160704150220172\",\"orderType\":0,\"paperBackup\":0,\"paperLowSeatCount\":0,\"paperType\":0,\"passengers\":[{\"certNo\":\"370724198811211442\",\"certType\":\"1\",\"name\":\"错过后悔\",\"ticketType\":\"1\"}],\"seat\":{\"4\":54.5},\"seatMap\":{\"二等座\":54.5},\"ticketPay\":54.5,\"trainEndTime\":\"2016-07-08 06:49\",\"trainNo\":\"C2003\",\"trainStartTime\":\"2016-07-08 06:14\",\"transportAddress\":\"内蒙古自治区赤峰市巴林右旗沙发范德萨发\",\"transportName\":\"第三方\",\"transportPhone\":\"18612137499\"},{\"arrStation\":\"天津南\",\"dptStation\":\"上海虹桥\",\"extSeat\":[],\"extSeatMap\":[],\"isPaper\":1,\"orderDate\":\"2016-07-08 16:30:43\",\"orderNo\":\"xcslw160708163043002\",\"orderType\":0,\"paperBackup\":0,\"paperLowSeatCount\":0,\"paperType\":3,\"passengers\":[{\"certNo\":\"370724198811211442\",\"certType\":\"1\",\"name\":\"电饭锅回家\",\"ticketType\":\"1\"}],\"seat\":{\"4\":508.5},\"seatMap\":{\"二等座\":508.5},\"ticketPay\":508.5,\"ticketToStationNo\":\"9522\",\"trainEndTime\":\"2016-07-13 15:20\",\"trainNo\":\"G120\",\"trainStartTime\":\"2016-07-13 10:05\",\"transportAddress\":\"上海上海市闵行区上海虹桥火车站B1层东1出入口，上铁国旅商旅服务中心（近虹桥机场2号航站楼）\",\"transportName\":\"法国红酒交警\",\"transportPhone\":\"13301108360\"}],\"ret\":true,\"total\":6}";
        //String url="{\"data\":[{\"arrStation\":\"广州南\",\"dptStation\":\"上海虹桥\",\"extSeat\":[{\"unknown\":700}],\"extSeatMap\":[{\"动卧上\":700}],\"isPaper\":1,\"orderDate\":\"2016-07-08 16:57:48\",\"orderNo\":\"xcslw160708165748004\",\"orderType\":0,\"paperBackup\":0,\"paperLowSeatCount\":0,\"paperType\":2,\"passengers\":[{\"certNo\":\"370724198811211442\",\"certType\":\"1\",\"name\":\"电饭锅回家\",\"ticketType\":\"1\"}],\"seat\":{\"unknown\":805},\"seatMap\":{\"动卧下\":805},\"ticketPay\":805,\"ticketToStationNo\":\"9522\",\"trainEndTime\":\"2016-07-12 07:25\",\"trainNo\":\"D935\",\"trainStartTime\":\"2016-07-11 20:05\",\"transportAddress\":\"上海上海市闵行区上海虹桥火车站B1层东1出入口，上铁国旅商旅服务中心（近虹桥机场2号航站楼）\",\"transportName\":\"大风刮过\",\"transportPhone\":\"13301108360\"}],\"ret\":true,\"total\":1}";
        //String url="{\"data\":[{\"arrStation\":\"广州南\",\"dptStation\":\"上海虹桥\",\"extSeat\":[{\"unknown\":700}],\"extSeatMap\":[{\"动卧上\":700}],\"isPaper\":1,\"orderDate\":\"2016-07-08 17:23:54\",\"orderNo\":\"xcslw160708172354007\",\"orderType\":0,\"paperBackup\":0,\"paperLowSeatCount\":0,\"paperType\":2,\"passengers\":[{\"certNo\":\"370724198811211442\",\"certType\":\"1\",\"name\":\"电饭锅回家\",\"ticketType\":\"1\"}],\"seat\":{\"unknown\":805},\"seatMap\":{\"动卧下\":805},\"ticketPay\":805,\"ticketToStationNo\":\"9522\",\"trainEndTime\":\"2016-07-12 07:19\",\"trainNo\":\"D931\",\"trainStartTime\":\"2016-07-11 20:00\",\"transportAddress\":\"上海上海市闵行区上海虹桥火车站B1层东1出入口，上铁国旅商旅服务中心（近虹桥机场2号航站楼）\",\"transportName\":\"富贵花还\",\"transportPhone\":\"13301108360\"}],\"ret\":true,\"total\":1}";
        JobQunarOrderOffline jobQunarOrderOffline = new JobQunarOrderOffline();
        jobQunarOrderOffline.Classification(JSONObject.parseObject(url));
        //        String param = "jsonStr=" + url;
        //        url = "http://localhost:9201/ticket_inter/QunarOrderByHandServlet";
        //        String s = SendPostandGet.submitPost(url, param, "utf-8").toString();
        //        System.out.println(s);

    }
}
