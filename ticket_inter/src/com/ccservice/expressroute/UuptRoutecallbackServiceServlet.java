package com.ccservice.expressroute;

import java.io.IOException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.ccervice.util.db.DBHelperOffline;
import com.ccservice.huamin.WriteLog;

public class UuptRoutecallbackServiceServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String result = "";
		String data = request.getParameter("data");
		WriteLog.write("UU跑腿__回调路由信息", "------回调信息------" + data);
		JSONObject dataJson = JSONObject.parseObject(data);
		String order_code = dataJson.getString("order_code");//快递单号
		String driver_name = dataJson.getString("driver_name");//跑男姓名(跑男接单后)
		String driver_jobnum = dataJson.getString("driver_jobnum");//跑男工号(跑男接单后)
		String driver_mobile = dataJson.getString("driver_mobile");//跑男电话(跑男接单后)
		String state = dataJson.getString("state");//当前状态1下单成功 3跑男抢单 4已到达 5已取件 6到达目的地 10收件人已收货 -1订单取消
		String state_text = dataJson.getString("state_text");//当前状态说明
		String timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(dataJson.getString("timestamp"));//状态更新时间
		String origin_id = dataJson.getString("origin_id");//订单号
		String return_msg = dataJson.getString("return_msg");//返回信息，如非空，为错误原因，如签名失败、参数格式校验错误
		String return_code = dataJson.getString("return_code");//状态，ok/fail表示成功
		
		String sql = "INSERT INTO uupt_route (order_code, driver_name, driver_jobnum, driver_mobile, state, state_text, acceptTime, orderId, return_msg, return_code) VALUES "
		        +"('"+order_code+"', '"+driver_name+"', '"+driver_jobnum+"', '"+driver_mobile+"', "+state+" , '"+state_text+"', '"+timestamp+"', "+origin_id+", '"+return_msg+"', '"+return_code+"')";
		int returnCode = DBHelperOffline.insertSql(sql);
		if (returnCode == 1) {
			result = "success";
		} else {
			result = "failure";
		}
		WriteLog.write("UU跑腿__回调路由信息", "------回调结果------" + result);
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");
        response.getOutputStream().write(result.getBytes(Charset.forName("UTF-8")));
	}
}
