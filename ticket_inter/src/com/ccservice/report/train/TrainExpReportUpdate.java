package com.ccservice.report.train;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.b2b2c.base.customeragent.Customeragent;
import com.ccservice.inter.server.Server;

/**
 * 
 * @author wzc
 * 火车票异常报表更新定时更新
 *
 */
public class TrainExpReportUpdate implements Job {

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        List<Customeragent> agentlist = Server.getInstance().getMemberService()
                .findAllCustomeragent("where id>=46", "order by id asc ", -1, 0);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, -1);
        String begin_time = sdf.format(cal.getTime());
        String end_time = sdf.format(cal.getTime());
        for (int i = 0; i < agentlist.size(); i++) {
            Customeragent agent = agentlist.get(i);
            try {
                new TrainExptionReportJob().tcexprotfilereport(agent.getId() + "", begin_time, end_time);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
