package com.ccservice.report.train;

import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.io.FileUtils;

import com.ccservice.b2b2c.base.customeragent.Customeragent;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.b2b2c.base.sysconfig.Sysconfig;
import com.ccservice.b2b2c.policy.TimeUtil;
import com.ccservice.component.excel.HthyWorkSheet;
import com.ccservice.component.excel.HthyWritableWorkBook;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;

public class TrainReportJob {

    public static void main(String[] args) {
        try {
            new TrainReportJob().tcexprotfilereport("309", "2015-01-01", "2015-06-29");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
    * 同城报表
    * 
    * @return
    * @throws Exception
    */
    public void tcexprotfilereport(String agentid, String begin_time, String end_time) throws Exception {
        String tcagentid = agentid;
        if (Long.parseLong(tcagentid) > 0) {
            String jiekouname = "接口";
            try {
                jiekouname = getagentidshortnamebyAgentid(Long.valueOf(tcagentid));
                if (jiekouname == null || "null".equals(jiekouname)) {
                    jiekouname = "接口";
                }
            }
            catch (Exception e2) {
                e2.printStackTrace();
            }
            SimpleDateFormat formatguishu = new SimpleDateFormat("yyyy/MM/dd");
            SimpleDateFormat formatdb = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            String begin_timetemp = begin_time + " 00:00:00 ";
            String end_timetemp = end_time + " 23:59:59 ";
            ISystemService service = Server.getInstance().getSystemService();
            List listorder = new ArrayList();
            List listtuipiaoorder = new ArrayList();
            List caiwuorder = new ArrayList();
            List tongji = new ArrayList();
            listorder = service.findMapResultByProcedure("sp_TrainTCreport @reporttype=1,@begintime='" + begin_timetemp
                    + "',@endtime ='" + end_timetemp + "',@agentid = " + tcagentid);
            listtuipiaoorder = service.findMapResultByProcedure("sp_TrainTCreport @reporttype=2,@begintime='"
                    + begin_timetemp + "',@endtime ='" + end_timetemp + "',@agentid = " + tcagentid);
            caiwuorder = service.findMapResultByProcedure("sp_TrainTCreport @reporttype=3,@begintime='"
                    + begin_timetemp + "',@endtime ='" + end_timetemp + "',@agentid = " + tcagentid);
            tongji = service.findMapResultByProcedure("sp_TrainTCreport @reporttype=4,@begintime='" + begin_timetemp
                    + "',@endtime ='" + end_timetemp + "',@agentid = " + tcagentid);
            try {
                insertReportData(listorder, listtuipiaoorder, caiwuorder, Long.valueOf(tcagentid));
            }
            catch (Exception e3) {
                e3.printStackTrace();
            }
            String number = System.currentTimeMillis() + new Random().nextInt(10) + "";
            String name = number + "_" + begin_time + "_" + end_time + ".xls";
            List<String> titlelist = new ArrayList<String>();
            titlelist.add("供应商名称");
            titlelist.add(jiekouname + "订单号");
            titlelist.add("供应商订单号");
            titlelist.add("12306订单号");
            titlelist.add("结算金额");
            titlelist.add("结算类型");
            titlelist.add("张数");
            titlelist.add("交易时间");
            titlelist.add("手续费");
            titlelist.add("结算归属日期");
            titlelist.add("余额");
            String[] titleles = (String[]) titlelist.toArray(new String[titlelist.size()]);
            HthyWritableWorkBook book = HthyWritableWorkBook.getInstanceWrite();
            Map<Integer, Integer> column = new HashMap<Integer, Integer>();
            column.put(1, 100);
            column.put(2, 80);
            column.put(3, 80);
            column.put(4, 60);
            column.put(5, 80);
            column.put(6, 100);
            column.put(7, 80);
            column.put(8, 80);
            column.put(9, 80);
            column.put(10, 80);
            column.put(11, 80);
            column.put(12, 100);
            column.put(13, 100);
            int row = 200;
            try {
                row = (listorder.size() + listtuipiaoorder.size() + caiwuorder.size()) * 20;
                if (row <= 0) {
                    row = 200;
                }
            }
            catch (Exception e2) {
                e2.printStackTrace();
            }
            HthyWorkSheet sheet = book.createHthyWorkSheet("report", titleles.length + 10, row, column);
            try {
                long sumoutorder = 0;
                if (listorder != null && listorder.size() > 0) {
                    try {
                        for (int i = 0; i < listorder.size(); i++) {
                            Map map = (Map) listorder.get(i);
                            String ticketcount = parseNullData(map.get("TICKETCOUNT")).toString();//票数
                            sumoutorder += Integer.parseInt(ticketcount);
                        }
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                sheet.createRow();
                sheet.addCell("期初余额");//
                sheet.addCell("期末余额");//
                sheet.addCell("本期支出金额");//
                sheet.addCell("本期收入金额");
                sheet.addCell("出票数");
                sheet.addCell("退票数");
                sheet.addCell("");
                sheet.addCell("");
                sheet.addCell("");
                sheet.addCell("");
                sheet.addCell("");
                sheet.rowOver();
                sheet.createRow();
                Map map = (Map) tongji.get(0);
                sheet.addCell(parseNullData(map.get("qichu")).toString());//
                sheet.addCell(parseNullData(map.get("qimo")).toString());//
                sheet.addCell(parseNullData(map.get("zhichu")).toString());//本期支出金额
                sheet.addCell(parseNullData(map.get("shouru")).toString());//本期收入金额
                sheet.addCell(sumoutorder);
                sheet.addCell(listtuipiaoorder.size());
                sheet.addCell("");
                sheet.addCell("");
                sheet.addCell("");
                sheet.addCell("");
                sheet.addCell("");
                sheet.rowOver();
            }
            catch (Exception e2) {
                e2.printStackTrace();
            }
            sheet.createOneRow(titleles, HthyWorkSheet.CenterBlod);
            try {
                if (listorder != null && listorder.size() > 0) {
                    for (int i = 0; i < listorder.size(); i++) {
                        Map map = (Map) listorder.get(i);
                        String tcordernumber = map.get("C_QUNARORDERNUMBER").toString();////供应商订单号
                        String C_ORDERNUMBER = map.get("C_ORDERNUMBER").toString();////我们订单号
                        String extordernumber = map.get("C_EXTNUMBER").toString();////12306订单号
                        String price = map.get("C_SUPPLYPRICE").toString();//结算金额
                        String ticketcount = map.get("TICKETCOUNT").toString();//票数
                        String C_VMBALANCE = map.get("C_VMBALANCE").toString();//余额
                        String timestr = map.get("C_REBATETIME") == null ? "" : map.get("C_REBATETIME").toString();//交易时间
                        sheet.createRow();
                        sheet.addCell("航天商旅");//供应商订单号
                        sheet.addCell(tcordernumber);//供应商订单号
                        sheet.addCell(C_ORDERNUMBER);//我们订单号
                        sheet.addCell(extordernumber);//12306订单号
                        sheet.addCell("-" + price);//结算金额
                        sheet.addCell("出票");//结算类型
                        sheet.addCell(ticketcount);//票数
                        try {
                            if (isNotNullOrEpt(timestr)) {
                                sheet.addCell(timestr);//交易时间
                            }
                            else {
                                sheet.addCell("");//交易时间
                            }
                            sheet.addCell("0");//手续费
                            if (isNotNullOrEpt(timestr)) {
                                sheet.addCell(formatguishu.format(formatdb.parse(timestr)));//结算归属日期
                            }
                            else {
                                sheet.addCell("");//结算归属日期
                            }
                            sheet.addCell(formatPaymoney(Double.valueOf(C_VMBALANCE)));//期末金额
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }
                        sheet.rowOver();
                    }
                }
            }
            catch (Exception e1) {
                e1.printStackTrace();
            }
            //退票
            try {
                if (listtuipiaoorder != null && listtuipiaoorder.size() > 0) {
                    for (int i = 0; i < listtuipiaoorder.size(); i++) {
                        Map map = (Map) listtuipiaoorder.get(i);
                        String tcordernumber = map.get("C_QUNARORDERNUMBER").toString();////供应商订单号
                        String extordernumber = map.get("C_EXTNUMBER").toString();////12306订单号
                        String C_ORDERNUMBER = map.get("C_ORDERNUMBER").toString();////我们订单号
                        String refundprice = map.get("C_PRICE").toString();//结算金额
                        String isapplyticketype = map.get("C_ISAPPLYTICKET").toString();//结算类型
                        String timestr = map.get("C_CREATETIME") == null ? "" : map.get("C_CREATETIME").toString();//交易时间
                        String shouxufei = map.get("C_PROCEDURE").toString();
                        String C_VMBALANCE = map.get("C_VMBALANCE").toString();//余额
                        sheet.createRow();
                        sheet.addCell("航天商旅");//供应商订单号
                        sheet.addCell(tcordernumber);//供应商订单号
                        sheet.addCell(C_ORDERNUMBER);//我们订单号
                        sheet.addCell(extordernumber);//12306订单号
                        sheet.addCell(refundprice);//结算金额
                        String jiesuantype = "线上退票";
                        if ("1".equals(isapplyticketype)) {
                            jiesuantype = "线上退票";
                        }
                        else if ("2".equals(isapplyticketype)) {
                            jiesuantype = "线下退票";
                        }
                        sheet.addCell(jiesuantype);//结算类型
                        sheet.addCell("-1");//票数
                        try {
                            if (isNotNullOrEpt(timestr)) {
                                sheet.addCell(timestr);//交易时间
                            }
                            else {
                                sheet.addCell("");//交易时间
                            }
                            sheet.addCell(shouxufei);//手续费
                            if (isNotNullOrEpt(timestr)) {
                                sheet.addCell(formatguishu.format(formatdb.parse(timestr)));//结算归属日期
                            }
                            else {
                                sheet.addCell("");//结算归属日期
                            }
                            sheet.addCell(formatPaymoney(Double.valueOf(C_VMBALANCE)));//期末余额
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }
                        sheet.rowOver();
                    }
                }
            }
            catch (Exception e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            //财务充值
            try {
                if (caiwuorder != null && caiwuorder.size() > 0) {
                    for (int i = 0; i < caiwuorder.size(); i++) {
                        Map map = (Map) caiwuorder.get(i);
                        String C_ORDERNUMBER = map.get("C_ORDERNUMBER").toString();////我们订单号
                        String tcordernumber = map.get("C_REFORDERNUM").toString();////供应商订单号
                        String extordernumber = "";////12306订单号
                        String C_REBATEMONEY = map.get("C_REBATEMONEY").toString();//结算金额
                        String timestr = map.get("C_REBATETIME") == null ? "" : map.get("C_REBATETIME").toString();//交易时间
                        String C_VMBALANCE = map.get("C_VMBALANCE").toString();//余额
                        String C_YEWUTYPE = map.get("C_YEWUTYPE").toString();//业务类型
                        sheet.createRow();
                        sheet.addCell("航天商旅");//
                        sheet.addCell(tcordernumber);//同程商订单号
                        sheet.addCell(C_ORDERNUMBER);//我们订单号
                        sheet.addCell(extordernumber);//12306订单号
                        sheet.addCell(C_REBATEMONEY);//结算金额
                        String jiesuantype = "财务充值";
                        if ("0".equals(C_YEWUTYPE)) {
                            jiesuantype = "财务充值";
                        }
                        else if ("32".equals(C_YEWUTYPE)) {
                            jiesuantype = "改签退款";
                        }
                        else if ("33".equals(C_YEWUTYPE)) {
                            jiesuantype = "改签扣款";
                        }
                        else if ("34".equals(C_YEWUTYPE)) {
                            jiesuantype = "线下改签";
                        }
                        sheet.addCell(jiesuantype);//结算类型
                        sheet.addCell("0");//票数
                        try {
                            if (isNotNullOrEpt(timestr)) {
                                sheet.addCell(timestr);//交易时间
                            }
                            else {
                                sheet.addCell("");//交易时间
                            }
                            sheet.addCell("0");//手续费
                            if (isNotNullOrEpt(timestr)) {
                                sheet.addCell(formatguishu.format(formatdb.parse(timestr)));//结算归属日期
                            }
                            else {
                                sheet.addCell("");//结算归属日期
                            }
                            sheet.addCell(formatPaymoney(Double.valueOf(C_VMBALANCE)));//期末余额
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }
                        sheet.rowOver();
                    }
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            System.gc();
            sheet.sheetOver();
            book.writestr();
            book.closestr();
            String path = getSystemConfig("exportreportpath");
            if ("-1".equals(path)) {
                path = "d:";//默认地址
            }
            File f = new File(path + "/report/" + agentid + "/");
            if (!f.exists()) {
                f.mkdirs();
            }
            String filepath = "/report/" + agentid + "/" + name;
            File file = new File(path + filepath);
            if (file.exists()) {
                file.delete();
            }
            else {
                file.createNewFile();
            }
            FileUtils.writeStringToFile(file, book.getBookbody().toString(), "gb2312");
            try {
                createTaskauto(Long.valueOf(agentid), begin_time, end_time, filepath);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 创建自动执行任务
     */
    public void createTaskauto(long agentid, String start, String end, String path) {
        String createtime = TimeUtil.gettodaydate(4);
        String sql = "delete from T_TRAINREPORTTASKLIST where C_AGENTID="
                + agentid
                + " and C_BEGINTIME='"
                + start
                + "' and C_ENDTIME='"
                + end
                + "';insert into T_TRAINREPORTTASKLIST(C_AGENTID,C_BEGINTIME,C_ENDTIME,C_STATE,C_CREATETIME,C_USERID,C_PATH) values("
                + agentid + ",'" + start + "','" + end + "',2,'" + createtime + "',0,'" + path + "')";
        WriteLog.write("b自动生成报表", sql);
        Server.getInstance().getSystemService().findMapResultBySql(sql, null);
    }

    /**
     * 获取系统配置属性 实时
     */
    public static String getSystemConfig(String name) {
        List<Sysconfig> configs = Server.getInstance().getSystemService()
                .findAllSysconfig("where c_name='" + name + "'", "", -1, 0);
        if (configs != null && configs.size() == 1) {
            Sysconfig config = configs.get(0);
            return config.getValue();
        }
        return "-1";
    }

    /**
     * @param str
     * @return 是否为null或""
     */
    public boolean isNotNullOrEpt(String str) {
        if (str != null && str.trim().length() > 0) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * 将float格式化支持2伟小数
     * 
     * @param money
     * @return
     */
    public String formatPaymoney(Double num) {
        DecimalFormat format = null;
        format = (DecimalFormat) NumberFormat.getInstance();
        format.applyPattern("###0.00");
        try {
            String result = format.format(num);
            return result;
        }
        catch (Exception e) {
            return Double.toString(num);
        }
    }

    public String getagentname_b2bback(long id) {
        Customeragent agent = Server.getInstance().getMemberService().findCustomeragent(id);
        return getagentname(agent);
    }

    public String getagentname(Customeragent agent) {
        String agentname = "";
        if (this.isNotNullOrEpt(agent.getAgentshortname())) {
            agentname = agent.getAgentshortname();
        }
        else {
            agentname = agent.getAgentcompanyname();
        }
        return agentname;
    }

    /**
     * 解析空数据
     * @param data
     * @return
     */
    public Object parseNullData(Object data) {
        if (data == null) {
            return "";
        }
        return data;
    }

    /**
     * 根据agentid获取代理商的简称并且放缓存里
     */
    public String getagentidshortnamebyAgentid(Long id) {
        String shortnameString = "";
        shortnameString = getagentname_b2bback(id);
        return shortnameString;
    }

    /**
     * 添加到数据库
     */
    public boolean insertDb(String sqlstr) {
        try {
            int bo = Server.getInstance().getSystemService().excuteGiftBySql(sqlstr);
            if (bo > 0) {
                WriteLog.write("tc报表插入成功", sqlstr);
                return true;
            }
            WriteLog.write("tc报表插入失败", sqlstr);
            return false;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 保存到数据库
     * @param listorder 出票
     * @param listtuipiaoorder 退票
     * @param caiwuorder 财务充值
     */
    public void insertReportData(List listorder, List listtuipiaoorder, List caiwuorder, long agentid) {
        //出票
        try {
            if (listorder != null && listorder.size() > 0) {
                for (int i = 0; i < listorder.size(); i++) {
                    String sqlstr = "";
                    Map map = (Map) listorder.get(i);
                    String tcordernumber = map.get("C_QUNARORDERNUMBER").toString();////供应商订单号
                    String C_ORDERNUMBER = map.get("C_ORDERNUMBER").toString();////我们订单号
                    String extordernumber = map.get("C_EXTNUMBER").toString();////12306订单号
                    String price = map.get("C_SUPPLYPRICE").toString();//结算金额
                    String ticketcount = map.get("TICKETCOUNT").toString();//票数
                    String C_VMBALANCE = map.get("C_VMBALANCE").toString();//余额
                    String timestr = map.get("C_REBATETIME") == null ? "" : map.get("C_REBATETIME").toString();//交易时间
                    String commission = map.get("C_COMMISSION") == null ? "0" : map.get("C_COMMISSION").toString();//手续费
                    String reporttype = "1";
                    String state = "1";
                    sqlstr += "insert into trainreport(agentid,refordernumber,ordernumber,enumber,rebatemoney, reporttype, ticketcount,rebatetime,commission, rebatebalance,supplyname,state) values ("
                            + agentid
                            + ",'"
                            + tcordernumber
                            + "','"
                            + C_ORDERNUMBER
                            + "','"
                            + extordernumber
                            + "','"
                            + price
                            + "',"
                            + reporttype
                            + ",'"
                            + ticketcount
                            + "','"
                            + timestr
                            + "',"
                            + commission
                            + ",'" + C_VMBALANCE + "','航天商旅'," + state + ");";
                    insertDb(sqlstr);
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        //退票
        try {
            if (listtuipiaoorder != null && listtuipiaoorder.size() > 0) {
                for (int i = 0; i < listtuipiaoorder.size(); i++) {
                    String sqlstr = "";
                    Map map = (Map) listtuipiaoorder.get(i);
                    String tcordernumber = map.get("C_QUNARORDERNUMBER").toString();////供应商订单号
                    String extordernumber = map.get("C_EXTNUMBER").toString();////12306订单号
                    String C_ORDERNUMBER = map.get("C_ORDERNUMBER").toString();////我们订单号
                    String refundprice = map.get("C_PRICE").toString();//结算金额
                    String isapplyticketype = map.get("C_ISAPPLYTICKET").toString();//结算类型
                    String timestr = map.get("C_CREATETIME") == null ? "" : map.get("C_CREATETIME").toString();//交易时间
                    String shouxufei = map.get("C_PROCEDURE").toString();
                    String C_VMBALANCE = map.get("C_VMBALANCE").toString();//余额
                    String reporttype = "0";
                    if ("1".equals(isapplyticketype)) {
                        reporttype = "2";//线上退票
                    }
                    else if ("2".equals(isapplyticketype)) {
                        reporttype = "3";//线下退票
                    }
                    String commission = shouxufei;
                    String state = "1";
                    sqlstr += "insert into trainreport(agentid,refordernumber,ordernumber,enumber,rebatemoney, reporttype, ticketcount,rebatetime,commission, rebatebalance,supplyname,state) values ("
                            + agentid
                            + ",'"
                            + tcordernumber
                            + "','"
                            + C_ORDERNUMBER
                            + "','"
                            + extordernumber
                            + "','"
                            + refundprice
                            + "',"
                            + reporttype
                            + ",'1','"
                            + timestr
                            + "',"
                            + commission
                            + ",'"
                            + C_VMBALANCE + "','航天商旅'," + state + ");";
                    insertDb(sqlstr);
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        //财务充值
        try {
            if (caiwuorder != null && caiwuorder.size() > 0) {
                for (int i = 0; i < caiwuorder.size(); i++) {
                    String sqlstr = "";
                    Map map = (Map) caiwuorder.get(i);
                    String C_ORDERNUMBER = map.get("C_ORDERNUMBER").toString();////我们订单号
                    String tcordernumber = map.get("C_REFORDERNUM").toString();////供应商订单号
                    String extordernumber = "";////12306订单号
                    String C_REBATEMONEY = map.get("C_REBATEMONEY").toString();//结算金额
                    String timestr = map.get("C_REBATETIME") == null ? "" : map.get("C_REBATETIME").toString();//交易时间
                    String C_VMBALANCE = map.get("C_VMBALANCE").toString();//余额
                    String C_YEWUTYPE = map.get("C_YEWUTYPE").toString();//业务类型
                    String reporttype = "4";//财务充值
                    if ("0".equals(C_YEWUTYPE)) {
                        reporttype = "4";//财务充值
                    }
                    else if ("32".equals(C_YEWUTYPE)) {
                        reporttype = "5";//改签退款
                    }
                    else if ("33".equals(C_YEWUTYPE)) {
                        reporttype = "6";//改签扣款
                    }
                    else if ("34".equals(C_YEWUTYPE)) {
                        reporttype = "7";//线下改签
                    }
                    String commission = "0";
                    String state = "1";
                    sqlstr += "insert into trainreport(agentid,refordernumber,ordernumber,enumber,rebatemoney, reporttype, ticketcount,rebatetime,commission, rebatebalance,supplyname,state) values ("
                            + agentid
                            + ",'"
                            + tcordernumber
                            + "','"
                            + C_ORDERNUMBER
                            + "','"
                            + extordernumber
                            + "','"
                            + C_REBATEMONEY
                            + "',"
                            + reporttype
                            + ",'0','"
                            + timestr
                            + "',"
                            + commission
                            + ",'"
                            + C_VMBALANCE + "','航天商旅'," + state + ");";
                    insertDb(sqlstr);
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
