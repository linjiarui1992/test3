package com.ccservice.report.train;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.ccservice.b2b2c.base.customeragent.Customeragent;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.b2b2c.base.sysconfig.Sysconfig;
import com.ccservice.inter.server.Server;

public class TrainExptionReportJob {

    public static void main(String[] args) throws Exception {
        String begin_time = "2015-01-01";
        String end_time = "2015-05-01";
        String tcagentid = "46";
        if (Long.parseLong(tcagentid) > 0) {
            String jiekouname = "接口";
            try {
                if (jiekouname == null || "null".equals(jiekouname)) {
                    jiekouname = "接口";
                }
            }
            catch (Exception e2) {
                e2.printStackTrace();
            }
            ISystemService service = Server.getInstance().getSystemService();
            List kouerrorstate = new ArrayList();//查询扣款了状态不对的订单
            List ordererrorkou = new ArrayList();//查询出票了但没有扣款
            List kouticketerror = new ArrayList();//查询加款票状态不对
            List ticketkouerror = new ArrayList();//查询票状态对但没有加款的退票
            kouerrorstate = service.findMapResultByProcedure(" PROC_REPORT 1,'" + begin_time + "','" + end_time + "'");
            ordererrorkou = service.findMapResultByProcedure(" PROC_REPORT 2,'" + begin_time + "','" + end_time + "'");
            kouticketerror = service.findMapResultByProcedure(" PROC_REPORT 3,'" + begin_time + "','" + end_time + "'");
            ticketkouerror = service.findMapResultByProcedure(" PROC_REPORT 4,'" + begin_time + "','" + end_time + "'");
            String number = System.currentTimeMillis() + new Random().nextInt(10) + "";
            String name = "asdfasdfasdfasdfasdf.txt";
            String path = getSystemConfig("exportreportpath");
            if ("-1".equals(path)) {
                path = "d:";//默认地址
            }
            File f = new File(path + "/report/46/");
            if (!f.exists()) {
                f.mkdirs();
            }
            String filepath = "/report/46/" + name;
            File file = new File(path + filepath);
            if (file.exists()) {
                file.delete();
            }
            else {
                file.createNewFile();
            }
            String logString = "";
            if (kouerrorstate != null && kouerrorstate.size() > 0) {
                logString = "资金扣款订单状态不对的订单：";
                WriteLog1(file, logString);
                for (int i = 0; i < kouerrorstate.size(); i++) {
                    try {
                        Map map = (Map) kouerrorstate.get(i);
                        String ordernumber = map.get("C_ORDERNUMBER").toString();
                        String Oid = map.get("RID").toString();
                        logString = "订单号:" + ordernumber + ",订单ID:" + Oid;
                        WriteLog1(file, logString);
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            if (ordererrorkou != null && ordererrorkou.size() > 0) {
                logString = "查询出票了但没有扣款：";
                WriteLog1(file, logString);
                for (int i = 0; i < ordererrorkou.size(); i++) {
                    try {
                        Map map = (Map) ordererrorkou.get(i);
                        String ordernumber = map.get("C_ORDERNUMBER").toString();
                        String Oid = map.get("OID").toString();
                        logString = "订单号:" + ordernumber + ",订单ID:" + Oid;
                        WriteLog1(file, logString);
                    }
                    catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
            if (kouticketerror != null && kouticketerror.size() > 0) {
                logString = "加款了票状态不对：";
                WriteLog1(file, logString);
                for (int i = 0; i < kouticketerror.size(); i++) {
                    try {
                        Map map = (Map) kouticketerror.get(i);
                        String ordernumber = map.get("C_ORDERNUMBER").toString();
                        String Oid = map.get("RID").toString();
                        logString = "订单号:" + ordernumber + ",订单ID:" + Oid;
                        WriteLog1(file, logString);
                    }
                    catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
            if (ticketkouerror != null && ticketkouerror.size() > 0) {
                logString = "票状态对但没有加款的退票(联系技术):";
                WriteLog1(file, logString);
                for (int i = 0; i < ticketkouerror.size(); i++) {
                    try {
                        Map map = (Map) ticketkouerror.get(i);
                        String Oid = map.get("ID").toString();
                        logString = "票ID:" + Oid;
                        WriteLog1(file, logString);
                    }
                    catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        }

    }

    /**
    * 异常记录
    * 
    * @return
    * @throws Exception
    */
    public void tcexprotfilereport(String agentid, String begin_time, String end_time) throws Exception {
        String tcagentid = agentid;
        if (Long.parseLong(tcagentid) > 0) {
            String jiekouname = "接口";
            try {
                jiekouname = getagentidshortnamebyAgentid(Long.valueOf(tcagentid));
                if (jiekouname == null || "null".equals(jiekouname)) {
                    jiekouname = "接口";
                }
            }
            catch (Exception e2) {
                e2.printStackTrace();
            }
            ISystemService service = Server.getInstance().getSystemService();
            List kouerrorstate = new ArrayList();//查询扣款了状态不对的订单
            List ordererrorkou = new ArrayList();//查询出票了但没有扣款
            List kouticketerror = new ArrayList();//查询加款票状态不对
            List ticketkouerror = new ArrayList();//查询票状态对但没有加款的退票
            String number = System.currentTimeMillis() + new Random().nextInt(10) + "";
            String name = number + "_" + begin_time + "_" + end_time + "Explog.txt";
            String path = getSystemConfig("exportreportpath");
            if ("-1".equals(path)) {
                path = "d:";//默认地址
            }
            File f = new File(path + "/report/" + agentid + "/");
            if (!f.exists()) {
                f.mkdirs();
            }
            String filepath = "/report/" + agentid + "/" + name;
            File file = new File(path + filepath);
            if (file.exists()) {
                file.delete();
            }
            else {
                file.createNewFile();
            }
            kouerrorstate = service.findMapResultByProcedure(" PROC_REPORT 1,'" + begin_time + "','" + end_time + "'");
            ordererrorkou = service.findMapResultByProcedure(" PROC_REPORT 2,'" + begin_time + "','" + end_time + "'");
            kouticketerror = service.findMapResultByProcedure(" PROC_REPORT 3,'" + begin_time + "','" + end_time + "'");
            ticketkouerror = service.findMapResultByProcedure(" PROC_REPORT 4,'" + begin_time + "','" + end_time + "'");
            String logString = "";
            if (kouerrorstate != null && kouerrorstate.size() > 0) {
                logString = "资金扣款订单状态不对的订单：";
                WriteLog(file, logString);
                for (int i = 0; i < kouerrorstate.size(); i++) {
                    try {
                        Map map = (Map) kouerrorstate.get(i);
                        String ordernumber = map.get("C_ORDERNUMBER").toString();
                        String Oid = map.get("RID").toString();
                        logString = "订单号:" + ordernumber + ",订单ID:" + Oid;
                        WriteLog(file, logString);
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            if (ordererrorkou != null && ordererrorkou.size() > 0) {
                logString = "查询出票了但没有扣款：";
                WriteLog(file, logString);
                for (int i = 0; i < ordererrorkou.size(); i++) {
                    try {
                        Map map = (Map) ordererrorkou.get(i);
                        String ordernumber = map.get("C_ORDERNUMBER").toString();
                        String Oid = map.get("OID").toString();
                        logString = "订单号:" + ordernumber + ",订单ID:" + Oid;
                        WriteLog(file, logString);
                    }
                    catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
            if (kouticketerror != null && kouticketerror.size() > 0) {
                logString = "加款了票状态不对：";
                WriteLog(file, logString);
                for (int i = 0; i < kouticketerror.size(); i++) {
                    try {
                        Map map = (Map) kouticketerror.get(i);
                        String ordernumber = map.get("C_ORDERNUMBER").toString();
                        String Oid = map.get("RID").toString();
                        logString = "订单号:" + ordernumber + ",订单ID:" + Oid;
                        WriteLog(file, logString);
                    }
                    catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
            if (ticketkouerror != null && ticketkouerror.size() > 0) {
                logString = "票状态对但没有加款的退票(联系技术):";
                WriteLog(file, logString);
                for (int i = 0; i < ticketkouerror.size(); i++) {
                    try {
                        Map map = (Map) ticketkouerror.get(i);
                        String Oid = map.get("ID").toString();
                        logString = "票ID:" + Oid;
                        WriteLog(file, logString);
                    }
                    catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public void WriteLog(File file, String logString) {
        try {
            PrintWriter printWriter = new PrintWriter(new FileOutputStream(file, true));// 紧接文件尾写入日志字符串
            printWriter.println(logString);
            printWriter.flush();
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void WriteLog1(File file, String logString) {
        try {
            PrintWriter printWriter = new PrintWriter(new FileOutputStream(file, true));// 紧接文件尾写入日志字符串
            printWriter.println(logString);
            printWriter.flush();
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * 获取系统配置属性 实时
     */
    public static String getSystemConfig(String name) {
        List<Sysconfig> configs = Server.getInstance().getSystemService()
                .findAllSysconfig("where c_name='" + name + "'", "", -1, 0);
        if (configs != null && configs.size() == 1) {
            Sysconfig config = configs.get(0);
            return config.getValue();
        }
        return "-1";
    }

    /**
     * @param str
     * @return 是否为null或""
     */
    public boolean isNotNullOrEpt(String str) {
        if (str != null && str.trim().length() > 0) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * 将float格式化支持2伟小数
     * 
     * @param money
     * @return
     */
    public String formatPaymoney(Double num) {
        DecimalFormat format = null;
        format = (DecimalFormat) NumberFormat.getInstance();
        format.applyPattern("###0.00");
        try {
            String result = format.format(num);
            return result;
        }
        catch (Exception e) {
            return Double.toString(num);
        }
    }

    public String getagentname_b2bback(long id) {
        Customeragent agent = Server.getInstance().getMemberService().findCustomeragent(id);
        return getagentname(agent);
    }

    public String getagentname(Customeragent agent) {
        String agentname = "";
        if (this.isNotNullOrEpt(agent.getAgentshortname())) {
            agentname = agent.getAgentshortname();
        }
        else {
            agentname = agent.getAgentcompanyname();
        }
        return agentname;
    }

    /**
     * 根据agentid获取代理商的简称并且放缓存里
     */
    public String getagentidshortnamebyAgentid(Long id) {
        String shortnameString = "";
        shortnameString = getagentname_b2bback(id);
        return shortnameString;
    }

}
