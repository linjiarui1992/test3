package com.ccservice.train.qunar;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import com.ccservice.b2b2c.base.rebaterecord.Rebaterecord;
import com.ccservice.b2b2c.base.sysconfig.Sysconfig;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderchange;
import com.ccservice.b2b2c.ben.Payresult;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.server.Server;

/**
 * 改签消费者kou'fei
 * @author wzc
 * @time   2016年1月29日 下午12:44:36
 */
public class GqTrainorderDeduction extends TrainSupplyMethod {
    private Trainorder trainorder;

    private Trainorderchange orderchange;

    private long changeorderid;

    private long orderid;

    private boolean iscandeduction;

    //2后扣款模式 1预付模式
    private int type;

    public GqTrainorderDeduction(long orderid, int type, long changeorderid) {
        this.orderid = orderid;
        this.type = type;
        this.changeorderid = changeorderid;
    }

    public void tongchengDeduction() {
        this.trainorder = Server.getInstance().getTrainService().findTrainorder(orderid);
        this.orderchange = Server.getInstance().getTrainService().findTrainOrderChangeById(changeorderid);
        if (trainorder.getInterfacetype() != null && trainorder.getInterfacetype() == 4) {//出票前代扣

        }
        else {
            if (1 == type) {
                xuniDeduction();
            }
            if (2 == type) {
                //afterDeduction();//后扣款模式支付扣除支付宝里的钱
            }
        }
    }

    /**
     * 后扣款现扣模式是否可以扣款 
     * @time 2015年2月12日 下午10:30:04
     * @author fiend
     */
    @SuppressWarnings({ "rawtypes" })
    public void iscanDeductionAfter() {
        try {
            String sql = "select COUNT(id) countid from T_REBATERECORD with(nolock) where c_tradeno is not null and C_ORDERID="
                    + this.orderid;
            List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
            if (list.size() > 0) {
                Map map = (Map) list.get(0);
                int countid = Integer.valueOf(map.get("countid").toString());
                if (countid == 0) {
                    this.iscandeduction = true;
                    return;
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        this.iscandeduction = false;
    }

    //TODO============================================================虚拟扣款开始======================================================================
    /**
     * 同程虚拟支付
     * @return
     * @time 2015年1月27日 上午10:55:20
     * @author fiend
     */
    public void xuniDeduction() {
        long t1 = System.currentTimeMillis();
        iscanDeduction();
        long t2 = System.currentTimeMillis();
        if (this.iscandeduction) {
            double kouprice = getShouXuFei(this.trainorder.getAgentid(), trainorder, orderchange);
            if (kouprice > 0) {
                long t3 = System.currentTimeMillis();
                Payresult payresult = vmonyPay(this.orderchange.getId(), "GQ" + this.trainorder.getOrdernumber(),
                        kouprice, 51, this.trainorder.getQunarOrdernumber(), this.trainorder.getAgentid());
                long t4 = System.currentTimeMillis();
                if (payresult.isPaysuccess()) {
                    writeRC(this.trainorder.getId(), "[改签 - " + orderchange.getId() + "]改签服务费扣款成功:" + kouprice,
                            "系统自动扣款", 3, 1);
                    upTrainorder("UPDATE T_TRAINORDERCHANGE SET C_ISPLACEING=" + 1 + " WHERE ID="
                            + this.orderchange.getId());
                }
                else {
                    try {
                        writeRC(this.trainorder.getId(),
                                "[改签 - " + orderchange.getId() + "]改签服务费扣款失败【" + payresult.getResultmessage() + "】",
                                "系统自动扣款", 3, 1);
                        upTrainorder("UPDATE T_TRAINORDERCHANGE SET C_ISPLACEING=" + 4 + " WHERE ID="
                                + this.orderchange.getId());
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                long t5 = System.currentTimeMillis();
                WriteLog.write("t虚拟耗时改签", this.trainorder.getId() + ":耗时：2:" + (t3 - t2) + ",3：" + (t4 - t3) + ",4:"
                        + (t5 - t4));
            }
        }
        else {
            writeRC(this.trainorder.getId(), "改签拒绝扣款-已扣款或扣款异常", "系统自动扣款", 3, 1);
        }
        long t6 = System.currentTimeMillis();
        WriteLog.write("t虚拟耗时改签", this.trainorder.getId() + ":耗时：1：" + (t2 - t1) + ",5:" + (t6 - t1));
    }

    /**
     * 获取改签订单手续费
     * @param agentid
     * @return
     */
    public double getShouXuFei(long agentid, Trainorder order, Trainorderchange orderchange) {
        double sumshouxufei = 0.0d;
        try {
            double shouxufei = 0.0d;//代购手续费
            int ticketcount = 0;
            String sql = "select ISNULL(ChangeShouxufei,0) ChangeShouxufei  from T_INTERFACEACCOUNT with(nolock) where C_AGENTID="
                    + agentid;
            List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
            if (list.size() > 0) {
                Map map = (Map) list.get(0);
                shouxufei = Double.valueOf(map.get("ChangeShouxufei").toString());
            }
            if (shouxufei > 0) {
                String ticketidstr = orderchange.getTcTicketId();
                if (ticketidstr != null) {
                    ticketcount = ticketidstr.split("@").length;
                }
                System.out.println("改签订单id:" + orderchange.getId() + ",类型：" + order.getOrdertype());
                sumshouxufei = shouxufei * ticketcount;
                try {
                    String updatesql = "update T_TRAINORDERCHANGE set C_COMMISSION=" + sumshouxufei + " WHERE ID="
                            + orderchange.getId();
                    Server.getInstance().getSystemService().findMapResultBySql(updatesql, null);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        catch (Exception e) {
        }
        return sumshouxufei;
    }

    /**
     * 是否可以扣款 
     * @time 2015年2月12日 下午10:30:04
     * @author fiend
     */
    @SuppressWarnings({ "rawtypes" })
    public void iscanDeduction() {
        try {
            String sql = "select COUNT(id) countid from T_REBATERECORD with(nolock) where C_REBATETYPE=2 and C_YEWUTYPE=51 and C_ORDERID="
                    + this.changeorderid;
            List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
            if (list.size() > 0) {
                Map map = (Map) list.get(0);
                int countid = Integer.valueOf(map.get("countid").toString());
                if (countid == 0) {
                    this.iscandeduction = true;
                    return;
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        this.iscandeduction = false;
    }

    /**
     * 
     * 扣除虚拟账号里面的钱
     * 
     * @param orderid 订单ID
     * @param ordernumber 订单号
     * @param orderprice 订单支付金额
     * @param ywtype 业务类型 3：火车票
     * @param refordernum  同城关联订单号
     */
    public Payresult vmonyPay(long orderid, String ordernumber, double orderprice, int ywtype, String refordernum,
            long agentid) {
        WriteLog.write("tc虚拟账户支付", "订单号ID：" + orderid + ":订单号：" + ordernumber + ":订单价格：" + orderprice + ":业务类型："
                + ywtype);
        Payresult payresult = new Payresult();
        payresult.setPaysuccess(true);
        payresult.setResultmessage("支付成功");
        String msg = "订单";
        if (ywtype == 21) {
            msg = "发票";
            ywtype = 2;
        }
        if (orderprice == 0) {
            payresult.setPaysuccess(false);
            payresult.setResultmessage(msg + "支付失败,支付金额不能等于0！");
            WriteLog.write("tc虚拟账户支付", "订单号ID：" + orderid + ",订单号：" + ordernumber + "," + payresult.getResultmessage());
            return payresult;
        }
        //代理虚拟账户余额
        float agentvmoney = getTotalVmoney(agentid);
        //虚拟账户余额不足
        if (agentvmoney < orderprice) {
            payresult.setPaysuccess(false);
            payresult.setResultmessage(msg + "支付失败,您的账户余额不足于当前" + msg + "支付！");
            WriteLog.write("tc虚拟账户支付", "订单号ID：" + orderid + ":订单号：" + ordernumber + ":" + payresult.getResultmessage());
            return payresult;
        }
        else {
            try {
                Rebaterecord record = new Rebaterecord();
                record.setRebatemoney(0D - orderprice);
                record.setCustomerid(0);
                record.setRebatetype(Rebaterecord.PINGTAIXIAOFEI);
                record.setOrdernumber(ordernumber);
                record.setOrderid(orderid);
                record.setYewutype(ywtype);
                record.setRebate(0D);
                record.setVmenable(1);
                record.setPaymethod(10);
                if (refordernum != null) {
                    record.setRefordernum(refordernum);
                }
                record.setRebateagentid(agentid);
                record.setRebatetime(getCurrentTime());
                String memo = msg + "支付扣除" + orderprice + "元";
                addAgentvmoney(agentid, 0 - orderprice);
                record.setRebatememo(memo);
                record = Server.getInstance().getMemberService().createRebaterecord(record);
                payresult.setResultmessage(record.getTradeno());
                WriteLog.write("tc虚拟账户支付", "订单号ID：" + orderid + ":订单号：" + ordernumber + ":" + record.getTradeno());
            }
            catch (Exception e) {
                e.printStackTrace();
                payresult.setPaysuccess(false);
                payresult.setResultmessage(msg + "支付失败！");
                WriteLog.write("tc虚拟账户支付", "订单号ID：" + orderid + ":订单号：" + ordernumber + "," + e.getMessage());
            }
        }
        return payresult;
    }

    /**
     * 根据agentid获取余额
     */
    @SuppressWarnings("rawtypes")
    public static float getTotalVmoney(long agentid) {
        String sql = "SELECT C_VMONEY AS VMONEY FROM T_CUSTOMERAGENT with(nolock) WHERE ID=" + agentid;
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        if (list.size() > 0) {
            Map m = (Map) list.get(0);
            float vmoney = Float.valueOf(m.get("VMONEY").toString());
            return vmoney;
        }
        return 0;
    }

    /**
     * 获取系统配置属性 实时
     */
    @SuppressWarnings("unchecked")
    public static String getSystemConfigdb(String name) {
        List<Sysconfig> configs = Server.getInstance().getSystemService()
                .findAllSysconfig("where c_name='" + name + "'", "", -1, 0);
        if (configs != null && configs.size() == 1) {
            Sysconfig config = configs.get(0);
            return config.getValue();
        }
        return "-1";
    }

    /**
     * 获取当前时间
     */
    public static Timestamp getCurrentTime() {
        return new Timestamp(System.currentTimeMillis());
    }

    @SuppressWarnings("rawtypes")
    public static Long getuseridbyagentid(Long agentid) {
        String sql = " select id from T_CUSTOMERUSER with(nolock) where C_AGENTID=" + agentid + " and C_ISADMIN=1";
        Map map = (Map) Server.getInstance().getSystemService().findMapResultBySql(sql, null).get(0);
        return Long.valueOf(map.get("id").toString());

    }

    /**
     * 操作虚拟账户钱
     */
    @SuppressWarnings("rawtypes")
    public static float addAgentvmoney(long agentid, double money) {
        String sql = "UPDATE T_CUSTOMERAGENT SET C_VMONEY=C_VMONEY+" + money + " WHERE ID=" + agentid + ";";
        WriteLog.write("tc支付sql", sql);
        Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        WriteLog.write("tc支付sql", "成功：" + money);
        return 0.0f;
    }

}
