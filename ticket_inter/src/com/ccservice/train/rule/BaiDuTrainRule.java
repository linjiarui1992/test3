package com.ccservice.train.rule;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.qunar.util.ExceptionUtil;

/**
 * 百度锁单逻辑
 * 
 * @time 2017年9月27日 下午4:21:22
 * @author fiend
 */
public class BaiDuTrainRule {
    /**
     * 判断是否是百度
     * 
     * @param trainorder
     * @return
     * @time 2017年9月27日 下午4:30:33
     * @author fiend
     */
    public boolean isBaidu(Trainorder trainorder) {
        return trainorder != null && 161 == trainorder.getAgentid();
    }

    /**
     * 百度锁单
     * 
     * @param trainorder
     * @return
     * @time 2017年9月27日 下午3:41:15
     * @author fiend
     */
    public boolean handleBaiDuOrder(Trainorder trainorder) {
        boolean success = true;
        try {
            createTrainorderrc(trainorder.getId(), "baidu订单开始尝试锁单，锁单过程请勿人工参与！", "taobao锁单", trainorder.getOrderstatus());
            //拼接锁单请求
            String baidu_handle_url = PropertyUtil.getValue("baidu_handle_url", "train.properties");
            String param = "orderid=" + trainorder.getId() + "&ceshistatusF=true";
            WriteLog.write("百度锁单", baidu_handle_url + "?" + param);
            //虚拟锁单返回结果
            String result = "{\"success\":false,\"msg\":\"请求咱家自己的回调服务器异常！请联系技术！\"}";
            JSONObject resultJsonObject = JSONObject.parseObject(result);
            //为避免服务端更新等导致请求异常，特加10次循环共5分钟，防止死订单
            for (int i = 0; i < 10; i++) {
                try {
                    String resultreal = SendPostandGet.submitGet(baidu_handle_url + "?" + param, "UTF-8").toString();
                    WriteLog.write("百度锁单", trainorder.getId() + "--->" + resultreal);
                    JSONObject resultJsonObjectreal = JSONObject.parseObject(resultreal);
                    if (resultJsonObjectreal.containsKey("success") && resultJsonObjectreal.containsKey("msg")) {
                        resultJsonObject = resultJsonObjectreal;
                        break;
                    }
                    try {
                        Thread.sleep(10000L);
                    }
                    catch (Exception e) {
                    }
                }
                catch (Exception e) {
                    ExceptionUtil.writelogByException("百度锁单_Exception", e, "订单号:" + trainorder.getId() + "--->请求锁单异常");
                }
            }
            success = resultJsonObject.getBooleanValue("success");
            createTrainorderrc(trainorder.getId(), "baidu锁单结束--->" + resultJsonObject.getString("msg"), "baidu锁单",
                    trainorder.getOrderstatus());
            success = success && lockAfter(trainorder);
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("百度锁单_Exception", e, "订单号:" + trainorder.getId());
        }
        return success;
    }

    /**
     * 锁单后校验取消并发
     * 
     * @param trainorder
     * @return
     * @time 2017年9月27日 下午4:20:11
     * @author fiend
     */
    @SuppressWarnings("rawtypes")
    private boolean lockAfter(Trainorder trainorder) {
        boolean flag = false;
        String sql = "[sp_T_TRAINORDER_Baidu_LOCKED_SELECT] @oID=" + trainorder.getId();
        try {
            List list = Server.getInstance().getSystemService().findMapResultByProcedure(sql);
            Map map = (Map) list.get(0);
            flag = Boolean.valueOf(map.get("flag").toString());
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("百度锁单_Exception", e, sql);
        }
        WriteLog.write("百度锁单", "锁单后校验取消并发>>>" + trainorder.getId() + "--->" + flag);
        return flag;
    }

    /**
     * 创建火车票操作记录
     * 
     * @param trainorderId
     *            火车票订单id
     * @param content
     *            内容
     * @param createuser
     *            用户
     * @param status
     *            状态
     * @time 2015年1月18日 下午5:02:43
     * @author chendong
     */
    private void createTrainorderrc(Long trainorderId, String content, String createuser, int status) {
        try {
            Trainorderrc rc = new Trainorderrc();
            rc.setOrderid(trainorderId);
            rc.setContent(content);
            rc.setStatus(status);
            rc.setCreateuser(createuser);
            rc.setYwtype(1);
            Server.getInstance().getTrainService().createTrainorderrc(rc);
        }
        catch (Exception e) {
            WriteLog.write("操作记录失败", trainorderId + ":content:" + content);
        }
    }
}
