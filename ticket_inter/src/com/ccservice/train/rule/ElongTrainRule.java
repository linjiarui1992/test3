package com.ccservice.train.rule;

import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.inter.job.train.TrainInterfaceMethod;

public class ElongTrainRule {

    /**
     * 艺龙下单成功后是否可以支付规则
     * @param trainorder
     * @param interfacetype
     * @return
     * @author w.c.l
     */
    public static boolean isCanGenerate(Trainorder trainorder, int interfacetype) {
        if (TrainInterfaceMethod.YILONG2 == interfacetype) {

            for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
                for (Trainticket trainticket : trainpassenger.getTraintickets()) {
                    if (trainticket.getPrice() > trainticket.getPayprice()) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
}
