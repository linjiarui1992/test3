package com.ccservice.train.order;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.b2b2c.policy.TimeUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.server.Server;
import com.ccservice.train.mqlistener.MQMethod;
import com.ccservice.train.mqlistener.TrainActiveMQ;

/**
 * 支付结果处理
 * @author wzc
 *
 */
public class CallBackMethodResultUnionPayResult extends TrainSupplyMethod {
    /**
     * 支付结果处理
     * @param obj
     * @param payset 支付链接
     * @return
     */
    public String parsePayResult(String result, long orderid, int t, String payset, String paymethodtype,
            String payMethodNew) {
        WriteLog.write("pay_CallBackMethodResultPayResultUnion",
                t + ":请求返回:" + result + ":payMethodNew=" + payMethodNew);
        String haddleresult = "false";
        Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(orderid);
        try {
            trainorder.setSupplyaccount(trainorder.getSupplyaccount().split("/")[0] + "/" + payset);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        trainorder.setOrderstatus(0);//支付订单不改变订单状态
        Server.getInstance().getTrainService().updateTrainorder(trainorder);
        if ("".equals(result) || !result.contains("status")) {//没返回任何数据
            changeQuestionOrder(trainorder, "多次支付失败");
            return haddleresult;
        }
        JSONObject jsoresult = JSONObject.parseObject(result);
        if (jsoresult.getString("status").equals("1") && "支付成功".equals(jsoresult.getString("info"))) {
            try {
                trainRC(trainorder.getId(), "支付成功");
                trainorder.setSupplytradeno(jsoresult.getString("liushuihao"));
                trainorder.setAutounionpayurlsecond(jsoresult.getString("liushuihao"));//使用的银行卡号
                trainorder.setState12306(Trainorder.ORDEREDPAYED);//12306订单支付成功
                Server.getInstance().getTrainService().updateTrainorder(trainorder);
                new TrainActiveMQ(MQMethod.QUERY, trainorder.getId(), System.currentTimeMillis(),
                        System.currentTimeMillis(), 0).sendqueryMQ();
                String now = TimeUtil.gettodaydatebyfrontandback(4, 0);

                try {
                    String interfaceOrderNumber = trainorder.getQunarOrdernumber();
                    Integer PayMethod = Integer.valueOf(payMethodNew);
                    // 更新正式的支付方式
                    String updatePayMethodSql = "[dbo].[sp_TrainOrderInfo_insert_PayMethod]@interfaceOrderNumber = '"
                            + interfaceOrderNumber + "',@PayMethod = " + PayMethod;
                    Server.getInstance().getSystemService().findMapResultByProcedure(updatePayMethodSql);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }

                String sql = "delete from T_TRAINREFINFO where C_ORDERID=" + trainorder.getId()
                        + ";insert T_TRAINREFINFO(C_ORDERID,C_PAYSUCCESSTIME) values(" + trainorder.getId() + ",'" + now
                        + "')";
                WriteLog.write("pay_CallBackMethodResultPayResultUnion", t + ":更新sql：" + sql);
                Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                WriteLog.write("pay_CallBackMethodResultPayResultUnion",
                        t + ":订单号：" + trainorder.getOrdernumber() + ",提交自动支付成功");
            }
            catch (Exception e1) {
                WriteLog.write("pay_CallBackMethodResultPayResultUnion",
                        t + ":订单号：" + trainorder.getOrdernumber() + "保持银联交易号失败");
                e1.printStackTrace();
            }
        }
        else {
            String info = jsoresult.containsKey("info") ? jsoresult.getString("info") : "";
            WriteLog.write("pay_CallBackMethodResultPayResultUnion",
                    t + ":订单号：" + trainorder.getOrdernumber() + "result:" + result + ":支付失败");
            changeQuestionOrder(trainorder, "多次支付失败[" + info + "]");
        }
        return haddleresult;
    }

    /**
     * 变成问题订单
     * @param trainorderid
     * @param msg
     */
    public void changeQuestionOrder(Trainorder trainorder, String msg) {
        trainorder.setState12306(Integer.valueOf(7));
        trainorder.setIsquestionorder(Integer.valueOf(2));
        trainorder.setPaysupplystatus(Integer.valueOf(2));
        try {
            trainRC(trainorder.getId(), msg);
            Server.getInstance().getTrainService().updateTrainorder(trainorder);
        }
        catch (Exception e) {
        }
    }

    public void trainRC(long trainorderid, String msg) {
        Trainorderrc rc = new Trainorderrc();
        rc.setOrderid(trainorderid);
        rc.setContent(msg);
        rc.setCreateuser("自动支付");
        rc.setYwtype(1);
        Server.getInstance().getTrainService().createTrainorderrc(rc);
    }

}
