package com.ccservice.train.account;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.account.util.RefreshAccountCookie;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.inter.job.train.Account12306Util;
import com.ccservice.inter.server.Server;
import com.ccservice.qunar.util.ExceptionUtil;

/**
 * 刷新cookie的
 * 
 * @parameter
 * @time 2017年7月13日 下午10:10:58
 * @author YangFan
 */
public class CookieLogic extends RefreshAccountCookie {
    static CookieLogic cookieLogic = new CookieLogic();

    private CookieLogic() {

    }

    public static CookieLogic getInstance() {
        return cookieLogic;
    }

    @Override
    public boolean getFlagByDB() {
        try {
            //从DB获取flag，然后判断是true还是false
            List list = Server.getInstance().getSystemService()
                    .findMapResultBySql("[sp_T_SYSCONFIG_selectValue]", null);
            if (list.size() > 0) {
                Map map = (Map) list.get(0);
                String value = (String) map.get("Value");
                if ("1".equals(value)) {
                    return true;
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }

    private boolean isLogin(String result, Customeruser customeruser) {
        return !Account12306Util.accountNoLogin(result, customeruser);
    }

    public void refresh(String result, Customeruser customeruser) {
        if (customeruser == null || result == null || "".equals(result)) {//以上情况直接结束
            return;
        }
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = JSONObject.parseObject(result);
        }
        catch (Exception e) {//抛异常直接结束
            return;
        }
        if (!jsonObject.containsKey("cookie")) {//不包含cookie
            return;
        }
        if (isLogin(result, customeruser)) {//不是掉线的情况，刷新cookie
            try {
                boolean refreshResult = CookieLogic.getInstance().refreshCookie(customeruser.getLoginname(),
                        customeruser.getCardnunber(), jsonObject.getString("cookie"));
                if (refreshResult) {//刷新内存中的cookie
                    customeruser.setCardnunber(jsonObject.getString("cookie"));
                }
                WriteLog.write("CookieLogic_refresh",
                        customeruser.getLoginname() + ">>>>>newCookie:" + jsonObject.getString("cookie") + "isSussess:"
                                + refreshResult);
            }
            catch (Exception e) {
                ExceptionUtil.writelogByException("CookieLogic_refresh_Exception", e, customeruser.getLoginname()
                        + ">>>>>newCookie:" + jsonObject.getString("cookie"));
            }
        }
    }
}
