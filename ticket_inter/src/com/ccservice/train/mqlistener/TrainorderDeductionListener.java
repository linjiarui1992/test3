package com.ccservice.train.mqlistener;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.train.qunar.TrainorderDeduction;

public class TrainorderDeductionListener extends TrainSupplyMethod implements MessageListener {
    private String orderNoticeResult;

    private long orderid;

    //2后扣款模式 1预付模式
    private int type;

    @Override
    public void onMessage(Message message) {
        try {
            this.orderNoticeResult = ((TextMessage) message).getText();
            WriteLog.write("TrainorderDeduction", orderNoticeResult);
            JSONObject jsonobject = JSONObject.parseObject(orderNoticeResult);
            this.orderid = jsonobject.containsKey("orderid") ? jsonobject.getLongValue("orderid") : 0;
            this.type = jsonobject.containsKey("type") ? jsonobject.getIntValue("type") : 0;
            new TrainorderDeduction(this.orderid, type).tongchengDeduction();
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        JSONObject jsonobject = new JSONObject();
        jsonobject.put("orderid", "31025");
        jsonobject.put("type", "2");
        System.out.println(jsonobject.toJSONString());

    }
}
