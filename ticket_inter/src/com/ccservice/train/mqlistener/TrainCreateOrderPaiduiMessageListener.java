package com.ccservice.train.mqlistener;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.job.train.thread.TrainCreateOrderPaiDui;

/**
 * 排队监听
 * @time 2015年12月29日 上午11:24:10
 * @author fanchangwei
 */
public class TrainCreateOrderPaiduiMessageListener extends TrainSupplyMethod implements MessageListener {
    @Override
    public void onMessage(Message message) {
        try {
            String orderResult = ((TextMessage) message).getText(); // 获取字符串
            JSONObject json = JSONObject.parseObject(orderResult); //字符串转为JSONObject
            long trainorderid = json.getLongValue("trainorderid"); //解析JSON对象获取订单id
            boolean isLastPaidui = json.getBooleanValue("isLastPaidui");//解析JSONobject获取是否排队
            TrainCreateOrderPaiDui trainCreateOrderPaiDui = new TrainCreateOrderPaiDui(trainorderid);
            trainCreateOrderPaiDui.startPaiduiResult(isLastPaidui);
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
