package com.ccservice.train.mqlistener;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.train.pay.MyGetPayURLNew;
import com.ccservice.inter.train.pay.MyThreadCallBack;

/**
 * 获取支付链接
 * @author wzc
 * @time 2015-05-18 16:01
 *
 */
public class TrainGetURLMessageListenerNew extends TrainSupplyMethod implements MessageListener {

    @Override
    public void onMessage(Message message) {
        try {
            int t = new Random().nextInt(10000);
            String orderNoticeResult = ((TextMessage) message).getText();
            JSONObject jsonobject = JSONObject.parseObject(orderNoticeResult);
            long orderid = jsonobject.containsKey("orderid") ? jsonobject.getLongValue("orderid") : 0;
            String loginname = jsonobject.containsKey("loginname") ? jsonobject.getString("loginname") : "";
            String cookieString = jsonobject.containsKey("cookieString") ? jsonobject.getString("cookieString") : "";
            String callbackurl = jsonobject.containsKey("callbackurl") ? jsonobject.getString("callbackurl") : "";
            String mqname = jsonobject.containsKey("mqname") ? jsonobject.getString("mqname") : "";
            String extnumber = jsonobject.containsKey("extnumber") ? jsonobject.getString("extnumber") : "";
            String ordernumber = jsonobject.containsKey("ordernumber") ? jsonobject.getString("ordernumber") : "";
            String password = jsonobject.containsKey("password") ? jsonobject.getString("password") : "";
            long payflag = jsonobject.containsKey("payflag") ? jsonobject.getLongValue("payflag") : 1;
            long ordersource = jsonobject.containsKey("ordersource") ? jsonobject.getLongValue("ordersource") : 0;
            float price = jsonobject.containsKey("price") ? jsonobject.getFloatValue("price") : 0;
            WriteLog.write("12306_TrainGetURLMessageListener_MQ", t + ":" + orderNoticeResult);
            String getpayurltype = getSysconfigString("getpayurltype");//支付类型
            String result = "";
            if (cookieString != null) {
                MyGetPayURLNew payurlobj = new MyGetPayURLNew(orderid, loginname, cookieString, callbackurl, mqname, t,
                        getpayurltype, extnumber, ordernumber, password, ordersource, payflag, price);
                result = payurlobj.getPayUrl();
            }
            try {
                ExecutorService pool = Executors.newFixedThreadPool(1);
                Thread thr = new MyThreadCallBack(t, result, callbackurl, orderid, "payurl");
                pool.execute(thr);
                pool.shutdown();
            }
            catch (Exception e) {
                e.printStackTrace();
            }

        }
        catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
