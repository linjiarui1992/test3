package com.ccservice.train.mqlistener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Random;

import com.alibaba.fastjson.JSONObject;
import com.aliyun.openservices.ons.api.Action;
import com.aliyun.openservices.ons.api.ConsumeContext;
import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.MessageListener;
import com.aliyun.openservices.ons.api.ONSFactory;
import com.aliyun.openservices.ons.api.Producer;
import com.aliyun.openservices.ons.api.PropertyKeyConst;
import com.aliyun.openservices.ons.api.SendResult;
import com.ccervice.util.SendMQmsgUtil;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.thread.TrainCreateOrder;

/**
 * 下单消费者
 * @author wzc
 *
 */
public class TrainALiCreateOrderMessageListener implements MessageListener {

    private long orderid;

    private boolean isUse = false;//是否使用

    public boolean isUse() {
        return isUse;
    }

    public void setUse(boolean isUse) {
        this.isUse = isUse;
    }

    /**
     * 是否可以下单 06:00:00-07:01:30不能下单
     * @param date
     * @return
     * @time 2015年4月13日 下午9:58:34
     * @author fiend
     */
    private boolean isCanOrdering(Date date) {
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        try {
            Date dateBefor = df.parse("05:00:00");
            Date dateAfter = df.parse("06:01:30");
            Date time = df.parse(df.format(date));
            if (time.after(dateBefor) && time.before(dateAfter)) {
                WriteLog.write("开始自动下单_error", "当前时间不能下单");
                return false;
            }
        }
        catch (ParseException e) {
            WriteLog.write("开始自动下单_Exception", "获取时间出错");
        }
        return true;
    }

    @Override
    public Action consume(Message arg0, ConsumeContext arg1) {
        String messageStr = new String(arg0.getBody());
        isUse = true;
        WriteLog.write("开始自动下单", System.currentTimeMillis() + ":orderNoticeResult:" + messageStr);
        if (isCanOrdering(new Date())) {
            try {
                this.orderid = Long.valueOf(messageStr);
            }
            catch (NumberFormatException e) {
                //同程3151
                try {
                    //JSON格式
                    JSONObject json = JSONObject.parseObject(messageStr);
                    //JSON数据正确
                    if (json != null && json.containsKey("orderId")) {
                        //订单ID
                        orderid = json.getLongValue("orderId");
                        //ID正确
                        if (orderid > 0) {
                            //JSON转换
                            Customeruser user = json.getObject("customeruser", Customeruser.class);
                            //开始下单
                            new TrainCreateOrder(orderid).createOrderStart(user, true);
                        }
                        //中断返回
                        return null;
                    }
                }
                catch (Exception E) {
                    this.orderid = 0;
                }
            }
            int t = new Random().nextInt(10000);
            if (this.orderid > 0) {
                new TrainCreateOrder(this.orderid).createOrderStart(new Customeruser(), false);
            }
            WriteLog.write("12306_TrainCreateOrderMessageListener_MQ", t + ":" + arg0);
        }
        else {
            try {
                Thread.sleep(7000L);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                SendMQmsgUtil.sendGetUrlMQmsg(messageStr);
                WriteLog.write("12306_TrainCreateOrderMessageListener_MQ", messageStr + "--->重新下单");
            }
            catch (Exception e) {
                WriteLog.write("12306_TrainCreateOrderMessageListener_MQ", messageStr + "--->重新下单异常");
                try {
                    SendMQmsgUtil.sendGetUrlMQmsg(messageStr);
                    WriteLog.write("12306_TrainCreateOrderMessageListener_MQ", messageStr + "--->重新下单");
                }
                catch (Exception e1) {
                    WriteLog.write("12306_TrainCreateOrderMessageListener_MQ", messageStr + "--->重新下单异常");
                }
            }
        }
        return Action.CommitMessage;
    }

    public static void main(String[] args) {
        test();
    }

    private static void test() {
        Properties properties = new Properties();
        properties.put(PropertyKeyConst.ProducerId, "PID_createorder");
        properties.put(PropertyKeyConst.AccessKey, "nq4ka9UAhf3jEhut");
        properties.put(PropertyKeyConst.SecretKey, "shgY0ghVFiMlp9gQ9uNaDe5A2RjTLt");
        Producer producer = ONSFactory.createProducer(properties);

        //在发送消息前，必须调用start方法来启动Producer，只需调用一次即可。
        producer.start();
        Message msg = new Message(
        //Message Topic
                "createorder_Topic",
                //Message Tag,
                //可理解为Gmail中的标签，对消息进行再归类，方便Consumer指定过滤条件在ONS服务器过滤       
                "TagA",
                //Message Body
                //任何二进制形式的数据，ONS不做任何干预，需要Producer与Consumer协商好一致的序列化和反序列化方式
                "Hello ONS".getBytes());

        // 设置代表消息的业务关键属性，请尽可能全局唯一。
        // 以方便您在无法正常收到消息情况下，可通过ONS Console查询消息并补发。
        // 注意：不设置也不会影响消息正常收发
        msg.setKey("ORDERID_100");

        //发送消息，只要不抛异常就是成功
        SendResult sendResult = producer.send(msg);
        System.out.println(sendResult);

        // 在应用退出前，销毁Producer对象
        // 注意：如果不销毁也没有问题
        producer.shutdown();
        System.out.println(producer.isClosed());
        System.out.println(producer.isStarted());
        producer = null;
    }

}
