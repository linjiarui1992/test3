package com.ccservice.train.mqlistener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderchange;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.b2b2c.ben.Paymentmethod;
import com.ccservice.b2b2c.policy.TimeUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.server.Server;
import com.ccservice.util.httpclient.HttpsClientUtils;

/**
 * 改签支付处理消费者
 * @author wzc
 *
 */
public class GQTrainorderPayMessageListener extends TrainSupplyMethod implements MessageListener {
    private String orderNoticeResult;

    private long orderid;

    private long changeorderid;//改签订单ID

    private String alipayurlset;

    public GQTrainorderPayMessageListener(String alipayurlset) {
        this.alipayurlset = alipayurlset;
    }

    @Override
    public void onMessage(Message message) {
        try {
            orderNoticeResult = ((TextMessage) message).getText();
            JSONObject jsonobject = JSONObject.parseObject(orderNoticeResult);
            this.orderid = jsonobject.containsKey("orderid") ? jsonobject.getLongValue("orderid") : 0;
            this.changeorderid = jsonobject.containsKey("changeorderid") ? jsonobject.getLongValue("changeorderid") : 0;
            int type = jsonobject.containsKey("type") ? jsonobject.getIntValue("type") : 0;
            int paytype = jsonobject.containsKey("paytype") ? jsonobject.getIntValue("paytype") : 1;
            int paytimes = jsonobject.containsKey("paytimes") ? jsonobject.getIntValue("paytimes") : 0;
            int t = new Random().nextInt(10000);
            WriteLog.write("12306_GQTrainorderPayMessageListener_MQ", t + ":" + orderNoticeResult);
            if (MQMethod.ORDERPAY == type) {//订单支付
                Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(Long.valueOf(orderid));
                Trainorderchange orderchange = Server.getInstance().getTrainService()
                        .findTrainOrderChangeById(changeorderid);
                if (orderchange.getTcstatus() == Trainorderchange.CHANGEWAITPAY) {//等待出票
                    if (paytype == 1) {//支付宝支付
                        String alipayurl = "-1";
                        String i_url = alipayurlset;
                        if (orderchange.getPayflag() != null && orderchange.getPayflag().contains("alipayurl")) {
                            alipayurl = getSysconfigString(orderchange.getPayflag());
                            i_url = orderchange.getPayflag().replaceAll("alipayurl", "");
                            WriteLog.write("12306_GQTrainorderPayMessageListener_MQ", t + ":二次支付：" + alipayurl
                                    + ",支付索引：" + i_url + ",支付账户：" + orderchange.getPayflag());
                        }
                        else {
                            alipayurl = getSysconfigString("alipayurl" + alipayurlset);
                        }
                        WriteLog.write("12306_GQTrainorderPayMessageListener_MQ", t + ":支付：" + alipayurl + ",支付索引："
                                + i_url + ",支付账户：" + orderchange.getPayflag());
                        autoalipayPay(trainorder, i_url, alipayurl, t, paytimes, orderchange);
                    }
                    else if (paytype == 2) {//网银支付

                    }
                }
                else {
                    WriteLog.write("12306_GQTrainorderPayMessageListener_MQ", t + " 改签订单id:" + orderchange.getId()
                            + ":不符合支付条件,订单状态：" + orderchange.getTcstatus());
                }
            }
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Trainorderchange o = new Trainorderchange();
        o.setPayaccount("1111");
        o.setPayaddress("1111");
        o.setPayflag("1111");
        o.setSupplypaymethod(2);
        o.setSupplyprice(2.0f);
        o.setSupplytradeno("2222");
        o.setId(4);
        Server.getInstance().getTrainService().updateTrainorcerchange(o);
    }

    /**
     * 支付方法
     * @param trainorder 订单
     * @param i 支付索引
     * @param url 支付ur
     * @param t 标识
     * @param times 支付此时
     * @return
     */
    public boolean autoalipayPay(Trainorder trainorder, String i, String url, int t, int times,
            Trainorderchange orderchange) {
        boolean flag = false;
        String payurl = orderchange.getPayaddress();
        if ((payurl != null) && (!("".equals(payurl)))) {
            JSONObject jso = new JSONObject();
            jso.put("username", "trainone");
            jso.put("sign", "FF1E07242CE2C86A");
            jso.put("postdata", payurl);
            jso.put("paymode", "1");
            jso.put("servid", "1");
            jso.put("ordernum", orderchange.getTcnumber());
            jso.put("paytype", "1");
            int paycount = 0;
            try {
                paycount = Integer.valueOf(getSysconfigString("paycount"));//支付次数
                if (times > paycount) {
                    changeQuestionOrder(trainorder, "改签支付多次支付失败");
                    return flag;
                }
                if (times > 0) {
                    jso.put("RePay", "true");
                    WriteLog.write("12306_GQTrainorderPayMessageListener_MQ",
                            t + ":" + times + "次支付:" + jso.getString("RePay"));
                }
            }
            catch (Exception e) {
            }
            trainRC(trainorder.getId(), "改签开始支付订单");
            orderchange.setPayflag("alipayurl" + i);
            String supplyaccount = orderchange.getPayflag();
            WriteLog.write("12306_GQTrainorderPayMessageListener_MQ", t + ":订单号：" + trainorder.getOrdernumber()
                    + ",请求数据：" + jso.toString() + ",支付hou支付账号：" + supplyaccount);
            Server.getInstance().getTrainService().updateTrainorcerchange(orderchange);
            Map listdata = new HashMap();
            listdata.put("data", jso.toString());
            String result = HttpsClientUtils.posthttpclientdata(url, listdata, 70000l);
            WriteLog.write("12306_GQTrainorderPayMessageListener_MQ", t + ":70t返回结果：" + result);
            if ("".equals(result) || !(result.contains("status"))) {
                changeQuestionOrder(trainorder, "改签返回数据异常。");
                return flag;
            }
            if (result.contains("已经存在")) {
                String par = "{'ordernum':'" + orderchange.getTcnumber() + "','cmd':'seachliushuihao','paytype':'1'}";
                Map listdatatt = new HashMap();
                listdatatt.put("data", par);
                result = HttpsClientUtils.posthttpclientdata(url, listdatatt, 60000l);
                WriteLog.write("12306_GQTrainorderPayMessageListener_MQ", t + ",订单已存在，查询订单结果：" + result);
            }
            if (result.contains("status")) {
                JSONObject obj = JSONObject.parseObject(result);
                boolean status = obj.getBoolean("status");
                if (status) {
                    String liushuihao = obj.getString("AliTradeNo");
                    String state = "";
                    if (obj.containsKey("orderstatus")) {
                        state = obj.getString("orderstatus");
                    }
                    //查询失败成功信息
                    String msg = "";
                    if (obj.containsKey("info")) {
                        msg = obj.getString("info");
                    }
                    String AliPayUserName = "";
                    if (obj.containsKey("AliPayUserName")) {
                        AliPayUserName = obj.getString("AliPayUserName");//当前支付宝账号
                    }
                    float balance = -1;
                    String Banlancestr = "";
                    if (obj.containsKey("Banlance")) {
                        Banlancestr = obj.getString("Banlance");
                        if (Banlancestr != null && !"".equals(Banlancestr) && !"null".equals(Banlancestr)
                                && "支付成功".equals(state)) {
                            balance = Float.valueOf(Banlancestr);//当前账户余额
                        }
                    }
                    //解析银联参数成功  已经有交易号  必须返回RunOK才能确认已经支付
                    if (liushuihao != null && !"".equals(liushuihao) && liushuihao.length() > 0 && "支付成功".equals(state)) {
                        payclock(AliPayUserName, balance, t);
                        //                        String sqlpaytime = "delete from T_TRAINREFINFO where C_ORDERID=" + trainorder.getId()
                        //                                + ";insert T_TRAINREFINFO(C_ORDERID,C_PAYSUCCESSTIME) values(" + trainorder.getId()
                        //                                + ",'" + TimeUtil.gettodaydate(4) + "')";
                        String sqlpaytime = "";
                        WriteLog.write("12306_GQTrainorderPayMessageListener_MQ", t + ":" + sqlpaytime + ":"
                                + liushuihao);
                        orderchange.setSupplytradeno(liushuihao);
                        orderchange.setPayaccount(AliPayUserName);//使用的银行卡号
                        orderchange.setTcstatus(Trainorderchange.FINISHCHANGE);//已支付订单
                        orderchange.setSupplypaymethod(Paymentmethod.ALIPAY);
                        trainRC(trainorder.getId(), "改签支付成功");
                        Server.getInstance().getTrainService().updateTrainorcerchange(orderchange);
                        WriteLog.write("12306_GQTrainorderPayMessageListener_MQ", t + ":" + orderchange.getTcnumber()
                                + ":" + msg);
                        //new TrainActiveMQ(MQMethod.QUERY, trainorder.getId(), System.currentTimeMillis(),
                        //System.currentTimeMillis(), 0).sendqueryMQ();
                        WriteLog.write("12306_GQTrainorderPayMessageListener_MQ", t + ":更新结束");
                        flag = true;
                    }
                    else if ("支付失败".equals(state) && getpaycontrol(msg)) {//运行错误
                        WriteLog.write("12306_GQTrainorderPayMessageListener_MQ", t + ":更新结束");
                        new TrainpayMqMSGUtil(MQMethod.GQPayNumberUPDATE_NAME).sendGQPayMQmsg(trainorder, 1, times,
                                orderchange);
                        return flag;
                    }
                    else {
                        WriteLog.write("12306_GQTrainorderPayMessageListener_MQ", t + ":该状态下不用处理。");
                    }
                }
                else {
                    WriteLog.write("12306_GQTrainorderPayMessageListener_MQ", t + ":未支付成功，重新发送队列支付");
                    new TrainpayMqMSGUtil(MQMethod.GQPayNumberUPDATE_NAME).sendGQPayMQmsg(trainorder, 1, times,
                            orderchange);
                    return flag;
                }
            }
        }
        else {
            new TrainpayMqMSGUtil(MQMethod.GQORDERGETURL_NAME).sendGetGQUrlMQmsg(trainorder, orderchange);
            WriteLog.write("12306_GQTrainorderPayMessageListener_MQ", t + ":" + orderchange.getTcnumber()
                    + ":发送支付链接队列。");
        }
        return flag;
    }

    /**
     * 根据返回信息是否确定支付失败
     * @param info
     * @return
     */
    public boolean getpaycontrol(String info) {
        if (info != null && !"".equals(info)) {
            if ("支付失败:CASHIER_ACCESS_GAP_CONTROL_TIP".equals(info)) {
                return false;
            }
            //            else if (info.contains("登录失效")) {
            //                return false;
            //            }
        }
        return true;
    }

    /**
     * 支付预警
     */
    public void payclock(String AliPayUserName, Float balance, int t) {
        try {
            WriteLog.write("12306tz资金账户余额", t + ":" + AliPayUserName + ",余额：" + balance);
            if (balance >= 0) {
                String sqlmoney = "SELECT C_CLOCKMONEY,C_BANLANCE FROM T_PAYACCOUNTINFO WHERE C_ACCOUNT='"
                        + AliPayUserName + "'";
                List yuetixinglistt = Server.getInstance().getSystemService().findMapResultBySql(sqlmoney, null);
                float C_CLOCKMONEY = 20000;
                float C_BANLANCE = 0;
                if (yuetixinglistt.size() > 0) {
                    Map map = (Map) yuetixinglistt.get(0);
                    C_CLOCKMONEY = Float.valueOf(map.get("C_CLOCKMONEY").toString());
                    C_BANLANCE = Float.valueOf(map.get("C_BANLANCE").toString());
                    WriteLog.write("12306tz资金账户余额", t + ":支付前：" + C_BANLANCE + ",支付后：" + balance + ",提醒金额："
                            + C_CLOCKMONEY);
                    if (C_BANLANCE > C_CLOCKMONEY && balance < C_CLOCKMONEY) {
                        String content = AliPayUserName + "账户余额" + balance + ",请尽快充值。";
                        WriteLog.write("12306tz资金账户余额", t + ":" + content);
                        String tetctelphonenum = getSysconfigString("payremandtel");
                        sendSmspublic(content, tetctelphonenum, 46);
                    }
                }
                else {
                    String content = "账户数据库没找到支付账号" + AliPayUserName;
                    WriteLog.write("12306tz资金账户余额", t + ":" + content);
                    sendSmspublic(content, "15811073432", 46);
                }
                String sql = "update T_PAYACCOUNTINFO set C_BANLANCE=" + balance + ",C_LASTUPDATETIME='"
                        + TimeUtil.gettodaydate(4) + "' where C_ACCOUNT='" + AliPayUserName + "'";
                Server.getInstance().getSystemService().findMapResultBySql(sql, null);
            }
        }
        catch (Exception e) {
        }
    }

    /**
     * 变成问题订单
     * @param trainorderid
     * @param msg
     */
    public void changeQuestionOrder(Trainorder trainorder, String msg) {
        trainorder.setState12306(Integer.valueOf(7));
        trainorder.setIsquestionorder(Integer.valueOf(2));
        trainorder.setPaysupplystatus(Integer.valueOf(2));
        try {
            trainRC(trainorder.getId(), msg);
            Server.getInstance().getTrainService().updateTrainorder(trainorder);
        }
        catch (Exception e) {
        }
    }

    public void trainRC(long trainorderid, String msg) {
        Trainorderrc rc = new Trainorderrc();
        rc.setOrderid(trainorderid);
        rc.setContent(msg);
        rc.setCreateuser("自动支付");
        rc.setYwtype(1);
        Server.getInstance().getTrainService().createTrainorderrc(rc);
    }

    /**
     * 获取支付索引
     * @return
     */
    public int getPayIndex() {
        int payindex = 0;
        Map<String, String> datavale = Server.getInstance().getDateHashMap();
        if (datavale.containsKey("payindex")) {
            try {
                payindex = Integer.parseInt(datavale.get("payindex"));
            }
            catch (NumberFormatException e) {
            }
        }
        else {
            datavale.put("payindex", payindex + "");
        }
        return payindex;
    }

    /**
     * 增加索引
     * @return
     */
    public int addPayIndex() {
        int payindex = 0;
        Map<String, String> datavale = Server.getInstance().getDateHashMap();
        if (datavale.containsKey("payindex")) {
            try {
                payindex = Integer.parseInt(datavale.get("payindex"));
                if (payindex > 500) {
                    datavale.put("payindex", "0");
                }
                else {
                    payindex++;
                    datavale.put("payindex", "" + payindex);
                }
            }
            catch (NumberFormatException e) {
            }
        }
        else {
            datavale.put("payindex", payindex + "");
        }
        return payindex;
    }

    public String getAlipayurlset() {
        return alipayurlset;
    }

    public void setAlipayurlset(String alipayurlset) {
        this.alipayurlset = alipayurlset;
    }

}
