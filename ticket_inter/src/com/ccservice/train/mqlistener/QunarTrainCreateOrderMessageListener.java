package com.ccservice.train.mqlistener;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.inter.job.train.thread.CreateQunarOrder;
import com.ccservice.inter.job.train.thread.HandleQunarOrder;
import com.ccservice.train.qunar.QunarOrderMethod;

/**
 * 
 * @ClassName: QunarTrainCreateOrderMessageListener 
 * @Description: TODO(下单消费者) 
 * @author wangwei 
 * @date 2015年4月22日 下午6:44:36 
 *
 */
@SuppressWarnings("serial")
public class QunarTrainCreateOrderMessageListener extends HttpServlet {

    private String mqaddress = "";// MQ地址

    private String mqusername = "";// MQ 用户名

    private String isstart = "";

    private int getXiadanCount = 2;

    private String trainOrderChangeResult = "";// 从MQ中接收到得信息

    @Override
    public void init() throws ServletException {
        mqaddress = this.getInitParameter("Qunar_MQURL");
        //	         mqaddress = "tcp://192.168.0.5:61616";
        mqusername = this.getInitParameter("Qunar_MQName");
        //	         mqusername = "Gunar_Train_Order";
        isstart = this.getInitParameter("isstart");
        getXiadanCount = Integer.parseInt(this.getInitParameter("getXiadanCount"));
        if ("1".equals(isstart)) {
            System.out.println("去哪儿下单处理:开启");
            handleQunarOrder();
        }
    }

    public void handleQunarOrder() {
        ConnectionFactory cf = new ActiveMQConnectionFactory(mqaddress);
        Connection conn = null;
        Session session = null;
        try {
            conn = cf.createConnection();
            Destination destination = new ActiveMQQueue(mqusername);
            for (int i = 0; i < getXiadanCount; i++) {
                session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
                MessageConsumer consumer = session.createConsumer(destination);
                consumer.setMessageListener(new MessageListener() {
                    @Override
                    public void onMessage(Message message) {
                        try {
                            //int r1 = new Random().nextInt(10000000);

                            trainOrderChangeResult = ((TextMessage) message).getText();
                            QunarOrderMethod qom = new QunarOrderMethod();
                            JSONObject jsonObject = JSONObject.parseObject(trainOrderChangeResult);
                            System.out.println(jsonObject.toString());
                            if (jsonObject.containsKey("InterfaceType")) {
                                qom.setId(jsonObject.getLong("id"));
                                qom.setInterfaceType(jsonObject.getIntValue("InterfaceType"));
                                qom.setCreatetime(jsonObject.getString("Createtime"));
                                qom.setOrderjson(jsonObject.getJSONObject("Orderjson"));
                                qom.setIsquestionorder(jsonObject.getIntValue("Isquestionorder"));
                                qom.setOrderstatus(jsonObject.getIntValue("Orderstatus"));
                                qom.setQunarordernumber(jsonObject.getString("Qunarordernumber"));
                                qom.setState12306(jsonObject.getIntValue("State12306"));
                                long qunarAgentid = jsonObject.getLong("qunar_agentid");
                                String qunarPayurl = jsonObject.getString("qunarPayurl");
                                HandleQunarOrder hqo = new HandleQunarOrder(qom, qunarAgentid, qunarPayurl);
                                hqo.handleTrainOrder();
                            }
                            else {
                                long orderid = jsonObject.getLong("orderid");
                                int ordermax = jsonObject.getIntValue("ordermax");
                                String QunarCallBack = jsonObject.getString("QunarCallBack");
                                String merchantCode = jsonObject.getString("merchantCode");
                                CreateQunarOrder cqo = new CreateQunarOrder(orderid, ordermax, QunarCallBack,
                                        merchantCode);
                                cqo.createOrderRun();
                            }
                        }
                        catch (JMSException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
            conn.start();
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
    }

}
