package com.ccservice.train.mqlistener;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.job.train.thread.MyThreadGetPayURL;
import com.ccservice.inter.server.Server;

/**
 * 获取支付链接
 * @author wzc
 *
 */
public class TrainGetURLMessageListener extends TrainSupplyMethod implements MessageListener {

    private String orderNoticeResult;

    private long orderid;

    @Override
    public void onMessage(Message message) {
        try {
            int t = new Random().nextInt(10000);
            orderNoticeResult = ((TextMessage) message).getText();
            JSONObject jsonobject = JSONObject.parseObject(orderNoticeResult);
            this.orderid = jsonobject.containsKey("orderid") ? jsonobject.getLongValue("orderid") : 0;
            int type = jsonobject.containsKey("type") ? jsonobject.getIntValue("type") : 0;
            WriteLog.write("12306_TrainGetURLMessageListener_MQ", t + ":" + orderNoticeResult);
            if (MQMethod.ORDERGETURL == type) {//获取支付链接
                String getpayurltype = getSysconfigString("getpayurltype");//支付类型
                WriteLog.write("12306_TrainGetURLMessageListener_MQ", t + ":" + orderid);
                Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(Long.valueOf(orderid));
                ExecutorService pool = Executors.newFixedThreadPool(1);
                Thread thr = new MyThreadGetPayURL(trainorder, getpayurltype);
                pool.execute(thr);
                pool.shutdown();
            }
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        ExecutorService pool = Executors.newFixedThreadPool(1);
        Thread thr = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    Thread.sleep(10000);
                }
                catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                System.out.println("xiuxi10s");
            }
        });
        pool.execute(thr);
        pool.shutdown();
        System.out.println("zoule");
    }
}
