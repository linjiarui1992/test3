package com.ccservice.train.mqlistener;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.inter.job.train.JobQueryMyOrder;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.train.qunar.TrainOrderDeadlock;

public class TrainorderDeadlockListener extends TrainSupplyMethod implements MessageListener {
    private String orderNoticeResult;

    private long orderid;

    @Override
    public void onMessage(Message message) {
        try {
            this.orderNoticeResult = ((TextMessage) message).getText();
            JSONObject jsonobject = JSONObject.parseObject(orderNoticeResult);
            this.orderid = jsonobject.containsKey("orderid") ? jsonobject.getLongValue("orderid") : 0;
            new TrainOrderDeadlock(this.orderid).payTrue();
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
