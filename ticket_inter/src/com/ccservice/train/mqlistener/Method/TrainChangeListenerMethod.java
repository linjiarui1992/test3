package com.ccservice.train.mqlistener.Method;

public class TrainChangeListenerMethod {
    //mcc的key 前面加点东西避免重复 
    private static String keyName = "TrainChange=";

    //mcc锁
    private static Object mccLocker = new Object();

    /**
     * 订单正在改签
     * @param orderid
     * @param changeRetoken
     * @return
     * @author fiend
     */
    public static boolean changeing(String loginname) {
        boolean result = true;
        //KEY
        String key = keyName + loginname;
        synchronized (mccLocker) {
            //内存不存在
            //            if (MemCached.getInstance().get(key) == null) {
            //                result = false;
            //                Date date = new Date(1000 * 1 * 60 * 35);//35分鐘缓存
            //                MemCached.getInstance().add(key, "true", date);
            //            }
            String valueString = OcsMethod.getInstance().get(key);
            if (valueString == null || "".equals(valueString)) {
                result = false;
                OcsMethod.getInstance().add(key, "true", 60 * 35);
            }
        }
        return result;
    }

    /**
     * 内存中移除
     */
    public static void remove(String tradeNum) {
        //KEY
        String key = keyName + tradeNum;
        //移除
        //        MemCached.getInstance().delete(key);
        OcsMethod.getInstance().remove(key);
    }

    public static void main(String[] args) {
        String key = keyName + "yangpiityz";
        System.out.println((MemCached.getInstance().get(key) == null ? "11111" : MemCached.getInstance().get(key)));
    }

}
