package com.ccservice.train.mqlistener;

import javax.jms.JMSException;

import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.policy.SendPostandGet;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.server.Server;
import com.ccservice.rabbitmq.util.PropertyUtil;

public class TrainpayMqMSGUtilNew extends TrainSupplyMethod {

    /**
     * 发送
     * @param order 订单
     * @param urltype 1 标识获取链接并去支付   0 只获取链接
     * @throws JMSException 
     */
    public void sendPayMQmsgByUrltype(long orderid, long urltype) throws JMSException {
        try {
            //订单状态修改为等待出票
            String updateSql = "update T_TRAINORDER set C_ORDERSTATUS = 2, C_CONFIRMTIME = GETDATE() where ID="
                    + orderid;
            Server.getInstance().getSystemService().findMapResultBySql(updateSql, null);
            WriteLog.write("TB抢票支付测试_MQ_sendOrderPayMQmsgnew", "begin;orderId:" + orderid);
            String url = PropertyUtil.getValue("Bespeak_sendPayOrderId_url", "train.properties");
            WriteLog.write("TB抢票支付测试_MQ_sendOrderPayMQmsgnew", "url:" + url + ";orderId:" + orderid);
            String result = SendPostandGet.submitPost(url, "orderId=" + orderid, "UTF-8").toString();
            WriteLog.write("TB抢票支付测试_MQ_sendOrderPayMQmsgnew", ";orderId:" + orderid + ";result:" + result);
            writeRC(orderid, "开始获取链接", "自动支付", 2, 1);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
