package com.ccservice.accountSwitchJob;

import com.ccservice.Util.db.DBHelper;
import com.ccservice.Util.db.DataRow;
import com.ccservice.Util.db.DataTable;
import com.ccservice.Util.file.ExceptionUtil;
import com.ccservice.Util.file.WriteLog;

public class AccountSwitchLogic {

	public void work() {
		boolean accountSwitch  = false;
    	String sql = "GetAccountSwitch";
    	try {
			DataTable dataTable = DBHelper.GetDataTable(sql);
			DataRow dataRow = dataTable.GetRow().get(0);
			String accountSwitchStr = dataRow.GetColumnString("C_VALUE");
			if (!"".equals(accountSwitchStr) && accountSwitchStr != null) {
				 accountSwitch = Integer.valueOf(accountSwitchStr) == 1?true:false;
			}
			AccountSwitch.setAccountSwitch(accountSwitch);
		} catch (Exception e) {
			ExceptionUtil.writelogByException("获取账号开关状态", e, sql);
		}
    	WriteLog.write("定时刷新内存状态", "AccountSwitch>>"+AccountSwitch.isAccountSwitch());
	}

}
