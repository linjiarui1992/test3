/**
 * 
 */
package com.ccservice.service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang.ArrayUtils;

import com.ccervice.util.Util;
import com.ccservice.b2b2c.base.eaccount.Eaccount;
import com.ccservice.b2b2c.base.orderinfo.Orderinfo;
import com.ccservice.b2b2c.base.orderinforc.Orderinforc;
import com.ccservice.b2b2c.base.passenger.Passenger;
import com.ccservice.b2b2c.base.segmentinfo.Segmentinfo;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.FiveoneBookutil;
import com.ccservice.b2b2c.policy.Method12580;
import com.ccservice.b2b2c.policy.QunarBestDataV2;
import com.ccservice.b2b2c.policy.QunarBestPolicy;
import com.ccservice.b2b2c.policy.SupplyMethod;
import com.ccservice.b2b2c.policy.TianQuPolicy;
import com.ccservice.b2b2c.policy.UtilMethod;
import com.ccservice.b2b2c.policy.YeeXingMethod;
import com.ccservice.b2b2c.policy.YeebookingMethod;
import com.ccservice.b2b2c.policy.Yi9eMethod;
import com.ccservice.b2b2c.policy.Bartour.BartourMethod;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;

/**
 * 
 * @time 2016年6月17日 下午8:26:47
 * @author chendong
 */
public class AutoPayMethod extends SupplyMethod {
    public static void main(String[] args) {
        AutoPayMethod autoPayMethod = new AutoPayMethod();
        //        System.out.println(java.lang.System.getProperty("java.version"));
        Server.getInstance().setIsCreateExtOrder("1");
        Server.getInstance().setIsAutoPay("1");
        Orderinfo order = new Orderinfo();
        String strExtOrderNumber = "S|112016062080141312|https://mapi.alipay.com/gateway.do?body=%E6%9C%BA%E7%A5%A8%E9%87%87%E8%B4%AD++HFQ5E5%E8%88%AA%E7%8F%AD%3AMU5990+%E8%88%AA%E7%A8%8B%3A+ZAT-KMG%E4%B9%98%E6%9C%BA%E4%BA%BA%3A%E4%BD%99%E5%AE%81%2C%E6%9B%BE%E6%98%8E%28%E8%AE%A2%E5%8D%95%E5%8F%B7%3A112016062080141312%29%E6%93%8D%E4%BD%9C%E5%91%98%3A%E8%88%AA%E5%A4%A9%E5%8D%8E%E6%9C%89%E7%BD%91%E7%BB%9C%E9%87%87%E8%B4%AD%EF%BC%9AHTHYWL+%E5%87%BA%E7%A5%A8%E4%B8%AD%E5%BF%83%E8%AE%A2%E5%8D%95%E5%8F%B7%EF%BC%9A&subject=%E6%9C%BA%E7%A5%A8&notify_url=http%3A%2F%2Fpay.51ebill.com%2Fpayment%2Fpay%2FalipayPaynotify_1.in%3FlogId%3D199250227&out_trade_no=112016062080141312&return_url=http%3A%2F%2Falipay.solaridc.com%3A8000%2Fliantuo%2Fmanage%2FpolicyOrderPaidReturn.in&credit_card_pay=Y&_input_charset=utf-8&total_fee=1264.0&credit_card_default_display=Y&service=create_direct_pay_by_user&paymethod=directPay&partner=2088201994709221&seller_email=zfblt%40126.com&payment_type=1&sign=ec46465fed7d43dc89b34129df626e31&sign_type=MD5|SHA255|1264.0";
        order = autoPayMethod.updateExtOrder(order, strExtOrderNumber);
        //        RateService rateService = new RateService();
        //        Long orderId = 429343L;
        //！！！！！！！！！！！！！！【数据组织】！！！！！！！！！！！！！！
        //        Orderinfo order = Server.getInstance().getAirService().findOrderinfo(orderId);
        //        String segmentSql = "select * from T_SEGMENTINFO with(nolock) where C_ORDERID=" + orderId;
        //        List<Segmentinfo> sinfos = Server.getInstance().getAirService().findAllSegmentinfoBySql(segmentSql, -1, 0);
        //        Segmentinfo segmentOne = sinfos.get(0);
        //        String passengerSql = "select * from T_PASSENGER with(nolock) where C_ORDERID=" + orderId;
        //        List<Passenger> listPassenger = Server.getInstance().getAirService().findAllPassengerBySql(passengerSql, -1, 0);
        //        order.setPassengerlist(listPassenger);
        //        order.setSegmentlist(sinfos);
        //        Long zrateid = segmentOne.getZrateid();
        //        Zrate zreate = Server.getInstance().getAirService().findZrate(zrateid);
        //        segmentOne.setZrate(zreate);
        //！！！！！！！！！！！！！！【数据组织完了】！！！！！！！！！！！！！！
        //        order = rateService.autoPayBeforeCreateOrder(order, sinfos, listPassenger);
        //        Orderinfo order = Server.getInstance().getAirService().findOrderinfo(429810);
        //        new AutoPayMethod().autopay(order);

    }

    /**
     * 
     * @time 2016年6月17日 下午8:27:29
     * @author chendong
     */
    public String autopay(Orderinfo order) {
        //        sendSmsbyOrderOnpay(order);
        long aid = order.getPolicyagentid();
        long tq8LPolicyAgentId = 0;
        try {
            tq8LPolicyAgentId = Long.parseLong(PropertyUtil.getValue("tq8LPolicyAgentId", "air.properties"));
        }
        catch (Exception e) {
        }
        String isVirtualPnrAll = getisVirtualPnrAll();//【都不创建外部订单】
        String isAutoPay = Server.getInstance().getIsAutoPay();

        Long orderId = order.getId();
        Segmentinfo segmentOne = new Segmentinfo();
        WriteLog.write("自动代扣", orderId + ":isVirtualPnrAll:" + isVirtualPnrAll + ";aid:" + aid);
        //        [2016-06-17 19:59:33.440] 429345:isVirtualPnrAll:true;aid:13
        //        [2016-06-17 19:59:33.443] 429345:isAutoPay:1;agentOutId:13;orderOutId:null
        //        [2016-06-17 19:59:33.445] 429345:Isspecial:null;pnr:TSSUKP;patNUM:1;totalprice:967.0;extOrderprice:null;NameIsRight:
        //！！！！！！！！！！！！！！【数据组织】！！！！！！！！！！！！！！
        order = Server.getInstance().getAirService().findOrderinfo(orderId);
        String segmentSql = "select * from T_SEGMENTINFO with(nolock) where C_ORDERID=" + orderId;
        List<Segmentinfo> sinfos = Server.getInstance().getAirService().findAllSegmentinfoBySql(segmentSql, -1, 0);
        segmentOne = sinfos.get(0);
        String passengerSql = "select * from T_PASSENGER with(nolock) where C_ORDERID=" + orderId;
        List<Passenger> listPassenger = Server.getInstance().getAirService()
                .findAllPassengerBySql(passengerSql, -1, 0);
        order.setPassengerlist(listPassenger);
        order.setSegmentlist(sinfos);
        Long zrateid = segmentOne.getZrateid();
        Zrate zreate = Server.getInstance().getAirService().findZrate(zrateid);
        segmentOne.setZrate(zreate);
        //！！！！！！！！！！！！！！【数据组织完了】！！！！！！！！！！！！！！
        if (("1".equals(isVirtualPnrAll) && (aid == 5 || aid == 13))) {
            order = autoPayBeforeCreateOrder(order, sinfos, listPassenger);//创建编码
        }
        String result = "-1";
        // 如果是0的话就是在易订行下单那么就把支付信息通知给易订行
        //|| (order.getPolicyagentid() != null && order.getPolicyagentid() == 16
        if ("1".equals(isAutoPay)) {
            order = Server.getInstance().getAirService().findOrderinfo(orderId);
            Float extOrderPrice = order.getExtorderprice(); // 外部订单实际金额
            String extorderid = order.getExtorderid(); // 外部订单id
            int patNUM = 1;
            patNUM = getpatNUM(order.getPnrpatinfo());
            Double totalprice = getPayPrice(order.getId());//客户支付的金额
            WriteLog.write("自动代扣", orderId + ":isAutoPay:" + isAutoPay + ";agentOutId:" + aid + ";orderOutId:"
                    + extorderid);
            WriteLog.write("自动代扣", orderId + ":Isspecial:" + segmentOne.getIsspecial() + ";pnr:" + order.getPnr()
                    + ";patNUM:" + patNUM + ";totalprice:" + totalprice + ";extOrderprice:" + extOrderPrice
                    + ";NameIsRight:" + order.getNameIsRight());
            //            [2016-06-20 12:46:32.962] 429813:isVirtualPnrAll:1;aid:5
            //            [2016-06-20 12:46:40.547] 429813:isAutoPay:1;agentOutId:5;orderOutId:null
            //            [2016-06-20 12:46:40.547] 429813:Isspecial:1;pnr:HFQ5E5;patNUM:1;totalprice:1272.0;extOrderprice:null;NameIsRight:
            if (extOrderPrice == null || extorderid == null) {
                createOrderInfoRc(orderId, "自动代扣失败,价格或者订单号为空", 0L, 0, 1);
                return result;
            }
            else if (totalprice < extOrderPrice) {//价格不对
                createOrderInfoRc(orderId, "自动代扣失败,客户支付价格比供应价格低", 0L, 0, 1);
            }
            else if (((order.getNameIsRight() == null) || (order.getNameIsRight().length() == 0))) { // 普通政策&&名字没有错误的&&自动代扣开启
                if (aid == 2) {// 票盟
                    //                    result = piaomeng.payOut(order);
                }
                if (aid == 3) {// 8000yi
                    //                    result = ariutilNew40.AutomatismPay(extorderid);
                }
                if (aid == 4) {// 易行天下
                    result = YeeXingMethod.autoPay(extorderid, extOrderPrice + "", 1 + "");
                }
                if (aid == 5) {
                    FiveoneBookutil fiveoneBookUtil = new FiveoneBookutil();
                    result = fiveoneBookUtil.autoPayOrder(extorderid, order.getOrdernumber(), "0", "1");
                }
                if (aid == 6) {// 今日
                    //                    result = jinriMethod.AutoPayOrder(order);
                }
                if (aid == 7) {// 517na
                    //                    result = iwanttogotowhichMethod.payOrder(extOrderPrice + "", extorderid + "");
                }
                if (aid == 8) {// 纵横天地
                    //result = zonghengtiandimethod.autoPayOrder(extorderid, order.getId() + "");
                }
                if (aid == 9) {// KKKK
                    //result = kkkkMethod.autoPayOrder(extorderid,order.getPnr(), order.getOrdernumber(), "1");
                }
                if (aid == 10) {// 百拓
                    
                }
                if (aid == 13) {// 19e
                    result = Yi9eMethod.autopay(extorderid, extOrderPrice + "");
                }
                if (aid == 15) {//qunar优选
                    result = QunarBestPolicy.payqnorder(order);
                }
                if (aid == 17) {//qunar优选
                    result = QunarBestPolicy.payqnorder(order);
                }
                if (aid == 16) {//天衢自动支付
                   
                    //                    result = TianQuPolicy.payorder(order);
                    //                }
                    //                if (aid == tq8LPolicyAgentId) {//如果供应商agentid和配置文件里的一样说明这个是8L下单到天衢
                    result = TianQuPolicy.autoPayOrder8L(order);//【8L】自动通知到天衢支付
                }
                if ("S".equals(result.substring(0, 1))) {
                    String sql = "UPDATE T_ORDERINFO SET C_UCODE=1," + Orderinfo.COL_extorderstatus + "='2' WHERE ID="
                            + order.getId();
                    Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                    createOrderInfoRc(order.getId(), "出票方跟进订单-自动支付供应成功", order.getUserid(), 2, 1);
                    createOrderInfoRc(order.getId(), "出票方跟进订单-系统自动跟进待出票订单", order.getUserid(), 2, 0);
                }
            }
        }
        else if (Server.getInstance().getIsAutoPay().equals("0")) {
            String yeebookingOrdernum = "";
            String sql2 = "SELECT C_TRIPNOTE AS YEEORDERNUM FROM T_ORDERINFO WHERE ID=" + order.getId();
            List YEEORDERNUMs = Server.getInstance().getSystemService().findMapResultBySql(sql2, null);
            if (YEEORDERNUMs.size() > 0) {
                Map m = (Map) YEEORDERNUMs.get(0);
                if (m.get("YEEORDERNUM") != null) {
                    yeebookingOrdernum = m.get("YEEORDERNUM").toString();
                }
            }

            Eaccount eaccount = Server.getInstance().getSystemService().findEaccount(20);
            String url = eaccount.getUrl();
            String ordernum = "";
            List<Orderinforc> list = Server.getInstance().getAirService()
                    .findAllOrderinforc("WHERE C_STATE=888 AND C_ORDERINFOID=" + order.getId(), "", -1, 0);
            String payrc = "";
            if (list.size() > 0) {
                try {
                    payrc = URLEncoder.encode(list.get(0).getContent(), "UTF-8");
                }
                catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            String strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><CCS-informpay><Account username=\""
                    + eaccount.getUsername() + "\" password=\"" + eaccount.getPassword()
                    + "\" cmd=\"UPDATEPAYSTATUS\" ordernum=\"" + order.getExtorderid() + "\" yeebookingOrdernum=\""
                    + yeebookingOrdernum + "\" orderinfopayrc=\"" + payrc + "\"></Account></CCS-informpay>";
            WriteLog.write("通知YDX已支付", "0:" + strXML);
            result = "通知易订行返回的结果:" + submitPost(url, strXML);
            WriteLog.write("通知YDX已支付", "1:" + result);
            if (result.equals("SUCCESS")) {
                // 如果成功锁定订单,跟进订单,并且修改该订单的供应商状态为等待出票
                String sql = "UPDATE T_ORDERINFO SET C_UCODE=1,C_EXTORDERSTATUS=2 WHERE ID=" + order.getId();
                Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                createOrderInfoRc(order.getId(), "出票方跟进订单-系统自动跟进待出票订单", 46L, 2, 0);// 插入操作记录
            }
            // 如果是1的话就是在本地下单并支付到供应
        }
        else if (Server.getInstance().getIsAutoPay().equals("2")) {
        }
        WriteLog.write("自动代扣", orderId + ":返回的信息:" + result);
        return result;

    }

    /**
     * 根据订单id获取客户支付的金额
     * @param orderId
     * @return
     * @time 2016年6月23日 下午12:10:30
     * @author chendong
     */
    private Double getPayPrice(long orderId) {
        String procedure = "sp_T_AirticketPaymentrecordSelectPayPriceByYewuTypeAndOrderId @ywType =1, @orderId="
                + orderId;
        List list = Server.getInstance().getSystemService().findMapResultByProcedure(procedure);
        //        order.getTotalfuelfee() + order.getTotalairportfee() + order.getTotalticketprice();//客户支付的钱
        Double totalPrice = 0D;
        if (list.size() > 0) {
            Map map = (Map) list.get(0);
            try {
                totalPrice = Double.parseDouble(objectisnull(map.get("totalPrice")));
            }
            catch (Exception e) {
            }
        }
        return totalPrice;
    }

    /**
     * 
     * @time 2016年6月16日 上午11:52:05
     * @author chendong
     * @param order 
     */
    private Orderinfo autoPayBeforeCreateOrder(Orderinfo order, List<Segmentinfo> sinfos, List<Passenger> listPassenger) {
        String autoPayCreatePnr = getSysConfigByProcedure("AutoPayCreatePnr");
        if ("1".equals(autoPayCreatePnr)) {//机票自动支付的时候是否创建pnr
            autoPayCreatePnr(order, sinfos, listPassenger);
        }
        Segmentinfo sinfo = sinfos.get(0);
        //        S|112016061680039590|
        //        https://mapi.alipay.com/gateway.do?body=%E6%9C%BA%E7%A5%A8&subject=%E6%96%B0%E6%8E%A5%E5%8F%A3%3A%E6%9C%BA%E7%A5%A8%E9%87%87%E8%B4%AD++TSQJV0%E8%88%AA%E7%8F%AD%3ACA1325+%E8%88%AA%E7%A8%8B%3A+PEK-CGO%E4%B9%98%E6%9C%BA%E4%BA%BA%3A%E5%BC%A0%E4%BB%81%E5%8F%8C%28%E8%AE%A2%E5%8D%95%E5%8F%B7%3A112016061680039590%29%E6%93%8D%E4%BD%9C%E5%91%98%3A%E8%88%AA%E5%A4%A9%E5%8D%8E%E6%9C%89%E7%BD%91%E7%BB%9C%E9%87%87%E8%B4%AD%EF%BC%9AHTHYWL+%E5%87%BA%E7%A5%A8%E4%B8%AD%E5%BF%83%E8%AE%A2%E5%8D%95%E5%8F%B7%EF%BC%9A&notify_url=http%3A%2F%2Fpay.51ebill.com%2Fpayment%2Fpay%2FalipayPaynotify_1.in%3FlogId%3D198875276&out_trade_no=112016061680039590&return_url=http%3A%2F%2Falipay.solaridc.com%3A8000%2Fliantuo%2Fmanage%2FpolicyOrderPaidReturn.in&credit_card_pay=Y&_input_charset=utf-8&total_fee=1303.0&credit_card_default_display=Y&service=create_direct_pay_by_user&paymethod=directPay&partner=2088201994709221&seller_email=zfbltjs%40126.com&payment_type=1&sign=958a1b5aaf6c75b700ec713848b41a47&sign_type=MD5
        //        |PEK506|1303.0
        String strExtOrderNumber = CreateOrder(order, sinfo, listPassenger);//下单到供应商的结果
        WriteLog.write("RateService-autoPayBeforeCreateOrder", order.getId() + ":strExtOrderNumber:"
                + strExtOrderNumber);
        order = updateExtOrder(order, strExtOrderNumber);
        return order;
    }

    /**
     * 
     * @param order
     * @param strExtOrderNumber
     * @time 2016年6月16日 下午4:41:46
     * @author chendong
     */
    private Orderinfo updateExtOrder(Orderinfo order, String strExtOrderNumber) {
        //        [2016-06-20 12:46:40.528] 429813:strExtOrderNumber:S|112016062080141312|https://mapi.alipay.com/gateway.do?body=%E6%9C%BA%E7%A5%A8%E9%87%87%E8%B4%AD++HFQ5E5%E8%88%AA%E7%8F%AD%3AMU5990+%E8%88%AA%E7%A8%8B%3A+ZAT-KMG%E4%B9%98%E6%9C%BA%E4%BA%BA%3A%E4%BD%99%E5%AE%81%2C%E6%9B%BE%E6%98%8E%28%E8%AE%A2%E5%8D%95%E5%8F%B7%3A112016062080141312%29%E6%93%8D%E4%BD%9C%E5%91%98%3A%E8%88%AA%E5%A4%A9%E5%8D%8E%E6%9C%89%E7%BD%91%E7%BB%9C%E9%87%87%E8%B4%AD%EF%BC%9AHTHYWL+%E5%87%BA%E7%A5%A8%E4%B8%AD%E5%BF%83%E8%AE%A2%E5%8D%95%E5%8F%B7%EF%BC%9A&subject=%E6%9C%BA%E7%A5%A8&notify_url=http%3A%2F%2Fpay.51ebill.com%2Fpayment%2Fpay%2FalipayPaynotify_1.in%3FlogId%3D199250227&out_trade_no=112016062080141312&return_url=http%3A%2F%2Falipay.solaridc.com%3A8000%2Fliantuo%2Fmanage%2FpolicyOrderPaidReturn.in&credit_card_pay=Y&_input_charset=utf-8&total_fee=1264.0&credit_card_default_display=Y&service=create_direct_pay_by_user&paymethod=directPay&partner=2088201994709221&seller_email=zfblt%40126.com&payment_type=1&sign=ec46465fed7d43dc89b34129df626e31&sign_type=MD5|SHA255|1264.0
        if (strExtOrderNumber.indexOf("|") > 0) {// 外部订单号
            String[] strExtOrderArr = strExtOrderNumber.split("[|]");
            String createIsSuccess = strExtOrderArr[0];
            // 外部订单id
            String extorderid = strExtOrderArr.length > 1 ? strExtOrderArr[1] : "";
            String paymenturl = strExtOrderArr.length > 2 ? strExtOrderArr[2] : "";
            String strAgentOfficeNo = strExtOrderArr.length > 3 ? strExtOrderArr[3].trim() : "";
            // 外部订单实际金额
            Float extorderprice = Float.parseFloat(strExtOrderArr.length > 4 ? strExtOrderArr[4] : "0");
            if ("S".equals(createIsSuccess)) {
                order.setExtorderid(extorderid);
                order.setPaymenturl(paymenturl);
                order.setExtorderprice(extorderprice);
                if (strAgentOfficeNo.length() == 6) {
                    // 是否授权
                    //                              String strAuthInfo = Server.getInstance().getTicketSearchService().AUTHpnr(s_returnpnr, strAgentOfficeNo);
                    //                              WriteLog.write("授权","pnr" + s_returnpnr);
                    //                              WriteLog.write("授权",strAuthInfo);
                    //下单后把要授权的office号保存起来等用户支付后再授权,减少流量开支
                    order.setPostcode(strAgentOfficeNo);
                }
                createOrderInfoRc(order.getId(), "创建供应商订单成功:" + extorderid + ":" + extorderprice, 0L, 0, 1);
                Server.getInstance().getAirService().updateOrderinfo(order);
            }
            else {
                createOrderInfoRc(order.getId(), "创建供应商订单失败:" + extorderid, 0L, 0, 1);
            }
        }
        return order;
    }

    /**
     * 
     * @param order
     * @param sinfos
     * @param listPassenger
     * @time 2016年6月16日 下午12:33:34
     * @author chendong
     */
    private void autoPayCreatePnr(Orderinfo order, List<Segmentinfo> sinfos, List<Passenger> listPassenger) {
        String newpnr = order.getNewpnr();
        String s_returnpnr = Server.getInstance().getTicketSearchService()
                .CreatePNRByCmd(sinfos, listPassenger, newpnr);//创建编码的方法
        createOrderInfoRc(order.getId(), order.getPnr() + ":创建新编码:" + s_returnpnr, 0L, 0, 1);
        if (!"123456".equals(s_returnpnr) && !"NOPNR".equals(s_returnpnr)) {
            if (s_returnpnr.length() == 6) {
                try {
                    WriteLog.write("RateService-autoPayCreatePnr", order.getId() + ":s_returnpnr:" + s_returnpnr);
                    String isVirtualPnr = "1";

                    order.setPnr(s_returnpnr);
                    order = checkPatprice(s_returnpnr, order, isVirtualPnr, listPassenger, sinfos);
                    if (order.getUserRtInfo() != null) {
                        createOrderInfoRc(order.getId(), order.getUserRtInfo(), 0L, 0, 1);
                    }
                    if (order.getPnrpatinfo() != null) {
                        createOrderInfoRc(order.getId(), order.getPnrpatinfo(), 0L, 0, 1);
                    }
                    Server.getInstance().getAirService().updateOrderinfo(order);
                    //                    orderinfo.setBigpnr(AirUtil.getBigPnrbyRT(orderinfo.getUserRtInfo()));
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        else {
            createOrderInfoRc(order.getId(), order.getPnr() + ":创建新编码失败", 0L, 0, 1);
        }
    }

    /**
     * 检查pat价格是否正确
     * @param s_returnpnr
     * @param listpassenger 请确保此对象为orderinfonew里面的passengerList
     * @param listsegmenginfo
     * @param orderinfonew
     * @author chendong 
     * @param isVirtualPnr 
     * @param listPassenger 
     * @param listsegmentinfo 
     */
    public Orderinfo checkPatprice(String s_returnpnr, Orderinfo orderinfonew, String isVirtualPnr,
            List<Passenger> listPassenger, List<Segmentinfo> listsegmentinfo) {
        Segmentinfo seginfo = listsegmentinfo.get(0);
        String pat_Price = "0";// 黑屏PAT票价，燃油，机建
        String pat_Fuleprice = "0";//燃油
        String pat_airportfee = "0";//基建
        String pnrrtString = "";
        String pnrpatString = "";
        String ptype = "1";
        try {
            try {
                // 儿童订单，则使用pat:*ch
                if (listPassenger.get(0).getPtype() == 2) {
                    ptype = "2";
                }
                pnrrtString = Server.getInstance().getTicketSearchService().getRealRT(s_returnpnr, ptype);
                pnrpatString = Server.getInstance().getTicketSearchService().getRealPat(s_returnpnr, ptype);
                WriteLog.write("RateService-checkPatprice", s_returnpnr + ":rt-pat:" + pnrpatString + ":ptype:" + ptype
                        + "=:" + s_returnpnr + "结果:" + pnrrtString);
                pnrrtString = Util.formatPNRHTML(pnrrtString);
                pnrpatString = Util.formatPNRHTML(pnrpatString);
                WriteLog.write("RateService-checkPatprice", s_returnpnr + ":rt-pat:" + pnrpatString + ":ptype:" + ptype
                        + "=:" + s_returnpnr + "结果:" + pnrrtString);
                orderinfonew.setUserRtInfo(pnrrtString);
                orderinfonew.setPnrpatinfo(pnrpatString.trim());
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            String formatPatString = Util.getFormatpat(pnrpatString);
            try {
                // 如果在页面查询到的价格和pat有一样的价格取和页面一样的价格，如果没有取黑屏最低的价格
                String[] prices = Util.getTruePrice(formatPatString, seginfo.getParvalue(), seginfo.getAirportfee(),
                        seginfo.getFuelfee());
                WriteLog.write("RateService-checkPatprice", s_returnpnr + ":prices:" + ArrayUtils.toString(prices)
                        + ":formatPatString:" + formatPatString);
                pat_Price = prices[0];
                pat_airportfee = prices[1];
                pat_Fuleprice = prices[2];
            }
            catch (Exception ex) {
                ex.printStackTrace();
                pat_Price = seginfo.getParvalue() + "";
                pat_airportfee = seginfo.getAirportfee() + "";
                pat_Fuleprice = seginfo.getFuelfee() + "";
            }
            // 核对黑屏中价格信息与航程中价格信息是否一样
            Float f_segmentprice = 0f;
            Float f_segmentfuelprice = 0f;
            Float f_segmentairportfee = 0f;
            f_segmentprice = Util.getFloatByString(pat_Price);//Float.parseFloat(pat_Price);
            if ("2".equals(ptype)) {//如果是儿童的加上儿童的手续费到票面价
                Float Floatchildcurrplatfee = Util.getChildcurrplatfee();
                f_segmentprice += Floatchildcurrplatfee;
            }
            f_segmentfuelprice = Util.getFloatByString(pat_Fuleprice);
            f_segmentairportfee = Util.getFloatByString(pat_airportfee);
            if (f_segmentprice.floatValue() != listsegmentinfo.get(0).getParvalue()
                    || f_segmentfuelprice.floatValue() != listsegmentinfo.get(0).getFuelfee()
                    || f_segmentairportfee.floatValue() != listsegmentinfo.get(0).getAirportfee()) {
                float sub_price = 0;
                float sub_fuelfee = 0;
                float sub_airportfee = 0;
                for (int i = 0; i < listsegmentinfo.size(); i++) {
                    // 修改航程信息中的价格信息
                    listsegmentinfo.get(i).setParvalue(f_segmentprice);
                    listsegmentinfo.get(i).setFuelfee(f_segmentfuelprice);
                    listsegmentinfo.get(i).setAirportfee(f_segmentairportfee);
                    listsegmentinfo.get(i).setDiscount(
                            Util.getdiscount(listsegmentinfo.get(i).getYprice(), f_segmentprice, listsegmentinfo.get(i)
                                    .getDiscount()));
                }
                for (Passenger passenger : listPassenger) {
                    passenger.setAirportfee(f_segmentairportfee);
                    passenger.setFuelprice(f_segmentfuelprice);
                    passenger.setPrice(f_segmentprice);
                    sub_price += passenger.getPrice();
                    sub_fuelfee += passenger.getFuelprice();
                    sub_airportfee += passenger.getAirportfee();
                }
                // 修改订单中的价格信息
                // 机建费
                orderinfonew.setTotalairportfee(sub_airportfee);
                // 燃油费
                orderinfonew.setTotalfuelfee(sub_fuelfee);
                // 总机票价格+平台费用
                orderinfonew.setTotalticketprice(sub_price);
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        return orderinfonew;
    }

    /**
     * 
     * @return
     * @time 2016年6月17日 下午8:34:05
     * @author chendong
     */
    public String CreateOrder(Orderinfo order, Segmentinfo sinfo, List<Passenger> listPassenger) {
        String result = "-1";
        int tempint = new Random().nextInt(1000);
        WriteLog.write("CreateOrder", "CreateOrder" + tempint + ":0:" + order.getPnr());
        try {
            // 以下定义为非常B2C下单需要参数
            // String B2cWebPrice = order.getB2cprofit();
            // String WebPayPrice = order.getCclientpayprice();
            // Passenger passenger = listPassenger.get(0);
            int IsWhy = order.getIspayhthy();// 0不分润 1分润
            Integer ptype = listPassenger.get(0).getPtype();//乘机人类型   1 成人    2 儿童  3 婴儿
            long policyagentid = order.getPolicyagentid();// 政策供应商
            String Outid = sinfo.getZrate().getOutid();//这个id是供应商真正的id,
            WriteLog.write("CreateOrder",
                    order.getId() + ":ticket_inter:加盟商ID:" + policyagentid + ",PNR:" + order.getPnr() + ",政策ID:"
                            + order.getPolicyid() + ",外部政策ID:" + Outid + ",Ispayhthy:" + IsWhy + ",创建PNR:"
                            + Server.getInstance().getCreatePnr());
            //授权信息,授权信息原来是在各个下单到接口的具体方法里面,
            //这里改到这里写这儿是为了不同的下单到易订行的接口用户使用黑屏可能和易订行的不同,可能会导致授权失败。
            //此方法是在接口用户的服务器上执行
            AUTHpnr(policyagentid, order.getPnr());
            if ("1".equals(Server.getInstance().getIsCreateExtOrder())) {// 如果isCreateExtOrder为1,表示生成外部订单（第三方供应商订单）
                if (policyagentid == 2) {// 票盟
                    Zrate zrate = sinfo.getZrate();
                    //                    result = CreatePiaomentOrder(order, zrate, sinfo, listPassenger);
                }
                else if (policyagentid == 3) {// 8000yi政策
                    Zrate zrate = sinfo.getZrate();
                    //                    result = Create8000YiOrder(order, zrate, sinfo, listPassenger, ptype);
                }
                else if (policyagentid == 4) {// 易行天下
                    //                    result = yeexingCreateOrder();
                }
                else if (policyagentid == 5) {// 51book
                    result = Create51bookOrder(order, sinfo, listPassenger, ptype, IsWhy, Outid);
                }
                else if (policyagentid == 6) { // 今日政策订单
                    //                    Zrate zrate = sinfo.getZrate();
                    //                                        result = CreatejinriOrder(order, zrate, sinfo, listPassenger, ptype, IsWhy);
                }
                else if (policyagentid == 7) {// 517NA
                    //                    createOrder517();
                }
                else if (policyagentid == 8) {// 纵横天地
                    //                    zhtdCreateOrder();
                }
                else if (policyagentid == 9) {// KKKK
                    //                    kkkkCreateorder();
                }
                else if (policyagentid == 10) {// 百拓
                    result = CreateBarTourOrder(order, sinfo, listPassenger, ptype, IsWhy, Outid);
                }
                else if (policyagentid == 11) {// 12580
                    String policyId = sinfo.getZrate().getOutid();
                    result = Method12580.createorder(order, sinfo, listPassenger);
                }
                else if (policyagentid == 13) {// 19e
                    result = Yi9eMethod.createOrderbyPnrNew(order, sinfo);
                }
                else if (policyagentid == 15) {//qunar优选
                    result = QunarBestPolicy.Qunarcreateorderlowprice(order, sinfo, listPassenger);
                }
                else if (policyagentid == 16) {
                    //                    result = yeebookingMethod.createOrder(order, sinfo, listPassenger);
                }
                else if (policyagentid == 17) {
                    result = QunarBestDataV2.Qunarcreateorder(order, sinfo, listPassenger);
                }
            }
            else if (Server.getInstance().getIsCreateExtOrder().equals("0")) {
                YeebookingMethod yeebookingMethod = new YeebookingMethod();
                result = yeebookingMethod.createOrder(order, sinfo, listPassenger);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            UtilMethod.writeEx("RateService", e);
        }
        WriteLog.write("CreateOrder", order.getId() + ":CreateOrder" + tempint + ":1:" + result);
        return result;
    }

    private String CreateBarTourOrder(Orderinfo order, Segmentinfo sinfo, List<Passenger> listPassenger, Integer ptype,
            int isWhy, String outid) {
        String agentCode = "B2B_073104";
        String agentUserName = "hthy";
        String agentPwd = "hthy";
        BartourMethod bartourMethod = new BartourMethod(agentCode, agentUserName, agentPwd);
        return bartourMethod.DetailCreateOrderMethod(order, sinfo, listPassenger);
    }

    /**
     * 
     * @param order
     * @param sinfo
     * @param listPassenger
     * @param ptype
     * @return
     * @time 2016年5月26日 下午1:44:29
     * @author chendong
     * @param isWhy 
     * @param outid2 
     */
    private String Create51bookOrder(Orderinfo order, Segmentinfo sinfo, List<Passenger> listPassenger, Integer ptype,
            int isWhy, String outid) {
        String result = "-1";
        FiveoneBookutil fiveoneBookUtil = new FiveoneBookutil();
        if (ptype == 1) {// 成人
            result = fiveoneBookUtil.createOrderByRtPat(order.getUserRtInfo(), order.getPnrpatinfo().trim(), outid);
        }
        else if (ptype == 2) {// 儿童
            result = fiveoneBookUtil.createOrderByRtPat(order.getUserRtInfo(), order.getPnrpatinfo().trim(), "0");
        }
        return result;
    }

}
