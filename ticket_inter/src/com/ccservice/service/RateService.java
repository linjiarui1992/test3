package com.ccservice.service;

import java.io.StringReader;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.base.cabin.Cabin;
import com.ccservice.b2b2c.base.eaccount.Eaccount;
import com.ccservice.b2b2c.base.flightinfo.FlightInfo;
import com.ccservice.b2b2c.base.orderinfo.Orderinfo;
import com.ccservice.b2b2c.base.passenger.Passenger;
import com.ccservice.b2b2c.base.segmentinfo.Segmentinfo;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.ben.Orderchange;
import com.ccservice.b2b2c.policy.Airutil;
import com.ccservice.b2b2c.policy.AirutilNew40;
import com.ccservice.b2b2c.policy.FiveoneBookutil;
import com.ccservice.b2b2c.policy.FiveoneSearch;
import com.ccservice.b2b2c.policy.IwanttogotowhichMethod;
import com.ccservice.b2b2c.policy.JinriMethod;
import com.ccservice.b2b2c.policy.Piaomeng;
import com.ccservice.b2b2c.policy.PiaomengGetPolicyByVoyage;
import com.ccservice.b2b2c.policy.QunarBestDataV2;
import com.ccservice.b2b2c.policy.QunarBestPolicy;
import com.ccservice.b2b2c.policy.SupplyMethod;
import com.ccservice.b2b2c.policy.YeebookingMethod;
import com.ccservice.b2b2c.policy.ben.BaQianYiBook;
import com.ccservice.b2b2c.policy.ben.BaiTuoBook;
import com.ccservice.b2b2c.policy.ben.FiveonBookSearchBook;
import com.ccservice.b2b2c.policy.ben.FiveonBookVersion3;
import com.ccservice.b2b2c.policy.ben.IwanttogotowhichBook;
import com.ccservice.b2b2c.policy.ben.JinRiBook;
import com.ccservice.b2b2c.policy.ben.LocalBook;
import com.ccservice.b2b2c.policy.ben.PiaoMengBook;
import com.ccservice.b2b2c.policy.ben.QunarBestBook;
import com.ccservice.b2b2c.policy.ben.TianQubook;
import com.ccservice.b2b2c.policy.ben.Yi9eBook;
import com.ccservice.b2b2c.policy.officialWebsite.OfficialWebsiteCz;
import com.ccservice.b2b2c.policy.thread.BaiTuoFlightNumThread;
import com.ccservice.b2b2c.policy.thread.EightZrateThread;
import com.ccservice.b2b2c.policy.thread.EightZratebyFlightNumThread;
import com.ccservice.b2b2c.policy.thread.EightZratebyback;
import com.ccservice.b2b2c.policy.thread.FiveOneSearchZratebyFlightNumThread;
import com.ccservice.b2b2c.policy.thread.FiveOneV3ZratebyFlightNumThread;
import com.ccservice.b2b2c.policy.thread.FiveoneZrateThread;
import com.ccservice.b2b2c.policy.thread.FiveoneZratebyBack;
import com.ccservice.b2b2c.policy.thread.IwanttogotowhichZrateThread;
import com.ccservice.b2b2c.policy.thread.JinriZrateThread;
import com.ccservice.b2b2c.policy.thread.JinriZratebyBack;
import com.ccservice.b2b2c.policy.thread.JinriZratebyFlightNumThread;
import com.ccservice.b2b2c.policy.thread.LocalZratebyBack;
import com.ccservice.b2b2c.policy.thread.PiaomengZrateThread;
import com.ccservice.b2b2c.policy.thread.PiaomengZratebyBack;
import com.ccservice.b2b2c.policy.thread.PiaomengZratebyFlightNumThread;
import com.ccservice.b2b2c.policy.thread.QunarBestDataV2ZrateCallback;
import com.ccservice.b2b2c.policy.thread.TianQuZratebyBack;
//import com.ccservice.b2b2c.policy.thread.YeeXingZrateThread;
import com.ccservice.b2b2c.policy.thread.Yi9eZrateThread;
import com.ccservice.b2b2c.policy.util.AirUtil;
import com.ccservice.elong.inter.PropertyUtil;
//import com.ccservice.b2b2c.policy.thread.Yi9eZratebyBack;
//import com.ccservice.b2b2c.policy.thread.ZhtdZrateThread;
//import com.ccservice.b2b2c.policy.thread.ZhtdZratebyFlightNumThread;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.test.HttpClient;

public class RateService extends SupplyMethod implements IRateService {

    public final static Logger logger = Logger.getLogger(RateService.class.getSimpleName());

    //    FiveoneSearch fiveonesearch = new FiveoneSearch();

    // 账号信息开始-----------------------T_EACCOUNT
    PiaoMengBook piaoMengBook = new PiaoMengBook();// ID:2

    BaQianYiBook baQianYiBook = new BaQianYiBook();// ID:3

    //    YeeXingBook yeeXingBook = new YeeXingBook();// ID:4

    //FiveonBook fiveonBook = new FiveonBook();// ID:5
    JinRiBook jinRiBook = new JinRiBook();// ID:6:勿删

    IwanttogotowhichBook iwanttogotowhichBook = new IwanttogotowhichBook();// ID:7

    //    ZongHengTianDiBook zonghengtiandibook = new ZongHengTianDiBook();// ID:8

    //	KkkkBook kkkkBook = new KkkkBook();// 9:勿删

    FiveonBookSearchBook fiveonBookSearchBook = new FiveonBookSearchBook();// ID:11

    FiveonBookVersion3 fiveonbookversion3 = new FiveonBookVersion3();// ID:12

    //	One2580Book one2580Book = new One2580Book();// ID:13:勿删
    //	CtripBook ctripBook = new CtripBook();// ID:14:勿删

    Yi9eBook yi9eBook = new Yi9eBook();// ID:15

    LocalBook localBook = new LocalBook();// ID:16 自己维护政策

    QunarBestBook qnbook = new QunarBestBook();//qunar优选

    TianQubook tqbook = new TianQubook();//18 天衢政策

    String qunarbeststatus = qnbook.getStatus();

    String piaomengstatus = piaoMengBook.getStaus();// 2

    String baqianyistatus = baQianYiBook.getStaus();// 3

    //    String yeeXingstatus = yeeXingBook.getStaus();// 4

    //    String fiveStatus = "0";// 5

    String jinristatus = jinRiBook.getStaus();// 6

    String iwanttogotowhichstatus = iwanttogotowhichBook.getStaus();// 7

    //    String zhtdStatus = zonghengtiandibook.getZhtdStaus();// 8

    //	String kkkkStatus = "0";//kkkkBook.getKkkkStaus();// 9

    String fiveoneStatusSearch = fiveonBookSearchBook.getFiveoneStatusSearch();// 11

    String fiveoneStatusV3 = fiveonbookversion3.getFiveoneStatusV3();// 12

    String one2580Status = "0";//one2580Book.getStaus();// 13

    String ctripStatus = "0";//ctripBook.getStaus();// 14

    String yi9eStatus = yi9eBook.getStaus();// 15

    String localStatus = localBook.getStaus();// 16

    String tqstate = tqbook.getStatus();//17

    // 账号信息结束-----------------------

    private Airutil ariutil = new Airutil();

    private AirutilNew40 ariutilNew40 = new AirutilNew40();

    private FiveoneBookutil fiveoneBookUtil = new FiveoneBookutil();

    private PiaomengGetPolicyByVoyage piaomengGetPolicy = new PiaomengGetPolicyByVoyage();

    private JinriMethod jinriMethod = new JinriMethod();

    private Piaomeng piaomeng = new Piaomeng();

    //    private YeeXingMethod yeeXingMethod = new YeeXingMethod();

    private IwanttogotowhichMethod iwanttogotowhichMethod = new IwanttogotowhichMethod();

    private YeebookingMethod yeebookingMethod = new YeebookingMethod();

    //	private KkkkMethod kkkkMethod = new KkkkMethod();
    //    private ZongHengTianDiMethod zonghengtiandimethod = new ZongHengTianDiMethod();

    Eaccount eaccount = Server.getInstance().getSystemService().findEaccount(20);

    //    private static final int TASK_TIMEOUT_1 = 1; // 设定最长超时时间为5s
    //
    //    private static final int TASK_TIMEOUT_2 = 2; // 设定最长超时时间为5s
    //
    //    private static final int TASK_TIMEOUT_5 = 5; // 设定最长超时时间为5s
    //
    //    private static final int TASK_TIMEOUT_10 = 10; // 设定最长超时时间为5s

    public List SeachFlight(String scity, String ecity, String compname, String depdate, String type) {
        List list = new ArrayList();
        //        if (!"0".equals(fiveStatus)) {
        list = fiveoneBookUtil.SeachFlight(scity, ecity, compname, depdate, type);
        //        }
        //        else {
        //            System.out.println("---51book接口被禁用---------");
        //        }
        return list;
    }

    /**
     * 根据出发到达航班号舱位查询政策所有政策
     * @param scity
     *            出发三字码
     * @param ecity
     *            到达三字码
     * @param sdate
     *            出发时间
     * @param flightnumber
     *            航班号
     * @param cabin
     *            舱位
     * @param traveltype
     *            行程类型1单程2往返联程
     * @param type
     * 			     查询类型1航班列表2下单页面
     * @author chendong
     * @return
     */
    @Override
    public List<Zrate> FindZrateByFlightNumber(String scity, String ecity, String sdate, String flightnumber,
            String cabin, String traveltype, String type) {
        //        Server.getInstance().setDateHashMap(new HashMap<String, String>());
        //        long l1 = System.currentTimeMillis();
        BaiTuoBook baiTuoBook = new BaiTuoBook();
        List<Zrate> zrates = new ArrayList<Zrate>();
        // 单程
        if (traveltype.equals("1")) {
            int zrateNum = 0;
            //勿删
            //			if ("1".equals(fiveStatus)) {
            //				zrateNum++;
            //			}
            if ("1".equals(piaomengstatus)) {
                zrateNum++;
            }

            //            boolean baqianyiStatus_type = "2".equals(type);
            if ("1".equals(baqianyistatus)
            //                    && baqianyiStatus_type
            ) {
                zrateNum++;
            }
            if ("1".equals(jinristatus)) {
                zrateNum++;
            }
            //			//勿删
            //			if ("1".equals(kkkkStatus)) {
            //				zrateNum++;
            //			}
            //只有在下单的时候才获取纵横天地的政策，此处的意义为减少列表的加载时间，增加下单页面的返点的点数
            //            boolean zhtdStatus_type = "2".equals(type);
            //            if ("1".equals(zhtdStatus) && zhtdStatus_type) {
            //                zrateNum++;
            //            }
            //只有在下单的时候才获取51book的政策，此处的意义为减少列表的加载时间，增加下单页面的返点的点数
            boolean fiveoneStatus_type = "2".equals(type);
            if ("1".equals(fiveoneStatusSearch)) {
                zrateNum++;
            }
            if ("1".equals(fiveoneStatusV3) && fiveoneStatus_type) {
                zrateNum++;
            }
            //勿删
            //			if ("1".equals(one2580Status)) {
            //				zrateNum++;
            //			}
            //勿删
            //			if ("1".equals(ctripStatus)) {
            //				zrateNum++;
            //			}
            //            String yi9eStatus1 = "0";//航班列表暂时不用19e的政策

            /*
            if ("1".equals(yi9eStatus)) {
                zrateNum++;
            }
            */
            if ("1".equals(localStatus)) {
                zrateNum++;
            }

            if ("1".equals(tqstate)) {
                zrateNum++;
            }

            if (zrateNum > 0) {
                //                ExecutorService threadPool2 = Executors.newFixedThreadPool(zrateNum);
                ExecutorService threadPool2 = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
                List<Future<List<Zrate>>> futures = new ArrayList<Future<List<Zrate>>>(zrateNum);

                CompletionService<List<Zrate>> completionService = new ExecutorCompletionService<List<Zrate>>(
                        threadPool2);
                Collection<Callable<List<Zrate>>> tasks = new ArrayList<Callable<List<Zrate>>>(zrateNum);
                // 今日根据航班取政策
                if ("1".equals(jinristatus)) {
                    if ("1".equals(getSysconfigString("jinrizratetype"))) {
                        // 政策来自接口中
                        futures.add(completionService.submit(new JinriZratebyFlightNumThread(scity, ecity, sdate,
                                flightnumber, cabin)));

                    }
                    else if ("2".equals(getSysconfigString("jinrizratetype"))) {
                        // 政策来自数据库（同步和全取）中
                        //						if ("1".equals(type)) {
                        futures.add(completionService.submit(new JinriZratebyBack(scity, ecity, sdate, flightnumber,
                                cabin)));

                        //                        tasks.add(new JinriZratebyBack(scity, ecity, sdate, flightnumber, cabin));

                        //						}else{
                        //							completionService.submit(new JinriZratebyBack(
                        //									scity, ecity, sdate, flightnumber, cabin));
                        //							completionService
                        //							.submit(new JinriZratebyFlightNumThread(scity,
                        //									ecity, sdate, flightnumber, cabin));
                        //						}
                    }
                }
                // 八千翼根据航班取政策
                if ("1".equals(baqianyistatus)) {
                    if ("1".equals(getSysconfigString("8000yizratetype"))) {
                        // 政策来自接口中
                        futures.add(completionService.submit(new EightZratebyFlightNumThread(scity, ecity, sdate,
                                flightnumber, cabin)));
                    }
                    else {
                        if ("1".equals(type)) {
                            // 政策来自数据库中
                            futures.add(completionService.submit(new EightZratebyback(scity, ecity, sdate,
                                    flightnumber, cabin)));

                            //                            tasks.add(new EightZratebyback(scity, ecity, sdate, flightnumber, cabin));
                        }
                        else {
                            futures.add(completionService.submit(new EightZratebyFlightNumThread(scity, ecity, sdate,
                                    flightnumber, cabin)));
                        }
                    }
                    // List<Zrate> eightzrates = Airutil.getZrateByFlightNumber(scity, ecity, sdate, flightnumber, cabin);
                    // if (eightzrates.size() > 0) {
                    // zrates.addAll(eightzrates);
                    // }
                }
                // 票盟根据航班取政策
                if ("1".equals(piaomengstatus)) {
                    if ("1".equals(getSysconfigString("piaomengzratetype"))) {
                        // 政策来自接口中
                        futures.add(completionService.submit(new PiaomengZratebyFlightNumThread(scity, ecity, sdate,
                                flightnumber, cabin)));
                    }
                    else {
                        // 政策来自数据库中
                        futures.add(completionService.submit(new PiaomengZratebyBack(scity, ecity, sdate, flightnumber,
                                cabin)));
                        //                        tasks.add(new PiaomengZratebyBack(scity, ecity, sdate, flightnumber, cabin));
                    }

                    // List<Zrate> PMzrates = Piaomeng.getZrateByFlightNumber(scity, ecity, sdate, flightnumber, cabin);
                    // if (PMzrates.size() > 0) {
                    // zrates.addAll(PMzrates);
                    // }
                }
                // 51book根据航班取政策_勿删
                //				if ("1".equals(fiveStatus)) {
                //					completionService.submit(new FiveoneZratebyFlightNumThread(scity, ecity, sdate, flightnumber, cabin));
                //					// List<Zrate> Fiveonezrates = FiveoneBookutil.getZrateByFlightNumber(scity, ecity, sdate,flightnumber, cabin);
                //					// if (Fiveonezrates.size() > 0) {
                //					// zrates.addAll(Fiveonezrates);
                //					// }
                //				}
                // KKKK根据航班取政策//勿删
                //				if ("1".equals(kkkkStatus)) {
                //					completionService.submit(new KkkkZratebyFlightNumThread(scity, ecity, sdate, flightnumber, cabin, "0"));
                //				}
                // 纵横天地根据航班取政策
                //                if ("1".equals(zhtdStatus) && zhtdStatus_type) {
                //                    futures.add(completionService.submit(new ZhtdZratebyFlightNumThread(scity, ecity, sdate,
                //                            flightnumber, cabin)));
                //                }
                // 51book查询接口里面自带的的政策,列表显示而下单页面不显示这个
                if ("1".equals(fiveoneStatusSearch)) {
                    //                if ("1".equals(fiveoneStatusSearch) && !fiveoneStatus_type) {
                    futures.add(completionService.submit(new FiveOneSearchZratebyFlightNumThread(scity, ecity, sdate,
                            flightnumber, cabin)));

                    //                    tasks.add(new FiveOneSearchZratebyFlightNumThread(scity, ecity, sdate,
                    //                            flightnumber, cabin));
                }
                // 51book(v3.0)根据航班获取政策
                if ("1".equals(fiveoneStatusV3) && fiveoneStatus_type) {
                    if ("1".equals(getSysconfigString("51bookzratetype"))) {
                        //从接口获取
                        futures.add(completionService.submit(new FiveOneV3ZratebyFlightNumThread(scity, ecity, sdate,
                                flightnumber, cabin)));
                    }
                    else {
                        //从数据库获取
                        futures.add(completionService.submit(new FiveoneZratebyBack(scity, ecity, sdate, flightnumber,
                                cabin)));
                    }
                }
                // 12580获取政策——勿删
                //				if ("1".equals(one2580Status)) {
                //					if ("1".equals(getSysconfigString("12580zratetype"))) {
                //					} else {
                //						completionService.submit(new One2580Zratebyback(scity,ecity, sdate, flightnumber, cabin));
                //					}
                //				}
                // 携程获取政策——勿删
                //				if ("1".equals(ctripStatus)) {
                //					if ("1".equals(getSysconfigString("ctripzratetype"))) {
                //					} else {
                //						completionService.submit(new CtripZratebyback(scity, ecity, sdate, flightnumber, cabin));
                //					}
                //				}
                // 19E获取政策
                /*
                if ("1".equals(yi9eStatus)) {
                    completionService.submit(new Yi9eZratebyBack(scity, ecity, sdate, flightnumber, cabin));
                }
                */
                // 本地或者远程获取政策
                if ("1".equals(localStatus)) {
                    futures.add(completionService.submit(new LocalZratebyBack(scity, ecity, sdate, flightnumber, cabin,
                            localBook.getZrateUrl())));
                }

                if ("1".equals(tqstate)) {
                    futures.add(completionService
                            .submit(new TianQuZratebyBack(scity, ecity, sdate, flightnumber, cabin)));
                }
                try {
                    for (int t = 0; t < futures.size(); t++) {
                        //                        System.out.println("Invocation:" + t);
                        Future<List<Zrate>> result = completionService.poll(3000, TimeUnit.MILLISECONDS);
                        if (result == null) {
                            //                            System.out.println(new Date() + ":Worker Timedout:" + t);
                            //So lets cancel the first futures we find that havent completed
                            for (Future future : futures) {
                                //                                System.out.println("Checking future");
                                if (future.isDone()) {
                                    continue;
                                }
                                else {
                                    future.cancel(true);
                                    //                                    System.out.println("Cancelled");
                                    break;
                                }
                            }
                            continue;
                        }
                        else {
                            try {
                                if (result.isDone() && !result.isCancelled() && result.get().size() > 0) {// && !result.isCancelled() && result.get()
                                    zrates.addAll(result.get());
                                }
                                else {
                                    continue;
                                }
                                //                                else if (result.isDone() && !result.isCancelled() && !result.get()) {
                                //                                    System.out.println(new Date() + ":Worker Failed");
                                //                                }
                            }
                            catch (ExecutionException ee) {
                                ee.printStackTrace(System.out);
                            }
                        }
                    }
                }
                catch (InterruptedException ie) {
                    logger.error(ie.fillInStackTrace());
                }
                finally {
                    //Cancel by interrupting any existing tasks currently running in Executor Service
                    for (Future<List<Zrate>> f : futures) {
                        f.cancel(true);
                    }
                    threadPool2.shutdown();
                }

                /* try {
                     for (int i = 0; i < zrateNum; i++) {
                         try {
                             Future<List<Zrate>> result = completionService.poll(1000, TimeUnit.MILLISECONDS);
                             if (result == null) {
                                 throw new TimeoutException("waiting timeout");
                             }
                             else {
                                 zrates.addAll(completionService.take().get());
                             }
                         }
                         catch (InterruptedException e) {
                             e.printStackTrace();
                             Thread.currentThread().interrupt();
                         }
                         catch (ExecutionException e) {
                             e.printStackTrace();
                         }
                     }
                 }
                 //                catch (Exception e) {
                 catch (TimeoutException e) {
                     for (Future<List<Zrate>> future : futures) {
                         future.cancel(true);
                     }
                 }
                 finally {
                     threadPool2.shutdown();
                 }*/
            }
        }
        else if (traveltype.equals("2")) {// 往返联程
            if ("1".equals(piaomengstatus)) {
                List<Zrate> PMzrates = Piaomeng.getZrateByFlightNumber1(scity, ecity, sdate, flightnumber, cabin);
                if (PMzrates.size() > 0) {
                    zrates.addAll(PMzrates);
                }
            }
            if ("1".equals(jinristatus)) {
                List<Zrate> JRzrates = JinriMethod.getZrateByFlightNumber1(scity, ecity, sdate, flightnumber, cabin);
                if (JRzrates.size() > 0) {
                    zrates.addAll(JRzrates);
                }
            }
        }
        if (zrates != null && zrates.size() > 0) {
            zrates = sortZrate(zrates, 30);
        }
        //WriteLog.write("获取最优政策FindZrateByFlightNumber", "FindZrateByFlightNumber:" + tempint + ":1:" + (System.currentTimeMillis()-tempTimeMillis));
        return zrates;
    }

    /**
     * 根据pnr获取政策此方法是根据pnr，pnr的rt、pat信息，获取政策 
     * 
     * @param order
     * @param sinfo
     * @param Passenger
     * @param zratecount 返回政策的数量
     * @return Zrate 根据pnr获取的最优政策
     * @author cd
     */
    public List<Zrate> FindZrateByFlight(Orderinfo order, List<Segmentinfo> listSinfo, List<Passenger> listPassenger,
            int zratecount) {
        int tempint = new Random().nextInt(1000);
        BaiTuoBook baiTuoBook = new BaiTuoBook();

        long nowLong = System.currentTimeMillis();
        List<Zrate> zrates = new ArrayList<Zrate>();
        Segmentinfo sinfo = listSinfo.get(0);
        order.setSegmentlist(listSinfo);
        order.setPassengerlist(listPassenger);
        //        Zrate tempZrate = sinfo.getZrate();
        //是否是成人true:是,false:否
        boolean isadult = listPassenger.get(0).getPtype() == 1;
        //是否是单程true:是,false:否
        boolean isOW = listSinfo.size() == 1;
        try {
            //            try {
            //                // 如果选择的政策是特殊的并且是51book的政策就根据ID获取到这个政策的id并add到zrates里面
            //                if (tempZrate.getGeneral() == 2 && tempZrate.getAgentid() == 5) {
            //                    zrates.add(fiveoneBookUtil.FindOneZrateByIdTo51Book(tempZrate.getOutid()));
            //                }
            //            }
            //            catch (Exception e) {
            //                UtilMethod.writeEx(this.getClass().getSimpleName(), e);
            //                e.printStackTrace();
            //            }
            //            long aid = order.getPolicyagentid();//哪家供应商
            int zrateNum = 0;
            //51book2.0停止使用勿删
            //            if ("1".equals(fiveStatus)) {
            //                zrateNum++;
            //            }
            //            piaomengstatus = "1";
            //票盟
            if ("1".equals(piaomengstatus) && isadult) {
                zrateNum++;
            }
            //            baqianyistatus = "1";
            //8000YI
            if ("1".equals(baqianyistatus) && isadult && isOW) {
                zrateNum++;
            }
            //            jinristatus = "1";
            //今日
            if ("1".equals(jinristatus) && isadult && isOW) {
                zrateNum++;
            }
            //            iwanttogotowhichstatus = "1";
            //517NA
            if ("1".equals(iwanttogotowhichstatus) && isadult && isOW) {
                zrateNum++;
            }
            //            yeeXingstatus = "1";
            //易行天下
            //            if ("1".equals(yeeXingstatus) && isadult && isOW) {
            //                zrateNum++;
            //            }
            //纵横天地
            //            if ("1".equals(zhtdStatus) && isadult && isOW) {
            //                zrateNum++;
            //            }
            //            fiveoneStatusV3 = "1";
            if ("1".equals(fiveoneStatusV3) && isOW) {
                zrateNum++;
            }
            //            yi9eStatus = "1";
            if ("1".equals(yi9eStatus) && isadult && isOW) {
                zrateNum++;
            }
            if ("1".equals(localStatus) && isadult && isOW) {
                zrateNum++;
            }

            if ("1".equals(tqstate) && isadult && isOW) {
                zrateNum++;
            }

            if ("1".equals(qunarbeststatus) && isadult && isOW) {
                zrateNum++;
            }
            if ("1".equals(baiTuoBook.getBaiTuoStaus())) {
                zrateNum++;
            }
            ExecutorService threadPool2 = Executors.newFixedThreadPool(1);
            List<Future<List<Zrate>>> futures = new ArrayList<Future<List<Zrate>>>(zrateNum);
            CompletionService<List<Zrate>> completionService = new ExecutorCompletionService<List<Zrate>>(threadPool2);

            WriteLog.write("FindZrateByFlightandpnr_time", tempint + ":" + order.getPnr()
                    + ":newFixedThreadPool(zrateNum):" + zrateNum);
            WriteLog.write("FindZrateByFlightandpnr_time", tempint + ":isOW" + isOW + ":isadult" + isadult);
            // 51book政策
            //            if (aid == 5 || flag) {
            /*51book(v2.0)根据pnr获取政策//勿删
                             if ("1".equals(fiveStatus)) {
                                 completionService.submit(new FiveoneZrateThread(order));
                             }
            51book(v3.0)根据pnr获取政策
            */
            if ("1".equals(fiveoneStatusV3) && isOW) {
                futures.add(completionService.submit(new FiveoneZrateThread(order)));
            }
            if ("1".equals(baiTuoBook.getBaiTuoStaus())) {
                //从接口获取
                futures.add(completionService.submit(new BaiTuoFlightNumThread(order, listPassenger, sinfo)));
            }
            /**
             * 
             * FiveonBook fiveonBook1 = new FiveonBook(); if
             * (!fiveonBook1.getStaus().equals("0")) {
             * System.out.println("51book政策"); List<Zrate> fivezrates =
             * fiveoneBookUtil .GetPolicyDataByPNR(order); if
             * (fivezrates.size() > 0) { zrates.addAll(fivezrates); } }
             * else {
             * System.out.println("-----51book接口被禁用---------------"); }
             * 
             */

            //今日政策
            if ("1".equals(jinristatus) && isadult && isOW) {
                futures.add(completionService.submit(new JinriZrateThread(order, listPassenger, sinfo)));
            }
            //8000YI政策
            int special = 1;
            if ("1".equals(baqianyistatus) && isadult && isOW) {
                futures.add(completionService.submit(new EightZrateThread(order, special)));
            }

            //票盟政策
            if ("1".equals(piaomengstatus) && isadult) {
                futures.add(completionService.submit(new PiaomengZrateThread(listSinfo, listPassenger)));
            }

            //          //            if ("1".equals(yeeXingstatus) && isadul
            // isOW) {

            //               try {
            //                    futures.add(completionService.submit
            // YeeXingZrateThre
            //rder, 1)));
            //          
            //  }
            //                catch
            //ception e) {

            //     //                    e.printStackTrace();
            //                }
            //            }
            if ("1".equals(iwanttogotowhichstatus) && isadult && isOW) {
                try {
                    futures.add(completionService.submit(new IwanttogotowhichZrateThread(order, 1)));
                }
                catch (Exception e) {
                }
            }
            else {
            }
            //            if ("1".equals(zhtdStatus) && isadult && isOW) {
            //                try {
            //                    futures.add(completionService.submit(new ZhtdZrateThread(order)));
            //                }
            //                catch (Exception e) {
            //                    e.printStackTrace();
            //                }
            //            }
            //            else {
            //            }
            // 19E获取政策
            if ("1".equals(yi9eStatus) && isadult && isOW) {
                futures.add(completionService.submit(new Yi9eZrateThread(order)));
            }
            // 本地或者远程获取政策
            if ("1".equals(localStatus) && isadult && isOW) {
                futures.add(completionService.submit(new LocalZratebyBack(order, listSinfo, listPassenger)));
            }
            //去哪儿best
            if ("1".equals(qunarbeststatus) && isadult && isOW) {
                futures.add(completionService.submit(new QunarBestDataV2ZrateCallback(order, listSinfo, listPassenger)));
            }
            if ("1".equals(tqstate) && isadult && isOW) {
                //                futures.add(completionService.submit(new TianQuZratebyBack(order, listSinfo, listPassenger)));
                futures.add(completionService.submit(new TianQuZratebyBack(listSinfo.get(0).getStartairport(),
                        listSinfo.get(0).getEndairport(), new SimpleDateFormat("yyyy-MM-dd").format(sinfo
                                .getDeparttime()), sinfo.getFlightnumber(), sinfo.getCabincode())));
            }
            try {
                for (int t = 0; t < futures.size(); t++) {
                    //                        System.out.println("Invocation:" + t);
                    Future<List<Zrate>> result = completionService.poll(12000, TimeUnit.MILLISECONDS);
                    if (result == null) {
                        //                            System.out.println(new Date() + ":Worker Timedout:" + t);
                        //So lets cancel the first futures we find that havent completed
                        for (Future future : futures) {
                            //                                System.out.println("Checking future");
                            if (future.isDone()) {
                                continue;
                            }
                            else {
                                future.cancel(true);
                                //                                    System.out.println("Cancelled");
                                break;
                            }
                        }
                        continue;
                    }
                    else {
                        try {
                            if (result.isDone() && !result.isCancelled() && result.get().size() > 0) {// && !result.isCancelled() && result.get()
                                zrates.addAll(result.get());
                            }
                            else {
                                continue;
                            }
                            //                                else if (result.isDone() && !result.isCancelled() && !result.get()) {
                            //                                    System.out.println(new Date() + ":Worker Failed");
                            //                                }
                        }
                        catch (ExecutionException ee) {
                            ee.printStackTrace(System.out);
                        }
                    }
                }
            }
            catch (InterruptedException ie) {
                logger.error("rateservice_InterruptedException", ie.fillInStackTrace());
            }
            finally {
                //Cancel by interrupting any existing tasks currently running in Executor Service
                for (Future<List<Zrate>> f : futures) {
                    f.cancel(true);
                }
                threadPool2.shutdown();
            }
            zrates = sortZrate(zrates, zratecount);
            WriteLog.write("FindZrateByFlightandpnr_time",
                    "FindZrateByFlight:" + tempint + ":1:" + (new Date().getTime() - nowLong) / 1000);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        //儿童的只取儿童的基本政策
        if (isadult || zrates.size() == 0) {
            zrates.add(AirUtil.getbaseZrate());
        }
        else {
            zrates.add(AirUtil.getbaseChildZrate());
        }
        WriteLog.write("FindZrateByFlightandpnr_time", "FindZrateByFlight:zrates.size:" + zrates.size());
        return zrates;
    }

    /**
     * 自动支付
     */
    @Override
    public String AutoPay(Orderinfo order) {
        return new AutoPayMethod().autopay(order);
    }

    public String ticketrefundNotify(long changid) {
        // 如果是0的话就是在易订行下单那么就把支付信息通知给易订行
        String result = "-1";
        if (Server.getInstance().getIsAutoPay().equals("0")) {
            Orderchange change = Server.getInstance().getB2BAirticketService().findOrderchange(changid);
            Eaccount eaccount = Server.getInstance().getSystemService().findEaccount(20);
            String url = eaccount.getUrl();
            String ordernum = "";
            String strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><CCS-informpay><Account username=\""
                    + eaccount.getUsername() + "\" password=\"" + eaccount.getPassword()
                    + "\" cmd=\"UPDATERERUNDSTATUS\" changenumber=\"" + change.getExtcnumber()
                    + "\"></Account></CCS-informpay>";
            WriteLog.write("通知YDX已退款成功", "0:" + strXML);
            result = "通知易订行返回的结果:" + submitPost(url, strXML);
            WriteLog.write("通知YDX已退款成功", "1:" + result);

            // 如果是1的话就是在本地下单并支付到供应
        }
        return "";
    }

    @Override
    public String CreateOrder(Orderinfo order, Segmentinfo sinfo, List<Passenger> listPassenger) {
        return new AutoPayMethod().CreateOrder(order, sinfo, listPassenger);
    }

    /**
     * 
     * @time 2016年6月16日 下午3:33:27
     * @author chendong
     */
    private void createOrder517() {
        //        Zrate zrate = sinfo.getZrate();
        //                    if ("1".equals(iwanttogotowhichstatus)) {
        //        result = iwanttogotowhichMethod.creatOrderByPnr(order.getUserRtInfo(), order.getPnrpatinfo(),
        //                order.getBigpnr(), zrate.getOutid(), "13439311111", "13439311111");
        //                    }
        //                    else if ("1".equals(iwanttogotowhichstatus)) {
        //                        result = yeebookingMethod.createOrder(order, sinfo, listPassenger);
        //                    }
    }

    /**
     * 
     * @time 2016年6月16日 下午3:32:42
     * @author chendong
     */
    private void kkkkCreateorder() {
        //      if ("1".equals(kkkkStatus)) {
        //                      String policyId = sinfo.getZrate().getOutid();
        //                      int tempPirce = (((int) ((0.01 * sinfo.getParvalue() * (100 - sinfo
        //                              .getRatevalue()))
        //                              + sinfo.getAirportfee() + sinfo.getFuelfee())) + 1)
        //                              * listPassenger.size();
        //                      String settleAmount = tempPirce + "";
        ////                    result = kkkkMethod.createOrderByPNRInfo(order,policyId, settleAmount, "0", "");
        //                  }
    }

    /**
     * 
     * @time 2016年6月16日 下午3:32:13
     * @author chendong
     */
    private void zhtdCreateOrder() {
        //        if ("1".equals(zhtdStatus)) {
        //            String accountPay = String.valueOf((int) (sinfo.getParvalue() + sinfo.getAirportfee() + sinfo.getFuelfee())
        //                    * listPassenger.size());
        //            result = zonghengtiandimethod.ReserveByPnr(order, accountPay, sinfo.getZrate().getOutid());
        //        }
    }

    /**
     * 
     * @return
     * @time 2016年6月16日 下午3:30:44
     * @author chendong
     */
    private String yeexingCreateOrder() {
        //        if (yeeXingBook.getStaus().equals("1")) {
        //            //                 result = yeeXingMethod.orderGenerationByPNR(order.getPnr(), zrate.getOutid(),
        //                   //                  sinfo.getParvalue() + "", order.getOrdernumber//(), zrate.getRatevalue(//) + "",
        ////                                     zrate.getAddratevalue() + "//");
        //                         }
        //                         else if (yeeXingBook.getStaus().equals("2")) //{
        //    result = yeebookingMethod.createOrder(order, sinfo, listPassenger);
        //                         }
        return null;
    }

    /**
     * 
     * @param order
     * @param zrate
     * @param sinfo
     * @param listPassenger
     * @param ptype
     * @param isWhy
     * @return
     * @time 2016年5月26日 下午1:45:43
     * @author chendong
     */
    private String CreatejinriOrder(Orderinfo order, Zrate zrate, Segmentinfo sinfo, List<Passenger> listPassenger,
            Integer ptype, int isWhy) {
        String result = "-1";
        if ("1".equals(jinristatus)) {
            if (Server.getInstance().getCreatePnr().equals("1")) {
                result = jinriMethod.createOrderbyPNR(order, listPassenger, sinfo);
            }
            else {
                // 在今日生成pnr
                result = jinriMethod.createOrder(order, listPassenger, sinfo);
            }
            // 如果供应状态是2就下单到易订行
        }
        else if ("2".equals(jinristatus)) {
            result = yeebookingMethod.createOrder(order, sinfo, listPassenger);
        }
        else {
            System.out.println("-------今日天下通接口被禁用----------");
        }
        return result;
    }

    /**
     * 
     * @param order
     * @param zrate
     * @param sinfo
     * @param listPassenger
     * @return
     * @time 2016年5月26日 下午1:42:52
     * @author chendong
     * @param ptype 
     */
    private String Create8000YiOrder(Orderinfo order, Zrate zrate, Segmentinfo sinfo, List<Passenger> listPassenger,
            Integer ptype) {
        String result = "-1";
        if (new BaQianYiBook().getStaus().equals("1")) {
            String orderid = "-1";
            try {
                // this.ariutil.createEeightOrder(order.getId(),order.getPolicyid(),
                // order.getPnr());
                if (ptype == 1) {// 成人
                    // orderid=ariutil.createEeightOrder(order.getPolicyid(),
                    // order.getPnr(),ty);
                    result = ariutilNew40.createEeightOrderNew40(order.getPnr(), zrate.getOutid(), order
                            .getUserRtInfo().trim());
                }

                if (ptype == 2) {// 儿童
                    result = ariutil.CreateOrderNewByCHD(order.getPnr(), "");
                }
                System.out.println("订单号为" + orderid);
            }
            catch (RemoteException e) {
                System.out.println("8000yi 创建订单失败：");
                // 删除相应的城市对
                try {
                    // zrate= UpdateZrate(zrate,
                    // sinfo.getStartairport(),
                    // sinfo.getEndairport());
                }
                catch (Exception e1) {
                    e1.printStackTrace();
                }
                e.printStackTrace();
                return "-1";
            }
        }
        else if (new BaQianYiBook().getStaus().equals("2")) {
            result = yeebookingMethod.createOrder(order, sinfo, listPassenger);
        }
        else {
            System.out.println("-------8000yi接口被禁//用--------");
        }
        return result;
    }

    /**
     * 
     * @return
     * @time 2016年5月26日 下午1:40:36
     * @author chendong
     * @param order 
     * @param listPassenger 
     * @param sinfo 
     * @param zrate 
     */
    private String CreatePiaomentOrder(Orderinfo order, Zrate zrate, Segmentinfo sinfo, List<Passenger> listPassenger) {
        String result = "-1";
        if (new PiaoMengBook().getStaus().equals("1")) {
            result = piaomeng.CreateOrderByRt(order.getPnr(), zrate, order);
        }
        else if (new PiaoMengBook().getStaus().equals("2")) {
            result = yeebookingMethod.createOrder(order, sinfo, listPassenger);
        }
        else {
            System.out.println("--------票盟接口被禁用-----------");
        }
        return result;
    }

    /**
     * 收银台模式
     */
    @Override
    public String OrderPay(Orderinfo order) {
        // PayURL
        String url = "";
        long aid = order.getPolicyagentid();
        if (aid == 6) {
            if (!jinRiBook.getStaus().equals("0")) {
                try {
                    url = jinRiBook.getJRURL() + "service=Order_Pay&partner=" + jinRiBook.getJRUSER() + "&orderNo="
                            + order.getExtorderid() + "&paymethod=0" + "&paytype=A" + "&sign=";
                    // 寄送方式(1:邮寄;2:邮寄已支付;3:自取)^联系人姓名^联系电话(电话，手机都可以)^邮政编码^省份^城市^地址
                    String[] Sortedstr = new String[] { "service=Order_Pay", "partner=" + jinRiBook.getJRUSER(),
                            "orderNo=" + order.getExtorderid(), "paymethod=0", "paytype=A" };

                    url += HttpClient.GetSign(Sortedstr);
                    System.out.println(url);
                    String re = HttpClient.httpget(url, "GB2312");

                    System.out.println((re));
                    SAXBuilder builder = new SAXBuilder();
                    Document doc = builder.build(new StringReader(re.trim()));
                    Element root = doc.getRootElement();
                    if (root.getChild("is_success").getTextTrim().equals("T")) {

                        Element response = root.getChild("response");
                        if (response != null) {
                            Element items = response.getChild("Pay_Url");
                            if (items != null) {
                                return URLDecoder.decode(items.getTextTrim(), "GBK");
                            }
                        }
                    }
                    else {
                        System.out.println(root.getChild("error").getTextTrim());
                    }

                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else {
                System.out.println("---------今日接口被禁用---------");
            }
        }
        return url;
    }

    public boolean ChangeOrder1(Orderinfo order, String flightDate, String flightNo) {

        long aid = order.getPolicyagentid();

        if (aid == 6) {
            if (!jinRiBook.getStaus().equals("0")) {

                String orderNo = order.getExtorderid();

                String where = " WHERE " + Passenger.COL_orderid + " = " + order.getId();
                List<Passenger> list = Server.getInstance().getAirService().findAllPassenger(where, "", -1, 0);

                String passengers = "";

                for (int i = 0; i < list.size(); i++) {
                    passengers += list.get(i).getName();
                    if (i != list.size() - 1) {
                        passengers += "^";
                    }

                }

                try {
                    String url = jinRiBook.getJRURL() + "service=ChangeSign_Apply&partner=" + jinRiBook.getJRUSER()
                            + "&orderNo=" + orderNo + "&passengers=" + passengers + "&flightDate=" + flightDate
                            + "&flightNo=" + flightNo + "&sign=";

                    String[] Sortedstr = new String[] { "service=ChangeSign_Apply", "partner=" + jinRiBook.getJRUSER(),
                            "orderNo=" + orderNo, "passengers=" + passengers, "flightDate=" + flightDate,
                            "flightNo=" + flightNo };
                    url += HttpClient.GetSign(Sortedstr);

                    String re2 = HttpClient.httpget(url, "GB2312");

                    SAXBuilder builder = new SAXBuilder();
                    Document doc = builder.build(new StringReader(re2.trim()));
                    Element root = doc.getRootElement();
                    if (root.getChild("is_success").getTextTrim().equals("T")) {
                        Element response = root.getChild("response");
                        if (response != null) {
                            Element items = response.getChild("State");
                            if (items != null && items.getTextTrim().equals("T")) {
                                return true;
                            }
                        }
                    }

                    return false;
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else {

                System.out.println("------今日接口被禁用------------");
            }
        }

        return false;
    }

    /**
     * 适时查找单条今日政策
     * 
     * @param rateno
     * @return
     */
    @Override
    public Zrate FindRateOne(String rateno) {
        // JinRiBook jinRiBook = new JinRiBook();
        if (!jinRiBook.getStaus().equals("0")) {
            try {
                String url = jinRiBook.getJRURL() + "service=GetRateItem&partner=" + jinRiBook.getJRUSER() + "&rateNo="
                        + rateno + "&sign=";
                String[] Sortedstr = new String[] { "service=GetRateItem", "partner=" + jinRiBook.getJRUSER(),
                        "rateNo=" + rateno };
                url += HttpClient.GetSign(Sortedstr);
                String re2 = HttpClient.httpget(url, "GB2312");
                System.out.println((re2));
                SAXBuilder builder = new SAXBuilder();
                Document doc = builder.build(new StringReader(re2.trim()));
                Element root = doc.getRootElement();
                if (root.getChild("is_success").getTextTrim().equals("T")) {
                    Element response = root.getChild("response");
                    if (response != null) {
                        JSONObject o = JSONObject.parseObject(response.getValue());
                        Zrate rate = new Zrate();
                        List<Zrate> listz = Server.getInstance().getAirService()
                                .findAllZrate(" where 1=1 and " + Zrate.COL_outid + " ='" + rateno + "'", "", -1, 0);
                        if (listz.size() > 0) {
                            rate = listz.get(0);
                        }
                        rate.setCreatetime(new Timestamp(System.currentTimeMillis()));
                        rate.setCreateuser("job");
                        rate.setModifytime(new Timestamp(System.currentTimeMillis()));
                        rate.setModifyuser("job");
                        rate.setAgentid(6l);
                        rate.setOutid(o.getString("RateNo"));
                        rate.setGeneral(Long.parseLong(o.getString("RateType")));
                        rate.setDepartureport(o.getString("Scitys")); // 起始机场
                        rate.setArrivalport(o.getString("Ecitys")); // 到达机场
                        rate.setAircompanycode(o.getString("Aircom")); // 航空公司
                        String FlightNos = o.getString("FlightNos");
                        String isUse = o.getString("isUse");

                        /*
                         * if (FlightNos.length() > 4) { // 适用航班
                         * 
                         * rate.setType(1);
                         * rate.setFlightnumber(FlightNos.replaceAll(o
                         * .getString("FlightNos"), "")); }
                         */
                        if (isUse != null && isUse.equals("1")) { // 适用航班

                            rate.setType(1);
                            rate.setFlightnumber(o.getString("FlightNos"));
                        }
                        else {
                            rate.setType(2);
                            rate.setWeeknum(o.getString("FlightNos"));

                        }
                        rate.setTickettype(1);

                        rate.setCabincode(o.getString("Cabins")); // 适用仓位
                        rate.setRatevalue(Float.parseFloat(o.getString("DisCount"))); // 政策返点

                        rate.setIssuedstartdate(new Timestamp(new SimpleDateFormat("yyyy-MM-dd").parse(
                                o.getString("SDate")).getTime())); // 出票开始时间
                        rate.setIssuedendate(new Timestamp(new SimpleDateFormat("yyyy-MM-dd").parse(
                                o.getString("EDate")).getTime())); // 出票结束时间

                        rate.setBegindate(new Timestamp(new SimpleDateFormat("yyyy-MM-dd").parse(o.getString("SDate"))
                                .getTime())); // 政策有效期开始时间
                        rate.setEnddate(new Timestamp(new SimpleDateFormat("yyyy-MM-dd").parse(o.getString("EDate"))
                                .getTime())); // 政策有效期结束时间

                        rate.setRemark(o.getString("Remark"));
                        rate.setUsertype(o.getString("AdultsType"));// 乘客类型
                        // 1：成人，2：儿童，3：成人或儿童
                        rate.setVoyagetype(o.getString("VoyageType"));// 航程类型
                        // 1单程，2往返，3单程及往返
                        rate.setZtype(o.getString("RateType"));// 普通 OR 高反

                        rate.setIsenable(Integer.parseInt(o.getString("Status")));
                        String WorkTime = o.getString("WorkTime");
                        // rate.setWorktime(WorkTime.substring(0,5));
                        // rate.setAfterworktime(WorkTime.substring(6));
                        Server.getInstance().getAirService().updateZrateIgnoreNull(rate);
                        System.out.println(rate);
                        return rate;
                    }
                }
            }
            catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        else {
            System.out.println("------------今日接口被禁用----------");
            return null;
        }
        return null;
    }

    /**
     * 适时查询订单详情
     * 
     * @param orderno
     * @return
     */
    public String FindExtOrder(String orderNo) {
        try {
            String url = jinRiBook.getJRURL() + "service=Order_QueryOrderInfo&partner=" + jinRiBook.getJRUSER()
                    + "&orderNo=" + orderNo + "&sign=";

            String[] Sortedstr = new String[] { "service=Order_QueryOrderInfo", "partner=" + jinRiBook.getJRUSER(),
                    "orderNo=" + orderNo };
            url += HttpClient.GetSign(Sortedstr);

            String re2 = HttpClient.httpget(url, "GB2312");

            System.out.println((re2));
            return re2;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 申请退费票
     * 
     * @param order
     * @return 凭据号 1（当日作废）, 2（按客规自愿退票）, 3（非自愿及特殊退票）, 4（航班延误申请全退）, 5（取消订单）
     * 
     */

    public String ChangeOrder(Orderinfo order, Orderchange orderchange) {
        // 如果不生成外部定单就到配置的链接地址提交退废票
        if (false) {
            long aid = order.getPolicyagentid();
            if (aid == 6) {
                if (!jinRiBook.getStaus().equals("0")) {
                    String orderNo = order.getExtorderid();
                    int refundtype = 5;
                    String remarks = "取消订单";
                    if (order.getOrderstatus() == Orderinfo.FEIPIAOSHENHETONGGUO) {
                        refundtype = 1;
                        remarks = "当日废票";
                    }
                    else if (order.getOrderstatus() == Orderinfo.GAIQIANSHEHETONGGUO) {
                        refundtype = 2;
                        remarks = "改签";
                    }
                    else if (order.getOrderstatus() == Orderinfo.TUIPIAOSHENHETONGGUO) {
                        refundtype = 3;
                        remarks = "退票";
                    }
                    String where = " WHERE " + Passenger.COL_orderid + " = " + order.getId();
                    List<Passenger> list = Server.getInstance().getAirService().findAllPassenger(where, "", -1, 0);
                    String passengers = "";
                    for (int i = 0; i < list.size(); i++) {
                        passengers += list.get(i).getName();
                        if (i != list.size() - 1) {
                            passengers += "^";
                        }
                    }
                    try {
                        String url = jinRiBook.getJRURL() + "service=Order_Refund&partner=" + jinRiBook.getJRUSER()
                                + "&orderNo=" + orderNo + "&passengers=" + passengers + "&refundtype=" + refundtype
                                + "&remarks=" + remarks + "&sign=";
                        String[] Sortedstr = new String[] { "service=Order_Refund", "partner=" + jinRiBook.getJRUSER(),
                                "orderNo=" + orderNo, "passengers=" + passengers, "refundtype=" + refundtype,
                                "remarks=" + remarks };
                        url += HttpClient.GetSign(Sortedstr);
                        String re2 = HttpClient.httpget(url, "GB2312");
                        SAXBuilder builder = new SAXBuilder();
                        Document doc = builder.build(new StringReader(re2.trim()));
                        Element root = doc.getRootElement();
                        if (root.getChild("is_success").getTextTrim().equals("T")) {
                            Element response = root.getChild("response");
                            if (response != null) {
                                Element items = response.getChild("refundNo");
                                return items.getTextTrim();
                            }
                        }
                        return "";
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                else {
                    System.out.println("-------------今日接口被禁用----------");
                    return "";
                }
            }
            // 如果这个值为0则把退费的信息通知到易订行
        }
        else if (Server.getInstance().getIsCreateExtOrder().equals("0")) {
            return yeebookingMethod.ChangeOrder(order, orderchange);
        }
        return "";
    }

    @Override
    public Orderinfo CreateB2COrder(Orderinfo order, Segmentinfo sinfo, List<Passenger> list) {
        if (order.getPolicyagentid() == null) {
            return order;
        }
        long aid = order.getPolicyagentid();

        if (aid == 6) { // 今日
            System.out.println("今日政策");

            if (!jinRiBook.getStaus().equals("0")) {
                try {
                    String flightInfo = sinfo.getStartairport() + "^" + sinfo.getEndairport() + "^"
                            + new SimpleDateFormat("yyyy-MM-dd HH:mm").format(sinfo.getDeparttime()) + "^"
                            + new SimpleDateFormat("yyyy-MM-dd HH:mm").format(sinfo.getArrivaltime()) + "^"
                            + sinfo.getCabincode() + "^" + sinfo.getFlightnumber() + "^" + sinfo.getFlightmodelnum()
                            + "^" + (sinfo.getDiscount().intValue()) + "^0";

                    // PVG^PEK^2010-09-16 08:30^2010-09-16
                    // 10:55^Y^MU5129^320^100^0

                    // 王丽嫦^1^20050106^1^1^1^A
                    // String where = " WHERE "+Passenger.COL_orderid + " = " +
                    // order.getId();
                    // List<Passenger> list
                    // =Server.getInstance().getAirService().findAllPassenger(where,
                    // "", -1, 0);

                    String passengerInfo = "";
                    for (int i = 0; i < list.size(); i++) {
                        passengerInfo += list.get(i).getName().trim();
                        if (list.get(i).getIdtype() == 1) {
                            passengerInfo += "^1";
                        }
                        else if (list.get(i).getIdtype() == 3) {
                            passengerInfo += "^2";
                        }
                        else {
                            passengerInfo += "^3";
                        }
                        passengerInfo += "^" + list.get(i).getIdnumber();
                        if (i != list.size() - 1) {
                            passengerInfo += "|";
                        }
                    }
                    passengerInfo += "^0^0^1^";
                    // 计算返点

                    Float fenxiaovalue = order.getFenxiaoshangfandian();

                    Float jingxiaoshangkoudian = order.getRatevalue() - order.getFenxiaoshangfandian();
                    //

                    String orderInfo = "1^" + jinRiBook.getJRUSER() + "^" + jinRiBook.getJRUSER_xiaji() + "^" + ""
                            + Server.getInstance().getAirService().findZrate(order.getPolicyid()).getOutid() +
                            // "^^^1" ;
                            "^^^1";
                    // 1^chenhairuif^chenhairuif^100907000285^3^0.1

                    String url = jinRiBook.getJRURL() + "" + "service=B2CCreateOrder&partner=" + jinRiBook.getJRUSER()
                            + "&flightInfo=" + URLEncoder.encode(flightInfo) + "&orderInfo="
                            + URLEncoder.encode(orderInfo) + "&passengerInfo=" + URLEncoder.encode(passengerInfo)
                            + "&rateType=1" + "&sign=";
                    String[] Sortedstr = new String[] { "service=B2CCreateOrder", "partner=" + jinRiBook.getJRUSER(),
                            "flightInfo=" + flightInfo, "orderInfo=" + orderInfo, "passengerInfo=" + passengerInfo,
                            "rateType=1" };
                    url += HttpClient.GetSign(Sortedstr);
                    System.out.println(url);
                    String re = HttpClient.httpget(url, "GB2312");
                    System.out.println((re));
                    SAXBuilder builder = new SAXBuilder();
                    Document doc = builder.build(new StringReader(re.trim()));
                    Element root = doc.getRootElement();
                    if (root.getChild("is_success").getTextTrim().equals("T")) {
                        Element response = root.getChild("response");
                        if (response != null) {
                            // Element items = response.getChild("RateItems");
                            // System.out.println("前=="+order);
                            // order.setExtorderid(response.getChildTextTrim("OrderNo"));
                            // order.setPnr(response.getChildTextTrim("PNR"));
                            // order.setExtorderstatus(0);
                            // order.setExtordercreatetime(new
                            // Timestamp(System.currentTimeMillis()));
                            // Server.getInstance().getAirService().updateOrderinfoIgnoreNull(order);
                            // System.out.println("后=="+order);
                        }
                    }
                    else {
                        order.setExtorderstatus(-1);
                        order.setPnr("NOPNR");
                        order.setExtordercreatetime(new Timestamp(System.currentTimeMillis()));
                        // Server.getInstance().getAirService().updateOrderinfoIgnoreNull(order);
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                    order.setExtorderstatus(-1);
                    order.setPnr("NOPNR");
                    order.setExtordercreatetime(new Timestamp(System.currentTimeMillis()));
                    // Server.getInstance().getAirService().updateOrderinfoIgnoreNull(order);
                }
            }
            else {
                System.out.println("---------------今日接口被禁用--------");
            }
        }
        else if (aid == 3) {
            if (!new BaQianYiBook().getStaus().equals("0")) {
                System.out.println("8000yi政策");
                try {
                    this.ariutil.createEeightOrder(order.getPolicyid(), order.getPnr(), "1");
                }
                catch (RemoteException e) {
                    System.out.println("8000yi 创建订单失败：");
                    e.printStackTrace();
                }
            }
            else {
                System.out.println("---------8000yi接口被禁用-----------");
            }
        }

        return order;
    }

    /**
     * 根据政策ID获取当前政策的最新信息。用于8000翼 (non-Javadoc)
     * 
     * @see com.ccservice.service.IRateService#getEightnewZrate(long,
     *      java.lang.String, java.lang.String, java.lang.String,
     *      java.lang.String)
     */
    @Override
    public Zrate getEightnewZrate(long zid, String outid, String fromcity, String tocity, String aircode) {
        try {
            return ariutil.getCurrentZrate(zid, outid, fromcity, tocity, aircode);
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Zrate getBestZrateByPNR(String PNR, int special) {
        return null;
    }

    public String formatMoney_short(String s) {
        // String s = "123.456 ";
        float money = Float.valueOf(s).floatValue();

        DecimalFormat format = null;
        format = (DecimalFormat) NumberFormat.getInstance();
        format.applyPattern("###0.00");
        try {
            String result = format.format(money);
            return result;
        }
        catch (Exception e) {
            return Float.toString(money);
        }
    }

    @Override
    public Zrate FindZrateByIdTo51Book(String zrateid) {
        Zrate zrate = new Zrate();
        return zrate;
    }

    @Override
    public String FindOutOrderInfoByOrderNo(String orderno, String angid) {
        String sub = "-1";
        if (angid.equals("2")) {// 票盟OK
            Piaomeng.getTicketNobyOrderid(orderno);
        }
        if (angid.equals("3")) {// 8000yiOK
            Airutil airutil = new Airutil();
            sub = airutil.GetTicktNoByOrderID(orderno);
        }
        if (angid.equals("5")) {// 51book
            FiveoneBookutil util = new FiveoneBookutil();
            sub = util.Get51bookOrderInfoByOrderNo(orderno);
        }
        if (angid.equals("6")) {// 今日天下通OK
            sub = jinriMethod.GetJinRiOrderTickteNoByOrderNo(orderno);
        }
        return sub;
    }

    public Zrate FindOneZrateTo51Book(String zrateoutid) {
        Zrate zrate = new Zrate();
        try {
            // zrate=book.FindOneZrateByIdTo51Book(zrateoutid);
        }
        catch (RuntimeException e) {
            e.printStackTrace();
        }
        return zrate;
    }

    @Override
    public List<Zrate> findAllPlantBestZrate(Orderinfo order, Segmentinfo sinfo, List<Passenger> listPassenger) {
        List<Segmentinfo> listSinfo = new ArrayList<Segmentinfo>();
        listSinfo.add(sinfo);
        return FindZrateByFlight(order, listSinfo, listPassenger, 50);
    }

    @Override
    public String PayOrderUrlById(String orderid) {
        return null;
    }

    public JinRiBook getJinRiBook() {
        return jinRiBook;
    }

    public void setJinRiBook(JinRiBook jinRiBook) {
        this.jinRiBook = jinRiBook;
    }

    public Airutil getAriutil() {
        return ariutil;
    }

    public void setAriutil(Airutil ariutil) {
        this.ariutil = ariutil;
    }

    //	public FiveonBook getFiveonBook() {
    //		return fiveonBook;
    //	}
    //
    //	public void setFiveonBook(FiveonBook fiveonBook) {
    //		this.fiveonBook = fiveonBook;
    //	}

    @Override
    public List SeachFiveoneFlight(String scity, String ecity, String compname, String depdate, String type) {
        List list = fiveoneBookUtil.SeachFlight(scity, ecity, compname, depdate, type);
        return list;
    }

    /**
     * 航班查询（含票面价、返佣）
     * 
     * @param orgAirportCode 出发机场三字码
     * @param dstAirportCode 抵达城市三字码
     * @param date 起飞日期
     *            格式:“yyyy-MM-dd”
     * @param airlineCode 航空公司三字码
     * @param onlyAvailableSeat 只返回可用舱位
     * @param onlyNormalCommision 是否包括特殊政策
     * @param onlyOnWorkingCommision 是否只返回在工作时间内政策
     * @param onlySelfPNR 是否可更换PNR出票
     * @return
     */
    public List<FlightInfo> getAvailableFlightWithPriceAndCommision(String orgAirportCode, String dstAirportCode,
            String date, String airlineCode, int onlyAvailableSeat, int onlyNormalCommision,
            int onlyOnWorkingCommision, int onlySelfPNR) {
        List<FlightInfo> list = new ArrayList<FlightInfo>();
        try {
            //            1:51book,2今日,3南航
            //            String ticket_inter_flighttype = getSysconfigString("ticket_inter_flighttype");
            String ticket_inter_flighttype = PropertyUtil.getValue("ticket_inter_flighttype", "air.properties");
            FiveoneSearch fiveonesearch = new FiveoneSearch();
            if ("1".equals(ticket_inter_flighttype)) {
                list = fiveonesearch.getAvailableFlightWithPriceAndCommision(orgAirportCode, dstAirportCode, date,
                        airlineCode, onlyAvailableSeat, onlyNormalCommision, onlyOnWorkingCommision, onlySelfPNR);
            }
            else if ("2".equals(ticket_inter_flighttype)) {
                list = JinriMethod.getAvailableFlightWithPriceAndCommision(orgAirportCode, dstAirportCode, date,
                        airlineCode, onlyAvailableSeat, onlyNormalCommision, onlyOnWorkingCommision, onlySelfPNR);
            }
            else if ("3".equals(ticket_inter_flighttype)) {
                list = OfficialWebsiteCz.SeachFlight(orgAirportCode, dstAirportCode, airlineCode, date, "");
            }
            else {
                list = fiveonesearch.getAvailableFlightWithPriceAndCommision(orgAirportCode, dstAirportCode, date,
                        airlineCode, onlyAvailableSeat, onlyNormalCommision, onlyOnWorkingCommision, onlySelfPNR);
            }
        }
        catch (Exception e) {
        }
        return list;
    }

    public FiveoneBookutil getBook() {
        return fiveoneBookUtil;
    }

    public void setBook(FiveoneBookutil book) {
        this.fiveoneBookUtil = book;
    }

    @Override
    public String TuiFeiOrder(Orderinfo orderinfo, List<Passenger> listPassenger, String whyid, Orderchange orderchange) {
        String sub = "-1";
        long policyagentid = orderinfo.getPolicyagentid();// 政策供应商
        String type = "退票";
        if (orderchange.getChangetype() == 4) {
            type = "废票";
        }
        StringBuilder TicketNo = new StringBuilder("");
        for (Passenger passenger : listPassenger) {
            if (listPassenger.size() == 1) {
                TicketNo.append(passenger.getTicketnum());
            }
            else {
                TicketNo.append("|" + passenger.getTicketnum());
            }
        }
        if (policyagentid == 3) {// 8000翼
            //sub = ariutil.orderRGIInfo(orderinfo.getExtorderid(), TicketNo.toString(), type, whyid);
        }
        else if (policyagentid == 5) {// 51book
            //sub = fiveoneBookUtil.TuiFeiOrder(TicketNo.toString(), orderinfo.getExtorderid(), whyid, "");
        }
        else if (policyagentid == 15) {//qunar优选
            if (orderchange.getChangetype() == 4) {
                //废票
            }
            else {
                //退票
                sub = QunarBestPolicy.submitrefundticket(orderinfo.getPostcode(), orderinfo.getExtorderid(),
                        orderinfo.getTripnote(), whyid, listPassenger);
            }
        }
        else if (policyagentid == 17) {
            String sql = "where C_ORDERID=" + orderinfo.getId();
            List<Segmentinfo> segmentinfos = Server.getInstance().getAirService().findAllSegmentinfo(sql, "", -1, 0);
            if (segmentinfos.size() > 0) {
                sub = QunarBestDataV2.tuifeiorder(orderinfo, segmentinfos.get(0), listPassenger, whyid);
            }
        }
        WriteLog.write("退费票申请", "供应商代码：" + policyagentid + ",订单号：" + orderinfo.getOrdernumber() + ",原因代码：" + whyid
                + ",响应结果：" + sub);
        return sub;
    }

    public PiaomengGetPolicyByVoyage getPiaomengGetPolicy() {
        return piaomengGetPolicy;
    }

    public void setPiaomengGetPolicy(PiaomengGetPolicyByVoyage piaomengGetPolicy) {
        this.piaomengGetPolicy = piaomengGetPolicy;
    }

    static void justtest() {
        List<Cabin> cabinList = Server.getInstance().getAirService().findAllCabin("WHERE 1=1", "order by id", -1, 0);
        for (int i = 0; i < cabinList.size(); i++) {
            Cabin cabin = cabinList.get(i);
            System.out.println(cabin.getAircompanycode() + cabin.getCabincode() + ":" + cabin.getTypename() + "|");
        }
    }

    //	public KkkkMethod getKkkkMethod() {
    //		return kkkkMethod;
    //	}
    //
    //	public void setKkkkMethod(KkkkMethod kkkkMethod) {
    //		this.kkkkMethod = kkkkMethod;
    //	}

    //	public KkkkBook getKkkkBook() {
    //		return kkkkBook;
    //	}
    //
    //	public void setKkkkBook(KkkkBook kkkkBook) {
    //		this.kkkkBook = kkkkBook;
    //	}

    @Override
    public String CreateBabyOrder(Orderinfo order, Segmentinfo sinfo, List<Passenger> listPassenger) {
        // TODO Auto-generated method stub
        String result = yeebookingMethod.createBabyOrder(order, sinfo, listPassenger);
        return result;
    }

    /**
     * 如果是改期订单支付后通知到易订行
     */
    @Override
    public String orderchangenotify(long orderid, String memo) {
        if (Server.getInstance().getIsAutoPay().equals("0")) {
            Orderchange change = Server.getInstance().getB2BAirticketService().findOrderchange(orderid);
            Eaccount eaccount = Server.getInstance().getSystemService().findEaccount(20);
            String strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><CCS-informpay><Account username=\""
                    + eaccount.getUsername() + "\" password=\"" + eaccount.getPassword()
                    + "\" cmd=\"ORDERCHANGENOTIFY\" ordercnum=\"" + change.getChangenumber() + "\" ybordercnum=\""
                    + change.getExtcnumber() + "\" orderchangepayrc=\"" + memo + "\"></Account></CCS-informpay>";
            WriteLog.write("通知YDX变更手续费已支付", "0:" + strXML);
            String url = eaccount.getUrl();
            String result = "通知易订行返回的结果:" + submitPost(url, strXML);
            WriteLog.write("通知YDX变更手续费已支付", "1:" + result);
        }
        return "";
    }
}
