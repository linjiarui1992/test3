package com.ccservice.bussinessman;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;


public class UpdateHotelInfoBus implements Job{

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		System.out.println("生意人酒店价格全部更新开始……");
		new BusinessmanUpdateJob().updateHotelInfo();
		System.out.println("生意人酒店价格全部更新结束……");
	}

}
