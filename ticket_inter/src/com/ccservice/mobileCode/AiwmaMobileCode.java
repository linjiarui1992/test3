/**
 * 
 */
package com.ccservice.mobileCode;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.elong.inter.PropertyUtil;

/**
 * 微码
 * http://www.aiwma.com/Default/Index.html
 * @time 2015年10月08日 下午1:58:11
 * @author chendong
 */
public class AiwmaMobileCode implements IMobileCode {

    public static void main(String[] args) {
        String pid = "3305";//12306项目项目编号
        AiwmaMobileCode Aiwmamobilecode = getInstance(pid);
        //        String mobile = Aiwmamobilecode.GetMobilenum("", "", "");
        String mobile = "13143208229";
        System.out.println("mobile:" + mobile);
        //        String SecurityCode = Aiwmamobilecode.getVcodeAndReleaseMobile("", mobile, "", "", "");
        //        System.out.println("SecurityCode:" + SecurityCode);
        System.out.println(Aiwmamobilecode.AddIgnoreList("", mobile, "", ""));

    }

    /**
     * 
     * 
     * @return
     * @time 2015年9月21日 上午10:12:26
     * @author chendong
     */
    public static AiwmaMobileCode getInstance(String pid) {
        //        String pid = "3305";//12306项目项目编号
        String uid = PropertyUtil.getValue("Aiwma_uid", "MobileCode.properties");//12306 
        String author_uid = PropertyUtil.getValue("AiwmaAuthor_uid", "MobileCode.properties");//"cd1989929";//开发者用户名
        String author_pwd = PropertyUtil.getValue("Aiwmaauthor_pwd", "MobileCode.properties");//"cd1989929";//开发者用户名
        AiwmaMobileCode Aiwmamobilecode = new AiwmaMobileCode(pid, uid, author_uid, author_pwd);
        return Aiwmamobilecode;
    }

    //爱码平台登陆
    final static String LOGININURL = "http://www.aiwma.com/DevApi/loginIn";

    //获取手机号码
    final static String GETMOBILENUMURL = "http://www.aiwma.com/DevApi/getMobilenum";

    //获取验证码并释放
    final static String GETVCODEANDRELEASEMOBILEURL = "http://www.aiwma.com/DevApi/getVcodeAndReleaseMobile";

    //加黑手机号码
    final static String ADDIGNORELISTURL = "http://www.aiwma.com/DevApi/addIgnoreList";

    //获取该用户所使用的号码和项目
    final static String GETRECVINGINFO = "";

    //释放
    final static String CANCELSMSRECV = "";

    String pid;//项目编号

    String uid;

    String author_uid;

    String author_pwd;

    String token;

    public AiwmaMobileCode(String pid, String uid, String author_uid, String author_pwd) {
        super();
        this.pid = pid;
        this.uid = uid;
        this.author_uid = author_uid;
        this.author_pwd = author_pwd;
        this.token = gettoken(LoginIn("", ""), "Token");
    }

    /**
     * 
     * @param result
     * @return
     * @time 2015年7月29日 下午3:35:09
     * @author chendong
     */
    public String gettoken(String result, String key) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = JSONObject.parseObject(result);
        }
        catch (Exception e) {
            System.out.println("gettoken_err:result:" + result);
        }
        String Token = "-1";
        try {
            Token = jsonObject.getString(key);
        }
        catch (Exception e) {
        }
        return Token;
    }

    /**
     * /// <summary>
        /// 优码平台登陆
        /// </summary>
        /// <param name="uid">用户名</param>
        /// <param name="pwd">密码</param>
        /// <returns></returns>
     */
    public String LoginIn(String uid, String pwd) {
        String paramContent = "uid=" + this.author_uid + "&pwd=" + this.author_pwd;
        String result = SendPostandGet.submitPost(LOGININURL, paramContent, "utf-8").toString();// .Post(API.LoginIn, String.Format("uid={0}&pwd={1}", uid, pwd));
        WriteLog.write("AiwmaMobileCode_LoginIn", paramContent + ":LoginIn:" + result);
        return result;
    }

    /**
     * 
     * 获取手机号码
     * @param uid 登陆返回的用户ID（数值型的UID，不是用户名，如2684）
     * @param pid 项目ID
     * @param token 令牌
     * @return
     * @time 2015年7月29日 下午3:46:44
     * @author chendong
     */
    public String GetMobilenum(String pid, String uid, String token) {
        String paramContent = "uid=" + this.uid + "&pid=" + this.pid + "&token=" + this.token + "";
        String result = SendPostandGet.submitPost(GETMOBILENUMURL, paramContent, "utf-8").toString();// .Post(API.LoginIn, String.Format("uid={0}&pwd={1}", uid, pwd));
        WriteLog.write("AiwmaMobileCode_GetMobilenum", paramContent + ":GetMobilenum:" + result);
        String[] results = result.split("[|]");
        if (results.length == 2) {
            result = results[0];
        }
        return result;
    }

    /**
     * 获取验证码并保留
     * 
     * @param uid
     * @param mobile
     * @param next_pid
     * @param token
     * @return
     * @time 2015年7月29日 下午3:59:31
     * @author chendong
     */
    public String GetVcodeAndHoldMobilenum(String uid, String mobile, String next_pid, String token) {
        if (uid == null || token == null) {
            return "参数传入有误";
        }
        else {
            String result = "";
            return result;
        }
    }

    /**
     * 获取验证码并释放
     /// <param name="uid">用户ID （同获取号码）</param>
        /// <param name="mobile">手机号码</param>
        /// <param name="pid">项目ID</param>
        /// <param name="token">令牌</param>
        /// <param name="author_uid">开发者用户名</param>
     * 
     * @time 2015年7月29日 下午3:18:57
     * @author chendong
     */
    public String getVcodeAndReleaseMobile(String uid, String mobile, String pid, String token, String author_uid) {
        if (mobile == null) {
            return "mobile不能为空_getVcodeAndReleaseMobile";
        }
        else {
            String paramContent = "mobile=" + mobile + "&token=" + this.token + "&uid=" + this.uid + "&author_uid="
                    + this.author_uid + "&pid=" + this.pid;
            String result = "-1";
            result = SendPostandGet.submitPost(GETVCODEANDRELEASEMOBILEURL, paramContent, "utf-8").toString();
            WriteLog.write("AiwmaMobileCode_getVcodeAndReleaseMobile",
                    paramContent + ":getVcodeAndReleaseMobile:" + result);
            if (result.contains("not_receive")) {
                result = "no_data";
            }
            return result;
        }
    }

    /// <summary>
    /// 添加黑名单
    /// </summary>
    /// <param name="uid">用户ID （同获取号码）</param>
    /// <param name="mobiles">以,号分隔的手机号列表</param>
    /// <param name="pid">项目ID</param>
    /// <param name="token">令牌</param>
    /// <returns></returns>
    public String AddIgnoreList(String uid, String mobiles, String pid, String token) {
        if (mobiles == null) {
            return "mobile不能为空_AddIgnoreList";
        }
        else {
            String paramContent = "uid=" + this.uid + "&pid=" + this.pid + "&token=" + this.token + "&mobiles="
                    + mobiles;
            String result = SendPostandGet.submitPost(ADDIGNORELISTURL, paramContent, "utf-8").toString();// .Post(API.LoginIn, String.Format("uid={0}&pwd={1}", uid, pwd));
            WriteLog.write("AiwmaMobileCode_AddIgnoreList", paramContent + ":AddIgnoreList:" + result);
            return result;
        }
    }

    /// <summary>
    /// 获取当前用户正在使用的号码列表
    /// </summary>
    /// <param name="uid">用户ID （同获取号码）</param>
    /// <param name="pid">项目ID</param>
    /// <param name="token">令牌</param>
    /// <returns></returns>
    public String GetRecvingInfo(String uid, String pid, String token) {
        if (uid == null || pid == null || token == null) {
            return "参数传入有误";
        }
        else {
            String paramContent = "uid=" + uid + "&pid=" + pid + "&token=" + token;
            //            System.out.println(GETRECVINGINFO);
            //            System.out.println(paramContent);
            String result = SendPostandGet.submitPost(GETRECVINGINFO, paramContent, "utf-8").toString();// .Post(API.LoginIn, String.Format("uid={0}&pwd={1}", uid, pwd));
            WriteLog.write("AiwmaMobileCode_GetRecvingInfo", paramContent + ":GetRecvingInfo:" + result);
            return result;
        }
    }

    /// <summary>
    /// 取消所有短信接收，可立即解锁所有被锁定的金额
    /// </summary>
    /// <param name="uid">用户ID （同获取号码）</param>
    /// <param name="token">令牌</param>
    /// <returns></returns>
    public String CancelSMSRecvAll(String uid, String token) {
        if (uid == null || token == null) {
            return "参数传入有误";
        }
        else {
            String result = "";
            return result;
        }
    }

    /// <summary>
    /// 取消一个短信接收，可立即解锁被锁定的金额
    /// </summary>
    /// <param name="uid"></param>
    /// <param name="mobile"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    public String CancelSMSRecv(String uid, String mobile, String token) {
        if (uid == null || token == null) {
            return "参数传入有误";
        }
        else {
            String paramContent = "uid=" + this.author_uid + "&pid=" + this.pid + "&token=" + this.token + "&mobile="
                    + mobile;
            String result = SendPostandGet.submitPost(ADDIGNORELISTURL, paramContent, "utf-8").toString();// .Post(API.LoginIn, String.Format("uid={0}&pwd={1}", uid, pwd));
            WriteLog.write("AiwmaMobileCode_CancelSMSRecv", paramContent + ":AddIgnoreList:" + result);
            return result;
        }
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getAuthor_uid() {
        return author_uid;
    }

    public void setAuthor_uid(String author_uid) {
        this.author_uid = author_uid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAuthor_pwd() {
        return author_pwd;
    }

    public void setAuthor_pwd(String author_pwd) {
        this.author_pwd = author_pwd;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#send(java.lang.String, java.lang.String)
     */
    @Override
    public String send(String mobile, String content) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String ReleaseMobile(String uid, String mobiles, String pid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

}
