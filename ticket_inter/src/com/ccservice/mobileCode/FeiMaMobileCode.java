package com.ccservice.mobileCode;

import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.elong.inter.PropertyUtil;

public class FeiMaMobileCode implements IMobileCode {

    public static void main(String[] args) {
        String pid = "1553";//项目ID
        String uid = "";//
        FeiMaMobileCode feimamobilecode = getInstance(pid);
        String token = feimamobilecode.LoginIn("", "");
        System.out.println(token);
        //        String mobile = feimamobilecode.GetMobilenum("", "", "");
        //        System.out.println(mobile);
        //        String yzm = feimamobilecode.getVcodeAndReleaseMobile(author_uid, mobile, pid, token, "");
        //        System.out.println(yzm);
        //        String black = feimamobilecode.AddIgnoreList("", mobile, pid, token);
        //        System.out.println("black:" + black);
    }

    /**
     * 
     * 
     * @param pid 4036(12306项目项目编号)
     * @return
     * @time 2015年9月21日 上午10:12:26
     * @author chendong
     */
    public static FeiMaMobileCode getInstance(String pid) {
        //        String pid = "1553";//12306项目项目编号
        String uid = "";//12306
        String author_uid = PropertyUtil.getValue("FeiMaAuthor_uid", "MobileCode.properties");//"cd1989929";//开发者用户名
        String author_pwd = PropertyUtil.getValue("FeiMaAuthor_pwd", "MobileCode.properties");//"cd1989929";//开发者用户名
        FeiMaMobileCode feiMaMobileCode = new FeiMaMobileCode(pid, uid, author_uid, author_pwd);
        return feiMaMobileCode;
    }

    String pid; //项目编号

    String uid; //

    String author_uid;//用户名

    String author_pwd;//用户密码

    String token; //令牌

    public FeiMaMobileCode(String pid, String uid, String author_uid, String author_pwd) {
        super();
        this.pid = pid;
        this.uid = uid;
        this.author_uid = author_uid;
        this.author_pwd = author_pwd;
        this.token = LoginIn("", "");
    }

    /**
     *  项目类型返回值说明：
     *  1 表示此项目用于接收验证码
     *  2表示此项目可发送短信
     *  3表示此项目即可接收验证码，也可以发送短信
     *  4表示此项目同一个号码可接收多次验证码
     **/
    final String LOGININURL = "http://api.83r.com:20153/User/login"; //登录

    final String GETITEMSURL = "http://api.83r.com:20153/User/getItems"; //登录

    final String GETMOBILENUMURL = "http://api.83r.com:20153/User/getPhone"; //手机号

    final String GETMESSAGEURL = "http://api.83r.com:20153/User/getMessage"; //验证码

    final String ADDBLACKURL = "http://api.83r.com:20153/User/addBlack"; //黑名单

    /**
     * @param uid 用户名
     * @param pwd 密码
     * @return 登录
     * @time 2015年9月16日 22:33:24
     * @author ZhiHong
     */
    public String LoginIn(String uid, String pwd) {
        String paramContent = this.LOGININURL + "?uName=" + this.author_uid + "&pWord=" + this.author_pwd;
        String result = SendPostandGet.submitGet(paramContent, "GB2312");
        WriteLog.write("FeiMaMobileCode_LoginIn", paramContent + ":LoginIn:" + result);
        String results[] = result.split("[&]");
        result = results[0];
        return result;
    }

    /**
     * @param pid 项目编号
     * @param token 令牌
     * @return 获取手机号
     * @time 2015年9月16日 22:34:14
     */
    @Override
    public String GetMobilenum(String uid, String pid, String token) {
        String paramContent = this.GETMOBILENUMURL + "?ItemId=" + this.pid + "&token=" + this.token;
        String result = SendPostandGet.submitGet(paramContent, "GB2312");
        WriteLog.write("FeiMaMobileCode_GetMobilenum", paramContent + ":GetMobilenum:" + result);
        if (result.length() >= 11) {
            result = result.substring(0, 11);
        }
        return result;
    }

    /**
     * @param token 令牌
     * @return 获取验证码短信继续使用本号
     * @time 2015年9月16日 22:36:16
     */
    @Override
    public String GetVcodeAndHoldMobilenum(String uid, String mobile, String next_pid, String token) {
        return null;
    }

    /**
     * @param token 令牌
     * @return 获取验证码短信不再使用本号
     * @time 2015年9月16日 22:36:16
     */
    @Override
    public String getVcodeAndReleaseMobile(String uid, String mobile, String pid, String token, String author_uid) {
        if (mobile == null) {
            return "feima手机号不能为空getVcodeAndReleaseMobile";
        }
        else {
            String paramContent = this.GETMESSAGEURL + "?token=" + this.token;
            String result = SendPostandGet.submitGet(paramContent, "GB2312");
            WriteLog.write("FeiMaMobileCode_GetVcodeAndHoldMobilenum",
                    paramContent + ":GetVcodeAndHoldMobilenum:" + result);
            if (result.contains("NOTION") || result.contains("Null") || result == null) {
                result = "no_data";
            }
            return result;
        }
    }

    /**
     * @param token 令牌
     * @param mobiles 加入黑名单
     * @return 2015年9月16日 22:39:18
     */
    @Override
    public String AddIgnoreList(String uid, String mobiles, String pid, String token) {
        if (mobiles == null) {
            return "feima手机号不能为空AddIgnoreList";
        }
        else {
            String paramContent = this.ADDBLACKURL + "?token=" + this.token + "&phoneList=" + this.pid + "-" + mobiles;
            String result = SendPostandGet.submitGet(paramContent, "GB2312");
            WriteLog.write("FeiMaMobileCode_AddIgnoreList", paramContent + ":AddIgnoreList:" + result);
            return result;
        }
    }

    @Override
    public String GetRecvingInfo(String uid, String pid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String CancelSMSRecvAll(String uid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String CancelSMSRecv(String uid, String mobile, String token) {
        // TODO Auto-generated method stub
        return null;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getAuthor_uid() {
        return author_uid;
    }

    public void setAuthor_uid(String author_uid) {
        this.author_uid = author_uid;
    }

    public String getAuthor_pwd() {
        return author_pwd;
    }

    public void setAuthor_pwd(String author_pwd) {
        this.author_pwd = author_pwd;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#send(java.lang.String, java.lang.String)
     */
    @Override
    public String send(String mobile, String content) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String ReleaseMobile(String uid, String mobiles, String pid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

}
