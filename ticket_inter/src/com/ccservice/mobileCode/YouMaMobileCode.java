/**
 * 
 */
package com.ccservice.mobileCode;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.elong.inter.PropertyUtil;

/**
 * http://www.6tudou.com:9000/adminlogin.html
 * 
 * 
 * 你好 这是最新的用户客户端 可用之前的账号密码登录 余额都还在 请内部使用 不对外开放
API接口：域名改变成www.yo369.com，其他保持不变 暂时不提供网页
下载地址：http://pan.baidu.com/s/1hrfVdLq
充值方式：支付宝账号是mayou@qq.com姚槐 在客户端充值
 * 
 * 
 * 
 * 黑名单共享
 * 
 * 系统可提供自动化API供开发者开发自动化程序，目前平台同时支持GET和POST请求

    如有不明白之处或自己没能力开发程序想找人开发的请加优码API开发者交流群:385444049.无类似需求勿加.

    下面是系统API的说明:统一参数调用方法，下面是API的基地址

    基地址： http://www.yo369.com/DevApi

    编码:UTF-8

    post数据:参数名1=值1&参数2=值2....

    如登录调用方法：

    posturl: http://www.yo369.com/DevApi/loginIn

    post数据:uid=用户名&pwd=密码

    get方式:http://www.yo369.com/DevApi/loginIn?uid=用户名&pwd=密码

    如获取号码调用方法：

    posturl: http://www.yo369.com/DevApi/getMobilenum

    post数据:uid=用户登陆返回的&pid=项目ID&token=登陆返回的令牌

    get方式:http://www.yo369.com/DevApi/getMobilenum?uid=用户登陆返回的uid&pid=项目id&token=登陆返回的令牌token

    所有API统一返回值含意
    true:正常

    error_parameter:传入参数错误

    no_data:没有匹配短信或者没收到短信

    UserMax:已经达到了可以获取手机号的最大数量，不要随便调用cancelSMSRecvAll释放,否则之前获取的所有号码将会失效.
    此处应当考虑到户用积分不足的情况,如果调用了cancelSMSRecvAll程序会进入死循环,会一直重复获取号码然后遇到UserMax然后cancelSMSRecvAll然后再重复以上过程.
 * @time 2015年7月29日 下午3:16:14
 * @author chendong
 */
public class YouMaMobileCode implements IMobileCode {
    //优码平台登陆  API接口：域名改变成www.yo369.com，其他保持不变 暂时不提供网页
    final static String LOGININURL = "http://www.yo369.com/DevApi/loginIn";

    //获取手机号码
    final static String GETMOBILENUMURL = "http://www.yo369.com/DevApi/getMobilenum";

    //获取验证码并释放
    final static String GETVCODEANDRELEASEMOBILEURL = "http://www.yo369.com/DevApi/getVcodeAndReleaseMobile";

    //获取验证码并释放
    final static String ADDIGNORELISTURL = "http://www.yo369.com/DevApi/addIgnoreList";

    //获取该用户所使用的号码和项目
    final static String GETRECVINGINFO = "http://www.yo369.com/DevApi/getRecvingInfo";

    //    public static readonly String LoginIn = "http://www.yo369.com/DevApi/loginIn";//登陆
    //    public static readonly String GetMobilenum = "http://www.yo369.com/DevApi/getMobilenum";//获取号码
    //    public static readonly String GetVcodeAndHoldMobilenum = "http://www.yo369.com/DevApi/getVcodeAndHoldMobilenum";//获取短信并继续使用该号码
    //    public static readonly String GetVcodeAndReleaseMobile = "http://www.yo369.com/DevApi/getVcodeAndReleaseMobile";//获取短信并释放该号码
    //    public static readonly String AddIgnoreList = "http://www.yo369.com/DevApi/addIgnoreList";//添加黑名单
    //    public static readonly String GetRecvingInfo = "http://www.yo369.com/DevApi/getRecvingInfo";//获取该用户所使用的号码和项目
    //    public static readonly String CancelSMSRecvAll = "http://www.yo369.com/DevApi/cancelSMSRecvAll";//释放所有号码
    //    public static readonly String CancelSMSRecv = "http://www.yo369.com/DevApi/cancelSMSRecv";//释放单个号码
    //    final static String PID = "6168";//12306项目id
    //
    //    final static String UID = "120865";//12306
    //
    //    final static String AUTHOR_UID = "cd1989929";//开发者用户名
    /**
     * 
     * 
     * @param pid 4036(12306项目项目编号)
     * @return
     * @time 2015年9月21日 上午10:12:26
     * @author chendong
     */
    public static YouMaMobileCode getInstance(String pid) {
        //        String pid = "4036";//12306项目项目编号
        String uid = "";//12306
        String author_uid = PropertyUtil.getValue("YouMaAuthor_uid", "MobileCode.properties");//"cd1989929";//开发者用户名
        String author_pwd = PropertyUtil.getValue("YouMaauthor_pwd", "MobileCode.properties");//"cd1989929";//开发者用户名
        YouMaMobileCode youmamobilecode = new YouMaMobileCode(pid, uid, author_uid, author_pwd);
        return youmamobilecode;
    }

    String pid;

    String uid;

    String author_uid;

    String author_pwd;

    String token;

    public YouMaMobileCode(String pid, String uid, String author_uid, String author_pwd) {
        super();
        this.pid = pid;
        this.uid = uid;
        this.author_uid = author_uid;
        this.author_pwd = author_pwd;
        token = gettoken(LoginIn(getAuthor_uid(), getAuthor_pwd()), "Token");
        setToken(token);
    }

    public static void main(String[] args) {
        String pid = "11796";//12306项目id
        String uid = "120865";//12306
        String author_uid = "cd1989929";//开发者用户名
        String author_pwd = "cd1989929";//开发者用户名

        YouMaMobileCode youmamobilecode = new YouMaMobileCode(pid, uid, author_uid, author_pwd);
        String mobile = "18718516480";
        mobile = youmamobilecode.GetMobilenum(youmamobilecode.getUid(), youmamobilecode.getPid(),
                youmamobilecode.getToken());
        System.out.println(mobile);
        //        result = youmamobilecode.getVcodeAndReleaseMobile(youmamobilecode.getUid(), mobile, youmamobilecode.getPid(),
        //                youmamobilecode.getToken(), youmamobilecode.getAuthor_uid());
        //添加黑名单
        //        result = youmamobilecode.AddIgnoreList(youmamobilecode.getUid(), mobile, youmamobilecode.getPid(),
        //                youmamobilecode.getToken());
        //获取当前用户正在使用的号码列表
        //        result = youmamobilecode.GetRecvingInfo(youmamobilecode.getUid(), youmamobilecode.getPid(),
        //                youmamobilecode.getToken());
        //        System.out.println(result);
    }

    /**
     * 
     * @param result
     * @return
     * @time 2015年7月29日 下午3:35:09
     * @author chendong
     */
    public String gettoken(String result, String key) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = JSONObject.parseObject(result);
        }
        catch (Exception e) {
            System.out.println("gettoken_err:result:" + result);
        }
        String Token = "-1";
        try {
            Token = jsonObject.getString(key);
        }
        catch (Exception e) {
        }
        return Token;
    }

    /**
     * /// <summary>
        /// 优码平台登陆
        /// </summary>
        /// <param name="uid">用户名</param>
        /// <param name="pwd">密码</param>
        /// <returns></returns>
     */
    public String LoginIn(String uid, String pwd) {
        if (uid == null || pwd == null) {
            return "用户名或密码不能为空";
        }
        else {
            String paramContent = "uid=" + uid + "&pwd=" + pwd;
            //            System.out.println(LOGININURL);
            //            System.out.println(paramContent);
            //            {"Uid":120865,"Token":"31RroGrAw5Var/mSNgs6BlSbPonIBFVeK8iNDhAgrA5nDBtGQmyoYtc0qApnKJl%2Bvyv%2BQXO/js0=","InCome":0.000,"Balance":4.900,"UsedMax":20}
            String result = SendPostandGet.submitPost(LOGININURL, paramContent, "utf-8").toString();// .Post(API.LoginIn, String.Format("uid={0}&pwd={1}", uid, pwd));
            //            System.out.println(result);
            WriteLog.write("YouMaMobileCode_LoginIn", paramContent + ":LoginIn:" + result);
            return result;
        }
    }

    /**
     * 获取手机号码
     * @param uid 登陆返回的用户ID（数值型的UID，不是用户名，如2684）
     * @param pid 项目ID
     * @param token 令牌
     * @return
     * @time 2015年7月29日 下午3:46:44
     * @author chendong
     */
    public String GetMobilenum(String uid, String pid, String token) {
        if (this.uid == null || this.pid == null || this.token == null) {
            return "用户名或密码不能为空";
        }
        else {
            String paramContent = "uid=" + this.uid + "&pid=" + this.pid + "&token=" + this.token;
            //            System.out.println(GETMOBILENUMURL);
            //            System.out.println(paramContent);
            String result = "";
            for (int i = 0; i < 3; i++) {
                result = SendPostandGet.submitPost(GETMOBILENUMURL, paramContent, "utf-8").toString();
                if (result.contains("请求过快")) {
                    try { //                亲，请求过快，请间隔300毫秒，1秒钟最大允许请求三次
                        Thread.sleep(300);
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    continue;
                }
                else {
                    break;
                }
            }
            //            System.out.println(result);
            WriteLog.write("YouMaMobileCode_GetMobilenum", paramContent + ":GetMobilenum:" + result);
            return result;
        }
    }

    /**
     * 获取验证码并保留
     * 
     * @param uid
     * @param mobile
     * @param next_pid
     * @param token
     * @return
     * @time 2015年7月29日 下午3:59:31
     * @author chendong
     */
    public String GetVcodeAndHoldMobilenum(String uid, String mobile, String next_pid, String token) {
        if (uid == null || token == null) {
            return "参数传入有误";
        }
        else {
            String result = "";
            return result;
        }
    }

    /**
     * 获取验证码并释放
     /// <param name="uid">用户ID （同获取号码）</param>
        /// <param name="mobile">手机号码</param>
        /// <param name="pid">项目ID</param>
        /// <param name="token">令牌</param>
        /// <param name="author_uid">开发者用户名</param>
     * 
     * @time 2015年7月29日 下午3:18:57
     * @author chendong
     */
    public String getVcodeAndReleaseMobile(String uid, String mobile, String pid, String token, String author_uid) {
        if (this.uid == null || this.pid == null || this.token == null) {
            return "用户名或密码不能为空";
        }
        else {
            //            uid={0}&mobile={1}&pid={2}&token={3}&author_uid={4}
            String paramContent = "uid=" + this.uid + "&pid=" + this.pid + "&token=" + this.token + "&mobile=" + mobile
                    + "&author_uid=" + this.author_uid;
            //            System.out.println(GETMOBILENUMURL);
            //            System.out.println(paramContent);
            String result = SendPostandGet.submitPost(GETVCODEANDRELEASEMOBILEURL, paramContent, "utf-8").toString();// .Post(API.LoginIn, String.Format("uid={0}&pwd={1}", uid, pwd));
            WriteLog.write("YouMaMobileCode_getVcodeAndReleaseMobile", paramContent + ":getVcodeAndReleaseMobile:"
                    + result);
            return result;
        }
    }

    /// <summary>
    /// 添加黑名单
    /// </summary>
    /// <param name="uid">用户ID （同获取号码）</param>
    /// <param name="mobiles">以,号分隔的手机号列表</param>
    /// <param name="pid">项目ID</param>
    /// <param name="token">令牌</param>
    /// <returns></returns>
    public String AddIgnoreList(String uid, String mobiles, String pid, String token) {
        if (this.uid == null || this.pid == null || this.token == null) {
            return "参数传入有误";
        }
        else {
            String paramContent = "uid=" + this.uid + "&pid=" + this.pid + "&token=" + this.token + "&mobiles="
                    + mobiles;
            //            System.out.println(ADDIGNORELISTURL);
            //            System.out.println(paramContent);
            String result = SendPostandGet.submitPost(ADDIGNORELISTURL, paramContent, "utf-8").toString();// .Post(API.LoginIn, String.Format("uid={0}&pwd={1}", uid, pwd));
            WriteLog.write("YouMaMobileCode_AddIgnoreList", paramContent + ":AddIgnoreList:" + result);
            return result;
        }
    }

    /// <summary>
    /// 获取当前用户正在使用的号码列表
    /// </summary>
    /// <param name="uid">用户ID （同获取号码）</param>
    /// <param name="pid">项目ID</param>
    /// <param name="token">令牌</param>
    /// <returns></returns>
    public String GetRecvingInfo(String uid, String pid, String token) {
        if (uid == null || pid == null || token == null) {
            return "参数传入有误";
        }
        else {
            String paramContent = "uid=" + uid + "&pid=" + pid + "&token=" + token;
            //            System.out.println(GETRECVINGINFO);
            //            System.out.println(paramContent);
            String result = SendPostandGet.submitPost(GETRECVINGINFO, paramContent, "utf-8").toString();// .Post(API.LoginIn, String.Format("uid={0}&pwd={1}", uid, pwd));
            WriteLog.write("YouMaMobileCode_GetRecvingInfo", paramContent + ":GetRecvingInfo:" + result);
            return result;
        }
    }

    /// <summary>
    /// 取消所有短信接收，可立即解锁所有被锁定的金额
    /// </summary>
    /// <param name="uid">用户ID （同获取号码）</param>
    /// <param name="token">令牌</param>
    /// <returns></returns>
    public String CancelSMSRecvAll(String uid, String token) {
        if (uid == null || token == null) {
            return "参数传入有误";
        }
        else {
            String result = "";
            return result;
        }
    }

    /// <summary>
    /// 取消一个短信接收，可立即解锁被锁定的金额
    /// </summary>
    /// <param name="uid"></param>
    /// <param name="mobile"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    public String CancelSMSRecv(String uid, String mobile, String token) {
        if (uid == null || token == null) {
            return "参数传入有误";
        }
        else {
            String result = "";

            return result;
        }
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getAuthor_uid() {
        return author_uid;
    }

    public void setAuthor_uid(String author_uid) {
        this.author_uid = author_uid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAuthor_pwd() {
        return author_pwd;
    }

    public void setAuthor_pwd(String author_pwd) {
        this.author_pwd = author_pwd;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#send(java.lang.String, java.lang.String)
     */
    @Override
    public String send(String mobile, String content) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String ReleaseMobile(String uid, String mobiles, String pid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

}
