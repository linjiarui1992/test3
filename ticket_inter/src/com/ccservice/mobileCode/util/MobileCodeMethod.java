package com.ccservice.mobileCode.util;

import java.util.Map;
import java.util.TreeSet;

import com.ccervice.util.StringUtil;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.Util.JobTrainUtil;
import com.ccservice.mobileCode.IMobileCode;

public class MobileCodeMethod {

    public static void main(String[] args) {
        String mobileCodeType = "6";//# 1:优码,2:淘码,3:y码,4:壹码,5:爱码,6:卓码,7:要码
        String useProxy = "0";//是否使用代理
        String proxyHost = "";//代理ip
        String proxyPort = "";//代理端口
        //  MobileCodeMethod mobileCodeMethod = new MobileCodeMethod(mobileCodeType, useProxy, proxyHost, proxyPort);
        String logpassword = "asd123456";
        String cookieString = "JSESSIONID=0A01D96498DB9DC9613328AEB59DCE959F2E104693; BIGipServerotn=1691943178.38945.0000; current_captcha_type=Z";
        String mobile = "13610311545";
        String mobilecode = "610262";

        if (logpassword != null && cookieString != null) {
            //            总流程
            //            String result = mobileCodeMethod.CheckAndChange12306Mobileno(cookieString, logpassword, mobileCodeMethod);
            //            System.out.println(result);
            //拿手机号,验证码,cookie流程,核验
            //            String checkChangeSuccess = mobileCodeMethod.checkMobileCode(mobile, mobilecode, cookieString);
            //            System.out.println(checkChangeSuccess);
        }

        //username=xiaxiaicy5&password=asd123456&cookie=JSESSIONID=0A01D72BFC3E2B284F9117AD495197858B128FEB8B; BIGipServerotn=735510794.64545.0000; current_captcha_type=Z

        MobileCodeMethod codeMethod = new MobileCodeMethod("31", "", "", "", "");
        codeMethod.mobilecode = MobileCodeBeanUtil.initMobileCodeType(codeMethod.mobileCodeType);
        for (int i = 0; i < 5; i++) {
            String mobileCodeString = codeMethod.getOneMobileNoCut(null, "", "", 1l);
        }

    }

    private static final String INITQUERYUSERINFO_URL_STRING = "https://kyfw.12306.cn/otn/modifyUser/initQueryUserInfo";

    private static final String BINDTEL_URL_STRING = "https://kyfw.12306.cn/otn/userSecurity/bindTel";

    private static final String DOEDITTEL_URL_STRING = "https://kyfw.12306.cn/otn/userSecurity/doEditTel";

    private static final String GETMOBILECODE_URL_STRING = "https://kyfw.12306.cn/otn/userSecurity/getMobileCode";

    private static final String CHECKMOBILECODE_URL_STRING = "https://kyfw.12306.cn/otn/userSecurity/checkMobileCode";

    IMobileCode mobilecode;

    //1:云码,2:淘码,
    String mobileCodeType;

    String useProxy;

    String proxyHost;

    String proxyPort;

    String mobile;//准备修改的手机号

    String repUrl;

    /**
     * 用來存儲mobileCode   状态 0 不可用(获取不到手机号)  1 待用()  
     */
    private TreeSet<String> handleMobileCodes; //---anki

    public MobileCodeMethod(String mobileCodeType, String useProxy, String proxyHost, String proxyPort, String repUrl) {
        super();
        this.mobileCodeType = mobileCodeType;//1:云码,2:淘码,3:y码
        this.useProxy = useProxy;
        this.proxyHost = proxyHost;
        this.proxyPort = proxyPort;
        this.repUrl = repUrl;
        this.handleMobileCodes = this.initConfigCodeTypes();
    }

    /**
     * 
     * @time 2015年7月29日 下午6:07:19
     * @author chendong
     * @param repUrl 
     * @param password2 
     * @param proxyPort 
     * @param proxyHost 
     * @param useProxy 
     */
    public String CheckAndChange12306Mobileno(String cookieString, String loginname, String password,
            MobileCodeMethod mobileCodeMethod) {
        String result = "";
        boolean isSuccess = false;
        //        访问个人中心,不需要结果
        String yitongGuoHeyan = null;
        for (int i = 0; i < 3; i++) {
            yitongGuoHeyan = mobileCodeMethod.initQueryUserInfo(cookieString);
            if (yitongGuoHeyan.contains("用户未登录")) {
                cookieString = JobTrainUtil.getCookie(this.repUrl, loginname, password);
                System.out.println("new_cookieString:" + cookieString);
                if (cookieString.contains("您的用户已经被锁定")) {
                    break;
                }
                else {
                    continue;
                }
            }
            else {
                System.out.println(yitongGuoHeyan);
                System.out.println("您的用户已经登录");
                break;
            }
        }
        System.out.println("账号是否通过核验:" + yitongGuoHeyan);
        if ("0".equals(yitongGuoHeyan)) {
            //第一步 输入新手机和密码确认核验  https://kyfw.12306.cn/otn/userSecurity/doEditTel
            mobilecode = MobileCodeBeanUtil.initMobileCodeType(this.mobileCodeType);//初始化手机验证码 平台
            String mobileNoisEnable = doedit(mobileCodeMethod, password, cookieString);
            System.out.println("doedit::" + mobile + ">" + mobileNoisEnable);
            if ("true".equals(mobileNoisEnable) && mobile.length() == 11) {
                //去12306验证手机号是否能用
                String sendResult = mobilecode.send(this.mobile, "999");
                System.out.println(mobile + ":发短信结果:" + sendResult);
                if ("true".equals(sendResult)) {
                    result = getCodeString();//根据手机号获取短信 
                    if (result.contains("no_data")) {
                        addIgnoreList(mobile);//没有获取到验证码就拉黑该手机号
                    }
                    System.out.println(result);
                    String mobilecode = StringUtil.getStringByRegex(result, "\\d{6}");
                    System.out.println("验证码:" + mobilecode + ":内容:" + result);
                    if (mobilecode.length() == 6) {
                        //检测是否通过验证
                        String checkChangeSuccess = mobileCodeMethod.checkMobileCode(mobile, mobilecode, cookieString);
                        if ("true".equals(checkChangeSuccess)) {
                            addIgnoreList(mobile);//
                            isSuccess = true;
                        }
                        else {
                            result = checkChangeSuccess;
                            isSuccess = false;
                        }
                    }
                }
                else {
                    result = sendResult;
                    if (result.contains("请您先对身份信息进行核验")) {
                        String releaseString = mobilecode.ReleaseMobile("", mobile, "", ""); //释放手机号
                        System.out.println(mobile + ":释放结果:" + releaseString);
                    }
                }
            }
            else {
                mobilecode.CancelSMSRecv("", mobile, "");
                result = mobileNoisEnable;
            }
        }
        else if (yitongGuoHeyan.contains("用户未登录")) {
            result = yitongGuoHeyan;
            isSuccess = false;
        }
        else {
            isSuccess = true;
        }
        return isSuccess + "|" + mobile + "|" + result;
    }

    /**
     * 获取到短信内容
     * 
     * @return
     * @time 2015年11月23日 下午1:21:18
     * @author chendong
     */
    private String getCodeString() {
        int getCodecount = 0;
        int maxCodecount = getMaxCodeCount();
        String result = "";
        do {
            try {
                Thread.sleep(3000);
            }
            catch (Exception e) {
            }
            //获取验证码并释放
            //获取短信验证码
            result = getVcodeAndReleaseMobile(mobile);//获取短信内容
            System.out.println(mobile + ":getCodecount(" + maxCodecount + "):" + getCodecount + ":result:" + result);
            getCodecount += 1;
        }
        while (getCodecount < maxCodecount && "no_data".equals(result));
        WriteLog.write("MobileCodeMethod", "mobile:" + mobile + ":getVcodeAndReleaseMobile:" + "getCodecount:"
                + getCodecount + ":result:" + result);
        return result;
    }

    /**
     * 获取短信最大次数
     * @return
     * @time 2015年11月23日 下午1:11:19
     * @author chendong
     */
    private int getMaxCodeCount() {
        int maxCodecount = 50;
        try {
            maxCodecount = Integer.parseInt(PropertyUtil
                    .getValue("getCodecount_String", "train.checkMobile.properties"));
        }
        catch (Exception e) {
        }
        return maxCodecount;
    }

    /**
     * 拉黑手机号
     * @time 2015年7月31日 下午5:36:39
     * @author chendong
     */
    private String addIgnoreList(String mobile) {
        String addignresult = "-1";
        addignresult = mobilecode.AddIgnoreList("", mobile, "", "");
        System.out.println(mobile + ":拉黑结果:" + addignresult);
        return addignresult;
    }

    /**
     * 获取短信内容
     * @param mobileno
     * @return
     * @time 2015年7月31日 下午4:44:14
     * @author chendong
     */
    private String getVcodeAndReleaseMobile(String mobileno) {
        String result = "";
        /*if ("1".equals(this.mobileCodeType)) {
            result = youmamobilecode.getVcodeAndReleaseMobile(uid, mobileno, pid, token, author_uid);
        }
        else if ("2".equals(this.mobileCodeType)) {
            result = taomamobilecode.getVcodeAndReleaseMobile(uid, mobileno, pid, author_pwd, author_uid);
        }*/
        result = mobilecode.getVcodeAndReleaseMobile("", mobileno, "", "", "");
        return result;
    }

    /**
     * 获取一个手机号
     * @param mobileCodeType2
     * @return
     * @time 2015年7月31日 下午4:08:06
     * @author chendong
     * @param mobileCodeMethod 
     * @param l1 
     */
    private String getOneMobileNo(MobileCodeMethod mobileCodeMethod, String password, String cookieString, Long l1) {
        String mobile = "";
        for (int i = 0; i < 30; i++) {
            try {
                if (i > 0) {
                    Thread.sleep(3000L);
                }
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
            mobile = mobilecode.GetMobilenum("", "", "");
            System.out.println("checkMobile:" + i + "次获取手机号:" + mobile);
            if (mobile.contains("no_data")) {
                continue;
            }
            else {
                break;
            }
        }
        return mobile;
    }

    /**
     * 获取一个手机号
     * @param getOneMobileNoCut
     * @time 2015年12月16日 下午4:08:06
     * @author Anki
     * @param mobileCodeMethod 
     * @param l1 
     */
    private String getOneMobileNoCut(MobileCodeMethod mobileCodeMethod, String password, String cookieString, Long l1) {
        do {
            String mobile = "";
            for (int i = 0; i < 1; i++) {
                try {
                    if (i > 0) {
                        Thread.sleep(3000L);
                    }
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
                mobile = mobilecode.GetMobilenum("", "", "");
                System.out.println("checkMobile:" + i + "次获取手机号:" + mobile);
                System.out.println(this.mobileCodeType.equals("31") ? "航天平台执行的获得手机号" : "同程平台执行的获得手机号");
                if (mobile.contains("no_data") || mobile.contains("申请号码失败")) {
                    continue;
                }
                else {
                    return mobile;
                }
            }
        }
        while (changeInitMobileCode(password, cookieString, l1)); //沒有則結束  。并重新初始化  handleMobileCodes  對象
        return mobile;
    }

    /**
     * 初始化配置文件中的手機號平台 || 用于切換
     * @time 2015年12月16日 上午12:25:19
     * @author Anki
     */
    public TreeSet<String> initConfigCodeTypes() {
        TreeSet<String> lists = null;
        String handleMobile = PropertyUtil.getValue("AutoHandleTurn_MobileCodeType", "train.reg.properties");
        if (!("".equals(handleMobile)) && handleMobile != null) {
            String[] mobiles = handleMobile.split("\\|");
            int len = mobiles.length;
            lists = new TreeSet<String>();
            for (int i = 0; i < len; i++) {
                if (!mobiles[i].equals(this.mobileCodeType)) {
                    lists.add(mobiles[i]);
                }
            }
        }
        return lists;
    }

    /**
     * 切换手机号获取平台
     * @time 2015年12月16日  15:03:15
     * @author Anki   
     */
    public boolean changeInitMobileCode(String password, String cookie, Long l1) { //=--------------------update
        if (this.handleMobileCodes != null) {
            if (handleMobileCodes.size() == 0) {
                this.handleMobileCodes = this.initConfigCodeTypes();
                return false;
            }
            else {
                String codeType = handleMobileCodes.pollFirst();
                if (codeType != null || !("".equals(codeType))) {
                    this.mobileCodeType = codeType;
                    this.mobilecode = MobileCodeBeanUtil.initMobileCodeType(this.mobileCodeType); //在一次初始化手機號平台
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @time 2015年8月19日 下午10:17:19
     * @author chendong
     */
    private String doedit(MobileCodeMethod mobileCodeMethod, String password, String cookieString) {
        Long l1 = System.currentTimeMillis();
        int maxRetryCount = 20;
        try {
            maxRetryCount = Integer.parseInt(PropertyUtil.getValue("maxRetryCount", "MobileCode.properties"));
        }
        catch (Exception e) {
        }
        String mobileNoisEnable = "-1";
        for (int i = 0; i < maxRetryCount; i++) {
            mobile = getOneMobileNo(mobileCodeMethod, password, cookieString, l1);
            System.out.println(l1 + ":" + i + "次获取手机号================mobileNoisEnable:" + mobileNoisEnable);
            if (mobile.length() == 11) {
                mobileNoisEnable = mobileCodeMethod.doEditTel(password, mobile, cookieString, 0); //修改绑定手机
                System.out.println(l1 + ":first================mobileNoisEnable:" + mobileNoisEnable);
                WriteLog.write("MobileCodeMethod", l1 + ":getMobileNoCount:" + i + ":mobile:" + mobile
                        + ":mobileNoisEnable:" + mobileNoisEnable);

                if ("true".equals(mobileNoisEnable)) { //修改成功
                    break;
                }

                else if (mobileNoisEnable.contains("已被其他用户用于在本网站注册用户")) {
                    String addignresult = addIgnoreList(mobile);//没有成功拉黑已经获取的手机号
                }
                else if (mobileNoisEnable.contains("已被其他用户用于在本网站注册用户") || mobileNoisEnable.contains("已被其他注册用户")) {
                    String addignresult = addIgnoreList(mobile);
                    continue;
                }
                else {
                    break;
                }
            }
        }
        return mobileNoisEnable;
    }

    //     * 访问个人中心,不需要结果
    public String initQueryUserInfo(String cookie) {
        String yitongGuoHeyan = "0";
        String param = "_json_att=";
        Map<String, String> header = RequestUtil.getRequestHeader(true, true, false);
        header.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        header.put("POST", "/otn/modifyUser/initQueryUserInfo HTTP/1.1");
        header.put("Host", "kyfw.12306.cn");
        header.put("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0");
        header.put("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3");
        header.put("Accept-Encoding", "gzip, deflate");
        header.put("Referer", "https://kyfw.12306.cn/otn/index/initMy12306");
        header.put("Cookie", cookie);
        header.put("Connection", "keep-alive");
        String html = "";
        if ("1".equals(useProxy)) {
            html = RequestUtil.submitProxy(INITQUERYUSERINFO_URL_STRING, param, "post", "utf-8", header, 0, proxyHost,
                    Integer.parseInt(proxyPort));
        }
        else {
            html = RequestUtil.post(INITQUERYUSERINFO_URL_STRING, param, "UTF-8", header, 0);
        }
        if (html.contains("已通过核验")) {
            yitongGuoHeyan = "1";
            mobile = getYiTongGuoMobile(html);
        }
        else if (html.contains("该用户已在其他地点登录，本次登录已失效")) {
            yitongGuoHeyan = "用户未登录";
        }
        return yitongGuoHeyan;
    }

    /**
     * 
     * @param html
     * @return
     * @time 2015年9月24日 下午1:48:20
     * @author chendong
     */
    private String getYiTongGuoMobile(String html) {
        String mobile = "";
        String[] htmls = html.split("class=\"info-item\"");
        for (int i = 0; i < htmls.length; i++) {
            String t_html = htmls[i];
            if (t_html.contains("手机号码")) {
                try {
                    mobile = t_html.split("class=\"con\">")[1];
                    mobile = mobile.substring(0, 11);
                    break;
                }
                catch (Exception e) {
                }
            }
            //            System.out.println(i + "------------" + htmls[i]);
        }
        return mobile;
    }

    //     * 请求绑定手机,不需要返回结果
    private void bindTel(String cookie) {
        Map<String, String> header = RequestUtil.getRequestHeader(true, true, false);
        header.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        header.put("POST", "/otn/modifyUser/initQueryUserInfo HTTP/1.1");
        header.put("Host", "kyfw.12306.cn");
        header.put("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0");
        header.put("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3");
        header.put("Accept-Encoding", "gzip, deflate");
        header.put("Referer", "https://kyfw.12306.cn/otn/index/initMy12306");
        header.put("Cookie", cookie);
        header.put("Connection", "keep-alive");
        String html = "";
        if ("1".equals(useProxy)) {
            html = RequestUtil.submitProxy(BINDTEL_URL_STRING, "", "post", "utf-8", header, 0, proxyHost,
                    Integer.parseInt(proxyPort));
        }
        else {
            html = RequestUtil.get(BINDTEL_URL_STRING, "UTF-8", header, 0);
        }
        //        System.out.println("bindTel:" + html);
    }

    //         * 修改绑定手机,需要返回
    //         * {"validateMessagesShowId":"_validatorMessage","status":true,"httpstatus":200,"data":{"flag":true},"messages":[],"validateMessages":{}}
    public String doEditTel(String password, String mobileno, String cookie, int count) {
        String param = "_loginPwd=" + password + "&mobile_no=" + mobileno;
        Map<String, String> header = RequestUtil.getRequestHeader(true, true, false);
        header.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        header.put("POST", "/otn/userSecurity/doEditTel HTTP/1.1");
        header.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        header.put("X-Requested-With", "XMLHttpRequest");
        header.put("Host", "kyfw.12306.cn");
        header.put("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0");
        header.put("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3");
        header.put("Accept-Encoding", "gzip, deflate");
        header.put("Referer", "https://kyfw.12306.cn/otn/userSecurity/bindTel");
        header.put("Cookie", cookie);
        header.put("Connection", "keep-alive");
        header.put("Pragma", "no-cache");
        header.put("Cache-Control", "no-cache");
        header.put("Content-Length", param.length() + "");
        String html = "-1";
        try {
            if ("1".equals(useProxy)) {
                html = RequestUtil.submitProxy(DOEDITTEL_URL_STRING, param, "post", "utf-8", header, 0, proxyHost,
                        Integer.parseInt(proxyPort));
            }
            else {
                html = RequestUtil.post(DOEDITTEL_URL_STRING, param, "UTF-8", header, 0);
            }
            if (html != null && html.contains("用户未登录")) {
            }
            else {
                String infoString = "doEditTel:" + mobileno + ":count:" + count;
                //                System.out.println(mobileno + "::::::::::::::::::::::::::::::" + infoString);
                //                System.out.println(mobileno + ":json:" + html);
                if (html.indexOf("已被其他用户用于在本网站注册用户") >= 0 || html.contains("已被其他注册用户")) {
                    return mobileno + ":已被其他用户用于在本网站注册用户";
                }
                else {
                    String jsonString = html;
                    if (jsonString.contains(("\"flag\":true"))) {
                        return "true";
                    }
                    /*JSONObject jsonObject1 = new JSONObject();
                    try {
                        jsonObject1 = JSONObject.parseObject(jsonString);
                    }
                    catch (Exception e) {
                        System.out.println("jsonString:" + jsonString);
                        e.printStackTrace();
                    }
                    if (jsonObject1 != null && jsonObject1.containsKey("data")) {
                        JSONObject dataJsonObject = jsonObject1.getJSONObject("data");
                        if (dataJsonObject.containsKey("flag") && dataJsonObject.getBooleanValue("flag")) {
                            return "true";
                        }
                    }*/
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return html;
    }

    //     * 发送短信
    //     * {"validateMessagesShowId":"_validatorMessage","status":true,"httpstatus":200,"data":{},"messages":[],"validateMessages":{}}
    private String getMobileCode(String mobileno, String cookie) {
        String param = "mobile=" + mobileno;
        Map<String, String> header = RequestUtil.getRequestHeader(true, true, false);
        header.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        header.put("POST", "/otn/userSecurity/getMobileCode HTTP/1.1");
        header.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        header.put("X-Requested-With", "XMLHttpRequest");
        header.put("Host", "kyfw.12306.cn");
        header.put("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0");
        header.put("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3");
        header.put("Accept-Encoding", "gzip, deflate");
        header.put("Referer", "https://kyfw.12306.cn/otn/userSecurity/bindTel");
        header.put("Cookie", cookie);
        header.put("Connection", "keep-alive");
        header.put("Pragma", "no-cache");
        header.put("Cache-Control", "no-cache");
        header.put("Content-Length", param.length() + "");
        String html;
        if ("1".equals(useProxy)) {
            html = RequestUtil.submitProxy(GETMOBILECODE_URL_STRING, param, "post", "utf-8", header, 0, proxyHost,
                    Integer.parseInt(proxyPort));
        }
        else {
            html = RequestUtil.post(GETMOBILECODE_URL_STRING, param, "UTF-8", header, 0);
        }
        String realString = "{\"validateMessagesShowId\":\"_validatorMessage\",\"status\":true,\"httpstatus\":200,\"data\":{},\"messages\":[],\"validateMessages\":{}}";
        if (html != null && realString.equals(html)) {
            return "true";
        }
        return html;
    }

    /*
     * 检测是否通过验证
     * String realString = "{\"validateMessagesShowId\":\"_validatorMessage\",\"status\":true,\"httpstatus\":200,\"data\":{\"flag\":true},\"messages\":[],\"validateMessages\":{}}";
     * 如果是错误的 会返回什么 验证码错误啥的
     */
    private String checkMobileCode(String mobileno, String mobilecode, String cookie) {
        String param = "mobile=" + mobileno + "&randCode=" + mobilecode;
        Map<String, String> header = RequestUtil.getRequestHeader(true, true, false);
        header.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        header.put("POST", "/otn/userSecurity/checkMobileCode HTTP/1.1");
        header.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        header.put("X-Requested-With", "XMLHttpRequest");
        header.put("Host", "kyfw.12306.cn");
        header.put("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0");
        header.put("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3");
        header.put("Accept-Encoding", "gzip, deflate");
        header.put("Referer", "https://kyfw.12306.cn/otn/userSecurity/bindTel");
        header.put("Cookie", cookie);
        header.put("Connection", "keep-alive");
        header.put("Pragma", "no-cache");
        header.put("Cache-Control", "no-cache");
        header.put("Content-Length", param.length() + "");
        String html = "";
        if ("1".equals(useProxy)) {
            html = RequestUtil.submitProxy(CHECKMOBILECODE_URL_STRING, param, "post", "utf-8", header, 0, proxyHost,
                    Integer.parseInt(proxyPort));
        }
        else {
            html = RequestUtil.post(CHECKMOBILECODE_URL_STRING, param, "UTF-8", header, 0);
        }
        System.out.println("checkMobileCode:" + html);
        String realString = "{\"validateMessagesShowId\":\"_validatorMessage\",\"status\":true,\"httpstatus\":200,\"data\":{},\"messages\":[],\"validateMessages\":{}}";
        if (html != null && realString.equals(html)) {
            return "true";
        }
        return html;
        //{"validateMessagesShowId":"_validatorMessage","status":true,"httpstatus":200,"data":{},"messages":[],"validateMessages":{}}
    }

}
