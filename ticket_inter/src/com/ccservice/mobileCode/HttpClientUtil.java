/**
 * 
 */
package com.ccservice.mobileCode;

import java.io.IOException;

import org.apache.commons.httpclient.HttpException;

import com.ccservice.b2b2c.atom.component.util.CCSGetMethod;
import com.ccservice.b2b2c.atom.component.util.CCSHttpClient;



/**
 * 
 * @time 2015年10月23日 下午11:06:34
 * @author chendong
 */
public class HttpClientUtil {

    /**
     * 
     * @time 2015年10月23日 下午11:03:15
     * @author chendong
     */
    public static String httpClientGet(String url) {
        String result = "-1";
        CCSGetMethod get = null;
        CCSHttpClient httpClient = new CCSHttpClient(false, 60000L, "*/*");
        get = new CCSGetMethod(url);
        get.setFollowRedirects(false);
        try {
            httpClient.executeMethod(get);
            result = get.getResponseBodyAsString();
            int statusCode = get.getStatusCode();
            //            System.out.println("statusCode==============>" + statusCode);
            //            System.out.println("==============responseBody==============");
            //            System.out.println("responseBody:" + responseBody);
        }
        catch (HttpException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
