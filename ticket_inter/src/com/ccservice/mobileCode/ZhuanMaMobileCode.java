/**
 * 
 */
package com.ccservice.mobileCode;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.elong.inter.PropertyUtil;

/**
 * 赚码
 * http://www.maz8.com/Default/apidoc.html
 * @time 2015年10月04日 下午10:16:14
 * @author chendong
 */
public class ZhuanMaMobileCode implements IMobileCode {

    public static void main(String[] args) {
        String pid = "3067";
        ZhuanMaMobileCode zhuanMamobilecode = ZhuanMaMobileCode.getInstance(pid);
        String mobile = zhuanMamobilecode.GetMobilenum("", "", "");
        //        String mobile = "13339734796";
        System.out.println(mobile);
        String result = "";
        result = zhuanMamobilecode.send(mobile, "999");
        System.out.println(result);
        //        for (int i = 0; i < args.length; i++) {
        //            result = zhuanMamobilecode.getVcodeAndReleaseMobile("", mobile, "", "", "");
        //            if ("no_data".equals(result)) {
        //                continue;
        //            }
        //            else {
        //                System.out.println(result);
        //                break;
        //            }
        //        }
        //添加黑名单
        //        result = zhuanMamobilecode.AddIgnoreList("", mobile, "", "");
        //获取当前用户正在使用的号码列表
        //        System.out.println(result);

    }

    //赚码平台登陆
    final static String LOGININURL = "http://www.maz8.com/DevApi/loginIn";

    //获取手机号码
    final static String GETMOBILENUMURL = "http://www.maz8.com/DevApi/getMobilenum";

    //获取验证码并释放
    final static String GETVCODEANDRELEASEMOBILEURL = "http://www.maz8.com/DevApi/getVcodeAndReleaseMobile";

    //获取验证码并释放
    final static String ADDIGNORELISTURL = "http://www.maz8.com/DevApi/addIgnoreList";

    //获取该用户所使用的号码和项目
    final static String GETRECVINGINFO = "http://www.maz8.com/DevApi/getRecvingInfo";

    /**
     * @param pid 4036(12306项目项目编号)
     * @return
     * @time 2015年9月21日 上午10:12:26
     * @author chendong
     */
    public static ZhuanMaMobileCode getInstance(String pid) {
        //        String pid = "3067";//12306项目项目编号
        String uid = PropertyUtil.getValue("ZhuanMa_uid", "MobileCode.properties");
        String author_uid = PropertyUtil.getValue("ZhuanMaAuthor_uid", "MobileCode.properties");//"cd1989929";//开发者用户名
        String author_pwd = PropertyUtil.getValue("ZhuanMaauthor_pwd", "MobileCode.properties");//"cd1989929";//开发者用户名
        ZhuanMaMobileCode ZhuanMamobilecode = new ZhuanMaMobileCode(pid, uid, author_uid, author_pwd);
        return ZhuanMamobilecode;
    }

    String pid;

    String uid;

    String author_uid;

    String author_pwd;

    String token;

    public ZhuanMaMobileCode(String pid, String uid, String author_uid, String author_pwd) {
        super();
        this.pid = pid;
        this.uid = uid;
        this.author_uid = author_uid;
        this.author_pwd = author_pwd;
        this.token = "X57tc0f/u11GuLghJmg5Yg7P2x12AVJoqxkJZHbISvYlXFW35trSlO0E5AtYPhnMVfPRN/gdEAk=";
    }

    /**
     * 
     * @param result
     * @return
     * @time 2015年7月29日 下午3:35:09
     * @author chendong
     */
    public String gettoken(String result, String key) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = JSONObject.parseObject(result);
        }
        catch (Exception e) {
            System.out.println("gettoken_err:result:" + result);
        }
        String Token = "-1";
        try {
            Token = jsonObject.getString(key);
        }
        catch (Exception e) {
        }
        return Token;
    }

    /**
     * /// <summary>
        /// 赚码平台登陆
        /// </summary>
        /// <param name="uid">用户名</param>
        /// <param name="pwd">密码</param>
        /// <returns></returns>
     */
    public String LoginIn(String uid, String pwd) {
        if (uid == null || pwd == null) {
            return "用户名或密码不能为空";
        }
        else {
            String paramContent = "uid=" + uid + "&pwd=" + pwd;
            //            System.out.println(LOGININURL);
            //            System.out.println(paramContent);
            //            {"Uid":120865,"Token":"31RroGrAw5Var/mSNgs6BlSbPonIBFVeK8iNDhAgrA5nDBtGQmyoYtc0qApnKJl%2Bvyv%2BQXO/js0=","InCome":0.000,"Balance":4.900,"UsedMax":20}
            String result = SendPostandGet.submitPost(LOGININURL, paramContent, "utf-8").toString();// .Post(API.LoginIn, String.Format("uid={0}&pwd={1}", uid, pwd));
            //            System.out.println(result);
            WriteLog.write("ZhuanMaMobileCode_LoginIn", paramContent + ":LoginIn:" + result);
            return result;
        }
    }

    /**
     * 获取手机号码
     * @param uid 登陆返回的用户ID（数值型的UID，不是用户名，如2684）
     * @param pid 项目ID
     * @param token 令牌
     * @return
     * @time 2015年7月29日 下午3:46:44
     * @author chendong
     */
    public String GetMobilenum(String uid, String pid, String token) {
        if (this.uid == null || this.pid == null || this.token == null) {
            return "用户名或密码不能为空";
        }
        else {
            String paramContent = "uid=" + this.uid + "&pid=" + this.pid + "&token=" + this.token;
            String result = SendPostandGet.submitPost(GETMOBILENUMURL, paramContent, "utf-8").toString();// .Post(API.LoginIn, String.Format("uid={0}&pwd={1}", uid, pwd));
            WriteLog.write("ZhuanMaMobileCode_GetMobilenum", paramContent + ":GetMobilenum:" + result);
            return result;
        }
    }

    /**
     * 获取验证码并保留
     * 
     * @param uid
     * @param mobile
     * @param next_pid
     * @param token
     * @return
     * @time 2015年7月29日 下午3:59:31
     * @author chendong
     */
    public String GetVcodeAndHoldMobilenum(String uid, String mobile, String next_pid, String token) {
        if (uid == null || token == null) {
            return "参数传入有误";
        }
        else {
            String result = "";
            return result;
        }
    }

    /**
     * 获取验证码并释放
     /// <param name="uid">用户ID （同获取号码）</param>
        /// <param name="mobile">手机号码</param>
        /// <param name="pid">项目ID</param>
        /// <param name="token">令牌</param>
        /// <param name="author_uid">开发者用户名</param>
     * 
     * @time 2015年7月29日 下午3:18:57
     * @author chendong
     */
    public String getVcodeAndReleaseMobile(String uid, String mobile, String pid, String token, String author_uid) {
        if (this.uid == null || this.pid == null || this.token == null) {
            return "用户名或密码不能为空";
        }
        else {
            String paramContent = "uid=" + this.uid + "&pid=" + this.pid + "&token=" + this.token + "&mobile=" + mobile
                    + "&author_uid=" + this.author_uid;
            String result = SendPostandGet.submitPost(GETVCODEANDRELEASEMOBILEURL, paramContent, "utf-8").toString();// .Post(API.LoginIn, String.Format("uid={0}&pwd={1}", uid, pwd));
            WriteLog.write("ZhuanMaMobileCode_getVcodeAndReleaseMobile",
                    paramContent + ":getVcodeAndReleaseMobile:" + result);
            return result;
        }
    }

    /// <summary>
    /// 添加黑名单
    /// </summary>
    /// <param name="uid">用户ID （同获取号码）</param>
    /// <param name="mobiles">以,号分隔的手机号列表</param>
    /// <param name="pid">项目ID</param>
    /// <param name="token">令牌</param>
    /// <returns></returns>
    public String AddIgnoreList(String uid, String mobiles, String pid, String token) {
        if (this.uid == null || this.pid == null || this.token == null) {
            return "参数传入有误";
        }
        else {
            String paramContent = "uid=" + this.uid + "&pid=" + this.pid + "&token=" + this.token + "&mobiles="
                    + mobiles;
            String result = SendPostandGet.submitPost(ADDIGNORELISTURL, paramContent, "utf-8").toString();// .Post(API.LoginIn, String.Format("uid={0}&pwd={1}", uid, pwd));
            WriteLog.write("ZhuanMaMobileCode_AddIgnoreList", paramContent + ":AddIgnoreList:" + result);
            return result;
        }
    }

    /// <summary>
    /// 获取当前用户正在使用的号码列表
    /// </summary>
    /// <param name="uid">用户ID （同获取号码）</param>
    /// <param name="pid">项目ID</param>
    /// <param name="token">令牌</param>
    /// <returns></returns>
    public String GetRecvingInfo(String uid, String pid, String token) {
        if (uid == null || pid == null || token == null) {
            return "参数传入有误";
        }
        else {
            String paramContent = "uid=" + uid + "&pid=" + pid + "&token=" + token;
            //            System.out.println(GETRECVINGINFO);
            //            System.out.println(paramContent);
            String result = SendPostandGet.submitPost(GETRECVINGINFO, paramContent, "utf-8").toString();// .Post(API.LoginIn, String.Format("uid={0}&pwd={1}", uid, pwd));
            WriteLog.write("ZhuanMaMobileCode_GetRecvingInfo", paramContent + ":GetRecvingInfo:" + result);
            return result;
        }
    }

    /// <summary>
    /// 取消所有短信接收，可立即解锁所有被锁定的金额
    /// </summary>
    /// <param name="uid">用户ID （同获取号码）</param>
    /// <param name="token">令牌</param>
    /// <returns></returns>
    public String CancelSMSRecvAll(String uid, String token) {
        if (uid == null || token == null) {
            return "参数传入有误";
        }
        else {
            String result = "";
            return result;
        }
    }

    /// <summary>
    /// 取消一个短信接收，可立即解锁被锁定的金额
    /// </summary>
    /// <param name="uid"></param>
    /// <param name="mobile"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    public String CancelSMSRecv(String uid, String mobile, String token) {
        if (uid == null || token == null) {
            return "参数传入有误";
        }
        else {
            String result = "";

            return result;
        }
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getAuthor_uid() {
        return author_uid;
    }

    public void setAuthor_uid(String author_uid) {
        this.author_uid = author_uid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAuthor_pwd() {
        return author_pwd;
    }

    public void setAuthor_pwd(String author_pwd) {
        this.author_pwd = author_pwd;
    }

    public String send(String mobile, String content) {
        //        http://api.zmyzm.com/apiGo.do?action=send&uid=用户名&token=登录时返回的令牌&pid=项目ID&mobile=号码&content=短信内容
        String paramContent = "uid=" + this.uid + "&pid=" + this.pid + "&token=" + this.token + "&mobiles=" + mobile;
        String result = SendPostandGet.submitPost(ADDIGNORELISTURL, paramContent, "utf-8").toString();// .Post(API.LoginIn, String.Format("uid={0}&pwd={1}", uid, pwd));
        WriteLog.write("ZhuanMaMobileCode_send", paramContent + ":send:" + result);
        return result;
    }

    @Override
    public String ReleaseMobile(String uid, String mobiles, String pid, String token) {
        // TODO Auto-generated method stub
        return null;
    }
}
