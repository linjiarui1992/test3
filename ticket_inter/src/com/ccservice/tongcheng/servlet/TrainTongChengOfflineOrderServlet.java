/**
 * 版权所有, 空铁无忧
 * Author: 火车票 H5-微信端 项目开发组
 * copyright: 2017
 */
package com.ccservice.tongcheng.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.offline.dao.TrainOfflineIdempotentDao;
import com.ccservice.offline.dao.TrainOrderOfflineDao;
import com.ccservice.offline.domain.TrainOfflineIdempotent;
import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.util.ExceptionTCUtil;
import com.ccservice.offline.util.HttpPostJsonUtil;
import com.ccservice.offline.util.TrainOrderOfflineUtil;
import com.ccservice.tongcheng.service.TrainTongChengOfflineAddOrderThread;

/**
 * @className: com.ccservice.tuniu.train.servlet.TrainTuNiuOfflineOrderServlet
 * @description: TODO - 
 * 
 * 同程线下票 - 同程线下票出票请求接口
 * 
 * 主要项目平台操作这些在cn_home里面
 * 
 * 
Method:HTTP-POST
Url:CreateOrder
DataType:json
 * 
 * isSuccess true用231000,false用231099
 * Exception
 * 同程区分了配送到站和配送票
 * 
你们传递的抢票单，里面，是不是也是会传很多这些定制信息？

抢票没有这些定制 内容的，
都是默认值

 * 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月15日 上午9:34:25 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainTongChengOfflineOrderServlet extends HttpServlet {
    private static final String LOGNAME = "同程线下票出票请求接口";

    private int r1 = new Random().nextInt(10000000);

    private TrainOrderOfflineDao trainOrderOfflineDao = new TrainOrderOfflineDao();

    private TrainOfflineIdempotentDao trainOfflineIdempotentDao = new TrainOfflineIdempotentDao();

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");

        BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
        String line = null;
        StringBuilder sb = new StringBuilder();
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }

        //将资料解码  
        String reqBody = sb.toString();
        //System.out.println(reqBody);

        //记录请求信息日志
        WriteLog.write(LOGNAME, r1 + ":同程线下票出票请求信息-reqBody-->" + reqBody);

        //幂等响应的初始化操作放在最开始接收到请求的地方

        //结果的返回
        JSONObject resResult = new JSONObject();

        PrintWriter out = response.getWriter();

        Boolean resultFlag = true;

        //同程没有加解密的设计，无需进行格式的替换反转
        //String reqBodyTemp = reqBody.substring(reqBody.indexOf("data")+7, reqBody.length()-2);

        //幂等的设计和实现
        String idempotentFlag = "TongChengOrder";

        //更换为数据库的持久化形式
        String idempotentLockKey = reqBody + idempotentFlag + "Lock";

        //String OrderLock = TrainOrderOfflineUtil.TnIdempotent.get(reqBody+idempotentFlag+"Lock");

        Integer PKID = 0;

        TrainOfflineIdempotent trainOfflineIdempotent = null;

        b: for (int i = 0; i < TrainOrderOfflineUtil.TNIDEMPOTENTRETRY; i++) {//幂等的后续处理-最多尝试三次 - 主要是跳出当前的并发处理逻辑-作为其它请求

            try {
                trainOfflineIdempotent = trainOfflineIdempotentDao
                        .findTrainOfflineIdempotentByLockKey(idempotentLockKey);
            }
            catch (Exception e1) {
                resResult = ExceptionTCUtil.handleTCException(e1);

                //记录请求信息日志
                WriteLog.write("同程线下票出票请求幂等判定", r1 + ":同程线下票出票请求幂等设计-幂等数据交互流程报错");

                out.print(resResult.toJSONString());
                out.flush();
                out.close();

                return;
            }

            if (trainOfflineIdempotent == null) {//锁定状态
                try {
                    PKID = Integer
                            .valueOf(trainOfflineIdempotentDao.addTrainOfflineIdempotent(idempotentLockKey, true));
                }
                catch (Exception e) {
                    resResult = ExceptionTCUtil.handleTCException(e);

                    //记录请求信息日志
                    WriteLog.write("同程线下票出票请求幂等判定", r1 + ":同程线下票出票请求幂等设计-幂等数据交互流程报错");

                    out.print(resResult.toJSONString());
                    out.flush();
                    out.close();

                    return;
                }

                //初始化之后进行赋值，方便后续使用
                try {
                    trainOfflineIdempotent = trainOfflineIdempotentDao.findTrainOfflineIdempotentByPKID(PKID);
                }
                catch (Exception e) {
                    resResult = ExceptionTCUtil.handleTCException(e);

                    //记录请求信息日志
                    WriteLog.write("同程线下票出票请求幂等判定", r1 + ":同程线下票出票请求幂等设计-幂等数据交互流程报错");

                    out.print(resResult.toJSONString());
                    out.flush();
                    out.close();

                    return;
                }

                //做二次健壮性判断
                Integer lockCount = 0;
                try {
                    lockCount = trainOfflineIdempotentDao.findCountByLockKey(idempotentLockKey);
                }
                catch (Exception e) {
                    ExceptionTCUtil.handleTCException(e);
                }
                if (lockCount > 1) {
                    //WriteLog.write("幂等判定流程测试", "进入重复请求处理流程-lockCount:"+lockCount);

                    //删除当前增加，并重新走b循环的流程
                    Integer flagTemp = trainOfflineIdempotentDao.delTrainOfflineIdempotentByPKID(PKID);
                    if (flagTemp < 1) {
                        //记录请求信息日志
                        WriteLog.write("同程线下票出票请求幂等判定", "同程线下票出票请求幂等设计-请求和反馈一致之后进行了清空操作---数据库清空失败");
                    }
                    continue b;
                }

                break b;
            }
            else {
                PKID = trainOfflineIdempotent.getPKID();

                trainOfflineIdempotent = TrainOrderOfflineUtil.idempotentGetPutReqNum(trainOfflineIdempotent);//其它次数的请求

                int loopNum = 1;

                a: while (true) {
                    String idempotentResultValue = trainOfflineIdempotent.getIdempotentResultValue();

                    //此处应做实时查询的动作之前，先进行一次判定
                    if (idempotentResultValue == null || "".equals(idempotentResultValue)) {
                        try {
                            idempotentResultValue = trainOfflineIdempotentDao.findResultValueByPKID(PKID);
                        }
                        catch (Exception e1) {
                            resResult = ExceptionTCUtil.handleTCException(e1);

                            //记录请求信息日志
                            WriteLog.write("同程线下票出票请求幂等判定", r1 + ":同程线下票出票请求幂等设计-幂等数据交互流程报错");

                            continue a;
                        }
                    }

                    //等待相同的结果出现之后直接进行反馈

                    if (idempotentResultValue == null || "".equals(idempotentResultValue)) {
                        //需要再取出一次做是否已删除的尝试判定 - 
                        //防止循环对象的提前删除
                        Boolean idempotentLockValue = null;
                        try {
                            idempotentLockValue = trainOfflineIdempotentDao.findLockValueByPKID(PKID);
                        }
                        catch (Exception e1) {
                            resResult = ExceptionTCUtil.handleTCException(e1);

                            //记录请求信息日志
                            WriteLog.write("同程线下票取消请求幂等判定", r1 + ":同程线下票取消请求幂等设计-幂等数据交互流程报错");

                            out.print(resResult.toJSONString());
                            out.flush();
                            out.close();

                            return;
                        }

                        if (idempotentLockValue == null) {//锁定状态
                            try {
                                Thread.sleep(3 * 1000);
                            }
                            catch (InterruptedException e) {
                                ExceptionTCUtil.handleTCException(e);
                            }
                            continue b;
                        }

                        try {
                            Thread.sleep(5 * 1000);
                        }
                        catch (InterruptedException e) {
                            ExceptionTCUtil.handleTCException(e);
                        }

                        loopNum++;

                        //记录请求信息日志
                        //WriteLog.write("同程线下票出票请求幂等判定", r1 + ":同程线下票出票请求幂等设计-等待反馈中:"+TrainOrderOfflineUtil.getNowDateStr());

                        //一分钟之后自动结束响应,避免报错之下引起的无限循环对程序本身造成困扰 - 在此处的可重复次数为 - 13次

                        if (loopNum >= TrainOrderOfflineUtil.TNIDEMPOTENTRETRY) {
                            Boolean isSuccess = false;
                            Integer msgCode = 231099;//isSuccess true用231000,false用231099

                            resResult = new JSONObject();

                            resResult.put("isSuccess", isSuccess);
                            resResult.put("msgCode", msgCode);

                            WriteLog.write(LOGNAME, r1 + ":同程线下票出票请求接口-报错之下引起的无限循环的卡断程序启动,接口反馈异常信息");

                            //resResult = JSONObject.parseObject(OrderHandleOverResultFlag);
                            resultFlag = false;

                            break b;
                        }

                        continue a;
                    }
                    else {
                        //幂等中的一次响应 - 该方法涉及到获取和存储，设计成同步的
                        //在这内里做了另外形式的判断
                        trainOfflineIdempotent = TrainOrderOfflineUtil.idempotentGetPutResNum(trainOfflineIdempotent);//减少次数的响应

                        if (trainOfflineIdempotent == null) {//锁定状态
                            try {
                                Thread.sleep(3 * 1000);
                            }
                            catch (InterruptedException e) {
                                ExceptionTCUtil.handleTCException(e);
                            }

                            continue b;
                        }

                        resResult = JSONObject.parseObject(idempotentResultValue);
                        resultFlag = false;

                        break b;
                    }
                }
            }

        }

        if (resultFlag) {
            resResult = new JSONObject();

            Boolean isSuccess = false;//程序是否执行成功
            Integer msgCode = 231099;//错误代码 true用231000,false用231099

            //没有相关的签名校验的逻辑 - 也就不用做相关的解密功能
            //校验通过的话,可以存储数据

            resResult = addTongChengOffline(resResult, reqBody, trainOfflineIdempotent);

            WriteLog.write(LOGNAME, r1 + ":同程线下票出票请求结果-resResult" + resResult);

        }

        out.print(resResult.toJSONString());
        out.flush();
        out.close();
    }

    private JSONObject addTongChengOffline(JSONObject resResult, String reqBody,
            TrainOfflineIdempotent trainOfflineIdempotent) {
        //非必传的值需要做非空判断

        Boolean isSuccess = false;//程序是否执行成功
        Integer msgCode = 231099;//错误代码 true用231000,false用231099

        JSONObject reqData = JSONObject.parseObject(reqBody);

        WriteLog.write(LOGNAME, r1 + ":同程线下票出票请求-进入到方法-addTongChengOffline--->reqData:" + reqData);

        //重复订单校验
        String orderNo = reqData.getString("orderNo");//同程订单号 - 例:FT599U57UF210D174002304643
        resResult.put("hsOrderNo", orderNo);

        if (orderNo == null || orderNo.equals("")) {
            resResult.put("isSuccess", isSuccess);
            resResult.put("msgCode", msgCode);
            resResult.put("msgInfo", "该订单不存在,请联系查询传递的订单号");

            //幂等的设计和实现
            TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, resResult.toJSONString(), r1);

            return resResult;
        }

        TrainOrderOffline trainOrderOffline = null;
        try {
            trainOrderOffline = trainOrderOfflineDao.findTrainOrderOfflineByOrderNumberOnline(orderNo);
        }
        catch (Exception e) {
            //幂等的设计和实现
            JSONObject jsonTemp = ExceptionTCUtil.handleTCException(e);
            TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, jsonTemp.toJSONString(), r1);
            return jsonTemp;
        }

        WriteLog.write(LOGNAME, r1 + ":同程线下票出票请求--orderNo-->" + orderNo + ",trainOrderOffline-->" + trainOrderOffline);

        if (trainOrderOffline != null) {
            resResult.put("isSuccess", isSuccess);
            resResult.put("msgCode", msgCode);
            resResult.put("msgInfo", "该订单已存在,请勿重复提交");

            //幂等的设计和实现
            TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, resResult.toJSONString(), r1);

            return resResult;
        }

        //此处另开线程处理出票请求超时的逻辑

        //如果不符合上述要求 - 执行入库流程操作 - 先做反馈
        new Thread(new TrainTongChengOfflineAddOrderThread(reqData)).start();

        /**
         * 
        {
        "isSuccess": true,
        "msgCode": 231000,
        "msgInfo": "请求已经接收",
        "data": {
        "orderNo": "tn17072220433404"
        }
        }
         * 
         */
        isSuccess = true;
        msgCode = 231000;//isSuccess true用231000,false用231099
        resResult.put("isSuccess", isSuccess);
        resResult.put("msgCode", msgCode);
        resResult.put("msgInfo", "请求已经接收");

        //幂等的设计和实现
        TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, resResult.toJSONString(), r1);

        return resResult;
    }

    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        HttpPostJsonUtil httpPostJsonUtil = new HttpPostJsonUtil();

        //String data = "{\"orderNo\":\"FT5A1176CF21094B2003928023\",\"contactName\":\"13592633810\",\"contactPhone\":\"13592633810\",\"ticketType\":0,\"orderType\":0,\"mailInfo\":{\"name\":\"贾永政\",\"mobile\":\"13653826511\",\"province\":\"河南省\",\"city\":\"郑州市\",\"district\":\"邙山区\",\"address\":\"迎宾路水境怡园11号楼\",\"expressType\":0},\"bookInfo\":{\"trainNo\":\"G423\",\"startStation\":\"郑州东\",\"endStation\":\"武汉\",\"startTime\":\"2017-11-22 09:59:00\",\"endTime\":\"2017-11-22 12:07:00\",\"customizeType\":5,\"customizeContent\":\"1/F\",\"acceptOtherSeat\":0,\"backupSeatClass\":\"\"},\"passengers\":[{\"id\":900093257,\"type\":1,\"name\":\"孙桂莲\",\"idType\":1,\"idNo\":\"410112195801084946\",\"gender\":2,\"seatClass\":4,\"ticketPrice\":244.00,\"issueFee\":5.00}]}";

        //送票到站的测试订单的信息
        /*String data = "{\"orderNo\":\"FT59B63596210D354010953318\",\"contactName\":\"测试\",\"contactPhone\":\"15370283333\","
                + "\"ticketType\":1,\"orderType\":0,"
                + "\"mailInfo\":{\"name\":\"送票到站测试单1\",\"mobile\":\"1801733161618017332626\",\"province\":\"天津\",\"city\":\"天津市\",\"district\":\"\",\"address\":\"天津站南进站口，前广场世纪联华超市为中心，向北方向走50米，西侧新龙超市（取票处）\",\"expressType\":1},"
                + "\"bookInfo\":{\"trainNo\":\"G7148\",\"startStation\":\"天津\",\"endStation\":\"苏州\",\"startTime\":\"2017-09-22 13:35:00\",\"endTime\":\"2017-09-22 14:09:00\",\"customizeType\":0,\"customizeContent\":\"\",\"acceptOtherSeat\":0,\"backupSeatClass\":\"\"},"
        
                //一个乘客
                //+ "\"passengers\":[{\"id\":898379388,\"type\":1,\"name\":\"送票到站测试单1\",\"idType\":3,\"idNo\":\"2727388383\",\"gender\":1,\"seatClass\":4,\"ticketPrice\":39.50,\"issueFee\":5.00}]}";
        
                //多个乘客
                + "\"passengers\":[{\"id\":898379388,\"type\":1,\"name\":\"送票到站测试单3-1\",\"idType\":3,\"idNo\":\"2727388383\",\"gender\":1,\"seatClass\":4,\"ticketPrice\":39.50,\"issueFee\":5.00},"
                + "{\"id\":898379388,\"type\":2,\"name\":\"送票到站测试单3-2\",\"idType\":3,\"idNo\":\"2727388383\",\"gender\":1,\"seatClass\":4,\"ticketPrice\":39.50,\"issueFee\":5.00},"
                + "{\"id\":898379388,\"type\":2,\"name\":\"送票到站测试单3-2\",\"idType\":3,\"idNo\":\"2727388383\",\"gender\":2,\"seatClass\":4,\"ticketPrice\":39.50,\"issueFee\":5.00}"
                + "]}";*/

        //送票到家的测试订单的信息
        /*String data = "{\"orderNo\":\"FT59B63596210D354010953311\",\"contactName\":\"测试\",\"contactPhone\":\"15370283333\","
                //+ "\"ticketType\":0,\"orderType\":0,"//正常订单
        
                + "\"ticketType\":0,\"orderType\":0,"//抢票订单不考虑定制信息 - //0: 普通订单,1: 抢票订单
        
                + "\"mailInfo\":{\"name\":\"邮寄测试单2\",\"mobile\":\"15370283333\",\"province\":\"上海\",\"city\":\"上海市\",\"district\":\"普陀区\",\"address\":\"中江路388号\",\"expressType\":0},"
        
                //无定制
                //+ "\"bookInfo\":{\"trainNo\":\"G7148\",\"startStation\":\"上海虹桥\",\"endStation\":\"苏州\",\"startTime\":\"2017-09-22 13:35:00\",\"endTime\":\"2017-09-22 14:09:00\",\"customizeType\":0,\"customizeContent\":\"\",\"acceptOtherSeat\":0,\"backupSeatClass\":\"\"},"
        
                //有定制
                + "\"bookInfo\":{\"trainNo\":\"G7148\",\"startStation\":\"上海虹桥\",\"endStation\":\"苏州\",\"startTime\":\"2017-09-22 13:35:00\",\"endTime\":\"2017-09-22 14:09:00\",\"customizeType\":7,\"customizeContent\":\"3/1|1/1\",\"acceptOtherSeat\":0,\"backupSeatClass\":\"\"},"
        
                //一个乘客
                //+ "\"passengers\":[{\"id\":898379388,\"type\":1,\"name\":\"邮寄测试单1\",\"idType\":3,\"idNo\":\"2727388383\",\"gender\":1,\"seatClass\":4,\"ticketPrice\":39.50,\"issueFee\":5.00}]}";
        
                //多个乘客
                + "\"passengers\":[{\"id\":898379388,\"type\":1,\"name\":\"邮寄测试单3-1\",\"idType\":3,\"idNo\":\"2727388383\",\"gender\":1,\"seatClass\":4,\"ticketPrice\":39.50,\"issueFee\":5.00},"
                + "{\"id\":898379388,\"type\":2,\"name\":\"邮寄测试单3-2\",\"idType\":3,\"idNo\":\"2727388383\",\"gender\":1,\"seatClass\":4,\"ticketPrice\":39.50,\"issueFee\":5.00},"
                + "{\"id\":898379388,\"type\":2,\"name\":\"邮寄测试单3-2\",\"idType\":3,\"idNo\":\"2727388383\",\"gender\":2,\"seatClass\":4,\"ticketPrice\":39.50,\"issueFee\":5.00}"
                + "]}";*/

        //闪送【送票到家】的测试订单的信息
        String data = "{\"orderNo\":\"FT59B63596210D354010953401\",\"contactName\":\"测试\",\"contactPhone\":\"15370283333\","
                + "\"ticketType\":0,\"orderType\":0,"//正常订单 - //0: 普通订单,1: 抢票订单

                //+ "\"mailInfo\":{\"name\":\"邮寄测试单2\",\"mobile\":\"15370283333\",\"province\":\"河南\",\"city\":\"郑州市\",\"district\":\"高新区\",\"address\":\"中江路388号\",\"expressType\":0},"

                // + "\"mailInfo\":{\"name\":\"邮寄测试单2\",\"mobile\":\"15370283333\",\"province\":\"河南\",\"city\":\"郑州市\",\"district\":\"二七区\",\"address\":\"二七纪念塔388号\",\"expressType\":0},"
                + "\"mailInfo\":{\"name\":\"邮寄测试单2\",\"mobile\":\"15370283333\",\"province\":\"河南\",\"city\":\"郑州市\",\"district\":\"二七区\",\"address\":\"庆丰街与淮河路北20米路东\",\"expressType\":0},"

                //无定制
                //+ "\"bookInfo\":{\"trainNo\":\"G7148\",\"startStation\":\"上海虹桥\",\"endStation\":\"苏州\",\"startTime\":\"2017-09-22 13:35:00\",\"endTime\":\"2017-09-22 14:09:00\",\"customizeType\":0,\"customizeContent\":\"\",\"acceptOtherSeat\":0,\"backupSeatClass\":\"\"},"

                //有定制
                + "\"bookInfo\":{\"trainNo\":\"G7148\",\"startStation\":\"上海虹桥\",\"endStation\":\"苏州\",\"startTime\":\"2017-11-23 13:35:00\",\"endTime\":\"2017-11-24 14:09:00\",\"customizeType\":7,\"customizeContent\":\"3/1|1/1\",\"acceptOtherSeat\":0,\"backupSeatClass\":\"\"},"

                //一个乘客
                //+ "\"passengers\":[{\"id\":898379388,\"type\":1,\"name\":\"邮寄测试单1\",\"idType\":3,\"idNo\":\"2727388383\",\"gender\":1,\"seatClass\":4,\"ticketPrice\":39.50,\"issueFee\":5.00}]}";

                //多个乘客
                + "\"passengers\":[{\"id\":898379388,\"type\":1,\"name\":\"邮寄测试单3-1\",\"idType\":3,\"idNo\":\"2727388383\",\"gender\":1,\"seatClass\":4,\"ticketPrice\":39.50,\"issueFee\":5.00},"
                + "{\"id\":898379388,\"type\":2,\"name\":\"邮寄测试单3-2\",\"idType\":3,\"idNo\":\"2727388383\",\"gender\":1,\"seatClass\":4,\"ticketPrice\":39.50,\"issueFee\":5.00},"
                + "{\"id\":898379388,\"type\":2,\"name\":\"邮寄测试单3-2\",\"idType\":3,\"idNo\":\"2727388383\",\"gender\":2,\"seatClass\":4,\"ticketPrice\":39.50,\"issueFee\":5.00}"
                + "]}";

        //送票到家的抢票测试订单的信息
        /*String data = "{\"orderNo\":\"FT59B63596210D354010952952\",\"contactName\":\"邮寄测试单2\",\"contactPhone\":\"15370283333\","
                //+ "\"ticketType\":0,\"orderType\":0,"//正常订单
                
                + "\"ticketType\":0,\"orderType\":1,"//抢票订单不考虑定制信息 - //0: 普通订单,1: 抢票订单
                
                + "\"mailInfo\":{\"name\":\"邮寄测试单2\",\"mobile\":\"15370283333\",\"province\":\"河南省\",\"city\":\"新乡市\",\"district\":\"卫滨区\",\"address\":\"河南省新乡市牧野区东干道360号\",\"expressType\":0},"
                
                //无定制
                + "\"bookInfo\":{\"trainNo\":\"K7804\",\"startStation\":\"太原\",\"endStation\":\"太原东\",\"startTime\":\"2017-10-27 08:55:00\",\"endTime\":\"2017-09-30 09:01:00\",\"customizeType\":0,\"customizeContent\":\"\",\"acceptOtherSeat\":0,\"backupSeatClass\":\"\",\"grabEndTime\":\"2017-09-26 20:20:00\",\"grabSeatClass\":\"硬卧,硬座,无座\"},"
                
                //一个乘客
                //+ "\"passengers\":[{\"id\":898379388,\"type\":1,\"name\":\"邮寄测试单1\",\"idType\":3,\"idNo\":\"2727388383\",\"gender\":1,\"seatClass\":4,\"ticketPrice\":39.50,\"issueFee\":5.00}]}";
        
                //多个乘客
                + "\"passengers\":[{\"id\":898379388,\"type\":1,\"name\":\"邮寄测试单3-1\",\"idType\":3,\"idNo\":\"2727388383\",\"gender\":1,\"seatClass\":4,\"ticketPrice\":39.50,\"issueFee\":5.00},"
                + "{\"id\":898379388,\"type\":2,\"name\":\"邮寄测试单3-2\",\"idType\":1,\"idNo\":\"111222190001013333\",\"gender\":1,\"seatClass\":4,\"ticketPrice\":39.50,\"issueFee\":5.00},"
                + "{\"id\":898379388,\"type\":2,\"name\":\"邮寄测试单3-2\",\"idType\":3,\"idNo\":\"2727388383\",\"gender\":2,\"seatClass\":4,\"ticketPrice\":39.50,\"issueFee\":5.00}"
                + "]}";*/

        System.out.println(data);

        //本地测试地址
        //String url = "http://localhost:8097/ticket_inter/TrainTongChengOfflineOrder";
        //测试环境地址
        //String url = "http://121.40.226.72:9007/ticket_inter/TrainTongChengOfflineOrder";
        //String url = "http://peisong.test.51kongtie.com/ticket_inter/TrainTongChengOfflineOrder";

        //正式环境地址
        //String url = "http://121.40.241.126:9020/ticket_inter/TrainTongChengOfflineOrder";
        String url = "http://ws.peisong.51kongtie.com/ticket_inter/TrainTongChengOfflineOrder";

        System.out.println(httpPostJsonUtil.doPost(url, data));

        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间: " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间: " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间: " + runTimeS / 60 + "min:" + runTimeS % 60 + "s");//分
    }

}
