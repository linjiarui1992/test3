/**
 * 版权所有, 空铁无忧
 * Author: 火车票 H5-微信端 项目开发组
 * copyright: 2017
 */
package com.ccservice.tongcheng.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.PropertyUtil;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.offline.dao.T_CUSTOMERAGENTDao;
import com.ccservice.offline.dao.TrainOrderOfflineDao;
import com.ccservice.offline.dao.TrainOrderOfflineRecordDao;
import com.ccservice.offline.dao.TrainPassengerOfflineDao;
import com.ccservice.offline.dao.TrainTicketOfflineDao;
import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.domain.TrainPassengerOffline;
import com.ccservice.offline.domain.TrainTicketOffline;
import com.ccservice.offline.util.ExceptionTCUtil;
import com.ccservice.offline.util.HttpPostJsonUtil;
import com.ccservice.offline.util.TrainOrderOfflineUtil;

/**
 * @className: com.ccservice.tuniu.train.servlet.TrainTuNiuOfflineOrderServlet
 * @description: TODO - 
 * 
 * 同程线下票 - 同程线下票出票回调请求接口 - 由CN平台发起
 * 
 * 

//票价是否需要核验？ - 在出票回调中根据金额完成
//获取总票价的Double类型 - 做价格校验

 * 计算总票价 - 总票价之一 - InsurePrice不涉及折半优惠 - 可以放在外面直接计算 - InsurePrice * TicketCount
 * 放到外面算 - 也要判定其存在 的 业务逻辑 - 比较麻烦，不如直接放在内部加和计算
 * 
 * 总票价
 *  成人票的票价 + 儿童票的【半票价】 + 保险的票价 + 【邮寄的票价 - 暂无】
 *  
 *  TotalPrice - [需要订单提交的时候，即进行计算] - 来自车票中的价格的相关计算

//BigDecimal TotalInsurePrice = new BigDecimal(InsurePrice).multiply(new BigDecimal(TicketCount));
//TotalPrice = TotalPrice.add(TotalInsurePrice);

 * 
 * 
 * success true用231000，false用231099
 * 
 * 出票成功的话，需要取消掉出票回调请求的自动取消的定时任务 - 只有等待出票状态的单子才会完成自动解锁
 * 
 * 更新快递时效信息
 * 
 * 新增相关的校验的判定
 * 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月15日 上午9:34:25 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainTongChengOfflineOrderCallbackSuccessServletCN extends HttpServlet {
    private static final String LOGNAME = "同程线下票出票回调请求接口";

    private int r1 = new Random().nextInt(10000000);

    private TrainOrderOfflineDao trainOrderOfflineDao = new TrainOrderOfflineDao();
    private TrainTicketOfflineDao trainTicketOfflineDao = new TrainTicketOfflineDao();
    private TrainPassengerOfflineDao trainPassengerOfflineDao = new TrainPassengerOfflineDao();
    private TrainOrderOfflineRecordDao trainOrderOfflineRecordDao = new TrainOrderOfflineRecordDao();//记录操作日志，单独封装工具类
    private T_CUSTOMERAGENTDao tCUSTOMERAGENTDao = new T_CUSTOMERAGENTDao();//记录操作日志，单独封装工具类

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");

        BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));  
        String line = null;  
        StringBuilder sb = new StringBuilder();  
        while((line = br.readLine())!=null){  
            sb.append(line);  
        }
   
        //将资料解码  
        String reqBody = sb.toString();
        //System.out.println(reqBody);

        //记录请求信息日志
        WriteLog.write(LOGNAME, r1 + ":同程线下票出票回调请求接口请求信息-reqBody-->"+reqBody);

        JSONObject requestBody = JSONObject.parseObject(reqBody);
        
        Long orderId = requestBody.getLong("orderId");
        Integer useridi = requestBody.getInteger("userid");
        
        TrainOrderOfflineUtil.CreateBusLogAdmin(orderId, useridi, "发起出票回调请求-出票成功");

        JSONObject result = orderCallbackCN(orderId, useridi);
        
        if (!result.getBooleanValue("success")) {
            TrainOrderOfflineUtil.CreateBusLogAdmin(orderId, useridi, result.getString("msg"));
        }
        
        WriteLog.write(LOGNAME, r1 + ":同程线下票出票回调请求结果-result" + result);

        PrintWriter out = response.getWriter();

        out.print(result.toJSONString());
        out.flush();
        out.close();
    }

    private JSONObject orderCallbackCN(Long orderIdl, Integer useridi) {
        JSONObject resResult = new JSONObject();

        WriteLog.write(LOGNAME, r1 + ":同程线下票出票回调请求-进入到方法-orderCallbackCN");

        if (orderIdl == null || orderIdl == 0L) {
            resResult.put("success", "false");
            resResult.put("msg", "传入的订单号有误，请排查");
            return resResult;
        }
        
        TrainOrderOffline trainOrderOffline = null;
        try {
            trainOrderOffline = trainOrderOfflineDao.findTrainOrderOfflineById(orderIdl);
        }
        catch (Exception e) {
            return ExceptionTCUtil.handleTCException(e);
        }

        if (trainOrderOffline == null) {
            resResult.put("success", "false");
            resResult.put("msg", "该订单不存在，请排查");
            return resResult;
        }

        if (trainOrderOffline.getOrderStatus() == 2) {
            resResult.put("success", "false");
            resResult.put("msg", "该订单已是出票状态，请联系客服处理");
            return resResult;
        }

        if (trainOrderOffline.getOrderStatus() == 3) {
            resResult.put("success", "false");
            resResult.put("msg", "该订单已被拒单或者取消，请联系客服处理");
            return resResult;
        }
        
        String orderId = trainOrderOffline.getOrderNumberOnline();

        HttpPostJsonUtil httpPostJsonUtil = new HttpPostJsonUtil();

        JSONObject dataJson = new JSONObject();
        
        /**
         * 
 SELECT * from TrainTicketOffline where OrderId=" + orderId;
 String sql1 = "UPDATE TrainTicketOffline SET Coach='" + coach + "',SeatNo='" + seatno + "',sealPrice="
                        + priceString + ",ticketNo='" + ticketNo + "',realSeat='" + extString + "' WHERE TrainPid="
                        + TrainPid;

                WriteLogTNUtil.write("TN线下票出票回调成功结果", "sql1:" + sql1); - 在上一步中已经完成了入库的操作
         * 
做超时时间校验
做价格校验
         * 
         */
        /*String ticketNo = "E000000";//出票成功之后回调的票号
        //String seatType = "软卧";//出票成功之后席别
        String seatNo = "14车厢，19座上铺";//14车厢，19座上铺
        
        //快递信息
        String deliveryCompanyName = "顺丰速运";//
        String trackingNumber = "666666666";//快递单号
        Double cost = 15.50;//*/
        
        dataJson.put("orderNo", orderId);
        
        JSONArray passengers = new JSONArray();
        
        List<TrainPassengerOffline> trainPassengerOfflineList = null;
        try {
            trainPassengerOfflineList = trainPassengerOfflineDao.findTrainPassengerOfflineListByOrderId(orderIdl);
        }
        catch (Exception e) {
            resResult = ExceptionTCUtil.handleTCException(e);
            return resResult;
        }

        if (trainPassengerOfflineList == null) {
            String content = "无法查询获取到出票回调的乘客对象";
            TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, content);
            //throw new RuntimeException("");
        }
        
        //System.out.println(trainPassengerOfflineList);

        //价格校验 - 在前台完成了
        
        //判定总价 - 判定儿童票是否半价
        
        String departureDate = "";//在出票回调的地方可能会使用到
        
        for (int i = 0; i < trainPassengerOfflineList.size(); i++) {
            JSONObject passenger = new JSONObject();
            
            TrainPassengerOffline trainPassengerOffline = trainPassengerOfflineList.get(i);
            
            passenger.put("passengerId", Integer.valueOf(trainPassengerOffline.getPassengerId()));
            
            TrainTicketOffline trainTicketOffline = null;
            try {
                trainTicketOffline = trainTicketOfflineDao.findTrainTicketOfflineByTrainPid(trainPassengerOffline.getId());
            }
            catch (Exception e) {
                resResult = ExceptionTCUtil.handleTCException(e);
                return resResult;
            }

            if (trainTicketOffline == null) {
                String content = "无法查询获取到出票回调的车票对象";
                TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, content);
                //throw new RuntimeException("");
            }
            
            //passenger.put("ticketNo", trainTicketOffline.getTicketNo());
            
            departureDate = trainTicketOffline.getDepartTime().substring(0, 10);//出发日期 - 2016-06-28 18:10:00.000 否   假驳回重新出票回填发车日期.格式：yyyy-MM-dd
            
            String realSeat = trainTicketOffline.getRealSeat();
            if (realSeat == null) {
                passenger.put("zwCode", "");
            } else {
                passenger.put("zwCode", TrainOrderOfflineUtil.getSeatTypeCode(realSeat));
            }
            
            passenger.put("zwName", trainTicketOffline.getRealSeat());
            
            passenger.put("carriageNo", trainTicketOffline.getCoach());
            
            Double sealPrice = trainTicketOffline.getSealPrice();
            
            if (sealPrice == null) {
                passenger.put("ticketPrice", "");
            } else {
                passenger.put("ticketPrice", sealPrice);
            }
            

            String SeatType = trainTicketOffline.getSeatType();
            if (SeatType == null) {
                SeatType = "";
            }
            
            /**
             * 动卧和高级动卧的特殊处理 - 
else if (SeatType.equals("动卧上铺")) {
    seatTypeCode = 15;
}
else if (SeatType.equals("动卧下铺")) {
    seatTypeCode = 16;
}
else if (SeatType.equals("高级动卧上铺")) {
    seatTypeCode = 17;
}
else if (SeatType.equals("高级动卧下铺")) {
    seatTypeCode = 18;
}
             * 
             */
            int seatClass = 0;

            String SeatNo = trainTicketOffline.getSeatNo();//座位号 - 25下
            
            if (SeatType.equals("动卧")) {
                if (SeatNo.contains("上")) {
                    SeatNo = SeatNo.replace("上", "");
                    
                    SeatType = "动卧上铺";
                    seatClass = TrainOrderOfflineUtil.getSeatClassByTCSeatType(SeatType);
                } else if (SeatNo.contains("下")) {
                    SeatNo = SeatNo.replace("下", "");
                    
                    SeatType = "动卧下铺";
                    seatClass = TrainOrderOfflineUtil.getSeatClassByTCSeatType(SeatType);
                } else {
                    String content = "动卧席别未给出对应的上下铺标识";
                    TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, content);

                    resResult.put("success", "false");
                    resResult.put("msg", content);

                    //记录请求信息日志
                    WriteLog.write(LOGNAME, r1 + ":同程线下票出票回调请求反馈信息-resResult" + resResult);

                    trainOrderOfflineDao.updateTrainOrderOfflineStatusQuestionById(orderIdl, 1, 1);//出票问题订单
                    
                    return resResult;
                }
                
            } else if (SeatType.equals("高级动卧")) {
                if (SeatNo.contains("上")) {
                    SeatNo = SeatNo.replace("上", "");
                    
                    SeatType = "高级动卧上铺";
                    seatClass = TrainOrderOfflineUtil.getSeatClassByTCSeatType(SeatType);
                } else if (SeatNo.contains("下")) {
                    SeatNo = SeatNo.replace("下", "");
                    
                    SeatType = "高级动卧下铺";
                    seatClass = TrainOrderOfflineUtil.getSeatClassByTCSeatType(SeatType);
                } else {
                    String content = "高级动卧席别未给出对应的上下铺标识";
                    TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, content);

                    resResult.put("success", "false");
                    resResult.put("msg", content);

                    //记录请求信息日志
                    WriteLog.write(LOGNAME, r1 + ":同程线下票出票回调请求反馈信息-resResult" + resResult);

                    trainOrderOfflineDao.updateTrainOrderOfflineStatusQuestionById(orderIdl, 1, 1);//出票问题订单
                    
                    return resResult;
                }
            } else {
                seatClass = TrainOrderOfflineUtil.getSeatClassByTCSeatType(SeatType);
            }
            passenger.put("seatNo", SeatNo);
            passenger.put("seatClass", seatClass);
            
            passengers.add(passenger);
        }

        dataJson.put("passengerTickets", passengers);
        
        //获取代售点的名称并进行相关内容的反馈 - 通过 orderIdl 取到agentId之后，取到代售点的全称
        String ticketAgency = null;
        try {
            ticketAgency = tCUSTOMERAGENTDao.findCityNameByOrderId(orderIdl);
        }
        catch (Exception e2) {
            resResult = ExceptionTCUtil.handleTCException(e2);
            return resResult;
        }
        if (ticketAgency == null) {
            ticketAgency = "代售点城市名称获取失败";
        }
        dataJson.put("ticketAgency", ticketAgency);//ticketAgency  string  否   代售点名称

        /**
         * 由于此处是所有的结果反馈 - 
         * 
         * departureDate   String  否   假驳回重新出票回填发车日期.格式：yyyy-MM-dd
         * 所有的假驳回的二次进单的日期变更
         * 
         */
        Boolean isSecondDateCallback = trainOrderOffline.getIsSecondDateCallback();//是否是二次日期替换之后的回调 - 默认是false - true-二次进单，且日期发生了变化【同程20171108出票回调日期更新新增需求】
        if (isSecondDateCallback == null) {
            isSecondDateCallback = false;
        }
        
        if (isSecondDateCallback) {
            dataJson.put("departureDate", departureDate);//String  否   假驳回重新出票回填发车日期.格式：yyyy-MM-dd - 2017-09-22 13:35:00.000
        }
        
        String data = dataJson.toJSONString();
        
        //记录请求信息日志
        WriteLog.write(LOGNAME, r1 + ":同程线下票出票回调请求反馈信息-dataJson" + data);

        String tongChengOfflineCallbackSuccessURL = PropertyUtil.getValue("TongChengOfflineCallbackSuccessURL", "Train.GuestAccount.properties");
        
        String res = "";
        
        /**
         * 
{"isSuccess":false,"msgCode":106315,"msgInfo":"订单不存在","executeTime":"53"}
{"isSuccess":false,"msgCode":106410,"msgInfo":"订单状态不正确","executeTime":"401"}
         * 
         */
        for (int i = 0; i < TrainOrderOfflineUtil.TNRETRY; i++) {
            try {
                res = httpPostJsonUtil.doPost(tongChengOfflineCallbackSuccessURL, data);//{"success":true}
            }
            catch (Exception e1) {
                String content = "同程线下票出票回调请求失败";
                TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, content);
                ExceptionTCUtil.handleTCException(e1);
            }
            
            //{"isSuccess":false,"msgCode":106315,"msgInfo":"订单不存在","executeTime":"4"}

            WriteLog.write(LOGNAME, r1 + ":同程线下票出票回调请求-res:"+res);
            
            //pickupNo  string  否   送票到站取票号（只有送票到站才有） - 但是对于我们来说，没什么用处，不需要了 - 信息是同程发的

            if (res != null && res.contains("true")) {//重试请求三次
                //{"pickupNo":"","isSuccess":true,"msgCode":106100,"msgInfo":"票台订单出票成功","executeTime":"706"}
                
                resResult.put("success", "true");

                //更新出票点的相关信息
                //修改订单状态和出票时间 - ChuPiaoTime - OrderStatus
                try {
                    trainOrderOfflineDao.updateTrainOrderOfflineSuccessById(orderIdl, useridi);
                }
                catch (Exception e) {
                    return ExceptionTCUtil.handleTCException(e);
                }

                //记录日志记录
                String content = "出票成功，回调成功";
                TrainOrderOfflineUtil.CreateBusLogAdminOtherDealResult(orderIdl, useridi, content, 1);//1 - 订单出票完成
                
                //更新票的状态
                //订单状态 - 1-等待出票 - 2-出票成功 - 3-已拒单【已取消】 - 其他值都是无效状态 - 可操作-是在页面上进行的判断处理
                //questionDraw = "1"：出票采购问题/邮寄采购问题 questionDraw = "2" //出票成功已邮寄/出票成功待邮寄 - 采购问题订单
                trainOrderOfflineDao.updateTrainOrderOfflineStatusQuestionById(orderIdl, 2, 2);
                
                //更新响应时间
                try {
                    trainOrderOfflineRecordDao.updateTrainOrderOfflineRecordResponseTimeByOrderIdAndProviderAgentid(orderIdl, useridi);
                }
                catch (Exception e) {
                    return ExceptionTCUtil.handleTCException(e);
                }

                resResult.put("msg", content);

                //记录请求信息日志
                WriteLog.write(LOGNAME, r1 + ":同程线下票出票回调请求反馈信息-resResult" + resResult);

                return resResult;
            } else {
                //对于已知结果反馈 - 无需重试
                
                //{"isSuccess":false,"msgCode":106410,"msgInfo":"订单状态不正确","executeTime":"28"}
                //{"isSuccess":false,"msgCode":106315,"msgInfo":"订单不存在","executeTime":"53"}

                if (res == null || res.equals("")) {
                    if (i == (TrainOrderOfflineUtil.TNRETRY-1)) {
                        //记录日志
                        TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "由于网络原因，出票回调请求反馈失败，稍后重试或者联系技术处理");
                        
                        resResult.put("success", "false");
                        resResult.put("msg", "由于网络原因，导致同程的出票回调请求接口无法访问，请您暂且不要操作，稍后重试或者联系客服处理");

                        //记录请求信息日志
                        WriteLog.write(LOGNAME, r1 + ":同程线下票出票回调请求反馈信息-resResult" + resResult);

                        trainOrderOfflineDao.updateTrainOrderOfflineStatusQuestionById(orderIdl, 1, 1);//出票问题订单
                        
                        return resResult;
                    }
                    //记录操作记录
                    TrainOrderOfflineUtil.sleep(10*1000);
                    continue;
                }

                if (res.contains("订单不存在") || res.contains("订单状态不正确")) {
                    JSONObject resObject = JSONObject.parseObject(res);

                    //记录日志记录
                    String content = resObject.getString("msgInfo");
                    TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, content);
                    
                    //更新票的状态
                    //订单状态 - 1-等待出票 - 2-出票成功 - 3-已拒单【已取消】 - 其他值都是无效状态 - 可操作-是在页面上进行的判断处理
                    //questionDraw = "1"：出票采购问题/邮寄采购问题 questionDraw = "2" //出票成功已邮寄/出票成功待邮寄 - 采购问题订单
                    trainOrderOfflineDao.updateTrainOrderOfflineStatusQuestionById(orderIdl, 1, 1);//出票问题订单
                    
                    resResult.put("success", resObject.getBoolean("isSuccess"));
                    resResult.put("msg", content);

                    //记录请求信息日志
                    WriteLog.write(LOGNAME, r1 + ":同程线下票出票回调请求反馈信息-resResult" + resResult);

                    return resResult;
                }

                resResult.put("success", "false");
                resResult.put("msg", "未知原因");
                
                if (res != null && !res.equals("")) {
                    JSONObject resJson = JSONObject.parseObject(res);
                    resResult.put("msg", resJson.getString("msgInfo"));
                }
                return resResult;
            }
        }

        //记录请求信息日志
        WriteLog.write(LOGNAME, r1 + ":同程线下票出票回调请求反馈信息-resResult" + resResult);

        return resResult;
    }

    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        HttpPostJsonUtil httpPostJsonUtil = new HttpPostJsonUtil();
        
        String data = "{\"userid\":382,\"orderId\":1290350}";//{"userid":382,"orderId":1290350}
        
        System.out.println(data);
        
        //本地测试地址
        String url = "http://localhost:8097/ticket_inter/TrainTongChengOfflineOrderCallbackSuccessServletCN";
        //测试环境地址
        //String url = "http://121.40.226.72:9007/ticket_inter/TrainTongChengOfflineOrderCallbackSuccessServletCN";
        //正式环境地址
        //String url = "http://121.40.241.126:9010/ticket_inter/TrainTongChengOfflineOrderCallbackSuccessServletCN";
        
        System.out.println(httpPostJsonUtil.doPost(url, data));


        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间： " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间： " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间： " + runTimeS / 60 + "min：" + runTimeS % 60 + "s");//分
    }
    
}
