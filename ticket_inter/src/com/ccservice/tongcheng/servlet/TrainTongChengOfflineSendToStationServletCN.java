/**
 * 版权所有, 空铁无忧
 * Author: 火车票 H5-微信端 项目开发组
 * copyright: 2017
 */
package com.ccservice.tongcheng.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.PropertyUtil;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.ctripoffsts.back.TrainCtripOfflineBack;
import com.ccservice.offline.dao.MailAddressDao;
import com.ccservice.offline.dao.TrainOrderOfflineDao;
import com.ccservice.offline.dao.TrainTicketOfflineDao;
import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.domain.TrainTicketOffline;
import com.ccservice.offline.util.ExceptionTCUtil;
import com.ccservice.offline.util.HttpPostJsonUtil;
import com.ccservice.offline.util.HttpUtil;
import com.ccservice.offline.util.TrainOrderOfflineUtil;

/**
 * @className: com.ccservice.tuniu.train.servlet.TrainTuNiuOfflineOrderServlet
 * @description: TODO - 
 * 
 * 同程线下票 - 同程线下票送票到站物流跟踪请求接口 - 由CN平台发起
 * 
 * 用于正常的流程中回填快递信息
 * 
 * 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月15日 上午9:34:25 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainTongChengOfflineSendToStationServletCN extends HttpServlet {
    private static final String LOGNAME = "同程线下票送票到站物流跟踪请求接口-CN发起";

    private int r1 = new Random().nextInt(10000000);

    private TrainOrderOfflineDao trainOrderOfflineDao = new TrainOrderOfflineDao();

    private TrainTicketOfflineDao trainTicketOfflineDao = new TrainTicketOfflineDao();

    private MailAddressDao mailAddressDao = new MailAddressDao();

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();

        String orderId = request.getParameter("orderId");//同程订单号
        String userid = request.getParameter("userid");//当前系统的登录用户的ID - 用于记录操作日志
        String deliveryStatus = request.getParameter("deliveryStatus");//1: 开始配送, 2: 已送达

        Long orderIdl = Long.valueOf(orderId);
        Integer useridi = Integer.valueOf(userid);

        //记录请求信息日志
        WriteLog.write(LOGNAME, r1 + ":同程线下票送票到站物流跟踪请求信息-orderId-->" + orderIdl + ",useridi-->" + useridi
                + ",deliveryStatus-->" + deliveryStatus);

        //记录日志
        TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "发起送票到站物流跟踪请求");

        JSONObject result = sendToStationCN(orderIdl, useridi, deliveryStatus);

        WriteLog.write(LOGNAME, r1 + ":同程线下票送票到站物流跟踪请求结果-result" + result);

        out.print(result.toJSONString());
        //out.print(TrainOrderOfflineUtil.getNowDateStr()+"同程线下票送票到站物流跟踪请求完成");
        out.flush();
        out.close();
    }

    private JSONObject sendToStationCN(Long orderIdl, Integer useridi, String deliveryStatus) {
        JSONObject resResult = new JSONObject();

        WriteLog.write(LOGNAME, r1 + ":同程线下票送票到站物流跟踪请求-进入到方法-lockTicketCN:orderIdl-->" + orderIdl + ",useridi-->"
                + useridi + ",deliveryStatus-->" + deliveryStatus);

        if (orderIdl == null || orderIdl == 0L) {
            resResult.put("success", "false");
            resResult.put("msg", "传入的订单号有误，请排查");
            return resResult;
        }

        TrainOrderOffline trainOrderOffline = null;
        try {
            trainOrderOffline = trainOrderOfflineDao.findTrainOrderOfflineById(orderIdl);
        }
        catch (Exception e) {
            return ExceptionTCUtil.handleTCException(e);
        }

        if (trainOrderOffline == null) {
            resResult.put("success", "false");
            resResult.put("msg", "该订单不存在，请排查");
            return resResult;
        }

        String orderId = trainOrderOffline.getOrderNumberOnline();

        resResult.put("orderid", orderId);

        //兼容别的采购的逻辑 - 
        Integer CreateUId = trainOrderOffline.getCreateUId();
        if (CreateUId == 81) {//同程
            TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "发起同程开始配送请求");

            HttpPostJsonUtil httpPostJsonUtil = new HttpPostJsonUtil();

            JSONObject dataJson = new JSONObject();

            dataJson.put("orderNo", orderId);
            dataJson.put("deliveryStatus", deliveryStatus);
            dataJson.put("deliveryTime", TrainOrderOfflineUtil.getTuNiuReqTimestamp());//开始配送时间或已送达时间     格式：”2017-03-10 10:30:00”

            String data = dataJson.toJSONString();

            //记录请求信息日志
            WriteLog.write(LOGNAME, r1 + ":同程线下票送票到站物流跟踪请求反馈信息-dataJson" + data);

            String tongChengOfflineCallbackFailURL = PropertyUtil.getValue("TongChengOfflineSendToStationURL",
                    "Train.GuestAccount.properties");

            String res = "";

            for (int i = 0; i < TrainOrderOfflineUtil.TNRETRY; i++) {
                try {
                    res = httpPostJsonUtil.doPost(tongChengOfflineCallbackFailURL, data);//{"success":true}
                }
                catch (Exception e1) {
                    String content = "同程线下票送票到站物流跟踪请求失败";
                    TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, content);
                    ExceptionTCUtil.handleTCException(e1);
                }

                WriteLog.write(LOGNAME, r1 + ":同程线下票送票到站物流跟踪请求-res:" + res);

                if (res != null && res.contains("true")) {//重试请求三次
                    String content = "送票到站物流跟踪信息回调成功";
                    TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, content);

                    //STATE - 未知字段 - 暂且用在标识配送的状态 - 默认值 - 0-未配送 - 1-开始配送 - 2-已送达

                    if (deliveryStatus.equals("1")) {//1: 开始配送, 2: 已送达
                        mailAddressDao.updateSTATEByORDERID(orderIdl, 1);

                        TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "开始配送");
                    }
                    else if (deliveryStatus.equals("2")) {//1: 开始配送, 2: 已送达
                        mailAddressDao.updateSTATEByORDERID(orderIdl, 2);

                        //订单状态改为已邮寄 - 已配送 - 在结算程序中已经做了相关的更改和处理
                        //mailAddressDao.updatePRINTSTATEByORDERID(orderIdl, 1);//邮寄状态 - 默认值是0 - 在相关的存储过程中已经不考虑该值的状态了 - 0 - 待邮寄 - 1 - 已邮寄  - 还原到正式环境的版本的处理

                        TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "已送达");
                    }

                    //TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, content);

                    resResult.put("success", "true");
                    resResult.put("msg", content);

                    return resResult;
                }
                else {
                    /**
                     * 
                     * {"isSuccess":true,"msgCode":199100,"msgInfo":"系统正常","executeTime":"30"}
                     * 
                    {"isSuccess":false,"msgCode":199315,"msgInfo":"订单不存在","executeTime":"22"}
                    {"isSuccess":false,"msgCode":199316,"msgInfo":"订单状态不正确","executeTime":"14"}
                     * 
                     */
                    if (res == null || res.equals("")) {
                        if (i == (TrainOrderOfflineUtil.TNRETRY - 1)) {
                            //记录日志
                            TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi,
                                    "由于网络原因，送票到站物流跟踪请求反馈失败，稍后重试或者联系技术处理");
                            TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "res:" + res);

                            resResult.put("success", "false");
                            resResult.put("msg", "由于网络原因，导致同程的送票到站物流跟踪请求接口无法访问，请您暂且不要操作，稍后重试或者联系客服处理");
                            return resResult;
                        }
                        //记录操作记录
                        TrainOrderOfflineUtil.sleep(10 * 1000);
                        continue;
                    }

                    if (res.contains("订单不存在") || res.contains("订单状态不正确")) {
                        JSONObject resObject = JSONObject.parseObject(res);

                        //记录日志记录
                        String content = resObject.getString("msgInfo");
                        TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, content);

                        //更新票的状态
                        //订单状态 - 1-等待出票 - 2-出票成功 - 3-已拒单【已取消】 - 其他值都是无效状态 - 可操作-是在页面上进行的判断处理
                        //questionDraw = "1"：出票采购问题/邮寄采购问题 questionDraw = "2" //出票成功已邮寄/出票成功待邮寄 - 采购问题订单
                        trainOrderOfflineDao.updateTrainOrderOfflineStatusQuestionById(orderIdl, 2, 1);//出票问题订单

                        resResult.put("success", resObject.getBoolean("isSuccess"));
                        resResult.put("msg", content);

                        //记录请求信息日志
                        WriteLog.write(LOGNAME, r1 + ":同程线下票送票到站物流跟踪请求信息-resResult" + resResult);

                        return resResult;
                    }

                    resResult.put("success", "false");
                    resResult.put("msg", "未知原因");

                    if (res != null && !res.equals("")) {
                        JSONObject resJson = JSONObject.parseObject(res);
                        resResult.put("msg", resJson.getString("msgInfo"));
                    }
                    return resResult;
                }
            }

            //记录请求信息日志
            WriteLog.write(LOGNAME, r1 + ":同程线下票送票到站物流跟踪请求反馈信息-resResult" + resResult);

            TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "同程开始配送请求结束");
        }
        else if (deliveryStatus.equals("1") && CreateUId == 90) {//携程_破解 - 1;//1: 开始配送, 2: 已送达
            TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "发起携程_破解开始配送请求");

            String username = PropertyUtil.getValue("TrainCtripOfflineLoginUsername", "Train.GuestAccount.properties");//从配置文件取出
            TrainCtripOfflineBack trainCtripOfflineBack = new TrainCtripOfflineBack(username);

            Boolean isOrderSuccess = false;
            try {
                WriteLog.write(LOGNAME,
                        r1 + ":携程_破解线下票开始配送请求-username:" + username + "-orderIdl:" + orderIdl + "-useridi:" + useridi);

                String orderNumberOnline = trainOrderOffline.getOrderNumberOnline();

                List<TrainTicketOffline> trainTicketOfflineList = trainTicketOfflineDao
                        .findTrainTicketOfflineListByOrderId(orderIdl);

                TrainTicketOffline trainTicketOffline = trainTicketOfflineList.get(0);
                String fromStationName = trainTicketOffline.getDeparture();
                String toStationName = trainTicketOffline.getArrival();

                isOrderSuccess = trainCtripOfflineBack.ctripSendToStation(orderNumberOnline, fromStationName,
                        toStationName);

                WriteLog.write(LOGNAME, r1 + ":携程_破解线下票开始配送请求-isOrderSuccess:" + isOrderSuccess);
            }
            catch (Exception e3) {
                String content = "携程_破解线下票开始配送交互请求异常";
                TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, content);
                ExceptionTCUtil.handleTCException(e3);
                resResult.put("success", "false");
                resResult.put("msg", content);
            }

            if (isOrderSuccess) {
                String content = "携程_破解线下票开始配送请求成功";
                TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, content);
                resResult.put("success", "true");
                resResult.put("msg", content);

                //result{"msg":"携程_破解线下票开始配送请求成功","success":"true"}

                //STATE - 未知字段 - 暂且用在标识配送的状态 - 默认值 - 0-未配送 - 1-开始配送 - 2-已送达

                if (deliveryStatus.equals("1")) {//1: 开始配送, 2: 已送达
                    mailAddressDao.updateSTATEByORDERID(orderIdl, 1);

                    TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "开始配送");
                }
                else if (deliveryStatus.equals("2")) {//1: 开始配送, 2: 已送达
                    mailAddressDao.updateSTATEByORDERID(orderIdl, 2);

                    TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "已送达");
                }

                resResult.put("success", "true");
                resResult.put("msg", content);
            }
            else {
                String content = "携程_破解线下开始配送请求失败";
                TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, content);
                resResult.put("success", "false");
                resResult.put("msg", content);
            }

            TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "携程_破解开始配送请求结束");
        }
        return resResult;
    }

    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        HttpUtil httpUtil = new HttpUtil();

        Integer orderId = 1650;//系统表的主键的订单表的ID

        Integer agentid = 382;
        Integer deliveryStatus = 1;//1: 开始配送, 2: 已送达

        String param = "?orderId=" + orderId + "&userid=" + agentid + "&deliveryStatus=" + deliveryStatus;//1: 开始配送, 2: 已送达

        //本地测试地址
        String url = "http://localhost:8097/ticket_inter/TrainTongChengOfflineSendToStationServletCN";
        //测试环境地址
        //String url = "http://121.40.226.72:9007/ticket_inter/TrainTongChengOfflineSendToStationServletCN";
        //正式环境地址
        //String url = "http://121.40.241.126:9010/ticket_inter/TrainTongChengOfflineSendToStationServletCN";

        System.out.println(httpUtil.doGet(url, param));

        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间: " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间: " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间: " + runTimeS / 60 + "min:" + runTimeS % 60 + "s");//分
    }

}
