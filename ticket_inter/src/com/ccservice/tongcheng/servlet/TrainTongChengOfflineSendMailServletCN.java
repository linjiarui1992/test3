/**
 * 版权所有, 空铁无忧
 * Author: 火车票 H5-微信端 项目开发组
 * copyright: 2017
 */
package com.ccservice.tongcheng.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.PropertyUtil;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.offline.dao.MailAddressDao;
import com.ccservice.offline.dao.TrainOrderOfflineDao;
import com.ccservice.offline.dao.TrainOrderOfflineRecordDao;
import com.ccservice.offline.domain.MailAddress;
import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.util.ExceptionTCUtil;
import com.ccservice.offline.util.HttpPostJsonUtil;
import com.ccservice.offline.util.TrainOrderOfflineUtil;

/**
 * @className: com.ccservice.tuniu.train.servlet.TrainTuNiuOfflineOrderServlet
 * @description: TODO - 
 * 
 * 同程线下票 - 同程线下票邮寄请求接口 - 由CN平台发起
 * 
 * 用于正常的流程中回填快递信息
 * 
 * 
 * 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月15日 上午9:34:25 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainTongChengOfflineSendMailServletCN extends HttpServlet {
    private static final String LOGNAME = "同程线下票邮寄请求接口-CN发起";

    private int r1 = new Random().nextInt(10000000);

    private TrainOrderOfflineDao trainOrderOfflineDao = new TrainOrderOfflineDao();

    private MailAddressDao mailAddressDao = new MailAddressDao();

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");

        BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
        String line = null;
        StringBuilder sb = new StringBuilder();
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }

        //将资料解码  
        String reqBody = sb.toString();
        //System.out.println(reqBody);

        //记录请求信息日志
        WriteLog.write(LOGNAME, r1 + ":同程线下票邮寄请求接口请求信息-reqBody-->" + reqBody);

        JSONObject requestBody = JSONObject.parseObject(reqBody);

        Long orderId = requestBody.getLong("orderId");
        Integer useridi = requestBody.getInteger("userid");

        /*String orderId = request.getParameter("orderId");//同程订单号
        String userid = request.getParameter("userid");//当前系统的登录用户的ID - 用于记录操作日志
        
        Long orderIdl = Long.valueOf(orderId);
        Integer useridi = Integer.valueOf(userid);
        */

        /**
         * 
        UPDATE mailaddress SET ExpressNum='" + expressnum + "',ExpressAgent=" + expressAgent;
        sql += " WHERE ORDERID=" + orderid;
         * 
         * 上一步直接完成了入库操作，此处可以直接从数据库中取出相关的数据
         * 
         * String expressType = request.getParameter("expressType");//int  是   0: 顺丰，1：EMS - 同程文档
        String expressNo = request.getParameter("expressNo");*///快递单号

        //记录请求信息日志
        WriteLog.write(LOGNAME, r1 + ":同程线下票邮寄请求信息-orderId-->" + orderId + ",useridi-->" + useridi);

        //记录日志
        TrainOrderOfflineUtil.CreateBusLogAdmin(orderId, useridi, "发起邮寄请求");

        JSONObject result = sendMailCN(orderId, useridi);

        WriteLog.write(LOGNAME, r1 + ":同程线下票邮寄请求结果-result" + result);

        PrintWriter out = response.getWriter();

        out.print(result.toJSONString());
        //out.print(TrainOrderOfflineUtil.getNowDateStr()+"同程线下票邮寄请求完成");
        out.flush();
        out.close();
    }

    private JSONObject sendMailCN(Long orderIdl, Integer useridi) {
        JSONObject resResult = new JSONObject();

        WriteLog.write(LOGNAME, r1 + ":同程线下票邮寄请求-进入到方法-sendMailCN");

        if (orderIdl == null || orderIdl == 0L) {
            resResult.put("success", "false");
            resResult.put("msg", "传入的订单号有误，请排查");
            return resResult;
        }

        TrainOrderOffline trainOrderOffline = null;
        try {
            trainOrderOffline = trainOrderOfflineDao.findTrainOrderOfflineById(orderIdl);
        }
        catch (Exception e) {
            return ExceptionTCUtil.handleTCException(e);
        }

        if (trainOrderOffline == null) {
            resResult.put("success", "false");
            resResult.put("msg", "该订单不存在，请排查");
            return resResult;
        }

        //上述两种情况在正常的流程中是不可能发生的

        /**
         * 由于配送到站的开始配送也需要进行相关的邮寄逻辑 - 且需要传递的参数减少 - 
         * 
        dataJson.put("orderNo", orderId);//同程订单号
        dataJson.put("expressType", expressType);//int  是   0: 顺丰，1：EMS - 同程文档 - 2：送票到站
        dataJson.put("expressNo", expressNo);//顺丰、EMS 必填 - 送票到站无
         * 
         */
        Integer isDelivery = trainOrderOffline.getIsDelivery();//送票到站标识 ??? 0-不送票到站 1-送票到站 - 默认值是 0 
        Integer isRapidSend = trainOrderOffline.getIsRapidSend();//20171023-新增需求 - 是否是闪送订单  0: 普通订单,1: 闪送订单 - 默认值是 0 //通过快递类型进行判定 - 目前只有邮寄票会是闪送订单

        String orderId = trainOrderOffline.getOrderNumberOnline();

        resResult.put("orderid", orderId);

        JSONObject dataJson = new JSONObject();

        if (isDelivery == null) {
            /*resResult.put("success", "false");
            resResult.put("msg", "送票到站标识为空，请排查");
            return resResult;*/
            isDelivery = 0;
        }

        if (isRapidSend == null) {
            isRapidSend = 0;
        }

        if (isDelivery == 0) {//送票上门
            //快递单号入库之后，调用此处的方法，进行相关内容的回填的获取和回调
            if (isRapidSend == 0) {//普通邮寄票
                /**
                 * 此处的逻辑的具体处理需要兼容原先的逻辑
                 */
                MailAddress mailAddress = null;
                try {
                    mailAddress = mailAddressDao.findMailAddressByORDERID(orderIdl);
                }
                catch (Exception e) {
                    return ExceptionTCUtil.handleTCException(e);
                }

                if (mailAddress == null) {//一般不会出现这种问题 - 而且 相关的订单和快递信息必须是一对一的关系
                    resResult.put("success", "false");
                    resResult.put("msg", "无法获取该订单-orderIdl:" + orderIdl + ",对应的快递信息的对象-mailAddress:" + mailAddress);
                    return resResult;
                }

                Integer expressType = mailAddress.getExpressAgent();//int  是   0: 顺丰，1：EMS - 同程文档
                String expressNo = mailAddress.getExpressNum();//快递单号

                dataJson = new JSONObject();

                dataJson.put("orderNo", orderId);
                dataJson.put("expressType", expressType);
                dataJson.put("expressNo", expressNo);
            }
            else {//闪送邮寄票
                /**
                 * 此处的逻辑的具体处理需要兼容原先的逻辑
                 */
                MailAddress mailAddress = null;
                try {
                    mailAddress = mailAddressDao.findMailAddressByORDERID(orderIdl);
                }
                catch (Exception e) {
                    return ExceptionTCUtil.handleTCException(e);
                }

                if (mailAddress == null) {//一般不会出现这种问题 - 而且 相关的订单和快递信息必须是一对一的关系
                    resResult.put("success", "false");
                    resResult.put("msg", "无法获取该订单-orderIdl:" + orderIdl + ",对应的快递信息的对象-mailAddress:" + mailAddress);
                    return resResult;
                }

                Integer expressType = mailAddress.getExpressAgent();//int  是   0: 顺丰，1：EMS - 同程文档 - 10
                String expressNo = mailAddress.getExpressNum();//快递单号

                dataJson = new JSONObject();

                dataJson.put("orderNo", orderId);
                dataJson.put("expressType", 3);//统一按照0-顺丰进行反馈 - 闪送单的邮寄返回类型
                dataJson.put("expressNo", expressNo);
            }
        }
        else if (isDelivery == 1) {//送票到站

            dataJson = new JSONObject();

            dataJson.put("orderNo", orderId);
            dataJson.put("expressType", 2);
            dataJson.put("expressNo", null);

        }
        else {
            resResult.put("success", "false");
            resResult.put("msg", "送票到站标识异常，请排查");
            return resResult;
        }

        String data = dataJson.toJSONString();

        //记录请求信息日志
        WriteLog.write(LOGNAME, r1 + ":同程线下票邮寄请求反馈信息-dataJson" + data);

        String tongChengOfflineCallbackFailURL = PropertyUtil.getValue("TongChengOfflineSendMailURL",
                "Train.GuestAccount.properties");

        String res = "";

        HttpPostJsonUtil httpPostJsonUtil = new HttpPostJsonUtil();

        for (int i = 0; i < TrainOrderOfflineUtil.TNRETRY; i++) {
            try {
                res = httpPostJsonUtil.doPost(tongChengOfflineCallbackFailURL, data);//{"success":true}
            }
            catch (Exception e1) {
                String content = "同程线下票邮寄请求失败";
                TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, content);
                ExceptionTCUtil.handleTCException(e1);
            }

            WriteLog.write(LOGNAME, r1 + ":同程线下票邮寄请求-res:" + res);

            if (res != null && res.contains("true")) {//重试请求三次
                String content = "邮寄信息回调成功";

                //为了防止二次的报表的导出 - 该单的相关的操作记录插入之前需要做相关的二次判定 DealResult = 15 , FKTrainOrderOfflineId = orderIdl
                TrainOrderOfflineRecordDao trainOrderOfflineRecordDao = new TrainOrderOfflineRecordDao();
                Integer dealResultCount = 0;
                try {
                    dealResultCount = trainOrderOfflineRecordDao.findDealResultCountByOrderId(orderIdl, 15);
                }
                catch (Exception e) {
                    ExceptionTCUtil.handleTCException(e);
                }
                //便于同程报表的导出
                if (dealResultCount >= 1) {
                    TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, content);
                }
                else {
                    TrainOrderOfflineUtil.CreateBusLogAdminOtherDealResult(orderIdl, useridi, content, 15);
                }

                resResult.put("success", "true");
                resResult.put("msg", content);

                return resResult;
            }
            else {
                /**
                 * 
                 * {"isSuccess":true,"msgCode":126100,"msgInfo":"系统正常","executeTime":"4379"}
                 * 
                {"isSuccess":false,"msgCode":126315,"msgInfo":"订单不存在","executeTime":"63"}
                {"isSuccess":false,"msgCode":126330,"msgInfo":"保存数据出错_更新邮寄信息","executeTime":"308"}
                {"isSuccess":false,"msgCode":126316,"msgInfo":"订单状态不正确","executeTime":"8"}
                 * 
                 */
                if (res == null || res.equals("")) {
                    if (i == (TrainOrderOfflineUtil.TNRETRY - 1)) {
                        //记录日志
                        TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "由于网络原因，邮寄请求反馈失败，稍后重试或者联系技术处理");
                        TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "res:" + res);

                        resResult.put("success", "false");
                        resResult.put("msg", "由于网络原因，导致同程的邮寄请求接口无法访问，请您暂且不要操作，稍后重试或者联系客服处理");
                        return resResult;
                    }
                    //记录操作记录
                    TrainOrderOfflineUtil.sleep(10 * 1000);
                    continue;
                }

                if (res.contains("订单不存在") || res.contains("保存数据出错_更新邮寄信息")) {
                    JSONObject resObject = JSONObject.parseObject(res);

                    //记录日志记录
                    String content = resObject.getString("msgInfo");
                    TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, content);

                    //更新票的状态
                    //订单状态 - 1-等待出票 - 2-出票成功 - 3-已拒单【已取消】 - 其他值都是无效状态 - 可操作-是在页面上进行的判断处理
                    //questionDraw = "1"：出票采购问题/邮寄采购问题 questionDraw = "2" //出票成功已邮寄/出票成功待邮寄 - 采购问题订单
                    trainOrderOfflineDao.updateTrainOrderOfflineStatusQuestionById(orderIdl, 2, 1);//出票问题订单

                    resResult.put("success", resObject.getBoolean("isSuccess"));
                    resResult.put("msg", content);

                    //记录请求信息日志
                    WriteLog.write(LOGNAME, r1 + ":同程线下票邮寄请求信息-resResult" + resResult);

                    return resResult;
                }

                resResult.put("success", "false");
                resResult.put("msg", "未知原因");

                if (res != null && !res.equals("")) {
                    JSONObject resJson = JSONObject.parseObject(res);
                    resResult.put("msg", resJson.getString("msgInfo"));
                }
                return resResult;
            }
        }

        //记录请求信息日志
        WriteLog.write(LOGNAME, r1 + ":同程线下票邮寄请求反馈信息-resResult" + resResult);

        return resResult;
    }

    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        HttpPostJsonUtil httpPostJsonUtil = new HttpPostJsonUtil();

        //送票到家的测试订单的信息
        String data = "{\"userid\":382,\"orderId\":\"1669\"}";

        System.out.println(data);

        //本地测试地址
        String url = "http://localhost:8097/ticket_inter/TrainTongChengOfflineSendMailServletCN";
        //测试环境地址
        //String url = "http://121.40.226.72:9007/ticket_inter/TrainTongChengOfflineSendMailServletCN";
        //正式环境地址
        //String url = "http://121.40.241.126:9010/ticket_inter/TrainTongChengOfflineSendMailServletCN";

        System.out.println(httpPostJsonUtil.doPost(url, data));

        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间: " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间: " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间: " + runTimeS / 60 + "min:" + runTimeS % 60 + "s");//分
    }

}
