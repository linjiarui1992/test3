package com.ccservice.tongcheng.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.quartz.JobDetail;
import org.quartz.SchedulerException;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.PropertyUtil;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.offline.dao.TrainOrderOfflineDao;
import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.util.ExceptionTCUtil;
import com.ccservice.offline.util.ExceptionTNUtil;
import com.ccservice.offline.util.SchedulerUtil;
import com.ccservice.offline.util.TrainOrderOfflineUtil;
import com.ccservice.tongcheng.service.TongChengOfflineLockOvertime;

/**
 * @className: com.ccservice.tuniu.train.servlet.TrainTuNiuOfflineLockOrderCallbackServlet
 * @description: TODO - 同程线下票锁票请求接口 - 
 * 
 * cn_home平台 请求该接口
 * 
 * 模拟实现锁单的问题 - 锁单期间不允许取消 - 锁单时长
 * 
 * 一期没有锁单的接口需求
 * 
 * 既然同程锁单在咱们这里仅仅是为了防止锁单中的取消请求。那么，不涉及跟同程的交互，即使锁单超时，也应该可以再次锁单吧？@叶旺盛 @北京-产品-郅鹏 
 * 同程没有最晚出票时间之类的设定
 * 
 * 
 * @author: 郑州-技术-郭伟强  E-mail:gwq20521@163.com
 * @createTime: 2017年8月18日 下午1:57:36
 * @version: v 1.0
 * @since 
 * 
 */
public class TrainTongChengOfflineLockTicketServletCN extends HttpServlet {
    private static final String LOGNAME = "同程线下票锁票请求接口-CN发起";

    private int r1 = new Random().nextInt(10000000);

    private TrainOrderOfflineDao trainOrderOfflineDao = new TrainOrderOfflineDao();

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();

        String orderId = request.getParameter("orderId");//系统表的主键的订单表的ID
        String userid = request.getParameter("userid");//当前系统的登录用户的ID - 用于记录操作日志

        Long orderIdl = Long.valueOf(orderId);
        Integer useridi = Integer.valueOf(userid);

        //记录请求信息日志
        WriteLog.write(LOGNAME, r1 + ":同程线下票锁票请求信息-orderId-->"+orderIdl+",userid-->"+useridi);

        //记录日志
        TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "发起锁票请求");
        
        JSONObject result = lockTicketCN(orderIdl, useridi);
        
        WriteLog.write(LOGNAME, r1 + ":同程线下票锁票请求结果-result" + result);
        
        out.print(result.toJSONString());
        //out.print(TrainOrderOfflineUtil.getNowDateStr()+"同程线下票锁票请求完成");
        out.flush();
        out.close();
    }

    private JSONObject lockTicketCN(Long orderIdl, Integer useridi) {
        JSONObject resResult = new JSONObject();

        WriteLog.write(LOGNAME, r1 + ":同程线下票锁票请求-进入到方法-lockTicketCN:orderIdl-->"+orderIdl+",useridi-->"+useridi);

        if (orderIdl == null || orderIdl == 0L) {
            resResult.put("success", "false");
            resResult.put("msg", "传入的订单号有误，请排查");
            return resResult;
        }
        
        TrainOrderOffline trainOrderOffline = null;
        try {
            trainOrderOffline = trainOrderOfflineDao.findTrainOrderOfflineById(orderIdl);
        }
        catch (Exception e) {
            return ExceptionTCUtil.handleTCException(e);
        }

        if (trainOrderOffline == null) {
            resResult.put("success", "false");
            resResult.put("msg", "该订单不存在，请排查");
            return resResult;
        }

        //一般不会出现上述原因
        
        resResult.put("orderid", trainOrderOffline.getOrderNumber());
        
        if (trainOrderOffline.getLockedStatus()!=0) {
            resResult.put("success", "false");
            resResult.put("msg", "该订单已发起过锁单请求，请勿重复操作");
            return resResult;
        }

        //一般不会出现该种原因
        if (trainOrderOffline.getOrderStatus() != 1) {
            resResult.put("success", "false");
            resResult.put("msg", "该订单是非等待出票的状态，不允许锁单");
            return resResult;
        }

        //更新锁单状态为锁单中 - 点击一次之后
        trainOrderOfflineDao.updateTrainOrderOfflinelockedStatusById(orderIdl, 1);//锁单成功
        
        //更新锁单成功或者失败的时间 - 
        try {
            trainOrderOfflineDao.updateTrainOrderOfflineLockTimeById(orderIdl);
        }
        catch (Exception e2) {
            return ExceptionTCUtil.handleTCException(e2);
        }
        
        /**
         * 如果想模拟成为同步反馈的结果，需要判定是否收到异步的锁单反馈 - 且不允许再次锁单 - 需要加上一个标志位
         * 
         * 同程的锁票的异步转同步的内部等待时间 - 以min为单位 - 目前设定为半小时
         * 
         */
        String LockWait = PropertyUtil.getValue("TrainTongChengOfflineLockWait", "Train.GuestAccount.properties");
        
        //毫秒数
        Long overtime = new Date().getTime() + Integer.valueOf(LockWait)*60*1000;

        String lockWaitDateTime = TrainOrderOfflineUtil.getTimestrByTime(overtime);
        //记录日志
        TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "锁票请求成功，等待同程异步反馈结果，目前等待超时的设置时间是:"+LockWait+"min，超时后自动解锁");

        //同步入库成最晚出票时间 - 还是不能重复锁单的设计

        try {
            trainOrderOfflineDao.updateOrderTimeoutById(orderIdl, lockWaitDateTime);//2017-08-21 14:49:00
        }
        catch (Exception e1) {
            return ExceptionTCUtil.handleTCException(e1);
        }
        
        //此处另起JOB，完成5分钟之后的拒单操作

        String year = lockWaitDateTime.substring(0,4);
        String month = lockWaitDateTime.substring(5,7);
        String day = lockWaitDateTime.substring(8,10);
        String hour = lockWaitDateTime.substring(11,13);
        String minute = lockWaitDateTime.substring(14,16);
        String second = lockWaitDateTime.substring(17,19);
        
        //"40 17 14 25 08 ? 2017"
        String cronExpression = second+" "+minute+" "+hour+" "+day+" "+month+" ? "+year;
        
        //System.out.println(cronExpression);

        WriteLog.write(LOGNAME, r1 + ":同程线下票锁票请求启动定时任务-cronExpression" + cronExpression);

        try {
            JobDetail jobDetail = SchedulerUtil.getScheduler().getJobDetail("TongChengOfflineLockOvertime"+"Job"+trainOrderOffline.getId(), "TongChengOfflineLockOvertime"+"JobGroup");
            if (jobDetail!=null) {
                WriteLog.write(LOGNAME, r1 + ":同程线下票锁票请求定时任务已启动-jobDetail:" + jobDetail.getFullName());
                
                resResult.put("success", "true");
                resResult.put("msg", "锁票请求成功");
                return resResult;
            } else {
                WriteLog.write(LOGNAME, r1 + ":同程线下票锁票请求启动定时任务-jobDetail:" + null);

                //创建一个定时任务，并在指定的时间点进行启动
                try {
                    SchedulerUtil.startLockScheduler(trainOrderOffline.getId(), useridi, LockWait, cronExpression, "TongChengOfflineLockOvertime", TongChengOfflineLockOvertime.class);
                }
                catch (Exception e) {
                    return ExceptionTCUtil.handleTCException(e);
                }

                TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "锁票请求成功");
                
                resResult.put("success", "true");
                resResult.put("msg", "锁票请求成功");
                return resResult;
            }
        }
        catch (SchedulerException e1) {
            resResult.put("success", "true");
            resResult.put("msg", "锁票请求成功-定时任务出现异常");
            return resResult;
        }
    }

}
