package com.ccservice.newjltour.hoteldb;

import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.HashMap;
import java.util.Calendar;
import java.util.ArrayList;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

import com.ccservice.inter.server.Server;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.b2b2c.base.roomtype.Roomtype;
import com.ccservice.b2b2c.base.hmhotelprice.Hmhotelprice;
import com.ccservice.b2b2c.base.hmhotelprice.JLPriceResult;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;

/**
 * 深捷旅JSON接口数据更新
 * @author WH
 */

public class JlHotelDB {

    private static int month = Integer.valueOf(PropertyUtil.getValue("jlCatchPriceMonth"));//JSON接口，最多一个月

    /**
     * 更新酒店、房型基础信息
     */
    @SuppressWarnings("unchecked")
    public static void updateBaseInfo() {
        long start = System.currentTimeMillis();
        System.out.println(ElongHotelInterfaceUtil.getCurrentTime() + "=====开始=====更新深捷旅酒店及房型基础信息=====");
        try {
            //本地酒店，酒店、房型静态信息
            List<Hotel> hotels = Server.getInstance().getHotelService()
                    .findAllHotel("where C_SOURCETYPE = 6", "order by C_CITYID, ID", -1, 0);
            int count = 0;//合计，深捷旅最多20个一次
            String jlIds = "";//深捷旅酒店ID
            int size = hotels.size();
            Map<String, Hotel> hotelMap = new HashMap<String, Hotel>();
            for (int i = 0; i < size; i++) {
                Hotel hotel = hotels.get(i);
                String jlId = hotel.getHotelcode();
                if (ElongHotelInterfaceUtil.StringIsNull(jlId) || hotelMap.containsKey(jlId)) {
                    continue;
                }
                count++;
                jlIds += jlId + "/";
                hotelMap.put(jlId, hotel);
                //20个了或最后一个酒店
                if (count == 20 || i == size - 1) {
                    //POST
                    Server.getInstance().getIJLHotelService().newUpdateHotelInfo(jlIds, hotelMap, 0);
                    //RESET
                    count = 0;
                    jlIds = "";
                    hotelMap = new HashMap<String, Hotel>();
                }
            }
            //深捷旅屏蔽酒店
            Server.getInstance().getIJLHotelService().newShieldHotel();
        }
        catch (Exception e) {
        }
        long end = System.currentTimeMillis();
        long ss = (end - start) / 1000;//秒
        long mm = ss / 60;//分
        System.out.print(ElongHotelInterfaceUtil.getCurrentTime() + "=====结束=====更新深捷旅酒店及房型基础信息=====");
        if (mm > 0) {
            System.out.println("耗时：" + mm + "分钟");
        }
        else {
            System.out.println("耗时：" + ss + "秒钟");
        }
    }

    /**
     * 整体更新酒店价格
     * 一次最多只能抓取20家酒店的价格数据，多个ID用/分开
     */
    @SuppressWarnings("unchecked")
    public static void updateJlprice() {
        long start = System.currentTimeMillis();
        System.out.println(ElongHotelInterfaceUtil.getCurrentTime() + "=====开始=====整体更新深捷旅酒店价格=====");
        //删除今天之前的数据
        String delSql = "delete from T_HMHOTELPRICE where C_STATEDATE < '" + ElongHotelInterfaceUtil.getCurrentDate()
                + "'";
        //Server.getInstance().getSystemService().findMapResultBySql(delSql, null);
        //深捷旅酒店
        String sql = "where C_SOURCETYPE = 6 and C_HOTELCODE != ''";
        List<Hotel> hotels = Server.getInstance().getHotelService().findAllHotel(sql, "", -1, 0);
        for (Hotel hotel : hotels) {
            try {
                requestJlPrice(hotel);
            }
            catch (Exception e) {
                System.out.println(hotel.getName() + "同步价格出错：" + ElongHotelInterfaceUtil.errormsg(e));
            }
        }
        long end = System.currentTimeMillis();
        long ss = (end - start) / 1000;//秒
        long mm = ss / 60;//分
        System.out.print(ElongHotelInterfaceUtil.getCurrentTime() + "=====结束=====整体更新深捷旅酒店价格=====");
        if (mm > 0) {
            System.out.println("耗时：" + mm + "分钟");
        }
        else {
            System.out.println("耗时：" + ss + "秒钟");
        }
    }

    /**
     * 深捷旅变价通知后更新数据
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static void updateChange(String roomIds) {
        if (ElongHotelInterfaceUtil.StringIsNull(roomIds)) {
            return;
        }
        //日期格式化
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println(timeFormat.format(new Date()) + "=====开始深捷旅价格同步=====");
        //酒店、房型
        Map<String, Hotel> hotels = new HashMap<String, Hotel>();//key：深捷旅酒店ID
        Map<String, Long> roomtypeids = new HashMap<String, Long>();
        //循环
        String[] idArray = roomIds.split("/");
        for (String roomId : idArray) {
            try {
                if (ElongHotelInterfaceUtil.StringIsNull(roomId)) {
                    continue;
                }
                //验证
                String namechn = Server.getInstance().getHotelCacheService().checkJlTourInternationalRoom(roomId, "");
                //国际酒店
                if (!ElongHotelInterfaceUtil.StringIsNull(namechn)) {
                    System.out.println(timeFormat.format(new Date()) + " JlRoomId：" + roomId + "-->缓存，" + namechn
                            + "，国际酒店，跳过。");
                    continue;
                }
                //价格日期
                Calendar start = GregorianCalendar.getInstance();
                Calendar end = GregorianCalendar.getInstance();
                end.add(Calendar.MONTH, month);
                String startDate = dateFormat.format(start.getTime());
                String endDate = dateFormat.format(end.getTime());
                //POST
                List<JLPriceResult> list = Server.getInstance().getIJLHotelService()
                        .newUpdateHotelPrice("UpdateChange", "", roomId, startDate, endDate);
                //删除房型所有价格
                if (list == null || list.size() == 0) {
                    System.out.println(timeFormat.format(new Date()) + "=====" + roomId + "，深捷旅房型无价格=====");
                    //通过房型ID查找本地对应酒店、房型ID
                    String idSql = "select h.ID hid, r.ID rid from T_HOTEL h with(nolock), T_ROOMTYPE r with(nolock)"
                            + "where h.C_SOURCETYPE = 6 and h.ID = r.C_HOTELID and r.C_ROOMCODE = '" + roomId + "'";
                    List idList = Server.getInstance().getSystemService().findMapResultBySql(idSql, null);
                    if (idList != null && idList.size() > 0) {
                        for (int i = 0; i < idList.size(); i++) {
                            try {
                                Map idMap = (Map) idList.get(i);
                                String hid = idMap.get("hid").toString();//酒店ID
                                String rid = idMap.get("rid").toString();//房型ID
                                //验证是否存在价格
                                String checkSql = "select top 1 * from T_HMHOTELPRICE with(nolock) where C_HOTELID = "
                                        + hid + " and C_ROOMTYPEID = " + rid;
                                List<Hmhotelprice> havePriceList = Server.getInstance().getHotelService()
                                        .findAllHmhotelpriceBySql(checkSql, -1, 0);
                                //有价格、删除
                                if (havePriceList != null && havePriceList.size() > 0) {
                                    String delSql = "delete from T_HMHOTELPRICE where C_HOTELID = " + hid
                                            + " and C_ROOMTYPEID = " + rid;
                                    Server.getInstance().getSystemService().findMapResultBySql(delSql, null);
                                    System.out.println(timeFormat.format(new Date()) + "=====" + roomId
                                            + "，房型存在本地价格，删除=====");
                                }
                            }
                            catch (Exception e) {
                            }
                        }
                    }
                    continue;
                }
                //房型所有价格
                int allflag = 0;//0：未查询过所有价格；其他：查过
                boolean hotelExists = true;//对应本地酒店是否存在，默认存在，不存在直接删除原有价格
                Map<String, Hmhotelprice> oldAllKeys = new HashMap<String, Hmhotelprice>();//key：jlkeyid
                //房型无变化价格
                Map<String, Hmhotelprice> oldNoChangeKeys = new HashMap<String, Hmhotelprice>();//key：jlkeyid
                //循环
                for (JLPriceResult result : list) {
                    Hotel hotel = null;//本地酒店
                    long roomtypeid = 0;//本地房型ID
                    try {
                        String jlcode = result.getHotelid();//深捷旅酒店编码
                        String jltime = result.getJltime();
                        String jlkeyid = result.getJlkeyid();
                        //本地数据
                        String where = "select top 1 * from T_HMHOTELPRICE with(nolock) where C_JLKEYID = '" + jlkeyid
                                + "'";
                        List<Hmhotelprice> olds = Server.getInstance().getHotelService()
                                .findAllHmhotelpriceBySql(where, -1, 0);
                        boolean existsOld = olds != null && olds.size() > 0 ? true : false;
                        if (existsOld) {
                            Hmhotelprice old = olds.get(0);
                            if (jltime.equals(old.getJltime())) {
                                oldNoChangeKeys.put(jlkeyid, old);
                                System.out.println(timeFormat.format(new Date()) + "~~~~~" + roomId + "，"
                                        + old.getStatedate() + "，深捷旅价格无变化~~~~~");
                                continue;
                            }
                        }
                        //查询本地酒店
                        hotel = hotels.get(jlcode);
                        if (hotel == null && hotelExists) {
                            String hotelsql = "select top 1 * from T_HOTEL with(nolock) where C_SOURCETYPE = 6 and C_HOTELCODE = '"
                                    + jlcode + "'";
                            List<Hotel> tempList = Server.getInstance().getHotelService()
                                    .findAllHotelBySql(hotelsql, -1, 0);
                            //酒店已经存在
                            if (tempList != null && tempList.size() > 0) {
                                hotel = tempList.get(0);
                                hotels.put(jlcode, hotel);
                            }
                            //酒店不存在，请求酒店基本信息接口，新增酒店
                            else {
                                Map<String, Hotel> hotelMap = new HashMap<String, Hotel>();
                                hotelMap.put(jlcode, new Hotel());//防止再次查询
                                Server.getInstance().getIJLHotelService().newUpdateHotelInfo(jlcode, hotelMap, 0);
                                //再次查询
                                tempList = Server.getInstance().getHotelService().findAllHotelBySql(hotelsql, -1, 0);
                                if (tempList != null && tempList.size() > 0) {
                                    hotel = tempList.get(0);
                                    hotels.put(jlcode, hotel);
                                }
                            }
                        }
                        //酒店存在
                        if (hotel != null) {
                            long hotelid = hotel.getId();
                            String roomkey = hotelid + "-" + result.getJlroomtype();
                            if (roomtypeids.containsKey(roomkey)) {
                                roomtypeid = roomtypeids.get(roomkey);
                            }
                            else {
                                String roomsql = "select top 1 * from T_ROOMTYPE with(nolock) where C_HOTELID = "
                                        + hotelid + " and C_ROOMCODE = '" + result.getJlroomtype() + "'";
                                List<Roomtype> tempList = Server.getInstance().getHotelService()
                                        .findAllRoomtypeBySql(roomsql, -1, 0);
                                //房型已经存在
                                if (tempList != null && tempList.size() > 0) {
                                    Roomtype room = tempList.get(0);
                                    roomtypeid = room.getId();
                                    roomtypeids.put(roomkey, roomtypeid);
                                }
                                //房型不存在，请求酒店基本信息接口，新增房型
                                else {
                                    Map<String, Hotel> hotelMap = new HashMap<String, Hotel>();
                                    hotelMap.put(jlcode, hotel);//防止再次查询
                                    Server.getInstance().getIJLHotelService().newUpdateHotelInfo(jlcode, hotelMap, 0);
                                    //再次查询
                                    tempList = Server.getInstance().getHotelService()
                                            .findAllRoomtypeBySql(roomsql, -1, 0);
                                    if (tempList != null && tempList.size() > 0) {
                                        Roomtype room = tempList.get(0);
                                        roomtypeid = room.getId();
                                        roomtypeids.put(roomkey, roomtypeid);
                                    }
                                }
                            }
                        }
                        else {
                            hotelExists = false;
                        }
                        //酒店或房型不存在，无法通过酒店和房型查询现有价格，中断
                        if (!hotelExists || roomtypeid == 0) {
                            if (existsOld) {
                                String sql = "delete from T_HMHOTELPRICE where C_JLKEYID = '" + jlkeyid + "'";
                                Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                                System.out.println(timeFormat.format(new Date()) + "-----删除深捷旅多余房型价格，KEYID：" + jlkeyid
                                        + "-----");
                            }
                            continue;
                        }
                        //房型下所有本地价格
                        if (allflag == 0) {
                            String allSql = "select * from T_HMHOTELPRICE with(nolock) where C_HOTELID = "
                                    + hotel.getId() + " and C_ROOMTYPEID = " + roomtypeid;
                            List<Hmhotelprice> oldAlls = Server.getInstance().getHotelService()
                                    .findAllHmhotelpriceBySql(allSql, -1, 0);
                            for (Hmhotelprice o : oldAlls) {
                                oldAllKeys.put(o.getJlkeyid(), o);
                            }
                            allflag = 1;
                        }
                        try {
                            saveUpdate(hotel, result, oldAllKeys, roomtypeids, timeFormat);
                        }
                        catch (Exception e) {
                            System.out.println(hotel.getName() + "同步价格出错：" + ElongHotelInterfaceUtil.errormsg(e));
                        }
                    }
                    catch (Exception e) {
                        if (hotel == null) {
                            System.out.println("变价同步出错：" + ElongHotelInterfaceUtil.errormsg(e));
                        }
                        else {
                            System.out.println(hotel.getName() + "变价同步出错：" + ElongHotelInterfaceUtil.errormsg(e));
                        }
                    }
                }
                //删除多余数据
                if (oldAllKeys.size() > 0) {
                    //移除未变化的
                    if (oldNoChangeKeys.size() > 0) {
                        for (String keyId : oldNoChangeKeys.keySet()) {
                            if (oldAllKeys.containsKey(keyId)) {
                                oldAllKeys.remove(keyId);
                            }
                        }
                    }
                    if (oldAllKeys.size() > 0) {
                        String keyIds = "";
                        for (String keyId : oldAllKeys.keySet()) {
                            keyIds += "'" + keyId + "',";
                        }
                        if (keyIds.endsWith(",")) {
                            keyIds = keyIds.substring(0, keyIds.length() - 1);
                            String sql = "delete from T_HMHOTELPRICE where C_JLKEYID in (" + keyIds + ")";
                            Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                            System.out.println(timeFormat.format(new Date()) + "=====删除深捷旅多余酒店价格=====");
                        }
                    }
                }
            }
            catch (Exception e) {
                System.out.println("变价同步出错：" + ElongHotelInterfaceUtil.errormsg(e));
            }
        }
        System.out.println(timeFormat.format(new Date()) + "=====结束深捷旅价格同步=====");
    }

    @SuppressWarnings("unchecked")
    private static void requestJlPrice(Hotel hotel) {
        String jlHotelId = hotel.getHotelcode();
        if (ElongHotelInterfaceUtil.StringIsNull(jlHotelId)) {
            return;
        }
        //日期格式化
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println(timeFormat.format(new Date()) + "=====" + hotel.getName() + "，开始更新深捷旅酒店价格=====");
        //价格日期
        Calendar start = GregorianCalendar.getInstance();
        Calendar end = GregorianCalendar.getInstance();
        end.add(Calendar.MONTH, month);
        String startDate = dateFormat.format(start.getTime());
        String endDate = dateFormat.format(end.getTime());
        //POST
        List<JLPriceResult> list = Server.getInstance().getIJLHotelService()
                .newUpdateHotelPrice("", jlHotelId, "", startDate, endDate);
        if (list == null) {
            list = new ArrayList<JLPriceResult>();
        }
        //本地数据
        long hotelid = hotel.getId();
        List<Hmhotelprice> olds = Server.getInstance().getHotelService()
                .findAllHmhotelprice("where C_HOTELID = " + hotelid, "", -1, 0);
        Map<String, Hmhotelprice> oldkeys = new HashMap<String, Hmhotelprice>();
        for (Hmhotelprice o : olds) {
            oldkeys.put(o.getJlkeyid(), o);
        }
        Map<String, Long> roomtypeids = new HashMap<String, Long>();
        for (JLPriceResult result : list) {
            try {
                saveUpdate(hotel, result, oldkeys, roomtypeids, timeFormat);
            }
            catch (Exception e) {
                System.out.println(hotel.getName() + "同步价格出错：" + ElongHotelInterfaceUtil.errormsg(e));
            }
        }
        //删除多余数据
        if (oldkeys.size() > 0) {
            String keyIds = "";
            for (String keyId : oldkeys.keySet()) {
                keyIds += "'" + keyId + "',";
            }
            if (keyIds.endsWith(",")) {
                keyIds = keyIds.substring(0, keyIds.length() - 1);
                String sql = "delete from T_HMHOTELPRICE where C_JLKEYID in (" + keyIds + ")";
                Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                System.out.println(timeFormat.format(new Date()) + "=====" + hotel.getName() + "，删除深捷旅多余酒店价格=====");
            }
        }
        System.out.println(timeFormat.format(new Date()) + "=====" + hotel.getName() + "，结束更新深捷旅酒店价格=====");
    }

    //公共保存方法
    @SuppressWarnings("unchecked")
    private static void saveUpdate(Hotel hotel, JLPriceResult result, Map<String, Hmhotelprice> oldkeys,
            Map<String, Long> roomtypeids, SimpleDateFormat timeFormat) throws Exception {
        String jlkeyid = result.getJlkeyid();
        String jltime = result.getJltime();
        Hmhotelprice price = oldkeys.get(jlkeyid);
        boolean newflag = false;
        if (price == null) {
            newflag = true;
            price = new Hmhotelprice();
        }
        else if (jltime.equals(price.getJltime())) {
            oldkeys.remove(jlkeyid);
            return;
        }
        long hotelid = hotel.getId();
        price.setJlkeyid(jlkeyid);
        price.setSupplyid(Integer.parseInt(result.getSupplierid()));
        price.setJltime(jltime);
        price.setContractid(result.getHotelcd());
        price.setHotelid(hotelid);
        price.setContractver("");
        price.setCur(result.getCurrency());
        price.setProd(result.getRatetypeid());
        price.setMaxday(99l);
        //最少连住
        try {
            price.setMinday(Long.valueOf(result.getMinDay()));
        }
        catch (Exception e) {
            price.setMinday(1l);
        }
        //提前天数
        try {
            price.setAdvancedday(Long.valueOf(result.getLeadTime()));
        }
        catch (Exception e) {
            price.setAdvancedday(0l);
        }
        price.setTicket("0");
        long roomtypeid = 0;
        String roomkey = hotelid + "-" + result.getJlroomtype();
        if (roomtypeids.containsKey(roomkey)) {
            roomtypeid = roomtypeids.get(roomkey);
        }
        else {
            String roomsql = "select top 1 * from T_ROOMTYPE with(nolock) where C_HOTELID = " + hotelid
                    + " and C_ROOMCODE = '" + result.getJlroomtype() + "'";
            List<Roomtype> rooms = Server.getInstance().getHotelService().findAllRoomtypeBySql(roomsql, -1, 0);
            if (rooms != null && rooms.size() > 0) {
                Roomtype room = rooms.get(0);
                roomtypeid = room.getId();
                roomtypeids.put(roomkey, roomtypeid);
            }
        }
        if (roomtypeid == 0) {
            return;
        }
        price.setRoomtypeid(roomtypeid);
        price.setType("");
        price.setServ("");
        String bf = result.getBreakfast();
        if (ElongHotelInterfaceUtil.StringIsNull(bf)) {
            bf = "";
        }
        price.setBreakfasttype(bf);
        if (bf.equals("10")) {
            price.setBf(0l);
        }
        else if (bf.equals("11") || bf.equals("12") || bf.equals("13")) {
            price.setBf(1l);
        }
        else if (bf.equals("21") || bf.equals("22") || bf.equals("23")) {
            price.setBf(2l);
        }
        else if (bf.equals("31") || bf.equals("32") || bf.equals("33")) {
            price.setBf(3l);
        }
        else if (bf.equals("34")) {
            price.setBf(6l);
        }
        else {
            price.setBf(0l);
        }
        price.setStatedate(result.getStayDate());
        String pricet = result.getPprice();
        double saveprice = 0d;
        try {
            saveprice = Double.parseDouble(pricet);
        }
        catch (Exception e) {
            saveprice = 0d;
        }
        if (saveprice <= 0) {
            return;
        }
        price.setPrice(saveprice);
        price.setPriceoffer(saveprice);
        price.setQunarprice(saveprice);
        price.setUpdatetime(timeFormat.format(new Date()));
        price.setSourcetype("6");
        price.setCityid(hotel.getCityid() == null ? "" : hotel.getCityid().toString());
        price.setRatetype(result.getRatetype());
        String roomstatus = result.getAllot();
        if ("12".equals(roomstatus)) {
            price.setIsallot("Y");
            price.setRoomstatus(0l);
            price.setYuliuNum(99l);
        }
        else if ("16".equals(roomstatus)) {
            price.setIsallot("C");
            price.setRoomstatus(1l);
            price.setYuliuNum(0l);
        }
        else {
            try {
                price.setYuliuNum(Long.parseLong(result.getFangliang()));
            }
            catch (Exception e) {
                price.setYuliuNum(0l);
            }
            if (price.getYuliuNum() > 0) {
                price.setIsallot("Y");
                price.setRoomstatus(0l);
            }
            else {
                price.setIsallot("N");
                price.setRoomstatus(0l);
                price.setYuliuNum(0l);
            }
        }
        price.setAllotmenttype(result.getAllotmenttype());
        //取消描述
        price.setCanceldesc(result.getCanceldesc());
        //类型：即订即保、提前多少天等
        if (!ElongHotelInterfaceUtil.StringIsNull(result.getVoidabletype())
                && !"null".equalsIgnoreCase(result.getVoidabletype().trim())) {
            price.setCanceltype(Integer.parseInt(result.getVoidabletype().trim()));
        }
        //提前天数
        if (!ElongHotelInterfaceUtil.StringIsNull(result.getDayselect())
                && !"null".equalsIgnoreCase(result.getDayselect().trim())) {
            price.setCanceladvance(Integer.parseInt(result.getDayselect().trim()));
        }
        //提前时间  多少小时
        if (!ElongHotelInterfaceUtil.StringIsNull(result.getTimeselect())
                && !"null".equalsIgnoreCase(result.getTimeselect().trim())) {
            price.setCanceltime(result.getTimeselect());
        }
        else {
            price.setCanceltime("");
        }
        //不可修改、不可取消、两者
        if (!ElongHotelInterfaceUtil.StringIsNull(result.getNoeditorcancel())
                && !"null".equalsIgnoreCase(result.getNoeditorcancel().trim())) {
            price.setNoeditcancel(Integer.parseInt(result.getNoeditorcancel().trim()));
        }
        //不可修改内容
        if (result.getNoedit() != null && !"".equals(result.getNoedit().trim())
                && !"null".equalsIgnoreCase(result.getNoedit().trim())) {
            price.setNoedittype(result.getNoedit().trim());
        }
        else {
            price.setNoedittype("");
        }
        //担保金额类型
        if (!ElongHotelInterfaceUtil.StringIsNull(result.getGuaranteeamounttype())
                && !"null".equalsIgnoreCase(result.getGuaranteeamounttype().trim())) {
            price.setGuamoneytype(Integer.parseInt(result.getGuaranteeamounttype().trim()));
        }
        //新增
        if (newflag) {
            Server.getInstance().getHotelService().createHmhotelprice(price);
            System.out.println(timeFormat.format(new Date()) + " 新增价格：" + hotel.getName() + "---"
                    + result.getRoomtype() + "---" + result.getRatetype() + "---" + price.getStatedate());
        }
        else {
            String updatesql = "update T_HMHOTELPRICE set C_HOTELID = " + hotelid + ", C_CUR = '" + price.getCur()
                    + "', C_ADVANCEDDAY = " + price.getAdvancedday() + ", C_ROOMTYPEID = " + roomtypeid + ", C_BF = "
                    + price.getBf().longValue() + ", C_STATEDATE = '" + price.getStatedate() + "', C_PRICE = "
                    + price.getPrice() + ", C_ISALLOT = '" + price.getIsallot() + "', C_MINDAY = " + price.getMinday()
                    + ", C_PRICEOFFER = " + price.getPriceoffer() + ", C_QUNARPRICE = " + price.getQunarprice()
                    + " , C_UPDATETIME = '" + price.getUpdatetime() + "', C_YULIUNUM = '" + price.getYuliuNum()
                    + "', C_ROOMSTATUS = '" + price.getRoomstatus() + "', C_CITYID = " + price.getCityid()
                    + ", C_JLTIME = '" + jltime + "', C_JLALLMENT = '" + price.getAllotmenttype()
                    + "', C_JLRATENAME = '" + price.getRatetype() + "', C_SUPPLYID = " + price.getSupplyid()
                    + ", C_PROD = '" + price.getProd() + "', C_CANCELDESC = '" + price.getCanceldesc()
                    + "', C_CANCELTYPE = " + price.getCanceltype() + ", C_CANCELADVANCE = " + price.getCanceladvance()
                    + ", C_CANCELTIME = '" + price.getCanceltime() + "', C_NOEDITCANCEL = " + price.getNoeditcancel()
                    + ", C_NOEDITTYPE = '" + price.getNoedittype() + "', C_GUAMONEYTYPE = " + price.getGuamoneytype()
                    + " where C_JLKEYID = '" + jlkeyid + "';";
            Server.getInstance().getSystemService().findMapResultBySql(updatesql, null);
            System.out.println(timeFormat.format(new Date()) + " 更新价格：" + hotel.getName() + "~~~"
                    + result.getRoomtype() + "~~~" + result.getRatetype() + "~~~" + price.getStatedate());
            oldkeys.remove(jlkeyid);
        }
    }

}