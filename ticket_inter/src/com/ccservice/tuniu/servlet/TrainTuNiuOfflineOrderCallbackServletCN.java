/**
 * 版权所有, 空铁无忧
 * Author: 火车票 H5-微信端 项目开发组
 * copyright: 2017
 */
package com.ccservice.tuniu.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tools.ant.types.CommandlineJava.SysProperties;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.PropertyUtil;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.offline.dao.MailAddressDao;
import com.ccservice.offline.dao.TrainOfflineAgentKeyDao;
import com.ccservice.offline.dao.TrainOrderOfflineDao;
import com.ccservice.offline.dao.TrainOrderOfflineRecordDao;
import com.ccservice.offline.dao.TrainPassengerOfflineDao;
import com.ccservice.offline.dao.TrainTicketOfflineDao;
import com.ccservice.offline.domain.MailAddress;
import com.ccservice.offline.domain.TrainOfflineAgentKey;
import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.domain.TrainPassengerOffline;
import com.ccservice.offline.domain.TrainTicketOffline;
import com.ccservice.offline.util.ExceptionTNUtil;
import com.ccservice.offline.util.HttpPostJsonUtil;
import com.ccservice.offline.util.HttpUtil;
import com.ccservice.offline.util.TrainOrderOfflineUtil;

/**
 * @className: com.ccservice.tuniu.train.servlet.TrainTuNiuOfflineOrderServlet
 * @description: TODO - 
 * 
 * 途牛线下票 - 途牛线下票出票回调请求接口 - 由CN平台发起
 * 
 * 

//票价是否需要核验？ - 在出票回调中根据金额完成
//获取总票价的Double类型 - 做价格校验

 * 计算总票价 - 总票价之一 - InsurePrice不涉及折半优惠 - 可以放在外面直接计算 - InsurePrice * TicketCount
 * 放到外面算 - 也要判定其存在 的 业务逻辑 - 比较麻烦，不如直接放在内部加和计算
 * 
 * 总票价
 *  成人票的票价 + 儿童票的【半票价】 + 保险的票价 + 【邮寄的票价 - 暂无】
 *  
 *  TotalPrice - [需要订单提交的时候，即进行计算] - 来自车票中的价格的相关计算

//BigDecimal TotalInsurePrice = new BigDecimal(InsurePrice).multiply(new BigDecimal(TicketCount));
//TotalPrice = TotalPrice.add(TotalInsurePrice);

 * 
 * 
 * success true用231000，false用231099
 * 
 * 出票成功的话，需要取消掉出票回调请求的自动取消的定时任务 - 只有等待出票状态的单子才会完成自动解锁
 * 
 * 更新快递时效信息
 * 
 * 新增相关的校验的判定
 * 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月15日 上午9:34:25 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainTuNiuOfflineOrderCallbackServletCN extends HttpServlet {
    private static final String LOGNAME = "途牛线下票出票回调请求接口";

    private int r1 = new Random().nextInt(10000000);

    private TrainOrderOfflineDao trainOrderOfflineDao = new TrainOrderOfflineDao();
    private TrainOfflineAgentKeyDao trainOfflineAgentKeyDao = new TrainOfflineAgentKeyDao();
    private TrainTicketOfflineDao trainTicketOfflineDao = new TrainTicketOfflineDao();
    private TrainPassengerOfflineDao trainPassengerOfflineDao = new TrainPassengerOfflineDao();
    private MailAddressDao mailAddressDao = new MailAddressDao();
    private TrainOrderOfflineRecordDao trainOrderOfflineRecordDao = new TrainOrderOfflineRecordDao();//记录操作日志，单独封装工具类

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");

        BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));  
        String line = null;  
        StringBuilder sb = new StringBuilder();  
        while((line = br.readLine())!=null){  
            sb.append(line);  
        }
   
        //将资料解码  
        String reqBody = sb.toString();
        //System.out.println(reqBody);

        //记录请求信息日志
        WriteLog.write(LOGNAME, r1 + ":途牛线下票出票回调请求接口请求信息-reqBody-->"+reqBody);

        JSONObject requestBody = JSONObject.parseObject(reqBody);
        
        //"?orderId="+orderId+"&userid="+agentid+"&expressNum="+expressNum+"&expressCompanyName="+expressCompanyName+"&orderSuccess="+true;
        /*String orderId = request.getParameter("orderId");//系统表的主键的订单表的ID
        String userid = request.getParameter("userid");//当前系统的登录用户的ID - 用于记录操作日志
        String orderSuccesss = request.getParameter("orderSuccess");

        Long orderIdl = Long.valueOf(orderId);
        Integer useridi = Integer.valueOf(userid);

        Boolean orderSuccess = Boolean.valueOf(orderSuccesss);//出票成功或者失败
        
        
        resObj.put("orderId", OrderId);
        resObj.put("userid", useridi);
        resObj.put("refundReason", 108);
        resObj.put("refundreasonstr", "途牛锁票异步回调在"+LockWait+"s内没有结果反馈，回调出票失败");
        resObj.put("orderSuccess", false);
        
        */

        Long orderId = requestBody.getLong("orderId");
        Integer useridi = requestBody.getInteger("userid");

        Boolean orderSuccess = requestBody.getBoolean("orderSuccess");//出票成功或者失败
        
        String expressNum = "";
        String expressCompanyName = "";
        String refundReason = "";
        String refundreasonstr = "";

        if (orderSuccess) {//出票成功
            /*expressNum = request.getParameter("expressNum");
            expressCompanyName = request.getParameter("expressCompanyName");//0-顺丰 - 1-EMS 2-宅急送*/
            
            expressNum = requestBody.getString("expressNum");
            expressCompanyName = requestBody.getString("expressCompanyName");
            
            //在此处就直接入库 - 方便二次出票回调

            //更新快递单号和快递公司
            mailAddressDao.updateMailAddressExpress(orderId.intValue(), expressNum, TrainOrderOfflineUtil.getExpressAgentByName(expressCompanyName));
            
            
        } else {//出票失败
            /*refundReason = request.getParameter("refundReason");
            refundreasonstr = URLDecoder.decode(request.getParameter("refundreasonstr"), "UTF-8");*///0-顺丰 - 1-EMS 2-宅急送

            refundReason = requestBody.getString("refundReason");
            refundreasonstr = requestBody.getString("refundreasonstr");//0-顺丰 - 1-EMS 2-宅急送
            
            if (refundreasonstr == null || refundreasonstr.equals("null") || refundreasonstr.equals("")) {
                refundreasonstr = TrainOrderOfflineUtil.getRefundreasonstrByRefundReason(refundReason);
            }
            
            //在此处就直接入库 - 方便二次出票回调

            //途牛错误原因的定制处理 - 我们这边对客展现有统一格式 - 把 - 原因： - 全额退款 - 字样删除
            if (refundreasonstr.contains("原因：")) {
                refundreasonstr = refundreasonstr.replace("原因：", "");
            }
            
            if (refundreasonstr.contains("，全额退款")) {
                refundreasonstr = refundreasonstr.replace("，全额退款", "");
            }
            else if (refundreasonstr.contains("全额退款")) {
                refundreasonstr = refundreasonstr.replace("全额退款", "");
            }
            
            //更新失败原因
            try {
                trainOrderOfflineDao.updateTrainOrderOfflineFailById(orderId, useridi, refundReason, refundreasonstr);
            }
            catch (Exception e) {
                ExceptionTNUtil.handleTNException(e);
            }
        }
        
        //记录请求信息日志
        WriteLog.write(LOGNAME, r1 + ":途牛线下票出票回调请求信息-orderId-->"+orderId+",userid-->"+useridi+",expressNum-->"+expressNum+",expressCompanyName-->"+expressCompanyName
                +",refundReason-->"+refundReason+",refundreasonstr-->"+refundreasonstr+",orderSuccess-->"+orderSuccess);

        //记录日志
        TrainOrderOfflineUtil.CreateBusLogAdmin(orderId, useridi, "发起出票回调请求");

        //从数据库中读取出相关位置的出票回调地址的信息
        TrainOfflineAgentKey trainOfflineAgentKey = null;
        try {
            trainOfflineAgentKey = trainOfflineAgentKeyDao.findTrainOfflineAgentKeyByPartnerName("trainTuNiuOfflineOrderCallBackUrl");
        }
        catch (Exception e1) {
            ExceptionTNUtil.handleTNException(e1);
        }

        if (trainOfflineAgentKey == null || trainOfflineAgentKey.getKeys().equals("")) {
            String content = "无法获取到相关位置的配置信息";
            TrainOrderOfflineUtil.CreateBusLogAdmin(orderId, useridi, content);
            //后期把所有的异常信息作为日志记录进行前台打印做针对性的处理
            //throw new RuntimeException(content);
        }
        
        JSONObject result = orderCallbackCN(orderId, useridi, expressNum, expressCompanyName, orderSuccess, trainOfflineAgentKey, refundReason, refundreasonstr);

        if (!result.getBooleanValue("success")) {
            TrainOrderOfflineUtil.CreateBusLogAdmin(orderId, useridi, result.getString("msg"));
        }
        
        WriteLog.write(LOGNAME, r1 + ":途牛线下票出票回调请求结果-result" + result);

        PrintWriter out = response.getWriter();

        out.print(result.toJSONString());
        //out.print(TrainOrderOfflineUtil.getNowDateStr()+"途牛线下票出票回调请求完成");
        out.flush();
        out.close();
    }

    private JSONObject orderCallbackCN(Long orderIdl, Integer useridi, String expressNum, String expressCompanyName,
            Boolean orderSuccess, TrainOfflineAgentKey trainOfflineAgentKey, String refundReason, String refundreasonstr) {
        JSONObject resResult = new JSONObject();

        String trainTuNiuOfflineOrderCallBackUrl = trainOfflineAgentKey.getKeys();
        
        //trainTuNiuOfflineOrderCallBackUrl = "http://218.94.82.118:9181/aln/common/delivery/confirmFeedback";
        
        WriteLog.write(LOGNAME, r1 + ":途牛线下票出票回调请求-进入到方法-orderCallbackCN");

        if (orderIdl == null || orderIdl == 0L) {
            resResult.put("success", "false");
            resResult.put("msg", "传入的订单号有误，请排查");
            return resResult;
        }
        
        TrainOrderOffline trainOrderOffline = null;
        try {
            trainOrderOffline = trainOrderOfflineDao.findTrainOrderOfflineById(orderIdl);
        }
        catch (Exception e) {
            return ExceptionTNUtil.handleTNException(e);
        }

        if (trainOrderOffline == null) {
            resResult.put("success", "false");
            resResult.put("msg", "该订单不存在，请排查");
            return resResult;
        }
        
        /*
         * 出票超时校验
         * 
         * 只需要校验状态，必须是等待出票的状态 - 而无需对超时时间做校验了
         * 
         * 超时之后，客户取消之前，都是仍然可以出单
         * 
         * try {
            if (trainOrderOffline.getIsLockCallback() && trainOrderOffline.getLockedStatus() == 1 && TrainOrderOfflineUtil.getOverTimeDateByStr(trainOrderOffline.getOrderTimeout()).getTime() < new Date().getTime()) {
                resResult.put("success", "false");
                resResult.put("msg", "该订单已出票超时，请选择拒单");
                return resResult;
            }
        }
        catch (Exception e2) {
            return ExceptionTNUtil.handleTNException(e2);
        }*/

        //WriteLog.write(LOGNAME, r1 + ":拒单流程状态值校验-trainOrderOffline:"+trainOrderOffline);

        if (trainOrderOffline.getIsLockCallback() == null) {
            trainOrderOffline.setIsLockCallback(false);
        }
        
        if (trainOrderOffline.getIsLockCallback() && trainOrderOffline.getOrderStatus() == 2) {
            resResult.put("success", "false");
            resResult.put("msg", "该订单已是出票状态，请联系客服处理");
            return resResult;
        }

        //这个是未锁单，直接拒单的时候 - 进不到此处， - 但是已锁单，再次拒单的时候，会在此处进行判定 - 所以需要修正 - 原有逻辑内的订单状态的修改时机
        if (trainOrderOffline.getIsLockCallback() && trainOrderOffline.getOrderStatus() == 3) {
            resResult.put("success", "false");
            resResult.put("msg", "该订单已被拒单或者取消，请联系客服处理");
            return resResult;
        }
        
        String orderId = trainOrderOffline.getOrderNumberOnline();

        HttpPostJsonUtil httpPostJsonUtil = new HttpPostJsonUtil();

        JSONObject dataJson = new JSONObject();
        
        /**
         * 
 SELECT * from TrainTicketOffline where OrderId=" + orderId;
 String sql1 = "UPDATE TrainTicketOffline SET Coach='" + coach + "',SeatNo='" + seatno + "',sealPrice="
                        + priceString + ",ticketNo='" + ticketNo + "',realSeat='" + extString + "' WHERE TrainPid="
                        + TrainPid;

                WriteLogTNUtil.write("TN线下票出票回调成功结果", "sql1:" + sql1); - 在上一步中已经完成了入库的操作
         * 
做超时时间校验
做价格校验
         * 
         */
        /*String ticketNo = "E000000";//出票成功之后回调的票号
        //String seatType = "软卧";//出票成功之后席别
        String seatNo = "14车厢，19座上铺";//14车厢，19座上铺
        
        //快递信息
        String deliveryCompanyName = "顺丰速运";//
        String trackingNumber = "666666666";//快递单号
        Double cost = 15.50;//*/
        
        dataJson.put("orderId", orderId);
        dataJson.put("orderSuccess", orderSuccess);//出票成功或者失败
        
        //这个是取票号 - 线下票没有，不传就可以了
        //dataJson.put("orderNumber", trainOrderOffline.getOrderNumber());
        dataJson.put("orderNumber", null);

        if (orderSuccess) {//出票成功
            JSONArray passengers = new JSONArray();
            
            List<TrainPassengerOffline> trainPassengerOfflineList = null;
            try {
                trainPassengerOfflineList = trainPassengerOfflineDao.findTrainPassengerOfflineListByOrderId(orderIdl);
            }
            catch (Exception e) {
                ExceptionTNUtil.handleTNException(e);
            }

            if (trainPassengerOfflineList == null) {
                String content = "无法查询获取到出票回调的乘客对象";
                TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, content);
                //throw new RuntimeException("");
            }
            
            //System.out.println(trainPassengerOfflineList);

            //价格校验 - 在前台完成了
            
            //判定总价 - 判定儿童票是否半价
            
            Double OrderPrice = 0.0D;
            
            for (int i = 0; i < trainPassengerOfflineList.size(); i++) {
                JSONObject passenger = new JSONObject();
                
                TrainPassengerOffline trainPassengerOffline = trainPassengerOfflineList.get(i);
                
                passenger.put("passengerId", Integer.valueOf(trainPassengerOffline.getPassengerId()));
                passenger.put("passengerName", trainPassengerOffline.getName());
                passenger.put("passportNo", trainPassengerOffline.getIdNumber());
                
                //int 类型和 String 类型之间需要做转换
                int IdType = trainPassengerOffline.getIdType();
                passenger.put("passportTypeId", TrainOrderOfflineUtil.getTuNiuResIdTypeStr(IdType));
                passenger.put("passportTypeName", TrainOrderOfflineUtil.getTuNiuIdTypeStr(IdType));
                
                TrainTicketOffline trainTicketOffline = null;
                try {
                    trainTicketOffline = trainTicketOfflineDao.findTrainTicketOfflineByTrainPid(trainPassengerOffline.getId());
                }
                catch (Exception e) {
                    ExceptionTNUtil.handleTNException(e);
                }

                if (trainTicketOffline == null) {
                    String content = "无法查询获取到出票回调的车票对象";
                    TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, content);
                    //throw new RuntimeException("");
                }
                
                /*trainTicketOffline.setTicketNo(ticketNo);
                //trainTicketOffline.setSeatType(seatType);
                trainTicketOffline.setSeatNo(seatNo);*/

                //System.out.println(trainTicketOffline);
                
                passenger.put("ticketNo", trainTicketOffline.getTicketNo());
                
                int ticketType = trainTicketOffline.getTicketType();
                passenger.put("piaoType", ticketType);
                passenger.put("piaoTypeName", TrainOrderOfflineUtil.getTicketTypeStr(ticketType));
                
                /*passenger.put("zwCode", TrainOrderOfflineUtil.getSeatTypeCode(trainTicketOffline.getSeatType()));
                passenger.put("zwName", trainTicketOffline.getSeatType());*/
                
                String realSeat = trainTicketOffline.getRealSeat();
                if (realSeat == null) {
                    passenger.put("zwCode", "");
                } else {
                    passenger.put("zwCode", TrainOrderOfflineUtil.getSeatTypeCode(realSeat));
                }
                
                passenger.put("zwName", trainTicketOffline.getRealSeat());

                String SeatNo = trainTicketOffline.getSeatNo();//座位号 - 一般情况下不区分 这两个字段 - 统一放在一起 - //14车厢，19座上铺 - //15车 008
                String SeatType = trainTicketOffline.getSeatType();//席别
                String SeatName = "";
                
                if (SeatNo.contains("座") || SeatNo.contains("铺") || SeatNo.contains("号")) {
                    SeatName = SeatNo;
                } else {
                    if (SeatType.contains("座")) {
                        SeatName = SeatNo+"座";
                    } else if (SeatType.contains("卧")) {
                        SeatName = SeatNo+"铺";
                    } else {
                        SeatName = SeatNo+"号";
                    }
                }
                
                passenger.put("cxin", trainTicketOffline.getCoach()+"车厢，"+SeatName);
                
                //passenger.put("price", trainTicketOffline.getPrice());
                
                Double sealPrice = trainTicketOffline.getSealPrice();
                
                if (sealPrice == null) {
                    passenger.put("price", "");
                    OrderPrice = null;
                } else {
                    passenger.put("price", sealPrice);
                    OrderPrice = OrderPrice + sealPrice;
                }
                
                passengers.add(passenger);
            }

            dataJson.put("orderAmount", OrderPrice);
            
            dataJson.put("passengers", passengers);
            
            JSONObject deliveryInformation = new JSONObject();

            //deliveryInformation.put("deliveryCompanyName", deliveryCompanyName);
            deliveryInformation.put("deliveryCompanyName", TrainOrderOfflineUtil.getExpressAgentNameByName(expressCompanyName));
            
            //从MailAddress中取出快递单号 - ExpressNum
            //deliveryInformation.put("trackingNumber", trackingNumber);
            deliveryInformation.put("trackingNumber", expressNum);
            
            //deliveryInformation.put("cost", cost);

            dataJson.put("deliveryInformation", deliveryInformation);//快递信息

        } else {//出票失败
            dataJson.put("orderAmount", null);
            dataJson.put("passengers", null);
            dataJson.put("deliveryInformation", null);//快递信息
            dataJson.put("msg", refundreasonstr);//失败原因
        }
        
        //String data = "{\"orderId\":\"test17081800133473\",\"msg\":\"请求锁票\"}";
        String data = dataJson.toJSONString();
        //System.out.println(data);

        //记录请求信息日志
        WriteLog.write(LOGNAME, r1 + ":途牛线下票出票回调请求反馈信息-dataJson" + data);

        String timestamp = TrainOrderOfflineUtil.getTuNiuReqTimestamp();

        String errorMsg = "";
        String res = "";
        
        for (int i = 0; i < TrainOrderOfflineUtil.TNRETRY; i++) {
            res = httpPostJsonUtil.getTuNiuReqRes(data, timestamp, trainTuNiuOfflineOrderCallBackUrl, errorMsg);//{"success":true}

            WriteLog.write(LOGNAME, r1 + ":途牛线下票出票回调请求-res:"+res);

            if (res != null && res.contains("true")) {//重试请求三次
                resResult.put("success", "true");

                //更新出票点的相关信息
                //修改订单状态和出票时间 - ChuPiaoTime - OrderStatus
                try {
                    trainOrderOfflineDao.updateTrainOrderOfflineSuccessById(orderIdl, useridi);
                }
                catch (Exception e) {
                    return ExceptionTNUtil.handleTNException(e);
                }

                String content = "";
                if (orderSuccess) {
                    //快递信息如果存在的话，在上面就直接入库了
                    
                    //记录日志记录
                    content = "出票成功，回调成功";
                    TrainOrderOfflineUtil.CreateBusLogAdminOtherDealResult(orderIdl, useridi, content, 1);//1 - 订单出票完成
                    
                    //更新票的状态
                    //订单状态 - 1-等待出票 - 2-出票成功 - 3-已拒单【已取消】 - 其他值都是无效状态 - 可操作-是在页面上进行的判断处理
                    //questionDraw = "1"：出票采购问题/邮寄采购问题 questionDraw = "2" //出票成功已邮寄/出票成功待邮寄 - 采购问题订单
                    trainOrderOfflineDao.updateTrainOrderOfflineStatusQuestionById(orderIdl, 2, 2);
                    
                } else {
                    //更新失败原因 - 如果存在的话，在上面就直接入库了
                    
                    //记录日志记录
                    TrainOrderOfflineUtil.CreateBusLogAdminNoUserId(orderIdl, refundreasonstr);

                    content = "出票失败，回调成功";//以第一次拒绝为准
                    //TrainOrderOfflineUtil.CreateBusLogAdminOtherDealResult(orderIdl, useridi, content, 2);//2 - 订单被拒绝
                    TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, content);

                    //更新票的状态
                    //订单状态 - 1-等待出票 - 2-出票成功 - 3-已拒单【已取消】 - 其他值都是无效状态 - 可操作-是在页面上进行的判断处理
                    //questionDraw = "1"：出票采购问题/邮寄采购问题 questionDraw = "2" //出票成功已邮寄/出票成功待邮寄 - 采购问题订单
                    trainOrderOfflineDao.updateTrainOrderOfflineStatusQuestionById(orderIdl, 3, 1);
                    
                }

                //更新响应时间
                try {
                    trainOrderOfflineRecordDao.updateTrainOrderOfflineRecordResponseTimeByOrderIdAndProviderAgentid(orderIdl, useridi);
                }
                catch (Exception e) {
                    return ExceptionTNUtil.handleTNException(e);
                }

                resResult.put("success", "true");
                resResult.put("msg", content);

                //记录请求信息日志
                WriteLog.write(LOGNAME, r1 + ":同程线下票出票回调请求反馈信息-resResult" + resResult);

                return resResult;
            } else {
                if (res == null || res.equals("")) {
                    if (i == (TrainOrderOfflineUtil.TNRETRY-1)) {
                        //记录日志
                        TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "由于网络原因，出票回调请求反馈失败，稍后重试或者联系技术处理");
                        
                        resResult.put("success", "false");
                        resResult.put("msg", "由于网络原因，导致途牛的出票回调请求接口无法访问，请您暂且不要操作，稍后重试或者联系客服处理");

                        //记录请求信息日志
                        WriteLog.write(LOGNAME, r1 + ":同程线下票出票回调请求反馈信息-resResult" + resResult);

                        trainOrderOfflineDao.updateTrainOrderOfflineStatusQuestionById(orderIdl, 1, 1);//出票问题订单
                        
                        return resResult;
                    }
                    //记录操作记录
                    TrainOrderOfflineUtil.sleep(10*1000);
                    continue;
                }

                resResult.put("success", "false");
                resResult.put("msg", "未知原因");
                
                /*if (res != null && !res.equals("")) {
                    JSONObject resJson = JSONObject.parseObject(res);
                    resResult.put("msg", resJson.getString("msgInfo"));
                }*/
                return resResult;
            }
        }

        //记录请求信息日志
        WriteLog.write(LOGNAME, r1 + ":途牛线下票出票回调请求反馈信息-resResult" + resResult);

        return resResult;
    }

    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        HttpPostJsonUtil httpPostJsonUtil = new HttpPostJsonUtil();

        Long OrderId = 1594L;
        Integer useridi = 382;
        String LockWait = "300";
        
        String result = "";

        //String tuniuOrderCallbackServlet = PropertyUtil.getValue("TuniuOrderCallbackServlet", "Train.GuestAccount.properties");

        String tuniuOrderCallbackServlet = "http://localhost:8097/ticket_inter/TrainTuNiuOfflineOrderCallbackServletCN";
             
        /*String data = "?orderId="+OrderId+"&userid="+useridi+"&refundReason="+108
                        +"&refundreasonstr="+URLEncoder.encode("途牛锁票异步回调在"+LockWait+"s内没有结果反馈，回调出票失败", "UTF-8")+"&orderSuccess="+false;*/
        
        /**
         * 
{"refundReason":108,"userid":382,"orderSuccess":false,"refundreasonstr":"閫旂墰閿佺エ寮傛鍥炶皟鍦?00s鍐呮病鏈夌粨鏋滃弽棣堬紝鍥炶皟鍑虹エ澶辫触","orderId":1594}
         * 
         */
        JSONObject resObj = new JSONObject();
        resObj.put("orderId", OrderId);
        resObj.put("userid", useridi);
        resObj.put("refundReason", 108);
        resObj.put("refundreasonstr", "途牛锁票异步回调在"+LockWait+"s内没有结果反馈，回调出票失败");
        resObj.put("orderSuccess", false);
        
        try {
            result = new HttpUtil().doPost(tuniuOrderCallbackServlet, resObj.toJSONString());
        }
        catch (Exception e1) {
            e1.printStackTrace();
        }
        System.out.println(result);

        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间： " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间： " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间： " + runTimeS / 60 + "min：" + runTimeS % 60 + "s");//分
    }
    
}
