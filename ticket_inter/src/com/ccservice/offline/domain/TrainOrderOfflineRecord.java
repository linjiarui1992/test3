/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.domain;

/**
 * @className: com.ccservice.tuniu.train.domain.TrainPassengerOffline
 * @description: TODO - 操作记录表  订单详情页的操作记录显示
 * 
 * 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月16日 下午12:06:51 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOrderOfflineRecord {
    private Long PKId;//
    private Long FKTrainOrderOfflineId;//火车票线下订单id
    private Integer ProviderAgentid;//供应商（售票点agentid） - 不仅仅是代售点出票点，操作记录的不同角色操作都会用这个字段
    private String DistributionTime;//分配时间
    private String ResponseTime;//响应时间
    
    private Integer DealResult;//处理结果
    private Integer RefundReason;//拒单原因 - 状态值
    
    /**
     * 
     * 

//操作记录的记录内容
 * 
 * 
 * 222 - getAllRecordList - agents - 显示名字专用

                String dealReault = "";
                if ("0".equals(map.get("DealResult").toString())) {
                    dealReault = "订单发放成功，等待处理...";
                }
                else if ("1".equals(map.get("DealResult").toString())) {
                    dealReault = "订单出票完成！";
                }
                else if ("2".equals(map.get("DealResult").toString())) {
                    dealReault = "订单被拒绝！";
                }
                else if ("3".equals(map.get("DealResult").toString())) {
                    dealReault = "订单邮寄完成！";
                }
                else if ("6".equals(map.get("DealResult").toString())) {
                    dealReault = "订单推送成功，等待支付...";
                }
                else if ("7".equals(map.get("DealResult").toString())) {
                    dealReault = "请求采购商差额退款成功...";
                }
                else if ("8".equals(map.get("DealResult").toString())) {
                    dealReault = "采购商响应差额退款成功通知！";
                }
                else if ("11".equals(map.get("DealResult").toString())) {
                    dealReault = "订单锁单成功！";
                }
                else if ("12".equals(map.get("DealResult").toString())) {
                    dealReault = "订单锁单失败！";
                }
                else if ("13".equals(map.get("DealResult").toString())) {
                    dealReault = "" + map.get("RefundReasonStr").toString();
                }
                else if ("51".equals(map.get("DealResult").toString())) {
                    dealReault = "" + map.get("RefundReasonStr").toString();
                }
                else if ("113".equals(map.get("DealResult").toString())) {
                    dealReault = "" + map.get("RefundReasonStr").toString();
                }
                else if ("14".equals(map.get("DealResult").toString())) {
                    dealReault = "" + map.get("RefundReasonStr").toString();
                }
                else if ("15".equals(map.get("DealResult").toString())) {
                    dealReault = "" + map.get("RefundReasonStr").toString();
                }
                else if ("16".equals(map.get("DealResult").toString())) {
                    dealReault = "" + map.get("RefundReasonStr").toString();
                }
                else if ("116".equals(map.get("DealResult").toString())) {
                    dealReault = "" + map.get("RefundReasonStr").toString();
                }
                else if ("19".equals(map.get("DealResult").toString())) {
                    dealReault = "" + map.get("RefundReasonStr").toString();
                }
                else if ("119".equals(map.get("DealResult").toString())) {
                    dealReault = "" + map.get("RefundReasonStr").toString();
                }
                else if ("17".equals(map.get("DealResult").toString())) {
                    dealReault = "" + map.get("RefundReasonStr").toString();
                }
                else if ("21".equals(map.get("DealResult").toString())) {
                    dealReault = "" + map.get("RefundReasonStr").toString();
                }
                else if ("4".equals(map.get("DealResult").toString())) {
                    if (!"".equals(map.get("RefundReasonStr").toString())
                            && map.get("RefundReasonStr").toString() != null) {
                        dealReault = "订单重新分配完成！操作员:" + map.get("RefundReasonStr").toString();
                    }
                    else {
                        dealReault = "订单重新分配完成！";
                    }
                }



拒单原因的逻辑判断

                //客服操作拒单
                else if ("5".equals(map.get("DealResult").toString())) {
                    dealReault = "操作拒单:-----回调采购商拒单接口！";
                }

                String refundReason = "";
                if ("0".equals(map.get("RefundReason").toString())) {
                    refundReason = "";
                }
                else if ("1".equals(map.get("RefundReason").toString())) {
                    refundReason = "原因：订单价格不符！";
                }
                else if ("2".equals(map.get("RefundReason").toString())) {
                    refundReason = "原因：订单行程错误！";
                }
                else if ("4".equals(map.get("RefundReason").toString())) {
                    refundReason = "原因：无法满足定制服务！";
                }
                else if ("3".equals(map.get("RefundReason").toString())) {
                    refundReason = "原因：" + map.get("RefundReasonStr").toString() + "！";
                }
                else if ("15".equals(map.get("RefundReason").toString())) {
                    refundReason = "原因：" + map.get("RefundReasonStr").toString() + "！";
                }
                //客服操作拒单
                else if ("11".equals(map.get("RefundReason").toString())) {
                    refundReason = "原因：所购买的车次坐席已无票！";
                }
                else if ("13".equals(map.get("RefundReason").toString())) {
                    refundReason = "原因：去哪儿票价和12306不符！";
                }
                else if ("14".equals(map.get("RefundReason").toString())) {
                    refundReason = "原因：车次数据与12306不一致！";
                }
                else if ("15".equals(map.get("RefundReason").toString())) {
                    refundReason = "原因：乘客信息错误！";
                }
                else if ("10".equals(map.get("RefundReason").toString())) {
                    refundReason = "原因：" + map.get("RefundReasonStr").toString() + "！";
                }//拒单淘宝逻辑
                else if ("31".equals(map.get("RefundReason").toString())) {
                    refundReason = "原因：票已售完，出票失败全额退款！";
                }
                else if ("32".equals(map.get("RefundReason").toString())) {
                    refundReason = "原因：票价变动，出票失败全额退款！";
                }
                else if ("33".equals(map.get("RefundReason").toString())) {
                    refundReason = "原因：乘车人已购买相同车票，出票失败全额退款！";
                }
                else if ("34".equals(map.get("RefundReason").toString())) {
                    refundReason = "原因：出票超时，出票失败全额退款！";
                }
                else if ("35".equals(map.get("RefundReason").toString())) {
                    refundReason = "原因：乘车人证件未通过铁路局审核，需到售票窗口办理！";
                }
                else if ("36".equals(map.get("RefundReason").toString())) {
                    refundReason = "原因：发车时间变动，出票失败全额退款！";
                }
                else if ("37".equals(map.get("RefundReason").toString())) {
                    refundReason = "原因：车次信息错误，出票失败全额退款！";
                }
                else if ("38".equals(map.get("RefundReason").toString())) {
                    refundReason = "原因：12306故障,出票失败全额退款！";
                }
                else if ("30".equals(map.get("RefundReason").toString())) {
                    refundReason = "原因：出票失败全额退款！";
                }
                else if ("26".equals(map.get("RefundReason").toString())) {
                    refundReason = "原因：邮寄地址无法保证及时送达！";
                }
                else if ("27".equals(map.get("RefundReason").toString())) {
                    refundReason = "原因：无法满足用户定制需求！";
                }
                else if ("28".equals(map.get("RefundReason").toString())) {
                    refundReason = "原因：与乘客沟通，乘客同意主动取消订单！";
                }
                else if ("39".equals(map.get("RefundReason").toString())) {
                    refundReason = "原因：锁单失败，全额退款！";
                }
                //去哪
                else if ("101".equals(map.get("RefundReason").toString())) {
                    refundReason = "原因：所购买的车次坐席已无票！";
                }
                else if ("102".equals(map.get("RefundReason").toString())) {
                    refundReason = "原因：票价和12306不符！";
                }
                else if ("103".equals(map.get("RefundReason").toString())) {
                    refundReason = "原因：车次数据与12306不一致！";
                }
                else if ("104".equals(map.get("RefundReason").toString())) {
                    refundReason = "原因：乘客信息错误！";
                }
                else if ("105".equals(map.get("RefundReason").toString())) {
                    refundReason = "原因：快递无法送达！";
                }
                else if ("106".equals(map.get("RefundReason").toString())) {
                    refundReason = "原因：无下铺票！";
                }
                else if ("107".equals(map.get("RefundReason").toString())) {
                    refundReason = "原因：无靠窗同包厢票！";
                }
                else if ("108".equals(map.get("RefundReason").toString())) {
                    refundReason = "原因：其他！";
                }

     * 
     * 
     */
    
    private String RefundReasonStr;//拒单原因
    public Long getPKId() {
        return PKId;
    }
    public void setPKId(Long pKId) {
        PKId = pKId;
    }
    public Long getFKTrainOrderOfflineId() {
        return FKTrainOrderOfflineId;
    }
    public void setFKTrainOrderOfflineId(Long fKTrainOrderOfflineId) {
        FKTrainOrderOfflineId = fKTrainOrderOfflineId;
    }
    public Integer getProviderAgentid() {
        return ProviderAgentid;
    }
    public void setProviderAgentid(Integer providerAgentid) {
        ProviderAgentid = providerAgentid;
    }
    public String getDistributionTime() {
        return DistributionTime;
    }
    public void setDistributionTime(String distributionTime) {
        DistributionTime = distributionTime;
    }
    public String getResponseTime() {
        return ResponseTime;
    }
    public void setResponseTime(String responseTime) {
        ResponseTime = responseTime;
    }
    public Integer getDealResult() {
        return DealResult;
    }
    public void setDealResult(Integer dealResult) {
        DealResult = dealResult;
    }
    public Integer getRefundReason() {
        return RefundReason;
    }
    public void setRefundReason(Integer refundReason) {
        RefundReason = refundReason;
    }
    public String getRefundReasonStr() {
        return RefundReasonStr;
    }
    public void setRefundReasonStr(String refundReasonStr) {
        RefundReasonStr = refundReasonStr;
    }
    
    @Override
    public String toString() {
        return "TrainOrderOfflineRecord [PKId=" + PKId + ", FKTrainOrderOfflineId=" + FKTrainOrderOfflineId
                + ", ProviderAgentid=" + ProviderAgentid + ", DistributionTime=" + DistributionTime + ", ResponseTime="
                + ResponseTime + ", DealResult=" + DealResult + ", RefundReason=" + RefundReason + ", RefundReasonStr="
                + RefundReasonStr + "]";
    }
    
}
