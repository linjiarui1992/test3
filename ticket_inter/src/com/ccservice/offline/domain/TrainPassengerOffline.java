/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.domain;

/**
 * @className: com.ccservice.tuniu.train.domain.TrainPassengerOffline
 * @description: TODO - 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月16日 下午12:06:51 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainPassengerOffline {
    private Long Id;//
    private Long OrderId;//火车票线下订单ID
    private String Name;//乘客姓名
    private Integer IdType;
    //乘客证件类型 - 1:二代身份证，2:一代身份证，C:港澳通行证，G:台湾通行证，B:护照 ,H:外国人居留证 - 途牛
    //乘客证件类型 - 1: 身份证，2：护照，3：台胞证，4：港澳通行证 - 同程
    private String IdNumber;//乘客证件号码
    private String Birthday;//
    private String PassengerId;//采购商的分销客户ID
    public String getPassengerId() {
        return PassengerId;
    }
    public void setPassengerId(String passengerId) {
        PassengerId = passengerId;
    }
    public Long getId() {
        return Id;
    }
    public void setId(Long id) {
        Id = id;
    }
    public Long getOrderId() {
        return OrderId;
    }
    public void setOrderId(Long orderId) {
        OrderId = orderId;
    }
    public String getName() {
        return Name;
    }
    public void setName(String name) {
        Name = name;
    }
    public Integer getIdType() {
        return IdType;
    }
    public void setIdType(Integer idType) {
        IdType = idType;
    }
    public String getIdNumber() {
        return IdNumber;
    }
    public void setIdNumber(String idNumber) {
        IdNumber = idNumber;
    }
    public String getBirthday() {
        return Birthday;
    }
    public void setBirthday(String birthday) {
        Birthday = birthday;
    }
    
    @Override
    public String toString() {
        return "TrainPassengerOffline [Id=" + Id + ", OrderId=" + OrderId + ", Name=" + Name + ", IdType=" + IdType
                + ", IdNumber=" + IdNumber + ", Birthday=" + Birthday + ", PassengerId=" + PassengerId + "]";
    }
    
}
