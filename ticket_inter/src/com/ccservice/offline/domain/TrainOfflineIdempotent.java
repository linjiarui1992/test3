/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.domain;

/**
 * @className: com.ccservice.offline.domain.TrainOfflineIdempotent
 * @description: TODO - 幂等设计的持久化类
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年10月10日 下午5:51:21 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOfflineIdempotent {
    private Integer PKID;//主键
    private String idempotentLockKey;//幂等的基础标识key
    private Boolean idempotentLockValue;//幂等的基础标识value
    private String insertTime;//插入的初始化的时间
    private String updateTime;//后期更新的时间
    private Integer idempotentReqNum;//请求次数 - 默认是1
    private Integer idempotentResNum;//响应次数 - 默认是0
    private String idempotentResultKey;//幂等中上一次响应的结果的key的存储
    private String idempotentResultValue;//幂等中上一次响应的结果的value的获取
    private String remark1;//两个备注而已
    private String remark2;//两个备注而已
    public Integer getPKID() {
        return PKID;
    }
    public void setPKID(Integer pKID) {
        PKID = pKID;
    }
    public String getIdempotentLockKey() {
        return idempotentLockKey;
    }
    public void setIdempotentLockKey(String idempotentLockKey) {
        this.idempotentLockKey = idempotentLockKey;
    }
    public Boolean getIdempotentLockValue() {
        return idempotentLockValue;
    }
    public void setIdempotentLockValue(Boolean idempotentLockValue) {
        this.idempotentLockValue = idempotentLockValue;
    }
    public String getInsertTime() {
        return insertTime;
    }
    public void setInsertTime(String insertTime) {
        this.insertTime = insertTime;
    }
    public String getUpdateTime() {
        return updateTime;
    }
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
    public Integer getIdempotentReqNum() {
        return idempotentReqNum;
    }
    public void setIdempotentReqNum(Integer idempotentReqNum) {
        this.idempotentReqNum = idempotentReqNum;
    }
    public Integer getIdempotentResNum() {
        return idempotentResNum;
    }
    public void setIdempotentResNum(Integer idempotentResNum) {
        this.idempotentResNum = idempotentResNum;
    }
    public String getIdempotentResultKey() {
        return idempotentResultKey;
    }
    public void setIdempotentResultKey(String idempotentResultKey) {
        this.idempotentResultKey = idempotentResultKey;
    }
    public String getIdempotentResultValue() {
        return idempotentResultValue;
    }
    public void setIdempotentResultValue(String idempotentResultValue) {
        this.idempotentResultValue = idempotentResultValue;
    }
    public String getRemark1() {
        return remark1;
    }
    public void setRemark1(String remark1) {
        this.remark1 = remark1;
    }
    public String getRemark2() {
        return remark2;
    }
    public void setRemark2(String remark2) {
        this.remark2 = remark2;
    }
    @Override
    public String toString() {
        return "TrainOfflineIdempotent [PKID=" + PKID + ", idempotentLockKey=" + idempotentLockKey
                + ", idempotentLockValue=" + idempotentLockValue + ", insertTime=" + insertTime + ", updateTime="
                + updateTime + ", idempotentReqNum=" + idempotentReqNum + ", idempotentResNum=" + idempotentResNum
                + ", idempotentResultKey=" + idempotentResultKey + ", idempotentResultValue=" + idempotentResultValue
                + ", remark1=" + remark1 + ", remark2=" + remark2 + "]";
    }
}
