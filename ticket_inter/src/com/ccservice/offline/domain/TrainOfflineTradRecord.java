/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.domain;

/**
 * @className: com.ccservice.offline.domain.TrainOfflineTradRecord
 * @description: TODO - 结算的操作记录表
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年9月23日 下午4:31:33 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOfflineTradRecord {
    private Integer Id;//
    private Integer orderId;//订单号
    private Double agentId;//代理商Id
    private Double price;//实际出票价格
    private Double express;//快递费
    private Double shouxuPrice;//服务费
    private String createtime;//出票时间
    private Double alternative1;//备选字段1
    private String alternative2;//备选字段2
    public Integer getId() {
        return Id;
    }
    public void setId(Integer id) {
        Id = id;
    }
    public Integer getOrderId() {
        return orderId;
    }
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }
    public Double getAgentId() {
        return agentId;
    }
    public void setAgentId(Double agentId) {
        this.agentId = agentId;
    }
    public Double getPrice() {
        return price;
    }
    public void setPrice(Double price) {
        this.price = price;
    }
    public Double getExpress() {
        return express;
    }
    public void setExpress(Double express) {
        this.express = express;
    }
    public Double getShouxuPrice() {
        return shouxuPrice;
    }
    public void setShouxuPrice(Double shouxuPrice) {
        this.shouxuPrice = shouxuPrice;
    }
    public String getCreatetime() {
        return createtime;
    }
    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }
    public Double getAlternative1() {
        return alternative1;
    }
    public void setAlternative1(Double alternative1) {
        this.alternative1 = alternative1;
    }
    public String getAlternative2() {
        return alternative2;
    }
    public void setAlternative2(String alternative2) {
        this.alternative2 = alternative2;
    }
    @Override
    public String toString() {
        return "TrainOfflineTradRecord [Id=" + Id + ", orderId=" + orderId + ", agentId=" + agentId + ", price=" + price
                + ", express=" + express + ", shouxuPrice=" + shouxuPrice + ", createtime=" + createtime
                + ", alternative1=" + alternative1 + ", alternative2=" + alternative2 + "]";
    }
}
