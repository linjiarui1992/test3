/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.domain;

/**
 * @className: com.ccservice.tuniu.train.domain.TrainOfflineExpressCode
 * @description: TODO - 顺丰邮寄区域的相关首字编码
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月28日 下午4:46:17 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOfflineExpressCode {
    private Integer PKId;
    private Integer ExpressId;
    private String Province;//
    private String ProvinceSimple;//
    private String City;//
    private String CityCode;//
    public Integer getPKId() {
        return PKId;
    }
    public void setPKId(Integer pKId) {
        PKId = pKId;
    }
    public Integer getExpressId() {
        return ExpressId;
    }
    public void setExpressId(Integer expressId) {
        ExpressId = expressId;
    }
    public String getProvince() {
        return Province;
    }
    public void setProvince(String province) {
        Province = province;
    }
    public String getProvinceSimple() {
        return ProvinceSimple;
    }
    public void setProvinceSimple(String provinceSimple) {
        ProvinceSimple = provinceSimple;
    }
    public String getCity() {
        return City;
    }
    public void setCity(String city) {
        City = city;
    }
    public String getCityCode() {
        return CityCode;
    }
    public void setCityCode(String cityCode) {
        CityCode = cityCode;
    }
    @Override
    public String toString() {
        return "TrainOfflineExpressCode [PKId=" + PKId + ", ExpressId=" + ExpressId + ", Province=" + Province
                + ", ProvinceSimple=" + ProvinceSimple + ", City=" + City + ", CityCode=" + CityCode + "]";
    }
}
