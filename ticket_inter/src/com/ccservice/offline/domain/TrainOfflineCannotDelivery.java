/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.domain;

/**
 * @className: com.ccservice.offline.domain.TrainOfflineCannotDelivery
 * @description: TODO - 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年11月22日 下午3:55:09 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOfflineCannotDelivery {
    /**
     * TrainOfflineCannotDelivery
     * 
    [2017-11-22 15:03:23.50] 2896736:同程线下票送票上门地址核验信息请求-reqBody-->
    {"fromStation":"柴河","trainNo":"K40",
    "departureTime":"2017-11-23 19:01",
    "province":"黑龙江省","city":"牡丹江市","district":"海林市","address":"柴河林业局友谊家园5号楼5单元501"}
    
    [2017-11-22 15:03:23.503] 2896736:同程线下票送票上门地址核验结果-resResult
    {"arriveTime":"2017-11-24 18:00:00","msgCode":231000,"msgInfo":"","isSuccess":true,"expressType":0,"canDelivery":false}
     * 
     * 送票的地址信息 - 
     * 省市区地址
     * 
     * 发车日期-发车站
     * departureTimeRapidSend[yyyy-MM-dd HH:mm:ss]-fromStation
     * 
     * 预计送达时间，匹配到的快递类型 - 0-顺丰 - 1-EMS 2-宅急送 3-京东 - 99配送到站 10-闪送【UU跑腿】
     * arriveTime-delieveType
     */
    private Integer PKID;//

    private String insertTime;//数据的默认的数据库的插入日期

    private String fromStation;//发车站
    //送票的地址信息 - 省市区地址

    private String province;//目的地省

    private String city;//目的地市

    private String district;//目的地区

    private String address;//目的地地址

    private String departureTime;//发车日期 格式：yyyy-MM-dd HH:mm:ss

    private Integer delieveType;//获取除了UU跑腿之外的快递类型 - //0-顺丰 - 1-EMS 2-宅急送 3-京东 - 99配送到站 10-闪送【UU跑腿】 - [-1]

    private String arriveTime;//预计送达时间 yyyy-MM-dd HH:mm:ss

    private String remark1;//备注字段1

    private String remark2;//备注字段2

    public Integer getPKID() {
        return PKID;
    }

    public void setPKID(Integer pKID) {
        PKID = pKID;
    }

    public String getInsertTime() {
        return insertTime;
    }

    public void setInsertTime(String insertTime) {
        this.insertTime = insertTime;
    }

    public String getFromStation() {
        return fromStation;
    }

    public void setFromStation(String fromStation) {
        this.fromStation = fromStation;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public Integer getDelieveType() {
        return delieveType;
    }

    public void setDelieveType(Integer delieveType) {
        this.delieveType = delieveType;
    }

    public String getArriveTime() {
        return arriveTime;
    }

    public void setArriveTime(String arriveTime) {
        this.arriveTime = arriveTime;
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1;
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2;
    }

    @Override
    public String toString() {
        return "TrainOfflineCannotDelivery [PKID=" + PKID + ", insertTime=" + insertTime + ", fromStation="
                + fromStation + ", province=" + province + ", city=" + city + ", district=" + district + ", address="
                + address + ", departureTime=" + departureTime + ", delieveType=" + delieveType + ", arriveTime="
                + arriveTime + ", remark1=" + remark1 + ", remark2=" + remark2 + "]";
    }

}
