/**
 * 版权所有;//空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.domain;

/**
 * @className: com.ccservice.offline.domain.TrainOfflineExpRec
 * @description: TODO - 
 * @author: 郑州-技术-杨威   E-mail:653395455@qq.com
 * @createTime: 2017年9月5日 上午10:53:28 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOrderOfflineStatistics {
    private Integer id;//
    private Integer agentId;//
    private Integer finishTime;//出票时长（成功）
    private Integer avgAllTime;//出票时长（总）
    private double  finishAvg;//成功率
    private Integer letterSix;//6分钟内订单数
    private Integer biggerSix;//6-10分钟订单数
    private Integer biggerTen;//10分钟以上订单数
    private Integer finishSum;//出票订单数
    private Integer allOrderSum;//总订单数
    private String  staticDate;//统计时间
    private Integer ranking;//排名
    private double  grade;//得分
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getAgentId() {
		return agentId;
	}
	public void setAgentId(Integer agentId) {
		this.agentId = agentId;
	}
	public Integer getFinishTime() {
		return finishTime;
	}
	public void setFinishTime(Integer finishTime) {
		this.finishTime = finishTime;
	}
	public Integer getAvgAllTime() {
		return avgAllTime;
	}
	public void setAvgAllTime(Integer avgAllTime) {
		this.avgAllTime = avgAllTime;
	}
	public double getFinishAvg() {
		return finishAvg;
	}
	public void setFinishAvg(double finishAvg) {
		this.finishAvg = finishAvg;
	}
	public Integer getLetterSix() {
		return letterSix;
	}
	public void setLetterSix(Integer letterSix) {
		this.letterSix = letterSix;
	}
	public Integer getBiggerSix() {
		return biggerSix;
	}
	public void setBiggerSix(Integer biggerSix) {
		this.biggerSix = biggerSix;
	}
	public Integer getBiggerTen() {
		return biggerTen;
	}
	public void setBiggerTen(Integer biggerTen) {
		this.biggerTen = biggerTen;
	}
	public Integer getFinishSum() {
		return finishSum;
	}
	public void setFinishSum(Integer finishSum) {
		this.finishSum = finishSum;
	}
	public Integer getAllOrderSum() {
		return allOrderSum;
	}
	public void setAllOrderSum(Integer allOrderSum) {
		this.allOrderSum = allOrderSum;
	}
	public String getStaticDate() {
		return staticDate;
	}
	public void setStaticDate(String staticDate) {
		this.staticDate = staticDate;
	}
	public Integer getRanking() {
		return ranking;
	}
	public void setRanking(Integer ranking) {
		this.ranking = ranking;
	}
	public double getGrade() {
		return grade;
	}
	public void setGrade(double grade) {
		this.grade = grade;
	}
	@Override
	public String toString() {
		return "TrainOrderOfflineStatistics [id=" + id + ", agentId=" + agentId + ", finishTime=" + finishTime
				+ ", avgAllTime=" + avgAllTime + ", finishAvg=" + finishAvg + ", letterSix=" + letterSix
				+ ", biggerSix=" + biggerSix + ", biggerTen=" + biggerTen + ", finishSum=" + finishSum
				+ ", allOrderSum=" + allOrderSum + ", staticDate=" + staticDate + ", ranking=" + ranking + ", grade="
				+ grade + "]";
	}
}
