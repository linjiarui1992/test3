/**
 * 版权所有;//空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.domain;

import javax.mail.search.StringTerm;

/**
 * @className: com.ccservice.offline.domain.TrainOfflineExpRec
 * @description: TODO - 
 * @author: 郑州-技术-杨威   E-mail:653395455@qq.com
 * @createTime: 2017年9月5日 上午9:53:28 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOfflineExpRec {
    private int id;//
    private int orderid;//
    private String newExpnum;//
    private String historyExpnum;//
    private String updateTime;//
    private String expressAgent;//
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getOrderid() {
		return orderid;
	}
	public void setOrderid(int orderid) {
		this.orderid = orderid;
	}
	public String getNewExpnum() {
		return newExpnum;
	}
	public void setNewExpnum(String newExpnum) {
		this.newExpnum = newExpnum;
	}
	public String getHistoryExpnum() {
		return historyExpnum;
	}
	public void setHistoryExpnum(String historyExpnum) {
		this.historyExpnum = historyExpnum;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public String getExpressAgent() {
		return expressAgent;
	}
	public void setExpressAgent(String expressAgent) {
		this.expressAgent = expressAgent;
	}
	@Override
	public String toString() {
		return "TrainOfflineExpRec [id=" + id + ", orderid=" + orderid + ", newExpnum=" + newExpnum + ", historyExpnum="
				+ historyExpnum + ", updateTime=" + updateTime + ", expressAgent=" + expressAgent + "]";
	}
}
