/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;

import com.ccservice.Util.PropertyUtil;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.b2b2c.atom.component.util.CCSHttpClient;

/**
 * @className: com.ccservice.tuniu.util.HttpPostJsonUtil
 * @description: TODO - 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月18日 下午2:15:34 
 * @version: v 1.0
 * @since 
 *
 */
public class HttpUtil {
    public String doGetUTF8(String url, String reqGetParam) throws Exception {
        GetMethod getMethod = new GetMethod(url+reqGetParam);  
        //（1）、这里可以设置自己想要的编码格式
        getMethod.getParams().setContentCharset("UTF-8");
        
        //（2）、对于get方法也可以这样设置 
        /*getMethod.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET,"UTF-8");
        
        //（3）、还可以如下这样设置
        getMethod.addRequestHeader("Content-Type", "text/html; charset=UTF-8");*/
       
        //（4）、当然同样可以直接设置 httpClient 对象的编码格式
        /*HttpClient httpClient = new HttpClient();
        httpClient.getParams().setContentCharset("GB2312");

        //使用流的方式读取也可以如下设置
        InputStream in = getMethod.getResponseBodyAsStream();  
        //这里的编码规则要与上面的相对应  
        BufferedReader br = new BufferedReader(new InputStreamReader(in,"GB2312"));*/

        CCSHttpClient http = new CCSHttpClient(false, 10000L);

        BufferedReader reader = null;
        StringBuffer stringBuffer = new StringBuffer();
        
        http.executeMethod(getMethod);
        reader = new BufferedReader(new InputStreamReader(getMethod.getResponseBodyAsStream()));
        String str = "";
        while ((str = reader.readLine()) != null) {
            stringBuffer.append(str);
        }
        String ts = stringBuffer.toString();
        return ts;
    }
    
    public String doGet(String url, String reqGetParam) throws Exception {
        HttpGet get = setGetRequestHeader(url+reqGetParam);
        get.setHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
        
        //设置请求的报文头部的编码
        /*get.setHeader(new BasicHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf-8"));

        //设置期望服务端返回的编码
        get.setHeader(new BasicHeader("Accept", "text/plain;charset=utf-8"));*/
        
        CloseableHttpClient rrr = HttpClients.createDefault();//通过对象提交请求

        //rrr.getParams().setParameter("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
        
        CloseableHttpResponse res = rrr.execute(get);
        
        /*Header[] allHeaders = res.getAllHeaders();

        for (Header header : allHeaders) {
            //System.out.println(header.getName() + ":" + header.getValue());
            if ("Set-Cookie".equals(header.getName())) {
                String cookieTemp = header.getValue();
                if (cookieTemp.contains("bus365_SESSION")) {
                    cookie = cookieTemp;
                }
            }
        }*/

        return EntityUtils.toString(res.getEntity());
    }

    public String doPost(String url, String reqPostParam) throws Exception {
        HttpPost post = setPostRequestHeader(url);
        //post.setHeader("Content-Type", "application/x-www-form-urlencoded");
        post.setHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
        //post.setHeader("ContentEncoding", "UTF-8");
        /*s.setContentEncoding("UTF-8");
        s.setContentType("application/json");*///发送json数据需要设置contentType
        StringEntity s = new StringEntity(reqPostParam, "UTF-8");
        post.setEntity(s);

        CloseableHttpClient rrr = HttpClients.createDefault();//通过对象提交请求
        CloseableHttpResponse res = rrr.execute(post);
        
        return EntityUtils.toString(res.getEntity());
    }

    // get方式设置 请求头
    public HttpGet setGetRequestHeader(String url) {
        HttpGet get = new HttpGet(url);
        get.addHeader("User-Agent",
                "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.96 Safari/537.36");
        get.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        get.addHeader("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3");
        get.addHeader("Accept-Encoding", "gzip, deflate");
        get.addHeader("Connection", "keep-alive");
        get.addHeader("Upgrade-Insecure-Requests", "1");
        return get;
    }

    // 珠海post方式设置请求头，主要是为了表单的提交
    public HttpPost setPostRequestHeader(String url) {
        String temp = "";
        HttpPost post = new HttpPost(url);
        post.addHeader("User-Agent",
                "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.96 Safari/537.36");
        post.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        post.addHeader("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3");
        post.addHeader("Accept-Encoding", "gzip, deflate");
        post.addHeader("Connection", "keep-alive");
        post.addHeader("Upgrade-Insecure-Requests", "1");
        return post;
    }

}
