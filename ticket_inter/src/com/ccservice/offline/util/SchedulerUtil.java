/**
 * 版权所有, 空铁无忧
 * Author: 火车票 H5-微信端 项目开发组
 * copyright: 2017
 */
package com.ccservice.offline.util;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

import com.ccservice.tuniu.service.TrainTuNiuOfflineCancelLockTicket;

/**
 * @className: com.ccservice.yunku.app.train.order.web.servlet.timer.SchedulerUtil
 * @description: TODO - 
 * 
 * 单例模式下的 调度器，可以删除job，但是永远不能将调度器停止！！！
 * 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年3月28日 下午2:44:37 
 * @version: v 1.0
 * @since 
 *
 */
public class SchedulerUtil {
    private static Scheduler scheduler = getSchedulerTemp();

    private static Scheduler getSchedulerTemp() {
        Scheduler scheduler = null;
        try {
            scheduler = StdSchedulerFactory.getDefaultScheduler();
        }
        catch (SchedulerException e) {
            e.printStackTrace();
        }
        return scheduler;
    }

    /**
     * @description: TODO - 保证调度器本身的单例
     * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
     * @createTime: 2017年3月28日 下午2:46:13
     * @return
     * @throws SchedulerException
     */
    public static Scheduler getScheduler() {
        return scheduler;
    }

    //TuNiuOfflineOvertimeCancle - TrainTuNiuOfflineCancelLockTicket.class
    //TuNiuOfflineLockOvertimeCallback
    public static void startLockScheduler(Long OrderId, String cronExpression, String flagStr, Class clazz) throws Exception {
        /*scheduler.scheduleJob(new JobDetail("TuNiuOfflineOvertimeCancleJob"+OrderId, "TuNiuOfflineOvertimeCancleJobGroup"+OrderId, 
                TrainTuNiuOfflineCancelLockTicket.class), new CronTrigger("TuNiuOfflineOvertimeCancleCronTrigger"+OrderId, 
                        "TuNiuOfflineOvertimeCancleCronTriggerGroup"+OrderId, cronExpression));*/
        
        JobDetail jobDetail = new JobDetail(flagStr+"Job"+OrderId, flagStr+"JobGroup", clazz);
        jobDetail.getJobDataMap().put("OrderId", OrderId);
        scheduler.scheduleJob(jobDetail, new CronTrigger(flagStr+"CronTrigger"+OrderId, flagStr+"CronTriggerGroup", cronExpression));
        
        if (!scheduler.isStarted()) {
            scheduler.start();
        }
    }

    public static void startLockScheduler(Long OrderId, Integer useridi, String LockWait, String cronExpression, String flagStr, Class clazz) throws Exception {
        /*scheduler.scheduleJob(new JobDetail("TuNiuOfflineOvertimeCancleJob"+OrderId, "TuNiuOfflineOvertimeCancleJobGroup"+OrderId, 
                TrainTuNiuOfflineCancelLockTicket.class), new CronTrigger("TuNiuOfflineOvertimeCancleCronTrigger"+OrderId, 
                        "TuNiuOfflineOvertimeCancleCronTriggerGroup"+OrderId, cronExpression));*/
        
        JobDetail jobDetail = new JobDetail(flagStr+"Job"+OrderId, flagStr+"JobGroup", clazz);
        jobDetail.getJobDataMap().put("OrderId", OrderId);
        jobDetail.getJobDataMap().put("useridi", useridi);
        jobDetail.getJobDataMap().put("LockWait", LockWait);
        scheduler.scheduleJob(jobDetail, new CronTrigger(flagStr+"CronTrigger"+OrderId, flagStr+"CronTriggerGroup", cronExpression));
        
        if (!scheduler.isStarted()) {
            scheduler.start();
        }
    }

    /*public static void startNewLockScheduler(Long OrderId, Integer useridi, String LockWait, String cronExpression, String flagStr, Class clazz) throws Exception {
        scheduler.scheduleJob(new JobDetail("TuNiuOfflineOvertimeCancleJob"+OrderId, "TuNiuOfflineOvertimeCancleJobGroup"+OrderId, 
                TrainTuNiuOfflineCancelLockTicket.class), new CronTrigger("TuNiuOfflineOvertimeCancleCronTrigger"+OrderId, 
                        "TuNiuOfflineOvertimeCancleCronTriggerGroup"+OrderId, cronExpression));
        
        JobDetail jobDetail = new JobDetail(flagStr+"Job"+System.currentTimeMillis()+OrderId, flagStr+"JobGroup"+System.currentTimeMillis(), clazz);
        jobDetail.getJobDataMap().put("OrderId", OrderId);
        jobDetail.getJobDataMap().put("useridi", useridi);
        jobDetail.getJobDataMap().put("LockWait", LockWait);
        scheduler.scheduleJob(jobDetail, new CronTrigger(flagStr+"CronTrigger"+System.currentTimeMillis()+OrderId, flagStr+"CronTriggerGroup"+System.currentTimeMillis(), cronExpression));
        
        if (!scheduler.isStarted()) {
            scheduler.start();
        }
    }*/

    //TuNiuOfflineOvertimeCancle
    public static void cancelCancelLockTicketScheduler(Long OrderId, String flagStr) throws Exception {
        if (scheduler.isStarted()) {
            scheduler.pauseTrigger("CronTrigger"+OrderId, flagStr+"CronTriggerGroup");//停止触发器  
            scheduler.unscheduleJob(flagStr+"CronTrigger"+OrderId, flagStr+"CronTriggerGroup");//移除触发器  
            scheduler.deleteJob(flagStr+"Job"+OrderId, flagStr+"JobGroup");//删除任务 
        }
    }
    
}
