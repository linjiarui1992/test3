/**
 * 版权所有, 空铁无忧
 * Author: 火车票 H5-微信端 项目开发组
 * copyright: 2017
 */
package com.ccservice.offline.util;

import org.quartz.JobDetail;

/**
 * @className: com.ccservice.yunku.app.train.order.web.servlet.timer.SchedulerUtil
 * @description: TODO - 
 * 
 * 单例模式下的 调度器，可以删除job，但是永远不能将调度器停止！！！
 * 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年3月28日 下午2:44:37 
 * @version: v 1.0
 * @since 
 *
 */
public class SchedulerUtilTest {
    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        Long OrderId = 1L;
        String cronExpression1 = "10 43 16 09 11 ? 2017";
        String cronExpression2 = "20 43 16 09 11 ? 2017";
        String cronExpression3 = "30 43 16 09 11 ? 2017";
        
        String flagStr = "SchedulerUtilTestJob";
        //Unable to store Job with name: 'SchedulerUtilTestJobJob1' and group: 'SchedulerUtilTestJobJobGroup', because one already exists with this identification.
        SchedulerUtil.startLockScheduler(OrderId, 46, 8+"", cronExpression1, flagStr, SchedulerUtilTestJob.class);
        
        JobDetail jobDetail = SchedulerUtil.getScheduler().getJobDetail(flagStr+"Job"+OrderId, flagStr+"JobGroup");
        if (jobDetail!=null) {
            SchedulerUtil.cancelCancelLockTicketScheduler(OrderId, flagStr);
        }
        
        SchedulerUtil.startLockScheduler(OrderId, 46, 8+"", cronExpression2, flagStr, SchedulerUtilTestJob.class);
        
        /*SchedulerUtil.cancelCancelLockTicketScheduler(OrderId, "SchedulerUtilTestJob");

        SchedulerUtil.startLockScheduler(OrderId, 46, 8+"", cronExpression1, "SchedulerUtilTestJob", SchedulerUtilTestJob.class);
        SchedulerUtil.cancelCancelLockTicketScheduler(OrderId, "SchedulerUtilTestJob");*/
        

        /*SchedulerUtil.cancelCancelLockTicketScheduler(OrderId);

        SchedulerUtil.startLockScheduler(OrderId, cronExpression1);

        Thread.sleep(5*1000);

        OrderId = 3L;
        
        SchedulerUtil.startLockScheduler(OrderId, cronExpression2);

        OrderId = 1L;
        
        SchedulerUtil.startLockScheduler(OrderId);
        
        OrderId = 2L;
        
        Thread.sleep(5*1000);

        SchedulerUtil.startLockScheduler(OrderId, cronExpression3);*/

        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间： " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间： " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间： " + runTimeS / 60 + "min：" + runTimeS % 60 + "s");//分
    }
}
