package com.ccservice.offline.util;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;

import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataRow;
import com.ccervice.util.db.DataTable;
import com.ccservice.b2b2c.atom.component.WriteLog;

public class DistributionUtil {

    /**
     * 新版分单逻辑
     * @author zpy 2017-10-24 14:33
     * @param address1 邮寄地址
     * @param departTime 发车时间
     * @param type 1、拉单业务 2、夜间单轮训业务
     * @return
     */
    public String distribution(long r1, String address1, String departTime, int type) {
        String logName = "Log新版分单逻辑";
        String agentId = "1"; //默认分给订单未分配
        try {
            WriteLog.write(logName,
                    "r1" + r1 + ",address1=" + address1 + ";departTime=" + departTime + ";type=" + type);
            //取出默认出票点
            /**
             * 老逻辑中是默认分给378,由于新逻辑中有繁忙度筛选,所以很多订单会分给此代售点,故修改为1,订单未分配
             *              
            String sql1 = "SELECT agentId FROM TrainOfflineMatchAgent with(nolock) WHERE status=2 AND createUid="
                    + "56";
            //        List list1 = DBHelpUtil.getSystemServiceOldDB().findMapResultBySql(sql1, null);
            DataTable dataTable = new DataTable();
            dataTable = DBHelperOffline.GetDataTable(sql1, null);
            if (dataTable.GetRow().size() > 0) {
                //            Map map = (Map) dataTable.GetRow().get(0);
                DataRow map = dataTable.GetRow().get(0);
                agentId = map.GetColumnString("agentId");
            }
            */

            //程序自动分配出票点
            String sql2 = "SELECT a.agentId,provinces FROM TrainOfflineMatchAgent a with(nolock)"
                    + "join T_AGENTISONLINETIME b with(nolock) on b.agentid=a.agentid WHERE status=1 AND createUid=56 and b.isOnline = 1 and isOnlineDate=Convert(varchar(10),Getdate(),23)";
            //        List list2 = DBHelpUtil.getSystemServiceOldDB().findMapResultBySql(sql2, null);
            DataTable list2 = new DataTable();
            try {
                list2 = DBHelperOffline.GetDataTable(sql2, null);
            }
            catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            List<String> listAgents = new ArrayList<String>();//可用代售点
            String matchProvince = "";//匹配城市名称

            //确认到省份
            for (int i = 0; i < list2.GetRow().size(); i++) {
                //            Map mapp = (Map) list2.get(i);
                DataRow mapp = list2.GetRow().get(i);
                String provinces = mapp.GetColumnString("provinces");
                String agentid = mapp.GetColumnString("agentId").toString();
                String[] add = provinces.split(",");
                for (int j = 0; j < add.length; j++) {
                    boolean flag = false;
                    if (address1.startsWith(add[j])) {
                        listAgents.add(agentid);
                        if (matchProvince.length() == 0) {//拿到邮寄目的地城市
                            provinces = add[j];
                        }
                        flag = true;
                    }
                    if (flag) {
                        break;
                    }
                }
            }

            //        System.out.println("拿到目的地相关城市--》" + listAgents.toString());
            WriteLog.write(logName, "r1" + r1 + ",拿到目的地相关城市--》" + listAgents.toString());
            //判断为0的话筛选周边城市
            if (listAgents.size() == 0) {
                //进入筛选附近城市
                agentId = distributionProvinces(address1, departTime, logName, r1, agentId, list2, listAgents, type);
            }
            else {
                //根据繁忙度筛选代售点
                List<String> weightAgentIds = getWeightAgents(logName, r1, listAgents);
                if (weightAgentIds.size() > 0) {
                    if (weightAgentIds.size() == 1) {//存在1个直接返回
                        agentId = weightAgentIds.get(0).toString();
                    }
                    else {
                        String agentIdsSplit = StringUtils.join(weightAgentIds.toArray(), ",");
                        //进入分单计算
                        agentId = probCalculate(logName, r1, agentIdsSplit);
                    }
                }
                else {
                    //匹配邻省代售点
                    agentId = distributionProvinces(address1, departTime, logName, r1, agentId, list2, listAgents,
                            type);
                }

            }
        }
        catch (Exception e) {
            WriteLog.write(logName,
                    "r1" + r1 + "分单逻辑执行异常,address1=" + address1 + ";departTime=" + departTime + ";type=" + type);
            try {
                return TrainOrderOfflineUtil.distribution(address1, null);
            }
            catch (Exception e1) {
                WriteLog.write(logName, "r1" + r1 + "分单逻辑异常做老逻辑兼容又执行异常,address1=" + address1 + ";departTime="
                        + departTime + ";type=" + type);
                e1.printStackTrace();
            }
        }

        WriteLog.write(logName, "r1" + r1 + ",agentId=" + agentId + ";address1=" + address1);

        return agentId;
    }

    /**
     * 匹配邻省代售
     * @param address1
     * @param departTime
     * @param logName
     * @param r1
     * @param agentId
     * @param list2
     * @param listAgents
     * @param type 
     * @return
     */
    private String distributionProvinces(String address1, String departTime, String logName, long r1, String agentId,
            DataTable list2, List listAgents, int type) {
        //        System.out.println("====>进进入邻省匹配");
        WriteLog.write(logName, "r1" + r1 + "," + "进入邻省匹配");
        String departureTime = departTime;//是   出发日期 格式：”2017-08-24 10:20”

        List agingArriveLi = new ArrayList<>();//时效可到的代售点

        String prefixAddress = address1.substring(0, 2);//匹配前两个字到库中取值
        //调用sql 拿到周边城市数组
        String sql3 = "select nearProvinces from TrainOfflineMatchProvinces  with(nolock) where province like '"
                + prefixAddress + "%'";
        //        List list3 = DBHelpUtil.getSystemServiceOldDB().findMapResultBySql(sql3, null);
        DataTable list3 = new DataTable();
        try {
            list3 = DBHelperOffline.GetDataTable(sql3, null);
        }
        catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        if (list3.GetRow().size() > 0) {
            //            Map nearProvincesMap = (Map) list3.get(0);
            DataRow nearProvincesMap = list3.GetRow().get(0);
            //              String nearProvinces = nearProvincesMap.get("nearProvinces").toString(); 
            String[] nearProvinces = nearProvincesMap.GetColumnString("nearProvinces").split(",");

            //数组拆分，批量查出该城市哪个代售点存在?
            List<List> usableProvincesLi = new ArrayList<List>();//可用省份列表
            for (int k = 0; k < nearProvinces.length; k++) {
                String nearProvince = nearProvinces[k];
                List<String> nowList = new ArrayList<String>();
                for (int i = 0; i < list2.GetRow().size(); i++) {
                    //                    Map mapp = (Map) list2.get(i);
                    DataRow mapp = list2.GetRow().get(i);
                    String provinces = mapp.GetColumnString("provinces");
                    String agentid = mapp.GetColumnString("agentId");
                    String[] add = provinces.split(",");
                    for (int j = 0; j < add.length; j++) {
                        boolean flag = false;
                        if (nearProvince.startsWith(add[j])) {
                            nowList.add(agentid);
                            flag = true;
                        }
                        if (flag) {
                            break;
                        }
                    }
                }
                if (nowList.size() > 0) {
                    usableProvincesLi.add(nowList);
                }
            }

            //判断附近省份是否有可使用代售点
            List<String> optimizeAgent = new ArrayList<>();//优化效率,已经判断过时效无法到达到的代售点跳过
            if (usableProvincesLi.size() > 0) {
                for (int i = 0; i < usableProvincesLi.size(); i++) {
                    List agentIdListForProvinces = usableProvincesLi.get(i);
                    for (int j = 0; j < agentIdListForProvinces.size(); j++) {
                        String nearAgentId = agentIdListForProvinces.get(j).toString();
                        String arriveTime = DelieveUtils.getDelieveStr2(nearAgentId, address1);

                        //已经判断过快递点不到达的代售点直接跳过,减少操作,因为有的代售点会分配多个省份
                        if (optimizeAgent.contains(nearAgentId)) {
                            continue;
                        }
                        //arriveTime = "0";//测试
                        if (arriveTime.equals("0") || arriveTime.equals("1") || arriveTime.equals("2")) {//不拦截
                            //匹配新的快递时效的机制
                            arriveTime = DelieveUtils.ungetDelieveTime(address1, nearAgentId, logName, r1);
                        }
                        else {
                            arriveTime = arriveTime.substring(0, arriveTime.indexOf(","));//2017-09-07 18:00:00
                        }
                        Boolean canDelivery = false;
                        if (arriveTime != null && !"".equals(arriveTime)) {
                            try {
                                DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                                Date arriveTimeDf = df.parse(arriveTime);
                                Date departureTimeDf = df.parse(departureTime);

                                long interval = (departureTimeDf.getTime() - arriveTimeDf.getTime()) / (1000 * 60);
                                if (interval > (2 * 60)) {//判断出发时间和送达时间是否相差2小时。大于2小时，可以配送，小于2小时无法配送。
                                    canDelivery = true;
                                }
                            }
                            catch (ParseException e) {
                                WriteLog.write(logName, r1 + "新分单逻辑筛选附近代售点-时间类型转换失败");
                            }
                        }
                        if (canDelivery) {//保留可用代理点
                            agingArriveLi.add(nearAgentId);
                        }
                        else {//已经出现过的时效紧张代售点，做记录，下次下次循环直接跳过
                            optimizeAgent.add(nearAgentId);
                        }
                    }
                    //                    System.out.println("==>邻省代售点：" + agentIdListForProvinces.toString());
                    WriteLog.write(logName, "r1" + r1 + "," + "==>邻省参数：" + agentIdListForProvinces.toString());
                    //判断只要附近省份找到一个可用代售点，即结束筛选开始分单
                    if (agingArriveLi.size() > 0) {
                        break;
                    }
                }

                if (agingArriveLi.size() >= 1) {
                    List<String> weightAgentIds = getWeightAgents(logName, r1, agingArriveLi);
                    //筛选可用代售点权重
                    if (weightAgentIds.size() > 0) {
                        WriteLog.write(logName, "r1" + r1 + "," + "邻省代售点数量=" + weightAgentIds.size() + ",代售点id："
                                + weightAgentIds.toString());
                        if (weightAgentIds.size() == 1) {//存在1个直接返回
                            agentId = weightAgentIds.get(0).toString();
                        }
                        else {
                            String agentIdsSplit = StringUtils.join(weightAgentIds.toArray(), ",");
                            //权重筛选后的多个可用id，走分单概率分配
                            agentId = probCalculate(logName, r1, agentIdsSplit);
                        }
                    }
                    else {
                        //夜间单临时匹配代售点时没有匹配到则存到夜间单虚拟代售点中
                        agentId = distributionDefaultRecordAgent(agentId, type);
                    }
                }
                else {//夜间单临时匹配代售点时没有匹配到则存到夜间单虚拟代售点中
                    agentId = distributionDefaultRecordAgent(agentId, type);
                }
            }
            else {
                agentId = distributionDefaultRecordAgent(agentId, type);
            }
        }
        return agentId;
    }

    /**
     * 分配默认收录代理（夜间单逻辑使用）
     * @param agentId
     * @param type
     * @return
     */
    private String distributionDefaultRecordAgent(String agentId, int type) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int minute = cal.get(Calendar.MINUTE);
        try {
            if ((hour >= 23 || hour < 6) || (hour == 6 || (hour == 7 && minute < 10) && type == 2)) {//23-6点为夜间单,默认分配给收录代理,6-7点10分轮训夜间单账户时,若未分出去,默认回到收录账户
                //status = 3 为夜间单代售点
                String sql1 = "SELECT agentId FROM TrainOfflineMatchAgent with(nolock) WHERE status=3 AND createUid="
                        + "56";
                //            List list1 = DBHelpUtil.getSystemServiceOldDB().findMapResultBySql(sql1, null);
                DataTable list1 = DBHelperOffline.GetDataTable(sql1, null);
                if (list1.GetRow().size() > 0) {
                    //                Map map = (Map) list1.get(0);
                    DataRow map = list1.GetRow().get(0);
                    agentId = map.GetColumnString("agentId");
                }
            }
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return agentId;
    }

    /**
     * 根据权重筛选代售点
     * @param agentIdLi
     * @return list.size()>0 说明根绝权重筛选出代售点无需继续走分单计算，否则走分单计算
     */
    public List<String> getWeightAgents(String logName, long r1, List<String> agentIdLi) {
        List<String> li = new ArrayList<String>();
        //        System.out.println("r1," + "进入权重判断,agentids=" + agentIdLi.size());
        WriteLog.write(logName, "r1" + r1 + "," + "进入权重判断,agentids=" + agentIdLi.toString());
        //拿到昨天的日期
        SimpleDateFormat localTime = new SimpleDateFormat("yyyy-MM-dd");
        String date = localTime.format(new Date());

        String agentIds = StringUtils.join(agentIdLi.toArray(), ",");

        //筛选不同代售点的繁忙值
        String sql = "select agentid,count(id) as ordernum from TrainOrderOffline  with(nolock) "
                + "where CreateTime between '" + date + " 00:00:00' and '" + date + " 23:59:59' " + "and AgentId in ("
                + agentIds + ") and OrderStatus=1 group by AgentId";
        //        List list = DBHelpUtil.getSystemServiceOldDB().findMapResultBySql(sql, null);
        DataTable list = new DataTable();
        try {
            list = DBHelperOffline.GetDataTable(sql, null);
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Boolean isAllLtSisty = true;//判断是否小于60,走分单计算
        Boolean isAllBusy = true; //判断繁忙度都为100，走分单计算
        Map<String, Integer> agentAndOrderNumMap = new HashMap<String, Integer>();
        List<Integer> sortNum = new ArrayList<Integer>();
        boolean isExist = false;
        if (list.GetRow().size() > 0) {
            for (int j = 0; j < agentIdLi.size(); j++) {
                String compareAgentId = agentIdLi.get(j);
                for (int i = 0; i < list.GetRow().size(); i++) {
                    //                    Map<String, Integer> map = (Map<String, Integer>) list.get(i);
                    DataRow map = list.GetRow().get(i);
                    String agentId = map.GetColumnString("agentid");
                    if (compareAgentId.equals(agentId)) {
                        int orderNum = map.GetColumnInt("ordernum");
                        WriteLog.write(logName, "r1" + r1 + "," + "存在代售点：" + agentId + ",当前时刻拥有订单：" + orderNum);
                        sortNum.add(orderNum);
                        if (orderNum > 3) {
                            isAllLtSisty = false;
                        }
                        if (orderNum < 5) {
                            isAllBusy = false;
                        }
                        agentAndOrderNumMap.put(agentId, orderNum);
                        isExist = true;
                    }
                }
                if (!isExist) {
                    sortNum.add(0);
                    agentAndOrderNumMap.put(compareAgentId, 0);
                }
            }
        }
        else {
            //没有订单继续执行 
            isAllBusy = false;
        }

        /*
         * 0单为0，1单为20，2单为40，3单为60，4单为80，5单为100，繁忙度为100后，不考虑分单。
         * 繁忙度100：5单以上，返回空，走邻省，假如已经邻省则走默认代售点
         * list.size查出的当时的繁忙度的假如跟agentIdLi.size传入的相等,说明有存在繁忙度为0的代售点
         */
        if (!isAllBusy || agentIdLi.size() != list.GetRow().size()) {
            if (isAllLtSisty) {//繁忙度60，3单以下，走分单概率分配
                WriteLog.write(logName, "r1" + r1 + "," + "繁忙度都在60以下，3单以下，走分单概率分配");
                li = agentIdLi;
            }
            else {
                Collections.sort(sortNum);
                Set<String> kset = agentAndOrderNumMap.keySet();
                Integer minOrderNum = sortNum.get(0); //繁忙度最小值，
                for (String ks : kset) {
                    //取相等情况的agentid
                    if (minOrderNum.equals(agentAndOrderNumMap.get(ks))) {
                        li.add(ks);
                    }
                }
                WriteLog.write(logName, "r1" + r1 + "," + "繁忙度比较：最小值（繁忙度最低）的代售点：" + li.toString());
            }
        }

        return li;
    }

    //    public static void main(String[] args) {
    //
    //        if (true) {
    //            //                        Set agentGradeSet = new HashSet<>();
    //            //                        agentGradeSet.add(4);
    //            //                        //            agentGradeSet.add(6);
    //            //                        Iterator i = agentGradeSet.iterator();
    //            //                        System.out.println(i.next());
    //
    //            List s = new ArrayList<>();
    //            s.add(1);
    //            s.add(2);
    //            s.add(3);
    //            System.out.println(s.toString());
    //            return;
    //
    //        }
    //
    //        long startTime = System.nanoTime();
    //        DistributionUtil tdut = new DistributionUtil();
    //        //        System.out.println(tdut.distribution(2135469879, "广东省-佛山市-禅城区-南庄镇 吉利工业园创鑫光电有限公司", "2017-11-28 09:54:00", 1));
    //        Map<String, Double> map = tdut.getAgentEfficiency("测试Log", 1232135243, "437,436");
    //        Set set = map.keySet();
    //        Iterator it = set.iterator();
    //        while (it.hasNext()) {
    //            String str = it.next().toString();
    //            System.out.println("index=" + str + ",id=" + map.get(str));
    //
    //        }
    //        long endTime = System.nanoTime();//获取结束时间
    //        long runTimeNS = endTime - startTime;
    //        System.out.println("程序运行时间： " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
    //        int runTimeS = (int) (runTimeNS / 1000000000);
    //        System.out.println("程序运行时间： " + runTimeS + "s");//秒 - 
    //        System.out.println("程序运行时间： " + (runTimeNS / 1000000) + "毫秒");//秒 - 
    //        System.out.println("程序运行时间： " + runTimeS / 60 + "min：" + runTimeS % 60 + "s");//分
    //    }

    /**
     * 
     * @param agentIds
     * @return
     */
    public Map<String, Double> getAgentEfficiency(String logName, long r1, String agentIds) {
        //        System.out.println("=========进入代售点排名概率动态拿到==========");
        WriteLog.write(logName, "r1" + r1 + "," + "进入动态计算分单得分,agentids=" + agentIds);
        Map<String, Double> agentAndGradeMap = new HashMap<String, Double>();

        SimpleDateFormat localTime = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calLastDay = Calendar.getInstance();//昨天日期
        Calendar calEightDay = Calendar.getInstance();//八点前的日期

        Date d = new Date();
        calLastDay.setTime(d);
        calEightDay.setTime(d);
        calLastDay.set(Calendar.DAY_OF_MONTH, calLastDay.get(Calendar.DAY_OF_MONTH));//让日期-1  修改为当天起算
        calEightDay.set(Calendar.DAY_OF_MONTH, calEightDay.get(Calendar.DAY_OF_MONTH) - 7);//让日期-8  对应之前向后推一天
        String dateLastDay = localTime.format(calLastDay.getTime());
        String dateLastEightDay = localTime.format(calEightDay.getTime());

        //储存代售点array
        String[] agentArrays = agentIds.split(",");

        String sql = "select agentid,successtimetotal,successordertotal,allordertotal,questionordertotal from TrainOfflineAgentEfficiency "
                + "WHERE  countdate between '" + dateLastEightDay + "' and '" + dateLastDay
                + "' order by countdate desc";
        //        List list = DBHelpUtil.getSystemServiceOldDB().findMapResultBySql(sql, null);
        DataTable list = new DataTable();
        try {
            list = DBHelperOffline.GetDataTable(sql, null);
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        int agentId = 0;
        int successTimeTotal = 0; //出票总时长
        int threeDaySuccessOrderTotal = 0; //成功总订单（三天），为配合不大于三天的成功率与出票时长
        int threeDayAllOrderTotal = 0; //所有订单数(成功+失败)，也是为配合不大于三天的成功率与出票时长
        int successOrderTotal = 0; //成功总订单
        int allOrderTotal = 0; //所有订单数
        int questionOrderTotal = 0; //问题订单总数
        int saveTime = 0; //存入次数

        double successRate = 0d;//成功率
        double chupiaoTimeAvg = 0d;//出票成功时长（平均）
        double questionAvg = 0d;//问题订单率

        double score = 0;//代售点得分

        List<String> noPropAgent = new ArrayList<>();//保存七天内没有概率统计的代售点
        boolean isAppear = false;//判断传入的代售点近七天是否有数据参与统计

        //遍历所有代售点
        for (int j = 0; j < agentArrays.length; j++) {

            agentId = Integer.parseInt(agentArrays[j]);

            //一整次循环算出一个代售点的得分
            for (int i = 0; i < list.GetRow().size(); i++) {
                //                                tempMap = (Map) list.get(i);
                DataRow tempMap = list.GetRow().get(i);
                int agentIdTemp = Integer.parseInt(tempMap.GetColumnString("agentid").toString());
                if (agentId == agentIdTemp) {
                    isAppear = true;
                    int successTimeTotalTemp = Integer.parseInt(tempMap.GetColumnString("successtimetotal").toString());
                    int successOrderTotalTemp = Integer
                            .parseInt(tempMap.GetColumnString("successordertotal").toString());
                    int allOrderTotalTemp = Integer.parseInt(tempMap.GetColumnString("allordertotal").toString());
                    int questionOrderTotalTemp = Integer
                            .parseInt(tempMap.GetColumnString("questionordertotal").toString());
                    //成功率和出票时长最多统计三天的
                    if (saveTime < 3) {
                        successTimeTotal += successTimeTotalTemp;
                        threeDaySuccessOrderTotal += successOrderTotalTemp;
                        threeDayAllOrderTotal += allOrderTotalTemp;
                        saveTime++;
                    }
                    allOrderTotal += allOrderTotalTemp;
                    questionOrderTotal += questionOrderTotalTemp;
                    successOrderTotal += successOrderTotalTemp;
                }
            }
            if (isAppear) {
                DecimalFormat df = new DecimalFormat("0.00");
                if (successTimeTotal > 0) {
                    successRate = (double) threeDaySuccessOrderTotal / threeDayAllOrderTotal;//成功率计算
                    chupiaoTimeAvg = getTimeSwitchScore(successTimeTotal / threeDaySuccessOrderTotal);//出票时长计算
                }
                if (questionOrderTotal > 0) {
                    questionAvg = 1 - ((double) questionOrderTotal / successOrderTotal);//问题订单率计算
                }
                score = (successRate * 0.5) + (chupiaoTimeAvg * 0.25) + (questionAvg * 0.25);//最终得分计算
                if (score > 0) {
                    agentAndGradeMap.put(agentId + "", score);
                }
                //                System.out.print("-代售点：" + agentId + ":近三天进单总数：" + threeDayAllOrderTotal + ";近三天进单成功数："
                //                        + threeDaySuccessOrderTotal + ";近三天总时长：" + successOrderTotal + ";综合计算：（成功率" + successRate
                //                        + "x0.5）" + (successRate * 0.5) + "+(出票平均时长" + chupiaoTimeAvg + "x0.25)"
                //                        + (chupiaoTimeAvg * 0.25) + "问题订单率" + questionAvg + "x0.25)" + (questionAvg * 0.25) + "=="
                //                        + score + "////");
                successTimeTotal = 0;
                threeDaySuccessOrderTotal = 0;
                threeDayAllOrderTotal = 0;
                allOrderTotal = 0;
                questionOrderTotal = 0;
                saveTime = 0;
                isAppear = false;
            }
            else {
                noPropAgent.add(String.valueOf(agentId));
            }
        }
        /**
         * 如果出现近七天都没有订单数据，则默认分给该代售点，保证下个订单参与概率计算
         * 如果出现多个代售点都没的情况则，多个没有效率的代售点随机分配
         */
        if (noPropAgent.size() > 0) {
            List agentNumLi = noPropAgent;
            String id = "";
            int max = agentNumLi.size();
            int min = 1;
            Random random = new Random();
            int s = random.nextInt(max) % (max - min + 1) + min;
            if (s > 0 && s <= agentNumLi.size()) {
                id = agentNumLi.get(s - 1) + "";
            }
            //                        System.out.println("拿到随机分配后的代售点：" + id);
            WriteLog.write(logName, "r1" + r1 + "," + "效率统计无概率，优先随机分配近七天没有效率的代售点：" + id);
            Map<String, Double> tempAgentAndGradeMap = new HashMap<String, Double>();
            tempAgentAndGradeMap.put(id, 1d);
            return tempAgentAndGradeMap;
        }
        return agentAndGradeMap;
    }

    /**
     * 出票成功时长与得分的转换
     * 
     * @param second
     * @return
     */
    public static double getTimeSwitchScore(long second) {
        double score = 0;// 得分
        if (second >= 0 && second < 180) {
            score = 1d;
        }
        else if (second >= 180 && second < 300) {
            score = 0.9;
        }
        else if (second >= 300 && second < 480) {
            score = 0.8;
        }
        else if (second >= 480 && second < 720) {
            score = 0.7;
        }
        else if (second >= 720) {
            score = 0.6;
        }
        return score;

    }

    /**
     * 通过供应商分单排名概率计算所分配的代售点
     * @param agentIds
     * @return 代售点id
     */
    public String probCalculate(String logName, long r1, String agentIds) {
        //        System.out.println(r1 + "," + "进入分单概率计算,agentIds=" + agentIds);
        WriteLog.write(logName, "r1" + r1 + "," + "进入分单概率计算,agentids=" + agentIds);
        String id = "";

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Date d = new Date();
        double allNum = 0; //参与分单逻辑代售点进单总数
        Map<String, Double> agentAndGradeMap = getAgentEfficiency(logName, r1, agentIds);
        Set agentGradeSet = agentAndGradeMap.keySet();
        /**
         * 概率表中只存在一个值直接返回
         */
        if (agentGradeSet.size() == 1) {
            Iterator tempGradIt = agentGradeSet.iterator();
            id = tempGradIt.next().toString();
            return id;
        }
        Iterator gradIt = agentGradeSet.iterator();
        String existPropAgentId = "";//得到有排名概率的代售点id
        while (gradIt.hasNext()) {
            String aid = (String) gradIt.next();
            existPropAgentId = existPropAgentId + aid + ",";
            allNum += agentAndGradeMap.get(aid);//拿到总得分
        }
        Map<String, Double> agentProbabilityMap = new HashMap<String, Double>();//每个代售点的概率
        Set<String> kset = agentAndGradeMap.keySet();
        for (String ks : kset) {
            Double grade = agentAndGradeMap.get(ks);
            BigDecimal a = BigDecimal.valueOf(grade).divide(BigDecimal.valueOf(allNum), 3, BigDecimal.ROUND_HALF_UP);//保留三位小数
            Double probability = a.doubleValue();
            //            System.out.println("agentid-" + ks + "的概率+" + probability + "////");
            WriteLog.write(logName, "r1" + r1 + "," + "agentid-" + ks + "的概率+" + probability + "////");
            agentProbabilityMap.put(ks, probability);
        }

        if (existPropAgentId.length() > 0) {//代售点至少存在一个计算概率,否则走随机分配原则
            existPropAgentId = existPropAgentId.substring(0, existPropAgentId.length() - 1);

            //为了雨露均沾，计算概率时要统计今天该组代售点中进单数
            String sql = "select agentid,count(id) as ordernum from TrainOrderOffline  with(nolock) "
                    + "where CreateTime between '" + sdf.format(d) + " 00:00:00' and '" + sdf.format(d) + " 23:59:59' "
                    + "and AgentId in (" + existPropAgentId + ") group by AgentId";
            //            List list = DBHelpUtil.getSystemServiceOldDB().findMapResultBySql(sql, null);
            DataTable list = new DataTable();
            try {
                list = DBHelperOffline.GetDataTable(sql, null);
            }
            catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            int amount = 0;//几个代售点的总单数
            Map<String, Integer> agentPerOrders = new HashMap<String, Integer>();//储存每个代售点的单数
            for (int j = 0; j < list.GetRow().size(); j++) {
                //                Map ordersTodayMap = (Map) list.get(j);
                DataRow ordersTodayMap = list.GetRow().get(j);
                String agentIdOTM = ordersTodayMap.GetColumnString("agentid").toString();
                int orderNumOTM = ordersTodayMap.GetColumnInt("ordernum");
                amount += orderNumOTM;
                agentPerOrders.put(agentIdOTM, orderNumOTM);
            }

            Set<String> fdSet = agentProbabilityMap.keySet();
            int nowNum = 0;//当前数量
            double nowAgentidProp = 0d;//当前代售点概率
            BigDecimal ratio = new BigDecimal(0);//进单数+1与总数+1的比值
            BigDecimal calc = new BigDecimal(0);//得到概率计算后的值，概率最小分配原则

            Map<Double, String> mapPropCompare = new TreeMap();
            for (String str : fdSet) {
                nowNum = agentPerOrders.containsKey(str) ? agentPerOrders.get(str) : 0;
                nowAgentidProp = agentProbabilityMap.get(str);

                ratio = (BigDecimal.valueOf((nowNum + 1)).divide(BigDecimal.valueOf((amount + 1)), 3,
                        BigDecimal.ROUND_HALF_UP));
                calc = ratio.subtract(BigDecimal.valueOf(nowAgentidProp));
                mapPropCompare.put(calc.doubleValue(), str);
                //                System.out.print(str + "计算后的分配数差：" + calc + ",当天订单数/参与计算代售点订单总数" + nowNum + "/" + amount + ";////");
                WriteLog.write(logName, "r1" + r1 + "," + str + "计算后的分配数差：" + calc + ",当天订单数/参与计算代售点订单总数" + nowNum + "/"
                        + amount + ";////");
            }
            Set<Double> keys = mapPropCompare.keySet();
            Iterator<Double> it = keys.iterator();
            while (it.hasNext()) {
                double key = it.next();
                id = mapPropCompare.get(key);
                break;
            }
            //            System.out.println("拿到概率计算后的代售点：" + id);
            WriteLog.write(logName, "r1" + r1 + "," + "拿到概率计算后的代售点：" + id);
        }
        else {//随机分配
            List agentNumLi = Arrays.asList(agentIds.split(","));
            int max = agentNumLi.size();
            int min = 1;
            Random random = new Random();
            int s = random.nextInt(max) % (max - min + 1) + min;
            if (s > 0 && s <= agentNumLi.size()) {
                id = agentNumLi.get(s - 1) + "";
            }
            //            System.out.println("拿到随机分配后的代售点：" + id);
            WriteLog.write(logName, "r1" + r1 + "," + "拿到随机分配后的代售点：" + id);
        }
        return id;
    }

}
