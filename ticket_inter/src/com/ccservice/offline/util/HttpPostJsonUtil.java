/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.ccservice.Util.PropertyUtil;
import com.ccservice.Util.file.WriteLog;

/**
 * @className: com.ccservice.tuniu.util.HttpPostJsonUtil
 * @description: TODO - 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月18日 下午2:15:34 
 * @version: v 1.0
 * @since 
 *
 */
public class HttpPostJsonUtil {
    private String account = PropertyUtil.getValue("tuniu.account", "Train.GuestAccount.properties");
    private String key = PropertyUtil.getValue("tuniu.desKey", "Train.GuestAccount.properties");

    public String getTuNiuRes(String data, String timestamp, String url) {
        /*String account = "tuniulvyou";
        String key = "v66r9ogtcvtxv3v4xq3gog8fqdbhwmt0";*/
        
        String responce = "";
        
        try {
            //身份校验 - 数据解密，签名校验
            String dataStr = TuNiuDesUtil.encrypt(data);
            
            //传的是什么，就用什么解密
            String checkSign = SignUtil.generateSign(account, dataStr, timestamp, key);
            //System.out.println(checkSign);//E185A7FDA6B7F04364FEBE0FFD22FE46
            
            String reqParam = "{\"account\":\""+account+"\",\"sign\":\""+checkSign +"\",\"timestamp\":\""+timestamp+"\",\"data\":\""+dataStr+"\"}";
            //System.out.println(reqParam);

            //记录请求信息日志
            WriteLog.write("途牛交互数据", "途牛交互数据-向平台请求信息-reqParam" + reqParam);

            responce = doPost(url, reqParam);
            //System.out.println(ret);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return responce;
    }

    public String getTuNiuReqRes(String data, String timestamp, String url, String errorMsg) {
        String responce = "";
        Integer returnCode = 231000;//success true用231000，false用231099
        try {
            //身份校验 - 数据解密，签名校验
            String dataStr = TuNiuDesUtil.encrypt(data);
            
            //传的是什么，就用什么解密
            String checkSign = SignUtil.generateReqSign(account, dataStr, timestamp, key, errorMsg, returnCode);
            //System.out.println(checkSign);//E185A7FDA6B7F04364FEBE0FFD22FE46
            
            String reqParam = "{\"account\":\""+account+"\",\"sign\":\""+checkSign +"\",\"timestamp\":\""+timestamp +"\",\"returnCode\":\""+returnCode +"\",\"errorMsg\":\""+errorMsg+"\",\"data\":\""+dataStr+"\"}";
            //System.out.println(reqParam);

            //记录请求信息日志
            WriteLog.write("途牛交互数据", "途牛交互数据-向途牛请求信息-reqParam" + reqParam);

            responce = doPost(url, reqParam);
            //System.out.println(ret);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return responce;
    }

    public String doPost(String url, String reqParam) throws Exception {
        HttpPost post = setPostRequestHeader(url);
        //post.setHeader("Content-Type", "application/x-www-form-urlencoded");
        post.setHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
        //post.setHeader("ContentEncoding", "UTF-8");
        /*s.setContentEncoding("UTF-8");
        s.setContentType("application/json");*///发送json数据需要设置contentType
        StringEntity s = new StringEntity(reqParam, "UTF-8");
        post.setEntity(s);

        CloseableHttpClient rrr = HttpClients.createDefault();//通过对象提交请求
        CloseableHttpResponse res = rrr.execute(post);
        
        return EntityUtils.toString(res.getEntity());
    }

    public String doPostForm(String url, String reqParam) throws Exception {
        //破解登录
        HttpPost post = setPostRequestHeader(url);
        post.setHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
        
        List<NameValuePair> loginlist = new ArrayList<NameValuePair>();
        loginlist.add(new BasicNameValuePair("data", reqParam));
        /*loginlist.add(new BasicNameValuePair("ismock", ismock)); //
        loginlist.add(new BasicNameValuePair("module", module));
        loginlist.add(new BasicNameValuePair("empoent", empoent));
        loginlist.add(new BasicNameValuePair("authenticityToken", authenticityToken));
        loginlist.add(new BasicNameValuePair("user.username", user_username));
        loginlist.add(new BasicNameValuePair("user.password", user_password02));
        loginlist.add(new BasicNameValuePair("user.verifycode", captcha));*/
        
        UrlEncodedFormEntity loginEn = new UrlEncodedFormEntity(loginlist, "UTF-8");
        post.setEntity(loginEn);
        
        CloseableHttpClient rrr = HttpClients.createDefault();//通过对象提交请求
        CloseableHttpResponse res = rrr.execute(post);

        return EntityUtils.toString(res.getEntity());
    }
    
    // get方式设置 请求头
    public HttpGet setGetRequestHeader(String url) {
        HttpGet get = new HttpGet(url);
        get.addHeader("User-Agent",
                "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.96 Safari/537.36");
        get.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        get.addHeader("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3");
        get.addHeader("Accept-Encoding", "gzip, deflate");
        get.addHeader("Connection", "keep-alive");
        get.addHeader("Upgrade-Insecure-Requests", "1");
        return get;
    }

    // 珠海post方式设置请求头，主要是为了表单的提交
    public HttpPost setPostRequestHeader(String url) {
        String temp = "";
        HttpPost post = new HttpPost(url);
        post.addHeader("User-Agent",
                "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.96 Safari/537.36");
        post.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        post.addHeader("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3");
        post.addHeader("Accept-Encoding", "gzip, deflate");
        post.addHeader("Connection", "keep-alive");
        post.addHeader("Upgrade-Insecure-Requests", "1");
        return post;
    }

}
