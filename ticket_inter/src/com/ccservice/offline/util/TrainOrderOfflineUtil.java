/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.util;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.alibaba.fastjson.JSONObject;
import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataRow;
import com.ccervice.util.db.DataTable;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.util.ExceptionUtil;
import com.ccservice.offline.dao.TrainOfflineExpressCodeDao;
import com.ccservice.offline.dao.TrainOfflineIdempotentDao;
import com.ccservice.offline.dao.TrainOfflineMatchAgentDao;
import com.ccservice.offline.dao.TrainOfflinePriceDao;
import com.ccservice.offline.dao.TrainOfflineTomAgentAddDao;
import com.ccservice.offline.dao.TrainOrderAgentTimesDao;
import com.ccservice.offline.dao.TrainOrderOfflineRecordDao;
import com.ccservice.offline.domain.TrainOfflineIdempotent;
import com.ccservice.offline.domain.TrainOfflineMatchAgent;
import com.ccservice.offline.domain.TrainOrderAgentTimes;
import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.domain.TrainOrderOfflineRecord;
import com.ccservice.offlineExpress.uupt.UUptService;

/**
 * @className: com.ccservice.tuniu.util.TrainOrderOfflineUtil
 * @description: TODO - 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月16日 下午2:51:24 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOrderOfflineUtil {
    public static final Integer TNRETRY = 3;//途牛的网络异常的重试次数

    public static final Integer TNIDEMPOTENTRETRY = 10;//途牛的幂等的异常处理的重试次数

    private static TrainOfflineMatchAgentDao trainOfflineMatchAgentDao = new TrainOfflineMatchAgentDao();

    private static TrainOfflineExpressCodeDao trainOfflineExpressCodeDao = new TrainOfflineExpressCodeDao();

    private static TrainOrderAgentTimesDao trainOrderAgentTimesDao = new TrainOrderAgentTimesDao();

    private static TrainOfflineTomAgentAddDao trainOfflineTomAgentAddDao = new TrainOfflineTomAgentAddDao();

    private static TrainOfflineIdempotentDao trainOfflineIdempotentDao = new TrainOfflineIdempotentDao();

    private static TrainOfflinePriceDao trainOfflinePriceDao = new TrainOfflinePriceDao();

    /**
     * 途牛幂等实现的本地全局变量设计
     * 
     * 全局变量，要做到相关内容的后续删除 - 否则会越来越大，是不允许的
     * 
     * 对于订单的出票请求下单入口
     * 
     * 首先给出一个最快捷的请求处理中的锁，后续的请求不再做进一步的处理，等待全局结果的反馈 - orderId+OrderLock - 外围全局锁
     * 
     * 用于判断请求是否重复 - idempotentFlag - orderId - test17082523725055 + RetryFlag = orderId+OrderRetryFlag - 这个可以不要
     * 
     * 用于判断是否有某一个请求的处理完成 - orderId+OrderHandleOverFlag - 这个也可以不要
     * 
     * 用于存储某个请求完成处理后的结果 - 在等待范围内，做统一的返回 - orderId+OrderHandleOverResultFlag
     * 
     * 等待锁的状态变化之后，取出相关的结果进行反馈，并抹掉相关存储即可
     * 
     * 但是，只能在所有请求处理完毕之后才能抹掉数据
     * 
     * 所以说，此处需要记录在上锁的期间接收了多少次请求，反馈了多少次结果，然后再移除相关的数据内容
     * 
     * orderId+OrderReqNum
     * orderId+OrderResNum
     * 
     * 利用这个计数器，也方便记录日志
     * 
     * 最后一次反馈完成的时候，移除相关的内容
     * 
     * 实际共使用了四个属性
     * 
     * 其它接口类似
     */
    public static Map<String, String> TnIdempotent = new HashMap<String, String>();
    //设计为同步的
    //public static Map<String, String> TnIdempotent = Collections.synchronizedMap(new HashMap<String, String>());

    //幂等中的请求初始化数据
    /*public static void initIdempotent(String reqBody, String flag) {
        TnIdempotent.put(reqBody+flag+"Lock", "true");//这两者之间的时间差很快，异常情况，就不再考虑了
        TnIdempotent.put(reqBody+flag+"ReqNum", "1");//
        TnIdempotent.put(reqBody+flag+"ResNum", "0");//
    }*/

    /*public static void removeData(String reqBody, String flag) {
        TnIdempotent.remove(reqBody+flag+"Lock");
        TnIdempotent.remove(reqBody+flag+"ReqNum");
        TnIdempotent.remove(reqBody+flag+"ResNum");
        TnIdempotent.remove(reqBody+flag+"HandleOverResultFlag");
    }*/

    //幂等中的一次请求 - 该方法涉及到获取和存储，设计成同步的 - 此方法是此处幂等实现的关键
    /*public static synchronized String idempotentGetPut(String reqBody, String flag) {
        String OrderNum = TnIdempotent.get(reqBody+flag);
        OrderNum = Integer.valueOf(OrderNum)+1+"";
        TnIdempotent.put(reqBody+flag, OrderNum);
        return OrderNum;
    }*/

    //两个方法必须分开，不能使用同一个锁

    public static synchronized TrainOfflineIdempotent idempotentGetPutReqNum(
            TrainOfflineIdempotent trainOfflineIdempotent) {
        Integer idempotentReqNum = trainOfflineIdempotent.getIdempotentReqNum();
        idempotentReqNum = idempotentReqNum + 1;
        trainOfflineIdempotent.setIdempotentReqNum(idempotentReqNum);
        try {
            trainOfflineIdempotentDao.updateReqNumByPKID(trainOfflineIdempotent);
        }
        catch (Exception e) {
            ExceptionTNUtil.handleTNException(e);
        }
        return trainOfflineIdempotent;
    }

    public static synchronized TrainOfflineIdempotent idempotentGetPutResNum(
            TrainOfflineIdempotent trainOfflineIdempotent) {
        //也是 - 必须做数据库的实时查询
        //Integer idempotentResNum = trainOfflineIdempotent.getIdempotentResNum();
        Integer idempotentResNum = 0;
        try {
            idempotentResNum = trainOfflineIdempotentDao.findResNumByPKID(trainOfflineIdempotent.getPKID());
        }
        catch (Exception e) {
            ExceptionTNUtil.handleTNException(e);
        }

        if (idempotentResNum == null) {//提前完成的删除动作
            return null;
        }

        idempotentResNum = idempotentResNum + 1;
        trainOfflineIdempotent.setIdempotentResNum(idempotentResNum);
        try {
            trainOfflineIdempotentDao.updateResNumByPKID(trainOfflineIdempotent);
        }
        catch (Exception e) {
            ExceptionTNUtil.handleTNException(e);
        }

        //必须做数据库的实时查询
        //Integer idempotentReqNum = trainOfflineIdempotent.getIdempotentReqNum();//请求次数
        Integer idempotentReqNum = 0;
        try {
            idempotentReqNum = trainOfflineIdempotentDao.findReqNumByPKID(trainOfflineIdempotent.getPKID());
        }
        catch (Exception e) {
            ExceptionTNUtil.handleTNException(e);
        }

        //Integer idempotentResNum = trainOfflineIdempotent.getIdempotentResNum();//响应次数

        //WriteLog.write("幂等判定流程测试", "PKID-"+trainOfflineIdempotent.getPKID()+",idempotentReqNum="+trainOfflineIdempotent.getIdempotentReqNum()+",idempotentResNum="+idempotentResNum);

        if (idempotentReqNum.equals(idempotentResNum)) {//请求和响应次数一致，做删除动作
            //TrainOrderOfflineUtil.removeData(reqBodyTemp, idempotentFlag);// - 删除数据库的记录 - 
            Integer flagTemp = trainOfflineIdempotentDao
                    .delTrainOfflineIdempotentByPKID(trainOfflineIdempotent.getPKID());
            if (flagTemp < 1) {
                //记录请求信息日志
                WriteLog.write("途牛线下票出票请求幂等判定", "途牛线下票出票请求幂等设计-请求和反馈一致之后进行了清空操作---数据库清空失败");
            }
            //记录请求信息日志
            //WriteLog.write("途牛线下票出票请求幂等判定", "途牛线下票出票请求幂等设计-请求和反馈一致之后进行了清空操作:"+idempotentResNum);

            //WriteLog.write("幂等判定流程测试", "idempotentReqNum-"+idempotentReqNum+",idempotentResNum="+idempotentResNum+",进行了清空操作-PKID-"+trainOfflineIdempotent.getPKID());
        }

        return trainOfflineIdempotent;
    }

    /*public static synchronized TrainOfflineIdempotent idempotentGetPut(TrainOfflineIdempotent trainOfflineIdempotent, int flag) {
        //flag - 转换更新标记 - 1-ReqNum  - 2-ResNum
        if (flag == 1) {//ReqNum
            Integer idempotentReqNum = trainOfflineIdempotent.getIdempotentReqNum();
            idempotentReqNum = idempotentReqNum+1;
            trainOfflineIdempotent.setIdempotentReqNum(idempotentReqNum);
            try {
                trainOfflineIdempotentDao.updateReqNumByPKID(trainOfflineIdempotent);
            }
            catch (Exception e) {
                ExceptionTNUtil.handleTNException(e);
            }
        } else if (flag == 2) {//ResNum
            Integer idempotentResNum = trainOfflineIdempotent.getIdempotentResNum();
            idempotentResNum = idempotentResNum+1;
            trainOfflineIdempotent.setIdempotentResNum(idempotentResNum);
            try {
                trainOfflineIdempotentDao.updateResNumByPKID(trainOfflineIdempotent);
            }
            catch (Exception e) {
                ExceptionTNUtil.handleTNException(e);
            }
            
            Integer idempotentReqNum = trainOfflineIdempotent.getIdempotentReqNum();//请求次数
            //Integer idempotentResNum = trainOfflineIdempotent.getIdempotentResNum();//响应次数
    
            WriteLog.write("幂等判定流程测试", "PKID-"+trainOfflineIdempotent.getPKID()+",idempotentReqNum="+trainOfflineIdempotent.getIdempotentReqNum()+",idempotentResNum="+idempotentResNum);
    
            if (idempotentReqNum.equals(idempotentResNum)) {//请求和响应次数一致，做删除动作
                //TrainOrderOfflineUtil.removeData(reqBodyTemp, idempotentFlag);// - 删除数据库的记录 - 
                Integer flagTemp = trainOfflineIdempotentDao.delTrainOfflineIdempotentByPKID(trainOfflineIdempotent.getPKID());
                if (flagTemp<1) {
                    //记录请求信息日志
                    WriteLog.write("途牛线下票出票请求幂等判定", "途牛线下票出票请求幂等设计-请求和反馈一致之后进行了清空操作---数据库清空失败");
                }
                //记录请求信息日志
                //WriteLog.write("途牛线下票出票请求幂等判定", "途牛线下票出票请求幂等设计-请求和反馈一致之后进行了清空操作:"+idempotentResNum);
    
                WriteLog.write("幂等判定流程测试", "idempotentReqNum-"+idempotentReqNum+",idempotentResNum="+idempotentResNum+",进行了清空操作-PKID-"+trainOfflineIdempotent.getPKID());
    
            }
        }
        return trainOfflineIdempotent;
    }*/

    public static void resOneAndAddResult(TrainOfflineIdempotent trainOfflineIdempotent, String result,
            Integer random) {
        //TnIdempotent.put(reqBody+flag+"HandleOverResultFlag", result);
        trainOfflineIdempotent.setIdempotentResultValue(result);

        try {
            trainOfflineIdempotentDao.updateResultValueByPKID(trainOfflineIdempotent);
        }
        catch (Exception e) {
            ExceptionTNUtil.handleTNException(e);
        }

        //trainOfflineIdempotent = idempotentGetPut(trainOfflineIdempotent, 2);
        trainOfflineIdempotent = idempotentGetPutResNum(trainOfflineIdempotent);

        //能走到这一步 - 肯定不为空
        /*Integer idempotentReqNum = trainOfflineIdempotent.getIdempotentReqNum();//请求次数
        Integer idempotentResNum = trainOfflineIdempotent.getIdempotentResNum();//响应次数
        
        WriteLog.write("幂等判定流程测试", "PKID-"+trainOfflineIdempotent.getPKID()+",idempotentReqNum="+trainOfflineIdempotent.getIdempotentReqNum()+",idempotentResNum="+idempotentResNum);
        
        if (idempotentReqNum.equals(idempotentResNum)) {//请求和响应次数一致，做删除动作
            //TrainOrderOfflineUtil.removeData(reqBodyTemp, idempotentFlag);// - 删除数据库的记录 - 
            Integer flagTemp = trainOfflineIdempotentDao.delTrainOfflineIdempotentByPKID(trainOfflineIdempotent.getPKID());
            if (flagTemp<1) {
                //记录请求信息日志
                WriteLog.write("途牛线下票出票请求幂等判定", "途牛线下票出票请求幂等设计-请求和反馈一致之后进行了清空操作---数据库清空失败");
            }
            //记录请求信息日志
            //WriteLog.write("途牛线下票出票请求幂等判定", "途牛线下票出票请求幂等设计-请求和反馈一致之后进行了清空操作:"+idempotentResNum);
        
            WriteLog.write("幂等判定流程测试", "idempotentReqNum-"+idempotentReqNum+",idempotentResNum="+idempotentResNum+",进行了清空操作-PKID-"+trainOfflineIdempotent.getPKID());
        
        }*/
    }

    /*public static void resOneAndAddResult(String reqBody, String flag, String result, Integer random) {
        TnIdempotent.put(reqBody+flag+"HandleOverResultFlag", result);
    
        //幂等中的一次响应并反馈结果
        String OrderResNum = TnIdempotent.get(reqBody+flag+"ResNum");
        OrderResNum = Integer.valueOf(OrderResNum)+1+"";
        TnIdempotent.put(reqBody+flag+"ReqNum", OrderResNum);
        
        String OrderResNum = idempotentGetPut(reqBody, flag+"ResNum");
    
        //主要处理一次请求和响应的问题
        
        //记录请求信息日志
        //WriteLog.write("途牛线下票出票请求幂等判定", "000000:途牛线下票出票请求幂等设计-OrderResNum:"+OrderResNum);
        
        //一次请求完毕之后，也需要清除幂等的效果
        
        //此处做一次请求效果的清除判定
    
        //幂等中的一次请求 - 该方法涉及到获取和存储，设计成同步的
        String OrderReqNum = TrainOrderOfflineUtil.TnIdempotent.get(reqBody+flag+"ReqNum");
        //能走到这一步 - 肯定不为空
    
        //记录请求信息日志
        //WriteLog.write("途牛线下票出票请求幂等判定", "000000:途牛线下票出票请求幂等设计-OrderReqNum:"+OrderReqNum);
        
        if (OrderReqNum.equals(OrderResNum)) {//请求和响应次数一致，做删除动作
            TrainOrderOfflineUtil.removeData(reqBody, flag);
    
            //记录请求信息日志
            //WriteLog.write("途牛线下票出票请求幂等判定", "000000:途牛线下票出票请求幂等设计-请求和反馈一致之后进行了清空操作:"+OrderResNum);
        }
        
        
        //记录请求信息日志
        String logName = "途牛线下票出票请求幂等判定";
        if (flag.contains("Cancel")) {
            logName = "途牛线下票取消请求幂等判定";
        }
        WriteLog.write(logName, random + ":"+logName+"-幂等中的一次响应并反馈结果:"+OrderResNum);
    }*/

    public static void CreateBusLogAdmin(Long orderId, Integer ProviderAgentid, String optContent) {
        TrainOrderOfflineRecord trainOrderOfflineRecord = new TrainOrderOfflineRecord();
        trainOrderOfflineRecord.setFKTrainOrderOfflineId(orderId);
        trainOrderOfflineRecord.setProviderAgentid(ProviderAgentid);
        trainOrderOfflineRecord.setRefundReasonStr(optContent);
        int flag = TrainOrderOfflineRecordDao.CreateLogAdmin(trainOrderOfflineRecord);
        if (flag == -10) {
            CreateLogAdminFail(trainOrderOfflineRecord);
        }
    }

    public static void CreateBusLogAdminOtherDealResult(Long orderId, Integer ProviderAgentid, String optContent,
            Integer dealResult) {
        TrainOrderOfflineRecord trainOrderOfflineRecord = new TrainOrderOfflineRecord();
        trainOrderOfflineRecord.setFKTrainOrderOfflineId(orderId);
        trainOrderOfflineRecord.setProviderAgentid(ProviderAgentid);
        trainOrderOfflineRecord.setRefundReasonStr(optContent);
        int flag = TrainOrderOfflineRecordDao.CreateLogAdminOtherDealResult(trainOrderOfflineRecord, dealResult);
        if (flag == -10) {
            CreateLogAdminFail(trainOrderOfflineRecord);
        }
    }

    public static void CreateBusLogAdminNoUserId(Long orderId, String optContent) {
        TrainOrderOfflineRecord trainOrderOfflineRecord = new TrainOrderOfflineRecord();
        trainOrderOfflineRecord.setFKTrainOrderOfflineId(orderId);
        trainOrderOfflineRecord.setProviderAgentid(46);//正式环境里面这个配置的是 - 系统的预留用户角色
        trainOrderOfflineRecord.setRefundReasonStr(optContent);
        int flag = TrainOrderOfflineRecordDao.CreateLogAdmin(trainOrderOfflineRecord);
        if (flag == -10) {
            CreateLogAdminFail(trainOrderOfflineRecord);
        }
    }

    public static void CreateBusLogAdminNoUserIdOtherDealResult(Long orderId, String optContent, Integer dealResult) {
        TrainOrderOfflineRecord trainOrderOfflineRecord = new TrainOrderOfflineRecord();
        trainOrderOfflineRecord.setFKTrainOrderOfflineId(orderId);
        trainOrderOfflineRecord.setProviderAgentid(46);//正式环境里面这个配置的是 - 系统的预留用户角色
        trainOrderOfflineRecord.setRefundReasonStr(optContent);
        int flag = TrainOrderOfflineRecordDao.CreateLogAdminOtherDealResult(trainOrderOfflineRecord, dealResult);
        if (flag == -10) {
            CreateLogAdminFail(trainOrderOfflineRecord);
        }
    }

    public static void CreateLogAdminFail(TrainOrderOfflineRecord trainOrderOfflineRecord) {
        TrainOrderOfflineRecord trainOrderOfflineRecordTemp = new TrainOrderOfflineRecord();
        trainOrderOfflineRecordTemp.setFKTrainOrderOfflineId(trainOrderOfflineRecord.getFKTrainOrderOfflineId());
        trainOrderOfflineRecordTemp.setProviderAgentid(46);
        trainOrderOfflineRecordTemp.setRefundReasonStr("日志记录失败");
        /*int flag = */TrainOrderOfflineRecordDao.CreateLogAdmin(trainOrderOfflineRecord);
    }

    public static void sleep(Integer sleepTime) {
        try {
            Thread.sleep(sleepTime);
        }
        catch (InterruptedException e) {
            ExceptionTNUtil.handleTNException(e);
        }
    }

    public static Timestamp getTimestampByStr(String insertTime) throws Exception {
        return new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(insertTime).getTime());
    }

    public static String getNowDateStr() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date());
    }

    public static String getTuNiuTimestamp() {
        return new SimpleDateFormat("yyyy-MM-ddHH:mm:ss").format(new Date());//2017-08-1810:34:29
    }

    public static String getTuNiuReqTimestamp() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());//2017-08-1810:34:29
    }

    public static String getTimestrByTime(Long time) {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(time));//2017-08-1810:34:29
    }

    public static Date getDateByTimeStr(String timeStr) throws Exception {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(timeStr);//2017-07-06 20:20:00
    }

    public static Date getOverTimeDateByStr(String timeStr) throws Exception {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(timeStr);//2017-07-06 20:20:00
    }

    public static String getCostTime(String startTime, String endTime) throws Exception {
        Long costTimeL = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(endTime).getTime()
                - new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(startTime).getTime();
        Long costTimeS = costTimeL / 1000;
        Long costTimeM = costTimeS / 60;
        int costTimeH = (int) (costTimeM / 60);
        int costTimeMm = (int) (costTimeM % 60);
        /*System.out.println(costTimeL);
        System.out.println(costTimeS);
        System.out.println(costTimeM);
        System.out.println(costTimeH);
        System.out.println(costTimeMm);
        System.out.println(costTimeH+":"+costTimeMm);*/
        return costTimeH + ":" + costTimeMm;
    }

    //Integer IdType;
    public static int getTuNiuIdType(String IdTypeStr) {
        int type = 0;
        if (IdTypeStr.equals("1")) {
            type = 1;
        }
        else if (IdTypeStr.equals("2")) {
            type = 2;
        }
        else if (IdTypeStr.equals("C")) {
            type = 10;
        }
        else if (IdTypeStr.equals("G")) {
            type = 11;
        }
        else if (IdTypeStr.equals("B")) {
            type = 12;
        }
        else if (IdTypeStr.equals("H")) {
            type = 13;
        }
        return type;//
    }

    //Integer IdType;
    public static String getTuNiuResIdTypeStr(Integer IdType) {
        String resTypeStr = "";
        if (IdType == 1) {
            resTypeStr = "1";
        }
        else if (IdType == 2) {
            resTypeStr = "2";
        }
        else if (IdType == 10) {
            resTypeStr = "C";
        }
        else if (IdType == 11) {
            resTypeStr = "G";
        }
        else if (IdType == 12) {
            resTypeStr = "B";
        }
        else if (IdType == 13) {
            resTypeStr = "H";
        }
        return resTypeStr;//
    }

    //乘客证件类型 - 1:二代身份证，2:一代身份证，C:港澳通行证，G:台湾通行证，B:护照 ,H:外国人居留证
    public static String getTuNiuIdTypeStr(Integer IdType) {
        String typeStr = "";
        if (IdType == 1) {
            typeStr = "二代身份证";
        }
        else if (IdType == 2) {
            typeStr = "一代身份证";
        }
        else if (IdType == 10) {
            typeStr = "港澳通行证";
        }
        else if (IdType == 11) {
            typeStr = "台湾通行证";
        }
        else if (IdType == 12) {
            typeStr = "护照";
        }
        else if (IdType == 13) {
            typeStr = "外国人居留证";
        }
        return typeStr;
    }

    //票类型:成人，小孩。儿童 1:成人票，2:儿童票，3:学生票，4:残军票
    public static String getTicketTypeStr(Integer TicketType) {
        String typeStr = "";
        if (TicketType == 1) {
            typeStr = "成人票";
        }
        else if (TicketType == 2) {
            typeStr = "儿童票";
        }
        else if (TicketType == 3) {
            typeStr = "学生票";
        }
        else if (TicketType == 4) {
            typeStr = "残军票";
        }
        return typeStr;
    }

    /**
     * 途牛
     * 
    0               棚车
    1               硬座 - 没有座位号 - 显示无座
    2               软座
    3               硬卧
    4               软卧
    5               包厢硬卧
    6               高级软卧
    7               一等软座
    8               二等软座
    9               商务座
    A               高级动卧
    B               混编硬座
    C               混编硬卧
    D               包厢软座
    E               特等软座
    F               动卧
    G               二人软包
    H               一人软包
    I               一等双软
    J               二等双软
    K               混编软座
    L               混编软卧
    M               一等座
    O               二等座
    P               特等座
    Q               观光座
    S               一等包座
     * 
     */
    public static String getSeatTypeCode(String SeatType) {
        String seatTypeCode = "";
        if (SeatType.equals("棚车")) {
            seatTypeCode = "0";
        }
        else if (SeatType.equals("硬座")) {
            seatTypeCode = "1";
        }
        else if (SeatType.equals("软座")) {
            seatTypeCode = "2";
        }
        else if (SeatType.equals("硬卧")) {
            seatTypeCode = "3";
        }
        else if (SeatType.equals("软卧")) {
            seatTypeCode = "4";
        }
        else if (SeatType.equals("包厢硬卧")) {
            seatTypeCode = "5";
        }
        else if (SeatType.equals("高级软卧")) {
            seatTypeCode = "6";
        }
        else if (SeatType.equals("一等软座")) {
            seatTypeCode = "7";
        }
        else if (SeatType.equals("二等软座")) {
            seatTypeCode = "8";
        }
        else if (SeatType.equals("商务座")) {
            seatTypeCode = "9";
        }
        else if (SeatType.equals("高级动卧")) {
            seatTypeCode = "A";
        }
        else if (SeatType.equals("混编硬座")) {
            seatTypeCode = "B";
        }
        else if (SeatType.equals("混编硬卧")) {
            seatTypeCode = "C";
        }
        else if (SeatType.equals("包厢软座")) {
            seatTypeCode = "D";
        }
        else if (SeatType.equals("特等软座")) {
            seatTypeCode = "E";
        }
        else if (SeatType.equals("动卧")) {
            seatTypeCode = "F";
        }
        else if (SeatType.equals("二人软包")) {
            seatTypeCode = "G";
        }
        else if (SeatType.equals("一人软包")) {
            seatTypeCode = "H";
        }
        else if (SeatType.equals("一等双软")) {
            seatTypeCode = "I";
        }
        else if (SeatType.equals("二等双软")) {
            seatTypeCode = "J";
        }
        else if (SeatType.equals("混编软座")) {
            seatTypeCode = "K";
        }
        else if (SeatType.equals("混编软卧")) {
            seatTypeCode = "L";
        }
        else if (SeatType.equals("一等座")) {
            seatTypeCode = "M";
        }
        else if (SeatType.equals("二等座")) {
            seatTypeCode = "O";
        }
        else if (SeatType.equals("特等座")) {
            seatTypeCode = "P";
        }
        else if (SeatType.equals("观光座")) {
            seatTypeCode = "Q";
        }
        else if (SeatType.equals("一等包座")) {
            seatTypeCode = "S";
        }
        return seatTypeCode;
    }

    /**
     * @description: TODO - 
     * 
     * 同程
     * 
    bakcupSeatClass这个没有，看acceptOtherSeat ， acceptOtherSeat为0， 直接驳回
    不看传给你的座席的
     * 20170925
     * 
     * 
     * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
     * @createTime: 2017年8月31日 下午5:19:34
     * @param seatClass
     * @return
     */
    public static String getSeatTypeByTCSeatClass(Integer SeatType) {
        String seatTypeCode = "";
        if (SeatType == 0) {
            seatTypeCode = "";
        }
        else if (SeatType == 1) {
            seatTypeCode = "硬座";
        }
        else if (SeatType == 2) {
            seatTypeCode = "软座";
        }
        else if (SeatType == 3) {
            seatTypeCode = "一等座";
        }
        else if (SeatType == 33) {
            seatTypeCode = "一等软座";
        }
        else if (SeatType == 4) {
            seatTypeCode = "二等座";
        }
        else if (SeatType == 44) {
            seatTypeCode = "二等软座";
        }
        else if (SeatType == 5) {
            seatTypeCode = "硬卧";
            //seatTypeCode = "硬卧上铺";
        }
        else if (SeatType == 6) {
            seatTypeCode = "硬卧";
            //seatTypeCode = "硬卧中铺";
        }
        else if (SeatType == 116) {
            seatTypeCode = "硬卧";
        }
        else if (SeatType == 7) {
            seatTypeCode = "硬卧";
            //seatTypeCode = "硬卧下铺";
        }
        else if (SeatType == 8) {
            seatTypeCode = "软卧";
            //seatTypeCode = "软卧上铺";
        }
        else if (SeatType == 9) {
            seatTypeCode = "软卧";
            //seatTypeCode = "软卧下铺";
        }
        else if (SeatType == 119) {
            seatTypeCode = "软卧";
        }
        else if (SeatType == 10) {
            seatTypeCode = "无座";
        }
        else if (SeatType == 11) {
            seatTypeCode = "商务座";
        }
        else if (SeatType == 12) {
            seatTypeCode = "特等座";
        }
        else if (SeatType == 13) {
            seatTypeCode = "其它";
        }
        else if (SeatType == 14) {
            seatTypeCode = "高级软卧";
        }
        else if (SeatType == 15) {
            seatTypeCode = "动卧";
            //seatTypeCode = "动卧上铺";
        }
        /*else if (SeatType == 115) {
            seatTypeCode = "动卧";
        }*/
        else if (SeatType == 16) {
            seatTypeCode = "动卧";
            //seatTypeCode = "动卧下铺";
        }
        else if (SeatType == 17) {
            seatTypeCode = "高级动卧";
            //seatTypeCode = "高级动卧上铺";
        }
        /*else if (SeatType == 118) {
            seatTypeCode = "高级动卧";
        }*/
        else if (SeatType == 18) {
            seatTypeCode = "高级动卧";
            //seatTypeCode = "高级动卧下铺";
        }
        return seatTypeCode;
    }

    public static Integer getSeatClassByTCSeatType(String SeatType) {
        Integer seatTypeCode = 0;
        if (SeatType.equals("")) {
            seatTypeCode = 0;
        }
        else if (SeatType.equals("硬座")) {
            seatTypeCode = 1;
        }
        else if (SeatType.equals("软座")) {
            seatTypeCode = 2;
        }
        else if (SeatType.equals("一等座")) {
            seatTypeCode = 3;
        }
        else if (SeatType.equals("一等软座")) {
            seatTypeCode = 33;
        }
        else if (SeatType.equals("二等座")) {
            seatTypeCode = 4;
        }
        else if (SeatType.equals("二等软座")) {
            seatTypeCode = 44;
        }
        else if (SeatType.equals("硬卧上铺")) {
            seatTypeCode = 5;
        }
        else if (SeatType.equals("硬卧中铺")) {
            seatTypeCode = 6;
        }
        else if (SeatType.equals("硬卧")) {
            seatTypeCode = 116;
        }
        else if (SeatType.equals("硬卧下铺")) {
            seatTypeCode = 7;
        }
        else if (SeatType.equals("软卧上铺")) {
            seatTypeCode = 8;
        }
        else if (SeatType.equals("软卧下铺")) {
            seatTypeCode = 9;
        }
        else if (SeatType.equals("软卧")) {
            seatTypeCode = 119;
        }
        else if (SeatType.equals("无座")) {
            seatTypeCode = 10;
        }
        else if (SeatType.equals("商务座")) {
            seatTypeCode = 11;
        }
        else if (SeatType.equals("特等座")) {
            seatTypeCode = 12;
        }
        else if (SeatType.equals("其它")) {
            seatTypeCode = 13;
        }
        else if (SeatType.equals("高级软卧")) {
            seatTypeCode = 14;
        }
        else if (SeatType.equals("动卧上铺")) {
            seatTypeCode = 15;
        }
        /*else if (SeatType.equals("动卧")) {
            seatTypeCode = 115;
        }*/
        else if (SeatType.equals("动卧下铺")) {
            seatTypeCode = 16;
        }
        else if (SeatType.equals("高级动卧上铺")) {
            seatTypeCode = 17;
        }
        /*else if (SeatType.equals("高级动卧")) {
            seatTypeCode = 118;
        }*/
        else if (SeatType.equals("高级动卧下铺")) {
            seatTypeCode = 18;
        }
        return seatTypeCode;
    }

    public static String getTCSeatNameBySeatCode(Integer SeatCode) {
        String seatName = "";
        if (SeatCode == 1) {
            seatName = "上铺";
        }
        else if (SeatCode == 2) {
            seatName = "中铺";
        }
        else if (SeatCode == 3) {
            seatName = "下铺";
        }
        return seatName;
    }

    public static Integer getExpressAgentByName(String expressCompanyName) {
        int type = 0;//默认 - 顺丰 - //0-顺丰 - 1-EMS 2-宅急送
        if (expressCompanyName.equals("1")) {
            type = 1;
        }
        else if (expressCompanyName.equals("2")) {
            type = 2;
        }
        return type;//
    }

    public static String getExpressAgentNameByName(String expressCompanyName) {
        String AgentName = "顺丰";//默认 - 顺丰 - //0-顺丰 - 1-EMS 2-宅急送
        if (expressCompanyName.equals("1")) {
            AgentName = "EMS";
        }
        else if (expressCompanyName.equals("2")) {
            AgentName = "宅急送";
        }
        return AgentName;//
    }

    /**
     * 中原银行的订单号不超过20位 - 给他一个20位的
     * @return
     */
    public static String timeMinID() {
        Random random = new Random();
        int rannum = (int) (random.nextDouble() * (999 - 100 + 1)) + 100;// 获取3位随机数  
        return new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()) + rannum;
    }

    public static String timeMinIDRapidSend() {
        Random random = new Random();
        int rannum = (int) (random.nextDouble() * (99999 - 10000 + 1)) + 10000;// 获取3位随机数  
        return new SimpleDateFormat("yyMMddHHmmssSSS").format(new Date()) + rannum;
    }

    //配送票的分单逻辑
    //新版分单 - 参考代码位置 - com.ccservice.b2b2c.atom.taobao.thread.MyThreadTaoBaoOrderOfflineAdd.distribution2(String) - 途牛分单逻辑
    public static String distribution(String deliveryAddress, Integer createUid) throws Exception {
        //默认出票点
        String agentId = "";

        //既然目前的分单逻辑需要和淘宝保持一致-那么直接采用淘宝的匹配规则就好了-无需再次添加！！！
        //String sql1 = "SELECT agentId FROM TrainOfflineMatchAgent WHERE status=2 AND createUid="+createUid;
        String sql1 = "SELECT agentId FROM TrainOfflineMatchAgent WHERE status=2 AND createUid=56";

        DataTable dataTable = DBHelperOffline.GetDataTable(sql1, null);
        if (dataTable.GetRow().size() == 0) {
            return "途牛的默认出票点的信息不存在-agentId" + agentId;
        }
        agentId = String.valueOf(dataTable.GetRow().get(0).GetColumn("agentId").GetValue());

        //中间差一个判断逻辑

        //程序自动分配出票点
        List<TrainOfflineMatchAgent> trainOfflineMatchAgentList = trainOfflineMatchAgentDao
                .findTrainOfflineMatchAgentListDetailByRandom();
        List listAgents = new ArrayList();
        for (int i = 0; i < trainOfflineMatchAgentList.size(); i++) {
            TrainOfflineMatchAgent trainOfflineMatchAgent = trainOfflineMatchAgentList.get(i);
            String provinces = trainOfflineMatchAgent.getProvinces();
            Integer agentid = trainOfflineMatchAgent.getAgentId();
            String[] add = provinces.split(",");
            for (int j = 0; j < add.length; j++) {
                boolean flag = false;
                if (deliveryAddress.startsWith(add[j])) {
                    listAgents.add(agentid);
                    flag = true;
                }
                if (flag) {
                    break;
                }
            }
        }

        /**
         * 目前走的是随机分配的逻辑，
         * 
         * 后期的分单的方式和比例以及排名问题的处理可以采用之前的可操作的部分代码来处理
         * com.ccservice.b2b2c.atom.taobao.thread.MyThreadTaoBaoOrderOfflineAdd.trainorderofflineadd(Trainorder)
         * 
         * com.ccservice.b2b2c.atom.taobao.thread.MyThreadTaoBaoOrderOfflineAdd.offlineTicketShunt(String, String)
         * 之前列出的逻辑，但是并未使用
         * 
         */
        if (listAgents.size() > 0) {//如果不存在，会分配到默认的出票点
            int max = listAgents.size();
            int min = 1;
            Random random = new Random();
            int s = random.nextInt(max) % (max - min + 1) + min;
            if (s > 0 && s <= listAgents.size()) {
                agentId = listAgents.get(s - 1) + "";
            }
        }
        WriteLog.write("途牛线下新版分配订单", "agentId=" + agentId + ";deliveryAddress=" + deliveryAddress);

        //agentId = "382";//测试环境的测试账号
        /*qicailvtu1
        123456*/

        //agentId="414";//正式环境的测试账号
        /*guozhengju123
        123456*/

        return agentId;
    }

    //配送到站的分单逻辑
    public static Integer distributionStation(String fromStationName) {
        Integer agentid = 1;//agentId="1"; 默认的话，直接分给 正式环境的订单未分配的账号 - 进行操作

        Integer agentidTemp = null;
        try {
            agentidTemp = trainOfflineTomAgentAddDao.findAgentIdByTrainStation(fromStationName);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        if (agentidTemp != null) {
            agentid = agentidTemp;
        }

        WriteLog.write("线下票配送到站分配订单", "agentid=" + agentid + ";fromStationName=" + fromStationName);

        return agentid;
    }

    //配送到家的抢票订单的分单逻辑
    public static Integer distributionGrabTicket() {
        Integer agentid = 1;//agentId="1"; 默认的话，直接分给 正式环境的订单未分配的账号 - 进行操作

        /**
         * 抢票的代售点的账号是有限的
         * 
         * 黄牛的账号 - 正式环境 - 分抢票单的逻辑 - 530
         * 
         * 其它抢票的代售点的账号 - 
         * 
         * 后期账号多的话，暂时借用邮寄分单逻辑的 - 随机分配
         */

        agentid = 530;//正式环境的黄牛的账号
        /*guozhengju123
        123456*/

        WriteLog.write("线下票配送到家的抢票订单的分配订单", "agentid=" + agentid);

        return agentid;
    }

    /**
     * @description: TODO - 闪送的分单逻辑 - 只有快递类型是闪送的时候，才会走入这个逻辑
     * 
     * @author liujun
     * @createTime: 2017年10月24日 上午10:58:18
     * @param OrderNumber
     * @param totalAddress - 完整的包含省市区县的地址信息
     * @param cityName - 邮寄地址中的城市的名称
     * @return
     */
    public static JSONObject distributionRapidSend(int random, String orderNumber, String totalAddress,
            String cityName) {
        JSONObject jsonObject = new JSONObject();
        String agentId = "414";// 正式环境的测试账号
        String sql = "SELECT ID,C_AGENTADDRESS FROM T_CUSTOMERAGENT_RAPIDSEND WHERE C_STATE = '0' AND C_AGENTCITYNAME = '"
                + cityName + "'";
        DataTable dataTable = new DataTable();
        try {
            dataTable = DBHelperOffline.GetDataTable(sql, null);
        }
        catch (Exception e) {
            // 记录异常日志
            ExceptionUtil.writelogByException("线下火车票_采购快递判断_闪送", e, String.valueOf(random));
        }
        Map<String, Double> map = new HashMap<>();
        // 支付金额
        for (DataRow dataRow : dataTable.GetRow()) {
            String id = dataRow.GetColumnString("ID");
            String fromAddress = dataRow.GetColumnString("C_AGENTADDRESS");
            JSONObject data = new JSONObject();// 请求参数
            cityName = cityName.endsWith("市") ? cityName : cityName + "市";
            data.put("cityName", cityName);// 订单所在城市名

            Boolean isSecondRapidSend = TrainOrderOfflineUtil.getIsSecondOrderRapidSend(orderNumber);//判断闪送单是否是二次订单 - 
            if (isSecondRapidSend) {
                data.put("orderNumber", orderNumber + random);// 订单ID
            }
            else {
                data.put("orderNumber", orderNumber);// 订单ID
            }

            data.put("toAddress", totalAddress);// 目的地
            data.put("fromAddress", fromAddress);// 代售点地址
            JSONObject responseJson = new JSONObject();
            try {
                responseJson = new UUptService().operate("线下火车票_采购快递判断_闪送", "uupt", random, "getOrderPrice", data,
                        responseJson);
            }
            catch (Exception e) {
                // 记录异常日志
                ExceptionUtil.writelogByException("线下火车票_采购快递判断_闪送", e, String.valueOf(random));
            }
            if (responseJson.getBooleanValue("success")) {
                JSONObject resultJson = JSONObject.parseObject(responseJson.getString("result"));
                map.put(id, resultJson.getDouble("payMoney"));
            }
        }
        // 记录日志
        WriteLog.write("线下火车票_采购快递判断_闪送", random + "----代售点ID和价格----" + map);
        // 根据各个代售点UU跑腿费用排序
        ArrayList<Map.Entry<String, Double>> entries = MapSortUtil.sortMap(map);
        List<String> listAgents = new ArrayList<String>();
        double payMoney = entries.get(0).getValue();
        for (int i = 0; i < entries.size(); i++) {
            if (entries.get(i).getValue() == payMoney) {
                listAgents.add(entries.get(i).getKey());
            }
        }
        // 目前走的是随机分配的逻辑，后期的分单的方式和比例以及排名问题的处理可以采用之前的可操作的部分代码来处理
        if (listAgents.size() > 0) {//如果不存在，会分配到默认的出票点
            int max = listAgents.size();
            int min = 1;
            Random randoms = new Random();
            int s = randoms.nextInt(max) % (max - min + 1) + min;
            if (s > 0 && s <= listAgents.size()) {
                agentId = listAgents.get(s - 1) + "";
            }
        }
        // 记录日志
        WriteLog.write("线下火车票_采购快递判断_闪送", random + "----随机分配的代售点ID----" + agentId);
        jsonObject.put("agentId", agentId);
        jsonObject.put("need_paymoney", map.get(agentId));
        return jsonObject;
    }

    /**
     * @description: TODO - 获取快递类型 //是 出发时间,格式:2017-08-24 13:15:00 - 此方法暂时弃用
     * 
     * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
     * @createTime: 2017年10月24日 上午10:53:06
     * @param totalAddress - 完整的包含省市区县的地址信息
     * @param startTime - 发车时间,格式:2017-08-24 13:15:00
     * @param agentId - 代售点的分单的地址信息 - 判定是否存在 - EMS的快递单号
     * @param cityName - 邮寄地址中的城市的名称
     * @return
     */
    /*public static Integer getDelieveTypeRapidSend(String totalAddress, String startTime, Integer agentId, String cityName) {
        Integer delieveType = 0;//0-顺丰 - 1-EMS 2-宅急送 3-京东 - 99配送到站 10-闪送【UU跑腿】
        if (cityName != null && !"".equals(cityName)) {
            
            delieveType = 0;
        } else {
            
            delieveType = 0;
        }
        return delieveType;
    }*/

    /**
     * @description: TODO - 获取快递类型 - 包含闪送 - 此方法修正为判定是否是闪送的快递类型
     * 
     * @author liujun
     * @createTime: 2017年10月24日 上午10:54:20
     * @param province
     * @param city - 邮寄地址中的城市的名称
     * @param county
     * @param address - 完整的包含省市区县的地址信息
     * @param departTime - 发车时间,格式:2017-08-24 13:15:00
     * @param agentId - 代售点的分单的地址信息 - 判定是否存在 - EMS的快递单号
     * @return
     */
    /*public static Integer getDelieveTypeRapidSend(String province, String city, String county, String address, String departTime, Integer agentId) {
        Integer delieveType = 0;//0-顺丰 - 1-EMS 2-宅急送 3-京东 - 99配送到站 10-闪送【UU跑腿】
        int random = new Random().nextInt(9000000) + 1000000;
        WriteLog.write("线下火车票_采购快递判断", random + "：" + "省：" + ":" + province + "----市：" + ":" + city + "----区：" + ":" + county);
        WriteLog.write("线下火车票_采购快递判断", random + "：" + "代售点ID：" + ":" + agentId + "----发车时间：" + ":" + departTime + "----收件地址：" + ":" + address);
        // 根据订单ID获取发车时间
        String nowDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        Double daysBetween = daysBetween(nowDate, departTime, random);
        // 默认发ems快递时间
        Double uuptdays = new Double(PropertyUtil.getValue("UUPT_express_mail_default_days", "train.properties"));
        // 如果发车时间少于等于默认发UU跑腿快递时间，而且是UU跑腿快递服务城市，发UU跑腿快递
        if (daysBetween.compareTo(uuptdays) <= 0) {
            String[] citys = PropertyUtil.getValue("rapidSend_city", "train.properties").split("/");
            for (int i = 0; i < citys.length; i++) {
                if (city.startsWith(citys[i])) {
                    delieveType = 10;
                    return delieveType;
                }
            }
        }
        // 判断热敏账号开通权限
        String sqlEMSAccount = "select EMSAccount from T_CUSTOMERAGENT with(nolock) where ID = '" + agentId + "'";
        DataTable dataTable = new DataTable();
        try {
            dataTable = DBHelperOffline.GetDataTable(sqlEMSAccount, null);
        } catch (Exception e1) {
            WriteLog.write("线下火车票_采购快递判断", random + "：" + "------查询热敏账号权限异常------" + sqlEMSAccount);
        }
        // 如果该代售点没有热敏账号，默认顺丰
        if (dataTable.GetRow().size() < 0) {
            return delieveType;
        }
        WriteLog.write("线下火车票_采购快递判断", random + "：" + "当前时间：" + ":" + nowDate + "发车时间：" + ":" + departTime + "发车时间与出票时间时差：" + ":" + daysBetween);
        // 默认发ems快递时间
        Double emsdays = new Double(PropertyUtil.getValue("EMS_express_mail_default_days", "train.properties"));
        // 默认发顺丰快递时间
        Double sfdays = new Double(PropertyUtil.getValue("SF_express_mail_default_days", "train.properties"));
        // 如果发车时间大于等于默认发ems快递时间，发ems快递
        if (daysBetween.compareTo(emsdays) >= 0) {
            delieveType = 1;
            return delieveType;
        }
        // 如果发车时间少于等于默认发顺丰快递时间，发顺丰快递
        if (daysBetween.compareTo(sfdays) <= 0) {
            return delieveType;
        }
        String[] provinces = PropertyUtil.getValue("default_ems_express_province", "train.properties").split("/");
        // 新疆/西藏/青海/广西/甘肃/内蒙古/云南/宁夏默认ems
        for (int i = 0; i < provinces.length; i++) {
            if (province.equals(provinces[i])) {
                delieveType = 1;
                return delieveType;
            }
        }
        // 三级地址为县/县级市
        if (county.endsWith("市") || county.endsWith("县") ) {
            //详细地址为街道，默认顺丰
            if (address.contains("街道")) {
                return delieveType;
            }
            // 详细地址为村乡镇，默认EMS
            if (address.contains("村") || address.contains("乡") || address.contains("镇")) {
                delieveType = 1;
                return delieveType;
            }
        } else {
            return delieveType;
        }
        return delieveType;
    }*/

    public static void main(String[] args) {

        isRapidSend("456789", "河南郑州市邓州市", "", "2017-10-31 10:30:00");
    }

    /**
     * @description: TODO - 获取快递类型 - 包含闪送 - 此方法修正为判定是否是闪送的快递类型
     * 
     * @author liujun
     * @createTime: 2017年10月24日 上午10:54:20
     * @param totalAddress - 全部邮寄地址
     * @param cityName - 邮寄地址中的城市的名称
     * @param departTime - 发车时间,格式:2017-08-24 13:15:00
     * @return
     */
    public static JSONObject isRapidSend(String orderNumber, String totalAddress, String cityName, String departTime) {
        JSONObject result = new JSONObject();
        result.put("isRapidSend", false);
        int random = new Random().nextInt(9000000) + 1000000;
        WriteLog.write("线下火车票_采购快递判断_闪送", random + "：" + "----发车时间：" + ":" + departTime + "----收件城市：" + ":" + cityName);
        if (cityName == null || "".equals(cityName)) {
            if (totalAddress.startsWith("北京") || totalAddress.startsWith("天津") || totalAddress.startsWith("上海")
                    || totalAddress.startsWith("重庆")) {
                cityName = totalAddress.substring(0, 2);
                if (!cityName.contains("市")) {
                    cityName += "市";
                }
            }
            else if (totalAddress.contains("省")) {
                cityName = totalAddress.substring(totalAddress.indexOf("省") + 1, totalAddress.indexOf("市") + 1);
                if (cityName.startsWith("-")) {
                    cityName = cityName.substring(1);
                }
            }
            else {
                String[] provinces = PropertyUtil.getValue("default_uupt_express_province", "train.properties")
                        .split("/");
                for (int i = 0; i < provinces.length; i++) {
                    if (totalAddress.contains(provinces[i])) {
                        totalAddress = totalAddress.replace(provinces[i], "");
                        if (totalAddress.startsWith("-")) {
                            totalAddress = totalAddress.substring(1);
                        }
                        cityName = totalAddress.substring(0, totalAddress.indexOf("市") + 1);
                        break;
                    }
                }
            }
        }
        // 判断闪送城市
        boolean isRapidSendCity = false;
        String[] citys = PropertyUtil.getValue("rapidSend_city", "train.properties").split("/");
        for (int i = 0; i < citys.length; i++) {
            if (cityName.contains(citys[i])) {
                isRapidSendCity = true;
                break;
            }
        }
        // 如果是闪送城市
        if (isRapidSendCity) {
            // 当前时间
            Date departTimeDate = null;
            try {
                departTimeDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(departTime);
            }
            catch (ParseException e) {
                WriteLog.write("线下火车票_采购快递判断_闪送", random + "：" + "日期格式转换错误：departTime = " + departTime);
                // 记录异常日志
                ExceptionUtil.writelogByException("线下火车票_采购快递判断_闪送", e, String.valueOf(random));
            }
            // 时间符合闪送条件
            if (isRapidSendDepartTime(departTimeDate)) {
                // 判断预算
                JSONObject jsonObject = distributionRapidSend(random, orderNumber, totalAddress, cityName);
                double need_paymoney = Double.valueOf(jsonObject.getString("need_paymoney"));
                cityName = cityName.endsWith("市") ? cityName.substring(0, 2) : cityName;
                // 根据闪送城市匹配最低预算
                double budget = Double.valueOf(PropertyUtil.getValue(cityName, "train.properties"));
                if (need_paymoney <= budget) {
                    String agentId = jsonObject.getString("agentId");
                    result.put("isRapidSend", true);
                    result.put("need_paymoney", need_paymoney);
                    result.put("agentId", agentId);
                }
            }
        }
        return result;
    }

    /**
     * @description: TODO - 
     * 
     * 原闪送单，在地址不变，只变日期的话，判定原先的日期是否符合闪送的条件
     * 
     * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
     * @createTime: 2017年11月4日 上午10:26:48
     * @param totalAddress
     * @param cityName
     * @param departTime
     * @return
     */
    public static Boolean isRapidSendTime(String totalAddress, String cityName, String departTime) {
        Boolean result = false;
        int random = new Random().nextInt(9000000) + 1000000;
        WriteLog.write("线下火车票_采购快递判断_闪送", random + "：" + "----发车时间：" + ":" + departTime + "----收件城市：" + ":" + cityName);
        if (cityName == null || "".equals(cityName)) {
            if (totalAddress.startsWith("北京") || totalAddress.startsWith("天津") || totalAddress.startsWith("上海")
                    || totalAddress.startsWith("重庆")) {
                cityName = totalAddress.substring(0, 2);
            }
            else if (totalAddress.contains("省")) {
                cityName = totalAddress.substring(totalAddress.indexOf("省") + 1, totalAddress.indexOf("市") + 1);
            }
            else {
                String[] provinces = PropertyUtil.getValue("default_uupt_express_province", "train.properties")
                        .split("/");
                for (int i = 0; i < provinces.length; i++) {
                    if (totalAddress.contains(provinces[i])) {
                        totalAddress = totalAddress.replace(provinces[i], "");
                        cityName = totalAddress.substring(0, totalAddress.indexOf("市") + 1);
                        break;
                    }
                }
            }
        }
        // 判断闪送城市
        boolean isRapidSendCity = false;
        String[] citys = PropertyUtil.getValue("rapidSend_city", "train.properties").split("/");
        for (int i = 0; i < citys.length; i++) {
            if (cityName.contains(citys[i])) {
                isRapidSendCity = true;
                break;
            }
        }
        // 如果是闪送城市
        if (isRapidSendCity) {
            // 当前时间
            Date departTimeDate = null;
            try {
                departTimeDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(departTime);
            }
            catch (ParseException e) {
                WriteLog.write("线下火车票_采购快递判断_闪送", random + "：" + "日期格式转换错误：departTime = " + departTime);
                // 记录异常日志
                ExceptionUtil.writelogByException("线下火车票_采购快递判断_闪送", e, String.valueOf(random));
            }
            // 时间符合闪送条件
            if (isRapidSendDepartTime(departTimeDate)) {
                result = true;
            }
        }
        return result;
    }

    /**
     * 根据发车时间判断是否符合闪送条件
     * 
     * @param departTimeDate
     * @return
     * @time 2017年10月27日 下午5:53:02
     * @author liujun
     */
    public static Boolean isRapidSendDepartTime(Date departTimeDate) {
        Boolean isRapidSend = false;
        // 下单时间
        Calendar nowDate_cal = Calendar.getInstance();
        // 发车时间
        Calendar departTimeDate_cal = Calendar.getInstance();
        Date nowDate = new Date();
        nowDate_cal.setTime(nowDate);
        departTimeDate_cal.setTime(departTimeDate);
        long nowDateTime = nowDate_cal.getTimeInMillis();
        long departTimeDateTime = departTimeDate_cal.getTimeInMillis();
        // 两天以内
        int uuptdays = Integer.valueOf(PropertyUtil.getValue("UUPT_express_mail_default_days", "train.properties"));
        if ((departTimeDateTime - nowDateTime) >= 0
                && (departTimeDateTime - nowDateTime) <= (1000 * 3600 * 24 * uuptdays)) {
            // 获取当前小时
            int nowDate_hour = nowDate_cal.get(Calendar.HOUR_OF_DAY);
            // 0点到7点，只接受两天以为当天12点以后的订单
            if (nowDate_hour >= 0 && nowDate_hour < 7) {
                nowDate_cal.set(Calendar.HOUR_OF_DAY, 12);
                nowDate_cal.set(Calendar.MINUTE, 0);
                nowDate_cal.set(Calendar.SECOND, 0);
                long uuptFreeTime = nowDate_cal.getTimeInMillis();
                if ((departTimeDateTime - uuptFreeTime) > 0) {
                    isRapidSend = true;
                }
                // 当天20点以后，只接受次日12点以后的订单
            }
            else if (nowDate_hour >= 20 && nowDate_hour < 23) {
                nowDate_cal.add(Calendar.DATE, 1);
                nowDate_cal.set(Calendar.HOUR_OF_DAY, 12);
                nowDate_cal.set(Calendar.MINUTE, 0);
                nowDate_cal.set(Calendar.SECOND, 0);
                long uuptFreeTime = nowDate_cal.getTimeInMillis();
                if ((departTimeDateTime - uuptFreeTime) > 0) {
                    isRapidSend = true;
                }
            }
            else {
                // 大于发车时间5小时的
                int uuptpeisonghour = Integer
                        .valueOf(PropertyUtil.getValue("UUPT_express_peisong_time", "train.properties"));
                if ((departTimeDateTime - nowDateTime) >= (1000 * 3600 * uuptpeisonghour)) {
                    isRapidSend = true;
                }
            }
        }
        return isRapidSend;
    }

    //判断闪送单是否是二次订单 - 只有 = 2 的时候是二次操作订单
    public static Boolean getIsSecondCancelRapidSend(String orderNumber) {
        Boolean isSecondRapidSend = false;
        Integer rapidSendCOUNT = 0;

        String sql = "select COUNT(0) AS num from uupt_orderprice where orderNumber = '" + orderNumber + "'";
        DataTable dataTable = new DataTable();
        try {
            dataTable = DBHelperOffline.GetDataTable(sql, null);
        }
        catch (Exception e) {
            // 记录异常日志
            ExceptionTCUtil.handleTCException(e);
            WriteLog.write("线下火车票_UU跑腿回调路由信息", "订单查询失败");
        }
        // 取到数据
        if (dataTable.GetRow().size() > 0) {
            rapidSendCOUNT = Integer.valueOf(String.valueOf(dataTable.GetRow().get(0).GetColumn("num").GetValue()));
        }

        if (rapidSendCOUNT == 2) {//二次取消
            isSecondRapidSend = true;
        }

        return isSecondRapidSend;
    }

    //与二次取消相比，二次下单差1
    public static Boolean getIsSecondOrderRapidSend(String orderNumber) {
        Boolean isSecondRapidSend = false;
        Integer rapidSendCOUNT = 0;

        String sql = "select COUNT(0) AS num from uupt_orderprice where orderNumber = '" + orderNumber + "'";
        DataTable dataTable = new DataTable();
        try {
            dataTable = DBHelperOffline.GetDataTable(sql, null);
        }
        catch (Exception e) {
            // 记录异常日志
            ExceptionTCUtil.handleTCException(e);
            WriteLog.write("线下火车票_UU跑腿回调路由信息", "订单查询失败");
        }
        // 取到数据
        if (dataTable.GetRow().size() > 0) {
            rapidSendCOUNT = Integer.valueOf(String.valueOf(dataTable.GetRow().get(0).GetColumn("num").GetValue()));
        }

        if (rapidSendCOUNT > 0) {//二次下单
            isSecondRapidSend = true;
        }

        return isSecondRapidSend;
    }

    /**
     * @description: TODO - 获取快递类型 - 不包含闪送
     * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
     * @createTime: 2017年10月24日 上午11:55:24
     * @param province
     * @param city
     * @param county
     * @param address - 此处需要传递的是不包含省市区的后续的地址的信息
     * @param departTime
     * @param agentId
     * @return
     */
    public static Integer getDelieveType(String province, String city, String county, String address, String departTime,
            Integer agentId) {
        Integer delieveType = 0;// 0-顺丰 - 1-EMS 2-宅急送 3-京东 - 99配送到站 10-闪送【UU跑腿】
        int random = new Random().nextInt(9000000) + 1000000;
        WriteLog.write("线下火车票_采购快递判断", random + "：" + "省：" + ":" + province + "----市：" + ":" + city + "----县区：" + ":"
                + county + "----乡/镇/街道：" + ":" + address + "代售点ID：" + ":" + agentId + "----发车时间：" + ":" + departTime);
        // 判断热敏账号开通权限
        String sqlEMSAccount = "select EMSAccount from T_CUSTOMERAGENT with(nolock) where ID = '" + agentId + "'";
        DataTable dataTable = new DataTable();
        try {
            dataTable = DBHelperOffline.GetDataTable(sqlEMSAccount, null);
        }
        catch (Exception e) {
            // 记录异常日志
            ExceptionUtil.writelogByException("线下火车票_采购快递判断", e, String.valueOf(random));
        }
        // 如果该代售点没有热敏账号，默认顺丰
        String emsAccount = String.valueOf(dataTable.GetRow().get(0).GetColumn("EMSAccount").GetValue());
        if (emsAccount == null || "".equals(emsAccount)) {
            return delieveType;
        }
        // 根据订单ID获取发车时间
        String nowDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        Double daysBetween = daysBetween(nowDate, departTime, random);
        WriteLog.write("线下火车票_采购快递判断", random + "：" + "当前时间：" + ":" + nowDate + "发车时间：" + ":" + departTime
                + "发车时间与出票时间时差：" + ":" + daysBetween);
        // 默认发ems快递时间
        Double emsdays = new Double(PropertyUtil.getValue("EMS_express_mail_default_days", "train.properties"));
        // 默认发顺丰快递时间
        Double sfdays = new Double(PropertyUtil.getValue("SF_express_mail_default_days", "train.properties"));
        // 如果发车时间大于等于默认发ems快递时间，发ems快递
        if (daysBetween.compareTo(emsdays) >= 0) {
            WriteLog.write("线下火车票_采购快递判断", random + "：" + "15天以外默认ems");
            delieveType = 1;
            return delieveType;
        }
        // 如果发车时间少于等于默认发顺丰快递时间，发顺丰快递
        if (daysBetween.compareTo(sfdays) <= 0) {
            WriteLog.write("线下火车票_采购快递判断", random + "：" + "5天以内默认顺丰");
            return delieveType;
        }
        // 判断偏远地区
        boolean isRemoteProvince = false;
        String[] remoteProvinces = PropertyUtil.getValue("default_ems_express_province", "train.properties").split("/");
        for (int i = 0; i < remoteProvinces.length; i++) {
            if (province.contains(remoteProvinces[i])) {
                isRemoteProvince = true;
                break;
            }
        }
        // 偏远省份（新疆/西藏/青海/广西/甘肃/内蒙古/云南/宁夏）的使用EMS
        if (isRemoteProvince) {
            WriteLog.write("线下火车票_采购快递判断", random + "：" + "偏远地区默认ems");
            delieveType = 1;
            return delieveType;
        }
        else {
            // 三级地址为县/县级市
            if (county.endsWith("市") || county.endsWith("县")) {
                // 详细地址为村乡镇，默认EMS
                if (address.contains("村") || address.contains("乡") || address.contains("镇")) {
                    WriteLog.write("线下火车票_采购快递判断", random + "：" + "县级市乡村镇默认ems");
                    delieveType = 1;
                    return delieveType;
                }
                else {
                    try {
                        String town = "";
                        if (address.contains("街道")) {
                            town = address.substring(0, address.indexOf("街道") + 2);
                        }
                        String sql = "SELECT isCanMail from SFExpressCityCode where province = '" + province
                                + "' and city = '" + city + "' and county = '" + county + "' and town = '" + town + "'";
                        DataTable sfPeisong = DBHelperOffline.GetDataTable(sql, null);
                        // 顺丰数据库，地址不存在， 默认ems
                        if (sfPeisong == null || sfPeisong.GetRow().size() == 0) {
                            WriteLog.write("线下火车票_采购快递判断", random + "：" + "顺丰数据库地址找不到默认ems");
                            delieveType = 1;
                            return delieveType;
                        }
                        else {
                            String isCanMail = sfPeisong.GetRow().get(0).GetColumnString("isCanMail");
                            // 顺丰不可以配送
                            if (!"1".equals(isCanMail)) {
                                WriteLog.write("线下火车票_采购快递判断", random + "：" + "顺丰配送不到默认ems");
                                delieveType = 1;
                                return delieveType;
                            }
                        }
                    }
                    catch (Exception e) {
                        // 记录异常日志
                        ExceptionUtil.writelogByException("线下火车票_采购快递判断", e, String.valueOf(random));
                    }
                }
            }
        }
        return delieveType;
    }

    /**
     * @description: TODO - 获取快递类型 - 不包含闪送
     * 
     * @author liujun
     * @createTime: 2017年10月24日 上午10:57:40
     * @param totaladdress - 完整的包含省市区县的地址信息
     * @param departTime - 发车时间,格式:2017-08-24 13:15:00
     * @param agentId - 代售点的分单的地址信息 - 判定是否存在 - EMS的快递单号
     * @return
     */
    public static Integer getDelieveTypeTotalAddress(String totaladdress, String departTime, Integer agentId) {
        Integer delieveType = 0;// 0-顺丰 - 1-EMS 2-宅急送 3-京东 - 99配送到站 10-闪送【UU跑腿】
        int random = new Random().nextInt(9000000) + 1000000;
        JSONObject addressJson = splitAddress(totaladdress, random);
        String province = addressJson.getString("province");
        String city = addressJson.getString("city");
        String county = addressJson.getString("county");
        String town = addressJson.getString("town");
        String address = addressJson.getString("address");
        WriteLog.write("线下火车票_采购快递判断",
                random + "：" + "省：" + ":" + province + "----市：" + ":" + city + "----县区：" + ":" + county + "----乡/镇/街道："
                        + ":" + town + "----" + address + "代售点ID：" + ":" + agentId + "----发车时间：" + ":" + departTime);
        // 判断热敏账号开通权限
        String sqlEMSAccount = "select EMSAccount from T_CUSTOMERAGENT with(nolock) where ID = '" + agentId + "'";
        DataTable dataTable = new DataTable();
        try {
            dataTable = DBHelperOffline.GetDataTable(sqlEMSAccount, null);
        }
        catch (Exception e) {
            // 记录异常日志
            ExceptionUtil.writelogByException("线下火车票_采购快递判断", e, String.valueOf(random));
        }
        // 如果该代售点没有热敏账号，默认顺丰
        if (dataTable.GetRow().size() < 0) {
            return delieveType;
        }
        // 根据订单ID获取发车时间
        String nowDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        Double daysBetween = daysBetween(nowDate, departTime, random);
        WriteLog.write("线下火车票_采购快递判断", random + "：" + "当前时间：" + ":" + nowDate + "发车时间：" + ":" + departTime
                + "发车时间与出票时间时差：" + ":" + daysBetween);
        // 默认发ems快递时间
        Double emsdays = new Double(PropertyUtil.getValue("EMS_express_mail_default_days", "train.properties"));
        // 默认发顺丰快递时间
        Double sfdays = new Double(PropertyUtil.getValue("SF_express_mail_default_days", "train.properties"));
        // 如果发车时间大于等于默认发ems快递时间，发ems快递
        if (daysBetween.compareTo(emsdays) >= 0) {
            delieveType = 1;
            return delieveType;
        }
        // 如果发车时间少于等于默认发顺丰快递时间，发顺丰快递
        if (daysBetween.compareTo(sfdays) <= 0) {
            return delieveType;
        }
        // 判断偏远地区
        boolean isRemoteProvince = false;
        String[] remoteProvinces = PropertyUtil.getValue("default_ems_express_province", "train.properties").split("/");
        for (int i = 0; i < remoteProvinces.length; i++) {
            if (province.contains(remoteProvinces[i])) {
                isRemoteProvince = true;
                break;
            }
        }
        // 偏远省份（新疆/西藏/青海/广西/甘肃/内蒙古/云南/宁夏）的使用EMS
        if (isRemoteProvince) {
            delieveType = 1;
            return delieveType;
        }
        else {
            // 三级地址为县/县级市
            if (county.endsWith("市") || county.endsWith("县")) {
                // 详细地址为村乡镇，默认EMS
                if (address.contains("村") || address.contains("乡") || address.contains("镇")) {
                    delieveType = 1;
                    return delieveType;
                }
                else {
                    try {
                        String sql = "SELECT isCanMail from SFExpressCityCode where province = '" + province
                                + "' and city = '" + city + "' and county = '" + county + "' and town = '" + town + "'";
                        DataTable sfPeisong = DBHelperOffline.GetDataTable(sql, null);
                        // 顺丰数据库，地址不存在， 默认ems
                        if (sfPeisong == null || sfPeisong.GetRow().size() == 0) {
                            delieveType = 1;
                            return delieveType;
                        }
                        else {
                            String isCanMail = sfPeisong.GetRow().get(0).GetColumnString("isCanMail");
                            // 顺丰不可以配送
                            if (!"1".equals(isCanMail)) {
                                delieveType = 1;
                                return delieveType;
                            }
                        }
                    }
                    catch (Exception e) {
                        // 记录异常日志
                        ExceptionUtil.writelogByException("线下火车票_采购快递判断", e, String.valueOf(random));
                    }
                }
            }
        }
        return delieveType;
    }

    /**
     * 根据地址判断发送快递
     * 
     * @param address 邮寄地址
     * @return 发送快递
     * @time 2017年10月13日 上午10:12:18
     * @author liujun
     */
    private static JSONObject splitAddress(String totaladdress, int random) {
        JSONObject result = new JSONObject();
        String province = "";
        String city = "";
        String county = "";
        String town = "";
        String address = "";
        try {
            totaladdress = totaladdress.replaceAll("-", "");
            address = totaladdress;
            if (totaladdress.startsWith("北京") || totaladdress.startsWith("天津") || totaladdress.startsWith("上海")
                    || totaladdress.startsWith("重庆")) {
                province = totaladdress.substring(0, 2);
                if (totaladdress.indexOf("市") == 4) {
                    city = totaladdress.substring(2, totaladdress.indexOf("市") + 1);
                    totaladdress = totaladdress.substring((province + city).length(), totaladdress.length());
                }
                else {
                    city = totaladdress.substring(3, totaladdress.indexOf("市", totaladdress.indexOf("市") + 1) + 1);
                    totaladdress = totaladdress.substring((province + "市" + city).length(), totaladdress.length());
                }
            }
            else {
                if (totaladdress.startsWith("新疆") || totaladdress.startsWith("西藏") || totaladdress.startsWith("广西")
                        || totaladdress.startsWith("内蒙古") || totaladdress.startsWith("宁夏 ")) {
                    province = totaladdress.substring(0, totaladdress.indexOf("自治区") + 3);
                }
                else if (totaladdress.contains("省")) {
                    province = totaladdress.substring(0, totaladdress.indexOf("省") + 1);
                }
                totaladdress = totaladdress.substring((province).length(), totaladdress.length());
                // 二级地址
                if (totaladdress.indexOf("自治州") > 0) {
                    city = totaladdress.substring(0, totaladdress.indexOf("自治州") + 3);
                }
                if (totaladdress.indexOf("盟") > 0) {
                    if (city.length() == 0) {
                        city = totaladdress.substring(0, totaladdress.indexOf("盟") + 1);
                    }
                    else {
                        if (totaladdress.substring(0, totaladdress.indexOf("盟") + 1).length() < city.length()) {
                            city = totaladdress.substring(0, totaladdress.indexOf("盟") + 1);
                        }
                    }
                }
                if (totaladdress.indexOf("区") > 0) {
                    if (city.length() == 0) {
                        city = totaladdress.substring(0, totaladdress.indexOf("区") + 1);
                    }
                    else {
                        if (totaladdress.substring(0, totaladdress.indexOf("区") + 1).length() < city.length()) {
                            city = totaladdress.substring(0, totaladdress.indexOf("区") + 1);
                        }
                    }
                }
                if (totaladdress.indexOf("市") > 0) {
                    if (city.length() == 0) {
                        city = totaladdress.substring(0, totaladdress.indexOf("市") + 1);
                    }
                    else {
                        if (totaladdress.substring(0, totaladdress.indexOf("市") + 1).length() < city.length()) {
                            city = totaladdress.substring(0, totaladdress.indexOf("市") + 1);
                        }
                    }
                }
                totaladdress = totaladdress.substring((city).length(), totaladdress.length());
            }
            WriteLog.write("线下火车票_采购快递判断", random + "：" + "省：" + ":" + province + "----市：" + ":" + city);
            // 三级地址
            if (totaladdress.indexOf("区") > 0) {
                county = totaladdress.substring(0, totaladdress.indexOf("区") + 1);
            }
            if (totaladdress.indexOf("市") > 0) {
                if (county.length() == 0) {
                    county = totaladdress.substring(0, totaladdress.indexOf("市") + 1);
                }
                else {
                    if (totaladdress.substring(0, totaladdress.indexOf("市") + 1).length() < county.length()) {
                        county = totaladdress.substring(0, totaladdress.indexOf("市") + 1);
                    }
                }
            }
            if (totaladdress.indexOf("县") > 0) {
                if (county.length() == 0) {
                    county = totaladdress.substring(0, totaladdress.indexOf("县") + 1);
                }
                else {
                    if (totaladdress.substring(0, totaladdress.indexOf("县") + 1).length() < county.length()) {
                        county = totaladdress.substring(0, totaladdress.indexOf("县") + 1);
                    }
                }
            }
            if (totaladdress.indexOf("旗") > 0) {
                if (county.length() == 0) {
                    county = totaladdress.substring(0, totaladdress.indexOf("旗") + 1);
                }
                else {
                    if (totaladdress.substring(0, totaladdress.indexOf("旗") + 1).length() < county.length()) {
                        county = totaladdress.substring(0, totaladdress.indexOf("旗") + 1);
                    }
                }
            }
            WriteLog.write("线下火车票_采购快递判断", random + "：" + "区县：" + ":" + county);
            totaladdress = totaladdress.substring((county).length(), totaladdress.length());
            if (totaladdress.indexOf("街道") > 0) {
                town = totaladdress.substring(0, totaladdress.indexOf("街道") + 2);
            }
            if (totaladdress.indexOf("镇") > 0) {
                if (county.length() == 0) {
                    town = totaladdress.substring(0, totaladdress.indexOf("镇") + 1);
                }
                else {
                    if (totaladdress.substring(0, totaladdress.indexOf("镇") + 1).length() < town.length()) {
                        town = totaladdress.substring(0, totaladdress.indexOf("镇") + 1);
                    }
                }
            }
            if (totaladdress.indexOf("乡") > 0) {
                if (town.length() == 0) {
                    town = totaladdress.substring(0, totaladdress.indexOf("乡") + 1);
                }
                else {
                    if (totaladdress.substring(0, totaladdress.indexOf("乡") + 1).length() < town.length()) {
                        town = totaladdress.substring(0, totaladdress.indexOf("乡") + 1);
                    }
                }
            }

            WriteLog.write("线下火车票_采购快递判断", random + "：" + "乡镇街道：" + ":" + town);
        }
        catch (Exception e) {
            // 记录异常日志
            ExceptionUtil.writelogByException("线下火车票_采购快递判断", e, String.valueOf(random));
        }
        result.put("province", province);
        result.put("city", city);
        result.put("county", county);
        result.put("town", town);
        result.put("address", address.replace(province + city + county + town, ""));
        return result;
    }

    /**
     * 日期天数差计算
     * 
     * @param startdate 开始日期
     * @param enddate 结束日期
     * @return
     * @time 2017年10月12日 下午3:12:43
     * @author liujun
     */
    private static Double daysBetween(String startdate, String enddate, int random) {
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        try {
            cal1.setTime(sdf1.parse(startdate));
            if (cal1.get(Calendar.HOUR_OF_DAY) < 12) {
                cal1.setTime(sdf2.parse(startdate));
                cal1.add(Calendar.HOUR_OF_DAY, 12);
            }
            else {
                cal1.setTime(sdf2.parse(startdate));
                cal1.add(Calendar.DATE, 1);
            }
            cal2.setTime(sdf1.parse(enddate));
            if (cal2.get(Calendar.HOUR_OF_DAY) < 12) {
                cal2.setTime(sdf2.parse(enddate));
            }
            else {
                cal2.setTime(sdf2.parse(enddate));
                cal2.add(Calendar.HOUR_OF_DAY, 12);
            }
        }
        catch (ParseException e) {
            WriteLog.write("线下火车票_采购快递判断", random + "：" + "日期格式转换错误：" + ":" + startdate + "→" + enddate);
            // 记录异常日志
            ExceptionUtil.writelogByException("线下火车票_采购快递判断", e, String.valueOf(random));
            return new Double(0);
        }
        BigDecimal time1 = new BigDecimal(cal1.getTimeInMillis());
        BigDecimal time2 = new BigDecimal(cal2.getTimeInMillis());
        BigDecimal between_days = (time2.subtract(time1)).divide(new BigDecimal(1000 * 3600 * 24));
        return Double.valueOf(between_days.toString());
    }

    //获取顺丰快递时效
    public static String getDelieveStr(Long orderIdl, Integer agentId, String address) {
        String results = "暂无快递信息!";

        //出发城市和到达城市的编码都在这个表里面 - TrainOfflineExpressCode1

        String tocode = "";
        try {
            tocode = trainOfflineExpressCodeDao.getCityCodeByAddress(address);
        }
        catch (Exception e) {
            return ExceptionTNUtil.handleTNException(e).toJSONString();
        }

        //记录请求信息日志
        WriteLog.write("线下票获取快递时效", "tocode--->" + tocode);

        if (tocode == null) {
            String content = "无法查询获取到对应的城市信息";
            TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, agentId, content);
            return results;
        }

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String dates = sdf.format(new Date());

        TrainOrderAgentTimes trainOrderAgentTimes = null;
        try {
            trainOrderAgentTimes = trainOrderAgentTimesDao.findTrainOrderAgentTimesByAgentId(String.valueOf(agentId));
        }
        catch (Exception e1) {
            return ExceptionTNUtil.handleTNException(e1).toJSONString();
        }

        if (trainOrderAgentTimes == null) {
            String content = "无法查询获取到对应代售点的快递时效的信息";
            TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, agentId, content);
            return results;
            //throw new RuntimeException("");
        }

        String fromcode = "010";
        String time1 = "10:00:00";
        String time2 = "18:00:00";

        fromcode = trainOrderAgentTimes.getFromcode();
        time1 = trainOrderAgentTimes.getTime1();
        time2 = trainOrderAgentTimes.getTime2();

        String realTime = getRealTimes(dates, time1, time2);

        //来自淘宝的 cn_interface 中的相关配置文件
        String urlString = PropertyUtil.getValue("expressDeliverUrl", "Train.GuestAccount.properties");

        String param = "times=" + realTime + "&fromcode=" + fromcode + "&tocode=" + tocode;

        //记录请求信息日志
        WriteLog.write("线下票获取快递时效", "getDelieveStr--->TrainOrderOfflineOffline_保存快递时效信息" + "agengId=" + agentId
                + "------->" + "address=" + address + "---------->" + urlString + "?" + param);

        String result = SendPostandGet.submitPost(urlString, param, "UTF-8").toString();

        //记录请求信息日志
        WriteLog.write("线下票获取快递时效", "getDelieveStr--->result" + "agengId=" + result);

        if (result.contains("OK") && result.contains("deliver_time")) {
            try {
                Document document = DocumentHelper.parseText(result);
                Element root = document.getRootElement();
                if (root != null) {
                    Element head = root.element("Head");
                    Element body = root.element("Body");
                    if ("OK".equals(root.elementText("Head"))) {
                        Element deliverTmResponse = body.element("DeliverTmResponse");
                        Element deliverTm = deliverTmResponse.element("DeliverTm");
                        String business_type_desc = deliverTm.attributeValue("business_type_desc");
                        String deliver_time = deliverTm.attributeValue("deliver_time");
                        String business_type = deliverTm.attributeValue("business_type");
                        results = "如果" + realTime + "正常发件。快递类型为:" + business_type_desc + "。快递预计到达时间:" + deliver_time
                                + "。以上时效为预计到达时间，仅供参考，精准时效以运单追踪结果中的“预计到达时间”为准。";
                    }
                }
            }
            catch (Exception e) {
                ExceptionTNUtil.handleTNException(e);
                return results;
            }
        }

        //记录请求信息日志
        WriteLog.write("线下票获取快递时效", "getDelieveStr--->results" + result + "agengId=" + agentId);

        return results;
    }

    /*public static String getDelieveStr(Long orderIdl, Integer agentId, String address) {
        
    }*/

    //同城获取顺丰快递时效
    public static String getDelieveStr2(Integer agentId, String address) {
        String results = ""; //暂无快递信息!

        String tocode = "";
        try {
            tocode = trainOfflineExpressCodeDao.getCityCodeByAddress(address);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        //记录请求信息日志
        WriteLog.write("同程线下票获取快递时效", "tocode--->" + tocode);

        if (tocode == null) {
            String content = "无法查询获取到对应的城市信息";
            //            TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, agentId, content);
            return results;
        }

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String dates = sdf.format(new Date());

        TrainOrderAgentTimes trainOrderAgentTimes = null;
        try {
            trainOrderAgentTimes = trainOrderAgentTimesDao.findTrainOrderAgentTimesByAgentId(String.valueOf(agentId));
        }
        catch (Exception e1) {
            return ExceptionTNUtil.handleTNException(e1).toJSONString();
        }

        if (trainOrderAgentTimes == null) {
            String content = "无法查询获取到对应代售点的快递时效的信息";
            //            TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, agentId, content);
            return results;
            //throw new RuntimeException("");
        }

        String fromcode = "010";
        String time1 = "10:00:00";
        String time2 = "18:00:00";

        fromcode = trainOrderAgentTimes.getFromcode();
        time1 = trainOrderAgentTimes.getTime1();
        time2 = trainOrderAgentTimes.getTime2();

        String realTime = getRealTimes(dates, time1, time2);

        //来自淘宝的 cn_interface 中的相关配置文件
        String urlString = PropertyUtil.getValue("expressDeliverUrl", "Train.GuestAccount.properties");

        String param = "times=" + realTime + "&fromcode=" + fromcode + "&tocode=" + tocode;

        //记录请求信息日志
        WriteLog.write("同程线下票获取快递时效", "getDelieveStr--->TrainOrderOfflineOffline_保存快递时效信息" + "agengId=" + agentId
                + "------->" + "address=" + address + "---------->" + urlString + "?" + param);

        String result = SendPostandGet.submitPost(urlString, param, "UTF-8").toString();

        //记录请求信息日志
        WriteLog.write("同程线下票获取快递时效", "getDelieveStr--->result" + "agengId=" + result);

        if (result.contains("OK") && result.contains("deliver_time")) {
            try {
                Document document = DocumentHelper.parseText(result);
                Element root = document.getRootElement();
                if (root != null) {
                    Element head = root.element("Head");
                    Element body = root.element("Body");
                    if ("OK".equals(root.elementText("Head"))) {
                        Element deliverTmResponse = body.element("DeliverTmResponse");
                        Element deliverTm = deliverTmResponse.element("DeliverTm");
                        String business_type_desc = deliverTm.attributeValue("business_type_desc");
                        String deliver_time = deliverTm.attributeValue("deliver_time");
                        String business_type = deliverTm.attributeValue("business_type");
                        //                        results = "如果" + realTime + "正常发件。快递类型为:" + business_type_desc + "。快递预计到达时间:" + deliver_time
                        //                                + "。以上时效为预计到达时间，仅供参考，精准时效以运单追踪结果中的“预计到达时间”为准。";
                        results = deliver_time;

                        //如果2017-09-06 18:00:00正常发件。快递类型为:标准快递。快递预计到达时间:2017-09-07 18:00:00,2017-09-07 18:00:00。以上时效为预计到达时间，仅供参考，精准时效以运单追踪结果中的“预计到达时间”为准。
                    }
                }
            }
            catch (Exception e) {
                ExceptionTNUtil.handleTNException(e);
                return results;
            }
        }

        //记录请求信息日志
        WriteLog.write("同程线下票获取快递时效", "getDelieveStr--->result" + result);
        WriteLog.write("同程线下票获取快递时效", "getDelieveStr--->results" + results);

        return results;
    }

    //获取顺丰快递时效
    public static String getDelieveStr2GT(Integer agentId, String address) {
        String results = "暂无快递信息!";

        //出发城市和到达城市的编码都在这个表里面 - TrainOfflineExpressCode1

        String tocode = "";
        try {
            tocode = trainOfflineExpressCodeDao.getCityCodeByAddress(address);
        }
        catch (Exception e) {
            return ExceptionTNUtil.handleTNException(e).toJSONString();
        }

        //记录请求信息日志
        WriteLog.write("线下票获取快递时效", "tocode--->" + tocode);

        if (tocode == null) {
            String content = "无法查询获取到对应的城市信息";
            return results;
        }

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String dates = sdf.format(new Date());

        TrainOrderAgentTimes trainOrderAgentTimes = null;
        try {
            trainOrderAgentTimes = trainOrderAgentTimesDao.findTrainOrderAgentTimesByAgentId(String.valueOf(agentId));
        }
        catch (Exception e1) {
            return ExceptionTNUtil.handleTNException(e1).toJSONString();
        }

        if (trainOrderAgentTimes == null) {
            String content = "无法查询获取到对应代售点的快递时效的信息";
            return results;
            //throw new RuntimeException("");
        }

        String fromcode = "010";
        String time1 = "10:00:00";
        String time2 = "18:00:00";

        fromcode = trainOrderAgentTimes.getFromcode();
        time1 = trainOrderAgentTimes.getTime1();
        time2 = trainOrderAgentTimes.getTime2();

        String realTime = getRealTimes(dates, time1, time2);

        //来自淘宝的 cn_interface 中的相关配置文件
        String urlString = PropertyUtil.getValue("expressDeliverUrl", "Train.GuestAccount.properties");

        String param = "times=" + realTime + "&fromcode=" + fromcode + "&tocode=" + tocode;

        //记录请求信息日志
        WriteLog.write("线下票获取快递时效", "getDelieveStr--->TrainOrderOfflineOffline_保存快递时效信息" + "agengId=" + agentId
                + "------->" + "address=" + address + "---------->" + urlString + "?" + param);

        String result = SendPostandGet.submitPost(urlString, param, "UTF-8").toString();

        //记录请求信息日志
        WriteLog.write("线下票获取快递时效", "getDelieveStr--->result" + "agengId=" + result);

        String totime = "";

        if (result.contains("OK") && result.contains("deliver_time")) {
            try {
                Document document = DocumentHelper.parseText(result);
                Element root = document.getRootElement();
                if (root != null) {
                    Element head = root.element("Head");
                    Element body = root.element("Body");

                    if ("OK".equals(root.elementText("Head"))) {
                        Element deliverTmResponse = body.element("DeliverTmResponse");
                        Element deliverTm = deliverTmResponse.element("DeliverTm");
                        String business_type_desc = deliverTm.attributeValue("business_type_desc");
                        String deliver_time = deliverTm.attributeValue("deliver_time");
                        String business_type = deliverTm.attributeValue("business_type");

                        totime = deliver_time;

                        results = "配送费用￥20," + realTime + "前完成支付," + deliver_time.split(",")[0] + "前送到！";

                        //                        results="如果"+realTime+"正常发件。快递类型为:"+business_type_desc+"。快递预计到达时间:"+deliver_time+"。以上时效为预计到达时间，仅供参考，精准时效以运单追踪结果中的“预计到达时间”为准。";
                    } /*else if("OK".equals(root.elementText("Head")) && !result.contains("deliver_time")){
                      //                    results="如果"+realTime+"正常发件。快递类型为:顺丰次日。快递预计到达时间:"+getNextDay(realTime.substring(0, 10),-2)+" "+realTime.substring(11, 19)+","+getNextDay(realTime.substring(0, 10),-2)+" "+realTime.substring(11, 19)+"。以上时效为预计到达时间，仅供参考，精准时效以运单追踪结果中的“预计到达时间”为准。";
                         if(Integer.parseInt(realTime.substring(11, 12))>12){
                      //                        results = "配送费用￥20," + realTime + "前完成支付," + getNextDay(realTime.substring(0, 10),-2)+" 18:00:00,"+getNextDay(realTime.substring(0, 10),-2)+" 18:00:00前送到！";
                             results = "配送费用￥20," + realTime + "前完成支付," +getNextDay(realTime.substring(0, 10),-2)+" 18:00:00前送到！";
                         }else{
                      //                        results = "配送费用￥20," + realTime + "前完成支付," + getNextDay(realTime.substring(0, 10),-2)+" 12:00:00,"+getNextDay(realTime.substring(0, 10),-2)+" 12:00:00前送到！";
                             results = "配送费用￥20," + realTime + "前完成支付," +getNextDay(realTime.substring(0, 10),-2)+" 18:00:00前送到！";
                         }
                      }
                      else{
                         results="获取快递时间失败！请上官网核验快递送达时间。";
                      }*/
                }
            }
            catch (Exception e) {
                ExceptionTNUtil.handleTNException(e);
                return results;
            }
        }

        //记录请求信息日志
        WriteLog.write("线下票获取快递时效", "getDelieveStr--->results" + result + "agengId=" + agentId);

        JSONObject jbb = new JSONObject();
        jbb.put("results", results);
        jbb.put("realTime", realTime);
        jbb.put("totime", totime);

        //记录请求信息日志
        WriteLog.write("线下票获取快递时效", "getGTDelieveStr--->results" + jbb.toJSONString());

        return results;
    }

    /**
     * 获取取快递时间
     * @param dates
     * @param time1
     * @param time2
     * @return
     */
    public static String getRealTimes(String dates, String time1, String time2) {
        String result = "";
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String realDates = sdf1.format(new Date());
        try {
            Date date0 = sdf.parse(dates);
            Date date1 = sdf.parse(time1);
            Date date2 = sdf.parse(time2);
            if (date0.before(date1)) {
                result = (realDates.substring(0, 10) + " " + sdf.format(date1));
            }
            else if (date0.after(date1) && date0.before(date2)) {
                result = (realDates.substring(0, 10) + " " + sdf.format(date2));
            }
            else if (date0.after(date2)) {
                Date ds = getDate(new Date());
                String nextd = sdf1.format(ds);
                result = (nextd.substring(0, 10) + " " + sdf.format(date1));
            }
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Date getDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        Date date1 = new Date(calendar.getTimeInMillis());
        return date1;
    }

    public static String ungetDelieveTime(String totalAddress, String agentid, String LOGNAME, Integer random) {
        /**
         * 0 - 警示：该地区不能送达，请选择其他快递。
         * 1 - 接口访问异常 - 获取快递时间失败！请上官网核验快递送达时间。
         * 2 - 反馈的结果的XML解析失败 - 获取快递时间失败！请上官网核验快递送达时间。
         * 
         * 以上三种情况需要进行人为判定选择别的快递，还是留待拒单操作
         * 
         * 
         * 目前新增逻辑判断
         * 
         * 顺丰不能送达 - 尝试走EMS【后期出单需人工判定】 - 
         * 
         * 按照48小时进行判定【系统判定】 - 二期 - 尝试走EMS快递时效
         * 
         * 
         * 接口访问异常 - 走公式逻辑
         * 
         * 代售点匹配之后，匹配省内还是省外 - 省内算两天，省外算四天
         * 
         **/
        String arriveTime = "";

        Boolean isInProvince = TrainOrderOfflineUtil.getIsInProvince(agentid, totalAddress, LOGNAME, random);//true-省内 - false-省外

        /**
         * 1;//当天上午件
         * 2;//当天下午件
         * 3;//第二天的上午件
         */
        Integer isMailPMOrNextAM = TrainOrderOfflineUtil.getIsMailPMOrNextAM();//是否包含在11点-18点的下午寄件的时间段内，否则就是当天11点或者第二天11点的寄件

        Calendar c = Calendar.getInstance();//可以对每个时间域单独修改
        DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        //arriveTime = "1900-01-01 00:00:00";
        int doubleNum = 1;
        if (isInProvince) {//省内 + 2天 - 11点寄件 - 18点寄件
            doubleNum = 2;
        }
        else {//省外 + 4天
            doubleNum = 4;
        }
        if (isMailPMOrNextAM == 1) {
            c.set(Calendar.HOUR_OF_DAY, 11 + 24 * doubleNum);//
            c.set(Calendar.MINUTE, 0);//
            c.set(Calendar.SECOND, 0);//
            arriveTime = df2.format(new Date(c.getTimeInMillis()));
        }
        else if (isMailPMOrNextAM == 2) {
            c.set(Calendar.HOUR_OF_DAY, 18 + 24 * doubleNum);//
            c.set(Calendar.MINUTE, 0);//
            c.set(Calendar.SECOND, 0);//
            arriveTime = df2.format(new Date(c.getTimeInMillis()));
        }
        else if (isMailPMOrNextAM == 3) {
            c.set(Calendar.HOUR_OF_DAY, 11 + 24 * (doubleNum + 1));//
            c.set(Calendar.MINUTE, 0);//
            c.set(Calendar.SECOND, 0);//
            arriveTime = df2.format(new Date(c.getTimeInMillis()));
        }
        return arriveTime;
    }

    public static String ungetEMSDelieveTime(String totalAddress, String agentid, String LOGNAME, Integer random) {
        String arriveTime = "";

        Boolean isInProvince = TrainOrderOfflineUtil.getIsInProvince(agentid, totalAddress, LOGNAME, random);//true-省内 - false-省外

        /**
         * 1;//当天上午件
         * 2;//当天下午件
         * 3;//第二天的上午件
         */
        Integer isMailPMOrNextAM = TrainOrderOfflineUtil.getIsMailPMOrNextAM();//是否包含在11点-18点的下午寄件的时间段内，否则就是当天11点或者第二天11点的寄件

        Calendar c = Calendar.getInstance();//可以对每个时间域单独修改
        DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        //arriveTime = "1900-01-01 00:00:00";
        int doubleNum = 1;
        if (isInProvince) {//省内 + 2天 - 11点寄件 - 18点寄件
            doubleNum = 3;
        }
        else {//省外 + 4天
            doubleNum = 6;
        }
        if (isMailPMOrNextAM == 1) {
            c.set(Calendar.HOUR_OF_DAY, 11 + 24 * doubleNum);//
            c.set(Calendar.MINUTE, 0);//
            c.set(Calendar.SECOND, 0);//
            arriveTime = df2.format(new Date(c.getTimeInMillis()));
        }
        else if (isMailPMOrNextAM == 2) {
            c.set(Calendar.HOUR_OF_DAY, 18 + 24 * doubleNum);//
            c.set(Calendar.MINUTE, 0);//
            c.set(Calendar.SECOND, 0);//
            arriveTime = df2.format(new Date(c.getTimeInMillis()));
        }
        else if (isMailPMOrNextAM == 3) {
            c.set(Calendar.HOUR_OF_DAY, 11 + 24 * (doubleNum + 1));//
            c.set(Calendar.MINUTE, 0);//
            c.set(Calendar.SECOND, 0);//
            arriveTime = df2.format(new Date(c.getTimeInMillis()));
        }
        return arriveTime;
    }

    public static String ungetUUPTDelieveTime() {
        String arriveTime = "";

        //以8点和18点为寄件时间进行区分 - 18点之后接单，以第二天8点发件为匹配开始
        Calendar c = Calendar.getInstance();//可以对每个时间域单独修改
        DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        int hour = c.get(Calendar.HOUR_OF_DAY);
        if (hour < 8) {
            c.set(Calendar.HOUR_OF_DAY, 11);//8+3[当天的十一点送达]
            c.set(Calendar.MINUTE, 0);//
            c.set(Calendar.SECOND, 0);//
            arriveTime = df2.format(new Date(c.getTimeInMillis()));
        }
        else if (hour >= 8 && hour <= 18) {
            c.set(Calendar.HOUR_OF_DAY, hour + 3);//当前的小时数+3
            arriveTime = df2.format(new Date(c.getTimeInMillis()));
        }
        else {
            c.set(Calendar.HOUR_OF_DAY, 35);//8+3+24[第二天的十一点送达]
            c.set(Calendar.MINUTE, 0);//
            c.set(Calendar.SECOND, 0);//
            arriveTime = df2.format(new Date(c.getTimeInMillis()));
        }
        return arriveTime;
    }

    public static String getRefundreasonstrByRefundReason(String refundReason) {
        String refundreasonstr = "";
        if ("0".equals(refundReason)) {
            refundreasonstr = "";
        }
        else if ("1".equals(refundReason)) {
            refundreasonstr = "原因：订单价格不符！";
        }
        else if ("2".equals(refundReason)) {
            refundreasonstr = "原因：订单行程错误！";
        }
        else if ("4".equals(refundReason)) {
            refundreasonstr = "原因：无法满足定制服务！";
        }
        /*else if ("3".equals(refundReason)) {
            refundreasonstr = "原因：" + RefundReasonStr + "！";
        }
        else if ("15".equals(refundReason)) {
            refundreasonstr = "原因：" + RefundReasonStr + "！";
        }*/

        /*else if ("10".equals(refundReason)) {
            refundreasonstr = "原因：" + RefundReasonStr + "！";
        }*/
        //拒单淘宝逻辑

        //客服操作拒单
        else if ("11".equals(refundReason)) {
            refundreasonstr = "原因：所购买的车次坐席已无票！";
        }
        else if ("13".equals(refundReason)) {
            refundreasonstr = "原因：去哪儿票价和12306不符！";
        }
        else if ("14".equals(refundReason)) {
            refundreasonstr = "原因：车次数据与12306不一致！";
        }
        else if ("15".equals(refundReason)) {
            refundreasonstr = "原因：乘客信息错误！";
        }

        else if ("26".equals(refundReason)) {
            refundreasonstr = "原因：邮寄地址无法保证及时送达！";
        }
        else if ("27".equals(refundReason)) {
            refundreasonstr = "原因：无法满足用户定制需求！";
        }
        else if ("28".equals(refundReason)) {
            refundreasonstr = "原因：与乘客沟通，乘客同意主动取消订单！";
        }

        else if ("30".equals(refundReason)) {
            refundreasonstr = "原因：出票失败全额退款！";
        }
        else if ("31".equals(refundReason)) {
            refundreasonstr = "原因：票已售完，出票失败全额退款！";
        }
        else if ("32".equals(refundReason)) {
            refundreasonstr = "原因：票价变动，出票失败全额退款！";
        }
        else if ("33".equals(refundReason)) {
            refundreasonstr = "原因：乘车人已购买相同车票，出票失败全额退款！";
        }
        else if ("34".equals(refundReason)) {
            refundreasonstr = "原因：出票超时，出票失败全额退款！";
        }
        else if ("35".equals(refundReason)) {
            refundreasonstr = "原因：乘车人证件未通过铁路局审核，需到售票窗口办理！";
        }
        else if ("36".equals(refundReason)) {
            refundreasonstr = "原因：发车时间变动，出票失败全额退款！";
        }
        else if ("37".equals(refundReason)) {
            refundreasonstr = "原因：车次信息错误，出票失败全额退款！";
        }
        else if ("38".equals(refundReason)) {
            refundreasonstr = "原因：12306故障,出票失败全额退款！";
        }

        else if ("39".equals(refundReason)) {
            refundreasonstr = "原因：锁单失败，全额退款！";
        }

        //去哪
        else if ("101".equals(refundReason)) {
            refundreasonstr = "原因：所购买的车次坐席已无票！";
        }
        else if ("102".equals(refundReason)) {
            refundreasonstr = "原因：票价和12306不符！";
        }
        else if ("103".equals(refundReason)) {
            refundreasonstr = "原因：车次数据与12306不一致！";
        }
        else if ("104".equals(refundReason)) {
            refundreasonstr = "原因：乘客信息错误！";
        }
        else if ("105".equals(refundReason)) {
            refundreasonstr = "原因：快递无法送达！";
        }
        else if ("106".equals(refundReason)) {
            refundreasonstr = "原因：无下铺票！";
        }
        else if ("107".equals(refundReason)) {
            refundreasonstr = "原因：无靠窗同包厢票！";
        }
        else if ("108".equals(refundReason)) {
            refundreasonstr = "原因：其他！";
        }
        return refundreasonstr;
    }

    public static Integer getTCfailReasonIdByRefundreasonstr(String refundreasonstr) {
        int failReasonId = 10;//其他 - 10
        if (refundreasonstr.contains("票已售完")) {//该座席已无票 - 
            failReasonId = 0;
        }
        else if (refundreasonstr.contains("乘车人已购买相同车票")) {//乘客实名制购票行程冲突 - 
            failReasonId = 1;
        }
        /*
         * 我们这里没有这个原因
        else if (refundreasonstr.contains("乘客被限制高消费")) {//乘客被限制高消费 - 
            failReasonId = 2;
        }*/
        else if (refundreasonstr.contains("票价变动")) {//支付票价少于铁路实际票价 - 
            failReasonId = 3;
        }
        /*
         * 我们这里没有这个原因
         * else if (refundreasonstr.contains("乘客信息错误")) {//乘客信息错误 - 
            failReasonId = 4;
        }*/
        else if (refundreasonstr.contains("车次信息错误")) {//车次数据错误 - 
            failReasonId = 5;
        }
        else if (refundreasonstr.contains("邮寄地址无法保证及时送达")) {//配送区域快递不可送达 - 
            failReasonId = 6;
        }
        else if (refundreasonstr.contains("12306故障")) {//铁路系统故障 - 
            failReasonId = 7;
        }
        /*
         * 我们这里没有这个原因
         * else if (refundreasonstr.contains("快递系统故障")) {//快递系统故障 - 
            failReasonId = 8;
        }*/
        else if (refundreasonstr.contains("无法满足用户定制需求")) {//座席不符合乘客要求 - 
            failReasonId = 9;
        }
        return failReasonId;
    }

    public static Boolean getIsInProvince(String AgentId, String totalAddress, String LOGNAME, Integer random) {
        Boolean isInProvince = true;

        String provinceName = "";
        try {
            provinceName = trainOfflinePriceDao.findAlternative1ByAgentId(AgentId);
        }
        catch (Exception e) {
            ExceptionTCUtil.handleTCException(e);
        }

        if (provinceName == null || "".equals(provinceName)) {
            WriteLog.write(LOGNAME, random + ":" + AgentId + "-对应的代售点的省份信息未配置");
            return isInProvince;//默认走省内的时效
        }

        //匹配省内或者更省外的快递费
        if (!totalAddress.startsWith(provinceName)) {
            isInProvince = false;
        }
        return isInProvince;
    }

    public static Integer getIsMailPMOrNextAM() {
        //以11点和18点为寄件时间进行区分 - 这个只以11点为准进行比较
        Integer isMailPMOrNextAM = 3;//第二天的上午件

        Calendar c = Calendar.getInstance();//可以对每个时间域单独修改
        int hour = c.get(Calendar.HOUR_OF_DAY);
        if (hour < 11) {
            isMailPMOrNextAM = 1;//当天上午件
        }
        else if (hour >= 11 && hour < 18) {
            isMailPMOrNextAM = 2;//当天下午件
        }
        /*else {
            isMailPMOrNextAM = 3;//第二天的上午件
        }*/
        return isMailPMOrNextAM;
    }

    public static TrainOrderOffline interceptTestCS(TrainOrderOffline trainOrderOffline) {
        //Integer agentid = 414;//正式环境的测试账号
        Integer agentid = 382;//测试环境的测试账号
        trainOrderOffline.setAgentId(agentid);//分配逻辑中分配的代售点的相关的信息
        return trainOrderOffline;
    }

    public static TrainOrderOffline interceptTestZS(TrainOrderOffline trainOrderOffline) {
        Integer agentid = 414;//正式环境的测试账号
        //Integer agentid = 382;//测试环境的测试账号
        trainOrderOffline.setAgentId(agentid);//分配逻辑中分配的代售点的相关的信息
        return trainOrderOffline;
    }

}
