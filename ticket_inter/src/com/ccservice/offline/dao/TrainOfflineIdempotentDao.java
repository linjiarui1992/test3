/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.dao;

import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataTable;
import com.ccservice.offline.domain.TrainOfflineIdempotent;
import com.ccservice.offline.util.TrainOrderOfflineUtil;

/**
 * @className: com.ccservice.offline.dao.TrainOfflineIdempotentDao
 * @description: TODO - 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年10月10日 下午5:52:39 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOfflineIdempotentDao {
    
    //插入幂等信息，并返回主键
    public String addTrainOfflineIdempotent(TrainOfflineIdempotent trainOfflineIdempotent) throws Exception {
        //超时时间是从数据库中配置的？
        String sql = "OFFLINEIDEMPOTENT_addTrainOfflineIdempotent @idempotentLockKey='" + trainOfflineIdempotent.getIdempotentLockKey()
                +"',@idempotentLockValue=" + trainOfflineIdempotent.getIdempotentLockValue();
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
        if (dataTable.GetRow().size() == 0) {
            return "幂等信息插入失败";
        }
        return String.valueOf(dataTable.GetRow().get(0).GetColumn("PKID").GetValue());
    }

    //插入幂等信息，并返回主键
    public String addTrainOfflineIdempotent(String idempotentLockKey, Boolean idempotentLockValue) throws Exception {
        //String nowDateStr = TrainOrderOfflineUtil.getNowDateStr(); - TrainOrderOfflineUtil.getTimestampByStr(nowDateStr) - 更新修改时间
        //超时时间是从数据库中配置的？
        String sql = "OFFLINEIDEMPOTENT_addTrainOfflineIdempotent @idempotentLockKey='" + idempotentLockKey
                +"',@idempotentLockValue=" + idempotentLockValue;
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
        if (dataTable.GetRow().size() == 0) {
            return "幂等信息插入失败";
        }
        return String.valueOf(dataTable.GetRow().get(0).GetColumn("PKID").GetValue());
    }

    public TrainOfflineIdempotent findTrainOfflineIdempotentByPKID(Integer PKID) throws Exception {
        String sql = "OFFLINEIDEMPOTENT_findTrainOfflineIdempotentByPKID @PKID=" + PKID;
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        //SQLServerUtil.printTable(dataTable);
        
        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return (TrainOfflineIdempotent) new BeanHanlder(TrainOfflineIdempotent.class).handle(dataTable);
    }

    public String findResultValueByPKID(Integer PKID) throws Exception {
        String sql = "OFFLINEIDEMPOTENT_findResultValueByPKID @PKID=" + PKID;
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        //SQLServerUtil.printTable(dataTable);
        
        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return String.valueOf(dataTable.GetRow().get(0).GetColumn("idempotentResultValue").GetValue());
    }

    public Boolean findLockValueByPKID(Integer PKID) throws Exception {
        String sql = "OFFLINEIDEMPOTENT_findLockValueByPKID @PKID=" + PKID;
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        //SQLServerUtil.printTable(dataTable);
        
        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return Boolean.valueOf(String.valueOf(dataTable.GetRow().get(0).GetColumn("idempotentLockValue").GetValue()));
    }

    public TrainOfflineIdempotent findTrainOfflineIdempotentByLockKey(String idempotentLockKey) throws Exception {
        String sql = "OFFLINEIDEMPOTENT_findTrainOfflineIdempotentByLockKey @idempotentLockKey='" + idempotentLockKey + "'";
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return (TrainOfflineIdempotent) new BeanHanlder(TrainOfflineIdempotent.class).handle(dataTable);
    }

    //存储的时候就保证了唯一性
    public Boolean findLockValueByLockKey(String idempotentLockKey) throws Exception {
        String sql = "OFFLINEIDEMPOTENT_findLockValueByLockKey @idempotentLockKey='" + idempotentLockKey + "'";
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return Boolean.valueOf(String.valueOf(dataTable.GetRow().get(0).GetColumn("idempotentLockValue").GetValue()));
    }

    public Integer findReqNumByPKID(Integer PKID) throws Exception {
        String sql = "OFFLINEIDEMPOTENT_findReqNumByPKID @PKID=" + PKID;
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        //SQLServerUtil.printTable(dataTable);
        
        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return Integer.valueOf(String.valueOf(dataTable.GetRow().get(0).GetColumn("idempotentReqNum").GetValue()));
    }

    public Integer findResNumByPKID(Integer PKID) throws Exception {
        String sql = "OFFLINEIDEMPOTENT_findResNumByPKID @PKID=" + PKID;
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        //SQLServerUtil.printTable(dataTable);
        
        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return Integer.valueOf(String.valueOf(dataTable.GetRow().get(0).GetColumn("idempotentResNum").GetValue()));
    }

    public Integer findCountByLockKey(String idempotentLockKey) throws Exception {
        String sql = "OFFLINEIDEMPOTENT_findCountByLockKey @idempotentLockKey='" + idempotentLockKey +"'";
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return Integer.valueOf(String.valueOf(dataTable.GetRow().get(0).GetColumn("idempotentCount").GetValue()));
    }

    public Integer updateReqNumByPKID(TrainOfflineIdempotent trainOfflineIdempotent) throws Exception {
        String nowDateStr = TrainOrderOfflineUtil.getNowDateStr();// -  - 更新修改时间
        String sql = "OFFLINEIDEMPOTENT_updateReqNumByPKID @PKID="+trainOfflineIdempotent.getPKID()
            +",@idempotentReqNum="+trainOfflineIdempotent.getIdempotentReqNum()
            +",@updateTime='"+TrainOrderOfflineUtil.getTimestampByStr(nowDateStr)+"'";
        return DBHelperOffline.UpdateData(sql);
    }

    public Integer updateResNumByPKID(TrainOfflineIdempotent trainOfflineIdempotent) throws Exception {
        String nowDateStr = TrainOrderOfflineUtil.getNowDateStr();// -  - 更新修改时间
        String sql = "OFFLINEIDEMPOTENT_updateResNumByPKID @PKID="+trainOfflineIdempotent.getPKID()
            +",@idempotentResNum="+trainOfflineIdempotent.getIdempotentResNum()
            +",@updateTime='"+TrainOrderOfflineUtil.getTimestampByStr(nowDateStr)+"'";
        return DBHelperOffline.UpdateData(sql);
    }

    public Integer updateResultValueByPKID(TrainOfflineIdempotent trainOfflineIdempotent) throws Exception {
        String nowDateStr = TrainOrderOfflineUtil.getNowDateStr();// -  - 更新修改时间
        String sql = "OFFLINEIDEMPOTENT_updateResultValueByPKID @PKID="+trainOfflineIdempotent.getPKID()
            +",@idempotentResultValue='"+trainOfflineIdempotent.getIdempotentResultValue()
            +"',@updateTime='"+TrainOrderOfflineUtil.getTimestampByStr(nowDateStr)+"'";
        return DBHelperOffline.UpdateData(sql);
    }

    public Integer delTrainOfflineIdempotentByPKID(Integer PKID) {
        String sql = "DELETE FROM TrainOfflineIdempotent WHERE PKID=" + PKID;
        return DBHelperOffline.UpdateData(sql);
    }

}
