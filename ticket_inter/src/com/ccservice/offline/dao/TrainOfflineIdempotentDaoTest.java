/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.dao;

import com.ccservice.offline.domain.TrainOfflineIdempotent;

/**
 * @className: com.ccservice.offline.dao.TrainOfflineIdempotentDaoTest
 * @description: TODO - 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年10月11日 上午10:07:34 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOfflineIdempotentDaoTest {
    private TrainOfflineIdempotentDao trainOfflineIdempotentDao = new TrainOfflineIdempotentDao();

    private void testAddTrainOfflineIdempotent() throws Exception {
        TrainOfflineIdempotent trainOfflineIdempotent = new TrainOfflineIdempotent();
        
        trainOfflineIdempotent.setIdempotentLockKey("fjw211");
        trainOfflineIdempotent.setIdempotentLockValue(true);

        System.out.println(trainOfflineIdempotentDao.addTrainOfflineIdempotent(trainOfflineIdempotent));
    }

    private void testFindTrainOfflineIdempotentByPKID() throws Exception {
        System.out.println(trainOfflineIdempotentDao.findTrainOfflineIdempotentByPKID(1));
    }

    private void testFindLockValueByLockKey() throws Exception {
        /*System.out.println(trainOfflineIdempotentDao.findLockValueByLockKey("fjw211222"));*/
        
        Boolean OrderLock = false;
        try {
            OrderLock = trainOfflineIdempotentDao.findLockValueByLockKey("Lock");
        }
        catch (Exception e1) {
            e1.printStackTrace();
        }
        System.out.println(OrderLock);
    }
    
    private void testDelTrainOfflineIdempotentByPKID() throws Exception {
        System.out.println(trainOfflineIdempotentDao.delTrainOfflineIdempotentByPKID(1));
    }
    
    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        TrainOfflineIdempotentDaoTest trainOfflineIdempotentDaoTest = new TrainOfflineIdempotentDaoTest();
        
        trainOfflineIdempotentDaoTest.testFindLockValueByLockKey();

        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间： " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间： " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间： " + runTimeS / 60 + "min：" + runTimeS % 60 + "s");//分
    }
}
