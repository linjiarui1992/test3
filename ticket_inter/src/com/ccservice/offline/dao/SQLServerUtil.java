package com.ccservice.offline.dao;

import java.util.Iterator;
import java.util.List;

import com.ccervice.util.db.DataColumn;
import com.ccervice.util.db.DataRow;
import com.ccervice.util.db.DataTable;

/**
 * @className: com.hthy.weixin.util.SQLServerUtil
 * @description: TODO - 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年2月23日 下午5:44:39 
 * @version: v 1.0
 * @since 
 *
 */
public class SQLServerUtil {

    public static void printTable(DataTable table) {
        List<DataRow> rList = table.GetRow();
        Iterator<DataRow> iterator = rList.iterator();
        while (iterator.hasNext()) {
            DataRow dataRow = (DataRow) iterator.next();
            List<DataColumn> columns = dataRow.GetColumn();
            Iterator<DataColumn> iteratorColumn = columns.iterator();
            while (iteratorColumn.hasNext()) {
                DataColumn dataColumn = (DataColumn) iteratorColumn.next();
                System.out.println(dataColumn.GetKey() + "...." + dataColumn.GetValue());
            }
            System.out.println();
        }
    }

}
