package com.ccservice.offline.dao;

import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.domain.TrainPassengerOffline;

public class TrainPassengerOfflineDaoTest {
    private TrainOrderOfflineDao trainOrderOfflineDao = new TrainOrderOfflineDao();
    private TrainPassengerOfflineDao trainPassengerOfflineDao = new TrainPassengerOfflineDao();

    public void testAddTrainPassengerOffline() throws Exception {
        TrainPassengerOffline model = new TrainPassengerOffline();
        
        TrainPassengerOfflineDao dao = new TrainPassengerOfflineDao();
        model.setOrderId(1L);
        model.setName("陈亚峰");
        model.setIdNumber("dddddddd");
        model.setIdType(1);
        model.setIdNumber("sdfsdfsdfsdf");
        
        try {
            System.out.println(dao.addTrainPassengerOffline(model));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void testFindTrainPassengerOfflineListByOrderId() throws Exception {
        String orderId = "test17081987854216";
        TrainOrderOffline trainOrderOffline = trainOrderOfflineDao.findTrainOrderOfflineByOrderNumberOnline(orderId);
        System.out.println(trainPassengerOfflineDao.findTrainPassengerOfflineListByOrderId(trainOrderOffline.getId()));
    }
    
    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        TrainPassengerOfflineDaoTest trainPassengerOfflineDaoTest = new TrainPassengerOfflineDaoTest();
        trainPassengerOfflineDaoTest.testFindTrainPassengerOfflineListByOrderId();

        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间： " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间： " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间： " + runTimeS / 60 + "min：" + runTimeS % 60 + "s");//分
    }
    
}
