/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.dao;

import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataTable;
import com.ccservice.offline.domain.TrainOfflineRefundRecord;
import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.util.TrainOrderOfflineUtil;

/**
 * @className: com.ccservice.offline.dao.TrainOfflineRefundRecordDao
 * @description: TODO - 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年9月12日 上午9:50:36 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOfflineRefundRecordDao {
    public String addTrainOfflineRefundRecord(TrainOfflineRefundRecord trainOfflineRefundRecord) throws Exception {
        String nowDateStr = TrainOrderOfflineUtil.getNowDateStr();
        String sql = "OFFLINENEW_addTrainOfflineRefundRecord "
                + "@orderId=" + trainOfflineRefundRecord.getOrderId()
                +",@operateName='" + trainOfflineRefundRecord.getOperateName() 
                +"',@refundSum='" + trainOfflineRefundRecord.getRefundSum() 
                +"',@operateTime='" + trainOfflineRefundRecord.getOperateTime() 
                +"',@refundType=" + trainOfflineRefundRecord.getRefundType()
                +",@seqId='" + trainOfflineRefundRecord.getSeqId()+"'";
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
        if (dataTable.GetRow().size() == 0) {
            return "线下火车票订单信息插入失败";
        }
        return String.valueOf(dataTable.GetRow().get(0).GetColumn("Id").GetValue());
    }
    
    public TrainOfflineRefundRecord findTrainOfflineRefundRecordById(Integer Id) throws Exception {
        String sql = "OFFLINENEW_findTrainOfflineRefundRecordById @Id=" + Id;
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        //SQLServerUtil.printTable(dataTable);
        
        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return (TrainOfflineRefundRecord) new BeanHanlder(TrainOfflineRefundRecord.class).handle(dataTable);
    }

    public Integer delTrainOfflineRefundRecordById(Integer Id) {
        String sql = "DELETE FROM TrainOfflineRefundRecord WHERE Id=" + Id;
        return DBHelperOffline.UpdateData(sql);
    }
    
}
