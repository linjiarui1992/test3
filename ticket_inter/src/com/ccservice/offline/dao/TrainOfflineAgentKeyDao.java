/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.dao;

import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataTable;
import com.ccservice.offline.domain.TrainOfflineAgentKey;
import com.ccservice.offline.domain.TrainTicketOffline;

/**
 * @className: com.ccservice.tuniu.train.dao.TrainOrderOfflineDao
 * @description: TODO -
 * @author: 郑州-技术-郭伟强 E-mail:gwq20521@163.com
 * @createTime: 2017年8月16日 下午2:08:02
 * @version: v 1.0
 * @since
 *
 */
public class TrainOfflineAgentKeyDao {
	// 插入线下火车票订单信息，并返回订单号
	public String addTrainOfflineAgentKey(TrainOfflineAgentKey trainOfflineAgentKey) throws Exception {
		String sql = "OFFLINE_addTrainOfflineAgentKey @partnerName='" + trainOfflineAgentKey.getPartnerName() 
		+ "',@keys='"+ trainOfflineAgentKey.getKeys() 
		+ "',@notes='" + trainOfflineAgentKey.getNotes()
		+ "',@notes1='"+ trainOfflineAgentKey.getNotes1() + "'";

		DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
		if (dataTable.GetRow().size() == 0) {
			return "插入失败";
		}
		return String.valueOf(dataTable.GetRow().get(0).GetColumn("Id").GetValue());
	}

	// 根据ID获取订单对象
	public TrainOfflineAgentKey findTrainOfflineAgentKeyById(Long Id) throws Exception {
		String sql = "OFFLINE_findTrainOfflineAgentKeyById @Id=" + Id;
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        //SQLServerUtil.printTable(dataTable);
        
        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return (TrainOfflineAgentKey) new BeanHanlder(TrainOfflineAgentKey.class).handle(dataTable);
	}

	// 修改，一般是修改指定值
	/*
	 * public Integer updateTrainOrderOfflineChuPiaoTimeById(TrainOrderOffline
	 * trainOrderOffline) { String sql =
	 * "OFFLINE_updateTrainOrderOfflineChuPiaoTimeById @ChuPiaoTime='" +
	 * trainOrderOffline.getNeedDeliveryTime() + "'"; return
	 * DBHelper.UpdateData(sql); }
	 */

	// 根据ID删除订单对象-本地测试
	public Integer delTrainOfflineAgentKeyById(Long Id) throws Exception {
		String sql = "DELETE FROM TrainOfflineAgentKey WHERE PkId=" + Id;
		return DBHelperOffline.UpdateData(sql);
	}

    public TrainOfflineAgentKey findTrainOfflineAgentKeyByPartnerName(String partnerName) throws Exception {
        String sql = "OFFLINE_findTrainOfflineAgentKeyByPartnerName @partnerName='" + partnerName+"'";
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return (TrainOfflineAgentKey) new BeanHanlder(TrainOfflineAgentKey.class).handle(dataTable);
    }

    public Integer updateTrainOfflineAgentKeysByPkId(Integer PkId, String keys) {
        String sql = "UPDATE TrainOfflineAgentKey SET keys = '"+keys+"' WHERE PkId=" + PkId;
        return DBHelperOffline.UpdateData(sql);
    }

}
