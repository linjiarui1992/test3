/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.dao;

import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataTable;
import com.ccservice.offline.domain.TrainOfflineCannotDelivery;

/**
 * @className: com.ccservice.tuniu.train.dao.TrainOrderOfflineDao
 * @description: TODO - 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月16日 下午2:08:02 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOfflineCannotDeliveryDao {
    public String addTrainOfflineCannotDelivery(TrainOfflineCannotDelivery trainOfflineCannotDelivery)
            throws Exception {
        String sql = "OFFLINE_addTrainOfflineCannotDelivery @fromStation='"
                + trainOfflineCannotDelivery.getFromStation() + "',@province='"
                + trainOfflineCannotDelivery.getProvince() + "',@city='" + trainOfflineCannotDelivery.getCity()
                + "',@district='" + trainOfflineCannotDelivery.getDistrict() + "',@address='"
                + trainOfflineCannotDelivery.getAddress() + "',@departureTime='"
                + trainOfflineCannotDelivery.getDepartureTime() + "',@delieveType="
                + trainOfflineCannotDelivery.getDelieveType() + ",@arriveTime='"
                + trainOfflineCannotDelivery.getArriveTime() + "'";
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
        if (dataTable.GetRow().size() == 0) {
            return "不可配送的相关对象信息插入失败";
        }
        return String.valueOf(dataTable.GetRow().get(0).GetColumn("PKID").GetValue());
    }
}
