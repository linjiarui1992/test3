/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.dao;

import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataTable;
import com.ccservice.offline.domain.TrainOfflinePrice;
import com.ccservice.offline.domain.TrainOrderOfflineStatistics;
import com.ccservice.offline.util.TrainOrderOfflineUtil;

/**
 * @className: com.ccservice.offline.dao.TrainOrderOfflineStatisticsDao
 * @description: TODO - 
 * @author: 郑州-技术-杨威   E-mail:653395455@qq.com
 * @createTime: 2017年9月5日 上午10:18:36 
 * @version: v 1.0
 * @since E
 *
 */
public class TrainOrderOfflineStatisticsDao {

    //插入出票点出票统计表信息，并返回订单号
    public String addTrainOrderOfflineStatistics(TrainOrderOfflineStatistics trainOrderOfflineStatistics) throws Exception {
        //超时时间是从数据库中配置的？
        String sql = "OFFLINENEW_addTrainOrderOfflineStatistics " + 
                 "@agentId=" + trainOrderOfflineStatistics.getAgentId()
                +",@finishTime=" + trainOrderOfflineStatistics.getFinishTime()
                +",@avgAllTime=" + trainOrderOfflineStatistics.getAvgAllTime()
                +",@finishAvg=" + trainOrderOfflineStatistics.getFinishAvg()
                +",@letterSix=" + trainOrderOfflineStatistics.getLetterSix()
                +",@biggerSix=" + trainOrderOfflineStatistics.getBiggerSix()
                +",@biggerTen=" + trainOrderOfflineStatistics.getBiggerTen()
                +",@finishSum=" + trainOrderOfflineStatistics.getFinishSum()
                +",@allOrderSum=" + trainOrderOfflineStatistics.getAllOrderSum()
                +",@staticDate='" + trainOrderOfflineStatistics.getStaticDate()
                +"',@ranking=" + trainOrderOfflineStatistics.getRanking()
                +",@grade=" + trainOrderOfflineStatistics.getGrade();
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
        if (dataTable.GetRow().size() == 0) {
            return "出票点出票统计表信息插入失败";
        }
        return String.valueOf(dataTable.GetRow().get(0).GetColumn("Id").GetValue());
    }
    /**
     * 根据Id查询ranking
     * @param Id
     * @return
     * @throws Exception
     */
    public String findrankingById(int Id) throws Exception {
        String sql = "OFFLINENEW_findrankingById @Id=" + Id;
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
        
        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return String.valueOf(dataTable.GetRow().get(0).GetColumn("ranking").GetValue());
    }
    //根据ID删除出出票点出票统计信息-本地测试
    public Integer delTrainOrderOfflineStatisticsById(int Id) {
        String sql = "DELETE FROM TrainOrderOfflineStatistics WHERE Id=" + Id;
        return DBHelperOffline.UpdateData(sql);
    }
    
}
