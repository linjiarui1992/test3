/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.dao;

import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataTable;
import com.ccservice.offline.domain.TrainOfflineConfig;
import com.ccservice.offline.util.TrainOrderOfflineUtil;

/**
 * @className: com.ccservice.offline.dao.TrainOfflineConfigDao
 * @description: TODO - 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年9月23日 下午12:43:37 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOfflineConfigDao {
    public String findValueByKeyName(String keyName) throws Exception {
        String sql = "SELECT value FROM TrainOfflineConfig WHERE keyName='" + keyName +"'";
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        //SQLServerUtil.printTable(dataTable);
        
        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return String.valueOf(dataTable.GetRow().get(0).GetColumn("value").GetValue());
    }

    //根据ID获取订单对象
    public TrainOfflineConfig findTrainOfflineConfigByKeyName(String keyName) throws Exception {
        String sql = "SELECT * FROM TrainOfflineConfig WHERE keyName='" + keyName +"'";
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        //SQLServerUtil.printTable(dataTable);
        
        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return (TrainOfflineConfig) new BeanHanlder(TrainOfflineConfig.class).handle(dataTable);
    }

    public Integer addTrainOfflineConfig(String keyName, String value, String pwd) {
        String sql = "INSERT INTO TrainOfflineConfig (keyName, value, remark1) VALUES ('"+keyName+"','"+value+"','"+pwd+"')";
        return DBHelperOffline.insertSql(sql);
    }

    public Integer updateValueByKeyName(String keyName, String value, String pwd) {
        String sql = "UPDATE TrainOfflineConfig SET value='"+value+"', remark1='"+pwd +"' WHERE keyName='" + keyName +"'";
        return DBHelperOffline.UpdateData(sql);
    }

    public Integer delTrainOfflineConfigByKeyName(String keyName) {
        String sql = "DELETE FROM TrainOfflineConfig WHERE keyName='" + keyName +"'";
        return DBHelperOffline.UpdateData(sql);
    }

}
