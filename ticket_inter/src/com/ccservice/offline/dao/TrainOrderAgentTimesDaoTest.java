/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.dao;

import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataTable;
import com.ccservice.offline.domain.TrainOrderAgentTimes;

/**
 * @className: com.ccservice.tuniu.train.dao.TrainOfflineExpressCodeDao
 * @description: TODO - 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月28日 下午4:48:32 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOrderAgentTimesDaoTest {
    private TrainOrderAgentTimesDao trainOrderAgentTimesDao = new TrainOrderAgentTimesDao();

    public void testFindTrainOrderAgentTimesByAgentId() throws Exception {
        System.out.println(trainOrderAgentTimesDao.findTrainOrderAgentTimesByAgentId("384"));
    }
    
    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        TrainOrderAgentTimesDaoTest trainOrderAgentTimesDaoTest = new TrainOrderAgentTimesDaoTest();

        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间： " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间： " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间： " + runTimeS / 60 + "min：" + runTimeS % 60 + "s");//分
    }
}
