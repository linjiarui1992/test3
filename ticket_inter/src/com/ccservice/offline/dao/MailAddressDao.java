package com.ccservice.offline.dao;

import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataTable;
import com.ccservice.offline.domain.MailAddress;

public class MailAddressDao {

    public String addMailAddress(MailAddress mailAddress) throws Exception {
        String sql = "OFFLINE_addMailAddress @MAILNAME ='" + mailAddress.getMAILNAME() + "',@MAILTEL='"
                + mailAddress.getMAILTEL() + "',@POSTCODE='" + mailAddress.getPOSTCODE() + "',@ADDRESS='"
                + mailAddress.getADDRESS() + "',@ORDERID=" + mailAddress.getORDERID() + ",@PROVINCENAME='"
                + mailAddress.getPROVINCENAME() + "',@CITYNAME='" + mailAddress.getCITYNAME() + "',@REGIONNAME='"
                + mailAddress.getREGIONNAME() + "'";
        //+ ",@ExpressNum='"+ mailAddress.getAddress()+"'";

        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
        if (dataTable.GetRow().size() == 0) {
            return "插入失败";
        }
        return String.valueOf(dataTable.GetRow().get(0).GetColumn("Id").GetValue());
    }

    public String addMailAddressTC(MailAddress mailAddress) throws Exception {
        String sql = "OFFLINETC_addMailAddressTC @MAILNAME ='" + mailAddress.getMAILNAME() + "',@MAILTEL='"
                + mailAddress.getMAILTEL() + "',@ADDRESS='" + mailAddress.getADDRESS() + "',@ORDERID="
                + mailAddress.getORDERID() + ",@PROVINCENAME='" + mailAddress.getPROVINCENAME() + "',@CITYNAME='"
                + mailAddress.getCITYNAME() + "',@REGIONNAME='" + mailAddress.getREGIONNAME() + "',@ExpressAgent="
                + mailAddress.getExpressAgent();

        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
        if (dataTable.GetRow().size() == 0) {
            return "插入失败";
        }
        return String.valueOf(dataTable.GetRow().get(0).GetColumn("Id").GetValue());
    }

    public String addMailAddressTCRapidSend(MailAddress mailAddress) throws Exception {
        String sql = "OFFLINETC_addMailAddressTCRapidSend @MAILNAME ='" + mailAddress.getMAILNAME() + "',@MAILTEL='"
                + mailAddress.getMAILTEL() + "',@ADDRESS='" + mailAddress.getADDRESS() + "',@ORDERID="
                + mailAddress.getORDERID() + ",@PROVINCENAME='" + mailAddress.getPROVINCENAME() + "',@CITYNAME='"
                + mailAddress.getCITYNAME() + "',@REGIONNAME='" + mailAddress.getREGIONNAME() + "',@ExpressAgent="
                + mailAddress.getExpressAgent() + ",@rapidSendPrice=" + mailAddress.getRapidSendPrice();

        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
        if (dataTable.GetRow().size() == 0) {
            return "插入失败";
        }
        return String.valueOf(dataTable.GetRow().get(0).GetColumn("Id").GetValue());
    }

    //根据ID获取订单对象
    public MailAddress findMailAddressById(Long Id) throws Exception {
        String sql = "OFFLINE_findMailAddressById @Id=" + Id;
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        //SQLServerUtil.printTable(dataTable);

        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return (MailAddress) new BeanHanlder(MailAddress.class).handle(dataTable);
    }

    //根据ID获取订单对象
    public MailAddress findMailAddressByORDERID(Long ORDERID) throws Exception {
        String sql = "SELECT * FROM MailAddress WHERE ORDERID=" + ORDERID;
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        //SQLServerUtil.printTable(dataTable);

        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return (MailAddress) new BeanHanlder(MailAddress.class).handle(dataTable);
    }

    public MailAddress findADDRESSCITYNAMEByORDERID(Long ORDERID) throws Exception {
        String sql = "SELECT ADDRESS,CITYNAME,ExpressAgent FROM MailAddress WHERE ORDERID=" + ORDERID;
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        //SQLServerUtil.printTable(dataTable);

        if (dataTable.GetRow().size() == 0) {
            return null;
        }

        String ADDRESS = String.valueOf(dataTable.GetRow().get(0).GetColumn("ADDRESS").GetValue());
        String CITYNAME = String.valueOf(dataTable.GetRow().get(0).GetColumn("CITYNAME").GetValue());
        String ExpressAgent = String.valueOf(dataTable.GetRow().get(0).GetColumn("ExpressAgent").GetValue());

        MailAddress mailAddress = new MailAddress();

        mailAddress.setADDRESS(ADDRESS);
        mailAddress.setCITYNAME(CITYNAME);
        mailAddress.setExpressAgent(Integer.parseInt(ExpressAgent));

        return mailAddress;
    }

    //根据ID删除订单对象-本地测试
    public Integer delMailAddressById(Long Id) throws Exception {
        String sql = "DELETE FROM MailAddress WHERE Id=" + Id;
        return DBHelperOffline.UpdateData(sql);
    }

    public Integer updateMailAddressExpress(Integer ORDERID, String ExpressNum, Integer ExpressAgent) {
        String sql = "UPDATE mailaddress SET ExpressNum ='" + ExpressNum + "',ExpressAgent=" + ExpressAgent
                + " WHERE ORDERID=" + ORDERID;
        return DBHelperOffline.UpdateData(sql);
    }

    public Integer updateExpressAgentByORDERID(Integer ORDERID, Integer ExpressAgent) {
        String sql = "UPDATE mailaddress SET ExpressAgent=" + ExpressAgent + " WHERE ORDERID=" + ORDERID;
        return DBHelperOffline.UpdateData(sql);
    }

    public Integer updateSTATEByORDERID(Long ORDERID, Integer STATE) {
        String sql = "UPDATE mailaddress SET STATE =" + STATE + " WHERE ORDERID=" + ORDERID;
        return DBHelperOffline.UpdateData(sql);
    }

    public Integer updatePRINTSTATEByORDERID(Long ORDERID, Integer PRINTSTATE) {
        String sql = "UPDATE mailaddress SET PRINTSTATE =" + PRINTSTATE + " WHERE ORDERID=" + ORDERID;
        return DBHelperOffline.UpdateData(sql);
    }

    //根据订单ID修改快递单号
    public Integer updateExpressNumByORDERID(Integer ORDERID, String ExpressNum) {
        String sql = "UPDATE mailaddress SET ExpressNum ='" + ExpressNum + "' WHERE ORDERID=" + ORDERID;
        return DBHelperOffline.UpdateData(sql);
    }

    public Integer updateRapidSendPriceByORDERID(Integer ORDERID, Double rapidSendPrice) {
        String sql = "UPDATE mailaddress SET rapidSendPrice =" + rapidSendPrice + " WHERE ORDERID=" + ORDERID;
        return DBHelperOffline.UpdateData(sql);
    }

    public Integer updateRapidSendPriceExpressAgentByORDERID(Integer ORDERID, Double rapidSendPrice,
            Integer ExpressAgent) {
        String sql = "UPDATE mailaddress SET rapidSendPrice =" + rapidSendPrice + ",ExpressAgent=" + ExpressAgent
                + " WHERE ORDERID=" + ORDERID;
        return DBHelperOffline.UpdateData(sql);
    }

}
