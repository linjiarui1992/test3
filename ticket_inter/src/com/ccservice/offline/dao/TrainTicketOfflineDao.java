package com.ccservice.offline.dao;

import java.util.List;

import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataTable;
import com.ccservice.offline.domain.TrainTicketOffline;
import com.ccservice.offline.util.TrainOrderOfflineUtil;

/**
 * @className: com.ccservice.tuniu.train.dao.TrainOrderOfflineRecordDao
 * @description: TODO -
 * @author: 郑州-技术-陈亚峰
 * @createTime: 2017年8月16日 下午03:36:02
 * @version: v 1.0
 * @since
 *
 */
public class TrainTicketOfflineDao {

    //插入
    public String addTrainTicketOffline(TrainTicketOffline trainTicketOffline) throws Exception {
        String sql = "OFFLINE_addTrainTicketOffline @TrainPid=" + trainTicketOffline.getTrainPid() + ",@OrderId="
                + trainTicketOffline.getOrderId()
                //+",@DepartTime='" + TrainOrderOfflineUtil.getTimestampByStr(trainTicketOffline.getDepartTime())

                + ",@DepartTime='" + trainTicketOffline.getDepartTime()

                + "',@Departure='" + trainTicketOffline.getDeparture() + "',@Arrival='"
                + trainTicketOffline.getArrival() + "',@TrainNo='" + trainTicketOffline.getTrainNo() + "',@TicketType="
                + trainTicketOffline.getTicketType() + ",@SeatType='" + trainTicketOffline.getSeatType()
                /*+"',@SeatNo='" + trainTicketOffline.getSeatNo()
                +"',@Coach='" + trainTicketOffline.getCoach()*/
                + "',@Price=" + trainTicketOffline.getPrice()

                + ",@CostTime='" + trainTicketOffline.getCostTime() + "',@StartTime='"
                + trainTicketOffline.getStartTime() + "',@ArrivalTime='" + trainTicketOffline.getArrivalTime() + "'";

        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
        if (dataTable.GetRow().size() == 0) {
            return "插入失败";
        }
        return String.valueOf(dataTable.GetRow().get(0).GetColumn("Id").GetValue());
    }

    //根据ID获取订单对象
    public TrainTicketOffline findTrainTicketOfflineById(Long Id) throws Exception {
        String sql = "OFFLINE_findTrainTicketOfflineById @Id=" + Id;
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        //SQLServerUtil.printTable(dataTable);

        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return (TrainTicketOffline) new BeanHanlder(TrainTicketOffline.class).handle(dataTable);
    }

    //根据ID删除订单对象-本地测试
    public Integer delTrainTicketOfflineById(Long Id) throws Exception {
        String sql = "DELETE FROM TrainTicketOffline WHERE Id=" + Id;
        return DBHelperOffline.UpdateData(sql);
    }

    public List<TrainTicketOffline> findTrainTicketOfflineListByOrderId(Long OrderId) throws Exception {
        String sql = "SELECT * FROM TrainTicketOffline WHERE OrderId=" + OrderId;
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return (List<TrainTicketOffline>) new BeanListHanlder(TrainTicketOffline.class).handle(dataTable);
    }

    public String findDepartTimeByOrderId(Long OrderId) throws Exception {
        String sql = "SELECT DepartTime FROM TrainTicketOffline WHERE OrderId=" + OrderId;
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return String.valueOf(dataTable.GetRow().get(0).GetColumn("DepartTime").GetValue());
    }

    public TrainTicketOffline findTrainTicketOfflineByTrainPid(Long TrainPid) throws Exception {
        String sql = "SELECT * FROM TrainTicketOffline WHERE TrainPid=" + TrainPid;
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return (TrainTicketOffline) new BeanHanlder(TrainTicketOffline.class).handle(dataTable);
    }

    public Integer updateDepartTimeByOrderId(String departTimeTCNEW, Long orderId) throws Exception {
        String sql = "UPDATE TrainTicketOffline SET DepartTime='"
                + TrainOrderOfflineUtil.getTimestampByStr(departTimeTCNEW) + "' WHERE OrderId=" + orderId;
        return DBHelperOffline.UpdateData(sql);
    }

}
