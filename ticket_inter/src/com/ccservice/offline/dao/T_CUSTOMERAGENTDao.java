/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.dao;

import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataTable;
import com.ccservice.offline.domain.T_CUSTOMERAGENT;
import com.ccservice.offline.util.TrainOrderOfflineUtil;

/**
 * @className: com.ccservice.offline.dao.T_CUSTOMERAGENTDao
 * @description: TODO - 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年9月4日 下午3:48:36 
 * @version: v 1.0
 * @since E
 *
 */
public class T_CUSTOMERAGENTDao {

    //插入平台各个员工信息，并返回订单号
    public String addT_CUSTOMERAGENT(T_CUSTOMERAGENT tCUSTOMERAGENT) throws Exception {
        String nowDateStr = TrainOrderOfflineUtil.getNowDateStr();
        //超时时间是从数据库中配置的？
        String sql = "OFFLINENEW_addT_CUSTOMERAGENT " + "@ID=" + tCUSTOMERAGENT.getID() + ",@C_CODE='"
                + tCUSTOMERAGENT.getC_CODE() + "',@C_AGENTTYPE=" + tCUSTOMERAGENT.getC_AGENTTYPE() + ",@C_AGENTVSDATE='"
                + TrainOrderOfflineUtil.getTimestampByStr(nowDateStr) + "',@C_AGENTVEDATE='"
                + TrainOrderOfflineUtil.getTimestampByStr(nowDateStr) + "',@C_AGENTCOMPANYNAME='"
                + tCUSTOMERAGENT.getC_AGENTCOMPANYNAME() + "',@C_AGENTSHORTNAME='"
                + tCUSTOMERAGENT.getC_AGENTSHORTNAME() + "',@C_ALLOWLEVELCOUNT=" + tCUSTOMERAGENT.getC_ALLOWLEVELCOUNT()
                + ",@C_ALLOWPROXYCOUNT=" + tCUSTOMERAGENT.getC_ALLOWPROXYCOUNT() + ",@C_AGENTCITYID="
                + tCUSTOMERAGENT.getC_AGENTCITYID() + ",@C_AGENTTEL='" + tCUSTOMERAGENT.getC_AGENTTEL()
                + "',@C_AGENTADDRESS='" + tCUSTOMERAGENT.getC_AGENTADDRESS() + "',@C_AGENTPOSTCODE='"
                + tCUSTOMERAGENT.getC_AGENTPOSTCODE() + "',@C_AGENTCONTACTNAME='"
                + tCUSTOMERAGENT.getC_AGENTCONTACTNAME() + "',@C_AGENTEMAIL='" + tCUSTOMERAGENT.getC_AGENTEMAIL()
                + "',@C_AGENTCHECKSTATUS=" + tCUSTOMERAGENT.getC_AGENTCHECKSTATUS() + ",@C_AGENTISENABLE="
                + tCUSTOMERAGENT.getC_AGENTISENABLE() + ",@C_MODIFYTIME='"
                + TrainOrderOfflineUtil.getTimestampByStr(nowDateStr) + "',@C_MODIFYUSER='"
                + tCUSTOMERAGENT.getC_MODIFYUSER() + "',@C_CREATETIME='"
                + TrainOrderOfflineUtil.getTimestampByStr(nowDateStr) + "',@C_CREATEUSER='"
                + tCUSTOMERAGENT.getC_CREATEUSER() + "',@C_PARENTSTR='" + tCUSTOMERAGENT.getC_PARENTSTR()
                + "',@C_BROKENUM=" + tCUSTOMERAGENT.getC_BROKENUM() + ",@C_CHILDBROKENUM="
                + tCUSTOMERAGENT.getC_CHILDBROKENUM() + ",@C_ALIPAYACCOUNT='" + tCUSTOMERAGENT.getC_ALIPAYACCOUNT()
                + "',@C_TENPAYACCOUNT='" + tCUSTOMERAGENT.getC_TENPAYACCOUNT() + "',@C_KUAIBILLACCOUNT='"
                + tCUSTOMERAGENT.getC_KUAIBILLACCOUNT() + "',@C_MSNQQ='" + tCUSTOMERAGENT.getC_MSNQQ()
                + "',@C_WEBSITE='" + tCUSTOMERAGENT.getC_WEBSITE() + "',@C_BIGTYPE=" + tCUSTOMERAGENT.getC_BIGTYPE()
                + ",@C_USERID=" + tCUSTOMERAGENT.getC_USERID() + ",@C_MUCODE='" + tCUSTOMERAGENT.getC_MUCODE()
                + "',@C_CACODE='" + tCUSTOMERAGENT.getC_CACODE() + "',@C_CZCODE='" + tCUSTOMERAGENT.getC_CZCODE()
                + "',@C_RUNTYPE=" + tCUSTOMERAGENT.getC_RUNTYPE() + ",@C_RUNVALUE=" + tCUSTOMERAGENT.getC_RUNVALUE()
                + ",@C_AGENTPHONE='" + tCUSTOMERAGENT.getC_AGENTPHONE() + "',@C_AGENRFAX='"
                + tCUSTOMERAGENT.getC_AGENRFAX() + "',@C_AGENTMOBILE='" + tCUSTOMERAGENT.getC_AGENTMOBILE()
                + "',@C_AGENTOTHER='" + tCUSTOMERAGENT.getC_AGENTOTHER() + "',@C_INDUSTRY='"
                + tCUSTOMERAGENT.getC_INDUSTRY() + "',@C_FINANCENAME='" + tCUSTOMERAGENT.getC_FINANCENAME()
                + "',@C_FINANCEPHONE='" + tCUSTOMERAGENT.getC_FINANCEPHONE() + "',@C_FINANCEFAX='"
                + tCUSTOMERAGENT.getC_FINANCEFAX() + "',@C_FINANCEMOBILE='" + tCUSTOMERAGENT.getC_FINANCEMOBILE()
                + "',@C_FINANCEEMAIL='" + tCUSTOMERAGENT.getC_FINANCEEMAIL() + "',@C_SELFCODE="
                + tCUSTOMERAGENT.getC_SELFCODE() + ",@C_AIRPORTCODE='" + tCUSTOMERAGENT.getC_AIRPORTCODE()
                + "',@C_ISMODIFYRET='" + tCUSTOMERAGENT.getC_ISMODIFYRET() + "',@C_SMSCOUNT="
                + tCUSTOMERAGENT.getC_SMSCOUNT() + ",@C_AGENTJIBIE=" + tCUSTOMERAGENT.getC_AGENTJIBIE() + ",@C_CITYID="
                + tCUSTOMERAGENT.getC_CITYID() + ",@C_PARENTID=" + tCUSTOMERAGENT.getC_PARENTID()
                + ",@C_ISALLOWMONTHPAY=" + tCUSTOMERAGENT.getC_ISALLOWMONTHPAY() + ",@C_OUTTICKETMANTEL='"
                + tCUSTOMERAGENT.getC_OUTTICKETMANTEL() + "',@C_OUTTICKETMANMSNQQ='"
                + tCUSTOMERAGENT.getC_OUTTICKETMANMSNQQ() + "',@C_BACKTICKETMANTEL='"
                + tCUSTOMERAGENT.getC_BACKTICKETMANTEL() + "',@C_BACKTICKETMANMSNQQ='"
                + tCUSTOMERAGENT.getC_BACKTICKETMANMSNQQ() + "',@C_WORKTIMEBEGIN='"
                + TrainOrderOfflineUtil.getTimestampByStr(nowDateStr) + "',@C_WORKTIMEEND='"
                + TrainOrderOfflineUtil.getTimestampByStr(nowDateStr) + "',@C_VMONEY='" + tCUSTOMERAGENT.getC_VMONEY()
                + "',@C_OPENABLE=" + tCUSTOMERAGENT.getC_OPENABLE() + ",@C_B2COPENABLE="
                + tCUSTOMERAGENT.getC_B2COPENABLE() + ",@C_CHINAPNRCOUNT='" + tCUSTOMERAGENT.getC_CHINAPNRCOUNT()
                + "',@C_SMSMONEY=" + tCUSTOMERAGENT.getC_SMSMONEY() + ",@C_SMSCOUNTER='"
                + tCUSTOMERAGENT.getC_SMSCOUNTER() + "',@C_SMSPWD='" + tCUSTOMERAGENT.getC_SMSPWD() + "',@C_ISPARTNER="
                + tCUSTOMERAGENT.getC_ISPARTNER() + ",@C_ITINERARYCOUNT=" + tCUSTOMERAGENT.getC_ITINERARYCOUNT()
                + ",@C_VMONEYPWD='" + tCUSTOMERAGENT.getC_VMONEYPWD() + "',@C_MANAGERUID="
                + tCUSTOMERAGENT.getC_MANAGERUID() + ",@C_SPECIALDATA=" + tCUSTOMERAGENT.getC_SPECIALDATA()
                + ",@C_DNSAGENTID=" + tCUSTOMERAGENT.getC_DNSAGENTID() + ",@C_DISABLEMONEY="
                + tCUSTOMERAGENT.getC_DISABLEMONEY() + ",@C_GUARANMONEY=" + tCUSTOMERAGENT.getC_GUARANMONEY()
                + ",@C_HOTELSUPPLIERFLAG=" + tCUSTOMERAGENT.getC_HOTELSUPPLIERFLAG() + ",@C_HOTELSUPPLIERID="
                + tCUSTOMERAGENT.getC_HOTELSUPPLIERID() + ",@C_SMSMOVEMONEY=" + tCUSTOMERAGENT.getC_SMSMOVEMONEY()
                + ",@C_SELLERNAME='" + tCUSTOMERAGENT.getC_SELLERNAME() + "',@C_SELLERPHONE='"
                + tCUSTOMERAGENT.getC_SELLERPHONE() + "',@C_SMSTICKETFLAG=" + tCUSTOMERAGENT.getC_SMSTICKETFLAG()
                + ",@C_SMSMOUNTCLOCK=" + tCUSTOMERAGENT.getC_SMSMOUNTCLOCK() + ",@C_SMSCLOCKMOUNT='"
                + tCUSTOMERAGENT.getC_SMSCLOCKMOUNT() + "',@C_SMSGETTELPHONE='" + tCUSTOMERAGENT.getC_SMSGETTELPHONE()
                + "',@C_ISTENPAYPARTNER=" + tCUSTOMERAGENT.getC_ISTENPAYPARTNER() + ",@TrainSingleFactorage="
                + tCUSTOMERAGENT.getTrainSingleFactorage() + ",@ProvinceId=" + tCUSTOMERAGENT.getProvinceId()
                + ",@ProvinceName='" + tCUSTOMERAGENT.getProvinceName() + "',@CityId=" + tCUSTOMERAGENT.getCityId()
                + ",@CityName='" + tCUSTOMERAGENT.getCityName() + "',@RegionID=" + tCUSTOMERAGENT.getRegionID()
                + ",@RegionName='" + tCUSTOMERAGENT.getRegionName() + "',@TownID=" + tCUSTOMERAGENT.getTownID()
                + ",@TownName='" + tCUSTOMERAGENT.getTownName() + "',@AgentHeat=" + tCUSTOMERAGENT.getAgentHeat()
                + ",@FKBusinessIdentityID=" + tCUSTOMERAGENT.getFKBusinessIdentityID() + ",@MonthPayNum='"
                + tCUSTOMERAGENT.getMonthPayNum() + "',@PayType='" + tCUSTOMERAGENT.getPayType() + "'";
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
        if (dataTable.GetRow().size() == 0) {
            return "平台各个员工信息插入失败";
        }
        return String.valueOf(dataTable.GetRow().get(0).GetColumn("Id").GetValue());
    }

    //根据ID获取员工信息对象
    public T_CUSTOMERAGENT findT_CUSTOMERAGENTById(Double Id) throws Exception {
        String sql = "OFFLINENEW_findT_CUSTOMERAGENTById @Id=" + Id;
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        //SQLServerUtil.printTable(dataTable);

        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return (T_CUSTOMERAGENT) new BeanHanlder(T_CUSTOMERAGENT.class).handle(dataTable);
    }

    public String findC_AGENTCOMPANYNAMEByOrderId(Long OrderId) throws Exception {
        String sql = "SELECT C_AGENTCOMPANYNAME FROM T_CUSTOMERAGENT WHERE ID = (SELECT AgentId FROM TrainOrderOffline WHERE Id = "
                + OrderId + ")";
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return String.valueOf(dataTable.GetRow().get(0).GetColumn("C_AGENTCOMPANYNAME").GetValue());
    }

    public String findCityNameByOrderId(Long OrderId) throws Exception {
        String sql = "SELECT CityName FROM T_CUSTOMERAGENT WHERE ID = (SELECT AgentId FROM TrainOrderOffline WHERE Id = "
                + OrderId + ")";
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return String.valueOf(dataTable.GetRow().get(0).GetColumn("CityName").GetValue());
    }

    public String findC_AGENTCOMPANYNAMEByID(Double ID) throws Exception {
        String sql = "SELECT C_AGENTCOMPANYNAME FROM T_CUSTOMERAGENT WHERE ID = " + ID;
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return String.valueOf(dataTable.GetRow().get(0).GetColumn("C_AGENTCOMPANYNAME").GetValue());
    }

    //根据ID删除员工信息-本地测试
    public Integer delT_CUSTOMERAGENTById(Double Id) {
        String sql = "DELETE FROM T_CUSTOMERAGENT WHERE Id=" + Id;
        return DBHelperOffline.UpdateData(sql);
    }

}
