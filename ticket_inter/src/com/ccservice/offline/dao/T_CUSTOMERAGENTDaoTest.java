/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.dao;

import com.ccservice.offline.domain.T_CUSTOMERAGENT;

/**
 * @className: com.ccservice.tuniu.train.dao.T_CUSTOMERAGENTDaoTest
 * @description: TODO - 
 * @author: 郑州-技术-杨威   E-mail:653395455@qq.com
 * @createTime: 2017年9月04日 下午5:20:11 
 * @version: v 1.0
 * @since 
 *
 */
public class T_CUSTOMERAGENTDaoTest {
    private T_CUSTOMERAGENTDao t_CUSTOMERAGENTDao = new T_CUSTOMERAGENTDao();

    public void testAddTrainOrderOffline() throws Exception {
        //订单信息入库
        T_CUSTOMERAGENT t_CUSTOMERAGENT = new T_CUSTOMERAGENT();

        t_CUSTOMERAGENT.setC_CODE("这就是个测试而已");
        //不能为空的字段的值
        t_CUSTOMERAGENT.setAgentHeat(Integer.valueOf("100"));
        t_CUSTOMERAGENT.setC_AGENTTYPE((double) 2);
        t_CUSTOMERAGENT.setC_AGENTCITYID((double) 2);
        t_CUSTOMERAGENT.setC_AGENTCHECKSTATUS((double) 3);
        t_CUSTOMERAGENT.setC_AGENTISENABLE((double) 3);
        t_CUSTOMERAGENT.setC_BIGTYPE((double) 2);
        t_CUSTOMERAGENT.setC_USERID((double) 2);
        t_CUSTOMERAGENT.setC_RUNTYPE((double) 2);
        t_CUSTOMERAGENT.setC_RUNVALUE((double) 2);
        t_CUSTOMERAGENT.setC_SELFCODE(1);
        t_CUSTOMERAGENT.setC_ISMODIFYRET((double) 2);
        t_CUSTOMERAGENT.setC_CITYID((double) 2);
        t_CUSTOMERAGENT.setC_PARENTID((double) 2);
        t_CUSTOMERAGENT.setC_ISALLOWMONTHPAY((double) 2);
        t_CUSTOMERAGENT.setC_VMONEY((double) 3);
        t_CUSTOMERAGENT.setC_OPENABLE(3);
        t_CUSTOMERAGENT.setC_B2COPENABLE(1);
        t_CUSTOMERAGENT.setC_SMSMONEY((double) 3);
        t_CUSTOMERAGENT.setC_ISPARTNER(1);
        t_CUSTOMERAGENT.setC_ITINERARYCOUNT(2);
        t_CUSTOMERAGENT.setC_MANAGERUID((double) 2);
        t_CUSTOMERAGENT.setC_SPECIALDATA((double) 2);
        t_CUSTOMERAGENT.setC_DNSAGENTID((double) 2);
        t_CUSTOMERAGENT.setC_DISABLEMONEY((double) 2);
        t_CUSTOMERAGENT.setC_GUARANMONEY((double) 2);
        t_CUSTOMERAGENT.setC_HOTELSUPPLIERFLAG(1);
        t_CUSTOMERAGENT.setC_HOTELSUPPLIERID((double) 2);
        t_CUSTOMERAGENT.setC_SMSMOVEMONEY((double) 2);
        t_CUSTOMERAGENT.setC_SMSTICKETFLAG(1);
        t_CUSTOMERAGENT.setC_SMSMOUNTCLOCK((double) 2);
        t_CUSTOMERAGENT.setC_SMSCLOCKMOUNT((double) 2);
        t_CUSTOMERAGENT.setC_ISTENPAYPARTNER(1);
        t_CUSTOMERAGENT.setProvinceId(1);
        t_CUSTOMERAGENT.setCityId(1);
        t_CUSTOMERAGENT.setRegionID(1);
        t_CUSTOMERAGENT.setTownID(1);
        t_CUSTOMERAGENT.setFKBusinessIdentityID(1);
        t_CUSTOMERAGENT.setID((double) 472);

        System.out.println(t_CUSTOMERAGENTDao.addT_CUSTOMERAGENT(t_CUSTOMERAGENT));
    }

    public void testdelT_CUSTOMERAGENTById() throws Exception {
        System.out.println(t_CUSTOMERAGENTDao.delT_CUSTOMERAGENTById((double) 472));
    }

    public void testFindT_CUSTOMERAGENTById() throws Exception {
        System.out.println(t_CUSTOMERAGENTDao.findT_CUSTOMERAGENTById((double) 460).toString());
    }

    public void testFindC_AGENTCOMPANYNAMEByID() throws Exception {
        System.out.println(t_CUSTOMERAGENTDao.findC_AGENTCOMPANYNAMEByID((double) 412).toString());
    }

    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        T_CUSTOMERAGENTDaoTest t_CUSTOMERAGENTDaoTest = new T_CUSTOMERAGENTDaoTest();

        //trainOrderOfflineDaoTest.testAddTrainOrderOffline();
        //trainOrderOfflineDaoTest.testFindTrainOrderOfflineById();
        //trainOrderOfflineDaoTest.testFindIsLockCallbackById();
        //t_CUSTOMERAGENTDaoTest.testdelT_CUSTOMERAGENTById();
        t_CUSTOMERAGENTDaoTest.testFindC_AGENTCOMPANYNAMEByID();
        //trainOrderOfflineDaoTest.testUpdateOrderTimeoutById();

        //trainOrderOfflineDaoTest.testFindTrainOrderOfflineByOrderNumberOnline();

        //trainOrderOfflineDaoTest.testTuNiuDesUtil();

        //trainOrderOfflineDaoTest.getCancelOrderParam();

        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间： " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间： " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间： " + runTimeS / 60 + "min：" + runTimeS % 60 + "s");//分
    }
}
