/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.dao;

import java.util.ArrayList;
import java.util.List;

import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataRow;
import com.ccervice.util.db.DataTable;
import com.ccservice.offline.domain.TrainOfflineRefundRecord;
import com.ccservice.offline.domain.TrainOrderOffline;

/**
 * @className: com.ccservice.offline.dao.TrainOfflineTomAgentAddDao
 * @description: TODO - 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年9月4日 下午6:23:17 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOfflineRefundRecordDaoTest {
    private TrainOfflineRefundRecordDao trainOfflineRefundRecordDao = new TrainOfflineRefundRecordDao();

    public void testAddTrainOfflineRefundRecord() throws Exception {
        TrainOfflineRefundRecord trainOfflineRefundRecord = new TrainOfflineRefundRecord();
        
        trainOfflineRefundRecord.setOrderId(6);
        trainOfflineRefundRecord.setOperateName("11");
        trainOfflineRefundRecord.setRefundSum("22");
        trainOfflineRefundRecord.setOperateTime("33");
        trainOfflineRefundRecord.setRefundType(4);
        trainOfflineRefundRecord.setSeqId("55");
        
        System.out.println(trainOfflineRefundRecordDao.addTrainOfflineRefundRecord(trainOfflineRefundRecord));
    }

    public void testFindTrainOfflineRefundRecordById() throws Exception {
        System.out.println(trainOfflineRefundRecordDao.findTrainOfflineRefundRecordById(43));
    }

    public void testDelTrainOfflineRefundRecordById() throws Exception {
        System.out.println(trainOfflineRefundRecordDao.delTrainOfflineRefundRecordById(42));
    }

    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        TrainOfflineRefundRecordDaoTest trainOfflineRefundRecordDaoTest = new TrainOfflineRefundRecordDaoTest();
        
        //trainOfflineRefundRecordDaoTest.testAddTrainOfflineRefundRecord();
        //trainOfflineRefundRecordDaoTest.testFindTrainOfflineRefundRecordById();
        trainOfflineRefundRecordDaoTest.testDelTrainOfflineRefundRecordById();

        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间： " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间： " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间： " + runTimeS / 60 + "min：" + runTimeS % 60 + "s");//分
    }
    
}
