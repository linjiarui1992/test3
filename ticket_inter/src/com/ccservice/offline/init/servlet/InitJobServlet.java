/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.init.servlet;

import java.util.Random;

import javax.servlet.http.HttpServlet;

import com.ccservice.offline.init.thread.InitJobThread;

/**
 * @className: com.ccservice.offline.init.InitJobServlet
 * @description: TODO - 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年10月13日 上午11:09:13 
 * @version: v 1.0
 * @since 
 *
 */
public class InitJobServlet extends HttpServlet {
    private static final String LOGNAME = "线下票相关初始化任务-效率统计JOB开启和供应商排名JOB开启工具类";

    private int r1 = new Random().nextInt(10000000);

    @Override
    public void init() {
        System.out.println(LOGNAME+"队列准备开启");
        new Thread(new InitJobThread()).start();
    }

}
