/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.init.thread;

import java.util.Random;

import com.ccservice.Util.file.WriteLog;
import com.ccservice.offline.util.ExceptionTCUtil;
import com.ccservice.offline.util.HttpUtil;

/**
 * @className: com.ccservice.offline.init.thread.InitJobThread
 * @description: TODO - 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年10月13日 上午11:18:13 
 * @version: v 1.0
 * @since 
 *
 */
public class InitJobThread implements Runnable {
    private static final String LOGNAME = "线下票相关初始化任务-效率统计JOB开启和供应商排名JOB开启工具类";

    private int r1 = new Random().nextInt(10000000);

    @Override
    public void run() {
        try {
            //Thread.sleep(1000*15);//本地测试一下
            Thread.sleep(1000 * 60 * 2);//2min之后-tomcat启动之后，自动开启
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(LOGNAME + "队列已开启");

        WriteLog.write(LOGNAME, r1 + ":" + LOGNAME + "队列已开启");

        HttpUtil httpUtil = new HttpUtil();

        String url = "";

        /**
         * #途牛是测试分单 - 添加有job任务的随机启动配置
         * #server 127.0.0.1:9020;
         * 
         * 之后这个定时任务重新迁移到 - 9010 上去 - 方便兼容之前的维护
         */
        try {
            //效率统计JOB开启 - http://121.40.241.126:9010/ticket_inter/job/JobTrainOfflineFlashData.jsp?type=1
            //url = "http://ws.peisong.51kongtie.com/ticket_inter/job/JobTrainOfflineFlashData.jsp?type=1";
            url = "http://121.40.241.126:9010/ticket_inter/job/JobTrainOfflineFlashData.jsp?type=1";
            //url = "http://121.40.241.126:9020/ticket_inter/job/JobTrainOfflineFlashData.jsp?type=1";
            initEfficiencyJob(httpUtil, url);

            //供应商排名JOB开启 - http://121.40.241.126:9010/ticket_inter/job/JobTrainOfflineRanking.jsp?type=1
            //url = "http://ws.peisong.51kongtie.com/ticket_inter/job/JobTrainOfflineRanking.jsp?type=1";
            url = "http://121.40.241.126:9010/ticket_inter/job/JobTrainOfflineRanking.jsp?type=1";
            //url = "http://121.40.241.126:9020/ticket_inter/job/JobTrainOfflineRanking.jsp?type=1";
            initRankingJob(httpUtil, url);

            //供应商排名JOB开启 - http://121.40.241.126:9010/ticket_inter/job/JobTrainOfflineRanking.jsp?type=1
            //url = "http://ws.peisong.51kongtie.com/ticket_inter/job/JobTrainOfflineRanking.jsp?type=1";
            url = "http://121.40.241.126:9010/ticket_inter/job/JobNightOrderDistribution.jsp?type=1";
            //url = "http://121.40.241.126:9020/ticket_inter/job/JobTrainOfflineRanking.jsp?type=1";
            initNightOrderJob(httpUtil, url);

            //供应商排名JOB开启 - http://121.40.241.126:9010/ticket_inter/job/JobTrainOfflineRanking.jsp?type=1
            //url = "http://ws.peisong.51kongtie.com/ticket_inter/job/JobTrainOfflineRanking.jsp?type=1";
            url = "http://121.40.241.126:9010/ticket_inter/job/JobTrainOfflineReminder.jsp?type=1";
            //url = "http://121.40.241.126:9020/ticket_inter/job/JobTrainOfflineRanking.jsp?type=1";
            reminderJob(httpUtil, url);

            //白天单JOB开启 - http://121.40.241.126:9010/ticket_inter/job/JobDayTimeOrderDistribution.jsp?type=1
            //url = "http://ws.peisong.51kongtie.com/ticket_inter/job/JobDayTimeOrderDistribution.jsp?type=1";
            url = "http://121.40.241.126:9010/ticket_inter/job/JobDayTimeOrderDistribution.jsp?type=1";
            //url = "http://121.40.241.126:9020/ticket_inter/job/JobDayTimeOrderDistribution.jsp?type=1";
            initDayTimeOrderJob(httpUtil, url);
        }
        catch (Exception e) {
            ExceptionTCUtil.handleTCException(e);
        }
    }

    private void initRankingJob(HttpUtil httpUtil, String url) throws Exception {
        httpUtil.doGet(url, "");
        System.out.println("效率统计JOB开启完毕");
        //System.out.println(httpUtil.doGet(url, ""));
    }

    private void initEfficiencyJob(HttpUtil httpUtil, String url) throws Exception {
        httpUtil.doGet(url, "");
        System.out.println("供应商排名JOB开启完毕");
        //System.out.println(httpUtil.doGet(url, ""));
    }

    private void initNightOrderJob(HttpUtil httpUtil, String url) throws Exception {
        httpUtil.doGet(url, "");
        System.out.println("供应商夜间单轮询JOB开启完毕");
        //System.out.println(httpUtil.doGet(url, ""));
    }

    private void reminderJob(HttpUtil httpUtil, String url) throws Exception {
        httpUtil.doGet(url, "");
        System.out.println("订单催单监控JOB开启完毕");
        //System.out.println(httpUtil.doGet(url, ""));
    }

    private void initDayTimeOrderJob(HttpUtil httpUtil, String url) throws Exception {
        httpUtil.doGet(url, "");
        System.out.println("供应商白天单JOB开启完毕");
        //System.out.println(httpUtil.doGet(url, ""));
    }

}
