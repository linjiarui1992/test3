package com.ccservice.common.util;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.service12306.bean.TrainOrderReturnBean;
import com.ccservice.common.DurationEnum;
import com.ccservice.common.GetStepCostTime;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.thread.db.TrainCreateOrderDBUtil;
import com.ccservice.qunar.util.ExceptionUtil;

public class DurationOperation {
    /**
     * 保存数据
     * @time:2017年6月21日下午4:40:41
     * @auto:baozz
     * @param costTime
     * @param orderid
     */
    public void saveDB(GetStepCostTime costTime, long orderid) {
        if (costTime != null && costTime.isFlag()) {
            JSONObject datas = costTime.getDatas(new JSONObject());
            try {
                saveDurationData(datas, orderid);
                WriteLog.write("TrainCreateOrder_createOrderStart", "最终数据: " + datas + ", 订单号为:" + orderid);
            }
            catch (Exception e) {
                ExceptionUtil.writelogByException("DurationOperation_saveDB_Exception", e, datas + "订单号:" + orderid);
            }
        }
    }

    /**
     * 收集数据
     * @time:2017年6月21日下午4:40:51
     * @auto:baozz
     * @param costTime
     * @param returnob
     */
    public void storeData(GetStepCostTime costTime, TrainOrderReturnBean returnob) {
        try {
            if (costTime != null && costTime.isFlag()) {
                JSONObject duractionData = returnob.getDuractionData();
                if (duractionData == null) {
                    duractionData = new JSONObject();
                }
                WriteLog.write("DurationOperation_storeData", duractionData.toJSONString());
                costTime.superaddition(duractionData);
            }
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("DurationOperation_storeData_Exception", e, costTime + "," + returnob);
        }
    }

    //    public static void main(String[] args) {
    //        TrainOrderReturnBean trainOrderReturnBean = new TrainOrderReturnBean();
    //        trainOrderReturnBean
    //                .setDuractionData(JSONObject
    //                        .parseObject("{\"datas\":[{\"round\":1,\"steps\":[{\"endTimeValue\":\"2017-06-22 11:17:56.279\",\"endTimeKey\":\"jointReptitleParameterEndTime\",\"costValue\":94,\"stepNo\":1005,\"costKey\":\"jointReptitleParameterCost\"},{\"endTimeValue\":\"2017-06-22 11:18:08.232\",\"endTimeKey\":\"reptitleEndTime\",\"costValue\":11953,\"stepNo\":1006,\"costKey\":\"reptitleEndCost\"},{\"endTimeValue\":\"2017-06-22 11:18:06.089\",\"endTimeKey\":\"suborderTrueSleepTimeEndtime\",\"costValue\":2000,\"stepNo\":2023,\"costKey\":\"suborderTrueSleepTimeCost\"},{\"endTimeValue\":\"2017-06-22 11:18:06.476\",\"endTimeKey\":\"queryOrderEndtime\",\"costValue\":387,\"stepNo\":2024,\"costKey\":\"queryOrderCost\"},{\"endTimeValue\":\"2017-06-22 11:18:06.933\",\"endTimeKey\":\"queueResultEndtime\",\"costValue\":457,\"stepNo\":2025,\"costKey\":\"queueResultCost\"},{\"endTimeValue\":\"2017-06-22 11:18:08.222\",\"endTimeKey\":\"checkUnfinishedOrderEndtime\",\"costValue\":1289,\"stepNo\":2026,\"costKey\":\"checkUnfinishedOrderCost\"},{\"endTimeValue\":\"2017-06-22 11:18:08.235\",\"endTimeKey\":\"deletePassengerEndtime\",\"costValue\":13,\"stepNo\":2027,\"costKey\":\"deletePassengerCost\"},{\"endTimeValue\":\"2017-06-22 11:18:08.321\",\"endTimeKey\":\"callBackSuccesEndtime\",\"costValue\":86,\"stepNo\":2028,\"costKey\":\"callBackSuccesCost\"},{\"endTimeValue\":\"2017-06-22 11:18:08.321\",\"endTimeKey\":\"repCreateOrderEndtime\",\"costValue\":0,\"stepNo\":2030,\"costKey\":\"repCreateOrderCost\"}]},{\"round\":2,\"steps\":[{\"endTimeValue\":\"2017-06-22 11:17:56.477\",\"endTimeKey\":\"parseDataEndtime\",\"costValue\":1,\"stepNo\":2001,\"costKey\":\"parseDataCost\"},{\"endTimeValue\":\"2017-06-22 11:17:56.722\",\"endTimeKey\":\"initQueryPageEndtime\",\"costValue\":245,\"stepNo\":2002,\"costKey\":\"initQueryPageCost\"},{\"endTimeValue\":\"2017-06-22 11:17:56.921\",\"endTimeKey\":\"getDynamicEndtime\",\"costValue\":199,\"stepNo\":2003,\"costKey\":\"getDynamicCost\"},{\"endTimeValue\":\"2017-06-22 11:17:58.152\",\"endTimeKey\":\"queryLeftTicketsEndtime\",\"costValue\":1231,\"stepNo\":2004,\"costKey\":\"queryLeftTicketsCost\"},{\"endTimeValue\":\"2017-06-22 11:17:58.408\",\"endTimeKey\":\"checkOnlineEndtime\",\"costValue\":256,\"stepNo\":2005,\"costKey\":\"checkOnlineCost\"},{\"endTimeValue\":\"2017-06-22 11:17:58.718\",\"endTimeKey\":\"submitBookingEndtime\",\"costValue\":310,\"stepNo\":2007,\"costKey\":\"submitBookingCost\"},{\"endTimeValue\":\"2017-06-22 11:17:59.133\",\"endTimeKey\":\"orderPageHtmlInitEndtime\",\"costValue\":415,\"stepNo\":2008,\"costKey\":\"orderPageHtmlInitCost\"},{\"endTimeValue\":\"2017-06-22 11:17:59.309\",\"endTimeKey\":\"passengerInfoJsEndtime\",\"costValue\":176,\"stepNo\":2009,\"costKey\":\"passengerInfoJsCost\"},{\"endTimeValue\":\"2017-06-22 11:17:59.309\",\"endTimeKey\":\"passengerListEndtime\",\"costValue\":0,\"stepNo\":2010,\"costKey\":\"passengerListCost\"},{\"endTimeValue\":\"2017-06-22 11:17:59.31\",\"endTimeKey\":\"viewLeftTicketsEndtime\",\"costValue\":1,\"stepNo\":2011,\"costKey\":\"viewLeftTicketsCost\"},{\"endTimeValue\":\"2017-06-22 11:17:59.598\",\"endTimeKey\":\"addPassengersEndtime\",\"costValue\":288,\"stepNo\":2012,\"costKey\":\"addPassengersCost\"},{\"endTimeValue\":\"2017-06-22 11:17:59.777\",\"endTimeKey\":\"getDynamicEndtime\",\"costValue\":179,\"stepNo\":2003,\"costKey\":\"getDynamicCost\"},{\"endTimeValue\":\"2017-06-22 11:17:59.974\",\"endTimeKey\":\"dwonPictureEndtime\",\"costValue\":197,\"stepNo\":2013,\"costKey\":\"dwonPictureCost\"},{\"endTimeValue\":\"2017-06-22 11:18:00.295\",\"endTimeKey\":\"checkOrderInfoEndtime\",\"costValue\":321,\"stepNo\":2018,\"costKey\":\"checkOrderInfoCost\"},{\"endTimeValue\":\"2017-06-22 11:18:00.712\",\"endTimeKey\":\"viewNumberLineEndtime\",\"costValue\":417,\"stepNo\":2020,\"costKey\":\"viewNumberLineCost\"},{\"endTimeValue\":\"2017-06-22 11:18:01.455\",\"endTimeKey\":\"afterQueueSleepTimeEndtime\",\"costValue\":743,\"stepNo\":2021,\"costKey\":\"afterQueueSleepTimeCost\"},{\"endTimeValue\":\"2017-06-22 11:18:03.54\",\"endTimeKey\":\"submitOrderEndtime\",\"costValue\":2085,\"stepNo\":2022,\"costKey\":\"submitOrderCost\"},{\"endTimeValue\":\"2017-06-22 11:18:03.54\",\"endTimeKey\":\"suborderTrueSleepTimeEndtime\",\"costValue\":0,\"stepNo\":2023,\"costKey\":\"suborderTrueSleepTimeCost\"},{\"endTimeValue\":\"2017-06-22 11:18:04.089\",\"endTimeKey\":\"queryOrderEndtime\",\"costValue\":549,\"stepNo\":2024,\"costKey\":\"queryOrderCost\"}]}]}"));
    //        new DurationOperation().storeData(new GetStepCostTime(true), trainOrderReturnBean);
    //    }

    /**
     * 进行时长数据的保存
     * @time:2017年6月20日下午8:13:03
     * @auto:baozz
     * @param datas
     * @param trainorderid
     */
    private void saveDurationData(JSONObject datas, long orderid) {
        if (datas != null && datas.containsKey("datas")) {
            JSONArray jsonArray = datas.getJSONArray("datas");
            JSONArray general = new JSONArray();
            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject circulation = jsonArray.getJSONObject(i);
                if (circulation.containsKey("steps")) {
                    JSONArray steps = circulation.getJSONArray("steps");
                    general.addAll(steps);
                }
            }
            String jsonStr = general.toJSONString();
            String sql = "insertDuractionMessage @jsonStr = '" + jsonStr + "' ,@orderNumber = " + orderid;
            try {
                TrainCreateOrderDBUtil.findMapResultByProcedure(sql);
                WriteLog.write("TrainCreateOrder_saveDurationData", sql);
            }
            catch (Exception e) {
                ExceptionUtil.writelogByException("DurationOperation_saveDurationData_Exception", e, sql);
            }
        }
    }

    /**
     * 初始化
     * @time:2017年6月23日下午1:29:14
     * @auto:baozz
     * @param costTime
     * @param trainorderid
     */
    public void init(GetStepCostTime costTime, long trainorderid) {
        if (costTime.isFlag()) {
            Map dbMap = null;
            Object startTimeObj = null;
            try {
                String sql = "[dbo].[getDurationStartTime] @orderNumber = '" + trainorderid + "'";
                List list = TrainCreateOrderDBUtil.findMapResultByProcedure(sql);
                if (list.size() > 0) {
                    dbMap = (Map) list.get(0);
                    startTimeObj = dbMap.get("StartTime");
                    if (startTimeObj instanceof String) {
                        String startTimeStr = (String) startTimeObj;
                        Timestamp timestamp = Timestamp.valueOf(startTimeStr);
                        costTime.setStepStartTime(timestamp.getTime());
                    }
                }
                costTime.add(DurationEnum.SEND_MQ);
                WriteLog.write("DurationOperation_init", "订单id:" + trainorderid + ",开始时间" + startTimeObj);
            }
            catch (Exception e) {
                ExceptionUtil.writelogByException("DurationOperation_init_Exception", e, dbMap + "," + startTimeObj);
            }
        }
    }
}
