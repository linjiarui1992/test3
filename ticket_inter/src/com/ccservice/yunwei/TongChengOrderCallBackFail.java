/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.yunwei;

import com.ccservice.tongcheng.servlet.TrainTongChengOfflineOrderCallbackFailServletCN;

/**
 * @className: com.ccservice.yunwei.TongChengOrderCallBackFail
 * @description: TODO - 同程批量驳回
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年9月28日 上午9:57:49 
 * @version: v 1.0
 * @since 
 *
 */
public class TongChengOrderCallBackFail {
    
    /**
     * @description: TODO - 同程的订单驳回
     * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
     * @createTime: 2017年9月28日 上午10:41:27
     * @throws Exception
     */
    private void tongChengOrderCallBackFail() throws Exception {
        String orderidStr = "1323614";
        
        TrainTongChengOfflineOrderCallbackFailServletCN trainTongChengOfflineOrderCallbackFailServletCN = new TrainTongChengOfflineOrderCallbackFailServletCN();
        trainTongChengOfflineOrderCallbackFailServletCN.tongChengOrderCallBackFail(orderidStr);
    }
    
    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        TongChengOrderCallBackFail tongChengOrderCallBackFail = new TongChengOrderCallBackFail();

        tongChengOrderCallBackFail.tongChengOrderCallBackFail();
        
        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间： " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间： " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间： " + runTimeS / 60 + "min：" + runTimeS % 60 + "s");//分
    }
}
