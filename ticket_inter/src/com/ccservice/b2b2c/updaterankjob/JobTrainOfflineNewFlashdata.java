package com.ccservice.b2b2c.updaterankjob;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;

import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.updaterankjob.util.JobUtils;
import com.ccservice.inter.server.Server;

/**
 * 2017年11月15日9:16:44
 * @author zpy
 * 供应商新效率统计
 *
 */
public class JobTrainOfflineNewFlashdata implements Job {

    private int r1 = new Random().nextInt(10000000);

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        //----写入日志
        WriteLog.write("JobTrainOfflineFlashdataAndRanking_expr", r1 + "-begin...");

        //        String day = DateUtil.getNowDateStr();
        String day = "2017-09-19";

        try {
            efficiencyStat(day);
            rankingCalc(day);
        }
        catch (Exception e) {
            WriteLog.write("JobTrainOfflineFlashdataAndRanking_expr", r1 + "-main异常-e-->" + e);
            e.printStackTrace();
        }

    }

    /**
     * 效率统计
     * @param day
     */
    private void efficiencyStat(String day) throws Exception {
        try {
            String dates = day;
            String beginTime = dates + " 00:06:00";
            String endTime = dates + " 23:00:00";

            //所有当天的出票成功的agentid
            String sql1 = "select distinct agentId FROM TrainOrderOffline with(nolock) where CreateTime between '"
                    + beginTime + "' and '" + endTime + "' and OrderStatus=2  order by agentId";
            List listagent = Server.getInstance().getSystemService().findMapResultBySql(sql1, null);
            //大于0执行
            if (listagent.size() > 0) {
                for (int i = 0; i < listagent.size(); i++) {
                    Map map = (Map) listagent.get(i);
                    String agentId = map.get("agentId").toString();//获得代售点id

                    //获得当天单个代售点的所有成功记录
                    String sql21 = "SELECT fktrainOrderOfflineId as offlineId FROM TrainOrderOfflineRecord with(nolock) WHERE ProviderAgentid="
                            + agentId + " AND dealresult=1 and DistributionTime between '" + beginTime + "' and  '"
                            + endTime + "' group by FKTrainOrderOfflineId";
                    List listtime = Server.getInstance().getSystemService().findMapResultBySql(sql21, null);
                    int alltime = 0; //成功总时长 
                    String letterSix = "";//小于六分钟的orderid
                    String biggerSix = "";//大于六分钟的orderid
                    String biggerTen = "";//大于十分钟的orderid
                    double successavg = 0;//成功率
                    String allordersyes = "";//成功出票数量
                    String allorders = "0";//所有订单数量
                    //循环处理成功的订单
                    for (int j = 0; j < listtime.size(); j++) {
                        Map map21 = (Map) listtime.get(j);
                        //不同采购商的记录处理稍有不同,此条件dealResult in(0,4)是为了兼容同程途牛的记录,order by pkId desc为了兼容重新分配的业务
                        String sql2 = "select pkId,fktrainOrderOfflineId,responsetime-distributiontime as chatTime,dealResult from TrainOrderOfflineRecord with(nolock) where FKTrainOrderOfflineId="
                                + map21.get("offlineId").toString() + " and ProviderAgentid=" + agentId
                                + " and ResponseTime>=DistributionTime  and dealResult in(0,4) order by pkId desc";
                        List listtimeValue = Server.getInstance().getSystemService().findMapResultBySql(sql2, null);

                        if (listtimeValue.size() > 0) {
                            //过渡分单出去后,又分回来的情况.选择最后的分单时间
                            Map map2 = (Map) listtimeValue.get(0);

                            //间隔时间
                            String chatTime = map2.get("chatTime").toString();

                            //算出所差时长(s)
                            long ti = Integer.parseInt(chatTime.substring(11, 13)) * 3600
                                    + Integer.parseInt(chatTime.substring(14, 16)) * 60
                                    + Integer.parseInt(chatTime.substring(17, 19));

                            //计算时间和
                            alltime += ti;

                            if (ti < 360) {
                                //拿到时长小于6分钟单号
                                letterSix += map2.get("fktrainOrderOfflineId").toString() + ",";
                            }
                            else if (ti > 360 && ti < 600) {
                                //拿到时长大于6分钟单号
                                biggerSix += map2.get("fktrainOrderOfflineId").toString() + ",";
                            }
                            else if (ti > 600) {
                                //拿到时长大于10分钟单号
                                biggerTen += map2.get("fktrainOrderOfflineId").toString() + ",";
                            }
                        }
                    }

                    //没有一单成功则跳出不记录
                    if (alltime == 0) {
                        continue;
                    }

                    //拿到该代售点当天成功的数量
                    String sqlavg1 = "SELECT COUNT(1) as success FROM TrainOrderOfflineRecord WITH (NOLOCK) WHERE ProviderAgentid="
                            + agentId + " and DistributionTime between '" + beginTime + "' and  '" + endTime
                            + "' and (dealResult=1 ) ";
                    List listavg1 = Server.getInstance().getSystemService().findMapResultBySql(sqlavg1, null);
                    Map mapavg1 = (Map) listavg1.get(0);
                    //拿到该代售点当天总订单数
                    String sqlavg2 = "SELECT COUNT(1) as alls FROM TrainOrderOfflineRecord WITH(NOLOCK) WHERE ProviderAgentid="
                            + agentId + " and DistributionTime between '" + beginTime + "' and  '" + endTime
                            + "' and (dealResult=1 or dealResult=2) ";
                    List listavg2 = Server.getInstance().getSystemService().findMapResultBySql(sqlavg2, null);
                    Map mapavg2 = (Map) listavg2.get(0);

                    //出票成功数量  
                    allordersyes = mapavg1.get("success").toString();
                    //总数
                    allorders = mapavg2.get("alls").toString();

                    float f1 = Float.parseFloat(mapavg1.get("success").toString())
                            / Float.parseFloat(mapavg2.get("alls").toString());
                    BigDecimal bd = new BigDecimal(Double.parseDouble(f1 + ""));
                    //成功率,保留两位小数
                    successavg = bd.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();

                    int letterSixSum = 0;
                    int biggerSixSum = 0;
                    int biggerTenSum = 0;
                    if (letterSix.contains(",")) {
                        //得到六分钟的订单数
                        letterSix = letterSix.substring(0, letterSix.length() - 1);
                        String sql3 = "select count(1) as ordercount from trainorderoffline WITH(NOLOCK) where id in ("
                                + letterSix + ")";
                        List listcount = Server.getInstance().getSystemService().findMapResultBySql(sql3, null);
                        Map mapcount = (Map) listcount.get(0);
                        letterSixSum = Integer.parseInt(mapcount.get("ordercount").toString());
                    }
                    else {
                        letterSixSum = 0;
                    }
                    if (biggerSix.contains(",")) {
                        //得到大于6分钟的订单数
                        biggerSix = biggerSix.substring(0, biggerSix.length() - 1);
                        String sql3 = "select count(1) as ordercount from trainorderoffline WITH(NOLOCK) where id in ("
                                + biggerSix + ")";
                        List listcount = Server.getInstance().getSystemService().findMapResultBySql(sql3, null);
                        Map mapcount = (Map) listcount.get(0);
                        biggerSixSum = Integer.parseInt(mapcount.get("ordercount").toString());
                    }
                    else {
                        biggerSixSum = 0;
                    }
                    if (biggerTen.contains(",")) {
                        //得到大于10分钟的订单数
                        biggerTen = biggerTen.substring(0, biggerTen.length() - 1);
                        String sql3 = "select count(1) as ordercount from trainorderoffline WITH(NOLOCK) where id in ("
                                + biggerTen + ")";
                        List listcount = Server.getInstance().getSystemService().findMapResultBySql(sql3, null);
                        Map mapcount = (Map) listcount.get(0);
                        biggerTenSum = Integer.parseInt(mapcount.get("ordercount").toString());
                    }
                    else {
                        biggerTenSum = 0;
                    }
                    //判断该效率是否存在，不存在新增，存在修改
                    String insuresql = "select 1 from TrainOrderOfflineStatistics WITH(NOLOCK) where agentId=" + agentId
                            + " and staticDate='" + dates + "'";
                    List listinsure = Server.getInstance().getSystemService().findMapResultBySql(insuresql, null);
                    //成功单数
                    int listTimeNum = listtime.size() == 0 ? 1 : listtime.size();

                    if (listinsure.size() == 0) {
                        //新增供应商概率
                        String sqlInsert = "insert into TrainOrderOfflineStatistics(agentId,finishTime,avgAllTime,finishAvg,letterSix,biggerSix,biggerTen,finishSum,allOrderSum,staticDate,ranking) "
                                + " values(" + agentId + "," + alltime / listTimeNum + "," + "0," + (successavg * 100)
                                + "," + letterSixSum + "," + biggerSixSum + "," + biggerTenSum + ","
                                + (letterSixSum + biggerSixSum + biggerTenSum) + "," + allorders + ",'" + dates
                                + "',0)";
                        WriteLog.write("statistics_1_sqlInsert_" + dates, "sqlInsert=" + sqlInsert);
                        Server.getInstance().getSystemService().excuteAdvertisementBySql(sqlInsert);
                    }
                    else {
                        //修改供应商概率
                        String updatesql = "update TrainOrderOfflineStatistics set finishTime=" + alltime / listTimeNum
                                + ",avgAllTime=0,finishAvg=" + (successavg * 100) + ",letterSix=" + letterSixSum
                                + ",biggerSix=" + biggerSixSum + ",biggerTen=" + biggerTenSum + ",finishSum="
                                + (letterSixSum + biggerSixSum + biggerTenSum) + ",allOrderSum=" + allorders
                                + " where staticDate='" + dates + "' and agentid=" + agentId;
                        WriteLog.write("statistics_1_sqlNoInsert_" + dates,
                                "agentId=" + agentId + ";avgtime=" + alltime / listTimeNum + ";allavgs=0;successavg="
                                        + successavg + ";letterSixSum=" + letterSixSum + ";biggerSixSum=" + biggerSixSum
                                        + ";biggerTenSum=" + biggerTenSum + ";all="
                                        + (letterSixSum + biggerSixSum + biggerTenSum) + ";allorders=" + allorders
                                        + ";allordersyes=" + allordersyes);
                        WriteLog.write("statistics_1_sqlNoInsert_" + dates, "updatesql=" + updatesql);
                        Server.getInstance().getSystemService().excuteAdvertisementBySql(updatesql);
                    }

                    WriteLog.write("statistics_1_" + dates,
                            "agentId=" + agentId + ";avgtime=" + alltime / listTimeNum + ";alltime=" + alltime
                                    + ";alltimes=0;allavgs=0;successavg=" + successavg + ";letterSixSum=" + letterSixSum
                                    + ";biggerSixSum=" + biggerSixSum + ";biggerTenSum=" + biggerTenSum + ";all="
                                    + (letterSixSum + biggerSixSum + biggerTenSum) + ";allorders=" + allorders
                                    + ";allordersyes=" + allordersyes);
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            WriteLog.write("JobTrainOfflineFlashdata_expr", r1 + "-供应商效率统计main异常-e-->" + e);
        }
    }

    /**
     * 排名计算
     * @param day
     * @throws NumberFormatException
     */
    private void rankingCalc(String day) throws Exception {
        String dates = day;

        try {
            //得到指定日期,供应商效率内容，用来后期计算
            String sql2 = "select agentId,allOrderSum,avgAllTime,finishTime,finishAvg from TrainOrderOfflineStatistics with(nolock) where staticDate='"
                    + dates + "'";
            List list2 = Server.getInstance().getSystemService().findMapResultBySql(sql2, null);

            //拿到供应商问题订单数量，用于稍后问题订单率计算
            String sql3 = "  select ProviderAgentid as agentid,COUNT(PKId) as questionnum from TrainOrderOfflineRecord with (nolock)  "
                    + "where DistributionTime between '" + dates + " 00:00:00' and '" + dates
                    + " 23:59:59'  and DealResult=223 group by ProviderAgentid";
            List list3 = Server.getInstance().getSystemService().findMapResultBySql(sql3, null);//得出供应商的问题订单
            Map<String, Double> agentQuestionMap = new HashMap<String, Double>();
            for (int i = 0; i < list3.size(); i++) {
                Map questionMap = (Map) list3.get(i);

                agentQuestionMap.put(questionMap.get("agentid").toString(),
                        Double.parseDouble(questionMap.get("questionnum").toString()));
            }

            Map<String, Double> maprank = new HashMap<String, Double>();
            for (int i = 0; i < list2.size(); i++) {
                Map map2 = (Map) list2.get(i);

                String agentId = map2.get("agentId").toString();

                //出票时间
                long finishTime = Long.parseLong(map2.get("finishTime").toString());
                //出票成功率
                double finishAvg = Double.parseDouble(map2.get("finishAvg").toString());
                //出票时长与得分的转换
                double finishScore = JobUtils.getTimeSwitchScore(finishTime);
                //成功率
                double avgSuccess = finishAvg / 100;
                double allOrderSum = Double.parseDouble(map2.get("allOrderSum").toString());
                //问题订单率
                double questionRate = 1
                        - (agentQuestionMap.containsKey(agentId) ? agentQuestionMap.get(agentId) : 0) / allOrderSum;

                //效率等于出票时长*50%+成功率*25%+问题订单率*25%
                double efficiency = (finishScore * 0.50 + avgSuccess * 0.25 + questionRate * 0.25);

                //放入map，做计算
                maprank.put(map2.get("agentId").toString(), efficiency);
            }

            //根据值排序 降序
            Map<String, Double> m = JobUtils.sortByValue(maprank);

            //遍历key
            Set<String> it = m.keySet();
            Iterator<String> i = it.iterator();
            int k = 0;
            while (i.hasNext()) {
                //名次
                k++;
                String key = (String) i.next();

                WriteLog.write("---排名计算" + dates, "k=" + k + "-->" + key + "-->" + maprank.get(key));

                //更新数据库名次
                String updaterank = "update TrainOrderOfflineStatistics set ranking=" + k + " where agentid=" + key
                        + " and STATicdate='" + dates + "'";

                WriteLog.write("updaterank_sql_" + dates, "updaterank=" + updaterank);
                Server.getInstance().getSystemService().excuteAdvertisementBySql(updaterank);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            WriteLog.write("JobTrainOfflineRanking_expr", r1 + "-供应商排名计算main异常-e-->" + e);
        }
    }

    // 设置调度任务及触发器，任务名及触发器名可用来停止任务或修改任务
    public static void startScheduler(String expr) throws Exception {
        WriteLog.write("JobTrainOfflineFlashdataRanking_expr", "expr=" + expr);
        //新增任务组
        JobDetail jobDetail = new JobDetail("JobTrainOfflineFlashdata", "JobTrainOfflineFlashdataGroup",
                JobTrainOfflineNewFlashdata.class);// 任务名，任务组名，任务执行类
        // 触发器名，触发器组名
        CronTrigger trigger = new CronTrigger("JobTrainOfflineFlashdata", "JobTrainOfflineFlashdataGroup", expr);
        //获得调度
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
        scheduler.scheduleJob(jobDetail, trigger);
        //启动job
        scheduler.start();
    }

}
