package com.ccservice.b2b2c.updaterankjob;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;

import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataTable;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.util.ExceptionUtil;

public class JobTrainOfflineReminder implements Job {

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        WriteLog.write("JobTrainOfflineReminder_expr", "begin...");
        String sql = "SELECT id, AgentId FROM TrainOrderOffline WITH(NOLOCK) WHERE OrderStatus=1 AND CreateTime >= convert(varchar(10),getdate(),120)+' 00:00:00' AND DATEADD(n, -3, getdate()) >= CreateTime";
        DataTable dataTable = new DataTable();
        try {
            dataTable = DBHelperOffline.GetDataTable(sql, null);
        }
        catch (Exception e) {
            // 记录异常日志
            ExceptionUtil.writelogByException("JobTrainOfflineReminder_expr", e);
        }
        if (dataTable != null) {
            String nowTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
            WriteLog.write("JobTrainOfflineReminder_expr", nowTime + "----提醒件数----" + dataTable.GetRow().size());
            if (dataTable.GetRow().size() > 0) {
                for (int i = 0; i < dataTable.GetRow().size(); i++) {
                    String orderId = String.valueOf(dataTable.GetRow().get(i).GetColumn("id").GetValue());
                    String agentId = String.valueOf(dataTable.GetRow().get(i).GetColumn("AgentId").GetValue());
                    String sql1 = "SELECT id, AgentId FROM TrainOrderOfflineReminder WITH(NOLOCK) WHERE orderId="
                            + orderId + " AND agentId=" + agentId;
                    DataTable dataTable1 = new DataTable();
                    try {
                        dataTable1 = DBHelperOffline.GetDataTable(sql1, null);
                    }
                    catch (Exception e) {
                        // 记录异常日志
                        ExceptionUtil.writelogByException("JobTrainOfflineReminder_expr", e);
                    }
                    if (dataTable1 == null || dataTable1.GetRow().size() == 0) {
                        String sqlInsert = "INSERT INTO TrainOrderOfflineReminder (orderId, agentId, reminderNum1, reminderNum2, createDate) VALUES("
                                + orderId + ", " + agentId + ", 0, 1, convert(varchar(10),getdate(),120))";
                        int result = DBHelperOffline.insertSql(sqlInsert);
                        WriteLog.write("JobTrainOfflineReminder_expr", nowTime + "----提醒更新结果----" + result);
                    }
                    else {
                        WriteLog.write("JobTrainOfflineReminder_expr", nowTime + "----该订单已提示----" + orderId);
                    }
                }
            }
        }
        WriteLog.write("JobTrainOfflineReminder_expr", "end...");
    }

    // 设置调度任务及触发器，任务名及触发器名可用来停止任务或修改任务
    public static void startScheduler(String expr) throws Exception {
        System.out.println("开始Reminder_StartJobTrainOfflineReminder_expr=" + expr);
        WriteLog.write("JobTrainOfflineReminder_expr", "开始Reminder_StartJobTrainOfflineReminder_expr=" + expr);
        JobDetail jobDetail = new JobDetail("JobTrainOfflineReminder", "JobTrainOfflineReminderGroup",
                JobTrainOfflineReminder.class);// 任务名，任务组名，任务执行类
        CronTrigger trigger = new CronTrigger("JobTrainOfflineReminder", "JobTrainOfflineReminderGroup", expr);// 触发器名，触发器组名
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
        scheduler.scheduleJob(jobDetail, trigger);
        scheduler.start();
    }

    public static void main(String[] args) throws Exception {
        startScheduler("0 0/3 0-23 * * ?");
    }
}
