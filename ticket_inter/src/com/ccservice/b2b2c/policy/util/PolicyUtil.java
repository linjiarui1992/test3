package com.ccservice.b2b2c.policy.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.ccservice.b2b2c.base.flightinfo.CarbinInfo;

public class PolicyUtil {
    /**
     * 将money格式化为类似于2,243,234.00的格式
     * 
     * @param money
     * @return
     */
    static DecimalFormat format = (DecimalFormat) NumberFormat.getInstance();

    public static String formatMoney(Float money) {
        format.applyPattern("#,##0.00");
        try {
            String result = format.format(money);
            return result;
        }
        catch (Exception e) {
            if (money != null) {
                return Float.toString(money);
            }
            else {
                return "0";
            }
        }
    }

    /**
     * 计算每个仓位的折扣
     * 
     * @param listCabinAll
     * @return
     * @time 2015年6月9日 下午7:20:26
     * @author chendong
     * @param yprice 
     */
    public static List<CarbinInfo> reSetDiscount(List<CarbinInfo> listCabinAll, float yprice) {
        for (int i = 0; i < listCabinAll.size(); i++) {
            CarbinInfo carbininfo = listCabinAll.get(i);
            Float discount = 0F;
            discount = (carbininfo.getPrice() / yprice) * 100;
            carbininfo.setDiscount(discount);
        }
        return listCabinAll;
    }

    /**
     * 对所有测仓位进行排序
     * 
     * @param listCabinAll
     * @return
     * @time 2015年6月9日 下午7:20:13
     * @author chendong
     */
    public static List<CarbinInfo> sortListCabinAll(List<CarbinInfo> listCabinAll) {
        Collections.sort(listCabinAll, new Comparator<CarbinInfo>() {
            @Override
            public int compare(CarbinInfo o1, CarbinInfo o2) {
                if (o1.getPrice() == null || o2.getPrice() == null) {
                    return 1;
                }
                else {
                    // TODO Discount排序
                    if (o1.getPrice() > o2.getPrice()) {
                        return 1;
                    }
                    else if (o1.getPrice() < o2.getPrice()) {
                        return -1;

                    }
                }
                return 0;
            }
        });
        return listCabinAll;
    }

}
