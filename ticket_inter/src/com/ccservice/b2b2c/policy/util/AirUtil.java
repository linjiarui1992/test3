/**
 * 
 */
package com.ccservice.b2b2c.policy.util;

import com.ccservice.b2b2c.base.zrate.Zrate;

/**
 * 
 * @time 2015年7月8日 下午5:53:06
 * @author chendong
 */
public class AirUtil {
    /**
     * 获取一条最基本的政策，返点为1.5
     * @return
     */
    public static Zrate getbaseZrate() {
        Zrate zrate = new Zrate();
        zrate.setId(1L);
        zrate.setRatevalue(0F);
        zrate.setGeneral(1L);
        zrate.setOutid("1");
        zrate.setAgentid(5L);
        zrate.setTickettype(1);
        zrate.setWorktime("18:00");
        zrate.setAfterworktime("23:59");
        zrate.setOnetofivewastetime("00:01");
        zrate.setSpeed("10分钟");
        zrate.setRemark("可能换编码出票，出票速度稍慢，请耐心等待。");
        return zrate;
    }

    /**
     * 获取一条最基本的政策，返点为2.5
     * @return
     */
    public static Zrate getbaseChildZrate() {
        Zrate zrate = new Zrate();
        zrate.setId(1L);
        zrate.setRatevalue(0.7F);
        zrate.setGeneral(1L);
        zrate.setOutid("1");
        zrate.setAgentid(5L);
        zrate.setTickettype(1);
        zrate.setWorktime("18:00");
        zrate.setAfterworktime("23:59");
        zrate.setOnetofivewastetime("00:01");
        zrate.setSpeed("10分钟");
        zrate.setRemark("可能换编码出票，出票速度稍慢，请耐心等待。");
        return zrate;
    }

}
