package com.ccservice.b2b2c.policy;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.base.sysconfig.Sysconfig;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.tenpay.util.MD5Util;

/**
 * 
 *  去哪儿出票，拒单，退票接口 
 * @time 2014年8月30日 下午4:16:22
 * @author yinshubin
 */
/**
 * 所有url和配置文件写到train.properties里!!!!!!!!!!!!!!!!!!!!!!!
 * 
 * 
 * @time 2015年4月28日 上午11:32:20
 * @author chendong
 */
public class QunarTrainMethod extends SupplyMethod {
    /**
     * 说明：调用出票结果接口（出票、拒单共用）
     * @param orderid
     * @param state
     * @return
     * @time 2014年8月30日 下午4:16:52
     * @author yinshubin
     */

    public static boolean trainIssueOrRefuse(long orderid, int state) {
        String merchantCode = "";
        String o_my = "";
        String url = "";
        WriteLog.write("qunartrain", "进入出票结果接口:" + orderid);
        boolean result = false;
        String paramContent = "";
        Trainorder trainorder = new Trainorder();
        trainorder = Server.getInstance().getTrainService().findTrainorder(orderid);
        String o_id = trainorder.getQunarOrdernumber();
        //取数据库获取数据开始
        int index = o_id.indexOf("1");
        String str = o_id.substring(0, index);
        String sql = "SELECT top 1  * FROM QunarTrainMerchantInfo WHERE MerchantCode like '%" + str + "%'";
        List sqlResultList = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        for (int i = 0; i < sqlResultList.size(); ++i) {
            Map map = (Map) sqlResultList.get(i);
            merchantCode = map.get("MerchantCode").toString();
            o_my = map.get("MerchantKey").toString();
            url = map.get("QunarUrl").toString();
        }
        WriteLog.write("qunar参数", "MerchantCode:" + merchantCode + ";MerchantKey:" + o_my + ";QunarUrl:" + url);
        //取数据库获取数据结束
        String dowhat = url + "ProcessPurchase.do";
        String o_opt = "";
        String resulturl = "";
        String o_comment = "";
        int count = 0;
        if (trainorder.getOrderstatus() == Trainorder.WAITISSUE && state == 3) {
            o_comment = "true";
            JSONArray jsonarray = new JSONArray();
            JSONArray jsonarray1 = new JSONArray();
            String extnumberALL = trainorder.getExtnumber();
            String[] extnumbers = extnumberALL.split(",");
            for (Trainpassenger p : trainorder.getPassengers()) {
                for (Trainticket tk : p.getTraintickets()) {
                    String str_st = tk.getSeattype();
                    String i_st = "";
                    JSONObject jsonobject = new JSONObject();
                    JSONObject jsonobject1 = new JSONObject();//用来加密用
                    if (null != trainorder.getIsjointtrip() && trainorder.getIsjointtrip() == 1) {//如果是联程票
                        if (null != tk.getSeq() && tk.getSeq() == 1) {
                            jsonobject.put("ticketNo", extnumbers[0]);
                            jsonobject1.put("ticketNo", extnumbers[0]);
                        }
                        else {
                            jsonobject.put("ticketNo", extnumbers[1]);//extnumbers[1]
                            jsonobject1.put("ticketNo", extnumbers[1]);//extnumbers[1]
                        }
                    }
                    else {
                        jsonobject.put("ticketNo", extnumbers[0]);
                        jsonobject1.put("ticketNo", extnumbers[0]);
                    }
                    //                    TODO 兼容
                    String seatNo = coachString(tk.getCoach(), tk.getSeatno());/*tk.getCoach() + "车" + tk.getSeatno() + "号";*/
                    String seatNo1 = coachString(tk.getCoach(), tk.getSeatno());/*tk.getCoach() + "车" + tk.getSeatno() + "号";*/
                    String passengerName = p.getName();
                    String passengerName1 = p.getName();
                    //0：站票；1：硬座；2：软座；3：一等软座；4：二等软座；5-7：硬卧上、中、下；8-9：软卧上、下；10-11：高级软卧上、下
                    if (seatNo.contains("上") && tk.getSeatno().indexOf("上") > 0) {
                        str_st += "上";
                    }
                    else if (seatNo.contains("中") && tk.getSeatno().indexOf("中") > 0) {
                        str_st += "中";
                    }
                    else if (seatNo.contains("下") && tk.getSeatno().indexOf("下") > 0) {
                        str_st += "下";
                    }
                    i_st = getseattype(str_st);
                    try {
                        WriteLog.write("qunartrain", "seatNo:" + seatNo);
                        WriteLog.write("qunartrain", "passengerName:" + passengerName);
                        seatNo = URLEncoder.encode(seatNo, "UTF-8");
                        passengerName = URLEncoder.encode(passengerName, "UTF-8");
                        WriteLog.write("qunartrain", "seatNo:" + seatNo);
                        WriteLog.write("qunartrain", "passengerName:" + passengerName);
                    }
                    catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    jsonobject.put("seatType", i_st);
                    jsonobject1.put("seatType", i_st);
                    jsonobject.put("seatNo", seatNo);
                    jsonobject1.put("seatNo", seatNo1);
                    float price = tk.getPrice();
                    //                    if (tk.getTickettype() == 1) {
                    //                        price = priceQunar(tk.getTcseatno(), tk.getPrice(), tk.getPayprice(), i_st,
                    //                                isCanIssue(trainorder));
                    //                    }
                    jsonobject.put("price", price);
                    jsonobject1.put("price", price);
                    jsonobject.put("passengerName", passengerName);
                    jsonobject1.put("passengerName", passengerName1);
                    jsonobject.put("seq", tk.getSeq().toString());
                    jsonobject1.put("seq", tk.getSeq().toString());
                    //TODO
                    jsonobject.put("ticketType", changeTicketTypeDB2Qunar(tk.getTickettype()));
                    jsonobject1.put("ticketType", changeTicketTypeDB2Qunar(tk.getTickettype()));
                    count++;
                    jsonarray.add(jsonobject);
                    jsonarray1.add(jsonobject1);
                }
            }
            JSONObject jsonobject = new JSONObject();
            JSONObject jsonobject1 = new JSONObject();
            o_opt = "CONFIRM";
            jsonobject.put("count", count);
            jsonobject1.put("count", count);
            jsonobject.put("tickets", jsonarray);
            jsonobject1.put("tickets", jsonarray1);

            String str_toMD5 = o_my + merchantCode + o_id + o_opt + jsonobject1.toString() + o_comment;
            WriteLog.write("qunartrain", "str_toMD5:" + str_toMD5);
            String str_MD5 = MD5Util.MD5Encode(str_toMD5, "UTF-8").toUpperCase();
            WriteLog.write("qunartrain", "str_MD5:" + str_MD5);
            paramContent = "merchantCode=" + merchantCode + "&orderNo=" + o_id + "&opt=" + o_opt + "&result="
                    + jsonobject.toString() + "&un=&comment=" + o_comment + "&HMAC=" + str_MD5;
        }
        else {//拒单调用去哪儿网出票结果接口
            o_opt = "NO_TICKET";
            o_comment = "false";
            JSONArray ja = new JSONArray();
            JSONArray ja1 = new JSONArray();
            JSONObject jo = new JSONObject();
            JSONObject jo1 = new JSONObject();
            if (trainorder.getRefundreason() == 6) {//如果错误信息是乘客身份错误
                for (Trainpassenger p : trainorder.getPassengers()) {
                    for (Trainticket tk : p.getTraintickets()) {
                        String getname = p.getName();
                        String getname1 = p.getName();

                        try {
                            getname = URLEncoder.encode(getname, "UTF-8");
                        }
                        catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        jo.put("certNo", p.getIdnumber());
                        jo1.put("certNo", p.getIdnumber());
                        jo.put("certType", p.getIdtype());
                        jo1.put("certType", p.getIdtype());
                        jo.put("name", getname);
                        jo1.put("name", getname1);
                        jo.put("ticketType", changeTicketTypeDB2Qunar(tk.getTickettype()));
                        jo1.put("ticketType", changeTicketTypeDB2Qunar(tk.getTickettype()));
                        jo.put("reason", p.getAduitstatus());
                        jo1.put("reason", p.getAduitstatus());
                        ja.add(jo);
                        ja1.add(jo1);
                    }
                }
                String str_toMD5 = o_my + merchantCode + o_id + o_opt + "6" + ja1.toString() + o_comment;
                String str_MD5 = MD5Util.MD5Encode(str_toMD5, "UTF-8").toUpperCase();
                paramContent = "merchantCode=" + merchantCode + "&orderNo=" + o_id + "&opt=" + o_opt + "&reason=" + "6"
                        + "&passengerReason=" + ja.toString() + "&comment=" + o_comment + "&HMAC=" + str_MD5;
            }
            else {
                String o_reason = String.valueOf(trainorder.getRefundreason());
                String str_toMD5 = o_my + merchantCode + o_id + o_opt + o_reason + o_comment;
                String str_MD5 = MD5Util.MD5Encode(str_toMD5, "UTF-8").toUpperCase();
                paramContent = "merchantCode=" + merchantCode + "&orderNo=" + o_id + "&opt=" + o_opt + "&un=&reason="
                        + trainorder.getRefundreason() + "&comment=" + o_comment + "&HMAC=" + str_MD5;
            }
        }
        WriteLog.write("qunartrain", "请求地址:" + dowhat + paramContent);
        try {
            resulturl = com.ccservice.b2b2c.atom.component.SendPostandGet.submitPost(dowhat, paramContent, "UTF-8")
                    .toString();
            WriteLog.write("qunartrain", "出票或拒单返回结果:" + resulturl);
            JSONObject json = JSONObject.parseObject(resulturl);
            String ret = json.getString("ret");
            if (ret.equals("true")) {
                result = true;
            }
            else {
                String errMsg = json.getString("errMsg");
                String errCode = json.getString("errCode");
                WriteLog.write("qunartrain", "操作失败！失败原因：" + errMsg + ";错误码及其备注" + errCode + ":"
                        + getErrorcodes(errCode));
                result = false;
            }
        }
        catch (Exception e) {
            WriteLog.write("qunartrain", "出票或拒单请求失败：" + e);
        }
        return result;
    }

    /**
     * 说明：调用出票结果接口（拒单）下单消费者私有
     * @param trianorder
     * @param msg
     * @return
     * @author yinshubin
     */
    public static boolean trainRefuseByCreateOrder(Trainorder trainorder, String msg) {
        String merchantCode = "";
        String o_my = "";
        String url = "";
        WriteLog.write("Qunar_trainRefuseByCreateOrder", "进入出票结果接口:" + trainorder.getId());
        boolean result = false;
        String paramContent = "";
        String o_id = trainorder.getQunarOrdernumber();
        //取数据库获取数据开始
        int index = o_id.indexOf("1");
        String str = o_id.substring(0, index);
        String sql = "SELECT top 1  * FROM QunarTrainMerchantInfo WHERE MerchantCode like '%" + str + "%'";
        List sqlResultList = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        for (int i = 0; i < sqlResultList.size(); ++i) {
            Map map = (Map) sqlResultList.get(i);
            merchantCode = map.get("MerchantCode").toString();
            o_my = map.get("MerchantKey").toString();
            url = map.get("QunarUrl").toString();
        }
        WriteLog.write("qunar参数", "MerchantCode:" + merchantCode + ";MerchantKey:" + o_my + ";QunarUrl:" + url);
        //取数据库获取数据结束
        String dowhat = url + "ProcessPurchase.do";
        String o_opt = "";
        String resulturl = "";
        String o_comment = "";
        int count = 0;
        o_opt = "NO_TICKET";
        o_comment = "false";
        JSONArray ja = new JSONArray();
        JSONArray ja1 = new JSONArray();
        JSONObject jo = new JSONObject();
        JSONObject jo1 = new JSONObject();
        int o_reason = refuseMsg12306ToQunar(msg);
        if (o_reason == 6) {//如果错误信息是乘客身份错误
            for (Trainpassenger p : trainorder.getPassengers()) {
                for (Trainticket tk : p.getTraintickets()) {
                    if (msg.contains(p.getName())) {
                        String getname = p.getName();
                        String getname1 = p.getName();
                        try {
                            getname = URLEncoder.encode(getname, "UTF-8");
                        }
                        catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        jo.put("certNo", p.getIdnumber());
                        jo1.put("certNo", p.getIdnumber());
                        jo.put("certType", p.getIdtype());
                        jo1.put("certType", p.getIdtype());
                        jo.put("name", getname);
                        jo1.put("name", getname1);
                        jo.put("ticketType", changeTicketTypeDB2Qunar(tk.getTickettype()));
                        jo1.put("ticketType", changeTicketTypeDB2Qunar(tk.getTickettype()));
                        jo.put("reason", 2);
                        jo1.put("reason", 2);
                        ja.add(jo);
                        ja1.add(jo1);
                    }
                }
            }
            String str_toMD5 = o_my + merchantCode + o_id + o_opt + o_reason + ja1.toString() + o_comment;
            String str_MD5 = MD5Util.MD5Encode(str_toMD5, "UTF-8").toUpperCase();
            paramContent = "merchantCode=" + merchantCode + "&orderNo=" + o_id + "&opt=" + o_opt + "&reason="
                    + o_reason + "&passengerReason=" + ja.toString() + "&comment=" + o_comment + "&HMAC=" + str_MD5;
        }
        if (o_reason == 2) {
            for (Trainpassenger p : trainorder.getPassengers()) {
                for (Trainticket tk : p.getTraintickets()) {
                    if (msg.contains(p.getName())) {
                        String getname = p.getName();
                        String getname1 = p.getName();
                        try {
                            getname = URLEncoder.encode(getname, "UTF-8");
                        }
                        catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        jo.put("certNo", p.getIdnumber());
                        jo1.put("certNo", p.getIdnumber());
                        jo.put("certType", p.getIdtype());
                        jo1.put("certType", p.getIdtype());
                        jo.put("name", getname);
                        jo1.put("name", getname1);
                        jo.put("ticketType", changeTicketTypeDB2Qunar(tk.getTickettype()));
                        jo1.put("ticketType", changeTicketTypeDB2Qunar(tk.getTickettype()));
                        jo.put("reason", 3);
                        jo1.put("reason", 3);
                        jo = getParameterByMsg(jo, msg);
                        jo1 = getParameterByMsg(jo1, msg);
                        ja.add(jo);
                        ja1.add(jo1);
                    }
                }
            }
            String str_toMD5 = o_my + merchantCode + o_id + o_opt + o_reason + ja1.toString() + o_comment;
            String str_MD5 = MD5Util.MD5Encode(str_toMD5, "UTF-8").toUpperCase();
            paramContent = "merchantCode=" + merchantCode + "&orderNo=" + o_id + "&opt=" + o_opt + "&reason="
                    + o_reason + "&passengerReason=" + ja.toString() + "&comment=" + o_comment + "&HMAC=" + str_MD5;
        }
        else {
            String str_toMD5 = o_my + merchantCode + o_id + o_opt + o_reason + o_comment;
            String str_MD5 = MD5Util.MD5Encode(str_toMD5, "UTF-8").toUpperCase();
            paramContent = "merchantCode=" + merchantCode + "&orderNo=" + o_id + "&opt=" + o_opt + "&un=&reason="
                    + o_reason + "&comment=" + o_comment + "&HMAC=" + str_MD5;
        }
        WriteLog.write("qunartrain", "请求地址:" + dowhat + paramContent);
        try {
            resulturl = com.ccservice.b2b2c.atom.component.SendPostandGet.submitPost(dowhat, paramContent, "UTF-8")
                    .toString();
            WriteLog.write("qunartrain", "出票或拒单返回结果:" + resulturl);
            JSONObject json = JSONObject.parseObject(resulturl);
            String ret = json.getString("ret");
            if (ret.equals("true")) {
                result = true;
            }
            else {
                String errMsg = json.getString("errMsg");
                String errCode = json.getString("errCode");
                WriteLog.write("qunartrain", "操作失败！失败原因：" + errMsg + ";错误码及其备注" + errCode + ":"
                        + getErrorcodes(errCode));
                result = false;
            }
        }
        catch (Exception e) {
            WriteLog.write("qunartrain", "出票或拒单请求失败：" + e);
        }
        return result;
    }

    /**
     * 说明：调用退票结果接口（正常退票、无法退票共用）
     * @param qunarordernum
     * @param state
     * @param reason
     * @return
     * @time 2014年8月30日 下午4:17:06
     * @author yinshubin
     */
    public static boolean trainRefundresult(String qunarordernum, int state, int reason) {
        String url = "";
        String o_my = "";
        String merchantCode = "";
        //取数据库获取数据开始
        int index = qunarordernum.indexOf("1");
        String str = qunarordernum.substring(0, index);
        String sql = "SELECT top 1  * FROM QunarTrainMerchantInfo WHERE MerchantCode like '%" + str + "%'";
        List sqlResultList = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        for (int i = 0; i < sqlResultList.size(); ++i) {
            Map map = (Map) sqlResultList.get(i);
            merchantCode = map.get("MerchantCode").toString();
            o_my = map.get("MerchantKey").toString();
            url = map.get("QunarUrl").toString();
        }
        WriteLog.write("qunar参数", "MerchantCode:" + merchantCode + ";MerchantKey:" + o_my + ";QunarUrl:" + url);
        //取数据库获取数据结束
        //reason  必填       退票失败原因
        //1、已取票       已取出实体票，请持车票及乘车人身份证件原件，至火车站退票窗口办理退票业务。如您已在窗口退票，将退票凭证（白色银联小 票）、
        //                   订单号、预留手机号及乘客姓名上传至cs@youayou.com，我们会在收到邮件后24小时内为您办理退款。
        //2、已过时间    您的车次已开，无法办理退票
        //3、来电取消    用户已经通过电话取消退票
        boolean result = false;
        String dowhat = url + "ProcessRefund.do";
        String pr_opt = "";
        String pr_comment = "";
        String pr_reason = "";
        String pr_HMAC = "";
        String paramContent = "";
        if (state == 7) {//拒绝退票
            pr_opt = "REFUSE";
            pr_comment = "REFUSE";
            pr_reason = String.valueOf(reason);
        }
        else if (state == 9) {//同意退票
            pr_opt = "AGREE";
            pr_comment = "NULL";
            pr_reason = "NULL";
        }
        String parmar = o_my + merchantCode + qunarordernum + pr_opt + pr_comment + pr_reason;
        pr_HMAC = MD5Util.MD5Encode(parmar, "UTF-8").toUpperCase();
        paramContent = "merchantCode=" + merchantCode + "&orderNo=" + qunarordernum + "&opt=" + pr_opt + "&comment="
                + pr_comment + "&reason=" + pr_reason + "&HMAC=" + pr_HMAC;
        WriteLog.write("qunartrain", "退票请求地址:" + dowhat);
        WriteLog.write("qunartrain", "退票请求地址:" + paramContent);

        String resulturl = com.ccservice.b2b2c.atom.component.SendPostandGet.submitPost(dowhat, paramContent, "UTF-8")
                .toString();
        WriteLog.write("qunartrain", "退票返回结果:" + resulturl);
        JSONObject json = JSONObject.parseObject(resulturl);
        String ret = json.getString("ret");
        if (ret.equals("true")) {
            result = true;
        }
        else {
            String errMsg = json.getString("errMsg");
            String errCode = json.getString("errCode");
            WriteLog.write("qunartrain", "操作失败！失败原因：" + errMsg + ";错误码及其备注" + errCode + ":" + getErrorcodes(errCode));
            result = false;
        }
        return result;
    }

    /**
     * 说明：调用原路退款接口
     * @param qunarordernum
     * @param reason
     * @param refundCash
     * @return
     * @time 2014年8月30日 下午4:17:16
     * @author yinshubin
     */
    public static boolean trainRefundPrice(String qunarordernum, int reason, float refundCash) {
        String url = "";
        String o_my = "";
        String merchantCode = "";
        //取数据库获取数据开始
        int index = qunarordernum.indexOf("1");
        String str = qunarordernum.substring(0, index);
        String sql = "SELECT top 1  * FROM QunarTrainMerchantInfo WHERE MerchantCode like '%" + str + "%'";
        List sqlResultList = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        for (int i = 0; i < sqlResultList.size(); ++i) {
            Map map = (Map) sqlResultList.get(i);
            merchantCode = map.get("MerchantCode").toString();
            o_my = map.get("MerchantKey").toString();
            url = map.get("QunarUrl").toString();
        }
        WriteLog.write("qunar参数", "MerchantCode:" + merchantCode + ";MerchantKey:" + o_my + ";QunarUrl:" + url);
        //取数据库获取数据结束
        boolean result = false;
        String dowhat = url + "ProcessApplyAutoRefund.do";
        String trp_reason = String.valueOf(reason);
        String trp_refundCash = String.valueOf(refundCash);
        String trp_comment = "nocomment";
        String resulturl = "";
        String to_MD5 = o_my + merchantCode + qunarordernum + trp_refundCash + trp_reason + trp_comment;
        String trp_HMAC = MD5Util.MD5Encode(to_MD5, "UTF-8").toUpperCase();
        String paramContent = "merchantCode=" + merchantCode + "&orderNo=" + qunarordernum + "&refundCash="
                + trp_refundCash + "&reason=" + trp_reason + "&comment=" + trp_comment + "&HMAC=" + trp_HMAC;
        WriteLog.write("qunartrain", "原路退款请求地址:" + dowhat);
        WriteLog.write("qunartrain", "原路退款请求地址:" + paramContent);
        resulturl = com.ccservice.b2b2c.atom.component.SendPostandGet.submitPost(dowhat, paramContent, "UTF-8")
                .toString();
        WriteLog.write("qunartrain", resulturl);
        JSONObject json = JSONObject.parseObject(resulturl);
        String ret = json.getString("ret");
        if (ret.equals("true")) {
            result = true;
        }
        else {
            String errMsg = json.getString("errMsg");
            String errCode = json.getString("errCode");
            WriteLog.write("qunartrain", "操作失败！失败原因：" + errMsg + ";错误码及其备注" + errCode + ":" + getErrorcodes(errCode));
            result = false;
            //return "操作失败！失败原因：" + errMsg + ";错误码及其备注" + errCode + ":" + getErrorcodes(errCode);
        }
        return result;
    }

    /**
     * 作者：邹远超
     * 日期：2014年6月12日
     * 说明：得到席别
     * @param str_st
     * @return
     */
    public static String getseattype(String str_st) {
        String i_st = "";
        if (str_st.equals("站票") || str_st.equals("无座")) {//
            i_st = "0";
        }
        else if (str_st.equals("硬座")) {
            i_st = "1";
        }
        else if (str_st.equals("软座")) {
            i_st = "2";
        }
        else if (str_st.equals("一等软座") || str_st.equals("一等座")) {
            i_st = "3";
        }
        else if (str_st.equals("二等软座") || str_st.equals("二等座")) {
            i_st = "4";
        }
        else if (str_st.equals("硬卧上")) {
            i_st = "5";
        }
        else if (str_st.equals("硬卧中")) {
            i_st = "6";
        }
        else if (str_st.equals("硬卧下")) {
            i_st = "7";
        }
        else if (str_st.equals("软卧上") || str_st.equals("动卧上")) {
            i_st = "8";
        }
        else if (str_st.equals("软卧下") || str_st.equals("动卧下")) {
            i_st = "9";
        }
        else if (str_st.equals("高级软卧上") || str_st.equals("软卧上")) {
            i_st = "10";
        }
        else if (str_st.equals("高级软卧下") || str_st.equals("软卧下")) {
            i_st = "11";
        }
        else if (str_st.equals("特等座")) {
            i_st = "12";
        }
        else if (str_st.equals("商务座")) {
            i_st = "13";
        }
        return i_st;
    }

    /**
     * 作者：邹远超
     * 日期：2014年6月15日
     * 说明：获取去哪儿网ID
     * @param name
     * @return
     */
    public static String getSystemConfig(String name) {
        List<Sysconfig> configs = Server.getInstance().getSystemService()
                .findAllSysconfig("where c_name='" + name + "'", "", -1, 0);
        if (configs != null && configs.size() == 1) {
            Sysconfig config = configs.get(0);
            return config.getValue();
        }
        return "46";
    }

    /**
     * 说明：错误码转换错误原因
     * @param str
     * @return
     * @time 2014年8月30日 下午4:20:23
     * @author yinshubin
     */
    public static String getErrorcodes(String str) {
        int i = Integer.valueOf(str);
        String s = "";
        switch (i) {
        case 0:
            s = "或者无，表示成功，一般不填写";
            break;
        case 1:
            s = "系统错误，未知服务异常";
            break;
        case 2:
            s = "安全验证错误，不符合安全校验规则";
            break;
        case 3:
            s = "输入参数错误";
            break;
        case 4:
            s = "非授权代理商（代理商merchantCode错误）";
            break;
        case 5:
            s = "备注不能超过100个字符";
            break;
        case 6:
            s = "订单未找到（订单编号错误）";
            break;
        case 7:
            s = "订单信息异常";
            break;
        case 8:
            s = "订单状态已经变化，例：未出票钱用户申请退票";
            break;
        case 9:
            s = "访问限制，访问频率过高，被禁止";
            break;
        case 100:
            s = "获取订单失败其他异常(除1x能够表示的状态之外的状态)";
            break;
        case 200:
            s = "出票失败其他异常(除2x能够表示的状态之外的状态)";
            break;
        case 201:
            s = "实际出票坐席数量与订单坐席数量不符合";
            break;
        case 202:
            s = "订单乘车人数量与出票数量不一致";
            break;
        case 203:
            s = "出票价格与Qunar基础价格不一致，需要人工确认";
            break;
        case 204:
            s = "座席类型与字典不匹配";
            break;
        case 205:
            s = "票号数组与个数不一致";
            break;
        case 206:
            s = "输入座席类型不是客户选定的类型";
            break;
        case 207:
            s = "坐席号错误";
            break;
        case 208:
            s = "车票号不能相同";
            break;
        case 209:
            s = "联程订单不能分开出票，必须同时出票";
            break;
        case 210:
            s = "乘车人错误";
            break;
        case 211:
            s = "儿童票";
            break;
        case 300:
            s = "退票失败其他异常(除3x能够表示的状态之外的状态)";
            break;
        default:
            s = "未知错误";
        }
        return s;
    }

    /**
     * 说明：为满足客服需求，添加出票订单坐席号兼容
     * @param a
     * @param b
     * @return
     * @time 2014年8月30日 下午4:20:37
     * @author yinshubin
     */
    public static String coachString(String a, String b) {
        b = b.trim();
        b = b.replace("无座", "00");
        if (!a.contains("车")) {
            StringBuffer sb = new StringBuffer(a);
            a = sb.append("车").toString();
        }

        if (b.contains("铺")) {
            StringBuffer sb = new StringBuffer(b);
            int i = b.indexOf("铺");
            if (!b.contains("号")) {
                b = sb.insert(i - 1, "号").toString();
            }
        }

        if (!b.contains("号")) {
            StringBuffer sb = new StringBuffer(b);
            b = sb.append("号").toString();
        }
        return a + b;
    }

    /**
     * 说明:通过去哪儿备选价格的报价和去哪儿支付票价格，对比12306价格，得到可出票价格（去哪儿价格高于12306价格） 
     * @param tcseatno
     * @param price
     * @param payprice
     * @param seattype
     * @param iscanissue
     * @return
     * @time 2014年9月23日 下午2:38:25
     * @author yinshubin
     */
    public static float priceQunar(String tcseatno, float price, float payprice, String seattype, boolean iscanissue) {
        if (!iscanissue) {
            return price;
        }
        if (tcseatno == null || tcseatno.equals("")) {
            return payprice;
        }
        JSONArray seatarray = JSONArray.parseArray(tcseatno);
        float seatValue_Value = 0;
        for (int i = 0; i < seatarray.size(); i++) {
            JSONObject seatObject = seatarray.getJSONObject(i);
            if (seatObject.containsKey(seattype)) {
                seatValue_Value = (float) seatObject.getDoubleValue(seattype);//得到票价
                break;
            }
            else if (seattype.equals("11") || seattype.equals("9") || seattype.equals("7")) {
                seatValue_Value = payprice;
            }
        }
        return seatValue_Value;
    }

    /**
     * 说明：根据订单中所有票差价汇总，确定是否可以出票 
     * @param trainorder
     * @return
     * @time 2014年9月25日 上午10:43:59
     * @author yinshubin
     */
    public static boolean isCanIssue(Trainorder trainorder) {
        float changeprice = 0;//去哪儿价-12306价
        for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
            for (Trainticket trainticket : trainpassenger.getTraintickets()) {
                String str_st = trainticket.getSeattype();
                String seatNo = coachString(trainticket.getCoach(), trainticket.getSeatno());/*tk.getCoach() + "车" + tk.getSeatno() + "号";*/
                String seatNo1 = coachString(trainticket.getCoach(), trainticket.getSeatno());/*tk.getCoach() + "车" + tk.getSeatno() + "号";*/
                //0：站票；1：硬座；2：软座；3：一等软座；4：二等软座；5-7：硬卧上、中、下；8-9：软卧上、下；10-11：高级软卧上、下
                if (seatNo.contains("上") && trainticket.getSeatno().indexOf("上") > 0) {
                    str_st += "上";
                }
                else if (seatNo.contains("中") && trainticket.getSeatno().indexOf("中") > 0) {
                    str_st += "中";
                }
                else if (seatNo.contains("下") && trainticket.getSeatno().indexOf("下") > 0) {
                    str_st += "下";
                }
                str_st = getseattype(str_st);
                if (trainticket.getTickettype() == 1) {
                    changeprice += priceQunarChange(trainticket.getTcseatno(), trainticket.getPrice(),
                            trainticket.getPayprice(), str_st);
                }
            }
        }
        if (changeprice >= 0) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * 说明：得到每张票同席别的差价（去哪儿价-12306价） 
     * @param tcseatno
     * @param price
     * @param payprice
     * @param seattype
     * @return
     * @time 2014年9月25日 上午10:43:13
     * @author yinshubin
     */
    public static float priceQunarChange(String tcseatno, float price, float payprice, String seattype) {
        if (tcseatno == null || tcseatno.equals("")) {
            return payprice - price;
        }
        JSONArray seatarray = JSONArray.parseArray(tcseatno);
        float seatValue_Value = 0;
        for (int i = 0; i < seatarray.size(); i++) {
            JSONObject seatObject = seatarray.getJSONObject(i);
            if (seatObject.containsKey(seattype)) {
                seatValue_Value = (float) seatObject.getDoubleValue(seattype);//得到票价
                break;
            }
            else if (seattype.equals("11") || seattype.equals("9") || seattype.equals("7")) {
                return payprice - price;
            }
        }
        return seatValue_Value - price;
    }

    /**
     * 将数据库票类型转换成QUNAR票类型
     * @param tickettypedb
     * @return
     */
    private static String changeTicketTypeDB2Qunar(int tickettypedb) {
        //1:成人票，2:儿童票，3:学生票，
        if (2 == tickettypedb) {
            return "0";
        }
        if (3 == tickettypedb) {
            return "2";
        }
        return "1";
    }

    /*0   其他  
    1   所购买的车次坐席已无票 
    2   身份证件已经实名制购票，不能再次购买同日期同车次的车票 
    3   qunar票价和12306不符 
    4   车次数据与12306不一致   
    5   乘客信息错误  
    6   12306乘客身份信息核验失败 12306乘客身份信息核验失败，passengerReason必填*/
    /**
     * @param msg
     * @return
     */
    public static int refuseMsg12306ToQunar(String msg) {
        int state = 1;
        if (msg.indexOf("票价不符") > -1) {
            return 3;
        }
        else if (msg.indexOf("没有余票") > -1 || msg.indexOf("此车次无票") > -1 || msg.indexOf("已无余票") > -1
                || msg.indexOf("没有足够的票") > -1 || msg.indexOf("余票不足") > -1 || msg.indexOf("非法的席别") > -1) {
            return 1;
        }
        else if (msg.indexOf("已订") > -1) {
            //提交订单失败：尊敬的旅客，您的证件-崔世雄(142327199611126631)已订2015年08月04日K1805次车票，与本次购票行程冲突，请将已购车票办理改签，或办理退票后重新购票；如您确认此身份信息被他人冒用，请点击“网上举报”并确认后，可以继续购票。谢谢您的合作。
            //提交订单失败：潘月炎(二代身份证-370782199411137632)已订2015年08月04日G199次的车票!!
            return 2;
        }
        else if (msg.indexOf("当前提交订单用户过多 ") > -1 || msg.indexOf("提交订单失败：包含排队中的订单") > -1) {
            return 1;
        }
        else if (msg.indexOf("身份") > -1) {
            //身份验证失败:添加乘客 未通过身份效验 刘红初512222197510090046
            return 6;
        }
        else if (msg.indexOf("距离开车时间太近") > -1) {
            return 1;
        }
        else if (msg.indexOf("限制高消费") > -1) {
            return 1;
        }
        else if (msg.indexOf("数据库同步") > -1) {
            return 4;
        }
        else if (msg.indexOf("validatorMessage") > -1) {
            return 1;
        }
        return 1;
    }

    /**
     * 通过msg获取发车日期及车次
     * @param jsonObject
     * @param msg
     * @return
     */
    public static JSONObject getParameterByMsg(JSONObject jsonObject, String msg) {
        //提交订单失败：尊敬的旅客，您的证件-崔世雄(142327199611126631)已订2015年08月04日K1805次车票，与本次购票行程冲突，请将已购车票办理改签，或办理退票后重新购票；如您确认此身份信息被他人冒用，请点击“网上举报”并确认后，可以继续购票。谢谢您的合作。
        //提交订单失败：潘月炎(二代身份证-370782199411137632)已订2015年08月04日G199次的车票!!
        if (msg.contains("已订") && msg.contains("与本次购票行程冲突") && msg.contains("年") && msg.contains("月")
                && msg.contains("日")) {
            String nsss = msg.split("已订")[1];
            String yeerString = nsss.split("年")[0];
            String monthString = nsss.split("年")[1].split("月")[0];
            String dayString = nsss.split("年")[1].split("月")[1].split("日")[0];
            String trainCodeString = nsss.split("年")[1].split("月")[1].split("日")[1].split("次车票")[0];
            jsonObject.put("preDate", yeerString + "-" + monthString + "-" + dayString);
            jsonObject.put("preTrainNo", trainCodeString);
        }
        else if (msg.contains("已订") && msg.contains("的车票") && msg.contains("年") && msg.contains("月")
                && msg.contains("日")) {
            String nsss = msg.split("已订")[1];
            String yeerString = nsss.split("年")[0];
            String monthString = nsss.split("年")[1].split("月")[0];
            String dayString = nsss.split("年")[1].split("月")[1].split("日")[0];
            String trainCodeString = nsss.split("年")[1].split("月")[1].split("日")[1].split("次的车票")[0];
            jsonObject.put("preDate", yeerString + "-" + monthString + "-" + dayString);
            jsonObject.put("preTrainNo", trainCodeString);
        }
        return jsonObject;
    }

    public static void main(String[] args) {
        String msg = "提交订单失败：尊敬的旅客，您的证件-崔世雄(142327199611126631)已订2015年08月04日K1805次车票，与本次购票行程冲突，请将已购车票办理改签，或办理退票后重新购票；如您确认此身份信息被他人冒用，请点击“网上举报”并确认后，可以继续购票。谢谢您的合作。";
        String msg1 = "提交订单失败：潘月炎(二代身份证-370782199411137632)已订2015年08月04日G199次的车票!!";
        System.out.println(getParameterByMsg(new JSONObject(), msg).toString());
        System.out.println(getParameterByMsg(new JSONObject(), msg1).toString());
    }
}
