package com.ccservice.b2b2c.policy.ben;

import java.util.ArrayList;
import java.util.List;
import com.ccservice.b2b2c.base.eaccount.Eaccount;
import com.ccservice.inter.server.Server;

public class FiveonBook {
	// 以下是51book接口需要参数
	private String agentcode;// 51用户名
	private String safecode;// 51安全吗
	private String notifiedUrl;
	private String paymentReturnUrl;
	private String b2cCreatorCn;// webclient createuser;
	private String ispay = "0";// 代扣 1自动 0不自动
	private String staus = "0";// 接口是否启用 1,启用 0,禁用
	private String tuifeiurl;// 退费订单通知URL
	private String password;

	public FiveonBook() {
		List<Eaccount> listEaccount51book = new ArrayList<Eaccount>();
		Eaccount e = Server.getInstance().getSystemService().findEaccount(2);
		if (e!=null&&e.getName() != null) {
			listEaccount51book.add(e);
		}
		if (listEaccount51book.size() > 0) {
			agentcode = listEaccount51book.get(0).getUsername();
			safecode = listEaccount51book.get(0).getPwd();
			notifiedUrl = listEaccount51book.get(0).getNourl();
			paymentReturnUrl = listEaccount51book.get(0).getPayurl();
			ispay = listEaccount51book.get(0).getIspay();
			tuifeiurl = listEaccount51book.get(0).getUrl();
			password = listEaccount51book.get(0).getPassword();
			staus = listEaccount51book.get(0).getState();
		} else {
			staus="0";
			System.out.println("NO-51book");
		}
	}

	public String getAgentcode() {
		return agentcode;
	}

	public void setAgentcode(String agentcode) {
		this.agentcode = agentcode;
	}

	public String getSafecode() {
		return safecode;
	}

	public void setSafecode(String safecode) {
		this.safecode = safecode;
	}

	public String getNotifiedUrl() {
		return notifiedUrl;
	}

	public void setNotifiedUrl(String notifiedUrl) {
		this.notifiedUrl = notifiedUrl;
	}

	public String getPaymentReturnUrl() {
		return paymentReturnUrl;
	}

	public void setPaymentReturnUrl(String paymentReturnUrl) {
		this.paymentReturnUrl = paymentReturnUrl;
	}

	public String getB2cCreatorCn() {
		return b2cCreatorCn;
	}

	public void setB2cCreatorCn(String creatorCn) {
		b2cCreatorCn = creatorCn;
	}

	public String getIspay() {
		return ispay;
	}

	public void setIspay(String ispay) {
		this.ispay = ispay;
	}

	public String getStaus() {
		return staus;
	}

	public void setStaus(String staus) {
		this.staus = staus;
	}

	public String getTuifeiurl() {
		return tuifeiurl;
	}

	public void setTuifeiurl(String tuifeiurl) {
		this.tuifeiurl = tuifeiurl;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
