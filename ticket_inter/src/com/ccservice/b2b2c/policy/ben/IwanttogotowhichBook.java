package com.ccservice.b2b2c.policy.ben;

import java.util.ArrayList;
import java.util.List;

import com.ccservice.b2b2c.base.eaccount.Eaccount;
import com.ccservice.inter.server.Server;

public class IwanttogotowhichBook {
    // 以下是517NA接口需要参数
    private String username;

    private String safecode;

    private String staus = "0";

    private String password;

    private String pid;

    public IwanttogotowhichBook() {
        List<Eaccount> listEaccount = new ArrayList<Eaccount>();
        Eaccount e = Server.getInstance().getSystemService().findEaccount(7);
        if (e != null && e.getName() != null) {
            listEaccount.add(e);
        }
        if (listEaccount.size() > 0) {
            username = listEaccount.get(0).getUsername();
            safecode = listEaccount.get(0).getPwd();//安全码
            password = listEaccount.get(0).getPassword();//密码
            staus = listEaccount.get(0).getState();
            pid = listEaccount.get(0).getKeystr();//PID
        }
        else {
            staus = "0";
            System.out.println("NO-517");
        }

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSafecode() {
        return safecode;
    }

    public void setSafecode(String safecode) {
        this.safecode = safecode;
    }

    public String getStaus() {
        return staus;
    }

    public void setStaus(String staus) {
        this.staus = staus;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

}
