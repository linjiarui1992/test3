package com.ccservice.b2b2c.policy.ben;

import java.util.ArrayList;
import java.util.List;
import com.ccservice.b2b2c.base.eaccount.Eaccount;
import com.ccservice.inter.server.Server;

public class FiveonBookSearchBook {
    // 以下是51book通过航班查询获得的政策的数据
    private String fiveoneUsernameSearch;// 51book(Search.0)用户名

    private String fiveonePasswordSearch;// 51book(Search.0)安全吗

    private String fiveoneStatusSearch;// 51book(Search.0)账号状态 0 禁用 1 启用

    private String fiveoneLinknameSearch;// 51book(Search.0) 联系人

    private String fiveoneLinkphoneSearch;// 51book(Search.0) 联系人电话

    private String fiveoneTicketNotifiedUrlSearch;// 51book(Search.0) 出票通知url

    private String fiveonePaymentReturnUrlSearch;// 51book(Search.0) 支付完成后返回的url地址

    public FiveonBookSearchBook() {
        List<Eaccount> listEaccount51bookSearch = new ArrayList<Eaccount>();
        Eaccount e = Server.getInstance().getSystemService().findEaccount(11);
        if (e != null && e.getName() != null) {
            listEaccount51bookSearch.add(e);
        }
        if (listEaccount51bookSearch.size() > 0) {
            fiveoneUsernameSearch = listEaccount51bookSearch.get(0).getUsername();
            fiveonePasswordSearch = listEaccount51bookSearch.get(0).getPassword();
            fiveoneStatusSearch = listEaccount51bookSearch.get(0).getState();
            fiveoneLinknameSearch = listEaccount51bookSearch.get(0).getCreateuser();
            fiveoneLinkphoneSearch = listEaccount51bookSearch.get(0).getKeystr();
            fiveoneTicketNotifiedUrlSearch = listEaccount51bookSearch.get(0).getNourl();
            fiveonePaymentReturnUrlSearch = listEaccount51bookSearch.get(0).getPayurl();
        }
        else {
            fiveoneStatusSearch = "0";
            System.out.println("NO-51booksearch");
        }
    }

    public String getFiveoneUsernameSearch() {
        return fiveoneUsernameSearch;
    }

    public void setFiveoneUsernameSearch(String fiveoneUsernameSearch) {
        this.fiveoneUsernameSearch = fiveoneUsernameSearch;
    }

    public String getFiveonePasswordSearch() {
        return fiveonePasswordSearch;
    }

    public void setFiveonePasswordSearch(String fiveonePasswordSearch) {
        this.fiveonePasswordSearch = fiveonePasswordSearch;
    }

    public String getFiveoneStatusSearch() {
        return fiveoneStatusSearch;
    }

    public void setFiveoneStatusSearch(String fiveoneStatusSearch) {
        this.fiveoneStatusSearch = fiveoneStatusSearch;
    }

    public String getFiveoneLinknameSearch() {
        return fiveoneLinknameSearch;
    }

    public void setFiveoneLinknameSearch(String fiveoneLinknameSearch) {
        this.fiveoneLinknameSearch = fiveoneLinknameSearch;
    }

    public String getFiveoneLinkphoneSearch() {
        return fiveoneLinkphoneSearch;
    }

    public void setFiveoneLinkphoneSearch(String fiveoneLinkphoneSearch) {
        this.fiveoneLinkphoneSearch = fiveoneLinkphoneSearch;
    }

    public String getFiveoneTicketNotifiedUrlSearch() {
        return fiveoneTicketNotifiedUrlSearch;
    }

    public void setFiveoneTicketNotifiedUrlSearch(String fiveoneTicketNotifiedUrlSearch) {
        this.fiveoneTicketNotifiedUrlSearch = fiveoneTicketNotifiedUrlSearch;
    }

    public String getFiveonePaymentReturnUrlSearch() {
        return fiveonePaymentReturnUrlSearch;
    }

    public void setFiveonePaymentReturnUrlSearch(String fiveonePaymentReturnUrlSearch) {
        this.fiveonePaymentReturnUrlSearch = fiveonePaymentReturnUrlSearch;
    }
}
