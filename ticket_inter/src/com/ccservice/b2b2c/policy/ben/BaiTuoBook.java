package com.ccservice.b2b2c.policy.ben;

import java.util.ArrayList;
import java.util.List;

import com.ccservice.b2b2c.base.eaccount.Eaccount;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;

/**
 * 
 * @author 贾建磊
 * 
 */
public class BaiTuoBook {

    // 以下是百拓接口需要参数
    private String baiTuoUsername = "";// 百拓用户名

    private String baiTuoPassword = "";// 百拓密码

    private String baiTuoSupplierCode = "";// 百拓供应商编号

    private String baiTuoStaus = "0";// 百拓账号状态： 0 禁用 1启用

    public BaiTuoBook() {
        List<Eaccount> listEaccountBaiTuo = new ArrayList<Eaccount>();
        Eaccount e = Server.getInstance().getSystemService().findEaccount(10);
        if (e != null && e.getName() != null) {
            listEaccountBaiTuo.add(e);
        }
        if (listEaccountBaiTuo.size() > 0) {
            baiTuoUsername = listEaccountBaiTuo.get(0).getUsername();
            baiTuoPassword = listEaccountBaiTuo.get(0).getPassword();
            baiTuoSupplierCode = listEaccountBaiTuo.get(0).getKeystr();
            baiTuoStaus = listEaccountBaiTuo.get(0).getState();
        }
        else {
            baiTuoStaus = "0";
            System.out.println("没有百拓账号，请添加账号");
            WriteLog.write("百拓接口日志", "百拓账号：----------无此账号，请添加账号");
        }
    }

    public String getBaiTuoUsername() {
        return baiTuoUsername;
    }

    public void setBaiTuoUsername(String baiTuoUsername) {
        this.baiTuoUsername = baiTuoUsername;
    }

    public String getBaiTuoPassword() {
        return baiTuoPassword;
    }

    public void setBaiTuoPassword(String baiTuoPassword) {
        this.baiTuoPassword = baiTuoPassword;
    }

    public String getBaiTuoSupplierCode() {
        return baiTuoSupplierCode;
    }

    public void setBaiTuoSupplierCode(String baiTuoSupplierCode) {
        this.baiTuoSupplierCode = baiTuoSupplierCode;
    }

    public String getBaiTuoStaus() {
        return baiTuoStaus;
    }

    public void setBaiTuoStaus(String baiTuoStaus) {
        this.baiTuoStaus = baiTuoStaus;
    }
}
