package com.ccservice.b2b2c.policy.ben;

import java.util.ArrayList;
import java.util.List;
import com.ccservice.b2b2c.base.eaccount.Eaccount;
import com.ccservice.inter.server.Server;

public class YeeXingBook {
    // 以下是易行天下接口需要参数
    private String username = "";

    private String password = "";

    private String pay_notify_url; // 扣款成功通知地址

    private String out_notify_url; // 出票成功通知地址

    private String key;

    private String staus = "0";// 接口是否启用 1,启用 0,禁用

    public YeeXingBook() {
        List<Eaccount> listEaccount = new ArrayList<Eaccount>();
        Eaccount e = Server.getInstance().getSystemService().findEaccount(6);
        if (e != null && e.getName() != null) {
            listEaccount.add(e);
        }
        if (listEaccount.size() > 0) {
            username = listEaccount.get(0).getUsername();
            password = listEaccount.get(0).getPassword();
            pay_notify_url = listEaccount.get(0).getPayurl();
            out_notify_url = listEaccount.get(0).getNourl();
            key = listEaccount.get(0).getKeystr();
            staus = listEaccount.get(0).getState();
        }
        else {
            staus = "0";
            System.out.println("NO-yeexing");
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStaus() {
        return staus;
    }

    public void setStaus(String staus) {
        this.staus = staus;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getPay_notify_url() {
        return pay_notify_url;
    }

    public void setPay_notify_url(String pay_notify_url) {
        this.pay_notify_url = pay_notify_url;
    }

    public String getOut_notify_url() {
        return out_notify_url;
    }

    public void setOut_notify_url(String out_notify_url) {
        this.out_notify_url = out_notify_url;
    }

}
