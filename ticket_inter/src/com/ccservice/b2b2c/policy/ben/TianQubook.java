package com.ccservice.b2b2c.policy.ben;

import java.util.ArrayList;
import java.util.List;

import com.ccservice.b2b2c.base.eaccount.Eaccount;
import com.ccservice.inter.server.Server;

public class TianQubook {
    private String status;// 接口是否启用 1,启用 0,禁用  

    private String requrl = "";//访问地址

    private Integer agentid;//对应供应商id

    private String username = "";//接口用户名

    private String pwd = "";//接口密码

    public TianQubook() {

        List<Eaccount> listEaccount = new ArrayList<Eaccount>();
        Eaccount e = Server.getInstance().getSystemService().findEaccount(18);
        if (e != null && e.getName() != null) {
            listEaccount.add(e);
        }
        if (listEaccount.size() > 0) {
            requrl = listEaccount.get(0).getUrl();
            status = listEaccount.get(0).getState();
            agentid = listEaccount.get(0).getAngentid();
            username = listEaccount.get(0).getUsername();
            pwd = listEaccount.get(0).getPassword();
        }
        else {
            status = "0";
            System.out.println("NO-TianQubook");
        }

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public Integer getAgentid() {
        return agentid;
    }

    public void setAgentid(Integer agentid) {
        this.agentid = agentid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRequrl() {
        return requrl;
    }

    public void setRequrl(String requrl) {
        this.requrl = requrl;
    }

}
