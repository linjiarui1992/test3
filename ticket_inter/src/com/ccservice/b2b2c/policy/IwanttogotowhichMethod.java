package com.ccservice.b2b2c.policy;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.dom4j.DocumentHelper;

import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.ben.IwanttogotowhichBook;
import com.ccservice.inter.job.WriteLog;
import com.tenpay.util.MD5Util;

/**
 * 517na的方法 2012-8-6
 * 
 * @author 陈栋
 * 
 */
public class IwanttogotowhichMethod extends SupplyMethod {
    private static String urls = "http://open.jk.517na.com/BenefitInterface.asmx/InterfaceFacade";

    /**
     * 根据PNR的rt和pat信息获取最优政策
     * 
     * @param rt 
     * @param pat
     * @return 
     */
    public static List<Zrate> getZrateByPNR(String rt, String pat) {
        List<Zrate> zrates = new ArrayList<Zrate>();
        rt = rt.replaceAll("\r\n", "").replaceAll("", "").replaceAll(" +", " ").trim();
        pat = pat.replaceAll("\r\n", "").replaceAll("", "").replaceAll(" +", " ").trim();

        //        rt = rt.replaceAll("", "").replaceAll(" +", "").trim();
        //        pat = pat.replaceAll("", "").replaceAll(" +", "").trim();
        try {
            IwanttogotowhichBook iwanttogotowhichBook = new IwanttogotowhichBook();
            String username = iwanttogotowhichBook.getUsername();
            String password = iwanttogotowhichBook.getPassword();
            String safeCode = iwanttogotowhichBook.getSafecode();
            String pid = iwanttogotowhichBook.getPid();
            rt = "<![CDATA[" + rt + "]]>";
            pat = "<![CDATA[" + pat + "]]>";
            String temp = "<pnrcontent>" + rt + "</pnrcontent><patcontent>" + pat + "</patcontent>" + username
                    + MD5Util.MD5Encode(password, "UTF-8").toUpperCase()
                    + new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + safeCode + pid;
            String sign = MD5Util.MD5Encode(temp, "UTF-8").toUpperCase();
            String xmlParam = "<request><service>get_benefit_pnrcontent</service><pid>" + pid + "</pid><username>"
                    + username + "</username><sign>" + sign + "</sign><params><pnrcontent>" + rt
                    + "</pnrcontent><patcontent>" + pat + "</patcontent></params></request>";
            WriteLog.write("getzratebypnr_517NA", "获取最优政策请求temp:" + temp);
            WriteLog.write("getzratebypnr_517NA", "sign:" + sign);
            WriteLog.write("getzratebypnr_517NA", "url:" + urls + "?xmlParam=" + xmlParam);
            //转码
            xmlParam = URLEncoder.encode(xmlParam, "UTF-8");
            //            WriteLog.write("getzratebypnr_517NA", "url:" + urls + "?xmlParam=" + xmlParam);
            //调用请求
            String html = postUrl(urls, xmlParam);
            WriteLog.write("getzratebypnr_517NA", "根据PNR的rt和pat信息获取政策返回:" + html);
            if (html.indexOf("ERROR") >= 0) {
                //				System.out.println(html);
            }
            org.dom4j.Document doc = DocumentHelper.parseText(html);
            org.dom4j.Element root = doc.getRootElement();
            if (root.element("benefit") != null) {
                //这个item节点有多个 是个循环
                Iterator FltItems = root.element("benefit").elementIterator();
                while (FltItems.hasNext()) {
                    org.dom4j.Element FltItem = (org.dom4j.Element) FltItems.next();
                    if ("item".equals(FltItem.getName())) {
                        //是否换编码出票(找不到对应bean)
                        //			datas[17];

                        //子政策编号
                        //			datas[18];

                        //zrate.setRelationzrateid(Long.parseLong(datas[18]));
                        //支付价
                        //			datas[19];

                        //票面价格
                        //			datas[20];

                        //机场建设费
                        //			datas[21];

                        //燃油费
                        //			datas[22];

                        try {
                            String data = FltItem.getText();
                            String[] datas = data.split("\\^");
                            Zrate zrate = new Zrate();
                            zrate.setAgentid(7L);
                            //政策编号
                            zrate.setOutid(datas[0]);
                            //航空公司
                            zrate.setAircompanycode(datas[1]);
                            //出发城市
                            zrate.setDepartureport(datas[2]);
                            //到达城市
                            zrate.setArrivalport(datas[3]);
                            //航班类型  //0：没有航班限制 1：只有“航班”中的航班才适用本政策 2：“航班”中的航班不适用本政策，其它航班都适用
                            if ("0".equals(datas[4])) {

                            }
                            else if ("1".equals(datas[4])) {
                                //航班
                                zrate.setFlightnumber(datas[5]);
                            }
                            else {
                                //航班
                                zrate.setWeeknum(datas[5]);
                            }
                            //舱位
                            zrate.setCabincode(datas[6]);
                            //行程类型(————————文档显示String类型 bean中为int类型 考虑方法名错误——————)
                            if ("单程".equals(datas[7])) {
                                zrate.setVoyagetype("1");
                            }
                            else if ("往返".equals(datas[7])) {
                                zrate.setVoyagetype("2");
                            }
                            else if ("单程/往返".equals(datas[7])) {
                                zrate.setVoyagetype("3");
                            }
                            else {
                                //中转
                                zrate.setVoyagetype("4");
                            }
                            //政策类型(——————————接口文档取名与bean中命名不同 保持(1=BSP,2=B2B)一致————————)
                            zrate.setTickettype(Integer.parseInt(datas[8]));
                            //政策返点
                            zrate.setRatevalue(Float.valueOf(datas[9]));
                            //班期限制(Bean中字段名称为“适用班期”中的第一个字段 同为String类型)..
                            zrate.setLimitdate(datas[10]);
                            //出票条件（——————————————————）
                            zrate.setRemark(datas[11]);
                            //			//生效日期(————————暂理解为政策开始时间—————————)
                            SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                            zrate.setBegindate(new Timestamp(dateFormat.parse(datas[12]).getTime()));
                            //			//失效日期
                            zrate.setEnddate(new Timestamp(dateFormat.parse(datas[13]).getTime()));
                            //供应商上班时间（星期一至星期五）(——————对应的Bean中“八千亿周1到周5上班时间”——————)
                            zrate.setWorktime(datas[14].split("-")[0].replaceAll(":", ""));
                            zrate.setAfterworktime(datas[14].split("-")[1].replaceAll(":", ""));
                            //供应商上班时间（星期六至星期日）(——————对应的Bean中“八千亿周6到周7上班时间”——————)
                            zrate.setWeekendworktime(datas[15].split("-")[0].replaceAll(":", ""));
                            zrate.setWeekendaftertime(datas[15].split("-")[1].replaceAll(":", ""));
                            //是否特殊政策(对应Bean中 政策类型  1 普通  !!!!!!  2特殊)
                            if ("0".equals(datas[16])) {
                                zrate.setGeneral(1L);
                            }
                            else {
                                zrate.setGeneral(2L);
                            }
                            //供应商废票时间一到五
                            zrate.setOnetofivewastetime(datas[23].replaceAll(":", ""));
                            //供应商废票时间六到七
                            zrate.setWeekendwastetime(datas[24].replaceAll(":", ""));
                            //出票效率（！！！！！ >60 为一分钟）
                            int speed = Integer.parseInt(datas[25]) / 60;
                            zrate.setSpeed(speed + "分钟");
                            //升舱换开
                            //	officeCode公司代码	
                            zrate.setRemark(datas[26]);
                            zrates.add(zrate);
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            }
        }
        catch (Exception e) {
            UtilMethod.writeEx("IwanttogotowhichMethod", e);
            e.printStackTrace();
        }
        return zrates;
    }

    /**
     * 根据字符串转化成时间格式
     * @param time
     * @return
     */
    private static Timestamp gettimetypebyString(String time) {
        SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("yyyy/MM/dd");
        SimpleDateFormat dateFormat1 = new java.text.SimpleDateFormat("yyyy/MM/d");
        SimpleDateFormat dateFormat2 = new java.text.SimpleDateFormat("yyyy/M/d");
        time = time.split(" ")[0];
        try {
            if (time.length() == 10) {
                return new Timestamp(dateFormat.parse(time).getTime());
            }
            else {
                return new Timestamp(dateFormat1.parse(time).getTime());
            }
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        return new Timestamp(System.currentTimeMillis());
    }

    /** 
     * 根据PNR内容创建订单  
     * @param rt PNR 的rt
     * @param pat PNR 的pat
     * @param bigPnr 大PNR
     * @param benefitID 政策id
     * @param linker 联系人
     * @param linkTel 联系人电话
     * @return str 如果未创建成功返回 空字符串 成功：返回 "S|" + orderNO + "|" + payurl + "|" + officeid + "|"+ payfee;
     */
    public String creatOrderByPnr(String rt, String pat, String bigPnr, String benefitID, String linker,
            String linkTel) {
        Long l1 = System.currentTimeMillis();
        if (bigPnr == null) {
            bigPnr = getBigPnr(rt, "123456");
        }
        //		bigPnr  大编码
        //		benefitID  政策编号
        //		linker  联系人
        //		linkTel  联系人电话
        //		splitBenefitid  子政策编号
        String str = "";
        IwanttogotowhichBook iwanttogotowhichBook = new IwanttogotowhichBook();
        String username = iwanttogotowhichBook.getUsername();
        String password = iwanttogotowhichBook.getPassword();
        String safeCode = iwanttogotowhichBook.getSafecode();
        String pid = iwanttogotowhichBook.getPid();
        rt = rt.replaceAll("\r\n", "").replaceAll("", "").replaceAll(" +", " ").trim();
        pat = pat.replaceAll("\r\n", "").replaceAll("", "").replaceAll(" +", " ").trim();
        rt = "<![CDATA[" + rt + "]]>";
        pat = "<![CDATA[" + pat + "]]>";
        String splitBenefitid = "";
        try {
            String temp = "<pnrcontent>" + rt + "</pnrcontent><bigpnr>" + bigPnr + "</bigpnr><benefitid>" + benefitID
                    + "</benefitid><linker>" + linker + "</linker><linktel>" + linkTel + "</linktel><splitbenefitid>"
                    + splitBenefitid + "</splitbenefitid><patcontent>" + pat + "</patcontent>" + username
                    + MD5Util.MD5Encode(password, "UTF-8").toUpperCase() + getDate() + safeCode + pid;
            //			System.out.println(temp);
            String sign = MD5Util.MD5Encode(temp, "UTF-8").toUpperCase();
            String param = "<request>" + "<service>create_order_pnrcontent</service>" + "<pid>" + pid + "</pid>"
                    + "<username>" + username + "</username>" + "<sign>" + sign + "</sign>" + "<params>"
                    + "<pnrcontent>" + rt + "</pnrcontent>" + "<bigpnr>" + bigPnr + "</bigpnr>" + "<benefitid>"
                    + benefitID + "</benefitid>" + "<linker>" + linker + "</linker>" + "<linktel>" + linkTel
                    + "</linktel>" + "<splitbenefitid>" + splitBenefitid + "</splitbenefitid>" + "<patcontent>" + pat
                    + "</patcontent>" + "</params></request>";
            WriteLog.write("CreateOrder517NA",
                    l1 + "根据PNR内容创建订单请求temp:" + temp + " ,sign:" + sign + ",url" + urls + " ,xmlparam:" + param);
            //转码
            param = URLEncoder.encode(param, "utf-8");
            String html = postUrl(urls, param);
            WriteLog.write("CreateOrder517NA", "根据PNR内容创建订单返回数据:" + html);
            str = getxmldate(html);
        }
        catch (Exception e) {
            UtilMethod.writeEx("IwanttogotowhichMethod", e);
            e.printStackTrace();
            return "-1";
        }
        return str;
    }

    /**
     * 支付接口
     * @param rt
     * @param pat
     * @param amount 支付金额
     * @param orderid 订单号
     * @return
     */
    public String payOrder(String amount, String orderid) {
        String result = "-1";
        try {
            IwanttogotowhichBook iwanttogotowhichBook = new IwanttogotowhichBook();
            String username = iwanttogotowhichBook.getUsername();
            String password = iwanttogotowhichBook.getPassword();
            String safeCode = iwanttogotowhichBook.getSafecode();
            String pid = iwanttogotowhichBook.getPid();

            String temp = "<orderid>" + orderid + "</orderid><amount>" + amount + "</amount>" + username
                    + MD5Util.MD5Encode(password, "UTF-8").toUpperCase() + getDate() + safeCode + pid;
            String sign = MD5Util.MD5Encode(temp, "UTF-8").toUpperCase();
            String param = "<request><service>pay_order</service>" + "<pid>" + pid + "</pid>" + "<username>" + username
                    + "</username>" + "<sign>" + sign + "</sign>" + "<params>" + "<orderid>" + orderid + "</orderid>"
                    + "<amount>" + amount + "</amount>" + "</params>" + "</request>";
            WriteLog.write("自动代扣",
                    "517NA支付接口pay_order请求temp:" + temp + " ,sign:" + sign + ",url" + urls + ",xmlparam:" + param);
            param = URLEncoder.encode(param, "utf-8");
            //	String urls = "http://webservice.517na.com/BenefitInterface.asmx/InterfaceFacade";
            String xml = postUrl(urls, param);
            WriteLog.write("自动代扣", "517NA支付接口pay_order返回的xml:" + xml);
            result = getPayStats(xml);
        }
        catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            UtilMethod.writeEx("IwanttogotowhichMethod", e);
            e.printStackTrace();
            return result;
        }
        return result;
    }

    /**
     * 提交post请求
     * @param urls 
     * @param param 
     * @return
     */
    public static String postUrl(String urls, String param) {
        String html = "";
        try {
            URL url = new URL(urls);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");// 提交模式
            conn.setDoOutput(true);// 是否输入参数
            InputStreamReader isr = null;
            StringBuffer params = new StringBuffer();
            // 表单参数与get形式一样
            params.append("xmlParam").append("=").append(param);
            byte[] bypes = params.toString().getBytes();
            conn.getOutputStream().write(bypes);// 输入参数
            InputStream inStream = conn.getInputStream();
            isr = new InputStreamReader(inStream, "UTF-8");//转码
            BufferedReader in = new BufferedReader(isr);
            StringWriter out = new StringWriter();
            int c = -1;
            while ((c = in.read()) != -1) {
                out.write(c);
            }
            //            System.out.println(out.toString().replace("&lt;", "<").replace("&gt;", ">"));
            html = out.toString().replace("&lt;", "<").replace("&gt;", ">");

        }
        catch (Exception e) {
            e.printStackTrace();
            UtilMethod.writeEx("IwanttogotowhichMethod", e);
            return "";
        }
        return html;
    }

    /**
     * 解析PNR内容创建订单返回的订单信息 
     * @param html
     * @return
     */
    public String getxmldate(String html) {
        //ret = "S|" + orderNO + "|" + payurl + "|" + officeid + "|"+ payfee;
        String str = "S|";
        try {
            //			StringBuffer sbTemp = new StringBuffer("");
            //			 BufferedReader br = null;
            //			   br = new BufferedReader(new InputStreamReader(new FileInputStream(
            //			     new File("d:/html/xx.txt"))));
            //			   String line = null;
            //			   while ((line = br.readLine()) != null) {
            //				   sbTemp.append(line);
            //			   }
            org.dom4j.Document doc = DocumentHelper.parseText(html);
            org.dom4j.Element root = doc.getRootElement();
            //            System.out.println(root.asXML());
            org.dom4j.Element orderinfo = root.element("OrderInfo");
            if (orderinfo == null) {
                return "-1";
            }
            //订单号
            if (orderinfo.attributeValue("OrderId") != null && !"".equals(orderinfo.attributeValue("OrderId"))) {
                str += orderinfo.attributeValue("OrderId") + "|";
            }
            else {
                return "-1";
            }
            String payurl = "";
            Iterator FltItems = orderinfo.elementIterator();
            while (FltItems.hasNext()) {
                org.dom4j.Element FltItem = (org.dom4j.Element) FltItems.next();
                if ("PayUrl".equals(FltItem.getName())) {
                    //支付链接 取支付宝的
                    String temp = FltItem.getText().replace(";", "&");
                    if (temp.indexOf("mapi.alipay.com") != -1) {
                        payurl = temp;
                        break;
                    }
                }
            }
            if (!"".equals(payurl)) {
                str += payurl + "|";
            }
            else {
                str += "|";
            }
            //officeid
            if (orderinfo.attributeValue("OfficeCode") != null && !"".equals(orderinfo.attributeValue("OfficeCode"))) {
                str += orderinfo.attributeValue("OfficeCode") + "|";
            }
            else {
                str += "|";
            }
            //支付价格
            if (orderinfo.attributeValue("TotlePirce") != null && !"".equals(orderinfo.attributeValue("TotlePirce"))) {
                str += orderinfo.attributeValue("TotlePirce");
            }
            else {
                str += "|";
            }
            //            System.out.println(str);
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            UtilMethod.writeEx("IwanttogotowhichMethod", e);
            e.printStackTrace();
            return "-1";
        }
        return str;
    }

    /**
     * 根据支付返回的数据 判断是否成功支付 
     * @return r ：S成功 -1支付失败
     */
    public String getPayStats(String str) {
        String r = "-1";
        StringBuffer sbTemp = new StringBuffer("");
        try {
            //				BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(
            //				     new File("d:/html/xd.txt"))));
            //				   String line = null;
            //				   while ((line = br.readLine()) != null) {
            //					   sbTemp.append(line);
            //				   }
            org.dom4j.Document doc = DocumentHelper.parseText(str);
            org.dom4j.Element root = doc.getRootElement();
            //            System.out.println(root.asXML());
            org.dom4j.Element PayResult = root.element("PayResult");
            if (PayResult != null) {
                if ("True".equals(PayResult.attributeValue("PaySuccess"))) {
                    r = "S";
                }
            }
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "-1";
        }
        return r;
    }

    public static void main(String[] args) {
        IwanttogotowhichMethod IwanttogotowhichMethod = new IwanttogotowhichMethod();
        List list = IwanttogotowhichMethod.getZrateByPNR(
                "1.黄小雪 JXLBY0 2. ZH9706 Y SU28JUL XFNSZX HK1 2205 2355 E --B 3.PEK/T BJS/T-64540699/BEIJING TIAN QU AIR SERVICE LTD.,CO/CHENYIMIN ABCDEFG 4.REMARK 0722 0947 GUEST 5.TL/2005/28JUL/PEK242 6.SSR FOID ZH HK1 NI420624199711292947/P1 7.OSI ZH CTCT01088259919 8.OSI ZH CTCT13439311111 9.RMK CA/NEN4NM 10.PEK242",
                ">PAT:A 01 Y FARE:CNY1090.00 TAX:CNY50.00 YQ:CNY110.00 TOTAL:1250.00 SFC:01");
        //		if(list!=null)
        //		System.out.println(list.size());
        //根据PNR内容创建订单 
        String rt = "1.李志鹏 JQ37PT 2. JD5169 Q TH22AUG CANHGH HK1 0715 0940 E 3.PEK/T BJS/T-64540699/BEIJING TIAN QU AIR SERVICE LTD.,CO/CHENYIMIN ABCDEFG 4.REMARK 0726 1041 GUEST 5.TL/0515/22AUG/PEK242 6.SSR FOID JD HK1 NI362202199101174038/P1 7.SSR ADTK 1E BY PEK26JUL13/1241 OR CXL JD5169 Q22AUG 8.OSI JD CTCT01088259919 9.OSI JD CTCT13439311111 10.RMK CA/MKR3B7 11.PEK242";
        String pat = ">PAT:A 01 Q FARE:CNY630.00 TAX:CNY50.00 YQ:CNY110.00 TOTAL:790.00 SFC:01";
        //		String rt = "1.李志鹏 JDNPHE 2. SC4773 H FR30AUG TAOHGH HK1 0755 0930 E 3.PEK/T BJS/T-64540699/BEIJING TIAN QU AIR SERVICE LTD.,CO/CHENYIMIN ABCDEFG 4.REMARK 0725 1740 GUEST 5.TL/0555/30AUG/PEK242 6.SSR FOID SC HK1 NI362202199101174032/P1 7.SSR ADTK 1E BY PEK22AUG13/0755 OR CXL SC4773 H30AUG 8.OSI SC CTCT01088259919 9.OSI SC CTCT13439311111 10.RMK CA/NKLV23 11.PEK242 ";
        //		String pat=">PAT:A 01 H FARE:CNY720.00 TAX:CNY50.00 YQ:CNY60.00 TOTAL:830.00 SFC:01 ";
        String linker = "李志鹏";
        String linkTel = "13146595083";
        String bigPnr = "MKR3B7";
        //		String benefitID = "7904468";//7891086,7837445,7824724,7539599,6496481,7898332,6889099,7538589,7849597
        //		String s = IwanttogotowhichMethod.creatOrderByPnr(rt, pat, bigPnr, benefitID, linker, linkTel);
        //		System.out.println("-------"+s);
        //		List list =IwanttogotowhichMethod.getZrateByPNR(rt, pat);
        //		if(list!=null)
        //		System.out.println(list.size());
        String orderid = "130726104547380137";
        String amount = "740.00";
        //		
        //		IwanttogotowhichMethod.payOrder(amount, orderid);
    }
}
