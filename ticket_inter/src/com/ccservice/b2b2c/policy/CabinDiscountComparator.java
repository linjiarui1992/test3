package com.ccservice.b2b2c.policy;

import java.util.*;

import com.ccservice.b2b2c.base.flightinfo.CarbinInfo;


public class CabinDiscountComparator implements Comparator {
	public int compare(Object o1,Object o2) {
       CarbinInfo cabin1=(CarbinInfo)o1;
       CarbinInfo cabin2=(CarbinInfo)o2;
       if(cabin1.getDiscount()>cabin2.getDiscount())
           return 1;
       else
           return 0;
       }

}
