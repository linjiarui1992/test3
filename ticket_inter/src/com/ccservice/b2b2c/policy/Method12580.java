package com.ccservice.b2b2c.policy;

import java.net.MalformedURLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.atom.server.Server;
import com.ccservice.b2b2c.base.orderinfo.Orderinfo;
import com.ccservice.b2b2c.base.passenger.Passenger;
import com.ccservice.b2b2c.base.segmentinfo.Segmentinfo;
import com.ccservice.b2b2c.base.service.IAirService;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.inter.job.WriteLog;
import com.tenpay.util.MD5Util;

/**
 * 12580接口方法
 * @author chendong
 *
 */
public class Method12580 {
	static String url = "http://www.yeebooking.com:9893/ccs_interface/Retransmission12580";
	static String url12580 = "http://61.49.29.40:8181/openapi/ApiServlet";
	static String CHANNELID = "QD00016";
	static String TERMINALID = "0006000001";
	static String SECURYKEY = "HTYHSECURYKEY";
	
	
	/**
	 * 机票生成订单 UNION_FLTSAVEORDER
	 * @param order
	 * @param sinfo
	 * @param listPassenger
	 */
	public static String createorder(Orderinfo order, Segmentinfo sinfo,
			List<Passenger> listPassenger) {
		
//		String TIMESTAMP = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		String TIMESTAMP = System.currentTimeMillis()+"";
		//机票生成订单
		String REQUESTTYPE = "UNION_FLTSAVEORDER";
		//0007200001代表航信接口，0007200002 代表春秋航空直连接口，0007200003代表东航直连接口
		String FITAPITYPE = "0007200001";
		String CARRIER = sinfo.getFlightnumber().substring(0,2);//承运人,航空公司二字码
		String AIRORDERTYPE = "";
		String FLIGHTNO = sinfo.getFlightnumber();//航班号，不含航空公司代码
		String DEPDATE = sinfo.getDeparttime().toString().substring(0, 10);//航班起飞日期格式：yyyy-MM-dd
		String DEPTIME = sinfo.getDeparttime().toString().substring(11, 16);//航班起飞时间格式：hh:mm
		String ARRTIME = sinfo.getArrivaltime().toString().substring(11, 16);//航班到达时间格式：hh:mm
		String FROMCITYCODE = sinfo.getDepartureCtiy();
		String TOCITYCODE = sinfo.getArriveCity();
		String RATE = sinfo.getDiscount()+"";
		String CABINCODE = sinfo.getCabincode();
		
		String PID = sinfo.getZrate().getOutid();
		String POLICYID = sinfo.getZrate().getOutid();
		String AIRPORTFEE = sinfo.getAirportfee()+"";
		String FUELTAX = sinfo.getFuelfee()+"";
		
		String PASSENGERINFOS = "";
		for (int i = 0; i < listPassenger.size(); i++) {
			int tempSEQ = i+1;
			Passenger passenger = listPassenger.get(i);
			String TYPE = "";
			if(passenger.getPtype()==1){
				TYPE = "0002300001";
			} else if(passenger.getPtype()==2){
				TYPE = "0002300002";
			} else if(passenger.getPtype()==3){
				TYPE = "0002300003";
			}
			
//			0002100001	身份证
//			0002100002	护照
//			0002100003	回乡证
//			0002100004	台胞证
//			0002100005	军人证
//			0002100006	港澳通行证
//			0002100007	其他有效证件
			String CREDENTIALTYPE = "";
			if(passenger.getIdtype()==1){
				CREDENTIALTYPE="0002100001";
			} else if (passenger.getIdtype()==3){
				CREDENTIALTYPE="0002100002";
			} else if (passenger.getIdtype()==4){
				CREDENTIALTYPE="0002100006";
			} else if (passenger.getIdtype()==6){
				CREDENTIALTYPE="0002100004";
			} else if (passenger.getIdtype()==7){
				CREDENTIALTYPE="0002100003";
			} else if (passenger.getIdtype()==8){
				CREDENTIALTYPE="0002100005";
			} else {
				CREDENTIALTYPE="0002100007";
			}
			
			
			PASSENGERINFOS +="<PASSENGERINFO><SEQ>"+tempSEQ+"</SEQ><NAME>"+passenger.getName()+"</NAME><TYPE>"+TYPE+"</TYPE><CREDENTIALTYPE>"+CREDENTIALTYPE+"</CREDENTIALTYPE>" +
					"<CREDENTIALNO>"+passenger.getIdnumber()+"</CREDENTIALNO><TELEPHONE>13439311111</TELEPHONE><BIRTHDAY></BIRTHDAY><CARDONE/><CARDTWO/>" +
					"<FOLLOWPASSENGERID/><INFANTPINYIN/></PASSENGERINFO>";
		}
		
//		String INSURANCELIST = "<INSURANCE><PSGNAME>张三</PSGNAME><FORMAIRPORTCODE>PEK</FORMAIRPORTCODE>" +
//		"<TOAIRPORTCODE>SHA</TOAIRPORTCODE><INSURANCETYPEID>8a8185c83a840e4a013a87adfa</INSURANCETYPEID><INSURANCENUM>1</INSURANCENUM>" +
//		"<INSURANCETEL>13812365487</INSURANCETEL></INSURANCE><INSURANCE><PSGNAME>李四</PSGNAME><FORMAIRPORTCODE>SHA</FORMAIRPORTCODE>" +
//		"<TOAIRPORTCODE>PEK</TOAIRPORTCODE><INSURANCETYPEID>8a8185c83a840e4a013a87adfa</INSURANCETYPEID><INSURANCENUM>2</INSURANCENUM>" +
//		"<INSURANCETEL>13812365487</INSURANCETEL></INSURANCE>";
		String INSURANCELIST = "<INSURANCE></INSURANCE>";
		
//		String PARAMETERCRM = "<CALLCUSTOMER>呼入联系人</CALLCUSTOMER><ORDERCUSTOMER>订单联系人</ORDERCUSTOMER><ORDERTEL>订单联系人电话</ORDERTEL>" +
//				"<CONFIRMTYPE>确认方式</CONFIRMTYPE><CONFIRM>订单确认内容</CONFIRM><ACTNUMBER>参加活动号码</ACTNUMBER>";
		String PARAMETERCRM = "";
		
		
		//API输入参数签名结果
		String REQUESTXML = "<REQUESTXML><QUEUEID>C615E91A58344B92AC311855DFE70981</QUEUEID><ORDER><OUTTICKETCITYCN></OUTTICKETCITYCN><AIRORDERTYPE>"+AIRORDERTYPE+"</AIRORDERTYPE>" +
				"<GROUPUSERID></GROUPUSERID><ISSUITTICKET>0</ISSUITTICKET><SERVICEID></SERVICEID><MEMBERCARDNO></MEMBERCARDNO><MEMBERNAME></MEMBERNAME>" +
				"<CUSTOMERTYPE></CUSTOMERTYPE><MEMBERTEL></MEMBERTEL><ORDERLEVEL></ORDERLEVEL><LEVELOTHERRSN></LEVELOTHERRSN><SPECIALRMK></SPECIALRMK><COEFFICIENT></COEFFICIENT><TICKETPRICE></TICKETPRICE>" +
				"<ROUTES><ROUTE><FLIGHTS><FLIGHT><SEQ>0</SEQ><CARRIER>"+CARRIER+"</CARRIER><FLIGHTNO>"+FLIGHTNO+"</FLIGHTNO><DEPDATE>"+DEPDATE+"</DEPDATE><DEPTIME>"+DEPTIME+"</DEPTIME>" +
				"<ARRDATE>"+DEPDATE+"</ARRDATE><ARRTIME>"+ARRTIME+"</ARRTIME><OUTTERMINAL></OUTTERMINAL><OUTTERMINALCN></OUTTERMINALCN><INTERMINAL></INTERMINAL>" +
				"<INTERMINALCN></INTERMINALCN><PLANESTYLE></PLANESTYLE><FROMCITYCODE>"+FROMCITYCODE+"</FROMCITYCODE><TOCITYCODE>"+TOCITYCODE+"</TOCITYCODE>" +
				"<DEPAIRPORT>"+FROMCITYCODE+"</DEPAIRPORT><ARRAIRPORT>"+TOCITYCODE+"</ARRAIRPORT><VIAPORT>0</VIAPORT><TPM>0</TPM><ELAPSETIME>0分钟</ELAPSETIME>" +
				"<CABIN><TKTAMOUNTADT></TKTAMOUNTADT><TKTAMOUNTCHD></TKTAMOUNTCHD><TKTAMOUNTINF></TKTAMOUNTINF><DISTTKTAMOUNTADT></DISTTKTAMOUNTADT>" +
				"<DISTTKTAMOUNTCHD></DISTTKTAMOUNTCHD><DISTTKTAMOUNTINF></DISTTKTAMOUNTINF><TERMINALAMOUNTADT></TERMINALAMOUNTADT>" +
				"<TERMINALAMOUNTCHD></TERMINALAMOUNTCHD><TERMINALAMOUNTINF></TERMINALAMOUNTINF><RATE>"+RATE+"</RATE><CABINCODE>"+CABINCODE+"</CABINCODE>" +
				"<CHDCABINCODE></CHDCABINCODE><REFUNDRMK><![CDATA[]]></REFUNDRMK><ENDORSEMENTRMK><![CDATA[]]></ENDORSEMENTRMK><SIGNRMK><![CDATA[]]></SIGNRMK>" +
				"<SPECIALRMK><![CDATA[]]></SPECIALRMK><PID>"+PID+"</PID><POLICYID>"+POLICYID+"</POLICYID>" +
				"<CHDPOLICYID></CHDPOLICYID><ETTYPE>1</ETTYPE><ISAUTOETDZ>0</ISAUTOETDZ><ISNEEDCHANGEPNR>0</ISNEEDCHANGEPNR>" +
				"<HASINVOCCE>1</HASINVOCCE><FITSENDTYPE>0000200002</FITSENDTYPE><PRODUCTID/><PRODUCTNAME/><AIRPORTFEE>"+AIRPORTFEE+"</AIRPORTFEE>" +
				"<FUELTAX>"+FUELTAX+"</FUELTAX><FUELTAXCHL></FUELTAXCHL><FUELTAXINF>0</FUELTAXINF><ISPAT>0</ISPAT><FITAPITYPE>"+FITAPITYPE+"</FITAPITYPE></CABIN>" +
				"</FLIGHT></FLIGHTS></ROUTE></ROUTES><PASSENGERINFOS>"+PASSENGERINFOS+"</PASSENGERINFOS><INSURANCELIST>"+INSURANCELIST+"</INSURANCELIST>" +
				"<PARAMETERCRM>"+PARAMETERCRM+"</PARAMETERCRM><DELIVERY><TYPE>0000200002</TYPE><DELIVERYCITY></DELIVERYCITY>" +
				"<RECEIVEUSER></RECEIVEUSER><USERPHONE></USERPHONE><EXPRESSAREA></EXPRESSAREA><ADDRESS></ADDRESS><DELEVERYFEE></DELEVERYFEE><STARTTIME></STARTTIME>" +
				"<ENDTIME></ENDTIME><PROTIMESTART></PROTIMESTART><PROTIMEEND></PROTIMEEND><SELFADR></SELFADR><PMTGETBEGIN></PMTGETBEGIN><PMTGETEND></PMTGETEND>" +
				"<SMSTELPHONE></SMSTELPHONE><TRAITICITY></TRAITICITY><TRAITIADD></TRAITIADD><TRAITISMSTEL></TRAITISMSTEL>" +
				"<RECCIPIENTS></RECCIPIENTS><RECIPIENTSTEL></RECIPIENTSTEL><MAILADDRESS></MAILADDRESS><ZIPCODE></ZIPCODE></DELIVERY>" +
				"<VOUCHERLIST><VOUCHER><COUPONCODE>521834925736152210</COUPONCODE><TELPHONE></TELPHONE><SENDSTATUS>0</SENDSTATUS>" +
				"<ENDTIME></ENDTIME><MEMO/><OPTUSER>admin</OPTUSER></VOUCHER></VOUCHERLIST></ORDER></REQUESTXML>";
		String SIGNATURE = CHANNELID+TERMINALID+TIMESTAMP+REQUESTTYPE+SECURYKEY+REQUESTXML;
		SIGNATURE = MD5Util.MD5Encode(SIGNATURE.replaceAll(" ", ""), "utf-8");
		String paramContent = "<?xml version=\"1.0\" encoding=\"utf-8\"?><REQUEST><HEADER CHANNELID=\""
				+ CHANNELID
				+ "\" "
				+ "CHANNELDEPID=\"\"  TERMINALID=\""
				+ TERMINALID
				+ "\"  TIMESTAMP=\""
				+ TIMESTAMP
				+ "\" REQUESTTYPE=\""
				+ REQUESTTYPE
				+ "\" "
				+ "SIGNATURE=\""
				+ SIGNATURE + "\" />" + REQUESTXML + "</REQUEST>";
		
		paramContent = "url="+url12580+"&paramContent="+paramContent;
		WriteLog.write("12580CreateOrder",  url);
		WriteLog.write("12580CreateOrder", paramContent);
		String resultData = SendPostandGet.submitPost(url, paramContent).toString().trim();
		System.out.println(resultData);
		WriteLog.write("12580CreateOrder", resultData);
		
		return "";
	}
	
	public static void UNION_PATAVERIFI(String FLIGHTNO,String DATE){
		String TIMESTAMP = System.currentTimeMillis()+"";
		String REQUESTTYPE = "UNION_PATAVERIFI";
		String REQUESTXML = "<REQUESTXML><FLIGHTNO>"+FLIGHTNO+"</FLIGHTNO><DATE>"+DATE+"</DATE></REQUESTXML>";
		
		String SIGNATURE = CHANNELID+TERMINALID+TIMESTAMP+REQUESTTYPE+SECURYKEY+REQUESTXML;
		SIGNATURE = MD5Util.MD5Encode(SIGNATURE.replaceAll(" ", ""), "utf-8");
		String paramContent = "<?xml version=\"1.0\" encoding=\"utf-8\"?><REQUEST><HEADER CHANNELID=\""
				+ CHANNELID
				+ "\" "
				+ "CHANNELDEPID=\"\"  TERMINALID=\""
				+ TERMINALID
				+ "\"  TIMESTAMP=\""
				+ TIMESTAMP
				+ "\" REQUESTTYPE=\""
				+ REQUESTTYPE
				+ "\" "
				+ "SIGNATURE=\""
				+ SIGNATURE + "\" />" + REQUESTXML + "</REQUEST>";
		
		paramContent = "url="+url12580+"&paramContent="+paramContent;
		WriteLog.write("12580UNION_FLTSTOPINQUERY",  url);
		WriteLog.write("12580UNION_FLTSTOPINQUERY", paramContent);
		String resultData = SendPostandGet.submitPost(url, paramContent).toString().trim();
		System.out.println(resultData);
		WriteLog.write("12580UNION_FLTSTOPINQUERY", resultData);
	}
	
	/**
	 * 航班经停信息查询
	 * @param FLIGHTNO
	 * @param DATE
	 */
	public static void UNION_FLTSTOPINQUERY(String FLIGHTNO,String DATE){

		String TIMESTAMP = System.currentTimeMillis()+"";
		String REQUESTTYPE = "UNION_INSURANCELIST";
		String REQUESTXML = "<REQUESTXML><FLIGHTNO>"+FLIGHTNO+"</FLIGHTNO><DATE>"+DATE+"</DATE></REQUESTXML>";
		
		String SIGNATURE = CHANNELID+TERMINALID+TIMESTAMP+REQUESTTYPE+SECURYKEY+REQUESTXML;
		SIGNATURE = MD5Util.MD5Encode(SIGNATURE.replaceAll(" ", ""), "utf-8");
		String paramContent = "<?xml version=\"1.0\" encoding=\"utf-8\"?><REQUEST><HEADER CHANNELID=\""
				+ CHANNELID
				+ "\" "
				+ "CHANNELDEPID=\"\"  TERMINALID=\""
				+ TERMINALID
				+ "\"  TIMESTAMP=\""
				+ TIMESTAMP
				+ "\" REQUESTTYPE=\""
				+ REQUESTTYPE
				+ "\" "
				+ "SIGNATURE=\""
				+ SIGNATURE + "\" />" + REQUESTXML + "</REQUEST>";
		
		paramContent = "url="+url12580+"&paramContent="+paramContent;
		WriteLog.write("12580UNION_FLTSTOPINQUERY",  url);
		WriteLog.write("12580UNION_FLTSTOPINQUERY", paramContent);
		String resultData = SendPostandGet.submitPost(url, paramContent).toString().trim();
		System.out.println(resultData);
		WriteLog.write("12580UNION_FLTSTOPINQUERY", resultData);
	}
	
	/**
	 * 机票订单列表
	 * @param STARTTIME 下单开始日期（只针对按照下单日期查询）
	 * @param ENDTIME 下单结束日期（只针对按照下单日期查询）
	 * @param PAGENUM 每页条数（只针对按照下单日期查询）
	 * @param PAGE 页码（只针对按照下单日期查询）
	 * @param ORDERSTATE 订单状态,多个状态用,隔开
	 */
	public static void UNION_FLTORDERLIST(String STARTTIME,String ENDTIME,String PAGENUM,String PAGE,String ORDERSTATE){
		String TIMESTAMP = System.currentTimeMillis()+"";
		String REQUESTTYPE = "UNION_FLTORDERLIST";
		String REQUESTXML = "<REQUESTXML><MEMBERNO></MEMBERNO><CALL_PHONE>13439311111</CALL_PHONE><QUERYTYPE>2</QUERYTYPE><STARTTIME>"
				+ STARTTIME
				+ "</STARTTIME><ENDTIME>"
				+ ENDTIME
				+ "</ENDTIME><PAGENUM>"
				+ PAGENUM
				+ "</PAGENUM><PAGE>"
				+ PAGE
				+ "</PAGE><ORDERSTATE>"
				+ ORDERSTATE
				+ "</ORDERSTATE></REQUESTXML>";

		String SIGNATURE = CHANNELID+TERMINALID+TIMESTAMP+REQUESTTYPE+SECURYKEY+REQUESTXML;
		SIGNATURE = MD5Util.MD5Encode(SIGNATURE.replaceAll(" ", ""), "utf-8");
		String paramContent = "<?xml version=\"1.0\" encoding=\"utf-8\"?><REQUEST><HEADER CHANNELID=\""
				+ CHANNELID
				+ "\" "
				+ "CHANNELDEPID=\"\"  TERMINALID=\""
				+ TERMINALID
				+ "\"  TIMESTAMP=\""
				+ TIMESTAMP
				+ "\" REQUESTTYPE=\""
				+ REQUESTTYPE
				+ "\" "
				+ "SIGNATURE=\""
				+ SIGNATURE + "\" />" + REQUESTXML + "</REQUEST>";
		
		paramContent = "url="+url12580+"&paramContent="+paramContent;
		WriteLog.write("12580UNION_FLTORDERLIST",  url);
		WriteLog.write("12580UNION_FLTORDERLIST", paramContent);
		String resultData = SendPostandGet.submitPost(url, paramContent).toString().trim();
		System.out.println(resultData);
		WriteLog.write("12580UNION_FLTORDERLIST", resultData);
	}
	
	/**
	 * 机票订单详情 UNION_FLTORDERDETAIL
	 * @param ORDERID 订单号
	 */
	public static void UNION_FLTORDERDETAIL(String ORDERID){
		String TIMESTAMP = System.currentTimeMillis()+"";
		String REQUESTTYPE = "UNION_FLTORDERDETAIL";
		String REQUESTXML = "<REQUESTXML><ORDERID>"+ORDERID+"</ORDERID></REQUESTXML>";

		String SIGNATURE = CHANNELID+TERMINALID+TIMESTAMP+REQUESTTYPE+SECURYKEY+REQUESTXML;
		SIGNATURE = MD5Util.MD5Encode(SIGNATURE.replaceAll(" ", ""), "utf-8");
		String paramContent = "<?xml version=\"1.0\" encoding=\"utf-8\"?><REQUEST><HEADER CHANNELID=\""
				+ CHANNELID
				+ "\" "
				+ "CHANNELDEPID=\"\"  TERMINALID=\""
				+ TERMINALID
				+ "\"  TIMESTAMP=\""
				+ TIMESTAMP
				+ "\" REQUESTTYPE=\""
				+ REQUESTTYPE
				+ "\" "
				+ "SIGNATURE=\""
				+ SIGNATURE + "\" />" + REQUESTXML + "</REQUEST>";
		
		paramContent = "url="+url12580+"&paramContent="+paramContent;
		WriteLog.write("12580UNION_FLTORDERDETAIL",  url);
		WriteLog.write("12580UNION_FLTORDERDETAIL", paramContent);
		String resultData = SendPostandGet.submitPost(url, paramContent).toString().trim();
		System.out.println(resultData);
		WriteLog.write("12580UNION_FLTORDERDETAIL", resultData);
	}
	
	public static void autopay(){
		
	}
	
	/**
	 * 保险产品列表
	 */
	public static void UNION_INSURANCELIST(){
		
		String TIMESTAMP = System.currentTimeMillis()+"";
		String REQUESTTYPE = "UNION_INSURANCELIST";
		String REQUESTXML = "<REQUESTXML><ALN>CA</ALN></REQUESTXML>";
		
		String SIGNATURE = CHANNELID+TERMINALID+TIMESTAMP+REQUESTTYPE+SECURYKEY+REQUESTXML;
		SIGNATURE = MD5Util.MD5Encode(SIGNATURE.replaceAll(" ", ""), "utf-8");
		String paramContent = "<?xml version=\"1.0\" encoding=\"utf-8\"?><REQUEST><HEADER CHANNELID=\""
				+ CHANNELID
				+ "\" "
				+ "CHANNELDEPID=\"\"  TERMINALID=\""
				+ TERMINALID
				+ "\"  TIMESTAMP=\""
				+ TIMESTAMP
				+ "\" REQUESTTYPE=\""
				+ REQUESTTYPE
				+ "\" "
				+ "SIGNATURE=\""
				+ SIGNATURE + "\" />" + REQUESTXML + "</REQUEST>";
		
		paramContent = "url="+url12580+"&paramContent="+paramContent;
		WriteLog.write("12580UNION_INSURANCELIST",  url);
		WriteLog.write("12580UNION_INSURANCELIST", paramContent);
		String resultData = SendPostandGet.submitPost(url, paramContent).toString().trim();
		System.out.println(resultData);
		WriteLog.write("12580UNION_INSURANCELIST", resultData);
	}
	
	/**
	 * X日特价航班信息
	 * @param from 出发三字码
	 * @param to 到达三字码
	 * @param startfltdate 开始时间
	 * @param endfltdate 结束时间
	 */
	public static void UNION_FLTVIEWBARGAINBYDAY(String from,String to,String startfltdate,String endfltdate){
		String TIMESTAMP = System.currentTimeMillis()+"";
		String REQUESTTYPE = "UNION_FLTVIEWBARGAINBYDAY";
		String REQUESTXML = "<REQUESTXML><FROM>"+from+"</FROM><TO>"+to+"</TO><STARTFLTDATE>"+startfltdate+"</STARTFLTDATE><ENDFLTDATE>"+endfltdate+"</ENDFLTDATE></REQUESTXML>";
		String SIGNATURE = CHANNELID+TERMINALID+TIMESTAMP+REQUESTTYPE+SECURYKEY+REQUESTXML;
		SIGNATURE = MD5Util.MD5Encode(SIGNATURE.replaceAll(" ", ""), "utf-8");
		String paramContent = "<?xml version=\"1.0\" encoding=\"utf-8\"?><REQUEST><HEADER CHANNELID=\""
				+ CHANNELID
				+ "\" "
				+ "CHANNELDEPID=\"\"  TERMINALID=\""
				+ TERMINALID
				+ "\"  TIMESTAMP=\""
				+ TIMESTAMP
				+ "\" REQUESTTYPE=\""
				+ REQUESTTYPE
				+ "\" "
				+ "SIGNATURE=\""
				+ SIGNATURE + "\" />" + REQUESTXML + "</REQUEST>";
		
		paramContent = "url="+url12580+"&paramContent="+paramContent;
		WriteLog.write("12580UNION_FLTVIEWBARGAINBYDAY",  url);
		WriteLog.write("12580UNION_FLTVIEWBARGAINBYDAY", paramContent);
		String resultData = SendPostandGet.submitPost(url, paramContent).toString().trim();
		System.out.println(resultData);
		WriteLog.write("12580UNION_FLTVIEWBARGAINBYDAY", resultData);
		
		try {
			org.dom4j.Document document = org.dom4j.DocumentHelper
					.parseText(resultData);
			org.dom4j.Element root = document.getRootElement();
			org.dom4j.Element RESULTMSGelement = root.element("RESULTMSG");
			//请求处理成功！
			String resultString = RESULTMSGelement.getTextTrim();
			System.out.println(resultString);
			org.dom4j.Element HEADERelement = root.element("HEADER");
			//SUCCESS
			String RESULTString = HEADERelement.attributeValue("RESULT");
			org.dom4j.Element REQUESTRESULTelement = root.element("REQUESTRESULT");
			List list = REQUESTRESULTelement.element("DATES").elements("D");
			Iterator it = list.iterator();
			while (it.hasNext()) {
				org.dom4j.Element elmt = (org.dom4j.Element) it.next();
				System.out.println(elmt.attributeValue("DATE")+":"+elmt.attributeValue("LOWPRICE")+":"+elmt.attributeValue("RATE"));
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	/**
	 * X日特价航班信息
	 */
	public static void UNION_FLTVIEWBARGAINBYCITY(){
		String TIMESTAMP = System.currentTimeMillis()+"";
		String REQUESTTYPE = "UNION_FLTVIEWBARGAINBYCITY";
		String REQUESTXML = "<REQUESTXML></REQUESTXML>";
		String SIGNATURE = CHANNELID+TERMINALID+TIMESTAMP+REQUESTTYPE+SECURYKEY+REQUESTXML;
		SIGNATURE = MD5Util.MD5Encode(SIGNATURE.replaceAll(" ", ""), "utf-8");
		String paramContent = "<?xml version=\"1.0\" encoding=\"utf-8\"?><REQUEST><HEADER CHANNELID=\""
				+ CHANNELID
				+ "\" "
				+ "CHANNELDEPID=\"\"  TERMINALID=\""
				+ TERMINALID
				+ "\"  TIMESTAMP=\""
				+ TIMESTAMP
				+ "\" REQUESTTYPE=\""
				+ REQUESTTYPE
				+ "\" "
				+ "SIGNATURE=\""
				+ SIGNATURE + "\" />" + REQUESTXML + "</REQUEST>";
		
		paramContent = "url="+url12580+"&paramContent="+paramContent;
		WriteLog.write("12580UNION_FLTVIEWBARGAINBYCITY",  url);
		WriteLog.write("12580UNION_FLTVIEWBARGAINBYCITY", paramContent);
		String resultData = SendPostandGet.submitPost(url, paramContent).toString().trim();
		System.out.println(resultData);
		WriteLog.write("12580UNION_FLTVIEWBARGAINBYCITY", resultData);
		
		try {
			org.dom4j.Document document = org.dom4j.DocumentHelper
					.parseText(resultData);
			org.dom4j.Element root = document.getRootElement();
			org.dom4j.Element RESULTMSGelement = root.element("RESULTMSG");
			//请求处理成功！
			String resultString = RESULTMSGelement.getTextTrim();
			System.out.println(resultString);
			org.dom4j.Element HEADERelement = root.element("HEADER");
			//SUCCESS
			String RESULTString = HEADERelement.attributeValue("RESULT");
			org.dom4j.Element HOTLISTelement = root.element("REQUESTRESULT").element("HOTLIST");
			List list = HOTLISTelement.elements("GROUP");
			Iterator it = list.iterator();
			while (it.hasNext()) {
				org.dom4j.Element elmt = (org.dom4j.Element) it.next();
				System.out.println(elmt.attributeValue("POINT"));
				
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	
	
	public static void main(String[] args) {
//		Orderinfo order = new Orderinfo();
//		Segmentinfo sinfo = new Segmentinfo();
//		sinfo.setFlightnumber("CA1331");
//		sinfo.setDeparttime(new Timestamp(113, 9, 27, 21, 30, 0, 0));
//		sinfo.setArrivaltime(new Timestamp(113, 9, 27, 22, 50, 0, 0));
//		sinfo.setDepartureCtiy("PEK");
//		sinfo.setArriveCity("CGO");
//		sinfo.setCabincode("Y");
//		sinfo.setDiscount(10f);
//		Zrate zrate = new Zrate();
//		zrate.setOutid("fa5b69c24cbc47838aacc99054ec9918");
//		sinfo.setZrate(zrate);
//		sinfo.setAirportfee(50f);
//		sinfo.setFuelfee(60f);
//		List<Passenger> listPassenger = new ArrayList<Passenger>();
//		Passenger Passenger = new Passenger();
//		Passenger.setPtype(1);
//		Passenger.setName("陈栋");
//		Passenger.setIdnumber("412823198909298017");
//		Passenger.setIdtype(1);
//		listPassenger.add(Passenger);
//		createorder(order, sinfo, listPassenger);
		//UNION_INSURANCELIST();
		//UNION_FLTVIEWBARGAINBYDAY("PEK", "SHA", "2013-10-25", "2013-10-28");
		//UNION_FLTVIEWBARGAINBYCITY();
		//UNION_FLTSTOPINQUERY("SC4795","2013-10-22");
	}




	public static List<Zrate> getZrateByFlightNumber(String scity,
			String ecity, String sdate, String flightnumber, String cabin,
			String url2) {
		// TODO Auto-generated method stub
		String where = "where C_AGENTID=11 and C_BEGINDATE = '"+sdate+"' and C_DEPARTUREPORT='"+scity+"' and C_ARRIVALPORT='"+ecity+
							"' and C_AIRCOMPANYCODE='"+flightnumber.substring(0,2)+"' and C_FLIGHTNUMBER='"+flightnumber+"' and C_CABINCODE='"+cabin+"'";
		url2 = url2 + "/cn_service/service/";
		HessianProxyFactory factory = new HessianProxyFactory();
		List<Zrate> zrates = new ArrayList<Zrate>();
		try {
			IAirService iAirService = (IAirService) factory.create(IAirService.class, url2 + IAirService.class.getSimpleName());
			
			zrates = iAirService.findAllZrate(where, "ORDER BY ID DESC", -1, 0);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		if(zrates.size()==0){
			zrates = new ArrayList<Zrate>();
		}
		return zrates;
	}
}
