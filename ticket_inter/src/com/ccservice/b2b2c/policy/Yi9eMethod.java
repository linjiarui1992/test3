package com.ccservice.b2b2c.policy;

import java.rmi.RemoteException;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.apache.axis2.AxisFault;

import com.alibaba.fastjson.JSONObject;
import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.base.eaccount.Eaccount;
import com.ccservice.b2b2c.base.orderinfo.Orderinfo;
import com.ccservice.b2b2c.base.segmentinfo.Segmentinfo;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.test.HttpClient;

import client.*;
import client.RateServiceImplServiceStub.Rate;

/**
 * 19E 的方法
 * 
 * @author chendong
 * 
 */
public class Yi9eMethod extends SupplyMethod {

    public static void main(String[] args) {
        //        Orderinfo order = new Orderinfo();
        //        order.setPnr("HDLP29");
        //        order.setUserRtInfo("1.杨大海 HDLP29                                                                "
        //                + " 2.  CA4182 V   SA28DEC  TYNCTU HK1   2100 2305          E --T2 V1              "
        //                + " 3.BJS/T BJS/T 69231033/1593/BJS XU YI HOLIDAY TICKET AGENCY LTD./ZHOU YUN      "
        //                + "     ABCDEFG                                                                    "
        //                + " 4.REMARK 1227 0826 GUEST                                                       "
        //                + " 5.TL/1900/28DEC/BJS849                                                         "
        //                + " 6.SSR FOID CA HK1 NI510902196804183973/P1                                      "
        //                + " 7.SSR ADTK 1E BY BJS27DEC13/0926 OR CXL CA ALL SEGS                            "
        //                + " 8.OSI CA CTCT13439311111                                                       "
        //                + " 9.RMK CA/NGT3GM                                                                " + "10.BJS849");
        //        order.setPnrpatinfo(">PAT:A                                                                          "
        //                + "01 V1 FARE:CNY460.00 TAX:CNY50.00 YQ:CNY120.00  TOTAL:630.00                    " + "SFC:01");
        //        //		List<Zrate> lists = getBestZratebyPnr_Note(order, 1);
        //        //		order.setRatevalue(lists.get(0).getRatevalue());
        //        //		Segmentinfo sinfo = new Segmentinfo();
        //        //		sinfo.setZrate(lists.get(0));
        //        //		createOrderbyPnr(order, sinfo);
        //        autopay("OR201409050958138330", "706.0");
        //        String sql = "select c_aircomapnycode from T_segmentinfo where c_orderid=1";
        //        String comCode = "";
        //        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        String rt = "rt999\n11";
        System.out.println(rt);
    }

    /**
     * 19E根据pnr和rt内容获取政策信息
     * @param order
     * @param special
     * @return
     */
    public static List<Zrate> getBestZratebyPnr_Note(Orderinfo order, int special) {
        Eaccount eaccount = Server.getInstance().getSystemService().findEaccount(15);
        long orderid = order.getId();
        List<Zrate> zrates = new ArrayList<Zrate>();
        //        try {
        RateServiceImplServiceStub stub = null;
        try {
            stub = new RateServiceImplServiceStub();
        }
        catch (AxisFault e1) {
            e1.printStackTrace();
        }
        RateServiceImplServiceStub.GetAgencyFeeByPnrE getAgencyFeeByPnrE = new RateServiceImplServiceStub.GetAgencyFeeByPnrE();
        RateServiceImplServiceStub.GetAgencyFeeByPnr getAgencyFeeByPnr = new RateServiceImplServiceStub.GetAgencyFeeByPnr();
        RateServiceImplServiceStub.AgencyFeeByPnrRequest param = new RateServiceImplServiceStub.AgencyFeeByPnrRequest();
        int appcode = 19;
        String pnr = order.getPnr();
        String Saftcode = eaccount.getPwd();
        param.setAppcode(appcode);
        param.setPnr(pnr);
        WriteLog.write("getzratebypnr_19EgetBestZratebyPnr_Note", orderid + ":pnr:" + pnr);
        String rtInfo = "rt" + pnr + "\n" + order.getUserRtInfo().replaceAll("", ""); //pnr的rt信息
        String patInfo = "pat:a\n" + order.getPnrpatinfo().replaceAll("", "");//pnr的pat信息
        WriteLog.write("getzratebypnr_19EgetBestZratebyPnr_Note", orderid + ":rtInfo:" + rtInfo);
        WriteLog.write("getzratebypnr_19EgetBestZratebyPnr_Note", orderid + ":patInfo:" + patInfo);
        String Pnr_content = rtInfo + patInfo;
        WriteLog.write("getzratebypnr_19EgetBestZratebyPnr_Note", orderid + ":Pnr_content:" + Pnr_content);
        param.setPnr_content(Pnr_content);
        String Aircomapnycode = getComCodeByOrderid(order.getId());
        String isVirtualPnr = new Yi9eMethod().getisVirtualPnr(Aircomapnycode, 1, 1);
        String pnr_type = "2"; //1- PNR小编导入(目前不支持),2-  2-黑屏导入 3-  3.虚拟PNR导入
        if ("1".equals(isVirtualPnr)) {
            pnr_type = "3";
        }
        param.setPnr_type(pnr_type);
        WriteLog.write("getzratebypnr_19EgetBestZratebyPnr_Note", orderid + ":pnr_type:" + pnr_type);
        String Sign = "AgencyFeeByPnrRequest" + order.getPnr() + pnr_type + eaccount.getUsername() + appcode + Saftcode;
        WriteLog.write("getzratebypnr_19EgetBestZratebyPnr_Note", orderid + ":Sign:" + Sign);
        try {
            Sign = HttpClient.MD5(Sign);
            WriteLog.write("getzratebypnr_19EgetBestZratebyPnr_Note", orderid + ":Sign:" + Sign);
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        param.setSign(Sign);
        param.setUsername(eaccount.getUsername());
        WriteLog.write("getzratebypnr_19EgetBestZratebyPnr_Note",
                orderid + ":appcode:" + appcode + ":pnr:" + order.getPnr() + ":Username:" + eaccount.getUsername());
        getAgencyFeeByPnr.setArg0(param);
        getAgencyFeeByPnrE.setGetAgencyFeeByPnr(getAgencyFeeByPnr);
        RateServiceImplServiceStub.AgencyFeeByPnrResponse response = null;
        try {
            response = stub.getAgencyFeeByPnr(getAgencyFeeByPnrE).getGetAgencyFeeByPnrResponse().get_return();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        WriteLog.write("getzratebypnr_19EgetBestZratebyPnr_Note", orderid + ":" + response.getCode());
        if ("S".equals(response.getCode())) {
            Rate[] rates = response.getRates();
            for (int i = 0; i < rates.length; i++) {
                Rate rate = rates[i];
                Zrate zrate = new Zrate();
                zrate.setModifyuser("19E");
                zrate.setCreateuser("19E");
                zrate.setUsertype("1");
                zrate.setAgentid(13l);
                zrate.setIsenable(1);
                zrate.setZtype("1");
                zrate.setType(1);
                zrate.setCreatetime(new Timestamp(System.currentTimeMillis()));
                zrate.setModifytime(new Timestamp(System.currentTimeMillis()));
                zrate.setOutid(rate.getRate_id());
                zrate.setRatevalue(Float.parseFloat(rate.getBackrate()));
                zrate.setGeneral(Long.parseLong(rate.getRate_type()));
                String tickettype = rate.getTicket_type();
                if ("1".equals(tickettype))
                    tickettype = "2";
                else if ("2".equals(tickettype))
                    tickettype = "1";
                zrate.setTickettype(Integer.parseInt(tickettype));
                zrate.setRemark(rate.getRemark());
                zrate.setWorktime(rate.getWork_time().split("-")[0].replace(":", ""));
                zrate.setAfterworktime(rate.getWork_time().split("-")[1].replace(":", ""));
                zrate.setOnetofiveworktime(rate.getWork_time().split("-")[0].replace(":", ""));
                zrate.setOnetofiveaftertime(rate.getWork_time().split("-")[1].replace(":", ""));
                zrate.setOnetofivewastetime(rate.getCancel_time().replace(":", ""));
                zrate.setWeekendwastetime(rate.getCancel_time().replace(":", ""));
                //					zrate.setSpeed((Integer.parseInt(rate.getOut_time())/60)+"");
                zrate.setSpeed(rate.getOut_time());
                zrate.setAgentcode(rate.getOffice_no());
                zrates.add(zrate);
            }
        }
        else {
            WriteLog.write("getzratebypnr_19EgetBestZratebyPnr_Note", orderid + ":ERROR:" + response.getErrorcode());
        }

        return zrates;
        //        }
        //        catch (AxisFault e) {
        //            e.printStackTrace();
        //            return zrates;
        //        }
        //        catch (RemoteException e) {
        //            e.printStackTrace();
        //            return zrates;
        //        }
    }

    /**
     * 
     * @param id
     * @return
     * @time 2016年5月26日 下午5:11:35
     * @author chendong
     */
    private static String getComCodeByOrderid(long id) {
        String sql = "select c_aircomapnycode from T_segmentinfo where c_orderid=" + id;
        String comCode = "";
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        if (list.size() > 0) {
            Map map = (Map) list.get(0);
            try {
                comCode = (String) map.get("c_aircomapnycode");
            }
            catch (Exception e) {
            }
        }
        return comCode;
    }

    /**
     * 根据航司获取是否需要生成虚拟pnr
     * @param aircomapnycode
     * @time 2016年5月10日 上午10:26:58
     * @author chendong
     * @param integer 
     * @param zrate 
     */
    private String getisVirtualPnr(String aircomapnycode, int ptype, Integer tickettype) {
        String isVirtualPnr = getSysConfigByProcedure("isVirtualPnr");//#是否虚拟pnr如果是1走虚拟pnr的流程;isVirtualPnr=1
        if ("1".equals(isVirtualPnr)) {
            String isVirtualPnrComCode = getSysConfigByProcedure("isVirtualPnrComCode");//#是否虚拟pnr如果是1走虚拟pnr的流程;isVirtualPnr=1
            if (ptype == 1 && aircomapnycode != null && isVirtualPnrComCode.indexOf(aircomapnycode) >= 0
                    && tickettype == 1) {//包含这些航司的话才创建虚拟pnr
                //如果政策是b2p的才生成假编码
            }
            else {
                isVirtualPnr = "0";//
            }

        }
        return isVirtualPnr;
    }

    /**
     * 19e 根据PNR创建订单(NEW)
     * @param order
     * @param sinfo
     * @return
     */
    public static String createOrderbyPnrNew(Orderinfo order, Segmentinfo sinfo) {
        Eaccount eaccount = Server.getInstance().getSystemService().findEaccount(15);
        try {
            OrderServiceImplServiceStub stub = new OrderServiceImplServiceStub();
            stub._getServiceClient().getOptions()
                    .setProperty(org.apache.axis2.Constants.Configuration.DISABLE_SOAP_ACTION, true);

            OrderServiceImplServiceStub.CreateOrderByPnrNewE createOrderByPnr = new OrderServiceImplServiceStub.CreateOrderByPnrNewE();
            OrderServiceImplServiceStub.CreateOrderByPnrNew param = new OrderServiceImplServiceStub.CreateOrderByPnrNew();
            OrderServiceImplServiceStub.OrderByPnrNewRequest param1 = new OrderServiceImplServiceStub.OrderByPnrNewRequest();
            int appcode = 19;
            String Aircomapnycode = getComCodeByOrderid(order.getId());
            String isVirtualPnr = new Yi9eMethod().getisVirtualPnr(Aircomapnycode, 1, 1);
            String pnr_type = "2"; //1- PNR小编导入(目前不支持),2-  2-黑屏导入 3-  3.虚拟PNR导入
            if ("1".equals(isVirtualPnr)) {
                pnr_type = "3";
            }
            String pnr = order.getPnr();
            String rtInfo = "rt" + pnr + "\n" + order.getUserRtInfo().replaceAll("", ""); //pnr的rt信息
            String patInfo = "pat:a\n" + order.getPnrpatinfo().replaceAll("", "");//pnr的pat信息
            param1.setPnr(pnr);
            param1.setPnr_type(pnr_type);
            param1.setPnr_content(rtInfo + patInfo);
            WriteLog.write("CreateOrder19EcreateOrderbyPnrNEW", order.getId() + ":pnr:" + pnr);
            WriteLog.write("CreateOrder19EcreateOrderbyPnrNEW", order.getId() + ":pnr_type:" + pnr_type);
            WriteLog.write("CreateOrder19EcreateOrderbyPnrNEW", order.getId() + ":rtInfo:" + rtInfo);
            WriteLog.write("CreateOrder19EcreateOrderbyPnrNEW", order.getId() + ":patInfo:" + patInfo);
            param1.setUsername(eaccount.getUsername());
            WriteLog.write("CreateOrder19EcreateOrderbyPnrNEW",
                    order.getId() + ":eaccount.getUsername():" + eaccount.getUsername());
            String rate_id = sinfo.getZrate().getOutid();
            param1.setRate_id(rate_id);
            String is_change_pnr = "0";
            param1.setOut_orderid("");
            param1.setDiscount(order.getRatevalue() + "");
            param1.setNotified_url(eaccount.getNourl());
            try {
                WriteLog.write("CreateOrder19EcreateOrderbyPnrNEW", order.getId() + ":" + order.getPnr()
                        + ":-1:param1:" + JSONObject.toJSONString(param1));
            }
            catch (Exception e) {
            }
            WriteLog.write("CreateOrder19EcreateOrderbyPnrNEW",
                    order.getId() + ":" + order.getPnr() + ":0:" + eaccount.getNourl());
            param1.setAppcode(appcode);
            String Sign = "OrderByPnrNewRequest" + order.getPnr() + pnr_type + eaccount.getUsername() + rate_id
                    + order.getRatevalue() + appcode + eaccount.getPwd();
            WriteLog.write("CreateOrder19EcreateOrderbyPnrNEW", order.getId() + ":" + order.getPnr() + ":0.1:" + Sign);
            try {
                Sign = HttpClient.MD5(Sign);
                WriteLog.write("CreateOrder19EcreateOrderbyPnrNEW", order.getId() + ":" + order.getPnr() + ":0.2:"
                        + Sign);
            }
            catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            param1.setSign(Sign);
            param.setArg0(param1);
            createOrderByPnr.setCreateOrderByPnrNew(param);
            OrderServiceImplServiceStub.OrderByPnrNewResponse ResponseNew = stub.createOrderByPnrNew(createOrderByPnr)
                    .getCreateOrderByPnrNewResponse().get_return();
            if ("S".equals(ResponseNew.getCode())) {
                String payurl = ResponseNew.getPayurl_alipay();
                String Office_no = ResponseNew.getRate().getOffice_no() == null ? "" : ResponseNew.getRate()
                        .getOffice_no();
                String ret = "S|" + ResponseNew.getOrder_id() + "|" + payurl + "|" + Office_no + "|"
                        + ResponseNew.getTotal_price();
                WriteLog.write("CreateOrder19EcreateOrderbyPnr", order.getId() + ":" + order.getPnr() + ":SUCCESS:"
                        + ret);
                return ret;
            }
            else {
                WriteLog.write("CreateOrder19EcreateOrderbyPnrNEW", order.getId() + ":" + order.getPnr() + ":ERROR:"
                        + ResponseNew.getErrorcode());
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String createOrderbyPnrusealipay(Orderinfo order, Segmentinfo sinfo) {
        return "";
    }

    public static String autopay(String orderid, String Total_price) {
        Eaccount eaccount = Server.getInstance().getSystemService().findEaccount(15);
        WriteLog.write("19Eautopay", "orderid:" + orderid);
        String result = "-1";
        try {
            OrderServiceImplServiceStub stub = new OrderServiceImplServiceStub();
            stub._getServiceClient().getOptions()
                    .setProperty(org.apache.axis2.Constants.Configuration.DISABLE_SOAP_ACTION, true);
            OrderServiceImplServiceStub.PayOrderE payOrder = new OrderServiceImplServiceStub.PayOrderE();
            OrderServiceImplServiceStub.PayOrder param = new OrderServiceImplServiceStub.PayOrder();
            OrderServiceImplServiceStub.PayOrderRequest PayOrderRequest = new OrderServiceImplServiceStub.PayOrderRequest();
            PayOrderRequest.setAppcode(19);
            PayOrderRequest.setOrder_id(orderid);
            String Sign = "PayOrderRequest" + eaccount.getUsername() + orderid + Total_price + "19" + eaccount.getPwd();
            WriteLog.write("19Eautopay", "0.1:" + Sign);
            try {
                Sign = HttpClient.MD5(Sign);
                WriteLog.write("19Eautopay", "0.2:" + Sign);
            }
            catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            PayOrderRequest.setSign(Sign);
            PayOrderRequest.setTotal_price(Total_price);
            PayOrderRequest.setUsername(eaccount.getUsername());
            param.setArg0(PayOrderRequest);
            payOrder.setPayOrder(param);
            OrderServiceImplServiceStub.PayOrderResponseE response = stub.payOrder(payOrder).getPayOrderResponse()
                    .get_return();
            if ("S".equals(response.getPay_status())) {
                result = "S";
                WriteLog.write("19Eautopay", "S:" + response.getPay_flow_id() + ":" + response.getPay_price() + ":"
                        + response.getPay_status() + ":" + response.getPay_time());
            }
            else {
                WriteLog.write("19Eautopay", "ERROR:" + response.getErrorcode());
            }
        }
        catch (AxisFault e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return result;
    }

    public List<Zrate> getZrateByFlightNumberbyDB(String scity, String ecity, String sdate, String flightnumber,
            String cabin, String url) {
        List<Zrate> zrates = new ArrayList<Zrate>();
        List list = new ArrayList();
        String mDateTime = new SimpleDateFormat("HH:mm").format(Calendar.getInstance().getTime());
        String strSP = "[dbo].[sp_GetZrateByFlight] " + "@chufajichang = N'" + scity + "'," + "@daodajichang = N'"
                + ecity + "'," + "@chufariqi = N'" + sdate + "'," + "@dangqianshijian= N'" + mDateTime + "',"
                + "@hangkonggongsi= N'" + flightnumber.substring(0, 2) + "'," + "@cangwei= N'" + cabin + "',"
                + "@hangbanhao= N'" + flightnumber + "'," + "@ismulity=1,@isgaofan=1";
        try {
            url = getSysconfigString("19Ezrateurl") + "/cn_service/service/";
            HessianProxyFactory factory = new HessianProxyFactory();
            ISystemService servier = (ISystemService) factory.create(ISystemService.class,
                    url + ISystemService.class.getSimpleName());
            list = servier.findMapResultByProcedure(strSP);
            if (list != null && list.size() > 0) {
                for (int i = 0; i < list.size(); i++) {
                    Zrate zrate = new Zrate();
                    Map zrateMap = (Map) list.get(i);
                    if (zrateMap.get("id") != null) {
                        zrate.setId(Integer.valueOf(zrateMap.get("id").toString()));
                    }
                    if (zrateMap.get("afterworktime") != null) {
                        zrate.setAfterworktime(zrateMap.get("afterworktime").toString());
                    }
                    if (zrateMap.get("ratevalue") != null) {
                        zrate.setRatevalue(Float.valueOf(zrateMap.get("ratevalue").toString()));
                    }
                    if (zrateMap.get("remark") != null) {
                        zrate.setRemark(zrateMap.get("remark").toString());
                    }
                    if (zrateMap.get("speed") != null) {
                        zrate.setSpeed(zrateMap.get("speed").toString());
                    }
                    if (zrateMap.get("onetofivewastetime") != null) {
                        zrate.setOnetofivewastetime(zrateMap.get("onetofivewastetime").toString());
                    }
                    if (zrateMap.get("worktime") != null) {
                        zrate.setWorktime(zrateMap.get("worktime").toString());
                    }
                    if (zrateMap.get("outid") != null) {
                        zrate.setOutid(zrateMap.get("outid").toString());
                    }
                    if (zrateMap.get("general") != null) {
                        zrate.setGeneral(Long.valueOf(zrateMap.get("general").toString()));
                    }
                    if (zrateMap.get("agentid") != null) {
                        zrate.setAgentid(Long.valueOf(zrateMap.get("agentid").toString()));
                    }
                    zrates.add(zrate);
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return zrates;
    }

}
