package com.ccservice.b2b2c.policy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.ccservice.b2b2c.base.segmentinfo.Segmentinfo;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.ben.PiaoMengBook;
import com.ccservice.inter.job.WriteLog;

public class PiaomengGetPolicyByVoyage {

	private static final long serialVersionUID = 1212L;
	PiaoMengBook piaoMengBook = new PiaoMengBook();
	String Uid = piaoMengBook.getPiaomenguser();
	String Pwd = piaoMengBook.getPiaomengkey();
	String key = piaoMengBook.getPiaomengkey();
	List<Zrate> list = new ArrayList<Zrate>();

	public Zrate getPlicyByVoyage(Segmentinfo sinfo) {
		String url = "http://pomservice.piaomeng.net:6000/?"
				+ "cmd=SEARCHPOLICY" + "&FromCity=" + sinfo.getStartairport()
				+ "&ToCity=" + sinfo.getEndairport() + "&Date="
				+ sinfo.getDeparttime().toString().substring(0, 10)
				+ "&Flightno=" + sinfo.getFlightnumber() + "&Seat="
				+ sinfo.getCabincode() + "&IsSpecmark=" + "" + "&Agiofee=" + ""
				+ "&OilBuildFee=" + "" + "&IsChild=" + "" + "&Uid=" + Uid
				+ "&Pwd=" + key;
		WriteLog.write("piaomeng", url);
		String str = getDateString(url);
		Zrate zrate = new Zrate();
		zrate.setAgentid(2L);
		try {
			Document document = DocumentHelper.parseText(str);
			Element root = document.getRootElement();
			String status = root.attributeValue("statuscode");// 0:成功 其他失败
			if (status.equals("0")) {
				String count = root.attributeValue("policycount");// 返回数量
				if (root.elements("item").size() > 0) {
					List<Element> listitem = root.elements("item");
					for (int i = 0; i < Integer.parseInt(count); i++) {
						Element ele = listitem.get(i);
						if (ele.attributeValue("officeid") != null) {
							zrate.setAgentcode(ele.attributeValue("officeid"));
						}
						zrate.setRemark(ele.attributeValue("note"));
						zrate.setCabincode(ele.attributeValue("applyclass"));
						zrate.setEnddate(dateToTimestamp(ele
								.attributeValue("totime")));
						zrate.setBegindate(dateToTimestamp(ele
								.attributeValue("fromtime")));
						ele.attributeValue("changerecord");
						String isspecial = ele.attributeValue("isspecmark");// 0：普通
						// 1：特殊
						if (ele.attributeValue("isspecial") != null
								&& ele.attributeValue("isspecial").length() > 0) {
							if (isspecial.equals("0")) {
								zrate.setGeneral(1l);
								zrate.setZtype("1");
							} else {
								zrate.setGeneral(2l);
							}
						}
						// String istype = ele.attributeValue("paytype");
						String policytype = ele.attributeValue("policytype");
						if (policytype.equals("B2B-ET")) {
							zrate.setTickettype(2);// B2B
						} else {
							zrate.setTickettype(1);// BSP
						}
						// if (istype.equals("B2B-ET")) {
						// zrate.setTickettype(2);
						// } else {
						// zrate.setTickettype(1);
						// }
						ele.attributeValue("profit");
						ele.attributeValue("paypoundage");
						zrate.setWorktime(ele.attributeValue("worktime")
								.substring(0, 4));
						zrate.setAfterworktime(ele.attributeValue("worktime")
								.substring(5, 9));
						zrate.setRatevalue(Float.parseFloat(ele
								.attributeValue("rate")));
						zrate.setOutid(ele.attributeValue("id"));
						if (zrate.getAgentcode() != null) {
							return zrate;
						} else {
							continue;
						}
					}
				}
			} else {
				System.out.println("票盟航程匹配失败");
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return zrate;
	}

	public List<Zrate> getZrateByFlightNumber(String scity, String ecity,
			String sdate, String flightnumber, String cabin) {
		List<Zrate> zrates = new ArrayList<Zrate>();
		String url = "http://pomservice.piaomeng.net:6000/?"
				+ "cmd=SEARCHPOLICY" + "&FromCity=" + scity + "&ToCity="
				+ ecity + "&Date=" + sdate + "&Flightno=" + flightnumber
				+ "&Seat=" + cabin + "&IsSpecmark=" + "" + "&Agiofee=" + ""
				+ "&OilBuildFee=" + "" + "&IsChild=" + "" + "&Uid=" + Uid
				+ "&Pwd=" + key;
		WriteLog.write("piaomeng", "getZrateByFlightNumber:"+url);
		String str = getDateString(url);
		Zrate zrate = new Zrate();
		zrate.setAgentid(2L);
		try {
			Document document = DocumentHelper.parseText(str);
			Element root = document.getRootElement();
			String status = root.attributeValue("statuscode");// 0:成功 其他失败
			if (status.equals("0")) {
				String count = root.attributeValue("policycount");// 返回数量
				if (root.elements("item").size() > 0) {
					List<Element> listitem = root.elements("item");
					for (int i = 0; i < Integer.parseInt(count); i++) {
						Element ele = listitem.get(i);
						if (ele.attributeValue("officeid") != null) {
							zrate.setAgentcode(ele.attributeValue("officeid"));
						}
						zrate.setRemark(ele.attributeValue("note"));
						zrate.setCabincode(ele.attributeValue("applyclass"));
						zrate.setEnddate(dateToTimestamp(ele
								.attributeValue("totime")));
						zrate.setBegindate(dateToTimestamp(ele
								.attributeValue("fromtime")));
						ele.attributeValue("changerecord");
						String isspecial = ele.attributeValue("isspecmark");// 0：普通
						// 1：特殊
						if (ele.attributeValue("isspecial") != null
								&& ele.attributeValue("isspecial").length() > 0) {
							if (isspecial.equals("0")) {
								zrate.setGeneral(1l);
								zrate.setZtype("1");
							} else {
								zrate.setGeneral(2l);
							}
						}
						// String istype = ele.attributeValue("paytype");
						String policytype = ele.attributeValue("policytype");
						if (policytype.equals("B2B-ET")) {
							zrate.setTickettype(2);// B2B
						} else {
							zrate.setTickettype(1);// BSP
						}
						// if (istype.equals("B2B-ET")) {
						// zrate.setTickettype(2);
						// } else {
						// zrate.setTickettype(1);
						// }
						ele.attributeValue("profit");
						ele.attributeValue("paypoundage");
						zrate.setWorktime(ele.attributeValue("worktime")
								.substring(0, 4));
						zrate.setAfterworktime(ele.attributeValue("worktime")
								.substring(5, 9));
						zrate.setRatevalue(Float.parseFloat(ele
								.attributeValue("rate")));
						zrate.setOutid(ele.attributeValue("id"));
						if (zrate.getAgentcode() != null) {
							zrates.add(zrate);
						} else {
							continue;
						}
					}
				}
			} else {
				System.out.println("票盟航程匹配失败");
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return zrates;
	}

	/**
	 * 打开url为urltemp的url
	 * 
	 * @param urltemp
	 *            请求的url
	 * @return 请求后响应内容
	 */
	public static String getDateString(String urltemp) {
		URL url;
		try {
			url = new URL(urltemp);
			URLConnection connection = url.openConnection();
			String sCurrentLine;
			String sTotalString;
			sCurrentLine = "";
			sTotalString = "";
			InputStream l_urlStream;
			l_urlStream = connection.getInputStream();
			BufferedReader bufferedReader = new BufferedReader(
					new InputStreamReader(l_urlStream, "UTF-8"));
			while ((sCurrentLine = bufferedReader.readLine()) != null) {
				sTotalString += sCurrentLine + "\r\n";
			}
			return sTotalString;
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}

	public Timestamp dateToTimestamp(String date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat();
		try {

			if (date.length() == 10) {
				dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			} else {
				dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			}
			return (new Timestamp(dateFormat.parse(date).getTime()));

		} catch (Exception e) {
			return null;
		}
	}

	public static void main(String[] args) {
		PiaomengGetPolicyByVoyage piaomengGetPolicy = new PiaomengGetPolicyByVoyage();
		Segmentinfo sinfo = new Segmentinfo();
		sinfo.setStartairport("HET");
		sinfo.setEndairport("SHA");
		Timestamp time = new Timestamp(new Date().getTime());
		time.setDate(2);
		sinfo.setFlightnumber("MU5158");
		sinfo.setDeparttime(time);
		sinfo.setCabincode("Y");

		piaomengGetPolicy.getPlicyByVoyage(sinfo);

	}
}
