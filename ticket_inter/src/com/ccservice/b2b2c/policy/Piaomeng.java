package com.ccservice.b2b2c.policy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.ccservice.b2b2c.base.orderinfo.Orderinfo;
import com.ccservice.b2b2c.base.passenger.Passenger;
import com.ccservice.b2b2c.base.segmentinfo.Segmentinfo;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.ben.PiaoMengBook;
import com.ccservice.inter.job.WriteLog;

/**
 * 
 * @author
 * 
 */
public class Piaomeng extends SupplyMethod {
    private static final long serialVersionUID = 1L;

    Log log = LogFactory.getLog(Piaomeng.class);

    private static final PiaoMengBook piaoMengBook = new PiaoMengBook();

    private static final String Uid = piaoMengBook.getPiaomenguser();

    private static final String Pwd = piaoMengBook.getPiaomengkey();

    private static final String key = piaoMengBook.getPiaomengkey();

    boolean flag;

    /**
     * 
     * base64 解码
     * 
     * @param payurl
     *            base64转码后得url
     * @param
     * @param
     * 
     * @return base64解码后得url
     * @throws IOException
     * @throws
     */
    public String BaseUrl(String payurl) throws IOException {
        System.out.println("payurl==" + payurl);
        byte[] bt = null;
        try {
            sun.misc.BASE64Decoder decoder = new sun.misc.BASE64Decoder();
            bt = decoder.decodeBuffer(payurl);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        String aa = new String(bt);
        return aa;
    }

    /**
     * 
     * 跳转到支付地址
     * 
     * @param OrderNo
     *            外部订单ID
     * @param
     * @param
     * 
     * @return
     * @throws Exception
     */
    public String ToPayUrl(String OrderNo) {
        PiaoMengBook piaoMengBook = new PiaoMengBook();
        String Uid = piaoMengBook.getPiaomenguser();
        String Pwd = piaoMengBook.getPiaomengkey();
        String key = piaoMengBook.getPiaomengkey();
        // String Uid="alhk999";
        // String Pwd="w9812f45";
        String url = "http://pomservice.piaomeng.net:6000/?cmd=SUBMITTOPAY&Uid=" + Uid + "&Pwd=" + Pwd + "&OrderID="
                + OrderNo;
        WriteLog.write("piaomengCToPayURL", url);
        String str = getDateString(url);
        WriteLog.write("piaomengToPayURL_ReturnInfo", str);

        return str;
    }

    /**
     * 票盟创建订单
     * 
     * @param price
     *            票面价
     * @param order
     * @return S|订单号|支付url
     */
    public String CreateOrder(String pnr, Zrate zrate, Orderinfo order) {
        PiaoMengBook piaoMengBook = new PiaoMengBook();
        String Uid = piaoMengBook.getPiaomenguser();
        String Pwd = piaoMengBook.getPiaomengkey();
        String key = piaoMengBook.getPiaomengkey();
        try {
            float CollFee = 0.00f;
            String url = "http://pomservice.piaomeng.net:6000/?cmd=SUBMITORDER&Uid=" + Uid + "&Pwd=" + Pwd
                    + "&CollFee=" + CollFee + "&DeductFee=0" + "&dataFormat=0&PolicyID=" + zrate.getOutid()
                    + "&PnrCode=" + pnr.trim() + "";
            String orderNO = "-1";// 外部订单号
            String payurl = "http://www.alhk999.com/cn_home/air/payresult.jsp";// 支付URL
            String officeid = "";
            if (zrate.getAgentcode() != null) {
                officeid = zrate.getAgentcode();
            }
            String payfee = "";
            WriteLog.write("piaomeng", "CreateOrderURL:" + url);
            String str = getDateString(url);
            WriteLog.write("piaomeng", "CreateOrderinfo" + str);
            System.out.println("str==" + str);
            Document document = DocumentHelper.parseText(str.toString());
            Element root = document.getRootElement();
            System.out.println("--" + root.attributeValue("statuscode") + "---");
            if (root.attributeValue("statuscode").equals("0")) {// 成功
                List<Element> info = root.elements("info");
                orderNO = info.get(0).attributeValue("orderid");
                payurl = info.get(0).attributeValue("payurl");
                if (info.get(0).attributeValue("officeid") != null) {
                    officeid = info.get(0).attributeValue("officeid");
                }
                payfee = info.get(0).attributeValue("payfee");
                if (payfee != null) {
                    order.setTotalticketprice(Float.parseFloat(payfee) - order.getTotalfuelfee()
                            - order.getTotalairportfee());
                }
                System.out.println("外部订单ID==" + orderNO);
                System.out.println("payurl==" + payurl);
                System.out.println("officeid==" + officeid);
                payurl = BaseUrl(payurl);
                // String zrateOutid = info.get(0).attributeValue("policyid");//
                // 政策id
                // String rate = info.get(0).attributeValue("rate");// 返点
                WriteLog.write("piaomeng", "CreateOrderPAYURL:ZrateID:" + order.getPolicyid() + ",orderNO:" + orderNO
                        + ",payurl:" + payurl);
                String ret = "S|" + orderNO + "|" + payurl + "|" + officeid;
                System.out.println("purl==" + payurl);
                return ret;
                // S|订单号|payurl|office号
            }
            System.out.println("--" + root.getText() + "---");
            return orderNO;
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return "-1";
    }

    /**
     * 票盟根据RT信息下单
     * 
     * @param pnr
     * @param zrate
     * @param order
     * @return
     */
    public String CreateOrderByRt(String pnr, Zrate zrate, Orderinfo order) {
        PiaoMengBook piaoMengBook = new PiaoMengBook();
        String Uid = piaoMengBook.getPiaomenguser();
        String Pwd = piaoMengBook.getPiaomengkey();
        String key = piaoMengBook.getPiaomengkey();
        try {
            String bigpnr = "";
            if ("null".equals(order.getBigpnr())) {
                bigpnr = getBigPnr(order.getUserRtInfo(), pnr);
            }
            else {
                bigpnr = order.getBigpnr();
            }
            String encoderPat = URLEncoder.encode(order.getPnrpatinfo() == null ? "" : order.getPnrpatinfo().trim(),
                    "UTF-8");
            String encoderRt = URLEncoder.encode(order.getUserRtInfo() == null ? "" : order.getUserRtInfo().trim(),
                    "UTF-8");
            String url = "http://pomservice.piaomeng.net:6000/?cmd=SUBMITORDERBYRTDATA&Uid=" + Uid + "&Pwd=" + Pwd
                    + "&PolicyID=" + zrate.getOutid() + "&dataFormat=0&PnrCode=" + pnr.trim() + "&BigPNR=" + bigpnr
                    + "&RTData=" + encoderRt + "&PatData=" + encoderPat;
            String orderNO = "-1";// 外部订单号
            String payurl = "";// 支付URL
            String officeid = "";
            if (zrate.getAgentcode() != null) {
                officeid = zrate.getAgentcode();
            }
            String payfee = "";
            WriteLog.write("CreateOrderpiaomeng", "CreateOrderURL:" + url);
            // 打开票盟下单url,返回票盟的返回结果:格式:Statuscode| orderid| policyid| rate|
            // payfee| payurl
            String str = getDateString(url);
            WriteLog.write("CreateOrderpiaomeng", "CreateOrderinfo:" + str);
            Document document = DocumentHelper.parseText(str.toString());
            Element root = document.getRootElement();
            if (root.attributeValue("statuscode").equals("0")) {// 成功
                List<Element> info = root.elements("info");
                orderNO = info.get(0).attributeValue("orderid");
                payurl = info.get(0).attributeValue("payurl");
                if (info.get(0).attributeValue("officeid") != null) {
                    officeid = info.get(0).attributeValue("officeid");
                }
                payfee = info.get(0).attributeValue("payfee");
                // if (payfee != null) {
                // try {
                // String where = "UPDATE T_ORDERINFO SET "
                // + Orderinfo.COL_extorderprice + "=" + payfee
                // + " WHERE " + Orderinfo.COL_pnr + "='" + pnr
                // + "'";
                // WriteLog.write("z支付供应", "piaomengCreateOrder:" + where);
                // Server.getInstance().getSystemService()
                // .findMapResultBySql(where, null);
                // } catch (Exception e) {
                // e.printStackTrace();
                // }
                // }
                System.out.println("外部订单ID==" + orderNO);
                System.out.println("officeid==" + officeid);
                payurl = BaseUrl(payurl);
                //				String zrateOutid = info.get(0).attributeValue("policyid");// 政策id
                //				String rate = info.get(0).attributeValue("rate");// 返点
                String ret = "-1";
                if (payfee != null) {
                    ret = "S|" + orderNO + "|" + payurl + "|" + officeid + "|" + payfee;
                }
                else {
                    ret = "S|" + orderNO + "|" + payurl + "|" + officeid + "|" + payfee;
                }
                WriteLog.write("CreateOrderpiaomeng", "CreateOrderPAYURL:ZrateID:" + order.getPolicyid() + ":" + ret);
                return ret;
                // S|订单号|payurl|office号
            }
            System.out.println("--" + root.getText() + "---");
            return orderNO;
        }
        catch (DocumentException e) {
            e.printStackTrace();

        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return "-1";
    }

    /**
     * 申请退废票
     * @param outOrderid
     * @param ticketNO
     * @return
     */
    public String returnTicket(String outOrderid, String ticketNO) {
        PiaoMengBook piaoMengBook = new PiaoMengBook();
        String Uid = piaoMengBook.getPiaomenguser();
        String Pwd = piaoMengBook.getPiaomengkey();
        String key = piaoMengBook.getPiaomengkey();
        String url = "http://pomservice.piaomeng.net:6000/?cmd=SubmitOrderStatus&Uid=" + Uid + "&Pwd=" + Pwd
                + "&Orderid=" + outOrderid + "&ticketno=" + ticketNO + "&status=20" + "&Dataformat=0";
        String str = getDateString(url);
        WriteLog.write("piaomengReturnTicketURL", url);
        Document document;
        try {
            document = DocumentHelper.parseText(str.toString());

            Element root = document.getRootElement();
            System.out.println("--" + root.attributeValue("statuscode") + "---");
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * RT+PAT结果下单
     * 
     * @return
     */
    // public String createOrderByPat(){
    //		
    // }
    /**
     * 
     * 
     * @param Orderid
     *            外部订单id
     * @param ticketno
     *            申请退票的票号，多个票号用“;”分割
     * @param status
     *            20:申请退票 30:申请废票
     * @param Refoundfee
     *            格式0.00 代理要扣的退废票手续费，此字段扣下的钱，票盟会返还给代理
     * @param comment
     *            备注信息
     * @return
     * @throws Exception
     */
    public String SubmitOrderStatus(String Orderid, String ticketno, String status, String comment, Double Refoundfee) {
        PiaoMengBook piaoMengBook = new PiaoMengBook();
        String Uid = piaoMengBook.getPiaomenguser();
        String Pwd = piaoMengBook.getPiaomengkey();
        String key = piaoMengBook.getPiaomengkey();
        // String Uid="alhk999";
        // String Pwd="38ED87E6-BC08-4EBA-A750-50F085FD524C";
        String Dataformat = "1";
        String url = "http://pomservice.piaomeng.net:6000/?cmd=SubmitOrderStatus&Uid=" + Uid + "&Pwd=" + Pwd
                + "&Orderid=" + Orderid + "&ticketno=" + ticketno + "&status=" + status + "&comment=" + comment
                + "&Refoundfee=" + Refoundfee + "&Dataformat=1";

        WriteLog.write("piaomeng", "SubmitOrderStatusUrl" + url);
        String str = getDateString(url);
        WriteLog.write("piaomeng", "SubmitOrderStatusUrlstr:" + str);

        System.out.println("str==" + str);
        if (str.equals("0")) {
            System.out.println("已经提交退/费票申请,等待票盟审核");
        }
        else {

            if (str.indexOf("|") != -1) {
                String[] strs = str.split("|");

                if (strs[0].equals("0")) {
                    System.out.println("已经提交退/费票申请,等待票盟审核");
                }
                else {

                    System.out.println("提交退/费申请失败");
                }

            }
        }

        return "";
    }

    /**
     * 根据航程匹配最优政策 
     * 
     * @param sinfo
     * @return
     */
    public static List<Zrate> getPlicyByVoyage(List<Segmentinfo> listSinfo, List<Passenger> listPassenger) {
        Segmentinfo sinfo = listSinfo.get(0);
        WriteLog.write("ZrateTime", "piaomeng:" + new SimpleDateFormat("HH:mm:ss.SSS").format(new Date()));
        PiaoMengBook piaoMengBook = new PiaoMengBook();
        String Uid = piaoMengBook.getPiaomenguser();
        String Pwd = piaoMengBook.getPiaomengkey();
        String key = piaoMengBook.getPiaomengkey();
        List<Zrate> zrates = new ArrayList<Zrate>();
        String ischild = getIschild(listPassenger);
        String url = "http://pomservice.piaomeng.net:6000/?" + "cmd=SEARCHPOLICY" + "&FromCity="
                + sinfo.getStartairport() + "&ToCity=" + sinfo.getEndairport() + "&Date="
                + sinfo.getDeparttime().toString().substring(0, 10) + "&Flightno=" + sinfo.getFlightnumber() + "&Seat="
                + sinfo.getCabincode() + "&IsSpecmark=" + "&Agiofee=" + "" + "&OilBuildFee=" + "" + "&IsChild="
                + ischild + "&Uid=" + Uid + "&Pwd=" + key;
        if (listSinfo.size() > 1) {
            Segmentinfo sinfo2 = listSinfo.get(1);
            String RouteType = "1";//1：往返2：联程
            if (sinfo2.getEndairport().equals(sinfo.getStartairport())) {
            }
            else {
                RouteType = "2";
            }
            url += "&RouteType=" + RouteType + "&FromCity2=" + sinfo2.getStartairport() + "&ToCity2="
                    + sinfo2.getEndairport() + "&Date2=" + sinfo2.getDeparttime().toString().substring(0, 10)
                    + "&Flightno2=" + sinfo2.getFlightnumber() + "&Seat2=" + sinfo2.getCabincode();
        }
        try {
            long templ = new Random().nextInt(10000000);
            WriteLog.write("getzratebypnr_piaomeng", "GetBestZrateURL:" + templ + ":" + url);
            String str = getDateString(url);
            if (str.isEmpty() || str.length() < 10) {
                str = getDateString(url);
                WriteLog.write("getzratebypnr_piaomeng", "GetBestZrateURL:" + templ + ":空了一次:" + str);
            }
            WriteLog.write("getzratebypnr_piaomeng", "BestZrate_Result:" + templ + ":" + str);
            Document document = DocumentHelper.parseText(str);
            Element root = document.getRootElement();
            String status = root.attributeValue("statuscode");// 0:成功 其他失败
            if (status.equals("0")) {
                String count = root.attributeValue("policycount");// 返回数量
                if (root.elements("item").size() > 0) {
                    List<Element> listitem = root.elements("item");
                    for (int i = 0; i < Integer.parseInt(count); i++) {
                        Zrate zrate = new Zrate();
                        zrate.setAgentid(2L);
                        Element ele = listitem.get(i);
                        if (ele.attributeValue("officeid") != null) {
                            zrate.setAgentcode(ele.attributeValue("officeid"));
                        }
                        zrate.setRemark(ele.attributeValue("note"));
                        zrate.setCabincode(ele.attributeValue("applyclass"));
                        zrate.setEnddate(dateToTimestamp(ele.attributeValue("totime")));
                        zrate.setBegindate(dateToTimestamp(ele.attributeValue("fromtime")));
                        ele.attributeValue("changerecord");

                        String isspecial = ele.attributeValue("isspecmark");// 0：普通
                        if (isspecial.equals("1")) {
                            zrate.setGeneral(2l);
                            zrate.setZtype("2");
                        }
                        else {
                            zrate.setGeneral(1l);
                            zrate.setZtype("1");
                        }
                        zrate.setSpeed("5-10分钟");
                        // String istype = ele.attributeValue("paytype");
                        String policytype = ele.attributeValue("policytype");
                        if (policytype.equals("B2B-ET")) {
                            zrate.setTickettype(2);// B2B
                        }
                        else {
                            zrate.setTickettype(1);// BSP
                        }
                        // if (istype.equals("B2B-ET")) {
                        // zrate.setTickettype(2);
                        // } else {
                        // zrate.setTickettype(1);
                        // }
                        ele.attributeValue("profit");
                        ele.attributeValue("paypoundage");
                        ele.attributeValue("RefundWorkTimeTo");
                        zrate.setWorktime(ele.attributeValue("worktime").split("[-]")[0]);
                        zrate.setAfterworktime(ele.attributeValue("worktime").split("[-]")[1]);
                        zrate.setRatevalue(Float.parseFloat(ele.attributeValue("rate")));
                        zrate.setOutid(ele.attributeValue("id"));
                        if (ele.attributeValue("RefundWorkTimeTo") != null) {
                            zrate.setAfterworktime(ele.attributeValue("RefundWorkTimeTo"));
                            zrate.setOnetofivewastetime(ele.attributeValue("RefundWorkTimeTo"));
                            zrate.setWeekendwastetime(ele.attributeValue("RefundWorkTimeTo"));
                        }

                        if (zrate.getAgentcode() != null && zrate.getRatevalue() > 0) {
                            zrates.add(zrate);
                        }
                        else {
                            continue;
                        }
                    }
                }
            }
            else {
                System.out.println("票盟航程匹配失败");
            }
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }

        return zrates;
    }

    /**
     * 默认成人 3:团队4：儿童

     * @param listPassenger
     * @return
     */
    private static String getIschild(List<Passenger> listPassenger) {
        try {
            if (listPassenger.get(0).getPtype() == 2) {
                return "4";
            }
        }
        catch (Exception e) {
        }
        return "";
    }

    /**
     * 根据航程匹配最优政策 往返联程
     * 
     * @param sinfo
     * @return
     */
    public static List<Zrate> getPlicyByVoyage11(List<Segmentinfo> sinfos) {
        WriteLog.write("ZrateTime", "piaomeng:" + new SimpleDateFormat("HH:mm:ss.SSS").format(new Date()));
        PiaoMengBook piaoMengBook = new PiaoMengBook();
        String Uid = piaoMengBook.getPiaomenguser();
        String Pwd = piaoMengBook.getPiaomengkey();
        String key = piaoMengBook.getPiaomengkey();
        List<Zrate> zrates = new ArrayList<Zrate>();
        // String url = "http://pomservice.piaomeng.net:6000/?"
        // + "cmd=SEARCHPOLICY" + "&FromCity=" + sinfo.getStartairport()
        // + "&ToCity=" + sinfo.getEndairport() + "&Date="
        // + sinfo.getDeparttime().toString().substring(0, 10)
        // + "&Flightno=" + sinfo.getFlightnumber() + "&Seat="
        // + sinfo.getCabincode() + "&IsSpecmark=" + "&Agiofee=" + ""
        // + "&OilBuildFee=" + "" + "&IsChild=" + "" + "&Uid=" + Uid
        // + "&Pwd=" + Pwd;
        // try {
        // Random random = new Random(1000);
        // long templ = random.nextLong();
        // WriteLog.write("piaomeng", "GetBestZrateURL:" + templ + ":" + url);
        // String str = getDateString(url);
        // if (str.isEmpty() || str.length() < 10) {
        // str = getDateString(url);
        // WriteLog.write("piaomeng", "GetBestZrateURL:" + templ
        // + ":空了一次:" + str);
        // }
        // WriteLog.write("piaomeng", "BestZrate_Result:" + templ + ":" + str);
        // Document document = DocumentHelper.parseText(str);
        // Element root = document.getRootElement();
        // String status = root.attributeValue("statuscode");// 0:成功 其他失败
        // if (status.equals("0")) {
        // String count = root.attributeValue("policycount");// 返回数量
        // if (root.elements("item").size() > 0) {
        // List<Element> listitem = root.elements("item");
        // for (int i = 0; i < Integer.parseInt(count); i++) {
        // Zrate zrate = new Zrate();
        // zrate.setAgentid(2L);
        // Element ele = listitem.get(i);
        // if (ele.attributeValue("officeid") != null) {
        // zrate.setAgentcode(ele.attributeValue("officeid"));
        // }
        // zrate.setRemark(ele.attributeValue("note"));
        // zrate.setCabincode(ele.attributeValue("applyclass"));
        // zrate.setEnddate(dateToTimestamp(ele
        // .attributeValue("totime")));
        // zrate.setBegindate(dateToTimestamp(ele
        // .attributeValue("fromtime")));
        // ele.attributeValue("changerecord");
        //
        // String isspecial = ele.attributeValue("isspecmark");// 0：普通
        // if (isspecial.equals("1")) {
        // zrate.setGeneral(2l);
        // zrate.setZtype("2");
        // } else {
        // zrate.setGeneral(1l);
        // zrate.setZtype("1");
        // }
        // zrate.setSpeed("10");
        // // String istype = ele.attributeValue("paytype");
        // String policytype = ele.attributeValue("policytype");
        // if (policytype.equals("B2B-ET")) {
        // zrate.setTickettype(2);// B2B
        // } else {
        // zrate.setTickettype(1);// BSP
        // }
        // // if (istype.equals("B2B-ET")) {
        // // zrate.setTickettype(2);
        // // } else {
        // // zrate.setTickettype(1);
        // // }
        // ele.attributeValue("profit");
        // ele.attributeValue("paypoundage");
        // ele.attributeValue("RefundWorkTimeTo");
        // zrate.setWorktime(ele.attributeValue("worktime").split(
        // "[-]")[0]);
        // zrate.setAfterworktime(ele.attributeValue("worktime")
        // .split("[-]")[1]);
        // zrate.setRatevalue(Float.parseFloat(ele
        // .attributeValue("rate")));
        // zrate.setOutid(ele.attributeValue("id"));
        // if (ele.attributeValue("RefundWorkTimeTo") != null) {
        // zrate.setAfterworktime(ele
        // .attributeValue("RefundWorkTimeTo"));
        // zrate.setOnetofivewastetime(ele
        // .attributeValue("RefundWorkTimeTo"));
        // zrate.setWeekendwastetime(ele
        // .attributeValue("RefundWorkTimeTo"));
        // }
        //
        // if (zrate.getAgentcode() != null
        // && zrate.getRatevalue() > 0) {
        // zrates.add(zrate);
        // } else {
        // continue;
        // }
        // }
        // }
        // } else {
        // System.out.println("票盟航程匹配失败");
        // }
        // } catch (DocumentException e) {
        // e.printStackTrace();
        // }

        return zrates;
    }

    /**
     * 票盟根据订单号获得价格
     * 
     * @param pnr
     * @param zrate
     * @param order
     * @return
     */
    public void getOrderPrice(String orderId) {
        PiaoMengBook piaoMengBook = new PiaoMengBook();
        String Uid = piaoMengBook.getPiaomenguser();
        String Pwd = piaoMengBook.getPiaomengkey();
        String key = piaoMengBook.getPiaomengkey();
        try {
            String url = "http://pomservice.piaomeng.net:6000/?cmd=SEARCHORDERSTATUS&Uid=" + Uid + "&Pwd=" + Pwd
                    + "&orderid=" + orderId;
            WriteLog.write("piaomeng", "查询订单状态:0:" + url);
            String str = getDateString(url);
            WriteLog.write("piaomeng", "查询订单状态:1:" + str);
            Document document = DocumentHelper.parseText(str.toString());
            Element root = document.getRootElement();
            if (root.attributeValue("statuscode").equals("0")) {// 成功
            }
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    /**
     * 票盟根据订单号获得票号
     * 
     * @param pnr
     * @param zrate
     * @param order
     * @return
     */
    public static String getTicketNobyOrderid(String orderId) {
        String result = "-1";
        String ticktNO = "";
        String passname = "";
        try {
            String url = "http://pomservice.piaomeng.net:6000/?cmd=SEARCHORDERSTATUS&Uid=" + Uid + "&Pwd=" + Pwd
                    + "&orderid=" + orderId;
            WriteLog.write("piaomeng", "查询订单状态:0:" + url);
            String str = getDateString(url);
            WriteLog.write("piaomeng", "查询订单状态:1:" + str);
            Document document = DocumentHelper.parseText(str.toString());
            Element root = document.getRootElement();
            if (root.attributeValue("statuscode").equals("0")
                    && (root.attributeValue("status").equals("13") || root.attributeValue("status").equals("14"))) {// 成功
                List<Element> list = root.elements("passinfo");
                for (Element passinfo : list) {
                    ticktNO += passinfo.attributeValue("ticketno") + "|";
                    passname += passinfo.attributeValue("passname") + "|";
                }
            }
            result = passname + "^" + ticktNO;
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String payOut(Orderinfo order) {
        String result = "-1";
        int tInt = new Random().nextInt(10000);
        PiaoMengBook piaoMengBook = new PiaoMengBook();
        String Uid = piaoMengBook.getPiaomenguser();
        String key = piaoMengBook.getPiaomengkey();
        String url = "http://pomservice.piaomeng.net:6000/?" + "cmd=SUBMITAUTOPAY" + "&OrderID="
                + order.getExtorderid() + "&dataFormat=0&Uid=" + Uid + "&Pwd=" + key;
        WriteLog.write("自动代扣", tInt + ":piaomeng:0:" + url);
        String str = getDateString(url);
        WriteLog.write("自动代扣", tInt + ":piaomeng:1:" + str);
        try {
            Document document = DocumentHelper.parseText(str);
            Element root = document.getRootElement();
            String status = root.attributeValue("statuscode");// 0:成功 其他失败
            if (status.equals("0")) {
                result = "S";
            }
            else {
                System.out.println("票盟自动支付失败OrderId:" + order.getId() + ":代码" + status);
            }
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Timestamp dateToTimestamp(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        try {
            if (date.length() == 10) {
                dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            }
            else {
                dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            }
            return (new Timestamp(dateFormat.parse(date).getTime()));

        }
        catch (Exception e) {
            return null;
        }
    }

    /**
     * 打开url为urltemp的url
     * 
     * @param urltemp
     *            请求的url
     * @return 请求后响应内容
     */
    public static String getDateString(String urltemp) {

        URL url;
        try {
            url = new URL(urltemp);
            URLConnection connection = url.openConnection();
            String sCurrentLine;
            String sTotalString;
            sCurrentLine = "";
            sTotalString = "";
            InputStream l_urlStream;
            l_urlStream = connection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(l_urlStream, "UTF-8"));

            while ((sCurrentLine = bufferedReader.readLine()) != null) {
                sTotalString += sCurrentLine + "\r\n";
            }
            return sTotalString;
        }
        catch (MalformedURLException e1) {
            e1.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static byte[] httpget(String url) {
        try {

            URL Url = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) Url.openConnection();
            conn.setDoInput(true);
            conn.connect();
            InputStream in = conn.getInputStream();
            byte[] buf = new byte[1024];
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            int len = 0;
            int size = 0;
            while ((len = in.read(buf)) > 0) {
                bout.write(buf, 0, len);
                size += len;
            }
            // System.out.println(new String(content,0,size));
            in.close();
            conn.disconnect();
            return bout.toByteArray();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String sendPostRequest(String xml, String url) throws Exception {
        URL url1 = new URL(url);
        URLConnection con = url1.openConnection();
        // con.set
        con.setUseCaches(false);
        con.setDoOutput(true);// 开启输入输出
        con.setDoInput(true);
        con.setRequestProperty("content-type", "text/html");
        BufferedWriter out = new BufferedWriter(new OutputStreamWriter(con.getOutputStream(), "UTF-8"));
        out.write(xml);
        out.flush();
        out.close();
        StringBuffer sendBack = new StringBuffer();
        // 49. try {
        // 50. InputStream is = con.getInputStream();
        // 51. InputStreamReader isr = new InputStreamReader(is,"UTF-8");
        // 52. BufferedReader br = new BufferedReader(isr);
        // 53. String s = "";
        // 54. while ((s = br.readLine()) != null) {
        // 55. sendBack.append(s);
        // 56. }
        // 57. } catch (Exception e) {
        // 58. System.out.println(e.toString());
        // 59. }
        // 60.
        return sendBack.toString();
    }

    public String applyRefund(Orderinfo order, List<Passenger> passengers) {
        PiaoMengBook piaoMengBook = new PiaoMengBook();
        String Uid = piaoMengBook.getPiaomenguser();
        String Pwd = piaoMengBook.getPiaomengkey();
        String key = piaoMengBook.getPiaomengkey();
        log.error("票盟申请退票");
        String result = "申请失败";
        // String
        // http://ip/?cmd=SubmitOrderStatus&uid=渠道用户账号&pwd&orderid=定单号&ticketno=申请退票的票号，多个票号用“;”分割&status=订单状态&comment=备注信息
        String urlstr = "http://pomservice.piaomeng.net:6000/";
        String ticketno = "";
        int status = order.getOrderstatus() == 4 ? 20 : 21;
        for (Passenger passenger : passengers) {
            if (ticketno.length() > 0) {
                ticketno += ";" + passenger.getTicketnum();
            }
            else {
                ticketno += passenger.getTicketnum();

            }
        }
        try {
            URL url = new URL(urlstr);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream(), "gb2312");
            String paream = "cmd=SubmitOrderStatus&Orderid=" + order.getExtorderid() + "&ticketno=" + ticketno
                    + "&status=" + status;
            paream += "&Refoundfee=0.00&Dataformat=0&Uid=" + Uid + "&Pwd" + Pwd;
            out.write(paream);
            out.flush();
            out.close();
            InputStream strem = connection.getInputStream();
            SAXReader reader = new SAXReader();
            Document document = reader.read(strem);
            Element root = document.getRootElement();
            result = root.getTextTrim();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 根据航班获取政策信息 单程
     * 
     * @param scity
     * @param ecity
     * @param sdate
     * @param flightnumber
     * @param cabin
     * @return
     */
    public static List<Zrate> getZrateByFlightNumber(String scity, String ecity, String sdate, String flightnumber,
            String cabin) {
        List<Zrate> zrates = new ArrayList<Zrate>();
        String url = "http://pomservice.piaomeng.net:6000?cmd=SEARCHPOLICY" + "&FromCity=" + scity + "&ToCity=" + ecity
                + "&Date=" + sdate + "&Flightno=" + flightnumber + "&Seat=" + cabin
                + "&IsSpecmark=&Agiofee=&OilBuildFee=&IsChild=&Uid=" + Uid + "&Pwd=" + key;
        try {
            int templ = getRandomInt();
            String str = getDateString(url);
            if (str.isEmpty() || str.length() < 10) {
                str = getDateString(url);
            }
            Document document = DocumentHelper.parseText(str);
            Element root = document.getRootElement();
            String status = root.attributeValue("statuscode");// 0:成功 其他失败
            if (status.equals("0")) {
                String count = root.attributeValue("policycount");// 返回数量
                if (root.elements("item").size() > 0) {
                    List<Element> listitem = root.elements("item");
                    for (int i = 0; i < Integer.parseInt(count); i++) {
                        Zrate zrate = new Zrate();
                        zrate.setAgentid(2L);
                        Element ele = listitem.get(i);
                        if (ele.attributeValue("officeid") != null) {
                            zrate.setAgentcode(ele.attributeValue("officeid"));
                        }
                        zrate.setRemark(ele.attributeValue("note"));
                        zrate.setCabincode(ele.attributeValue("applyclass"));
                        zrate.setEnddate(dateToTimestamp(ele.attributeValue("totime")));
                        zrate.setBegindate(dateToTimestamp(ele.attributeValue("fromtime")));
                        ele.attributeValue("changerecord");

                        String isspecial = ele.attributeValue("isspecmark");// 0：普通
                        if (isspecial.equals("1")) {
                            zrate.setGeneral(2l);
                            zrate.setZtype("2");
                        }
                        else {
                            zrate.setGeneral(1l);
                            zrate.setZtype("1");
                        }
                        // String istype = ele.attributeValue("paytype");
                        String policytype = ele.attributeValue("policytype");
                        if (policytype.equals("B2B-ET")) {
                            zrate.setTickettype(2);// B2B
                        }
                        else {
                            zrate.setTickettype(1);// BSP
                        }
                        // if (istype.equals("B2B-ET")) {
                        // zrate.setTickettype(2);
                        // } else {
                        // zrate.setTickettype(1);
                        // }
                        ele.attributeValue("profit");
                        ele.attributeValue("paypoundage");
                        ele.attributeValue("RefundWorkTimeTo");
                        zrate.setWorktime(ele.attributeValue("worktime").split("[-]")[0]);
                        zrate.setAfterworktime(ele.attributeValue("worktime").split("[-]")[1]);
                        zrate.setRatevalue(Float.parseFloat(ele.attributeValue("rate")));
                        zrate.setOutid(ele.attributeValue("id"));
                        if (ele.attributeValue("RefundWorkTimeTo") != null) {
                            zrate.setAfterworktime(ele.attributeValue("RefundWorkTimeTo"));
                            zrate.setOnetofivewastetime(ele.attributeValue("RefundWorkTimeTo"));
                            zrate.setWeekendwastetime(ele.attributeValue("RefundWorkTimeTo"));
                        }

                        if (zrate.getAgentcode() != null && zrate.getRatevalue() > 0) {
                            zrates.add(zrate);
                        }
                        else {
                            continue;
                        }
                    }
                }
            }
            else {
                WriteLog.write("piaomeng", "BestZrate_Result:" + templ + ":" + str);
                System.out.println("票盟航程匹配失败");
            }
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }

        return zrates;

    }

    /**
     * 根据航班获取政策信息 往返联程
     * 
     * @param scity
     * @param ecity
     * @param sdate
     * @param flightnumber
     * @param cabin
     * @return
     */
    public static List<Zrate> getZrateByFlightNumber1(String scity, String ecity, String sdate, String flightnumber,
            String cabin) {
        List<Zrate> zrates = new ArrayList<Zrate>();
        String scity1 = scity.split(",")[0];
        String scity2 = scity.split(",")[1];
        String ecity1 = ecity.split(",")[0];
        String ecity2 = ecity.split(",")[1];
        String sdate1 = sdate.split(",")[0];
        String sdate2 = sdate.split(",")[1];
        String flightnumber1 = flightnumber.split(",")[0];
        String flightnumber2 = flightnumber.split(",")[1];
        String cabin1 = cabin.split(",")[0];
        String cabin2 = cabin.split(",")[1];
        String routeType = "1";
        if (!scity1.equals(ecity2)) {
            routeType = "2";
        }

        String url = "http://pomservice.piaomeng.net:6000/?" + "cmd=SEARCHPOLICY" + "&FromCity=" + scity1 + "&ToCity="
                + ecity1 + "&Date=" + sdate1 + "&Flightno=" + flightnumber1 + "&Seat=" + cabin1 + "&IsSpecmark="
                + "&Agiofee=&OilBuildFee=&RouteType=" + routeType + "&IsSpecmark=&IsChild=&Uid=" + Uid + "&Pwd=" + key
                + "&FromCity2=" + scity2 + "&ToCity2=" + ecity2 + "&Date2=" + sdate2 + "&Flightno2=" + flightnumber2
                + "&Seat2=" + cabin2;
        try {
            int templ = getRandomInt();
            WriteLog.write("piaomeng", "GetBestZrateURL:" + templ + ":" + url);
            String str = getDateString(url);
            if (str.isEmpty() || str.length() < 10) {
                str = getDateString(url);
                WriteLog.write("piaomeng", "GetBestZrateURL:" + templ + ":空了一次:" + str);
            }
            WriteLog.write("piaomeng", "BestZrate_Result:" + templ + ":" + str);
            Document document = DocumentHelper.parseText(str);
            Element root = document.getRootElement();
            String status = root.attributeValue("statuscode");// 0:成功 其他失败
            if (status.equals("0")) {
                String count = root.attributeValue("policycount");// 返回数量
                if (root.elements("item").size() > 0) {
                    List<Element> listitem = root.elements("item");
                    for (int i = 0; i < Integer.parseInt(count); i++) {
                        Zrate zrate = new Zrate();
                        zrate.setAgentid(2L);
                        Element ele = listitem.get(i);
                        if (ele.attributeValue("officeid") != null) {
                            zrate.setAgentcode(ele.attributeValue("officeid"));
                        }
                        zrate.setRemark(ele.attributeValue("note"));
                        zrate.setCabincode(ele.attributeValue("applyclass"));
                        zrate.setEnddate(dateToTimestamp(ele.attributeValue("totime")));
                        zrate.setBegindate(dateToTimestamp(ele.attributeValue("fromtime")));
                        ele.attributeValue("changerecord");

                        String isspecial = ele.attributeValue("isspecmark");// 0：普通
                        if (isspecial.equals("1")) {
                            zrate.setGeneral(2l);
                            zrate.setZtype("2");
                        }
                        else {
                            zrate.setGeneral(1l);
                            zrate.setZtype("1");
                        }
                        zrate.setSpeed("10");
                        // String istype = ele.attributeValue("paytype");
                        String policytype = ele.attributeValue("policytype");
                        if (policytype.equals("B2B-ET")) {
                            zrate.setTickettype(2);// B2B
                        }
                        else {
                            zrate.setTickettype(1);// BSP
                        }
                        // if (istype.equals("B2B-ET")) {
                        // zrate.setTickettype(2);
                        // } else {
                        // zrate.setTickettype(1);
                        // }
                        ele.attributeValue("profit");
                        ele.attributeValue("paypoundage");
                        ele.attributeValue("RefundWorkTimeTo");
                        zrate.setWorktime(ele.attributeValue("worktime").split("[-]")[0]);
                        zrate.setAfterworktime(ele.attributeValue("worktime").split("[-]")[1]);
                        zrate.setRatevalue(Float.parseFloat(ele.attributeValue("rate")));
                        zrate.setOutid(ele.attributeValue("id"));
                        if (ele.attributeValue("RefundWorkTimeTo") != null) {
                            zrate.setAfterworktime(ele.attributeValue("RefundWorkTimeTo"));
                            zrate.setOnetofivewastetime(ele.attributeValue("RefundWorkTimeTo"));
                            zrate.setWeekendwastetime(ele.attributeValue("RefundWorkTimeTo"));
                        }

                        if (zrate.getAgentcode() != null && zrate.getRatevalue() > 0) {
                            zrates.add(zrate);
                        }
                        else {
                            continue;
                        }
                    }
                }
            }
            else {
                System.out.println("票盟航程匹配失败");
            }
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }
        return zrates;
    }

}
