package com.ccservice.b2b2c.policy.Bartour;

import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.axis2.AxisFault;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.tempuri.FlightPaymentStub;

import com.baitour.www.BaitourServiceStub;
import com.baitour.www.BaitourServiceStub.DetailCreateOrder;
import com.baitour.www.BaitourServiceStub.DetailCreateOrderResponse;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.base.orderinfo.Orderinfo;
import com.ccservice.b2b2c.base.passenger.Passenger;
import com.ccservice.b2b2c.base.segmentinfo.Segmentinfo;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.util.TimeUtil;

/**
 * 百拓的工具类
 * 
 * @time 2016年9月12日 下午3:12:26
 * @author chendong
 */
public class BartourMethod {
    /**
     * 
     * @param string
     * @return
     * @time 2015年9月24日 下午12:01:21
     * @author chendong
     */
    public static BartourMethod getInstance() {
        String agentCode = "B2B_073104";
        String agentUserName = "hthy";
        String agentPwd = "hthy";
        BartourMethod bartourMethod = new BartourMethod(agentCode, agentUserName, agentPwd);
        return bartourMethod;
    }

    String AgentCode;//用户编号：

    String AgentUserName;//接口用户名：

    String AgentPwd;//接口密码：

    public BartourMethod(String agentCode, String agentUserName, String agentPwd) {
        super();
        AgentCode = agentCode;
        AgentUserName = agentUserName;
        AgentPwd = agentPwd;
    }

    /**
     * 2.2.1机票生成订单
     * 
     * @time 2016年9月14日 下午12:57:18
     * @author chendong
     */
    public String DetailCreateOrderMethod(Orderinfo order, Segmentinfo sinfo, List<Passenger> listPassenger) {
        String str = "-1";

        BaitourServiceStub stub;
        try {
            stub = new BaitourServiceStub();
            DetailCreateOrder detailCreateOrder2 = new DetailCreateOrder();
            StringBuffer param = new StringBuffer("<ORDER_CREATE_RQ  AgentCode='" + this.AgentCode
                    + "'  AgentUserName='" + this.AgentUserName + "'  AgentPwd='" + this.AgentPwd + "' >");
            String pnr = order.getPnr();
            String bigpnr = order.getBigpnr();

            String PolicyId = "a8bd8bcb-f2ba-4f79-b3fe-0c3cb4b7972d";// 政策ID
            String Outid = sinfo.getZrate().getOutid();//这个id是供应商真正的id,
            PolicyId = Outid;
            String InternationalTicket = "0"; //国际国内标示  国内或国际 0：国内，1：国际 是
            String TripTypeCode = "1";//   行程类型    1 单程 2往返    是
            String OrderSrc = "1";//   订单来源    订单来源 1 国内普通  2 国内特价 3 国内低舱位 4 国际普通  是
            String InsuranceType = "";//   表示没有保险  0   是
            String LocalOrderID = order.getOrdernumber();//  本地机票订单号 123456789，没有传空  否

            param.append("<OrderInfo PolicyId=\"" + PolicyId + "\" InternationalTicket=\"" + InternationalTicket
                    + "\" TripTypeCode=\"" + TripTypeCode + "\" OrderSrc=\"" + OrderSrc
                    + "\" InsuranceType=\"0\" LocalOrderID=\"" + LocalOrderID + "\">");

            String DepartureDatetime = getDepartureDateTime(sinfo.getDeparttime());//  出发日期    2008-11-13T08:25:00 是
            String ArrivalDatetime = getDepartureDateTime(sinfo.getArrivaltime());// 到达日期    2008-11-13T08:25:00 是
            String FlightNumber = sinfo.getFlightnumber();//   航班号 CZ6132  是
            String PlaneStyle = "";//  机型  737 是
            String DepartureAirport = sinfo.getStartairport();//    出发城市    PEK 是
            String ArrivalAirport = sinfo.getEndairport();//      到达城市    PVG 是
            String Code = sinfo.getCabincode();// 舱位  L   是
            String Price = sinfo.getParvalue().toString();//  票面价 790 是
            String AgentPrice = Price;//  成人结算价   750 是
            String Tax = sinfo.getAirportfee().toString();// 机建  50  是
            String YQTax = sinfo.getFuelfee().toString();//   燃油  0   是
            String Insurance = "0";//      0   是

            param.append("<FlightSegments>");
            param.append("<FlightInfo DepartureDatetime=\"" + DepartureDatetime + "\" ArrivalDatetime=\""
                    + ArrivalDatetime + "\" FlightNumber=\"" + FlightNumber + "\" PlaneStyle=\"\">"
                    + "<DepartureAirport LocationCode=\"" + DepartureAirport + "\"/><ArrivalAirport LocationCode=\""
                    + ArrivalAirport + "\"/>");
            param.append("<Cabin Code=\"" + Code + "\" Price=\"" + Price + "\" AgentPrice=\"" + AgentPrice
                    + "\" Tax=\"" + Tax + "\" YQTax=\"" + YQTax + "\" SubCode=\"\">");
            String travelerInfo = "";
            for (int i = 0; i < listPassenger.size(); i++) {
                Passenger passenger = listPassenger.get(i);
                String name = passenger.getName();
                String personType = passenger.getPtype().toString();//  乘机人类型   1：成人 2：儿童    是
                String CertifyID = passenger.getIdtype() == 1 ? "0" : passenger.getIdtype() == 3 ? "3" : "4";//【自己实体类类型 1 身份证   3护照】 证件类型    0：身份证 1：士兵证  2：军官证 3：护照 4：其它 5：台胞证   是
                String IdNumber = passenger.getIdnumber();//  证件号码    GC4563  否
                String NationalityId = "";//   国籍  默认45为中国 是
                String Gender = "0";//   性别  0：男 1：女 是
                String Birthday = "1999-01-01";//  生日  默认1999-01-01    是
                param.append("<RPH personName=\"" + name + "\" PNR=\"" + pnr + "\" Insurance=\"" + Insurance
                        + "\" BigPNR=\"" + bigpnr + "\"></RPH>");
                travelerInfo += "<personInfo><personName>" + name + "</personName><personType>" + personType
                        + "</personType>" + "<CertifyID>" + CertifyID + "</CertifyID><IdNumber>" + IdNumber
                        + "</IdNumber><NationalityId>" + NationalityId + "</NationalityId>" + "<Gender>" + Gender
                        + "</Gender><Birthday>" + Birthday + "</Birthday></personInfo>";
                //                    + "<RPH personName=\"李四\" PNR=\"ZXETR1\" Insurance=\"1\" BigPNR =\"XETEWE\"></RPH>");
            }
            param.append("</Cabin></FlightInfo></FlightSegments>");
            param.append("<TravelerInfo>");
            param.append(travelerInfo);
            //            param.append("<TravelerInfo><personInfo><personName>张三</personName><personType>1</personType>"
            //                    + "<CertifyID>4</CertifyID><IdNumber>1111111</IdNumber<NationalityId>45</NationalityId>"
            //                    + "<Gender>1</Gender><Birthday>1900-01-01</Birthday></personInfo><personInfo>"
            //                    + "<personName>李四</personName><personType>1</personType><CertifyID>4</CertifyID>"
            //                    + "<IdNumber>22222</IdNumber><NationalityId>45</NationalityId><Gender>1</Gender>"
            //                    + "<Birthday>1900-01-01</Birthday></personInfo></TravelerInfo>");
            param.append("</TravelerInfo>");
            String PnrAuthOffice = "";//    PNR授权Office Pek000，进行票号反授权  否
            String TicketAuthOffice = "";//      票号授权Office  Pek000，进行票号反授权  否
            String AgentContactMob = "15538956152";//   OSICTCT客人联系电话   13500000000，通知航变信息等 是
            String IsBookPNR = "";//     平台带生成编码     0：默认不使用平台生编功能。 1：代理需要平台生编服务    否
            String SysRemark = "";//    备注说明    合作伙伴接口政策实时预定！   
            String Name = "";//     联系人     否
            String MobilePhone = "";//    联系电话        否

            param.append("<PnrAuthOffice>Pek000</PnrAuthOffice ><TicketAuthOffice></TicketAuthOffice>"
                    + "<AgentContactMob>"
                    + AgentContactMob
                    + "</AgentContactMob ><IsBookPNR >0</IsBookPNR>"
                    + "<LinkmanInfo Name=\"\" MobilePhone = \"\" /><Remark SysRemark=\"\"/></OrderInfo></ORDER_CREATE_RQ>");
            String paramString = param.toString();
            WriteLog.write("百拓CreateOrder", order.getId() + ":下单请求:下单:0:" + paramString);
            detailCreateOrder2.setXmlDoc(paramString);
            DetailCreateOrderResponse detailCreateOrderResponse = stub.detailCreateOrder(detailCreateOrder2);
            String DetailCreateOrderResult = detailCreateOrderResponse.getDetailCreateOrderResult();
            WriteLog.write("百拓CreateOrder", order.getId() + ":下单响应:下单:1:" + DetailCreateOrderResult);
            //            <BAITOUR_ORDER_CREATE_RS><Success forderformid="1keoc71200275755" shouldPay="1871.90" PolicyID="25AEB5B1-3D1E-4E02-BA52-4F6987EDA0B8" Rate="0.0060" LocalOrderID="YDXA201609181402598303" Office="" Price="1850.0" Tax="50" YQTax="0" MoneyKeep="17.00" ></Success></BAITOUR_ORDER_CREATE_RS>
            str = getCreateOrderResultString(DetailCreateOrderResult);

        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }

        return str;
    }

    private String getCreateOrderResultString(String detailCreateOrderResult) {
        String str = "-1";
        //      str = "S|" + orderNO + "|" + payurl + "|" + supplyOfficeNo + "|" + payfee;
        String isSuccess = "F";
        String orderNO = "";
        String payurl = "";
        String supplyOfficeNo = "";
        String payfee = "";
        org.dom4j.Document document = null;
        try {
            document = DocumentHelper.parseText(detailCreateOrderResult);
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }
        org.dom4j.Element root = document.getRootElement();
        org.dom4j.Element Success = root.element("Success");
        if (Success != null) {
            isSuccess = "S";
            orderNO = Success.attributeValue("forderformid");
            payfee = Success.attributeValue("shouldPay");
        }
        //        <BAITOUR_ORDER_CREATE_RS><Success forderformid=\"1keoc71200275755\" shouldPay=\"1871.90\" PolicyID=\"25AEB5B1-3D1E-4E02-BA52-4F6987EDA0B8\" 
        //        Rate=\"0.0060\" LocalOrderID=\"YDXA201609181402598303\" Office=\"\" Price=\"1850.0\" Tax=\"50\" YQTax=\"0\" MoneyKeep=\"17.00\" ></Success></BAITOUR_ORDER_CREATE_RS>
        str = isSuccess + "|" + orderNO + "|" + payurl + "|" + supplyOfficeNo + "|" + payfee;
        return str;
    }

    /**
     * 2.1.3    匹配政策接口
     * 
     * @param listSinfo
     * @param listPassenger
     * @return
     * @time 2016年9月14日 上午11:57:52
     * @author chendong
     */
    public List<Zrate> MatchCommonPolicy(List<Segmentinfo> listSinfo, List<Passenger> listPassenger) {
        long l1 = System.currentTimeMillis();
        List<Zrate> zrates = new ArrayList<Zrate>();
        BaitourServiceStub stub;
        if (listSinfo.size() > 0) {
            Segmentinfo sinfo = listSinfo.get(0);
            String DepartureDateTime = getDepartureDateTime(sinfo.getDeparttime());//DepartureDateTime   出发日期    2010-02-15T14:25:00
            String FlightNumber = sinfo.getFlightnumber();
            String ResBookDesigCode = sinfo.getCabincode();
            String DepartureAirport = sinfo.getStartairport();
            String ArrivalAirport = sinfo.getEndairport();//ArrivalAirport    到港城市    LZO
            String ReturnPolicyType = "2";//ReturnPolicyType 匹配结果返回类型， 1.最优的政策 2. 按返点降序排，政策列表默认条 1或2
            String TripType = "1";//TripType   行程类型 1单程 2往返    1或2
            String FlightNumberBack = "";
            String ResBookDesigCodeBack = "";
            String DepartureDateTimeBack = "";
            try {
                stub = new BaitourServiceStub();
                com.baitour.www.BaitourServiceStub.MatchCommonPolicy matchCommonPolicy24 = new com.baitour.www.BaitourServiceStub.MatchCommonPolicy();
                String param = "<OTA_AirFareRQ AgentCode='" + this.AgentCode + "'  AgentUserName='"
                        + this.AgentUserName + "'  AgentPwd='" + this.AgentPwd + "' DepartureDateTime='"
                        + DepartureDateTime + "' FlightNumber='" + FlightNumber + "' ResBookDesigCode='"
                        + ResBookDesigCode + "' DepartureAirport='" + DepartureAirport + "' ArrivalAirport='"
                        + ArrivalAirport + "' ReturnPolicyType='" + ReturnPolicyType + "'  TripType='" + TripType
                        + "'></OTA_AirFareRQ>";
                //                System.out.println("request:" + param);
                WriteLog.write("百拓MatchCommonPolicy", l1 + ":请求:0:" + param);
                matchCommonPolicy24.setXmlDoc(param);
                com.baitour.www.BaitourServiceStub.MatchCommonPolicyResponse matchCommonPolicyResponse = stub
                        .matchCommonPolicy(matchCommonPolicy24);
                String MatchCommonPolicyResult = matchCommonPolicyResponse.getMatchCommonPolicyResult();
                WriteLog.write("百拓MatchCommonPolicy", l1 + ":响应:0:" + MatchCommonPolicyResult);
                zrates = getzratesBy(MatchCommonPolicyResult);
                //                System.out.println("response:" + matchCommonPolicyResponse.getMatchCommonPolicyResult());
                //                System.out.println(matchCommonPolicyResponse.getMatchCommonPolicyResult());
            }
            catch (AxisFault e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (RemoteException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (DocumentException e) {
                e.printStackTrace();
            }
        }
        return zrates;
    }

    /**
     * 把返回回来的政策信息的xml解析成我们自己的对象
     * 
     * @param matchCommonPolicyResult
     * @return
     * @throws DocumentException
     * @time 2016年9月18日 下午3:04:20
     * @author chendong
     */
    private List<Zrate> getzratesBy(String matchCommonPolicyResult) throws DocumentException {
        List<Zrate> zrates = new ArrayList<Zrate>();
        Document document = DocumentHelper.parseText(matchCommonPolicyResult);
        Element root = document.getRootElement();
        List list = root.elements("Item");
        for (int i = 0; i < list.size(); i++) {
            try {
                Zrate zrate = new Zrate();
                zrate.setAgentid(10L);
                Element element = (Element) list.get(i);
                //            <Item  Id="D094651B-F1DF-4A7D-B26B-AC75A2D62FF3" Price="610" AgentPrice="597.36" Commission="12.64" Tax="50" YQTax="0" Rate="0.0040" MoneyKeep="10.20" Effdate="2016-08-01" Expdate="2016-09-30" IsAutoTicket="1" IssueTicketSpeed="0.1" ChangePnr="1" ProviderWorkTime="08:30-21:30,08:30-21:30,08:30-21:30,08:30-21:30,08:30-21:30,08:30-21:30,08:30-21:30" Remark="签转换开跨月改期收回代理费" PolicyType="1" VoidWorkTime="08:30-21:30,08:30-21:30,08:30-21:30,08:30-21:30,08:30-21:30,08:30-21:30,08:30-21:30" dataSource="0" Office="CTU503"></Item>
                //            System.out.println(element);
                //            Id  政策ID    d36cd3a5-da30-4b77-b855-84cb80efdae8
                String Id = element.attributeValue("Id");
                zrate.setOutid(Id);
                //            Price   票面价 2000
                String Price = element.attributeValue("Price");
                float totalfaceprice = 0F;
                try {
                    totalfaceprice = Float.parseFloat(Price);//获取到的票面价
                }
                catch (Exception e) {
                }
                zrate.setTotalfaceprice(totalfaceprice);
                //            AgentPrice  结算价 1700.1
                String AgentPrice = element.attributeValue("AgentPrice");
                //            Commission  佣金  299.9
                String Commission = element.attributeValue("Commission");
                //            Tax 机建  50
                String Tax = element.attributeValue("Tax");
                //            YQTax   燃油  140
                String YQTax = element.attributeValue("YQTax");
                //            Rate    返点  0.205
                String Rate = element.attributeValue("Rate");
                //            MoneyKeep   贴留钱（支持正负）   1.00
                String MoneyKeep = element.attributeValue("MoneyKeep");
                zrate.setRatevalue(getRateValue(Commission, Price));

                //            Effdate 开始时间    销售日期
                String Effdate = element.attributeValue("Effdate");
                //            Expdate 结束时间    销售日期
                String Expdate = element.attributeValue("Expdate");
                //            IsAutoTicket    是否自动出票  0或者1
                String IsAutoTicket = element.attributeValue("IsAutoTicket");
                zrate.setIsauto(Long.parseLong(IsAutoTicket));
                //            IssueTicketSpeed    出票速度    1
                String IssueTicketSpeed = element.attributeValue("IssueTicketSpeed");
                zrate.setSpeed(IssueTicketSpeed);
                //            ChangePnr   是否需要换编码出票   0或者1
                String ChangePnr = element.attributeValue("ChangePnr");
                zrate.setIschange(Long.parseLong(ChangePnr));
                //            ProviderWorkTime    工作时间    08:20-23:00,08:20-23:00, 8:20-23:00,08:20-23:00, 8:20-23:00,08:20-23:00, 8:20-23:00
                String ProviderWorkTime = element.attributeValue("ProviderWorkTime");
                zrate = setWorkTime(zrate, ProviderWorkTime);
                //            Remark  备注  
                String Remark = element.attributeValue("Remark");
                zrate.setRemark(Remark);
                //            PolicyType  政策类型    1  BSP  2  B2B  3 BSP及B2B
                String PolicyType = element.attributeValue("PolicyType");
                zrate.setTickettype(Integer.parseInt(PolicyType));//1=BSP,2=B2B)
                //            VoidWorkTime    废票时间    08:20-23:00,08:20-23:00, 8:20-23:00,08:20-23:00, 8:20-23:00,08:20-23:00, 8:20-23:00
                String VoidWorkTime = element.attributeValue("VoidWorkTime");
                //            Office  Office号码    PEK789
                String Office = element.attributeValue("Office");
                zrate.setAgentcode(Office);
                //            System.out.println(Office);
                zrate.setGeneral(1L);
                zrates.add(zrate);
            }
            catch (Exception e) {
                e.printStackTrace();
                // TODO: handle exception
            }
        }

        return zrates;
    }

    private Float getRateValue(String commission, String s_price) {
        Float ratevalue = 0F;
        try {
            Float price = Float.parseFloat(s_price);
            int totalMoney = (int) Float.parseFloat(commission);
            ratevalue = totalMoney / price * 100;
            DecimalFormat decimalFormat = new DecimalFormat("0.00");//构造方法的字符格式这里如果小数不足1位,会以0补足.
            String p = decimalFormat.format(ratevalue);//format 返回的是字符串
            ratevalue = Float.parseFloat(p);
        }
        catch (Exception e) {
        }
        return ratevalue;
    }

    private Zrate setWorkTime(Zrate zrate, String voidWorkTime) {
        try {
            String[] voidWorkTimes = voidWorkTime.split(",");
            String workTime = voidWorkTimes[getWeekOfDate()].trim();
            zrate.setWorktime(workTime.split("-")[0]);
            zrate.setAfterworktime(workTime.split("-")[1]);
        }
        catch (Exception e) {
        }
        return zrate;
    }

    /**
     * DepartureDateTime   出发日期    2010-02-15T14:25:00
     * 
     * @param listSinfo
     * @return
     * @time 2016年9月12日 下午3:35:23
     * @author chendong
     */
    private String getDepartureDateTime(Timestamp departtime) {
        String DepartureDateTime = "";
        Date date = new Date((long) departtime.getTime());
        DepartureDateTime = TimeUtil.dateToString(date, "yyyy-MM-dd HH:mm:ss");
        DepartureDateTime = DepartureDateTime.replace(" ", "T");
        return DepartureDateTime;
    }

    /**
     * 获取当前日期是星期几<br>
     * 
     * @param dt
     * @return 当前日期是星期几
     */
    public int getWeekOfDate() {
        //        String[] weekDays = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
        int[] weekDays = { 6, 0, 1, 2, 3, 4, 5 };
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0)
            w = 0;
        return weekDays[w];
    }

    /**
     * 
     * 
     * @param order
     * @param opUrl 支付、出票、退款成功平台回调接口合作伙伴的URL地址
     * @return
     * @time 2016年9月19日 上午10:40:11
     * @author chendong
     */
    public String autoPay(Orderinfo order, String opUrl) {
        String resultStr = "-1";
        FlightPaymentStub stub;
        try {
            stub = new FlightPaymentStub();
            org.tempuri.FlightPaymentStub.CoPayment coPayment0 = new org.tempuri.FlightPaymentStub.CoPayment();
            //            String opUrl = "";//支付、出票、退款成功平台回调接口合作伙伴的URL地址
            String param = "<CoPayment><UserName>" + this.AgentUserName + "</UserName><AgentPwd>" + this.AgentPwd
                    + "</AgentPwd><ForderId>" + order.getExtorderid() + "</ForderId><CoShouldPay>"
                    + order.getExtorderprice() + "</CoShouldPay>" + "<opUrl>" + opUrl
                    + "</opUrl><PaymentMode>AUTOALIPAY</PaymentMode></CoPayment>";
            WriteLog.write("百拓autoPay", order.getId() + ":请求:0:" + param);
            coPayment0.setXmlDoc(param);
            org.tempuri.FlightPaymentStub.CoPaymentResponse coPaymentResponse = stub.coPayment(coPayment0);
            String coPaymentResult = coPaymentResponse.getCoPaymentResult();
            WriteLog.write("百拓autoPay", order.getId() + ":响应:0:" + coPaymentResult);
            resultStr = analysisPayMentResult(coPaymentResult);
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        return resultStr;
    }

    private String analysisPayMentResult(String coPaymentResult) {
        String resultStr = "-1";
        Document document;
        try {
            document = DocumentHelper.parseText(coPaymentResult);
            Element root = document.getRootElement();
            //            PaymentResult   T（成功）/F（失败） T支付成功
            String PaymentResult = root.elementText("PaymentResult");
            if ("T".equals(PaymentResult)) {
                resultStr = "S";
            }
            else {
                resultStr = "F|" + root.elementText("Error");
            }
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }

        return resultStr;
    }

    public static void mai1n(String[] args) {
        BartourMethod bartourMethod = BartourMethod.getInstance();
        String coPaymentResult = "<CoPaymentResult><PaymentResult>T</PaymentResult><TradeNo>2012060607081223</TradeNo><Error></Error></CoPaymentResult>";
        System.out.println(bartourMethod.analysisPayMentResult(coPaymentResult));
    }

    public static void main(String[] args) {
        BartourMethod bartourMethod = BartourMethod.getInstance();
        //        bartourMethod
        //                .getCreateOrderResultString("<BAITOUR_ORDER_CREATE_RS><Success forderformid=\"1keoc71200275755\" shouldPay=\"1871.90\" PolicyID=\"25AEB5B1-3D1E-4E02-BA52-4F6987EDA0B8\" Rate=\"0.0060\" LocalOrderID=\"YDXA201609181402598303\" Office=\"\" Price=\"1850.0\" Tax=\"50\" YQTax=\"0\" MoneyKeep=\"17.00\" ></Success></BAITOUR_ORDER_CREATE_RS>");
        String flightnumber = "CA1501";
        List<Segmentinfo> listSinfo = new ArrayList<Segmentinfo>();
        Segmentinfo segmentinfo = new Segmentinfo();
        segmentinfo.setFlightnumber(flightnumber);
        try {
            segmentinfo.setDeparttime(new Timestamp(TimeUtil.stringToLong("2016-10-30 08:30", "yyyy-MM-dd HH:mm")));
            segmentinfo.setArrivaltime(new Timestamp(TimeUtil.stringToLong("2016-10-30 11:40", "yyyy-MM-dd HH:mm")));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        segmentinfo.setCabincode("A");
        segmentinfo.setDepartureCtiy("PEK");
        segmentinfo.setArriveCity("SHA");
        segmentinfo.setParvalue(2110F);
        segmentinfo.setAirportfee(50F);
        segmentinfo.setFuelfee(0F);

        listSinfo.add(segmentinfo);
        List<Passenger> listPassenger = getPassenger();

        String outid = "";
        outid = bartourMethod.MatchCommonPolicy(listSinfo, listPassenger).get(0).getOutid();//2.1.3    匹配政策接口

        Orderinfo order = new Orderinfo();
        order.setPnr("HTK5JX");
        order.setBigpnr("MHG0MD");
        order.setOrdernumber("CD201609141538AAA");
        Zrate zrate = new Zrate();
        zrate.setOutid(outid);
        segmentinfo.setZrate(zrate);
        //                        bartourMethod.DetailCreateOrderMethod(order, segmentinfo, listPassenger);
    }

    private static List<Passenger> getPassenger() {
        List<Passenger> listPassenger = new ArrayList<Passenger>();
        Passenger p1 = new Passenger();
        p1.setName("陶继新");
        p1.setPtype(1);
        p1.setIdtype(4);
        p1.setIdnumber("4558");

        Passenger p2 = new Passenger();
        p2.setName("陶继新");
        p2.setPtype(1);
        p2.setIdtype(4);
        p2.setIdnumber("4558");

        listPassenger.add(p1);
        listPassenger.add(p2);
        return listPassenger;
    }
}
