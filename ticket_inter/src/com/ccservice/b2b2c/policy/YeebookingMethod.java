package com.ccservice.b2b2c.policy;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.log4j.Logger;

import com.ccservice.b2b2c.base.eaccount.Eaccount;
import com.ccservice.b2b2c.base.orderinfo.Orderinfo;
import com.ccservice.b2b2c.base.passenger.Passenger;
import com.ccservice.b2b2c.base.segmentinfo.Segmentinfo;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.ben.Airticketform;
import com.ccservice.b2b2c.ben.Orderchange;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;

public class YeebookingMethod extends SupplyMethod {
    Logger logger = Logger.getLogger(this.getClass().getSimpleName());

    Airticketform airform = new Airticketform();

    //易订行账号信息
    Eaccount eaccount = Server.getInstance().getSystemService().findEaccount(20);

    public String createOrder(Orderinfo order, Segmentinfo sinfo, List<Passenger> listPassenger) {
        String result = "-1";
        Zrate zrate = sinfo.getZrate();
        //如果这个有值,这里放的是真正的返点值
        float realRateValue = zrate.getAddratevalue() != null ? zrate.getAddratevalue() : zrate.getRatevalue();
        long general = sinfo.getIsspecial();
        StringBuilder strXML = new StringBuilder("<?xml version=\"1.0\" encoding=\"UTF-8\"?><CCS-CreateOrder>");
        try {
            // 此处用id为20的Eaccount用作下单到易订行的账号
            Eaccount eaccount = Server.getInstance().getSystemService().findEaccount(20);
            strXML.append("<Account username=\"" + eaccount.getUsername() + "\" password=\"" + eaccount.getPassword()
                    + "\" cmd=\"SUBMITORDER\"></Account>");

            String segmentinfo1 = "";
            try {
                List<Segmentinfo> listsinfo = Server.getInstance().getAirService()
                        .findAllSegmentinfo("WHERE C_ORDERID=" + order.getId(), "ORDER BY ID DESC", -1, 0);
                if (listsinfo.size() > 1) {
                    Segmentinfo sinfo1 = listsinfo.get(0);
                    if (!sinfo1.getStartairport().equals(sinfo.getStartairport())) {
                    }
                    else {
                        sinfo1 = listsinfo.get(1);
                    }
                    //此信息用来记录往返和联程的行程信息
                    segmentinfo1 = " airline1=\"" + sinfo1.getFlightnumber() + "\" cabincode1=\""
                            + sinfo1.getCabincode() + "\" sdate1=\"" + sinfo1.getDeparttime() + "\" " + "edate1=\""
                            + sinfo1.getArrivaltime() + "\" discount1=\"" + sinfo1.getDiscount() + "\" " + " scity1=\""
                            + sinfo1.getStartairport() + "\" ecity1=\"" + sinfo1.getEndairport() + "\" ";
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            strXML.append("<Segmentinfo airline=\"" + sinfo.getFlightnumber() + "\" airportfee=\""
                    + listPassenger.get(0).getAirportfee() + "\" fuelfee=\"" + listPassenger.get(0).getFuelprice()
                    + "\" cabincode=\"" + sinfo.getCabincode() + "\" sdate=\"" + sinfo.getDeparttime() + "\" edate=\""
                    + sinfo.getArrivaltime() + "\" price=\"" + listPassenger.get(0).getPrice() + "\" realRateValue=\""
                    + realRateValue + "\" discount=\"" + sinfo.getDiscount() + "\" yprice=\"" + sinfo.getYprice()
                    + "\"  scity=\"" + sinfo.getStartairport() + "\" ecity=\"" + sinfo.getEndairport() + "\" rules=\""
                    + URLEncoder.encode(zrate.getRemark() == null ? "" : zrate.getRemark(), "UTF-8")
                    + "\" ratevalue=\"" + zrate.getRatevalue() + "\" parvalue=\"" + sinfo.getParvalue()
                    + "\" agentid=\"" + zrate.getAgentid() + "\" rateid=\"" + zrate.getOutid() + "\" agentcode=\""
                    + (zrate.getAgentcode() == null ? "" : zrate.getAgentcode()) + "\" ordernum=\""
                    + order.getOrdernumber() + "\" agentcontactmobile=\"" + order.getAgentcontactmobile()
                    + "\" afterworktime=\"" + order.getExtorderepolicyid() + "\" general=\"" + general + "\" "
                    + segmentinfo1 + " ></Segmentinfo>");
        }
        catch (UnsupportedEncodingException e1) {
            logger.error("", e1.fillInStackTrace());
        }
        catch (Exception e1) {
            logger.error("", e1.fillInStackTrace());
        }
        String names = "";
        String ptype = "";
        String idcard = "";
        String insurprice = "";
        String mobile = "";
        String birthday = "";
        for (int i = 0; i < listPassenger.size(); i++) {
            Passenger passenger = listPassenger.get(i);
            names += passenger.getName();
            ptype += passenger.getPtype();
            insurprice += passenger.getInsurprice();
            mobile += passenger.getMobile() == null ? "" : passenger.getMobile();
            birthday += passenger.getBirthday() == null ? "" : passenger.getBirthday();
            if (passenger.getIdtype() == 1) {
                idcard += "NI" + passenger.getIdnumber();
            }
            else {
                idcard += "PP" + passenger.getIdnumber();
            }
            if (i < listPassenger.size() - 1) {
                names += "|";
                ptype += "|";
                idcard += "|";
                insurprice += "|";
                mobile += "|";
                birthday += "|";
            }

        }
        //0 不买 1 买
        if ("0".equals(getSysconfigString("isbuyinsuretogetherOrderjk"))) {
            insurprice = "0";
        }
        strXML.append("<Passenger name=\"" + names + "\" ptype=\"" + ptype + "\" idcard=\"" + idcard
                + "\" insurprice=\"" + insurprice + "\" mobile=\"" + mobile + "\" birthday=\"" + birthday + "\"");

        strXML.append(" pnr=\"" + order.getPnr() + "\"");
        if (order.getUserRtInfo() != null) {
            try {
                strXML.append(" pnrrtinfo=\""
                        + URLEncoder.encode(order.getUserRtInfo() == null ? "" : order.getUserRtInfo(), "UTF-8")
                        + "\" pnrpatinfo=\""
                        + URLEncoder.encode(order.getPnrpatinfo() == null ? "" : order.getPnrpatinfo(), "UTF-8")
                        + "\" ");
            }
            catch (UnsupportedEncodingException e) {
                logger.error("", e.fillInStackTrace());
            }
            catch (Exception e) {
                logger.error("", e.fillInStackTrace());
            }
        }
        else {
            strXML.append(" pnrrtinfo=\"\" pnrpatinfo=\"\" ");
        }
        if (order.getNameIsRight() != null) {
            strXML.append(" nameisright=\"" + order.getNameIsRight().replace("<br>", "") + "\" >");
        }
        else {
            strXML.append(" nameisright=\"\" >");
        }
        strXML.append("</Passenger>");
        strXML.append("</CCS-CreateOrder>");
        int ran = new Random().nextInt(100);
        WriteLog.write("YDX", ran + "createOrder:0:" + strXML.toString());
        // <?xml version=\"1.0\"
        // encoding=\"UTF-8\"?><CCS-OrderResponse><Response
        // statuscode=\"S\"><OrderNo>W123456789</OrderNo><Remark></Remark></Response></CCS-OrderResponse>
        String Yeeresult = submitPost(eaccount.getUrl(), strXML.toString());
        WriteLog.write("YDX", ran + "createOrder:1.5:" + eaccount.getUrl());
        WriteLog.write("YDX", ran + "createOrder:1:" + Yeeresult);
        try {
            //			String statuscode = getstatuscode(Yeeresult);
            //			if (true) {
            String YeebookingOrderNo = getXMLMessageNodeValue(Yeeresult, "YeebookingOrderNo");
            String orderNo = getXMLMessageNodeValue(Yeeresult, "OrderNo");
            if (orderNo != null) {
                YeebookingOrderNo += "#" + orderNo;
            }
            String payYeeUrl = getXMLMessageNodeValue(Yeeresult, "payYeeUrl");
            String officeid = getXMLMessageNodeValue(Yeeresult, "officeid");
            if (YeebookingOrderNo != null) {
                result = "S|" + YeebookingOrderNo + "|" + payYeeUrl.replaceAll("223.4.155.3", "www.yeebooking.com");
            }
            else {
                result = "S|" + YeebookingOrderNo + "|http://www.";
            }
            if (officeid != "") {
                result += "|" + officeid;
            }
            //			}
            //			else {
            //				String Remark = getXMLMessageNodeValue(Yeeresult, "Remark");
            //				System.out.println(Remark);
            //			}
            try {
                String PayOrderprice = getXMLMessageNodeValue(Yeeresult, "PayOrderprice");
                WriteLog.write("YDX", ran + "PayOrderprice:1:" + PayOrderprice);
                if (PayOrderprice != null && !"".equals(PayOrderprice)) {
                    result += "|" + PayOrderprice;
                }
            }
            catch (Exception e) {
                UtilMethod.writeEx("YeebookingMethod", e);
                e.printStackTrace();
            }
        }
        catch (Exception e) {
            logger.error("", e.fillInStackTrace());
        }
        return result;
    }

    public String ChangeOrder(Orderinfo order, Orderchange orderchange) {
        int ran = new Random().nextInt(100);
        String ticketno = "";
        String temppid = "";
        String tripnote = order.getTripnote();
        String extorderid = order.getExtorderid();
        // 把orderchangeid暂时放到order的Ucode里面，方便查询出orderchange
        WriteLog.write("YDX", ran + "change:-1:orderid" + order.getId() + ":orderchangeid:" + orderchange.getId());
        Segmentinfo sege = getSegmentinfoByOrderchange(orderchange);
        List<Passenger> listpassList = getPassengerIDByOrderchange(orderchange);
        if (listpassList.size() > 0) {
            for (int i = 0; i < listpassList.size(); i++) {
                Passenger passenger = listpassList.get(i);
                ticketno += passenger.getTicketnum() + "|";
                String idnumber = passenger.getIdnumber();
                String airports = sege.getStartairport() + sege.getEndairport();
                temppid += idnumber + "@" + airports + "|";
            }
        }
        StringBuilder strXML = new StringBuilder("<?xml version=\"1.0\" encoding=\"UTF-8\"?><CCS-Orderchange>");
        // 此处用id为20的Eaccount用作提交退费到易订行
        Eaccount eaccount = Server.getInstance().getSystemService().findEaccount(20);
        strXML.append("<Account username=\"" + eaccount.getUsername() + "\" password=\"" + eaccount.getPassword()
                + "\" cmd=\"Orderchange\"></Account>");
        strXML.append("<Orderchange extOrderNum=\"" + extorderid + "\" yeebookingOrderNum=\"" + tripnote
                + "\" changestate=\"" + orderchange.getChangestate() + "\" changetype=\"" + orderchange.getChangetype()
                + "\" changereason=\"" + orderchange.getChangereason() + "\" changememo=\""
                + orderchange.getChangememo() + "\" provefile=\"" + orderchange.getProvefile() + "\" ticketno=\""
                + ticketno + "\" pid=\"" + temppid + "\"></Orderchange>");
        strXML.append("</CCS-Orderchange>");
        WriteLog.write("YDX", ran + "change:0:" + strXML.toString());
        System.out.println(strXML.toString());
        String Yeeresult = submitPost(eaccount.getUrl(), strXML.toString());
        WriteLog.write("YDX", ran + "change:1:" + Yeeresult);
        return Yeeresult;
    }

    private Segmentinfo getSegmentinfoByOrderchange(Orderchange orderchange) {
        Segmentinfo segmentinfo = new Segmentinfo();
        String sql = "SELECT C_SID AS SID FROM  T_PCHANGEREF WHERE C_CHANGEID=" + orderchange.getId();
        List pid = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        if (pid.size() > 0) {
            Map m = (Map) pid.get(0);
            long SID = Long.parseLong(m.get("SID").toString());
            segmentinfo = Server.getInstance().getAirService().findSegmentinfo(SID);
        }
        return segmentinfo;
    }

    public List<Passenger> getPassengerIDByOrderchange(Orderchange orderchange) {
        List<Passenger> listpassList = new ArrayList<Passenger>();
        String sql = "SELECT C_PID AS PID FROM  T_PCHANGEREF WHERE C_CHANGEID=" + orderchange.getId();
        List pid = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        if (pid.size() > 0) {
            for (int i = 0, size = pid.size(); i < size; i++) {
                Map m = (Map) pid.get(i);
                long pidl = Long.parseLong(m.get("PID").toString());
                listpassList.add(Server.getInstance().getAirService().findPassenger(pidl));
            }
        }
        return listpassList;
    }

    public static void main(String[] args) {
    }

    public void outTicket(String ordernum) {
        String string = this.getClass().getSimpleName();
        System.out.println(string);

    }

    public float getBackPoint(Zrate zrate) {
        // 此处用id为20的Eaccount用作下单到易订行的账号
        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><CCS-CreateOrder><Account username=\""
                + eaccount.getUsername() + "\" password=\"" + eaccount.getPassword() + "\" cmd=\"GETPOINT\" fpoint=\""
                + zrate.getRatevalue() + "\" /></CCS-CreateOrder>";
        float point = zrate.getRatevalue();
        String temppoint = submitPost(eaccount.getUrl(), xml);
        if (temppoint != null) {
            try {
                float tempbackpoint = Float.parseFloat(temppoint);
                point = tempbackpoint;
            }
            catch (Exception e) {
                UtilMethod.writeEx("YeebookingMethod", e);
                e.printStackTrace();
            }
        }
        return point;
    }

    /**
     * 从报文中截取指定名称的节点值（不包含节点本身）
     * 
     * @param message
     *            报文文本
     * @param nodeName
     *            节点名
     * @return 节点值
     */
    private static String getXMLMessageNodeValue(String message, String nodeName) {
        String nodeXML = getXMLMessageByNodeName(message, nodeName);
        int startIdx = nodeXML.indexOf('>') + 1;
        int endIdx = nodeXML.lastIndexOf('<');
        if (startIdx == -1 || endIdx == -1) {
            throw new RuntimeException("找不到对应的节点名：" + nodeName);
        }
        else {
            return nodeXML.substring(startIdx, endIdx);
        }
    }

    /**
     * 从报文中截取指定名称的节点内容（包含节点本身）
     * 
     * @param message
     *            报文文本
     * @param nodeName
     *            节点名
     * @return 节点内容
     */
    private static String getXMLMessageByNodeName(String message, String nodeName) {
        String openNode = "<" + nodeName + ">";
        String closeNode = "</" + nodeName + ">";
        int startIdx = message.indexOf(openNode);
        int endIdx = message.indexOf(closeNode) + closeNode.length();
        if (startIdx == -1 || endIdx == -1) {
            throw new RuntimeException("找不到对应的节点名：" + nodeName);
        }
        else {
            return message.substring(startIdx, endIdx);
        }
    }

    private static String getstatuscode(String xml) {
        String result = xml.substring(xml.indexOf("statuscode") + 12, xml.indexOf("statuscode") + 13);
        System.out.println(result);
        return result;
    }

    /**
     * 
     * @param order
     * @param sinfo
     * @param listPassenger
     * @return
     */
    public String createBabyOrder(Orderinfo order, Segmentinfo sinfo, List<Passenger> listPassenger) {

        String result = "-1";
        StringBuilder strXML = new StringBuilder("<?xml version=\"1.0\" encoding=\"UTF-8\"?><CCS-CreateOrder>");
        try {
            // 此处用id为20的Eaccount用作下单到易订行的账号
            Eaccount eaccount = Server.getInstance().getSystemService().findEaccount(20);
            //婴儿票 在易订行中需要关联的主订单（易订行的成人订单号）
            String relationorderidstr = "";
            strXML.append("<Account username=\"" + eaccount.getUsername() + "\" password=\"" + eaccount.getPassword()
                    + "\" cmd=\"SUBBABYORDER\"></Account>");
            relationorderidstr = "\"  relationorderid=\"" + order.getTripnote();

            strXML.append("<Segmentinfo airline=\"" + sinfo.getFlightnumber() + "\" airportfee=\""
                    + listPassenger.get(0).getAirportfee() + "\" fuelfee=\"" + listPassenger.get(0).getFuelprice()
                    + "\" cabincode=\"" + sinfo.getCabincode() + "\" sdate=\"" + sinfo.getDeparttime() + "\" edate=\""
                    + sinfo.getArrivaltime() + "\" price=\"" + listPassenger.get(0).getPrice() + "\"  discount=\""
                    + sinfo.getDiscount() + "\" yprice=\"" + sinfo.getYprice() + "\"  scity=\""
                    + sinfo.getStartairport() + "\" ecity=\"" + sinfo.getEndairport() + "\" parvalue=\""
                    + sinfo.getParvalue() + "\"  ordernum=\"" + order.getOrdernumber() + relationorderidstr
                    + "\"  agentcontactmobile=\"" + order.getAgentcontactmobile() + "\"  afterworktime=\""
                    + order.getExtorderepolicyid() + "\"></Segmentinfo>");
        }
        catch (Exception e) {
            logger.error("", e.fillInStackTrace());
        }
        String names = "";
        String ptype = "";
        String idcard = "";
        String insurprice = "";
        for (int i = 0; i < listPassenger.size(); i++) {
            Passenger passenger = listPassenger.get(i);
            names += passenger.getName();
            ptype += passenger.getPtype();
            insurprice += passenger.getInsurprice();
            if (passenger.getIdtype() == 1) {
                idcard += "NI" + passenger.getIdnumber();
            }
            else {
                idcard += "PP" + passenger.getIdnumber();
            }
            if (i < listPassenger.size() - 1) {
                names += "|";
                ptype += "|";
                idcard += "|";
                insurprice += "|";
            }

        }
        strXML.append("<Passenger name=\"" + names + "\" ptype=\"" + ptype + "\" idcard=\"" + idcard
                + "\" insurprice=\"" + insurprice + "\"");

        strXML.append(" pnr=\"" + order.getPnr() + "\"");
        if (order.getUserRtInfo() != null) {
            try {
                strXML.append(" pnrrtinfo=\""
                        + URLEncoder.encode(order.getUserRtInfo() == null ? "" : order.getUserRtInfo(), "UTF-8")
                        + "\" pnrpatinfo=\""
                        + URLEncoder.encode(order.getPnrpatinfo() == null ? "" : order.getPnrpatinfo(), "UTF-8")
                        + "\" ");
            }
            catch (UnsupportedEncodingException e) {
                UtilMethod.writeEx("YeebookingMethod", e);
                e.printStackTrace();
            }
        }
        else {
            strXML.append(" pnrrtinfo=\"\" pnrpatinfo=\"\" ");
        }
        if (order.getNameIsRight() != null) {
            strXML.append(" nameisright=\"" + order.getNameIsRight().replace("<br>", "") + "\" >");
        }
        else {
            strXML.append(" nameisright=\"\" >");
        }
        strXML.append("</Passenger>");
        strXML.append("</CCS-CreateOrder>");
        int ran = new Random().nextInt(100);
        WriteLog.write("YDX", ran + "createOrder:0:" + strXML.toString());
        String Yeeresult = submitPost(eaccount.getUrl(), strXML.toString());
        WriteLog.write("YDX", ran + "createOrder:1.5:" + eaccount.getUrl());
        WriteLog.write("YDX", ran + "createOrder:1:" + Yeeresult);
        try {
            String YeebookingOrderNo = getXMLMessageNodeValue(Yeeresult, "YeebookingOrderNo");
            String orderNo = getXMLMessageNodeValue(Yeeresult, "OrderNo");
            if (orderNo != null) {
                YeebookingOrderNo += "#" + orderNo;
            }
            String payYeeUrl = getXMLMessageNodeValue(Yeeresult, "payYeeUrl");
            String officeid = getXMLMessageNodeValue(Yeeresult, "officeid");
            if (YeebookingOrderNo != null) {
                result = "S|" + YeebookingOrderNo + "|" + payYeeUrl;
            }
            else {
                result = "S|" + YeebookingOrderNo + "|http://www.";
            }
            if (officeid != "") {
                result += "|" + officeid;
            }
        }
        catch (Exception e) {
            UtilMethod.writeEx("YeebookingMethod", e);
            e.printStackTrace();
        }
        return result;
    }

}
