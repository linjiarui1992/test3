package com.ccservice.b2b2c.policy;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


/**
 * MD5\u52A0\u5BC6\u7B97\u6CD5
 */
public class MD5Encrypt {
    /**
	 * md5??????
	 * 
	 * @param text
	 * @return
	 */
    public static String md5(String text) {
        MessageDigest msgDigest = null;
        try {
            msgDigest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("System doesn't support MD5 algorithm.");
        }
        try {
            msgDigest.update(text.getBytes("utf-8"));
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException("System doesn't support your  EncodingException.");
        }
        byte[] bytes = msgDigest.digest();
        return bytes2HexStr(bytes);
    }
    	/** */
	/**
	 * 
	 * @param byteArray
	 * @return
	 */
	public static final String bytes2HexStr(byte[] bArray) {
		StringBuffer sb = new StringBuffer(bArray.length);
		String sTemp;
		for (byte element : bArray) {
			sTemp = Integer.toHexString(0xFF & element);
			if (sTemp.length() < 2) {
				sb.append(0);
			}
			sb.append(sTemp.toUpperCase());
		}
		return sb.toString();
	}
}