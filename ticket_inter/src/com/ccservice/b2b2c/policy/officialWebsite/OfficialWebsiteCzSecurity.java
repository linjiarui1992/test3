package com.ccservice.b2b2c.policy.officialWebsite;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import com.airepay.yhb.exception.GetKeyException;
import com.airepay.yhb.security.CertUtils;
import com.airepay.yhb.security.Security;

public class OfficialWebsiteCzSecurity {
    private boolean isConvertEncode;

    protected String lastResultMsg;

    private String sessionMsg;

    protected String lastErrorMsg;

    protected String lastSignMsg;

    private String verInfo = "E易行卡电子账户系统的版本接口,测试版本号:1.0,最后编译日期:2010-06-10";

    private boolean isTestServer;

    public OfficialWebsiteCzSecurity() {
        this.isConvertEncode = true;
        this.isTestServer = true;
        java.security.Security.addProvider(new BouncyCastleProvider());
    }

    public boolean encryptMsg(String sourceMsg, String certPath) {
        boolean ret = false;
        try {
            RSAPublicKey publicKey = (RSAPublicKey) CertUtils.getPublicKeyByCer(certPath);
            BigInteger mod = publicKey.getModulus();
            int keylen = mod.bitLength() / 8;
            if (sourceMsg.getBytes().length > keylen - 11) {
                Cipher cipher = Cipher.getInstance("RSA/NONE/PKCS1Padding", "BC");
                cipher.init(3, publicKey);
                KeyGenerator kp = KeyGenerator.getInstance("DESEDE");
                kp.init(new SecureRandom());
                SecretKey secretKey = kp.generateKey();
                byte[] wrappedKey = cipher.wrap(secretKey);

                cipher = Cipher.getInstance("DESEDE/OFB/NoPadding", new BouncyCastleProvider());

                cipher.init(1, secretKey);
                byte[] encrypt = cipher.doFinal(sourceMsg.getBytes("GBK"));
                byte[] iv = cipher.getIV();

                byte[] wrap_asc = new byte[wrappedKey.length * 2];
                byte[] iv_asc = new byte[iv.length * 2];
                byte[] enc_asc = new byte[encrypt.length * 2];
                Hex2Ascii(iv.length, iv, iv_asc);
                Hex2Ascii(wrappedKey.length, wrappedKey, wrap_asc);
                Hex2Ascii(encrypt.length, encrypt, enc_asc);

                StringBuffer sb = new StringBuffer();
                sb.append(new String(iv_asc));
                sb.append(new String(wrap_asc));
                sb.append(new String(enc_asc));

                this.lastResultMsg = sb.toString();
            }
            else {
                Cipher cipher = Cipher.getInstance("RSA/NONE/PKCS1Padding", "BC");
                cipher.init(1, publicKey);

                byte[] eByte = cipher.doFinal(sourceMsg.getBytes("GBK"));
                this.lastResultMsg = byte2hex(eByte);
            }
            ret = true;
        }
        catch (NoSuchAlgorithmException e) {
            this.lastErrorMsg = "Error Number:-100007, Error Description: ER_NO_SUCH_ALGORITHM_ERROR(无效加密算法错误)";
            e.printStackTrace();
        }
        catch (GetKeyException e) {
            this.lastErrorMsg = e.getErrMsg();
            e.printStackTrace();
        }
        catch (NoSuchPaddingException e) {
            this.lastErrorMsg = "Error Number:-100008, Error Description: ER_NO_SUCH_PADDING_ERROR(无效参数错误)";
            e.printStackTrace();
        }
        catch (InvalidKeyException e) {
            this.lastErrorMsg = "Error Number:-100009, Error Description: ER_INVALID_KEY_ERROR(无效密钥错误)";
            e.printStackTrace();
        }
        catch (IllegalBlockSizeException e) {
            this.lastErrorMsg = "Error Number:-100010, Error Description: ER_ILLEGA_BLOCK_SIZE_ERROR(非法的块大小)";
            e.printStackTrace();
        }
        catch (BadPaddingException e) {
            this.lastErrorMsg = "Error Number:-100011, Error Description: ER_BAD_PADDING_ERROR(参数填充错误)";
            e.printStackTrace();
        }
        catch (NoSuchProviderException e) {
            this.lastErrorMsg = "Error Number:-100007, Error Description: ER_NO_SUCH_ALGORITHM_ERROR(无效加密算法提供商错误)";
            e.printStackTrace();
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return ret;
    }

    public boolean decryptMsg(String sourceMsg, String certPath, String pwd) {
        boolean ret = false;
        try {
            RSAPrivateKey privateKey = (RSAPrivateKey) CertUtils.getPrivateKeyByPfx(certPath, pwd);
            BigInteger mod = privateKey.getModulus();
            int keylen = mod.bitLength() / 8;
            if (sourceMsg.length() > keylen * 2) {
                byte[] iv_asc = sourceMsg.substring(0, 16).getBytes();
                byte[] wrap_asc = sourceMsg.substring(iv_asc.length, iv_asc.length + keylen * 2).getBytes();
                byte[] enc_asc = sourceMsg.substring(iv_asc.length + keylen * 2).getBytes();

                byte[] iv = new byte[8];
                byte[] unwrappedkey = new byte[wrap_asc.length / 2];
                byte[] decrypted = new byte[enc_asc.length / 2];
                Ascii2Hex(iv_asc.length, iv_asc, iv);
                Ascii2Hex(wrap_asc.length, wrap_asc, unwrappedkey);
                Ascii2Hex(enc_asc.length, enc_asc, decrypted);

                Cipher cipher = Cipher.getInstance("RSA/NONE/PKCS1Padding", "BC");
                cipher.init(4, privateKey);
                Key secrptKey = cipher.unwrap(unwrappedkey, "DESEDE", 3);

                IvParameterSpec ivspec = new IvParameterSpec(iv);

                cipher = Cipher.getInstance("DESEDE/OFB/NoPadding");
                cipher.init(2, secrptKey, ivspec);
                this.lastResultMsg = new String(cipher.doFinal(decrypted), "GBK");
            }
            else {
                Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding", new BouncyCastleProvider());

                cipher.init(2, privateKey);
                byte[] eByte = cipher.doFinal(hex2byte(sourceMsg.getBytes()));
                this.lastResultMsg = new String(eByte);
            }
            ret = true;
        }
        catch (NoSuchAlgorithmException e) {
            this.lastErrorMsg = "Error Number:-100007, Error Description: ER_NO_SUCH_ALGORITHM_ERROR(无效加密算法错误)";
            e.printStackTrace();
        }
        catch (GetKeyException e) {
            this.lastErrorMsg = e.getErrMsg();
            e.printStackTrace();
        }
        catch (NoSuchPaddingException e) {
            this.lastErrorMsg = "Error Number:-100008, Error Description: ER_NO_SUCH_PADDING_ERROR(无效参数错误)";
            e.printStackTrace();
        }
        catch (InvalidKeyException e) {
            this.lastErrorMsg = "Error Number:-100009, Error Description: ER_INVALID_KEY_ERROR(无效密钥错误)";
            e.printStackTrace();
        }
        catch (IllegalBlockSizeException e) {
            this.lastErrorMsg = "Error Number:-100010, Error Description: ER_ILLEGAL_BLOCK_SIZE_ERROR(分块大小错误)";
            e.printStackTrace();
        }
        catch (BadPaddingException e) {
            this.lastErrorMsg = "Error Number:-100011, Error Description: ER_BAD_PADDING_ERROR(参数填充错误)";
            e.printStackTrace();
        }
        catch (InvalidAlgorithmParameterException e) {
            this.lastErrorMsg = "Error Number:-100008, Error Description: ER_NO_SUCH_PADDING_ERROR(无效参数错误)";
            e.printStackTrace();
        }
        catch (NoSuchProviderException e) {
            this.lastErrorMsg = "Error Number:-100007, Error Description: ER_NO_SUCH_ALGORITHM_ERROR(无效加密算法提供商错误)";
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

    public boolean signMsg(String msg, String certPath, String pwd) {
        boolean ret = false;
        try {
            PrivateKey privateKey = CertUtils.getPrivateKeyByPfx(certPath, pwd);

            Signature sig = Signature.getInstance("SHA1WithRSA");
            sig.initSign(privateKey);
            sig.update(msg.getBytes());

            this.lastSignMsg = byte2hex(sig.sign());
            ret = true;
        }
        catch (NoSuchAlgorithmException e) {
            this.lastErrorMsg = "Error Number:-100007, Error Description: ER_NO_SUCH_ALGORITHM_ERROR(无效加密算法错误)";
            e.printStackTrace();
        }
        catch (GetKeyException e) {
            this.lastErrorMsg = e.getErrMsg();
            e.printStackTrace();
        }
        catch (SignatureException e) {
            this.lastErrorMsg = "Error Number:-100012, Error Description:ER_SIGNATURE_ERROR(签名异常)";
            e.printStackTrace();
        }
        catch (InvalidKeyException e) {
            this.lastErrorMsg = "Error Number:-100009, Error Description:ER_INVALID_KEY_ERROR(无效密钥错误)";
            e.printStackTrace();
        }
        return ret;
    }

    public boolean verifyMsg(String plainText, String verifiedText, String certPath) {
        boolean ret = false;
        try {
            PublicKey publicKey = CertUtils.getPublicKeyByCer(certPath);
            Signature sig = Signature.getInstance("SHA1WithRSA");
            sig.initVerify(publicKey);
            sig.update(plainText.getBytes());
            if (sig.verify(hex2byte(verifiedText.getBytes()))) {
                ret = true;
            }
            else {
                ret = false;
            }
        }
        catch (NoSuchAlgorithmException e) {
            this.lastErrorMsg = "Error Number:-100007, Error Description: ER_NO_SUCH_ALGORITHM_ERROR(无效加密算法错误)";
            e.printStackTrace();
        }
        catch (GetKeyException e) {
            this.lastErrorMsg = e.getErrMsg();
            e.printStackTrace();
        }
        catch (InvalidKeyException e) {
            this.lastErrorMsg = "Error Number:-100009, Error Description:ER_INVALID_KEY_ERROR(无效密钥错误)";
            e.printStackTrace();
        }
        catch (SignatureException e) {
            this.lastErrorMsg = "Error Number:-100012, Error Description:ER_SIGNA_TURE_ERROR(初始化失败)";
            e.printStackTrace();
        }
        return ret;
    }

    public String getLastResultMsg() {
        return this.lastResultMsg;
    }

    public String getLastErrorMsg() {
        return this.lastErrorMsg;
    }

    public String getLastSignMsg() {
        return this.lastSignMsg;
    }

    private String byte2hex(byte[] b) {
        String hs = "";
        String stmp = "";
        for (int n = 0; n < b.length; n++) {
            stmp = Integer.toHexString(b[n] & 0xFF);
            if (stmp.length() == 1) {
                hs = hs + "0" + stmp;
            }
            else {
                hs = hs + stmp;
            }
        }
        return hs.toUpperCase();
    }

    private byte[] hex2byte(byte[] b) {
        if (b.length % 2 != 0) {
            throw new IllegalArgumentException("长度不是偶数");
        }
        byte[] b2 = new byte[b.length / 2];
        for (int n = 0; n < b.length; n += 2) {
            String item = new String(b, n, 2);

            b2[(n / 2)] = ((byte) Integer.parseInt(item, 16));
        }
        return b2;
    }

    private static void Hex2Ascii(int len, byte[] data_in, byte[] data_out) {
        byte[] temp1 = new byte[1];
        byte[] temp2 = new byte[1];
        int i = 0;
        int j = 0;
        for (; i < len; i++) {
            temp1[0] = data_in[i];
            temp1[0] = ((byte) (temp1[0] >>> 4));
            temp1[0] = ((byte) (temp1[0] & 0xF));
            temp2[0] = data_in[i];
            temp2[0] = ((byte) (temp2[0] & 0xF));
            if ((temp1[0] >= 0) && (temp1[0] <= 9)) {
                data_out[j] = ((byte) (temp1[0] + 48));
            }
            else if ((temp1[0] >= 10) && (temp1[0] <= 15)) {
                data_out[j] = ((byte) (temp1[0] + 87));
            }
            if ((temp2[0] >= 0) && (temp2[0] <= 9)) {
                data_out[(j + 1)] = ((byte) (temp2[0] + 48));
            }
            else if ((temp2[0] >= 10) && (temp2[0] <= 15)) {
                data_out[(j + 1)] = ((byte) (temp2[0] + 87));
            }
            j += 2;
        }
    }

    private static void Ascii2Hex(int len, byte[] data_in, byte[] data_out) {
        byte[] temp1 = new byte[1];
        byte[] temp2 = new byte[1];
        int i = 0;
        for (int j = 0; i < len; j++) {
            temp1[0] = data_in[i];
            temp2[0] = data_in[(i + 1)];
            if ((temp1[0] >= 48) && (temp1[0] <= 57)) {
                int tmp53_52 = 0;
                byte[] tmp53_51 = temp1;
                tmp53_51[tmp53_52] = ((byte) (tmp53_51[tmp53_52] - 48));
                temp1[0] = ((byte) (temp1[0] << 4));
                temp1[0] = ((byte) (temp1[0] & 0xF0));
            }
            else if ((temp1[0] >= 97) && (temp1[0] <= 102)) {
                int tmp101_100 = 0;
                byte[] tmp101_99 = temp1;
                tmp101_99[tmp101_100] = ((byte) (tmp101_99[tmp101_100] - 87));
                temp1[0] = ((byte) (temp1[0] << 4));
                temp1[0] = ((byte) (temp1[0] & 0xF0));
            }
            if ((temp2[0] >= 48) && (temp2[0] <= 57)) {
                int tmp149_148 = 0;
                byte[] tmp149_146 = temp2;
                tmp149_146[tmp149_148] = ((byte) (tmp149_146[tmp149_148] - 48));
                temp2[0] = ((byte) (temp2[0] & 0xF));
            }
            else if ((temp2[0] >= 97) && (temp2[0] <= 102)) {
                int tmp192_191 = 0;
                byte[] tmp192_189 = temp2;
                tmp192_189[tmp192_191] = ((byte) (tmp192_189[tmp192_191] - 87));
                temp2[0] = ((byte) (temp2[0] & 0xF));
            }
            data_out[j] = ((byte) (temp1[0] | temp2[0]));
            i += 2;
        }
    }

    public String getSessionMsg() {
        return this.sessionMsg;
    }

    public void setSessionMsg(String sessionMsg) {
        this.sessionMsg = sessionMsg;
    }

    private String changeEncode2UTF8(String originEncode, String originMsg) throws CharacterCodingException,
            UnsupportedEncodingException {
        Charset gbkCharset = Charset.forName(originEncode);
        CharsetDecoder decoder = gbkCharset.newDecoder();
        ByteBuffer byteBuf = ByteBuffer.wrap(originMsg.getBytes("GBK"));
        CharBuffer charBuf = decoder.decode(byteBuf);
        if (charBuf.array().length == 0) {
            return originMsg;
        }
        return new String(charBuf.array());
    }

    private String changeEncode2UTF8(String originEncode, byte[] info) throws CharacterCodingException,
            UnsupportedEncodingException {
        Charset gbkCharset = Charset.forName(originEncode);
        CharsetDecoder decoder = gbkCharset.newDecoder();
        ByteBuffer byteBuf = ByteBuffer.wrap(info);
        CharBuffer charBuf = decoder.decode(byteBuf);
        if (charBuf.array().length == 0) {
            return new String(info);
        }
        return new String(charBuf.array());
    }

    protected String getProperties(String key, String propFile) {
        ResourceBundle rb = null;
        if (this.isTestServer) {
            rb = ResourceBundle.getBundle(propFile + "Test");
        }
        else {
            rb = ResourceBundle.getBundle(propFile);
        }
        return rb.getString(key);
    }

    public boolean isTestServer() {
        return this.isTestServer;
    }

    public void setTestServer(boolean isTestServer) {
        this.isTestServer = isTestServer;
    }

    public boolean isConvertEncode() {
        return this.isConvertEncode;
    }

    public void setConvertEncode(boolean isConvertEncode) {
        this.isConvertEncode = isConvertEncode;
    }

    public String getVerInfo() {
        return this.verInfo;
    }

    public void setVerInfo(String verInfo) {
        this.verInfo = verInfo;
    }

    public static void main(String[] args) {
        String ss = "OrderNo=20091016171050&MerPayNo=0123456789020091016171050&PayNo=34860636&PayAmount=100&CurrCode=156&RespCode=00&SettDate=20091016&Reserved01=&Reserved02=";

        Security s = new Security();
        String certNo = "360731198601225555";
        String merId = "121";
        String orderNo = null;
        String orderTime = "2009-10-26 18:02:30";
        String merPayNo = null;
        String orderAmount = "7";
        String couponFlag = "0";
        String currCode = "156";
        String callBackUrl = "http://61.144.61.86:9080/EEC_Mod/test/getResult.jsp";
        String resultMode = "0";
        String orderType = "01";
        String langType = "01";
        String reserved01 = "";
        String reserved02 = "";

        List sm = new ArrayList();
        List odn = new ArrayList();
        List mpn = new ArrayList();
        for (int i = 1; i < 101; i++) {
            orderNo = addZero(i + "", 25);
            merPayNo = orderNo;
            StringBuffer sb = new StringBuffer();
            sb.append("CertNo=").append(certNo);
            sb.append("&MerId=").append(merId);
            sb.append("&OrderNo=").append(orderNo);
            sb.append("&OrderTime=").append(orderTime);
            sb.append("&MerPayNo=").append(merPayNo);
            sb.append("&OrderAmount=").append(orderAmount);
            sb.append("&CouponFlag=").append(couponFlag);
            sb.append("&CurrCode=").append(currCode);
            sb.append("&CallBackUrl=").append(callBackUrl);
            sb.append("&ResultMode=").append(resultMode);
            sb.append("&OrderType=").append(orderType);
            sb.append("&LangType=").append(langType);
            sb.append("&Reserved01=").append(reserved01);
            sb.append("&Reserved02=").append(reserved02);
            s.signMsg(sb.toString(), "c:/key/merctest.pfx", "12345678");

            sm.add(s.getLastSignMsg());
            odn.add(orderNo);
            mpn.add(merPayNo);
        }
        for (int i = 0; i < sm.size(); i++) {
            System.out.println(sm.get(i).toString());
        }
        System.out.println();
        System.out.println();
        for (int i = 0; i < sm.size(); i++) {
            System.out.println(odn.get(i).toString());
        }
    }

    public static String addZero(String src, int length) {
        if ((src != null) && (src.length() >= length)) {
            return src.substring(0, length);
        }
        StringBuffer result = new StringBuffer();
        for (int i = 0; (src != null) && (i < length - src.length()); i++) {
            result.append("0");
        }
        result.append(src);

        return result.toString();
    }
}
