package com.ccservice.b2b2c.policy;

import java.rmi.RemoteException;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.axis2.AxisFault;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import client.OpenApiStub;

import com.ccservice.b2b2c.atom.hotel.WriteLog;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.test.HttpClient;

public class Air380Method extends SupplyMethod {
    //Log logger = LogFactory.getLog(ThisNodeTest.class);

    public static void main(String[] args) {
        System.out.println("begin");
        getAir380PolicyList();
    }

    /**Air380-2.12全量政策接口
     * 
     * 
     * @return
     * @time 2015年4月30日 下午7:09:01
     * @author guozhengju
     */
    public static List<Zrate> getAir380PolicyList() {
        List<Zrate> zrates = new ArrayList<Zrate>();

        try {
            OpenApiStub open = new OpenApiStub();
            StringBuffer stb = new StringBuffer();
            String charset = "utf-8";
            StringBuffer parameter = new StringBuffer();
            parameter.append("<policyfile>");
            parameter.append("<lastupdatetime/>");
            parameter.append("</policyfile>");
            String partner = "2012030902000027";
            String service = "policyfile";
            String signtype = "md5";
            String ver = "1.0";
            String cmlhead = "1";
            String keyString = "3a0e0ac6bc56430faa7da89a48d7fbfb";
            //签名MD5
            //MD5(“charset=”+charset+”&parameter=”+ parameter+”&partner=”+ partner+”&service=”+ service+”&ver=”+ver+” &xmlhead”+xmlhead+key,charset)
            String input = "cmlhead=" + cmlhead + "&partner=" + partner + "&parameter=" + parameter + "&service="
                    + service + "&ver=" + ver + keyString;
            String sign = "";
            try {
                sign = HttpClient.MD5(input);
            }
            catch (NoSuchAlgorithmException e1) {
                e1.printStackTrace();
            }

            stb.append("<openapi>");
            stb.append("<charset><![CDATA[" + charset + "]]></charset>");
            stb.append("<parameter><![CDATA[" + parameter + "]]></parameter>");
            stb.append("<partner><![CDATA[" + partner + "]]></partner>");
            stb.append("<service><![CDATA[" + service + "]]></service>");
            stb.append("<sign><![CDATA[" + sign + "]]></sign>");
            stb.append("<signtype><![CDATA[" + signtype + "]]></signtype>");
            stb.append("<ver><![CDATA[" + ver + "]]></ver>");

            stb.append("<xmlhead><![CDATA[" + cmlhead + "]]></xmlhead>");
            stb.append("</openapi>");

            OpenApiStub.FileSubmit fileSubmit = new OpenApiStub.FileSubmit();
            System.out.println("stb=" + stb.toString());

            fileSubmit.setXmlData(stb.toString());
            OpenApiStub.FileSubmitResponse response = open.fileSubmit(fileSubmit);
            String result = response.getError();
            System.out.println("result=" + result);

            if (result.length() < 10) {
                WriteLog.write("Air380全量政策:", "getAir380PolicyList" + result);
                return zrates;
            }
            Document document = DocumentHelper.parseText(result);
            Element element = document.getRootElement();

            if (element.elements("Response").size() > 0) {
                List<Element> list = element.elements("Response");
                System.out.println("listsize=" + list.size());
                for (Element e : list) {
                    Zrate zrate = new Zrate();
                    zrate.setId(Integer.parseInt(e.attributeValue("Outid")));//id政策ID 起止城+航班+舱+起飞日
                    zrate.setAircompanycode(e.attributeValue("Agentcode"));//航空公司二字码
                    zrate.setDepartureport(e.attributeValue("Departureport"));//出发城市三字码
                    zrate.setArrivalport(e.attributeValue("Arrivalport"));//到达城市三字码
                    zrate.setFlightnumber(e.attributeValue("Flightnumber"));//适用航班
                    //共享航班？？？zreta.setShare();
                    zrate.setCabincode(e.attributeValue("Cabincode"));//适用舱位
                    //cabinprice票面价？？zrate.setCabinprice();
                    zrate.setRatevalue(Float.parseFloat(e.attributeValue("Ratevalue")));//返点
                    zrate.setIsauto(Long.parseLong(e.attributeValue("Isauto")));//自动出票

                    DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    //format.setLenient(false);
                    try {
                        Timestamp ts = new Timestamp(format.parse(e.attributeValue("CreateTime")).getTime());
                        zrate.setCreatetime(ts);//政策生成时间
                        //起飞时间DepTime？？
                        Timestamp fsdate = new Timestamp(format.parse(e.attributeValue("Begindate")).getTime());
                        zrate.setCreatetime(fsdate);//旅行(乘机)开始时间
                        Timestamp fedate = new Timestamp(format.parse(e.attributeValue("Enddate")).getTime());
                        zrate.setCreatetime(fedate);//旅行(乘机)结束时间
                        Timestamp ssdate = new Timestamp(format.parse(e.attributeValue("Issuedstartdate")).getTime());
                        zrate.setBegindate(ssdate);//销售(出票)开始时间
                        Timestamp sedate = new Timestamp(format.parse(e.attributeValue("Issuedendate")).getTime());
                        zrate.setBegindate(sedate);//销售(出票)结束时间

                    }

                    catch (ParseException e1) {
                        e1.printStackTrace();
                    }
                    zrate.setRemark(e.attributeValue("Remark"));//政策备注
                    zrates.add(zrate);

                }
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e1) {
            e1.printStackTrace();
        }
        catch (DocumentException e1) {
            e1.printStackTrace();
        }
        return zrates;
    }
}
