package com.ccservice.b2b2c.policy.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import com.ccservice.b2b2c.base.passenger.Passenger;
import com.ccservice.b2b2c.base.segmentinfo.Segmentinfo;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.Piaomeng;

public class PiaomengZrateThread implements Callable<List<Zrate>> {

    private List<Segmentinfo> listSinfo;

    List<Passenger> listPassenger;

    public PiaomengZrateThread() {
    }

    public PiaomengZrateThread(List<Segmentinfo> listSinfo, List<Passenger> listPassenger) {
        this.listSinfo = listSinfo;
        this.listPassenger = listPassenger;
    }

    @Override
    public List<Zrate> call() throws Exception {
        List<Zrate> piaomengzrates = new ArrayList<Zrate>();
        try {
            piaomengzrates = Piaomeng.getPlicyByVoyage(listSinfo, listPassenger);
        }
        catch (Exception e) {
        }
        return piaomengzrates;
    }
}
