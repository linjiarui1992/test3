package com.ccservice.b2b2c.policy.thread;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;

import com.ccservice.b2b2c.base.orderinfo.Orderinfo;
import com.ccservice.b2b2c.base.passenger.Passenger;
import com.ccservice.b2b2c.base.segmentinfo.Segmentinfo;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.SupplyMethod;
import com.ccservice.b2b2c.policy.ben.TianQubook;
import com.ccservice.inter.job.WriteLog;

public class TianQuZratebyBack extends SupplyMethod implements Callable<List<Zrate>> {

    static TianQubook tqbook = new TianQubook();

    String scity;

    String ecity;

    String sdate;

    String flightnumber;

    String cabin;

    public TianQuZratebyBack() {
    }

    public TianQuZratebyBack(String scity, String ecity, String sdate, String flightnumber, String cabin) {
        this.scity = scity;
        this.ecity = ecity;
        this.sdate = sdate;
        this.flightnumber = flightnumber;
        this.cabin = cabin;
    }

    public TianQuZratebyBack(Orderinfo order, List<Segmentinfo> listSinfo, List<Passenger> listPassenger) {
        Segmentinfo sinfo = listSinfo.get(0);
        this.scity = sinfo.getStartairport();
        this.ecity = sinfo.getEndairport();
        if (sinfo.getDepartureDate() == null) {
            this.sdate = new SimpleDateFormat("yyyy-MM-dd").format(sinfo.getDeparttime());
        }
        else {
            this.sdate = sinfo.getDepartureDate();
        }
        this.flightnumber = sinfo.getFlightnumber();
        this.cabin = sinfo.getCabincode();
    }

    @Override
    public List<Zrate> call() {
        List<Zrate> localzrates = new ArrayList<Zrate>();
        int tempRandom = new Random().nextInt(1000);
        try {
            Long t = System.currentTimeMillis();
            WriteLog.write("FindZrateBytianqu_time", this.getClass().getSimpleName(), tempRandom + ":" + scity + ":"
                    + ecity + ":" + sdate + ":" + flightnumber + ":" + cabin + ":" + tqbook.getRequrl() + ":"
                    + tqbook.getAgentid().intValue());
            localzrates = getZrateByFlightNumberbyDBusewhere(scity, ecity, sdate, flightnumber, cabin,
                    tqbook.getRequrl(), tqbook.getAgentid().intValue() + "");
            t = System.currentTimeMillis() - t;
            WriteLog.write("FindZrateBytianqu_time", this.getClass().getSimpleName(), tempRandom + ":size:"
                    + localzrates.size());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return localzrates;

    }

    //    public static void main(String[] args) {
    //        List<Zrate> localzrates = SupplyMethod.getZrateByFlightNumberbyDBusewhere("PEK", "CAN", "2014-11-11", "CZ3110",
    //                "Y", "http://118.145.3.41:8780", "16");
    //        System.out.println(localzrates);
    //    }

}
