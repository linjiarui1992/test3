package com.ccservice.b2b2c.policy.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.JinriMethod;

/**
 * 
 * @author 栋 2013-6-28 19:24:15
 */
public class JinriZratebyFlightNumThread implements Callable<List<Zrate>> {

	String scity;
	String ecity;
	String sdate; 
	String flightnumber; 
	String cabin;

	public JinriZratebyFlightNumThread() {
	}

	public JinriZratebyFlightNumThread(String scity, String ecity,
			String sdate, String flightnumber, String cabin) {
		this.scity=scity;
		this.ecity=ecity;
		this.sdate=sdate;
		this.flightnumber=flightnumber;
		this.cabin=cabin;
	}

	@Override
	public List<Zrate> call() throws Exception {
		List<Zrate> jinriZrates = new ArrayList<Zrate>();
		try {
			jinriZrates = JinriMethod.getZrateByFlightNumber(scity, ecity,
					sdate, flightnumber, cabin);
		} catch (Exception e) {
		}
		return jinriZrates;
	}
}
