package com.ccservice.b2b2c.policy.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import com.ccservice.b2b2c.base.orderinfo.Orderinfo;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.IwanttogotowhichMethod;

public class IwanttogotowhichZratebyFlightNumThread implements Callable<List<Zrate>> {

	private Orderinfo order;

	public IwanttogotowhichZratebyFlightNumThread() {
	}

	public IwanttogotowhichZratebyFlightNumThread(Orderinfo order, int special) {
		this.order = order;
	}

	@Override
	public List<Zrate> call() throws Exception {
		List<Zrate> iwanttogotowhichZrates = new ArrayList<Zrate>();
		try {
			iwanttogotowhichZrates = IwanttogotowhichMethod.getZrateByPNR(order.getUserRtInfo(), order
					.getPnrpatinfo());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return iwanttogotowhichZrates;
	}
}
