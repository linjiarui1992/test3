package com.ccservice.b2b2c.policy.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import com.ccservice.b2b2c.base.segmentinfo.Segmentinfo;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.FiveoneBookutil;

/**
 * 
 * @author 贾建磊
 * 
 */
public class FiveOneV3ZratebyFlightNumThread implements Callable<List<Zrate>> {

    String scity;

    String ecity;

    String sdate;

    String flightnumber;

    String cabin;

    public FiveOneV3ZratebyFlightNumThread() {}

    public FiveOneV3ZratebyFlightNumThread(String scity, String ecity, String sdate, String flightnumber, String cabin) {
        this.scity = scity;
        this.ecity = ecity;
        this.sdate = sdate;
        this.flightnumber = flightnumber;
        this.cabin = cabin;
    }

    @Override
    public List<Zrate> call() throws Exception {
        List<Zrate> fiveOneV3Zrates = new ArrayList<Zrate>();
        try {
            Segmentinfo segmentinfo = new Segmentinfo();
            segmentinfo.setDepartureCtiy(scity.trim());
            segmentinfo.setArriveCity(ecity.trim());
            segmentinfo.setDepartureDate(sdate.trim());
            segmentinfo.setFlightnumber(flightnumber.trim());
            segmentinfo.setCabincode(cabin.trim());
            fiveOneV3Zrates = FiveoneBookutil.getPolicyAndFareInfoByFlights(segmentinfo, "1", "");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return fiveOneV3Zrates;
    }
}
