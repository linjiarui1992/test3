package com.ccservice.b2b2c.policy.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.SupplyMethod;
import com.ccservice.inter.job.WriteLog;

/**
 * 8000yi从数据库取政策
 * @author 栋 2013-6-28 19:27:14
 *
 */
public class EightZratebyback extends SupplyMethod implements Callable<List<Zrate>> {

    String scity;

    String ecity;

    String sdate;

    String flightnumber;

    String cabin;

    public EightZratebyback() {
    }

    public EightZratebyback(String scity, String ecity, String sdate, String flightnumber, String cabin) {
        this.scity = scity;
        this.ecity = ecity;
        this.sdate = sdate;
        this.flightnumber = flightnumber;
        this.cabin = cabin;
    }

    @Override
    public List<Zrate> call() throws Exception {
        List<Zrate> eightZrates = new ArrayList<Zrate>();
        String url = getSysconfigString("8000yizrateurl");
        Long t = System.currentTimeMillis();
        WriteLog.write("FindZrateByFlightNumber_time", this.getClass().getSimpleName(), t + "");

        try {
            //            Thread.sleep(100000L);
            //			int tempint = new Random().nextInt(1000);
            //            eightZrates = SupplyMethod.getZrateByFlightNumber(scity, ecity, sdate, flightnumber, cabin, url);
            eightZrates = getZrateByFlightNumberbyDBusewhere(scity, ecity, sdate, flightnumber, cabin, url);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        t = System.currentTimeMillis() - t;
        WriteLog.write("FindZrateByFlightNumber_time", this.getClass().getSimpleName(), t + "");
        url = null;
        t = null;
        return eightZrates;
    }
}
