package com.ccservice.b2b2c.policy.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.Airutil;

/**
 * 
 * @author 栋 2013-6-28 19:27:14
 *
 */
public class EightZratebyFlightNumThread implements Callable<List<Zrate>> {

	String scity;
	String ecity;
	String sdate;
	String flightnumber;
	String cabin;

	public EightZratebyFlightNumThread() {
	}

	public EightZratebyFlightNumThread(String scity, String ecity,
			String sdate, String flightnumber, String cabin) {
		this.scity = scity;
		this.ecity = ecity;
		this.sdate = sdate;
		this.flightnumber = flightnumber;
		this.cabin = cabin;
	}

	@Override
	public List<Zrate> call() throws Exception {
		List<Zrate> eightZrates = new ArrayList<Zrate>();
		try {
			eightZrates = Airutil.getZrateByFlightNumber(scity, ecity, sdate, flightnumber, cabin);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return eightZrates;
	}
}
