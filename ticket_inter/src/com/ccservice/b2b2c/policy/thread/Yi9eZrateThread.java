package com.ccservice.b2b2c.policy.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import com.ccservice.b2b2c.base.orderinfo.Orderinfo;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.Yi9eMethod;

/**
 * @author 陈栋
 */
public class Yi9eZrateThread implements Callable<List<Zrate>> {

    private Orderinfo order;

    public Yi9eZrateThread() {
    }

    public Yi9eZrateThread(Orderinfo order) {
        this.order = order;
    }

    @Override
    public List<Zrate> call() throws Exception {
        List<Zrate> yi9eZrates = new ArrayList<Zrate>();
        try {
            yi9eZrates = Yi9eMethod.getBestZratebyPnr_Note(order, 1);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        //        System.out.println(yi9eZrates.size());
        return yi9eZrates;
    }
}
