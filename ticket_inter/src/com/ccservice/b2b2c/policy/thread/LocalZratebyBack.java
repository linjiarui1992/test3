package com.ccservice.b2b2c.policy.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import com.ccservice.b2b2c.base.orderinfo.Orderinfo;
import com.ccservice.b2b2c.base.passenger.Passenger;
import com.ccservice.b2b2c.base.segmentinfo.Segmentinfo;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.SupplyMethod;
import com.ccservice.inter.job.WriteLog;

/**
 * 获取自己的本地或者远程政策
 * @author 陈栋
 * 
 */
public class LocalZratebyBack extends SupplyMethod implements Callable<List<Zrate>> {

    String scity;

    String ecity;

    String sdate;

    String flightnumber;

    String cabin;

    String zrateUrl;

    int type = 0;

    public LocalZratebyBack() {
    }

    public LocalZratebyBack(String scity, String ecity, String sdate, String flightnumber, String cabin,
            String zrateUrl) {
        this.scity = scity;
        this.ecity = ecity;
        this.sdate = sdate;
        this.flightnumber = flightnumber;
        this.cabin = cabin;
        this.zrateUrl = zrateUrl;
    }

    public LocalZratebyBack(Orderinfo order, List<Segmentinfo> listSinfo, List<Passenger> listPassenger) {
        Segmentinfo sinfo = listSinfo.get(0);
        this.scity = sinfo.getStartairport();
        this.ecity = sinfo.getEndairport();
        this.sdate = sinfo.getDeparttime().toString().substring(0, 10);
        this.flightnumber = sinfo.getFlightnumber();
        this.cabin = sinfo.getCabincode();
        this.type = 1;
    }

    @Override
    public List<Zrate> call() throws Exception {
        List<Zrate> localzrates = new ArrayList<Zrate>();
        try {
            zrateUrl = getSysconfigString("localzrateurl");
            //            System.out.println("LocalZratebyBack:" + zrateUrl);
            if (this.type == 1) {
                WriteLog.write("LocalZratebyBack_call",
                        scity + ":" + ecity + ":" + sdate + ":" + flightnumber + ":" + cabin + ":" + zrateUrl);
            }
            localzrates = getZrateByFlightNumberbyDBusewhere(scity, ecity, sdate, flightnumber, cabin, zrateUrl);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return localzrates;
    }

    //    public static void main(String[] args) {
    //        List<Zrate> localzrates = getZrateByFlightNumberbyDBusewhere("PEK", "INC", "2014-06-27", "CZ6117", "F",
    //                "http://118.145.3.41:8680");
    //        System.out.println(localzrates);
    //    }

}
