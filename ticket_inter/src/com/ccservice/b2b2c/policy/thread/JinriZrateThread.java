package com.ccservice.b2b2c.policy.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import com.ccservice.b2b2c.base.orderinfo.Orderinfo;
import com.ccservice.b2b2c.base.passenger.Passenger;
import com.ccservice.b2b2c.base.segmentinfo.Segmentinfo;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.JinriMethod;
import com.ccservice.inter.server.Server;

/**
 * 
 * @author 栋
 *
 */
public class JinriZrateThread implements Callable<List<Zrate>> {
    private Orderinfo order;

    private List<Passenger> listPassenger;

    private Segmentinfo sinfo;

    public JinriZrateThread() {
    }

    public JinriZrateThread(Orderinfo order, List<Passenger> listPassenger, Segmentinfo sinfo) {
        this.order = order;
        this.listPassenger = listPassenger;
        this.sinfo = sinfo;
    }

    @Override
    public List<Zrate> call() throws Exception {
        List<Zrate> jinriZrates = new ArrayList<Zrate>();
        JinriMethod jinriMethod = new JinriMethod();
        if (Server.getInstance().getCreatePnr().equals("1")) {
            jinriZrates = jinriMethod.getBestZrateByPnr(order, listPassenger, sinfo);
        }
        else {
            jinriZrates = jinriMethod.getBestZrate(sinfo);
        }
        return jinriZrates;
    }
}