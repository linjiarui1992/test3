package com.ccservice.b2b2c.policy;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.base.orderinfo.Orderinfo;
import com.ccservice.b2b2c.base.passenger.Passenger;
import com.ccservice.b2b2c.base.segmentinfo.Segmentinfo;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.ben.QunarBestBook;

/**
 * 
 * 
 * @time 2014年10月23日 上午10:01:31
 * @author wzc
 * 去哪批发接口 v1.0
 */
public class QunarBestDataV2 {
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    static QunarBestBook qunar = new QunarBestBook();

    /**
     * 
     * 
     * @param searchDepartureAirport  出发城市3字码
     * @param searchArrivalAirport 到达城市3字码
     * @param searchDepartureTime 航班日期，yyyy-mm-dd
     * @param flightnum 航班号
     * @param carbin 舱位字母大写，支持子舱位
     * @return
     * @time 2014年10月23日 上午10:38:18
     * @author wzc
     * 查询信息
     */
    public static List<Zrate> getQunarFlightinfozrate(String searchDepartureAirport, String searchArrivalAirport,
            String searchDepartureTime, String flightnum, String carbin) {
        List<Zrate> zrates = null;
        String url = "";
        try {
            String param = "dptCode=" + URLEncoder.encode(searchDepartureAirport, "utf-8") + "&arrCode="
                    + URLEncoder.encode(searchArrivalAirport, "utf-8") + "&dptDate="
                    + URLEncoder.encode(searchDepartureTime, "utf-8") + "&flightNum="
                    + URLEncoder.encode(flightnum, "utf-8") + "&cabin=" + URLEncoder.encode(carbin, "utf-8")
                    + "&vendorID=" + qunar.getVendorID();
            url = qunar.getQunarurl() + "/b2b_flight_search?" + param;
            WriteLog.write("qunarbestv2", url);
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String result = SendPostandGet.submitGet(url, "UTF-8");
        WriteLog.write("qunarbestv2", result);
        zrates = getLastData(result);
        System.out.println(zrates.size());
        return zrates;

    }

    /**
     * 解析返回最终集合数据
     * 
     * @return
     */
    public static List<Zrate> getLastData(String result) {
        List<Zrate> zrates = parseQunarData(result);
        return zrates;
    }

    /**
     * 解析去哪返回数据
     * 
     * @return
     */
    public static List<Zrate> parseQunarData(String result) {
        List<Zrate> infos = new ArrayList<Zrate>();
        if (!"".equals(result)) {
            JSONObject jsonobj = JSONObject.fromObject(result);
            String status = jsonobj.getString("status");// 成功码 1（成功） 、0（失败）
            if (!"1".equals(status)) {
                return infos;
            }
            String track_id = jsonobj.getString("track_id");// 交易码
            JSONArray data = jsonobj.getJSONArray("data");
            if (data.size() > 0) {
                for (int i = 0; i < data.size(); i++) {
                    try {
                        JSONObject info = data.getJSONObject(i);
                        String cabin = info.getString("cabin");//仓位
                        String oriPrice = info.getString("oriPrice");//舱位价
                        String discountPrice = info.getString("discountPrice");//折扣价,也就是实际价格
                        String workTime = info.getString("workTime");//工作时间
                        String invalidTicketTime = info.getString("invalidTicketTime");//废票时间
                        String ticketEff = info.getString("ticketEff");//出票时间
                        Zrate zrate = new Zrate();

                        String pid = info.getString("pid");//政策pid  政策FID       --
                        String clientId = info.getString("clientId");//出票用代理商域名 --
                        String policyType = info.getString("policyType");
                        String policyId = info.getString("policyId");//政策id
                        String dptCitystr = info.getString("dptCity");//出发城市str
                        String arrCitystr = info.getString("arrCity");//到达城市str
                        String distance = info.getString("distance");//里程
                        String airlineCompany = info.getString("airlineCompany");//获取航空公司名称

                        zrate.setClientId(clientId);
                        zrate.setPolicyId(Long.valueOf(policyId));
                        zrate.setDptCitystr(dptCitystr);
                        zrate.setArrCitystr(arrCitystr);
                        zrate.setDistance(Integer.valueOf(distance));
                        zrate.setPolicyType(policyType);
                        zrate.setAirlineCompany(airlineCompany);

                        zrate.setAgentid(17L);
                        zrate.setCreateuser("qunar(v2.0)");
                        zrate.setModifyuser("qunar(v2.0)");
                        zrate.setIsenable(1);//可用
                        zrate.setOutid(pid);//政策pid
                        zrate.setAircompanycode("");
                        zrate.setCabincode(cabin);
                        zrate.setRatevalue(getRateValue(oriPrice, discountPrice));
                        zrate.setTotalprice(Float.valueOf(discountPrice));
                        zrate.setTickettype(2);//1 B2B  2BSP
                        if (workTime != null && workTime.contains("-")) {
                            String[] str = workTime.split("-");
                            if (str != null && str.length > 0) {
                                zrate.setWorktime(str[0].trim());
                                zrate.setAfterworktime(str[1].trim());
                            }
                        }
                        else {
                            zrate.setWorktime("-");
                            zrate.setAfterworktime("-");
                        }
                        if (invalidTicketTime != null && invalidTicketTime.contains("-")) {
                            String[] str = invalidTicketTime.split("-");
                            if (str != null && str.length > 0) {
                                zrate.setOnetofivewastetime(str[1].trim());
                                zrate.setWeekendwastetime(str[1].trim());
                            }
                        }
                        else {
                            zrate.setOnetofivewastetime("-");
                            zrate.setWeekendwastetime("-");
                        }
                        zrate.setSpeed(ticketEff);
                        zrate.setGeneral(1l);//去哪
                        zrate.setVoyagetype("1");
                        zrate.setRemark("换编码出票，跨越改签收代理费，差价不退还");
                        infos.add(zrate);
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return infos;
    }

    /**
     * 
     * urf8编码  异常返回空字符串
     * @param msg
     * @return
     * @time 2014年10月24日 上午11:06:59
     * @author wzc
     */
    public static String getEncodeUtf8(String msg) {
        try {
            //return msg;
            return URLEncoder.encode(msg, "UTF-8");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void main(String[] args) {
        Orderinfo order = new Orderinfo();
        order.setExtorderid("asdfasdfasf");
        Segmentinfo segmentinfo = new Segmentinfo();
        segmentinfo.setStartairport("pek");
        segmentinfo.setEndairport("cgo");
        List<Passenger> passengers = new ArrayList<Passenger>();
        Passenger p = new Passenger();
        p.setName("wzc");
        p.setTicketnum("23441234123");
        p.setIdnumber("aksldjfalsdkjfalksdfj");
        p.setIdtype(1);
        p.setPtype(1);
        Passenger p1 = new Passenger();
        p1.setPtype(1);
        p1.setName("wzc111");
        p1.setTicketnum("2344123411111123");
        p1.setIdnumber("aksldjfalsdkjfalksdfj");
        p1.setIdtype(1);
        passengers.add(p1);
        passengers.add(p);
        String tpType = "aslkdjfalsk";
        tuifeiorder(order, segmentinfo, passengers, tpType);
    }

    /**
    *
    * 去哪退票接口 
    * @param order
    * @param segmentinfo
    * @param tpType
    * @return
    * @time 2014年11月22日 下午6:37:33
    * @author wzc
    */
    public static String tuifeiorder(Orderinfo order, Segmentinfo segmentinfo, List<Passenger> passengers, String tpType) {
        String resultmsg = "-1";
        String url = "";
        try {
            String param = "username=" + URLEncoder.encode(qunar.getRegistname(), "utf-8") + "&sourceOrderNo="
                    + URLEncoder.encode(order.getExtorderid(), "utf-8") + "&emergencyTel="
                    + URLEncoder.encode(qunar.getLinkmobile(), "utf-8") + "&codeDealType="
                    + URLEncoder.encode("BY_PLATFORM", "utf-8") + "&tpType=" + URLEncoder.encode(tpType, "utf-8")
                    + "&vendorID=" + qunar.getVendorID() + "&flightSegment=" + segmentinfo.getStartairport() + "-"
                    + segmentinfo.getEndairport() + "&comment=";
            for (int i = 0; i < passengers.size(); i++) {
                Passenger p = passengers.get(i);
                String cardType = "NI";
                if (p.getIdtype() == 1) {// 身份证
                    cardType = "NI";
                }
                else if (p.getIdtype() == 3) {// 护照
                    cardType = "PP";
                }
                else {
                    cardType = "ID";// 其他
                }
                String ageType = "ADULT";// 0 表示成人 1 表示儿童
                if (p.getPtype() == 1) {
                    ageType = "ADULT";
                }
                else if (p.getPtype() == 2) {//儿童
                    ageType = "CHILD";
                }
                else if (p.getPtype() == 3) {//婴儿
                    ageType = "BABY";
                }
                param += "&passengers[" + i + "].name=" + URLEncoder.encode(p.getName(), "utf-8");
                param += "&passengers[" + i + "].passengerType=" + URLEncoder.encode(ageType, "utf-8");
                param += "&passengers[" + i + "].cardType=" + URLEncoder.encode(cardType, "utf-8");
                param += "&passengers[" + i + "].cardNo=" + URLEncoder.encode(p.getIdnumber(), "utf-8");
                param += "&passengers[" + i + "].ticketNo=" + URLEncoder.encode(p.getTicketnum(), "utf-8");
            }
            url = qunar.getQunarurl() + "/b2b_apply_refund?" + param;
            WriteLog.write("qunarbestv2", url);
            StringBuffer resultTt = SendPostandGet.submitPost(url, param, "UTF-8");
            String resultt = resultTt == null ? "" : resultTt.toString();
            WriteLog.write("qunarbestv2", resultt);
            if (!"".equals(resultt)) {
                JSONObject obj = JSONObject.fromObject(resultt);
                String state = obj.getString("status");
                String msg = obj.getString("msg");
                if ("1".equals(state)) {
                    resultmsg = "1";
                }
                else {
                    resultmsg = "-1";
                }
            }
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return resultmsg;
    }

    /**
     * 去哪创建订单
     * 
     * @param supplyurl
     *            qunar供应商地址
     * @param bookingtab
     *            价格加密字符串
     */
    public static String Qunarcreateorder(Orderinfo order, Segmentinfo sinfo, List<Passenger> listPassenger) {
        String result = "";
        String quserName = qunar.getRegistname();// 在 qunar 注册的用户名
        String clientId = sinfo.getClientId();
        String contactMob = qunar.getLinkmobile();// 联系人手机
        String officeID = "";// "PEK242";//原代理商officeID
        int sumprice = (int) ((sinfo.getRealPrice() + sinfo.getFuelfee() + sinfo.getAirportfee()) * listPassenger
                .size());
        String purchasePrice = sumprice + "";//采购总价--
        // 的值）
        String url = "";
        try {
            String policyFid = order.getExtpolicyid();//政策FID --查询接口的pid
            String policyType = sinfo.getPolicyType();//政策类型  --查询接口的policy
            String pnr = "";//order.getPnr();//成人pnr

            String param = "officeId=" + getEncodeUtf8(officeID) + "&vendorID=" + qunar.getVendorID() + "&clientId="
                    + getEncodeUtf8(clientId) + "&purchasePrice=" + getEncodeUtf8(purchasePrice) + "&policyFid="
                    + getEncodeUtf8(policyFid) + "&policyType=" + getEncodeUtf8(policyType) + "&pnr="
                    + getEncodeUtf8(pnr) + "&contactTel=" + getEncodeUtf8(contactMob) + "&quserName="
                    + getEncodeUtf8(quserName);
            String flightNum = sinfo.getFlightnumber();//航班号
            String airlineCompany = sinfo.getAircompanyname();//航空公司
            String cabin = sinfo.getCabincode().toUpperCase();//cabin仓位 大写，比如：Y
            String departureDate = TimeUtil.gettodaydate(1, sinfo.getDeparttime());//出发日期  2014-09-19
            String departureAirport = sinfo.getStartairport().toUpperCase();//出发机场 大写，三字码
            String arrivalAirport = sinfo.getEndairport().toUpperCase();//到达机场 三字码
            String departureCity = sinfo.getDepartureCtiy();//出发城市 汉字
            String arrivalCity = sinfo.getArriveCity();//到达城市 汉字
            String departureTime = TimeUtil.gettodaydate(5, sinfo.getDeparttime());//出发时间 10:00
            String arrivalTime = TimeUtil.gettodaydate(5, sinfo.getArrivaltime());//到达时间 12:00
            String price = sinfo.getYprice().intValue() + "";//int  Y舱价格
            String discount = sinfo.getDistance().intValue() + "";//里程 double
            String constructionFee = sinfo.getAirportfee().intValue() + "";//机场建设费 int
            String fuelTax = sinfo.getFuelfee().intValue() + "";//燃油费 int
            String printPrice = sinfo.getParvalue().intValue() + "";//票面价 int
            String realPrice = sinfo.getRealPrice().intValue() + "";//销售价 double 
            String policyId = sinfo.getPolicyId().longValue() + "";//查询接口的policyId
            String policySource = "biz_b2b";//可以不传
            //航段信息
            param += "&flights[0].flightNum=" + getEncodeUtf8(flightNum) + "&flights[0].airlineCompany="
                    + getEncodeUtf8(airlineCompany) + "&flights[0].cabin=" + getEncodeUtf8(cabin)
                    + "&flights[0].departureDate=" + URLEncoder.encode(departureDate, "UTF-8")
                    + "&flights[0].departureAirport=" + getEncodeUtf8(departureAirport) + "&flights[0].arrivalAirport="
                    + getEncodeUtf8(arrivalAirport) + "&flights[0].departureCity=" + getEncodeUtf8(departureCity)
                    + "&flights[0].arrivalCity=" + getEncodeUtf8(arrivalCity) + "&flights[0].departureTime="
                    + getEncodeUtf8(departureTime) + "&flights[0].arrivalTime=" + getEncodeUtf8(arrivalTime)
                    + "&flights[0].price=" + getEncodeUtf8(price) + "&flights[0].discount=" + getEncodeUtf8(discount)
                    + "&flights[0].constructionFee=" + getEncodeUtf8(constructionFee) + "&flights[0].fuelTax="
                    + getEncodeUtf8(fuelTax) + "&flights[0].printPrice=" + getEncodeUtf8(printPrice)
                    + "&flights[0].realPrice=" + getEncodeUtf8(realPrice) + "&flights[0].policyId="
                    + getEncodeUtf8(policyId) + "&flights[0].policySource=" + getEncodeUtf8(policySource)
                    + "&flights[0].policyFid=" + getEncodeUtf8(policyFid);
            //乘客信息
            // 祝明明 360428198810080134 测试乘客数据
            // 左海妮 130225198207282943
            if (listPassenger.size() > 0) {
                for (int i = 0; i < listPassenger.size(); i++) {
                    Passenger p = listPassenger.get(i);
                    String ageType = "0";// 0 表示成人 1 表示儿童
                    if (p.getPtype() == 1) {
                        ageType = "0";
                    }
                    else {
                        ageType = "1";
                    }
                    String cardType = "NI";
                    if (p.getIdtype() == 1) {// 身份证
                        cardType = "NI";
                    }
                    else if (p.getIdtype() == 3) {// 护照
                        cardType = "PP";
                    }
                    else {
                        cardType = "ID";// 其他
                    }
                    String birthday = "19900628";//19820728
                    String gender = "true";//性别，默认女false; 男true
                    String totalPrice = (int) (sinfo.getRealPrice() + sinfo.getFuelfee() + sinfo.getAirportfee()) + "";//订单价格
                    String tprice = sinfo.getRealPrice().intValue() + "";//机票价格
                    String priceTypeCode = "";//价格类型
                    param += "&passengers[" + i + "].name=" + getEncodeUtf8(p.getName()) + "&passengers[" + i
                            + "].cardType=" + getEncodeUtf8(cardType) + "&passengers[" + i + "].cardNum="
                            + getEncodeUtf8(p.getIdnumber()) + "&passengers[" + i + "].ageType="
                            + getEncodeUtf8(ageType) + "&passengers[" + i + "].birthday=" + getEncodeUtf8(birthday)
                            + "&passengers[" + i + "].totalPrice=" + getEncodeUtf8(totalPrice) + "&passengers[" + i
                            + "].gender=" + getEncodeUtf8(gender) + "&passengers[" + i + "].price="
                            + getEncodeUtf8(tprice) + "&passengers[" + i + "].priceTypeCode=1"
                            + getEncodeUtf8(priceTypeCode);
                }
            }
            url = qunar.getQunarurl() + "/b2b_createOrder";
            WriteLog.write("qunarbestv2", "POST:" + url + "?" + param);
            StringBuffer resultTt = SendPostandGet.submitPost(url, param, "UTF-8");
            String resultt = resultTt == null ? "" : resultTt.toString();
            WriteLog.write("qunarbestv2", resultt);
            if (!"".equals(resultt)) {
                JSONObject obj = JSONObject.fromObject(resultt);
                String state = obj.getString("status");
                if ("1".equals(state)) {
                    String orderNo = obj.getString("orderNo");//外部订单号
                    String id = obj.getString("orderId");//外部订单号id
                    //String orderStatus = obj.getString("orderStatus");//订单状态
                    result = "1|" + orderNo + "|" + id + "|1|" + purchasePrice;
                }
                else {
                    result = "-1|" + state;
                }
            }

        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * 
     * 
     * @param oriPrice 舱位价
     * @param discountPrice 折扣价,也就是实际价格
     * @return
     * @time 2014年10月23日 上午11:30:46
     * @author wzc
     */
    public static float getRateValue(String oriPrice, String discountPrice) {
        float ratevalue = 1.5f;
        try {
            Double rve = ((Double.valueOf(oriPrice) - Double.valueOf(discountPrice)) / Double.valueOf(oriPrice)) * 100;
            DecimalFormat format = null;
            format = (DecimalFormat) NumberFormat.getInstance();
            format.applyPattern("###0.0");
            String result = format.format(rve);
            ratevalue = Float.valueOf(result);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return ratevalue;
    }

    /**
     * 解析日期
     * 
     * @param timeinfo
     * @return
     */
    public static Timestamp format(String timeinfo) {
        try {
            Date date = sdf.parse(timeinfo);
            return new Timestamp(date.getTime());
        }
        catch (ParseException e) {

        }
        return null;
    }
}
