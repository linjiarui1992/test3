package com.ccservice.b2b2c.policy;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.base.service.IAirService;
import com.ccservice.b2b2c.base.zrate.Zrate;

public class CtripMethod {

	public static List<Zrate> getZrateByFlightNumber(String scity,
			String ecity, String sdate, String flightnumber, String cabin,
			String url2) {
		// TODO Auto-generated method stub
		//12代表是携程
		String where = "where C_AGENTID=12 and C_BEGINDATE = '" + sdate
				+ "' and C_DEPARTUREPORT='" + scity + "' and C_ARRIVALPORT='"
				+ ecity + "' and C_AIRCOMPANYCODE='"
				+ flightnumber.substring(0, 2) + "' and C_FLIGHTNUMBER='"
				+ flightnumber + "' and C_CABINCODE='" + cabin + "'";

		url2 = url2 + "/cn_service/service/";
		HessianProxyFactory factory = new HessianProxyFactory();
		List<Zrate> zrates = new ArrayList<Zrate>();
		try {
			IAirService iAirService = (IAirService) factory
					.create(IAirService.class,
							url2 + IAirService.class.getSimpleName());

			zrates = iAirService.findAllZrate(where, "ORDER BY ID DESC", -1, 0);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (zrates.size() == 0) {
			zrates = new ArrayList<Zrate>();
		}
		return zrates;
	}
}
