package com.ccservice.b2b2c.policy;

import com.ccservice.inter.job.WriteLog;

public class UtilMethod {

	/**
	 * 记录异常信息
	 * 
	 * @param className
	 * @param e
	 */
	public static void writeEx(String className, Exception e) {
		StackTraceElement[] StackTraceElement = e.getStackTrace();
		WriteLog.write("EX", e.getClass().getName());
		for (int i = 0; i < StackTraceElement.length; i++) {
			if (StackTraceElement[i].toString().indexOf(className) >= 0) {
				WriteLog.write("EX", StackTraceElement[i].toString());
			}
		}
	}
}
