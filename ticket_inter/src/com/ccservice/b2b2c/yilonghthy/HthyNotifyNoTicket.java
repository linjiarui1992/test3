package com.ccservice.b2b2c.yilonghthy;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.axis2.AxisFault;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.ccervice.util.db.DBHelper;
import com.ccervice.util.db.DataRow;
import com.ccervice.util.db.DataTable;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.yilong.JaxRpcCallbackServiceStub;
import com.ccservice.b2b2c.yilong.JaxRpcCallbackServiceStub.CtripCallback;
import com.ccservice.b2b2c.yilong.JaxRpcCallbackServiceStub.CtripCallbackResponse;
import com.yeexing.webservice.service.MD5Util;


/**
 * 出票失败
 * @author guozhengju
 *
 */
public class HthyNotifyNoTicket {
    public static void main(String[] args) {
        System.out.println(new HthyNotifyNoTicket().notifyNoTicket("22265"));
    }
    
    public String notifyNoTicket(String orderid) {
        String result="";
        String returnstatus="";
        try {
            JaxRpcCallbackServiceStub Stub=new JaxRpcCallbackServiceStub();
            CtripCallback ctripCallback = new CtripCallback();
            org.apache.axis2.databinding.types.soapencoding.String param = new org.apache.axis2.databinding.types.soapencoding.String();
            String xmlparam=getXml(orderid);
            WriteLog.write("Elong_出票失败请求艺龙request", "订单id:" + orderid+ ",请求艺龙信息:" + xmlparam);
            param.setString(xmlparam);
            ctripCallback.setXml(param);
            CtripCallbackResponse ctripCallbackResponse = Stub.ctripCallback(ctripCallback);
            org.apache.axis2.databinding.types.soapencoding.String ss=ctripCallbackResponse.getCtripCallbackReturn();
            result=ss+"";
            System.out.println(result);
            WriteLog.write("Elong_出票失败请求艺龙response","订单id:"+orderid+",艺龙返回信息:"+result);
        } catch (AxisFault e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        //锁单返回信息解析
        try {
            Document document = DocumentHelper.parseText(result);
            Element orderResponse = document.getRootElement();
            String serviceName=orderResponse.elementText("ServiceName");//接口服务名
            String operationDateTime=orderResponse.elementText("OperationDateTime");//操作日期
            String orderNumber=orderResponse.elementText("OrderNumber");//订单号
            String status=orderResponse.elementText("Status");//状态
            returnstatus=status;
            Element errorResponse=orderResponse.element("ErrorResponse");
            String errorMessage=errorResponse.elementText("ErrorMessage");//信息
            if("FAIL".equals(status)){
                String errorCode=errorResponse.elementText("ErrorCode");//ErrorCode
            }
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    return returnstatus;
    }
    public String getXml(String orderid) {
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //下单请求接口调用Response
        Document document=DocumentHelper.createDocument();
        Element orderProcessRequest=document.addElement("OrderProcessRequest");
        orderProcessRequest.addAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance");
        Element authentication=orderProcessRequest.addElement("Authentication");
        Element timeStamp=authentication.addElement("TimeStamp");
        String times=sdf.format(new Date());
        timeStamp.setText(times);
        Element serviceName=authentication.addElement("ServiceName");
        serviceName.setText("web.order.notifyNoTicket");
        Element messageIdentity=authentication.addElement("MessageIdentity");
        String message=getMessage(times, "web.order.notifyNoTicket", "hangtian111");
        messageIdentity.setText(message);
        Element partnerName=authentication.addElement("PartnerName");
        partnerName.setText("hangtian111");
        Element trainOrderService=orderProcessRequest.addElement("TrainOrderService");
        Element orderInfo=trainOrderService.addElement("OrderInfo");
        Element orderNumber=orderInfo.addElement("OrderNumber");
        String sql="SELECT OrderNumberOnline,refusereason,refusereasonstr FROM TrainOrderOffline WHERE id="+orderid;
        DataTable myDt = DBHelper.GetDataTable(sql);
        List<DataRow> list = myDt.GetRow();
        String reason="";
        String orderNumberOnline="";
        if(list.size()>0){
        	DataRow datarow = list.get(0);
        	String refusereason=datarow.GetColumnString("refusereason");
        	String refusereasonstr=datarow.GetColumnString("refusereasonstr");
        	orderNumberOnline=datarow.GetColumnString("OrderNumberOnline");
        	if("0".equals(refusereason)){
        		reason=refusereasonstr;
        	}else{
        		if("1".equals(refusereason)){
        			reason="所购买的车次坐席已无票";
        		}else if("3".equals(refusereason)){
        			reason="票价和12306不符";
        		}else if("4".equals(refusereason)){
        			reason="车次数据与12306不一致";
        		}else if("5".equals(refusereason)){
        			reason="乘客信息错误";
        		}else if("0".equals(refusereason)){
        			reason=refusereasonstr;
        		}
        	
        	}
        }
        orderNumber.setText(orderNumberOnline);
        Element noTicketReasons=orderInfo.addElement("NoTicketReasons");
        noTicketReasons.setText(reason);
        return document.asXML();
    }
    public static String getMessage(String times,String serviceName,String partName){
		String result=MD5Util.MD5(times+serviceName+partName);
		return result.toUpperCase();
	}
}
