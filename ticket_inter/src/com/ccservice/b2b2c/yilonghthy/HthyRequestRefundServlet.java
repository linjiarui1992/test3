package com.ccservice.b2b2c.yilonghthy;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ccservice.b2b2c.atom.component.WriteLog;

public class HthyRequestRefundServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public HthyRequestRefundServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String orderid=request.getParameter("orderid");
		String refundsum=request.getParameter("refundsum");
		System.out.println("差额退款请求orderID："+orderid);
		WriteLog.write("cn_home请求差额退款入口", "orderid="+orderid+";refundsum="+refundsum);
		new HthyRequestRefund().requestRefund(orderid,refundsum);
	}

}
