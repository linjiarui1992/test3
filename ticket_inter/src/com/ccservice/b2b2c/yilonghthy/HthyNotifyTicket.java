package com.ccservice.b2b2c.yilonghthy;

import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.axis2.AxisFault;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.ccervice.util.db.DBHelper;
import com.ccervice.util.db.DataRow;
import com.ccervice.util.db.DataTable;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.yilong.JaxRpcCallbackServiceStub;
import com.ccservice.b2b2c.yilong.JaxRpcCallbackServiceStub.CtripCallback;
import com.ccservice.b2b2c.yilong.JaxRpcCallbackServiceStub.CtripCallbackResponse;
import com.yeexing.webservice.service.MD5Util;


public class HthyNotifyTicket {
	public static void main(String[] args) {
		String ss=new HthyNotifyTicket().notifyTicket("29241");
		System.out.println("请求出票成功结果：："+ss);
//		String xmlparam=getXml("503");
//		System.out.println(xmlparam);
	}
	

    public  String notifyTicket(String orderid) {
        String returnorder="";
        String result="";
        try {
            JaxRpcCallbackServiceStub Stub=new JaxRpcCallbackServiceStub();
            CtripCallback ctripCallback = new CtripCallback();
            org.apache.axis2.databinding.types.soapencoding.String param = new org.apache.axis2.databinding.types.soapencoding.String();
            String xmlparam=getXml(orderid);
            System.out.println(xmlparam);
            WriteLog.write("Elong_出票成功请求艺龙request","订单id:"+orderid+",请求艺龙信息:"+xmlparam);
            param.setString(xmlparam);
            ctripCallback.setXml(param);
            CtripCallbackResponse ctripCallbackResponse = Stub.ctripCallback(ctripCallback);
            org.apache.axis2.databinding.types.soapencoding.String ss=ctripCallbackResponse.getCtripCallbackReturn();
            System.out.println(ss+"");
            result=ss+"";
            WriteLog.write("Elong_出票成功请求艺龙response","订单id:"+orderid+",艺龙返回信息:"+result);
        } catch (AxisFault e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        //锁单返回信息解析
        try {
            Document document = DocumentHelper.parseText(result);
            Element orderResponse = document.getRootElement();
            String serviceName=orderResponse.elementText("ServiceName");//接口服务名
            String operationDateTime=orderResponse.elementText("OperationDateTime");//操作日期
            String orderNumber=orderResponse.elementText("OrderNumber");//订单号
            String status=orderResponse.elementText("Status");//状态
            returnorder=status;
            Element errorResponse=orderResponse.element("ErrorResponse");
            String errorMessage=errorResponse.elementText("ErrorMessage");//信息
            if("FAIL".equals(status)){
                String errorCode=errorResponse.elementText("ErrorCode");//ErrorCode
                System.out.println(serviceName+"--"+operationDateTime+"--"+orderNumber+"--"+status+"--"+errorMessage+"--"+errorCode);
            }
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        return returnorder;
    }
    public static String getXml(String orderid) {    
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdf0=new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat sdf1=new SimpleDateFormat("HH:mm");
        SimpleDateFormat sdf2=new SimpleDateFormat("yyyy-MM-dd");
        //下单请求接口调用Response
        String sql="select * from TrainOrderOffline O left join TrainPassengerOffline P on o.Id  = P.OrderId left join TrainTicketOffline T on p.Id = T.TrainPid where  O.Id="+orderid;
        DataTable myDt = DBHelper.GetDataTable(sql);
        List<DataRow> list = myDt.GetRow();
        String ordernum="";
        String orderprice="";
        String fromstation="";
        String endstation="";
        String departtime="";
        String trainno="";
        String price="";
        String seattype="";
        String coach="";
        String seatno="";
        String birthday="";
        String ticketTypess="";
        String acceptseat="";
        if(list.size()>0){
        	DataRow datarow = list.get(0);
            ordernum=datarow.GetColumnString("OrderNumberOnline");
            orderprice=datarow.GetColumnString("OrderPrice");
            fromstation=datarow.GetColumnString("Departure");
            endstation=datarow.GetColumnString("Arrival");
            departtime=datarow.GetColumnString("DepartTime");
            trainno=datarow.GetColumnString("TrainNo");
            price=datarow.GetColumnString("Price");
            seattype=datarow.GetColumnString("SeatType");
            coach=datarow.GetColumnString("Coach");
            seatno=datarow.GetColumnString("SeatNo");
            birthday=datarow.GetColumnString("Birthday");
            acceptseat=datarow.GetColumnString("TradeNo");
            String ticketTypes=datarow.GetColumnString("TicketType");
            if("1".equals(ticketTypes)){
                ticketTypess="成人票";
            }else if("0".equals(ticketTypes)){
                ticketTypess="儿童票";
            }else if("2".equals(ticketTypes)){
                ticketTypess="学生票";
            }
        }
        Document document=DocumentHelper.createDocument();
        Element orderProcessRequest=document.addElement("OrderProcessRequest");
        orderProcessRequest.addAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance");
        
        Element authentication=orderProcessRequest.addElement("Authentication");
        Element timeStamp=authentication.addElement("TimeStamp");
        String times=sdf.format(new Date());
        timeStamp.setText(times);
        Element serviceName=authentication.addElement("ServiceName");
        serviceName.setText("web.order.notifyTicket");
        Element messageIdentity=authentication.addElement("MessageIdentity");
        String message=getMessage(times, "web.order.notifyTicket", "hangtian111").toUpperCase();
        messageIdentity.setText(message);
        Element partnerName=authentication.addElement("PartnerName");
        partnerName.setText("hangtian111");
        Element trainOrderService=orderProcessRequest.addElement("TrainOrderService");
        Element orderInfo=trainOrderService.addElement("OrderInfo");
        Element orderNumber=orderInfo.addElement("OrderNumber");
        orderNumber.setText(ordernum);//订单号
        Element electronicOrderNumber=orderInfo.addElement("ElectronicOrderNumber");
        electronicOrderNumber.setText("");//电子订单号
        Element userName12306=orderInfo.addElement("UserName12306");//空
        Element userPass12306=orderInfo.addElement("UserPass12306");//空
        Element orderType=orderInfo.addElement("OrderType");
        orderType.setText("0");//订单类型：0：配送票、1：团购订单、2：电子票
        Element orderTotleFee=orderInfo.addElement("OrderTotleFee");
        orderTotleFee.setText(orderprice);//实际订单总价
        Element ticketInfo=orderInfo.addElement("TicketInfo");
        Element orderTicketFromStation=ticketInfo.addElement("OrderTicketFromStation");
        orderTicketFromStation.setText(fromstation);//出发站
        Element orderTicketToStation=ticketInfo.addElement("OrderTicketToStation");
        orderTicketToStation.setText(endstation);//到达站
        Element orderTicketYMD=ticketInfo.addElement("OrderTicketYMD");
        Element orderTicketTime=ticketInfo.addElement("OrderTicketTime");
        try {
            Date date=sdf.parse(departtime);
            orderTicketYMD.setText(sdf0.format(date));//车票日期 yyyyMMdd
            orderTicketTime.setText(sdf1.format(date));//车票时间
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        Element orderTicketCheci=ticketInfo.addElement("OrderTicketCheci");
        orderTicketCheci.setText(trainno);//车次
        Element orderTicketPrice=ticketInfo.addElement("OrderTicketPrice");
        orderTicketPrice.setText(price);//车票价格
        Element orderAcceptSeat=ticketInfo.addElement("OrderAcceptSeat");
        orderAcceptSeat.setText(acceptseat);//接受坐席
        Element orderTicketSeat=ticketInfo.addElement("OrderTicketSeat");
        orderTicketSeat.setText(seattype);//首选座席
        Element seatNumber=ticketInfo.addElement("SeatNumber");
        if(seattype.contains("座")){
        	seatNumber.setText(coach+"车"+seatno+"号");//座位
//        }else if(seattype.contains("卧")){
//        	seatNumber.setText(coach+"车"+seatno+"铺");//座位
        }else {
        	seatNumber.setText(coach+"车"+seatno+"铺");//座位
        	System.out.println("seatNumber="+coach+"车"+seatno+"铺");
        }
        Element railwayTip=ticketInfo.addElement("railwayTip");
        railwayTip.setText("");//车票提醒
        Element auditTicketCount=ticketInfo.addElement("AuditTicketCount");
        String sqlnum="SELECT auditnum,childnum FROM TrainOrderOfflinePassengernum WHERE orderid="+orderid;
        DataTable myDtnum = DBHelper.GetDataTable(sqlnum);
        List<DataRow> listnum = myDtnum.GetRow();
        String auditnum="";
        String childnum="";
        if(listnum.size()>0){
        	DataRow datarow = listnum.get(0);
        	auditnum=datarow.GetColumnString("auditnum");
        	childnum=datarow.GetColumnString("childnum");
        }
        auditTicketCount.setText(auditnum);//成人数量
        Element childTicketCount=ticketInfo.addElement("ChildTicketCount");
        childTicketCount.setText(childnum);//儿童数量
        Element Passengers=ticketInfo.addElement("Passengers");
        for(int i=0;i<list.size();i++){
        	DataRow datarow = list.get(i);
        	
            Element Passenger=Passengers.addElement("Passenger");
            Element realName=Passenger.addElement("RealName");
            realName.setText(datarow.GetColumnString("Name"));//真实姓名
            Element identityType=Passenger.addElement("IdentityType");
            String idtypes="";
            String idtype=datarow.GetColumnString("IdType");
            if("1".equals(idtype)){
                idtypes="身份证";
            }else if("3".equals(idtype)){
                idtypes="护照";
            }else if("4".equals(idtype)){
                idtypes="港澳通行证";
            }else if("5".equals(idtype)){
                idtypes="台湾通行证";
            }
            identityType.setText(idtypes);//证据类型 身份证,护照等
            Element numberID=Passenger.addElement("NumberID");
            numberID.setText(datarow.GetColumnString("IdNumber"));//证件号
            Element ticketType=Passenger.addElement("TicketType");
            String ticketTypesss="";
            String ticketTypes=datarow.GetColumnString("TicketType");
            if("1".equals(ticketTypes)){
                ticketTypesss="成人票";
            }else if("0".equals(ticketTypes)){
                ticketTypesss="儿童票";
            }else if("2".equals(ticketTypes)){
                ticketTypesss="学生票";
            }
            ticketType.setText(ticketTypesss);//乘客类型 成人票、儿童票、学生票
            Element birth=Passenger.addElement("Birth");
           
            try {
                Date dateb = sdf.parse(birthday);
                birth.setText(sdf2.format(dateb));//出生日期
            }
            catch (ParseException e) {
                e.printStackTrace();
            }
            Element insuranceCount=Passenger.addElement("InsuranceCount");
            insuranceCount.setText("0");//默认0 已废弃
        }
        Element ticketInfoFinal=orderInfo.addElement("TicketInfoFinal");
        Element orderTicketFromStation1=ticketInfoFinal.addElement("OrderTicketFromStation");
        orderTicketFromStation1.setText(fromstation);//出发站
        Element orderTicketToStation1=ticketInfoFinal.addElement("OrderTicketToStation");
        orderTicketToStation1.setText(endstation);//到达站
        Element childBillid=ticketInfoFinal.addElement("ChildBillid");
        childBillid.setText("");//子订单号
        Element electronicOrderNumber1=ticketInfoFinal.addElement("ElectronicOrderNumber");
        electronicOrderNumber1.setText("");//电子订单号
        Element result=ticketInfoFinal.addElement("Result");
        result.setText("2");//出票结果 1无票 2有票
        Element noTicketReasons=ticketInfoFinal.addElement("NoTicketReasons");
        noTicketReasons.setText("");//无票原因
        Element orderTicketYMD1=ticketInfoFinal.addElement("OrderTicketYMD");
        Element orderTicketTime1=ticketInfoFinal.addElement("OrderTicketTime");
        try {
            Date date=sdf.parse(departtime);
            orderTicketYMD1.setText(sdf0.format(date));//车票日期
            orderTicketTime1.setText(sdf1.format(date));//车票时间
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        Element orderTicketCheci1=ticketInfoFinal.addElement("OrderTicketCheci");
        orderTicketCheci1.setText(trainno);//车次
        Element orderTicketPrice1=ticketInfoFinal.addElement("OrderTicketPrice");
        orderTicketPrice1.setText(price);//车票价格
        Element seatNumber1=ticketInfoFinal.addElement("SeatNumber");
        if(seattype.contains("座")){
        	seatNumber1.setText(coach+"车"+seatno+"号");//座位
//        }else if(seattype.contains("卧")){
//        	seatNumber1.setText(coach+"车"+seatno+"铺");//座位
//        }
    }else {
    	seatNumber1.setText(coach+"车"+seatno+"铺");//座位
    	System.out.println("seatNumber1="+coach+"车"+seatno+"铺");
    }
        Element railwayTip1=ticketInfoFinal.addElement("railwayTip");
        railwayTip1.setText("");//
        Element tickets=ticketInfoFinal.addElement("Tickets");
        
        if(Integer.parseInt(auditnum)>0){
//        if(true){
        	String sqlaudit="select * from TrainOrderOffline O left join TrainPassengerOffline P on o.Id  = P.OrderId left join TrainTicketOffline T on p.Id = T.TrainPid where  O.Id="+orderid +" and T.TicketType!=0";
            DataTable myDtaudit = DBHelper.GetDataTable(sqlaudit);
            List<DataRow> listaudit = myDtaudit.GetRow();
            if(listaudit.size()>0){
            	DataRow auditrow = listaudit.get(0);
            	Element ticket=tickets.addElement("Ticket");
            	Element orderTicketSeat1=ticket.addElement("OrderTicketSeat");
            	orderTicketSeat1.setText(auditrow.GetColumnString("SeatType"));//首选座席TicketType
            	Element ticketType=ticket.addElement("TicketType");
            	String ticketTypetemp=auditrow.GetColumnString("TicketType");
            	String temps="";
            	if("1".equals(ticketTypetemp)){
            		temps="成人票";
                }else if("0".equals(ticketTypetemp)){
                	temps="儿童票";
                }else if("2".equals(ticketTypetemp)){
                	temps="学生票";
                }
            	Element orderTicketPrice2=ticket.addElement("OrderTicketPrice");
                orderTicketPrice2.setText(auditrow.GetColumnString("sealPrice"));//车票价格
                ticketType.setText(temps);//乘客类型 成人票、儿童票、学生票
                Element ticketCount=ticket.addElement("TicketCount");
                ticketCount.setText(listaudit.size()+"");//成人票车票数量
                Element detailInfos=ticket.addElement("DetailInfos");
            	for(int i=0;i<listaudit.size();i++){
            		DataRow auditrow1 = listaudit.get(i);
            		Element detailInfo=detailInfos.addElement("DetailInfo");
                    Element passengerName=detailInfo.addElement("PassengerName");
                    passengerName.setText(auditrow1.GetColumnString("Name"));//乘客名称
                    Element numberID=detailInfo.addElement("NumberID");
                    numberID.setText(auditrow1.GetColumnString("IdNumber"));//证件号
                    Element identityType=detailInfo.addElement("IdentityType");
                    String idtypes="";
                    String idtype=auditrow1.GetColumnString("IdType");
                    if("1".equals(idtype)){
                        idtypes="身份证";
                    }else if("3".equals(idtype)){
                        idtypes="护照";
                    }else if("4".equals(idtype)){
                        idtypes="港澳通行证";
                    }else if("5".equals(idtype)){
                        idtypes="台湾通行证";
                    }
                    identityType.setText(idtypes);//证据类型 身份证,护照等
                    Element seatNo=detailInfo.addElement("SeatNo");
                    if(seattype.contains("座")){
                    	seatNo.setText(auditrow1.GetColumnString("Coach")+"车"+auditrow1.GetColumnString("SeatNo")+"号");//座位
                    }else if(seattype.contains("卧")){
                    	seatNo.setText(auditrow1.GetColumnString("Coach")+"车"+auditrow1.GetColumnString("SeatNo")+"铺");//座位
                    }else{
                    	seatNo.setText(auditrow1.GetColumnString("Coach")+"车"+auditrow1.GetColumnString("SeatNo"));//座位
                    }
            	}
            }
        	
        }
        if(Integer.parseInt(childnum)>0){
//        if(false){
        	String sqlchild="select * from TrainOrderOffline O left join TrainPassengerOffline P on o.Id  = P.OrderId left join TrainTicketOffline T on p.Id = T.TrainPid where  O.Id="+orderid +" and T.TicketType=0";
            DataTable myDtchild = DBHelper.GetDataTable(sqlchild);
            List<DataRow> listchild = myDtchild.GetRow();
            if(listchild.size()>0){
            	DataRow childrow = listchild.get(0);
            	Element ticket=tickets.addElement("Ticket");
            	Element orderTicketSeat1=ticket.addElement("OrderTicketSeat");
            	orderTicketSeat1.setText(childrow.GetColumnString("SeatType"));//首选座席TicketType
            	Element ticketType=ticket.addElement("TicketType");
            	String ticketTypetemp=childrow.GetColumnString("TicketType");
            	String temps="";
            	if("1".equals(ticketTypetemp)){
            		temps="成人票";
                }else if("0".equals(ticketTypetemp)){
                	temps="儿童票";
                }else if("2".equals(ticketTypetemp)){
                	temps="学生票";
                }
            	Element orderTicketPrice2=ticket.addElement("OrderTicketPrice");
                orderTicketPrice2.setText(childrow.GetColumnString("sealPrice"));//车票价格
                ticketType.setText(temps);//乘客类型 成人票、儿童票、学生票
                Element ticketCount=ticket.addElement("TicketCount");
                ticketCount.setText(listchild.size()+"");//儿童票车票数量
                Element detailInfos=ticket.addElement("DetailInfos");
            	for(int i=0;i<listchild.size();i++){
            		DataRow childrow1 = listchild.get(i);
            		Element detailInfo=detailInfos.addElement("DetailInfo");
                    Element passengerName=detailInfo.addElement("PassengerName");
                    passengerName.setText(childrow1.GetColumnString("Name"));//乘客名称
                    Element numberID=detailInfo.addElement("NumberID");
                    numberID.setText(childrow1.GetColumnString("IdNumber"));//证件号
                    Element identityType=detailInfo.addElement("IdentityType");
                    String idtypes="";
                    String idtype=childrow1.GetColumnString("IdType");
                    if("1".equals(idtype)){
                        idtypes="身份证";
                    }else if("3".equals(idtype)){
                        idtypes="护照";
                    }else if("4".equals(idtype)){
                        idtypes="港澳通行证";
                    }else if("5".equals(idtype)){
                        idtypes="台湾通行证";
                    }
                    identityType.setText(idtypes);//证据类型 身份证,护照等
                    Element seatNo=detailInfo.addElement("SeatNo");
                    if(seattype.contains("座")){
                    	seatNo.setText(childrow1.GetColumnString("Coach")+"车"+childrow1.GetColumnString("SeatNo")+"号");//座位
                    }else if(seattype.contains("卧")){
                    	seatNo.setText(childrow1.GetColumnString("Coach")+"车"+childrow1.GetColumnString("SeatNo")+"铺");//座位
                    }else{
                    	seatNo.setText(childrow1.GetColumnString("Coach")+"车"+childrow1.GetColumnString("SeatNo"));//座位
                    }
            	}
            }
        	
        }
        
      return document.asXML();
    }
    public static String getMessage(String times,String serviceName,String partName){
		String result=MD5Util.MD5(times+serviceName+partName);
//		System.out.println(times+serviceName+partName+"<-->"+result);
		return result;
	}

}
