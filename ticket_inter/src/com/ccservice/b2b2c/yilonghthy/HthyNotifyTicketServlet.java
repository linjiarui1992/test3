package com.ccservice.b2b2c.yilonghthy;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ccservice.b2b2c.atom.component.WriteLog;


public class HthyNotifyTicketServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public HthyNotifyTicketServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String orderid=request.getParameter("orderid");
		System.out.println("出票成功请求orderID："+orderid);
		WriteLog.write("cn_home请求出票成功入口", "orderid="+orderid);
		new HthyNotifyTicket().notifyTicket(orderid);
	}

}
