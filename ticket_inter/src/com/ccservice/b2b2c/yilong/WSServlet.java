package com.ccservice.b2b2c.yilong;

import java.io.IOException;

import javax.servlet.GenericServlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.Endpoint;


public class WSServlet  extends GenericServlet {
	private static final long serialVersionUID = 1L;
       
	@Override 
    public void init(ServletConfig servletConfig) throws ServletException { 
            super.init(servletConfig); 
            System.out.println("准备启动WebService服务：http://localhost:8888:8888/java6ws/Java6WS"); 
            //发布一个WebService 
            Endpoint.publish("http://localhost:8888/service", new HelloWebservice()); 
            System.out.println("已成功启动WebService服务：http://localhost:8888:8888/java6ws/Java6WS"); 

    }

	@Override
	public void service(ServletRequest arg0, ServletResponse arg1)
			throws ServletException, IOException {
		System.out.println("此Servlet不处理任何业务逻辑，仅仅yonglai发布一个Web服务：http://localhost:8888:8888/java6ws/Java6WS");
		
	}

}
