package com.ccservice.b2b2c.yilong;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 物流区域-支持配送的地区
 * @author guozhengju
 *time 2015-12-29 14:45
 */
public class LogisticsArea extends HttpServlet {
	private static final long serialVersionUID = 1L;
       private String dates;
       private String partnerName;
       private String messageIdentity;
       private int levels;
    
    public LogisticsArea(String dates,String partnerName,String messageIdentity,int levels) {
    	this.dates=dates;
    	this.partnerName=partnerName;
    	this.messageIdentity=messageIdentity;
    	this.levels=levels;
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}
	
}
