package com.ccservice.b2b2c.yilong;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
//取消订单request
public class CancelOrder {
	public static void main1(String[] args) {
		String xml="<?xml version=\"1.0\" encoding=\"UTF-8\"?><OrderRequest><Authentication><ServiceName>order.cancelOrder</ServiceName><PartnerName>elong</PartnerName><TimeStamp>2011-11-30 19:22:30</TimeStamp><MessageIdentity>2ceb56939b9b9ebfeff0a11bb732ed3e</MessageIdentity></Authentication><TrainOrderService><OrderNumber>2011113000157001173</OrderNumber><CancelTime></CancelTime><CancelChooseReason></CancelChooseReason><CancelInputReason></CancelInputReason></TrainOrderService></OrderRequest>";
		
		try {
			Document document = DocumentHelper.parseText(xml);
			Element root = document.getRootElement();
			//Authentication
			Element Authentication=root.element("Authentication");
			String serviceName=Authentication.elementText("ServiceName");//接口服务名
			String partnerName=Authentication.elementText("PartnerName");//合作方名称
			String timeStamp=Authentication.elementText("TimeStamp");//时间
			String messageIdentity=Authentication.elementText("MessageIdentity");//验证信息
			//TrainOrderService
			Element TrainOrderService=root.element("TrainOrderService");
			String orderNumber=TrainOrderService.elementText("OrderNumber");//订单号
			String cancelTime=TrainOrderService.elementText("CancelTime");//取消时间
			String cancelChooseReason=TrainOrderService.elementText("CancelChooseReason");//取消原因 选择
			String cancelInputReason=TrainOrderService.elementText("CancelInputReason");//取消原因 编辑
			System.out.println(cancelTime+"--"+cancelChooseReason+"--"+cancelInputReason+"--"+messageIdentity+"--"+orderNumber);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
	}
	public static void main(String[] args) {
		//取消订单Response
		Document document=DocumentHelper.createDocument();
		Element orderResponse=document.addElement("OrderResponse");
		Element serviceName=orderResponse.addElement("ServiceName");
		serviceName.setText("order.cancelOrder");
		Element status=orderResponse.addElement("Status");
		status.setText("SUCCESS");
		Element channel=orderResponse.addElement("Channel");
		channel.setText("Jingyan");
		Element orderNumber=orderResponse.addElement("OrderNumber");
		orderNumber.setText("JingyanT20150825100014");
		Element discription=orderResponse.addElement("Discription");
		discription.setText("信息,取消成功..");
		System.out.println(document.asXML());
	}
}
