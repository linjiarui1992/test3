package com.ccservice.b2b2c.yilong;

import java.rmi.RemoteException;

import org.apache.axis2.AxisFault;

import com.ccservice.b2b2c.yilong.JaxRpcCallbackServiceStub.CtripCallback;
import com.ccservice.b2b2c.yilong.JaxRpcCallbackServiceStub.CtripCallbackResponse;

public class YiLongAction {
	
	public static String lockOrder(){
		try {
			JaxRpcCallbackServiceStub Stub=new JaxRpcCallbackServiceStub();
			CtripCallback ctripCallback = new CtripCallback();
			org.apache.axis2.databinding.types.soapencoding.String param = new org.apache.axis2.databinding.types.soapencoding.String();
			param.setString("<?xml version=\"1.0\"?><OrderProcessRequest xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><Authentication><TimeStamp>2015-12-29 15:57:58</TimeStamp><ServiceName>web.order.lockOrder</ServiceName><MessageIdentity>6AB1E18F5537ABE2474881076E9370AA</MessageIdentity><PartnerName>xxx</PartnerName></Authentication><TrainOrderService><OrderInfo><OrderNumber>2015072215292295733</OrderNumber></OrderInfo></TrainOrderService></OrderProcessRequest>");
			ctripCallback.setXml(param);
			CtripCallbackResponse ctripCallbackResponse = Stub.ctripCallback(ctripCallback);
			org.apache.axis2.databinding.types.soapencoding.String ss=ctripCallbackResponse.getCtripCallbackReturn();
			System.out.println(ss);
			
		} catch (AxisFault e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return "";
	}
public static void main(String[] args) {
	lockOrder();
}
}
