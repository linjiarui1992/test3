package com.ccservice.b2b2c.yilong;

import java.rmi.RemoteException;

import org.apache.axis2.AxisFault;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.ccservice.b2b2c.yilong.JaxRpcCallbackServiceStub.CtripCallback;
import com.ccservice.b2b2c.yilong.JaxRpcCallbackServiceStub.CtripCallbackResponse;
public class YiLongTest {

	public static void main(String[] args) {
//		jieXiResponse();
		try {
			JaxRpcCallbackServiceStub Stub=new JaxRpcCallbackServiceStub();
			CtripCallback ctripCallback = new CtripCallback();
			org.apache.axis2.databinding.types.soapencoding.String param = new org.apache.axis2.databinding.types.soapencoding.String();
			param.setString("<?xml version=\"1.0\"?><OrderProcessRequest xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><Authentication><TimeStamp>2015-12-29 15:57:58</TimeStamp><ServiceName>web.order.lockOrder</ServiceName><MessageIdentity>6AB1E18F5537ABE2474881076E9370AA</MessageIdentity><PartnerName>xxx</PartnerName></Authentication><TrainOrderService><OrderInfo><OrderNumber>2015072215292295733</OrderNumber></OrderInfo></TrainOrderService></OrderProcessRequest>");
			ctripCallback.setXml(param);
			CtripCallbackResponse ctripCallbackResponse = Stub.ctripCallback(ctripCallback);
			org.apache.axis2.databinding.types.soapencoding.String ss=ctripCallbackResponse.getCtripCallbackReturn();
			System.out.println(ss);
			
		} catch (AxisFault e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
	public static String jieXiResponse(){
		String str="<?xml version='1.0' encoding='UTF-8' ?><OrderRequest><Authentication><ServiceName>order.addOrder</ServiceName><!--接口服务名--><PartnerName>elong</PartnerName><!--合作方名称--><TimeStamp>2015-07-22 15:29:22</TimeStamp><!--时间--><MessageIdentity>4cad761817839dda5d9ec5f691563662</MessageIdentity><!--验证信息--></Authentication><TrainOrderService><OrderNumber>2015072215292295733</OrderNumber><!--订单号 --><ChannelName>360_search</ChannelName><!--渠道 --><OrderTicketType>0</OrderTicketType><!--车票类型 0普通票，1往返票--><Order><OrderTime>2015-07-22 15:29:22</OrderTime><!--下单时间 --><OrderMedia>pc</OrderMedia><!--下单类型  pc , wap 默认 pc--><TicketOffsetFee>0</TicketOffsetFee><!--优惠券金额 --><Insurance>N</Insurance><!--是否是保险 --><Invoice>N</Invoice><!--是否需要发票 --><PrivateCustomization>1</PrivateCustomization><!—0不是私人定制，1是私人定制 --><TicketInfo><TicketItem><FromStationName>上海</FromStationName><!--出发站 --><ToStationName>汉口</ToStationName><!--到达站 --><TicketTime>2015-07-27 22:15</TicketTime><!--车票时间 --><TrainNumber>K1152</TrainNumber><!--车次 --><TicketPrice>122</TicketPrice><!--票面价格 --><TicketCount>2</TicketCount><!--数量 --><AuditTicketCount>2</AuditTicketCount><!--成人数量 --><ChildTicketCount>0</ChildTicketCount><!--儿童数量 --><SeatName>硬座</SeatName><!--首选坐席 --><!--可接受席位和私人定制话术共用此节点 例子:普通票可接受席位(无座、硬卧上) 私人定制话术:优先下铺，可接受中上铺 --><AcceptSeat>必须连号，否则无票，出错会造成赔款!</AcceptSeat><passport>张志梦,身份证,42128119871020656X,成人票,1987-10-20,0|卢柳灿,身份证,352229198206214533,成人票,1982-06-21,0</passport><!--证据信息 --><OrderPrice>274</OrderPrice><!--订单总价格 Float--></TicketItem></TicketInfo></Order><User><UserID>54224353</UserID><!--用户id --><UserName>张志梦</UserName><!--收件人姓名 --><UserTel>--</UserTel><!--固定电话 --><userLoginName>13564818486</userLoginName><!--用户登录帐号 --><UserMobile>13564818486</UserMobile><!--用户手机号码 --></User><DeliverInfo><AreaID>14</AreaID><!--地区代码 --><address>鹤望路365弄65号501室</address><!--配送地址 --><OrderDeliverTypeID>440</OrderDeliverTypeID><!--规则ID --><DeliverPrice>20</DeliverPrice><!--物流价格 --><WeekendDeliver>N</WeekendDeliver><!--周末配送标志 Y/N Y代表周末进行配送--></DeliverInfo></TrainOrderService></OrderRequest>";
		try {
			Document document = DocumentHelper.parseText(str);
			Element root = document.getRootElement();
			 String weighTime=root.element("ServiceName").getText();
			//order.addOrder
//			String ServiceName=root.attributeValue("ServiceName");
			System.out.println(weighTime);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return "";
	}
}
