package com.ccservice.b2b2c.yilong;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.inter.server.Server;

public class ElongRefundNotify {
	public String refundNotify(String xml){
		String ordernum="";
		try {
			Document document = DocumentHelper.parseText(xml);
			Element root = document.getRootElement();
			//Authentication
			Element Authentication=root.element("Authentication");
			String serviceName=Authentication.elementText("ServiceName");//接口服务名
			String partnerName=Authentication.elementText("PartnerName");//合作方名称
			String timeStamp=Authentication.elementText("TimeStamp");//时间
			String messageIdentity=Authentication.elementText("MessageIdentity");//验证信息
			//TrainOrderService
			Element TrainOrderService=root.element("TrainOrderService");
			String orderNumber=TrainOrderService.elementText("OrderNumber");//订单号
			ordernum=orderNumber;
			String orderTid=TrainOrderService.elementText("OrderTid");//退款流水ID
			String refundFee=TrainOrderService.elementText("RefundFee");//退款金额
			String refundTime=TrainOrderService.elementText("RefundTime");//退款时间
			String orderTradeNo=TrainOrderService.elementText("OrderTradeNo");//退款流水ID
			String insertsql="INSERT INTO TrainOfflineRefundNotify(OrderNumber,OrderTid,RefundFee,RefundTime,OrderTradeNo) VALUES('"+orderNumber+"','"+orderTid+"','"+refundFee+"','"+refundTime+"','"+orderTradeNo+"')";
			Server.getInstance().getSystemService().excuteAdvertisementBySql(insertsql);
			WriteLog.write("ELong线下火车票退款成功通知记录","订单号:"+orderNumber+",sql:"+insertsql);
			String getid="SELECT ID FROM TrainOrderOffline WHERE OrderNumberOnline='"+ordernum+"'";
			List list=Server.getInstance().getSystemService().findMapResultBySql(getid, null);
			if(list.size()>0){
				Map map=(Map)list.get(0);
				String orderid=map.get("ID").toString();
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String recordsql="INSERT INTO TrainOrderOfflineRecord(FKTrainOrderOfflineId,ProviderAgentid,DistributionTime,DealResult) values("+orderid+",0,'"+sdf.format(new Date())+"',8)";
				Server.getInstance().getSystemService().excuteAdvertisementBySql(recordsql);
				WriteLog.write("ELong_cn_home线下火车票退款成功操作记录","订单号:"+orderNumber+",sql:"+recordsql);
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		String result=refundNotifyresult(ordernum);
		return result;
	}
	
	public String refundNotifyresult(String ordernumber){
		//下单请求接口调用Response
		Document document=DocumentHelper.createDocument();
		Element orderResponse=document.addElement("OrderResponse");
		Element serviceName=orderResponse.addElement("ServiceName");
		serviceName.setText("order.refundNotify");
		Element status=orderResponse.addElement("Status");
		status.setText("SUCCESS");
		Element channel=orderResponse.addElement("Channel");
		channel.setText("Jingyan");
		Element orderNumber=orderResponse.addElement("OrderNumber");
		orderNumber.setText(ordernumber);
		Element discription=orderResponse.addElement("Discription");
		discription.setText("退款成功通知.... ");
		return document.asXML();
	
	}
}
