package com.ccservice.b2b2c.yilong;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
//支付成功接口调用
public class OrderPay {
		public static void main1(String[] args) {
			String xml="<?xml version=\"1.0\" encoding=\"utf-8\"?><OrderRequest xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><Authentication><ServiceName>order.pay</ServiceName><PartnerName>elong</PartnerName><TimeStamp>2015-07-28 16:12:09</TimeStamp><MessageIdentity>b62587ac52ebc8c8cf0ce3d467b4db0a</MessageIdentity></Authentication><TrainOrderService><OrderNumber>JingyanT20150728160001</OrderNumber><PayedPrice>25.50</PayedPrice><PayTime>16:12:09</PayTime><PayType>yuejie</PayType><tradeNumber>JingyanT20150728160001</tradeNumber></TrainOrderService></OrderRequest>";
			try {
				//request
				Document document = DocumentHelper.parseText(xml);
				Element root = document.getRootElement();
				//Authentication
				Element Authentication=root.element("Authentication");
				String serviceName=Authentication.elementText("ServiceName");//接口服务名
				String partnerName=Authentication.elementText("PartnerName");//合作方名称
				String timeStamp=Authentication.elementText("TimeStamp");//时间
				String messageIdentity=Authentication.elementText("MessageIdentity");//验证信息
				//TrainOrderService
				Element TrainOrderService=root.element("TrainOrderService");
				String orderNumber=TrainOrderService.elementText("OrderNumber");//订单号
				String payedPrice=TrainOrderService.elementText("PayedPrice");//支付价格
				String payTime=TrainOrderService.elementText("PayTime");//支付时间
				String payType=TrainOrderService.elementText("PayType");//支付类型
				String tradeNumber=TrainOrderService.elementText("tradeNumber");//交易号
				//response
				System.out.println(orderNumber+""+payedPrice+""+payTime+""+payType+tradeNumber);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			
		}
		public static void main(String[] args) {
			//下单请求接口调用Response
			Document document=DocumentHelper.createDocument();
			Element orderResponse=document.addElement("OrderResponse");
			Element serviceName=orderResponse.addElement("ServiceName");
			serviceName.setText("order.pay");
			Element status=orderResponse.addElement("Status");
			status.setText("SUCCESS");
			Element channel=orderResponse.addElement("Channel");
			channel.setText("Jingyan");
			Element orderNumber=orderResponse.addElement("OrderNumber");
			orderNumber.setText("JingyanT20150825100014");
			Element discription=orderResponse.addElement("Discription");
			discription.setText("信息,支付成功..");
			System.out.println(document.asXML());
		}
}
