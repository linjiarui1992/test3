package com.ccservice.b2b2c.yilong;

import java.rmi.RemoteException;

import org.apache.axis2.AxisFault;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.ccservice.b2b2c.yilong.JaxRpcCallbackServiceStub;
import com.ccservice.b2b2c.yilong.JaxRpcCallbackServiceStub.CtripCallback;
import com.ccservice.b2b2c.yilong.JaxRpcCallbackServiceStub.CtripCallbackResponse;

public class RequestRefund {

	public static void main(String[] args) {

		String result="";
		try {
			JaxRpcCallbackServiceStub Stub=new JaxRpcCallbackServiceStub();
			CtripCallback ctripCallback = new CtripCallback();
			org.apache.axis2.databinding.types.soapencoding.String param = new org.apache.axis2.databinding.types.soapencoding.String();
			String xmlparam=getXml();
			param.setString(xmlparam);
			ctripCallback.setXml(param);
			CtripCallbackResponse ctripCallbackResponse = Stub.ctripCallback(ctripCallback);
			org.apache.axis2.databinding.types.soapencoding.String ss=ctripCallbackResponse.getCtripCallbackReturn();
			System.out.println(ss+"");
			result=ss+"";
		} catch (AxisFault e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		//锁单返回信息解析
		try {
			Document document = DocumentHelper.parseText(result);
			Element orderResponse = document.getRootElement();
			String serviceName=orderResponse.elementText("ServiceName");//接口服务名
			String operationDateTime=orderResponse.elementText("OperationDateTime");//操作日期
			String orderNumber=orderResponse.elementText("OrderNumber");//订单号
			String status=orderResponse.elementText("Status");//状态
			Element errorResponse=orderResponse.element("ErrorResponse");
			String errorMessage=errorResponse.elementText("ErrorMessage");//信息
			if("FAIL".equals(status)){
				String errorCode=errorResponse.elementText("ErrorCode");//ErrorCode
				System.out.println(serviceName+"--"+operationDateTime+"--"+orderNumber+"--"+status+"--"+errorMessage+"--"+errorCode);
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	
	
	}
	public static String getXml() {
		//下单请求接口调用Response
		Document document=DocumentHelper.createDocument();
		Element orderProcessRequest=document.addElement("OrderProcessRequest");
		orderProcessRequest.addAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance");
		Element authentication=orderProcessRequest.addElement("Authentication");
		
		Element serviceName=authentication.addElement("ServiceName");
		serviceName.setText("web.order.requestRefund");
		Element partnerName=authentication.addElement("PartnerName");
		partnerName.setText("xxx");
		Element timeStamp=authentication.addElement("TimeStamp");
		timeStamp.setText("2015-07-22 15:57:58");
		Element messageIdentity=authentication.addElement("MessageIdentity");
		messageIdentity.setText("70541E56C256EC9C79183DBAB7EE7332");
		
		Element trainOrderService=orderProcessRequest.addElement("TrainOrderService");
		Element orderInfo=trainOrderService.addElement("OrderInfo");
		Element orderNumber=orderInfo.addElement("OrderNumber");
		orderNumber.setText("订单号");//订单号
		Element orderTid=orderInfo.addElement("OrderTid");
		orderTid.setText("orderid");//OrderRefund.RefundBillNo退款bill单号
		Element orderTradeNo=orderInfo.addElement("OrderTradeNo");
		orderTradeNo.setText("orderTradeNo");// OrderRefund.RefundBillNo退款bill单号
		Element orderTotleFee=trainOrderService.addElement("OrderTotleFee");
//		orderTotleFee.setText("");//空值
		Element totalRefundAmount=trainOrderService.addElement("TotalRefundAmount");
		totalRefundAmount.setText("5.50");//OrderRefund.RefundAmount
		Element reason=trainOrderService.addElement("Reason");
		reason.setText("Test");//原因
		Element refundType=trainOrderService.addElement("RefundType");
		refundType.setText("0");//0差额退款、3核销退款
//		System.out.println(document.asXML());
		return document.asXML();
	}
}
