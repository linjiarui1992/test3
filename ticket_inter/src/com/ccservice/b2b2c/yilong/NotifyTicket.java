package com.ccservice.b2b2c.yilong;

import java.rmi.RemoteException;

import org.apache.axis2.AxisFault;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.ccservice.b2b2c.yilong.JaxRpcCallbackServiceStub.CtripCallback;
import com.ccservice.b2b2c.yilong.JaxRpcCallbackServiceStub.CtripCallbackResponse;

public class NotifyTicket {
	public static void main1(String[] args) {

		String result="";
		try {
			JaxRpcCallbackServiceStub Stub=new JaxRpcCallbackServiceStub();
			CtripCallback ctripCallback = new CtripCallback();
			org.apache.axis2.databinding.types.soapencoding.String param = new org.apache.axis2.databinding.types.soapencoding.String();
//			String xmlparam=getXml();
//			param.setString(xmlparam);
			ctripCallback.setXml(param);
			CtripCallbackResponse ctripCallbackResponse = Stub.ctripCallback(ctripCallback);
			org.apache.axis2.databinding.types.soapencoding.String ss=ctripCallbackResponse.getCtripCallbackReturn();
			System.out.println(ss+"");
			result=ss+"";
		} catch (AxisFault e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		//锁单返回信息解析
		try {
			Document document = DocumentHelper.parseText(result);
			Element orderResponse = document.getRootElement();
			String serviceName=orderResponse.elementText("ServiceName");//接口服务名
			String operationDateTime=orderResponse.elementText("OperationDateTime");//操作日期
			String orderNumber=orderResponse.elementText("OrderNumber");//订单号
			String status=orderResponse.elementText("Status");//状态
			Element errorResponse=orderResponse.element("ErrorResponse");
			String errorMessage=errorResponse.elementText("ErrorMessage");//信息
			if("FAIL".equals(status)){
				String errorCode=errorResponse.elementText("ErrorCode");//ErrorCode
				System.out.println(serviceName+"--"+operationDateTime+"--"+orderNumber+"--"+status+"--"+errorMessage+"--"+errorCode);
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
//	public static String getXml() {
	public static void main(String[] args) {	
		//下单请求接口调用Response
		Document document=DocumentHelper.createDocument();
		Element orderProcessRequest=document.addElement("OrderProcessRequest");
		orderProcessRequest.addAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance");
		
		Element authentication=orderProcessRequest.addElement("Authentication");
		Element timeStamp=authentication.addElement("TimeStamp");
		timeStamp.setText("2015-07-22 15:57:58");
		Element serviceName=authentication.addElement("ServiceName");
		serviceName.setText("web.order.notifyTicket");
		Element messageIdentity=authentication.addElement("MessageIdentity");
		messageIdentity.setText("70541E56C256EC9C79183DBAB7EE7332");
		Element partnerName=authentication.addElement("PartnerName");
		partnerName.setText("xxx");
		Element trainOrderService=orderProcessRequest.addElement("TrainOrderService");
		Element orderInfo=trainOrderService.addElement("OrderInfo");
		Element orderNumber=orderInfo.addElement("OrderNumber");
		orderNumber.setText("34132132123");//订单号
		Element electronicOrderNumber=orderInfo.addElement("ElectronicOrderNumber");
		electronicOrderNumber.setText("");//电子订单号
		Element userName12306=orderInfo.addElement("UserName12306");//空
		Element userPass12306=orderInfo.addElement("UserPass12306");//空
		Element orderType=orderInfo.addElement("OrderType");
		orderType.setText("订单号");//订单类型：0：配送票、1：团购订单、2：电子票
		Element orderTotleFee=orderInfo.addElement("OrderTotleFee");
		orderTotleFee.setText("订单号");//实际订单总价
		Element ticketInfo=orderInfo.addElement("TicketInfo");
		Element orderTicketFromStation=ticketInfo.addElement("OrderTicketFromStation");
		orderTicketFromStation.setText("出发站");//出发站
		Element orderTicketToStation=ticketInfo.addElement("OrderTicketToStation");
		orderTicketToStation.setText("到达站");//到达站
		Element orderTicketYMD=ticketInfo.addElement("OrderTicketYMD");
		orderTicketYMD.setText("20151230");//车票日期 yyyyMMdd
		Element orderTicketTime=ticketInfo.addElement("OrderTicketTime");
		orderTicketTime.setText("18:20");//车票时间
		Element orderTicketCheci=ticketInfo.addElement("OrderTicketCheci");
		orderTicketCheci.setText("D2006");//车次
		Element orderTicketPrice=ticketInfo.addElement("OrderTicketPrice");
		orderTicketPrice.setText("122.00");//车票价格
		Element orderAcceptSeat=ticketInfo.addElement("OrderAcceptSeat");
		orderAcceptSeat.setText("");//接受坐席
		Element orderTicketSeat=ticketInfo.addElement("OrderTicketSeat");
		orderTicketSeat.setText("硬座");//首选座席
		Element seatNumber=ticketInfo.addElement("SeatNumber");
		seatNumber.setText("01车厢011号");//座位
		Element railwayTip=ticketInfo.addElement("railwayTip");
		railwayTip.setText("");//车票提醒
		Element auditTicketCount=ticketInfo.addElement("AuditTicketCount");
		auditTicketCount.setText("2");//成人数量
		Element childTicketCount=ticketInfo.addElement("ChildTicketCount");
		childTicketCount.setText("0");//儿童数量
		Element Passengers=ticketInfo.addElement("Passengers");
		for(int i=0;i<2;i++){
			Element Passenger=Passengers.addElement("Passenger");
			Element realName=Passenger.addElement("Passenger");
			realName.setText("张志梦");//真实姓名
			Element identityType=Passenger.addElement("IdentityType");
			identityType.setText("身份证");//证据类型 身份证,护照等
			Element numberID=Passenger.addElement("NumberID");
			numberID.setText("证件号");//证件号
			Element ticketType=Passenger.addElement("TicketType");
			ticketType.setText("成人票");//乘客类型 成人票、儿童票、学生票
			Element birth=Passenger.addElement("Birth");
			birth.setText("1987-10-20");//出生日期
			Element insuranceCount=Passenger.addElement("InsuranceCount");
			insuranceCount.setText("0");//默认0 已废弃
		}
		Element ticketInfoFinal=orderInfo.addElement("TicketInfoFinal");
		Element orderTicketFromStation1=ticketInfoFinal.addElement("OrderTicketFromStation");
		orderTicketFromStation1.setText("上海");//出发站
		Element orderTicketToStation1=ticketInfoFinal.addElement("OrderTicketToStation");
		orderTicketToStation1.setText("汉口");//到达站
		Element childBillid=ticketInfoFinal.addElement("ChildBillid");
		childBillid.setText("");//子订单号
		Element electronicOrderNumber1=ticketInfoFinal.addElement("ElectronicOrderNumber");
		electronicOrderNumber1.setText("");//电子订单号
		Element result=ticketInfoFinal.addElement("Result");
		result.setText("2");//出票结果 1无票 2有票
		Element noTicketReasons=ticketInfoFinal.addElement("NoTicketReasons");
		noTicketReasons.setText("");//无票原因
		Element orderTicketYMD1=ticketInfoFinal.addElement("OrderTicketYMD");
		orderTicketYMD1.setText("20151230");//车票日期
		Element orderTicketTime1=ticketInfoFinal.addElement("OrderTicketTime");
		orderTicketTime1.setText("12:30");//车票时间
		Element orderTicketCheci1=ticketInfoFinal.addElement("OrderTicketCheci");
		orderTicketCheci1.setText("D2006");//车次
		Element orderTicketPrice1=ticketInfoFinal.addElement("OrderTicketPrice");
		orderTicketPrice1.setText("122.00");//车票价格
		Element seatNumber1=ticketInfoFinal.addElement("SeatNumber");
		seatNumber1.setText("01车厢011号");//座位
		Element railwayTip1=ticketInfoFinal.addElement("railwayTip");
		railwayTip1.setText("");//
		Element tickets=ticketInfoFinal.addElement("Tickets");
		Element ticket=tickets.addElement("Ticket");
		Element orderTicketSeat1=ticket.addElement("OrderTicketSeat");
		orderTicketSeat1.setText("硬座");//首选座席TicketType
		Element ticketType=ticket.addElement("TicketType");
		ticketType.setText("成人票");//乘客类型 成人票、儿童票、学生票
		Element orderTicketPrice2=ticket.addElement("OrderTicketPrice");
		orderTicketPrice2.setText("122.00");//车票价格
		Element ticketCount=ticket.addElement("TicketCount");
		ticketCount.setText("2");//车票数量
		Element detailInfos=ticket.addElement("DetailInfos");
		for(int i=0;i<2;i++){
			Element detailInfo=detailInfos.addElement("DetailInfo");
			Element passengerName=detailInfo.addElement("PassengerName");
			passengerName.setText("张志梦");//乘客名称
			Element identityType=detailInfo.addElement("IdentityType");
			identityType.setText("身份证");//证据类型 身份证,护照等
			Element numberID=detailInfo.addElement("NumberID");
			numberID.setText("42128119871020656X");//证件号
			Element seatNo=detailInfo.addElement("SeatNo");
			seatNo.setText("车厢");//座位
		}
		System.out.println(document.asXML());
//		return document.asXML();
	}


}
