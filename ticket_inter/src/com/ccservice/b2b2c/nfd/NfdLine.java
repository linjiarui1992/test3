package com.ccservice.b2b2c.nfd;

import java.util.regex.Pattern;

public class NfdLine {
	
	
	//承运人	单程票价	往返票价	票价级别	旅行代码	舱位	最长停留	最短停留	生效日期	截止日期	
	//去程排除航班	AP	基本代理费	额外代理费	OPEN票	RQ票	团队票	变更	签转	航班号范围	
	//提前预订	最早出票日期	最晚出票日期	签注信息	去程航班限制	签注信息2	随订随售	自愿退票收费	
	//儿童运价	FAREBASIS栏	升舱	最长旅行期限	BSP客票	BSPET客票	
	//本票	本票ET票	去程适用航班号范围	去程航班季节	回程航班季节	回程适用航班号范围	去程适用星期限制	
	//适用规定	去程/回程	乘机者	预订规定	运价组合	团队规定	支付/出票	退票规定	变更规定	扩充规定	其他规定
	//LN  CXR  OW       RT       FBC/TC      RBD MIN/MAX TRVDATE         R
	
	
	//行号 承运人	单程票价	往返票价	票价级别	旅行代码	舱位	最长停留	最短停留	生效日期	截止日期	
	
	
	private String LN;
	private String CXR;
	private String OW;
	private String RT;
	private String FBC;
	private String TC;
	private String RBD;
	private String MIN;
	private String MAX;
	private String TRVDATESTART;
	private String TRVDATEEND;	
	private String TRVDLINE;
	
	
	private String DESC;
	
	public String getLN() {
		return LN;
	}

	public void setLN(String ln) {
		LN = ln;
	}

	public String getCXR() {
		
		return CXR;
	}

	public void setCXR(String cxr) {
		CXR = cxr;
	}

	public String getOW() {
		return OW;
	}

	public void setOW(String ow) {
		OW = ow;
	}

	public String getRT() {
		return RT;
	}

	public void setRT(String rt) {
		RT = rt;
	}

	public String getFBC() {
		return FBC;
	}

	public void setFBC(String fbc) {
		FBC = fbc;
	}

	public String getTC() {
		return TC;
	}

	public void setTC(String tc) {
		TC = tc;
	}

	public String getRBD() {
		return RBD;
	}

	public void setRBD(String rbd) {
		RBD = rbd;
	}

	public String getMIN() {
		return MIN;
	}

	public void setMIN(String min) {
		MIN = min;
	}

	public String getMAX() {
		return MAX;
	}

	public void setMAX(String max) {
		MAX = max;
	}

	public String getTRVDATESTART() {
		return TRVDATESTART;
	}

	public void setTRVDATESTART(String trvdatestart) {
		TRVDATESTART = trvdatestart;
	}

	public String getTRVDATEEND() {
		return TRVDATEEND;
	}

	public void setTRVDATEEND(String trvdateend) {
		TRVDATEEND = trvdateend;
	}

	public String getDESC() {
		return DESC;
	}

	public void setDESC(String desc) {
		String[] lines = desc.split("\n");
		if(lines==null && lines.length<5){
			
			DESC = desc;
			return ;
		}
		StringBuffer sb = new StringBuffer();
		sb.append(lines[0]).append("\n");
		sb.append(lines[1]).append("\n");
		sb.append(lines[2]).append("\n");
		sb.append(lines[3]).append("\n");
		
		Pattern p1 = Pattern.compile(lines[0]);
		Pattern p2 = Pattern.compile(lines[1]);
		Pattern p3 = Pattern.compile(lines[2]);
		Pattern p4 = Pattern.compile(lines[3]);
		for(int i=4; i<lines.length; i++){
			if(		p1.matcher(lines[i]).find() || 
					p2.matcher(lines[i]).find() || 
					p3.matcher(lines[i]).find() ||
					p4.matcher(lines[i]).find()){
				
			}else{
				sb.append(lines[i]).append("\n");
			}
			
			
			
		}
		DESC = sb.toString();
		
		
	}

	public String getTRVDLINE() {
		return TRVDLINE;
	}

	public void setTRVDLINE(String trvdline) {
		TRVDLINE = trvdline;
	}
	
	public String toBaseXml(){
		StringBuffer sb = new StringBuffer();
		
		//行号 承运人	单程票价	往返票价	票价级别	旅行代码	舱位	最长停留	最短停留	生效日期	截止日期	
		
		sb.append("<LN>").append(LN).append("</LN>");
		sb.append("<CXR>").append(CXR).append("</CXR>");
		sb.append("<OW>").append(OW).append("</OW>");
		sb.append("<RT>").append(RT).append("</RT>");
		sb.append("<FBC>").append(FBC).append("</FBC>");
		sb.append("<TC>").append(TC).append("</TC>");
		sb.append("<RBD>").append(RBD).append("</RBD>");
		sb.append("<MIN>").append(MIN).append("</MIN>");
		sb.append("<MAX>").append(MAX).append("</MAX>");
		sb.append("<TRVDATESTART>").append(TRVDATESTART).append("</TRVDATESTART>");
		sb.append("<TRVDATEEND>").append(TRVDATEEND).append("</TRVDATEEND>");
		sb.append("<TRVDLINE>").append(TRVDLINE).append("</TRVDLINE>");
		
		
		return sb.toString();
	}
}
