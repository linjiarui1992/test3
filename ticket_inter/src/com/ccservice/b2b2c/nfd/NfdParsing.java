//package com.ccservice.b2b2c.nfd;
//
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.PrintStream;
//import java.sql.SQLException;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Locale;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//
//import com.ccservice.b2b2c.base.nfdspprice.Nfdspprice;
//import com.ccservice.inter.server.Server;
//
//public class NfdParsing {
//		
//	
//	public void Parse(String filename){
//		File file = new File(filename);
//		if(!file.exists()){
//			return;
//		}
//		
//		String match = NfdCollect.filetoString(file);
//		
//		String[]strs =match.split("\r");
//		Pattern p = Pattern.compile(
//				"(\\d{1,2})"+  //LN
//				"[ ]{1,3}"+
//				
//				"(\\w{2})"+  //CXR
//				"[ ]{3}"+
//				
//				"((\\d+\\.\\d+)|([ ]{6}))"+ //OW
//				"[ ]+"+
//				
//				"((\\d+\\.\\d+)|([ ]{5}))"+ //RT
//				"[ ]+"+
//				
//				"((\\w+)|(\\w+/\\w+))"+//FBC/TC
//				"[ ]*"+
//				
//				"(\\w+)"+//RBD
//				"[ ]*"+
//				
//				"((\\w+\\/\\w+)|([ ]+))"+ //MIN/MAX
//				"[ ]+"+
//				
//				"(\\w+-\\w*)"+ //TRVDATE
//				"[ ]*" +
//				
//				"(NFN:\\d+)" //R
//				
//				,Pattern.MULTILINE);
//		Pattern pr = Pattern.compile("(\\w{3}-\\w{3}).*(NFR:)(\\d\\d)");
//		
//		Pattern pl = Pattern.compile("((>NFN)|(>NFR))(\\d{2})");
//		
//		Pattern page = Pattern.compile("(PAGE).*([\\d+])/([\\d+])");
//		
//		String[] ss = filename.split("-");
//		
//		NfdData data = new NfdData();
//		
//		
//		
//		
//		data.setAllString(match);
//		
//		data.setOffice(ss[0]);
//		data.setCityespair(ss[1]);
////		data.setQuerytime(ss[2]);
//		data.setCarrier(ss[2]);
//		
//		
//		if(strs!=null){
//			for(int i=0; i<strs.length;i++){
//				String s=strs[i];
//				Matcher m = p.matcher(s);
//				
//				NfdLine line = null;
//				if(m.find()){
//					
//					line = new NfdLine();
//					line.setLN(m.group(1));
//					line.setCXR(m.group(2));
//					line.setOW(m.group(3));
//					line.setRT(m.group(6));
//					line.setFBC(m.group(9));
//					line.setRBD(m.group(12));
//					line.setMIN(m.group(13));
//					line.setTRVDATESTART(m.group(16));
//					
//				}else{
//					Matcher rm = pr.matcher(s);
//					if(rm.find()){
//						System.out.println(rm.group(3));
//						try{
//						data.FindLine(rm.group(3)).setTRVDLINE(rm.group(0).substring(0,rm.group(0).indexOf(" ") ));
//						}catch (Exception e) {
//							e.printStackTrace();
//						}
//					}
//					
//				}
//				
//				if(line!=null){
//					
//					data.getLines().add(line);
//					line=null;
//				}
//				
//			}
//			
//		}
//		
//		StringBuffer sb = new StringBuffer();;
//		String curline =null;
//		
//		for(String s:strs){
//			Matcher m = pl.matcher(s);
//			if(m.find()){
//				
//			
//				String line = m.group(4);
//				if(curline!=null){
//					if(!(line.equals(curline))){
//						
//						
//						try{
//							data.FindLine(curline).setDESC(sb.toString());
//							}catch(Exception e){
//								e.printStackTrace();
//						}
//						curline = line;
//						sb=new StringBuffer();
//					}
//					
//				}else{
//					curline = line;
//				}
//				
//				
//			}else{
//				if(!(page.matcher(s).find()) && curline!=null){
//					
//					if(sb!=null){
//						sb.append(s).append("\n");
//					}
//				}
//			}
//			
//		}
//		
//		if(curline==null){
//			return;
//		}
//		try{
//		data.FindLine(curline).setDESC(sb.toString());
//		}catch(Exception e){
//			e.printStackTrace();
//		}
//		System.out.println(filename+".xml");
//		
//		//genXml(data);
//		for(NfdLine l: data.getLines()){
//			Nfdspprice nfd = new Nfdspprice();
//			
//			nfd.setAircomcode(data.getCarrier());
//			nfd.setCabincode(l.getRBD());
//			nfd.setAllnfninfo(l.getDESC());
////			nfd.setChangerule(l.getDESC());
//			nfd.setLinenumber(l.getLN());
//			String cityp = data.getCityespair();
//			nfd.setStartaircode(cityp.substring(0,3));
//			nfd.setEndaircode(cityp.substring(3));
//			
//			String travle = l.getTRVDATESTART();
//			String[] ts = travle.split("-");
//			
//			if(ts.length==1){
//				try {
//					nfd.setStartdate(new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("ddMMMyy",Locale.ENGLISH).parse(ts[0])));
//				} catch (ParseException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				nfd.setEnddate("2011-12-12");
//			}else{
//				try {
//					nfd.setStartdate(new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("ddMMMyy",Locale.ENGLISH).parse(ts[0])));
//				} catch (ParseException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				try {
//					nfd.setEnddate(new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("ddMMMyy",Locale.ENGLISH).parse(ts[1])));
//				} catch (ParseException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				
//			}
//			
//			try{
//			nfd.setRtprice(Double.parseDouble(l.getRT()));
//			}catch (Exception e) {
//				// TODO: handle exception
//			}
//			
//			try{
//				nfd.setOwprice(Double.parseDouble(l.getOW()));
//			}catch (Exception e) {
//					// TODO: handle exception
//			}
//			nfd.setPricelevel(l.getFBC());
//			
//				
//			System.out.println(nfd);
//			try {
//				
//				Server.getInstance().getAirService().createNfdspprice(nfd);
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//		
//		
//	}
//	
//	
//	
//	
//	public void genXml(NfdData data){
//		FileOutputStream out =null;
//		try {
//			out = new FileOutputStream(data.getCityespair()+"-"+data.getQuerytime()+"-"+data.getCarrier()+".xml");
//			PrintStream ps = new PrintStream(out);
//			
//			ps.println("<?xml version=\"1.0\" encoding=\"gb2312\" ?>");
//			ps.println("<root>");
//			ps.println("	<office>"+data.getOffice()+"</office>");
//			ps.println("	<citypair>"+data.getCityespair()+"</citypair>");
//			ps.println("	<lines>");
//			for(NfdLine line:data.getLines()){
//				
//				ps.println("		<line>");
//					ps.println(line.toBaseXml());
//				
//				ps.println("		<desc><![CDATA["+line.getDESC()+"]]></desc>");
//				
//				ps.println("		</line>");
//				
//				
//			}
//			ps.println("	</lines>");
//			ps.println("</root>");
//			ps.close();
//		
//		} catch (FileNotFoundException e) {
//			
//			e.printStackTrace();
//		}finally{
//			try {
//				out.close();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//		
//		
//		
//	}
//	
//	public void genXls(NfdData data){
//		
//	}
//	
//	
//	
//	public static void main(String[] args) {
//	
//		
//		new NfdParsing().Parse("nfd-BHYBAV-02mar-ca");
//	
//		
///*	
////		String match = NfdClient.filetoString(new File("nfd-nkgpek-27sep-mu"));
//		String match = NfdCollect.filetoString(new File("nfd-peksha-27sep-ca"));
//		
//		
//	//	Pattern p = Pattern.compile("(PAGE).*([\\d+])/([\\d+])"); //�Ƿ�����ҳ
//		
//		
//		String[]strs =match.split("\r");
//	
////		06  MU   350.00            G           G   00D/00D 11MAR10-31DEC10  NFN:06  
////		18  MU            800.00   YW          W   00D/00D 11MAR10-31DEC10  NFN:18  
// //LN  CXR  OW       RT       FBC/TC      RBD MIN/MAX TRVDATE         R 	 	
//		
//		Pattern p = Pattern.compile(
//				"(\\d{1,2})"+  //LN
//				"[ ]{1,3}"+
//				
//				"(\\w{2})"+  //CXR
//				"[ ]{3}"+
//				
//				"((\\d+\\.\\d+)|([ ]{6}))"+ //OW
//				"[ ]+"+
//				
//				"((\\d+\\.\\d+)|([ ]{5}))"+ //RT
//				"[ ]+"+
//				
//				"((\\w+)|(\\w+/\\w+))"+//FBC/TC
//				"[ ]*"+
//				
//				"(\\w+)"+//RBD
//				"[ ]*"+
//				
//				"((\\w+\\/\\w+)|([ ]+))"+ //MIN/MAX
//				"[ ]+"+
//				
//				"(\\w+-\\w*)"+ //TRVDATE
//				"[ ]*" +
//				
//				"(NFN:\\d+)" //R
//				
//				,Pattern.MULTILINE);
//	
//		Pattern pr = Pattern.compile("(\\w{3}-\\w{3}).*(NFR:\\d+)");
//  	 	
//		if(strs!=null){
//			for(int i=0; i<strs.length;i++){
//				String s=strs[i];
//				Matcher m = p.matcher(s);
//				Matcher rm = pr.matcher(s);
//				if(m.find()){
//					//System.out.println(s);
//					//for(int i=0; i<m.groupCount(); i++)
//					{
//						System.out.println(m.group(0));
//						
//					}
//				}else{
//					if(rm.find()){
//						
//						System.out.println(rm.group(0));
//					}
//					
//				}
//				
//			}
//			
//		}
//		
//		*/
//	}
//}
