package com.ccservice.meituan.train;

import java.net.MalformedURLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;




import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.offline.util.DelieveUtils;
import com.ccservice.b2b2c.atom.service12306.TimeUtil;

/**
 * 美团增加订单的线程
 * 
 * @author zhangruixuan
 * 
 * 2017-08-24
 */
public class MyThreadMeiTuanAddOrderOfflin extends Thread {

	public JSONObject json;

	public long startThreadcurrentTime;

	public MyThreadMeiTuanAddOrderOfflin(JSONObject json) {
		this.json = json;
	}

	public void run() {
		try {
			WriteLog.write("MyThreadMeiTuanAddOrderOfflin",
					startThreadcurrentTime + "请求进入thread进行入库处理:"+json.toString());
			//获取参数
			String MTOrderNumber = json.getString("orderid");	//美团订单编号
			//String order_type = json.getString("order_type");	//订单类型，paied:先支付后占座，unpaied:先占座后支付
			String train_code = json.getString("train_code");	//车次
			String from_station_code = json.getString("from_station_code");	//出发站简码
			String from_station_name1 = json.getString("from_station_name");	//出发站名称
			String to_station_code = json.getString("to_station_code");		//到达站简码
			String to_station_name1 = json.getString("to_station_name");		//到达站名称
			String train_date1 = json.getString("train_date");				//乘车日期
			String req_token = json.getString("req_token");
			String choose_no_seat = json.getString("choose_no_seat");				//是否出无座票  true:不出无座票  false:允许出无座票		
			JSONArray passengers = json.getJSONArray("passengers");					//乘客信息
			String paper_type1 = json.getString("paper_type");			//纸质票类型(0：普通，1：靠窗，2：连座，3：过道，4：下铺，5：中铺，6：上铺，7：连铺包间)
			int paper_low_seat = json.getIntValue("paper_low_seat");						//定制坐席的最少数量		定制服务使用的
			String paper_backup1 = json.getString("paper_backup");							//当选择的纸质票定制坐席无票时，是否接收其他坐席(1：接收，0：不接收)
			String paper_transport_name = json.getString("paper_transport_name");		//纸质票配送收件人姓名
			String paper_transport_phone = json.getString("paper_transport_phone");		//纸质票配送收件人电话
			String paper_transport_address = json.getString("paper_transport_address");	//纸质票配送收件人地址		
			String train_date = train_date1+" 00:00:00";
			String from_station_name = from_station_name1+"("+from_station_code+")";
			String to_station_name = to_station_name1+"("+to_station_code+")";
			String paper_int = "";
			String paper_backup_int = "";
			if ("true".equals(choose_no_seat)) {
				paper_backup_int = "0";
			}else if("false".equals(choose_no_seat)){
				paper_backup_int = "1";
			}
			if (train_code.contains("D")||train_code.contains("G")) {
				String paper_msg = json.containsKey("select_seats")?json.get("select_seats").toString():"";
				if (paper_msg.contains("A")) {
					paper_int = paper_int+"1";
				}
				if (paper_msg.contains("B")) {
					paper_int = paper_int+"2";
				}
				if (paper_msg.contains("C")) {
					paper_int = paper_int+"3";
				}
				if (paper_msg.contains("D")) {
					paper_int = paper_int+"4";
				}
				if (paper_msg.contains("E")) {
					paper_int = paper_int+"5";
				}
				if (paper_msg.contains("F")) {
					paper_int = paper_int+"6";
				}
			}
			String paper_type_str = "1" + paper_type1 + paper_int;
		    String paper_backup_str = "1" + paper_backup1 + paper_backup_int;
			int paper_type = Integer.parseInt(paper_type_str);
			int paper_backup = Integer.parseInt(paper_backup_str);
			//下订单
			Trainorder trainorder = new Trainorder();
			Float price = 0f ;
								
			List<Trainpassenger> trainplist = new ArrayList<Trainpassenger>();	//乘客信息	
			for (int i = 0; i < passengers.size(); i++) {
				List<Trainticket> traintickets = new ArrayList<Trainticket>();
															
				Trainpassenger trainpassenger = new Trainpassenger();	//乘客对象
				Trainticket trainticket = new Trainticket();			//车票对象
				price = Float.valueOf(passengers.getJSONObject(0).getString("price"));			
				//乘客信息
				trainpassenger.setIdnumber(passengers.getJSONObject(i).getString("certificate_no"));	//证件号码
				String idtypestr = passengers.getJSONObject(i).getString("certificate_type");
		        if ("1".equals(idtypestr)) {
		          trainpassenger.setIdtypeval("1");
		        } else if ("C".equals(idtypestr)) {
		          trainpassenger.setIdtypeval("2");
		        } else if ("G".equals(idtypestr)) {
		          trainpassenger.setIdtypeval("3");
		        } else if ("B".equals(idtypestr)) {
		          trainpassenger.setIdtypeval("4");
		        }
				trainpassenger.setName(passengers.getJSONObject(i).getString("passenger_name"));	//乘客姓名
				trainpassenger.setPassengerid(passengers.getJSONObject(i).getString("passengerid"));//乘客id								
				//车票信息
				trainticket.setDeparttime(train_date);				//乘车日期
				trainticket.setDeparture(from_station_name);		//始发站
				trainticket.setArrival(to_station_name);			//终点站
				trainticket.setStatus(Trainticket.WAITISSUE);		//车票状态  2 等待出票
				trainticket.setTickettype(Integer.parseInt(passengers.getJSONObject(i).getString("ticket_type")));;//1:成人票，2:儿童票，3:学生票，4:残军票
				trainticket.setPrice(Float.valueOf(passengers.getJSONObject(i).getString("price")));		//票价		
				trainticket.setPayprice(Float.valueOf(passengers.getJSONObject(i).getString("price")));			    
			    trainticket.setTrainno(train_code);			//车次
			    String seat_type = passengers.getJSONObject(i).getString("seat_type");
			    String seat = getSeat(seat_type);
				trainticket.setSeattype(seat);	//座位编码（席别）				
			    trainpassenger.setTraintickets(traintickets);
				traintickets.add(trainticket);
				trainplist.add(trainpassenger);										
			}								
			trainorder.setTaobaosendid("无");						//备选席别
			trainorder.setQunarOrdernumber(MTOrderNumber);			//美团订单号
			trainorder.setTradeno(req_token);						//标识
			int ticketcount = trainplist.size();						
			float orderprice  = price*ticketcount;					//订单金额      票数*票价			
			//定制服务使用
			trainorder.setPaymethod(paper_type);					//纸质票类型(0：普通，1：靠窗，2：连座，3：过道，4：下铺，5：中铺，6：上铺，7：连铺包间)
			trainorder.setSupplypayway(paper_backup);				//当选择的纸质票定制坐席无票时，是否接收其他坐席(1：接收，0：不接收)
			trainorder.setRefuseaffirm(paper_low_seat);				//定制坐席的最少数量			
			trainorder.setContactuser(paper_transport_name);		//收件人姓名
			trainorder.setContacttel(paper_transport_phone);		//线下票收件电话
			trainorder.setControlname(paper_transport_phone);		//处理人员联系电话
			trainorder.setTicketcount(ticketcount);					//票数
			trainorder.setCreateuid(70l);							//代理商id
			trainorder.setInsureadreess(paper_transport_address);	//邮寄地址
			trainorder.setOrderprice(orderprice);					//订单金额			
			trainorder.setIsjointtrip(0);
			trainorder.setState12306(Trainorder.WAITORDER);			//12306订单状态
			trainorder.setPassengers(trainplist);					//乘客信息
			trainorder.setOrderstatus(Trainorder.WAITISSUE);		//订单状态	WAITISSUE = 2;等待出票
			trainorder.setAgentprofit(0f);							//代理利润
			trainorder.setCommission(0f);							//支付手续费
			trainorder.setTcprocedure(0f);
			trainorder.setInterfacetype(7);							//接口类型
			trainorder.setCreateuser("美团");							//创建人
			//日志
			WriteLog.write("MyThreadMeiTuanAddOrderOfflin",
					startThreadcurrentTime + ":getOrdernumber:" + trainorder.getQunarOrdernumber());			
			//订单数据入库（订单表，车票表，乘客信息表）			    
			trainorderinfoadd(trainorder);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			WriteLog.write("MyThreadMeiTuanAddOrderOfflin", 
			        this.startThreadcurrentTime + "解析数据异常" + e.getMessage());			
		}
		
	}

	/**
	 * 订单信息，乘车票信息，乘客表信息入库
	 * 
	 * @param trainorder
	 */
	private void trainorderinfoadd(Trainorder trainorder) {

		try {
			String agentId = "";
			// 获取邮寄地址
			String deliveryaddress = trainorder.getInsureadreess();	
			agentId = addressmatchingoutlets(deliveryaddress);
			// 订单信息入库拼接存储过程
			String sp_TrainOrderOffline_insert = getCreateTrainorderOfficeProcedureSql(trainorder, agentId);
			WriteLog.write("MyThreadMeiTuanAddOrderOfflin", startThreadcurrentTime + ":trainorderofflineadd:"
					+ "TrainOrderOffline_insert_meituan保存订单存储过程" + sp_TrainOrderOffline_insert);				
			// 订单信息入库
			List list = getSystemServiceOldDB().findMapResultByProcedure(sp_TrainOrderOffline_insert);
			Map map = (Map) list.get(0);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			//主键id
			String orderid = map.get("id").toString();
			// 通过出票点和邮寄地址获取预计到达时间results
			String delieveStr = "暂无快递信息!";

			try {
				// 调顺风接口，返回快递信息
				delieveStr = DelieveUtils.getDelieveStr(agentId,trainorder.getInsureadreess());
			} catch (Exception e) {
				e.printStackTrace();
			}
			// 更新订单快递时效信息
			String updatesql = "UPDATE TrainOrderOffline SET telephone='"+ trainorder.getControlname() + "',expressDeliver ='"
					+ delieveStr + "' WHERE ID=" + orderid;
			WriteLog.write("MyThreadMeiTuanAddOrderOfflin", startThreadcurrentTime + ":trainorderofflineadd:"
					+ "TrainOrderOfflineOffline_保存快递时效信息" + updatesql);
			getSystemServiceOldDB().excuteAdvertisementBySql(updatesql);

			// 插入操作记录信息 订单发放成功，等待处理...
			String procedureRecord = "sp_TrainOrderOfflineRecord_insert @FKTrainOrderOfflineId="
					+ orderid
					+ ",@ProviderAgentid="
					+ agentId
					+ ",@DistributionTime='"
					+ sdf.format(new Date())
					+ "',@ResponseTime='',@DealResult=0,@RefundReason=0,@RefundReasonStr=''";
			getSystemServiceOldDB().findMapResultByProcedure(procedureRecord);
			WriteLog.write("MyThreadMeiTuanAddOrderOfflin", startThreadcurrentTime + ":trainorderofflineadd:" 
					+ "sp_TrainOrderOfflineRecord_insert_记录表存储过程"
					+ procedureRecord);

			// 保存快递信息[MailAddress]
			String sp_MailAddress_insert = getMailAddressProcedureSql(trainorder, orderid);
			WriteLog.write("MyThreadMeiTuanAddOrderOfflin", startThreadcurrentTime + ":trainorderofflineadd:"
					+ "TrainOrderOfflineMailAddress_insert_MeiTuan保存邮寄地址存储过程"
					+ sp_MailAddress_insert);
			//保存快递信息，插入数据库
			getSystemServiceOldDB().findMapResultByProcedure(sp_MailAddress_insert);
			for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
				String sp_TrainPassengerOffline_insert = getCreateTrainpassengerOfficeProcedureSql( trainpassenger, orderid);
				//日志
				WriteLog.write("MyThreadMeiTuanAddOrderOfflin",startThreadcurrentTime + ":trainorderofflineadd:"
								+ "TrainOrderOfflinePassengerOffline_insert_MeiTuan保存乘客存储过程" + sp_TrainPassengerOffline_insert);
				
				List listP = getSystemServiceOldDB().findMapResultByProcedure(sp_TrainPassengerOffline_insert);
				
				Map map2 = (Map) listP.get(0);
				String trainPid = map2.get("Id").toString();
				for (Trainticket ticket : trainpassenger.getTraintickets()) {
					//车票信息的存储过程
					String sp_TrainTicketOffline_insert = getCreateTrainticketOfficeProcedureSql(ticket, trainPid, orderid);
					WriteLog.write("MyThreadMeiTuanAddOrderOfflin",startThreadcurrentTime + ":"
									+ "TrainOrderOfflineTicketOffline_insert_MeiTuan保存车票存储过程" + sp_TrainTicketOffline_insert);
					// 车票信息入库
					getSystemServiceOldDB().findMapResultByProcedure(sp_TrainTicketOffline_insert);
				}
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			WriteLog.write("MyThreadMeiTuanAddOrderOfflin", startThreadcurrentTime + ":订单入库异常捕获"+e.getMessage());
		}

	}

	/**
	 * 车票信息的存储过程
	 * 
	 * @param ticket
	 * @param trainPid
	 * @param orderid
	 * @return
	 */
	public String getCreateTrainticketOfficeProcedureSql(Trainticket ticket,
			String trainPid, String orderid) {
		StringBuffer procedureSql = new StringBuffer();
		procedureSql.append("sp_TrainTicketOffline_insert ");		
		Timestamp departTime = Timestamp.valueOf(ticket.getDeparttime());
		String starttime = ticket.getDeparttime().substring(11, 16);
		String departure = ticket.getDeparture();
		String arrival = ticket.getArrival();
		String trainno = ticket.getTrainno();
		int tickettype = ticket.getTickettype();
		String seattype = ticket.getSeattype();
		String seatno = ticket.getSeatno();
		String coach = ticket.getCoach();
		float price = ticket.getPrice();
		String costtime ="0";
		if (ticket.getCosttime()!= null && "".equals(ticket.getCosttime())) {
			costtime = ticket.getCosttime();
		}
		String arrivaltime = "";
		if (ticket.getArrivaltime()!=null) {
			arrivaltime = ticket.getArrivaltime().substring(11, 16);
		}		
		String suborderid = ticket.getTicketno();
		procedureSql.append("@TrainPid= " + trainPid + ",");
		procedureSql.append("@OrderId= " + orderid + ",");
		procedureSql.append("@DepartTime= '"+departTime+"',");
		procedureSql.append("@Departure ='" + departure + "',");
		procedureSql.append("@Arrival ='" + arrival + "',");
		procedureSql.append("@TrainNo ='" + trainno + "',");
		procedureSql.append("@TicketType =" + tickettype + ",");
		procedureSql.append("@SeatType ='" + seattype + "',");
		procedureSql.append("@SeatNo ='" + seatno + "',");
		procedureSql.append("@Coach ='" + coach + "',");
		procedureSql.append("@Price =" + price + ",");
		procedureSql.append("@CostTime ='" + costtime + "',");
		procedureSql.append("@StartTime='" + 0 + "',");
		procedureSql.append("@ArrivalTime ='" + arrivaltime + "',");
		if (suborderid == null || "".equals(suborderid)) {
			procedureSql.append("@SubOrderId ='0'");
		} else {
			procedureSql.append("@SubOrderId ='" + suborderid + "'");
		}
		return procedureSql.toString();
	}

	/**
	 * 乘客信息的存储过程
	 * 
	 * @param trainpassenger
	 * @param orderid
	 * @return
	 */
	public String getCreateTrainpassengerOfficeProcedureSql(
			Trainpassenger trainpassenger, String orderid) {
		StringBuffer procedureSql = new StringBuffer();
		procedureSql.append("[OFFLINE_addTrainPassengerOffline] ");
		// Long OrderId = trainPassengerOffline.getorderid();
		procedureSql.append("@OrderId = " + orderid + ",");
		String Name = trainpassenger.getName();
		procedureSql.append("@Name = '" + Name + "',");
		String IdType = trainpassenger.getIdtypeval();
		procedureSql.append("@IdType = " + IdType + ",");
		String IdNumber = trainpassenger.getIdnumber();
		procedureSql.append("@IdNumber = '" + IdNumber + "',");
		String passengerid = trainpassenger.getPassengerid();
		procedureSql.append("@PassengerId = '" + passengerid + "'");
		return procedureSql.toString();
	}

	/**
	 * 保存快递信息的存储过程
	 * @param trainorder
	 * @param orderid
	 * @return
	 */
	public String getMailAddressProcedureSql(Trainorder trainorder,
			String orderid) {
		StringBuffer procedureSql = new StringBuffer();
		procedureSql.append("sp_MailAddress1_insert ");
		String ProvinceName = "";
		String CityName = "";
		String RegionName = "";
		String TownName = "";
		String Mail = "";
		String Note = "";
		String Busstype = "0";
		procedureSql
				.append("@MailName= '" + trainorder.getContactuser() + "',");
		procedureSql.append("@MailTel= '" + trainorder.getContacttel() + "',");
		procedureSql.append("@Postcode= '100000',");
		procedureSql.append("@Address =\"" + trainorder.getInsureadreess()
				+ "\",");
		procedureSql.append("@ProvinceName ='" + ProvinceName + "',");
		procedureSql.append("@CityName ='" + CityName + "',");
		procedureSql.append("@RegionName ='" + RegionName + "',");
		procedureSql.append("@TownName ='" + TownName + "',");
		procedureSql.append("@Orderid ='" + orderid + "',");
		procedureSql.append("@Mail='" + Mail + "',");
		procedureSql.append("@Note='" + Note + "',");
		procedureSql.append("@Busstype=" + Busstype);
		return procedureSql.toString();
	}

	/**
	 * 调用顺风接口
	 * 
	 * @param agengId
	 * @param address
	 * @return results
	 */
	public String getDelieveStr(String agengId, String address) {
		String results = "";
		String fromcode = "010";
		String tocode = getExpressCodes(address);
		String time1 = "10:00:00";
		String time2 = "18:00:00";

		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		String dates = sdf.format(new Date());
		String sql1 = "SELECT fromcode,time1,time2 from TrainOrderAgentTimes with(nolock) where agentId="
				+ agengId;
		List list = getSystemServiceOldDB().findMapResultBySql(sql1, null);
		if (list.size() > 0) {
			Map map = (Map) list.get(0);
			fromcode = map.get("fromcode").toString();
			time1 = map.get("time1").toString();
			time2 = map.get("time2").toString();
		}
		String realTime = getRealTimes(dates, time1, time2);// 获取快递时间
		String urlString = PropertyUtil.getValue("expressDeliverUrl",
				"train.properties");
		String param = "times=" + realTime + "&fromcode=" + fromcode
				+ "&tocode=" + tocode;
		WriteLog.write("MyThreadMeiTuanAddOrderOfflin", startThreadcurrentTime + ":TrainOrderOfflineOffline_保存快递时效信息" + "agengId=" + agengId
				+ "------->" + "address=" + address + "---------->" + urlString
				+ "?" + param);

		String result = SendPostandGet.submitPost(urlString, param, "UTF-8").toString();
		WriteLog.write("MyThreadMeiTuanAddOrderOfflin", startThreadcurrentTime + ":result=" + result);

		if (result.contains("OK") && result.contains("deliver_time")) {
			try {
				Document document = DocumentHelper.parseText(result);
				Element root = document.getRootElement();
				if (root != null) {
					Element head = root.element("Head");
					Element body = root.element("Body");
					if ("OK".equals(root.elementText("Head"))) {
						Element deliverTmResponse = body
								.element("DeliverTmResponse");
						Element deliverTm = deliverTmResponse
								.element("DeliverTm");
						String business_type_desc = deliverTm
								.attributeValue("business_type_desc");
						String deliver_time = deliverTm
								.attributeValue("deliver_time");
						String business_type = deliverTm
								.attributeValue("business_type");
						results = "如果" + realTime + "正常发件。快递类型为:"
								+ business_type_desc + "。快递预计到达时间:"
								+ deliver_time
								+ "。以上时效为预计到达时间，仅供参考，精准时效以运单追踪结果中的“预计到达时间”为准。";
					}
				}
			} catch (DocumentException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			results = "暂无快递信息!";
		}

		return results;
	}

	/**
	 * 获取取快递时间
	 * 
	 * @param dates
	 * @param time1
	 * @param time2
	 * @return
	 */
	public String getRealTimes(String dates, String time1, String time2) {
		String result = "";
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String realDates = sdf1.format(new Date());
		try {
			Date date0 = sdf.parse(dates);
			Date date1 = sdf.parse(time1);
			Date date2 = sdf.parse(time2);
			if (date0.before(date1)) {
				result = (realDates.substring(0, 10) + " " + sdf.format(date1));
			} else if (date0.after(date1) && date0.before(date2)) {
				result = (realDates.substring(0, 10) + " " + sdf.format(date2));
			} else if (date0.after(date2)) {
				Date ds = getDate(new Date());
				String nextd = sdf1.format(ds);
				result = (nextd.substring(0, 10) + " " + sdf.format(date1));
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return result;
	}

	public Date getDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, 1);
		Date date1 = new Date(calendar.getTimeInMillis());
		return date1;
	}

	/**
	 * 通过存储过程获取乘客地址的citycode
	 * 
	 * @param address
	 * @return
	 */
	public String getExpressCodes(String address) {
		String procedure = "sp_TrainOfflineExpress_getCode @address=\""
				+ address + "\"";
		List list = getSystemServiceOldDB().findMapResultByProcedure(procedure);
		String cityCode = "010";
		if (list.size() > 0) {
			Map map = (Map) list.get(0);
			cityCode = map.get("CityCode").toString();
		}
		return cityCode;
	}

	/**
	 * 订单信息入库的存储过程
	 * @param trainorder
	 * @param agentId
	 * @return
	 */
	private String getCreateTrainorderOfficeProcedureSql(Trainorder trainorder,
			String agentId) {
		StringBuffer procedureSql = new StringBuffer();
		procedureSql.append("sp_TrainOrderOffline_insert1 ");
		procedureSql.append("@OrderNumber = N'"
				+ trainorder.getQunarOrdernumber() + "',");
		procedureSql.append("@AgentId = " + agentId + ",");
		String CreateUser = trainorder.getCreateuser();
		String ContactUser = trainorder.getContactuser();
		String ContactTel = trainorder.getContacttel();
		Float OrderPrice = trainorder.getOrderprice();
		Float AgentProfit = trainorder.getAgentprofit();
		Integer TicketCount = trainorder.getTicketcount();
		Timestamp outtime = trainorder.getOrdertimeout();
		String tradeno = trainorder.getTradeno();
		procedureSql.append("@CreateUId = " + trainorder.getCreateuid() + ",");
		procedureSql.append("@CreateUser = N'" + CreateUser + "',");
		procedureSql.append("@ContactUser = N'" + ContactUser + "',");
		procedureSql.append("@ContactTel = N'" + ContactTel + "',");
		procedureSql.append("@OrderPrice = " + OrderPrice + ",");
		procedureSql.append("@AgentProfit = " + AgentProfit + ",");
		procedureSql.append("@TicketCount = " + TicketCount + ",");
		procedureSql.append("@Paystatus = " + 1 + ",");
		procedureSql.append("@OrderNumberOnline = '"+ trainorder.getQunarOrdernumber() + "',");
		procedureSql.append("@PaperType = " + trainorder.getPaymethod() + ",");
		procedureSql.append("@PaperBackup = " + trainorder.getSupplypayway() + ",");
		procedureSql.append("@paperLowSeatCount = " + trainorder.getRefuseaffirm() + ",");
		procedureSql.append("@extSeat = '" + trainorder.getTaobaosendid()
				+ "',");
		procedureSql.append("@TradeNo = '" + tradeno + "',");
		if (outtime == null || "".equals(outtime)) {
			procedureSql.append("@OrderTimeout = ''");
		} else {
			procedureSql.append("@OrderTimeout = '" + outtime + "'");
		}

		return procedureSql.toString();

	}

	/**
	 * 根据邮寄地址匹配代售点
	 */
	private String addressmatchingoutlets(String deliveryaddress) {
		WriteLog.write("MyThreadMeiTuanAddOrderOfflin",
				startThreadcurrentTime + ":------(邮寄地址:)" + deliveryaddress+",  根据邮寄地址匹配代售点开始执行分单规则");
		// 默认出票点
		String sql1 = "SELECT agentId FROM TrainOfflineMatchAgent WHERE status=2 AND createUid="
				+ "56";
		List list1 = getSystemServiceOldDB().findMapResultBySql(sql1, null);
		WriteLog.write("MyThreadMeiTuanAddOrderOfflin",
				startThreadcurrentTime + ":------(邮寄地址:)" + deliveryaddress+",  程序默认分配出票点"+list1);
		String agentId = "378";
		if (list1.size() > 0) {
			Map map = (Map) list1.get(0);
			agentId = map.get("agentId").toString();
		}
		// 程序自动分配出票点
		String sql2 = "SELECT * FROM TrainOfflineMatchAgent WHERE status=1 AND createUid="
				+ "56";
		WriteLog.write("MyThreadMeiTuanAddOrderOfflin",
				startThreadcurrentTime + ":------(邮寄地址:)" + deliveryaddress+",  程序自动分配出票点sql"+sql2);
		List list2 = getSystemServiceOldDB().findMapResultBySql(sql2, null);
		WriteLog.write("MyThreadMeiTuanAddOrderOfflin",
				startThreadcurrentTime + ":------(邮寄地址:)" + deliveryaddress+",  程序自动分配出票点sql查询结果"+list2);
		List listAgents = new ArrayList();
		for (int i = 0; i < list2.size(); i++) {
			Map mapp = (Map) list2.get(i);
			String provinces = mapp.get("provinces").toString();
			String agentid = mapp.get("agentId").toString();
			String[] add = provinces.split(",");
			for (int j = 0; j < add.length; j++) {
				boolean flag = false;
				if (deliveryaddress.startsWith(add[j])) {
					listAgents.add(agentid);
					flag = true;
				}
				if (flag) {
					break;
				}
			}
		}
		WriteLog.write("MyThreadMeiTuanAddOrderOfflin",
				startThreadcurrentTime + ":------(邮寄地址:)" + deliveryaddress+",  查询出匹配符合出售点的listAgents的集合"+listAgents);
		if (listAgents.size() > 0) {
			int max = listAgents.size();
			int min = 1;
			Random random = new Random();
			int s = random.nextInt(max) % (max - min + 1) + min;
			if (s > 0 && s <= listAgents.size()) {
				agentId = listAgents.get(s - 1) + "";
			}
		}
		WriteLog.write("MyThreadMeiTuanAddOrderOfflin", "匹配的agentId=" + agentId + ";邮寄地址address1="
				+ deliveryaddress);
		return agentId;
	}

	private String getSeat(String seat_type) {
				
		String seat = "";
		int parseInt = Integer.parseInt(seat_type);
		switch (parseInt) {
		case 1:
			seat = "硬座";
			break;
		case 2:
			seat = "硬卧上";
			break;
		case 3:
			seat = "硬卧中";
			break;
		case 4:
			seat = "硬卧下";
			break;
		case 5:
			seat = "软座";
			break;
		case 6:
			seat = "软卧上";
			break;
		case 7:
			seat = "软卧中";
			break;
		case 8:
			seat = "软卧下";
//			break;
		case 9:
			seat = "商务座";
			break;
		case 10:
			seat = "观光座";
			break;
		case 11:
			seat = "一等包座";
			break;
		case 12:
			seat = "特等座";
			break;
		case 13:
			seat = "一等座";
			break;
		case 14:
			seat = "二等座";
			break;
		case 15:
			seat = "高级软卧上";
			break;
		case 16:
			seat = "高级软卧下";
			break;
		case 17:
			seat = "无座";
			break;
		case 18:
			seat = "一人软包";
			break;
		case 20:
			seat = "动卧";
			break;
		case 21:
			seat = "高级动卧";
			break;
		case 22:
			seat = "包厢硬卧";
			break;
		default:
			break;
		}
		return seat;

	}

	private ISystemService getSystemServiceOldDB() {
		String systemdburlString = PropertyUtil.getValue("offlineservice",
				"train.properties");
		HessianProxyFactory factory = new HessianProxyFactory();
		try {
			return (ISystemService) factory.create(ISystemService.class,
					systemdburlString + ISystemService.class.getSimpleName());
		} catch (MalformedURLException e)  {
			e.printStackTrace();
			return null;
		}
	}
	
	public static void main(String[] args) {
		
		JSONObject json = new JSONObject();
		HashMap map = new HashMap();
		HashMap map1 = new HashMap();
		ArrayList paper_msg = new ArrayList();
		JSONArray passengers = new JSONArray();
		json.put("callback_url", "http://i.meituan.com/uts/train/200/ticketNotify");
		json.put("choose_no_seat", true);
		json.put("from_station_code", "XA");
		json.put("from_station_name", "西安");
		json.put("is_paper", true);
		json.put("orderid", "46712543423");
		json.put("paper_backup", 0);
		json.put("paper_low_seat", 0);
		json.put("paper_transport_address", "湖北省武汉市武昌区紫阳街办事处中山路685号蓬源宾馆园内武汉爱乐文化艺术学校");
		json.put("paper_transport_name", "王五");
		json.put("paper_transport_phone", "18872132787");
		json.put("paper_type", "1");
		json.put("req_token", "5974829a85c171851c9e299c0a18ff5c");
		json.put("reqtime", "20170815122346");
		json.put("to_station_code", "BJ");
		json.put("to_station_name", "北京");
		json.put("train_code", "D2266");
		json.put("train_date", "2017-09-01");
		map.put("certificate_name", "二代身份证");
		map.put("certificate_no", "422828200010050025");
		map.put("certificate_type", "1");
		map.put("passenger_name", "王五");
		map.put("passengerid", 43480200);
		map.put("price", "148.00");
		map.put("seat_type", "14");
		map.put("ticket_type", "1");
		map1.put("certificate_name", "二代身份证");
		map1.put("certificate_no", "422828200010524687");
		map1.put("certificate_type", "1");
		map1.put("passenger_name", "张三");
		map1.put("passengerid", 43480201);
		map1.put("price", "148.00");
		map1.put("seat_type", "14");
		map1.put("ticket_type", "1");
		paper_msg.add("A");
		paper_msg.add("D");
		
		passengers.add(map);
		passengers.add(map1);
		json.put("passengers", passengers);
		json.put("paper_msg", paper_msg);
		
				
		new MyThreadMeiTuanAddOrderOfflin(json).start();;
	}
	

}
