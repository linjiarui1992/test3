package com.ccservice.meituan.train;

import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.alibaba.fastjson.JSONObject;
import com.callback.SendPostandGet;
import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.elong.inter.PropertyUtil;
import com.tenpay.util.MD5Util;

public class MeiTuanUpdateExpressJob implements Job {

	
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		//查询新建表，主键，订单id，美团订单号，快递单号，快递状态（0：已打包，1：已派送，2：派送中，3：已签收）,快递公司（0：顺丰速运，2：EMS），快递物流版本号，快递路由信息，快递时间路由，备用字段remark1，remark2，remark3）
		//查询条件（快递状态0：已打包，1：已派送，2：派送中）
		String sql = "select ExpressTime,ExpressInfo,ExpressAgent,Version,ExpressNum,OrderNumberOnline from ExpressInfoRoute where Status in ('0','1','2')";
		List list = getSystemServiceOldDB().findMapResultBySql(sql, null);		
		String urlString = PropertyUtil.getValue("expressRouteUrl", "train.properties");	//快递路由地址		
		String MeiTuanRequestUrl = PropertyUtil.getValue("MeiTuanRequestUrl", "train.properties");		
		String updateTransporturl = MeiTuanRequestUrl+"updateTransport";					//美团邮寄回调url
		try {
			if (list.size()>0) {
				for (int a = 0; a < list.size(); a++) {
					Map map = (Map) list.get(a);	
					JSONObject json = new JSONObject();
					//美团订单号
					String orderNumberOnline = (String) map.get("OrderNumberOnline");		
					//快递路由信息
					String expressInfo = (String) map.get("ExpressInfo");
					//快递公司
					String expressAgent = (String) map.get("ExpressAgent");					
					//快递路由版本
					int version = (int) map.get("Version");
					//快递路由时间
					String expresstime = (String) map.get("ExpressTime");
					//快递单号
					String expressNum = (String) map.get("ExpressNum");
					//请求参数
					String param = "expressno=" + expressNum;
					String key = "";				
					String routeInfo = "";
			    	String routetime = "";
					//发起请求调用快递路由接口
					String result = SendPostandGet.submitPost(urlString, param, "UTF-8").toString();
					WriteLog.write("线下票__获取快递路由信息", "express:" + expressNum + "urlString:" + urlString + "-->param:" + param
			                + "-->result:" + result);
					try {
			            Document document = DocumentHelper.parseText(result);
			            Element root = document.getRootElement();
			            Element head = root.element("Head");
			            Element body = root.element("Body");
			            Element routeResponse = body.element("RouteResponse");
			            if ("OK".equals(root.elementText("Head")) && routeResponse != null) {
			                List elements = routeResponse.elements("Route");
			                if (elements.size() > 0) {
			                	
			                    for (int i = 0; i < elements.size(); i++) {
			                        Element route = (Element) elements.get(i);
			                        routetime = route.attributeValue("accept_time");		//快递时间
			                        routeInfo = route.attributeValue("remark");				//快递路由信息		                           
			                    }
			                }
			            }
			                
					}catch (DocumentException e) {
			            e.printStackTrace();
			        }
					int newversion=0;
					if (!expressInfo.equals(routeInfo)&&!"".equals(routeInfo)&&routeInfo!=null) {										
						//修改表中的路由信息，版本号+1,快递路由时间					
						newversion = 1+version;
						String updatesql1="update TrainOrderOffline set version="+newversion+" where OrderNumberOnline="+orderNumberOnline;
	                    getSystemServiceOldDB().excuteAdvertisementBySql(updatesql1);
					}
					if (!"".equals(routeInfo)&&routeInfo.contains("已收取快件")) {					
						//修改订单状态	1：已派送,快递时间
						String updatesql1="update TrainOrderOffline set Status=1,ExpressTime="+routetime
								+",ExpressInfo="+routeInfo+" where OrderNumberOnline="+orderNumberOnline;
	                    getSystemServiceOldDB().excuteAdvertisementBySql(updatesql1);						
						
					}else if (!"".equals(routeInfo)&&((routeInfo.contains("快件到达"))
							||(routeInfo.contains("快件到达"))||(routeInfo.contains("快件在【"))||(routeInfo.contains("正在派送途中")))) {
						////修改订单状态	2：派送中	时间
						String updatesql1="update TrainOrderOffline set Status=2,ExpressTime="+routetime
								+",ExpressInfo="+routeInfo+" where OrderNumberOnline="+orderNumberOnline;
	                    getSystemServiceOldDB().excuteAdvertisementBySql(updatesql1);
						
					}else if (!"".equals(routeInfo)&&routeInfo.contains("已签收")) {
						////修改订单状态	3：已签收	时间
						String updatesql1="update TrainOrderOffline set Status=3,ExpressTime="+routetime
								+",ExpressInfo="+routeInfo+" where OrderNumberOnline="+orderNumberOnline;
	                    getSystemServiceOldDB().excuteAdvertisementBySql(updatesql1);						
					}					
					//根据美团订单号查询当前的快递路由信息
					String sql1 = "select ExpressInfo,Version,ExpressTime,Status from ExpressInfoRoute where OrderNumberOnline='"+orderNumberOnline+"'";
					List list1 = getSystemServiceOldDB().findMapResultBySql(sql1, null);
					Map map1 = (Map) list1.get(0);
					json.put("orderid", orderNumberOnline);
					json.put("transport_no", expressNum);				
					json.put("transport_time", map1.get("ExpressTime"));
					json.put("transport_company", expressAgent);
					json.put("transport_status", map1.get("Status"));
					json.put("transport_version", map1.get("Version"));
					json.put("transport_message_detail", map1.get("ExpressInfo"));
					String sing = MD5Util.MD5Encode(orderNumberOnline + expressNum + key, "UTF-8").toUpperCase(); 
			        json.put("sign", sing);        
			        if (newversion != version) {
			        	WriteLog.write("美团线下票快递路由开始请求信息", json.toString());
				        String resultjson = doJsonPost(updateTransporturl, json);
				        WriteLog.write("美团线下票快递路由return信息"+json.toString(), resultjson);
					}			                        
				}		
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			WriteLog.write("美团线下票快递路由JOB任务异常", e.toString());
		}
	}
	
	//发送post json请求
    public String doJsonPost(String url,JSONObject json){
        DefaultHttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(url);
        JSONObject responsejson =new JSONObject();
        String response = "";
        try {
            StringEntity s = new StringEntity(json.toString());
            s.setContentType("application/json;charset=UTF-8");
            post.setEntity(s);
            HttpResponse res = client.execute(post);
            if(res.getStatusLine().getStatusCode() == HttpStatus.SC_OK){
                response = EntityUtils.toString(res.getEntity());                
            }
        } catch (Exception e) {
            e.printStackTrace();
            WriteLog.write("303_MEITUAN_TRAINORDEROFFLINE_CALLBACK:" + System.currentTimeMillis(),"美团回调请求异常：" + e.toString());
            responsejson.put("success", "error");
            responsejson.put("msg", "请求美团回调异常");
            response = responsejson.toString();
        }
        return response;
    }
		
    
    
	private ISystemService getSystemServiceOldDB() {
		String systemdburlString = PropertyUtil.getValue("offlineservice",
				"Train.properties");
		HessianProxyFactory factory = new HessianProxyFactory();
		try {
			return (ISystemService) factory.create(ISystemService.class,
					systemdburlString + ISystemService.class.getSimpleName());
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		}
	}
}
