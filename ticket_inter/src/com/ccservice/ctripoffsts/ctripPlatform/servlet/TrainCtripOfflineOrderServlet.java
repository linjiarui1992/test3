/**
 * 版权所有, 空铁无忧
 * Author: 火车票 H5-微信端 项目开发组
 * copyright: 2017
 */
package com.ccservice.ctripoffsts.ctripPlatform.servlet;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccervice.util.db.DBHelperOffline;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.b2b2c.util.ExceptionUtil;
import com.ccservice.crack.ctrippffsts.TrainCtripOfflineUtil;
import com.ccservice.offline.dao.MailAddressDao;
import com.ccservice.offline.dao.TrainOrderOfflineDao;
import com.ccservice.offline.dao.TrainPassengerOfflineDao;
import com.ccservice.offline.dao.TrainTicketOfflineDao;
import com.ccservice.offline.domain.MailAddress;
import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.domain.TrainPassengerOffline;
import com.ccservice.offline.domain.TrainTicketOffline;
import com.ccservice.offline.util.ExceptionCtripUtil;
import com.ccservice.offline.util.RequestStreamUtil;
import com.ccservice.offline.util.TrainOrderOfflineUtil;

/**
 * 携程破解拉单
 * 
 * @time 2017年11月25日 下午3:57:04
 * @author liujun
 */
public class TrainCtripOfflineOrderServlet extends HttpServlet {

    private static final long serialVersionUID = -1470787851105519431L;

    private static final String logName = "线下火车票_携程下单_ticket_inter";

    private TrainOrderOfflineDao trainOrderOfflineDao = new TrainOrderOfflineDao();

    private TrainTicketOfflineDao trainTicketOfflineDao = new TrainTicketOfflineDao();

    private TrainPassengerOfflineDao trainPassengerOfflineDao = new TrainPassengerOfflineDao();

    private MailAddressDao mailAddressDao = new MailAddressDao();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //随机
        int random = new Random().nextInt(9000000) + 1000000;
        //编码
        request.setCharacterEncoding("utf-8");
        //结果
        JSONArray resResult = new JSONArray();
        try {
            // 请求参数转换
            JSONObject param = RequestStreamUtil.reqToJson(request);
            //日志
            WriteLog.write(logName, random + "---请求信息---" + param);
            if (param != null) {
                // 用户名
                String username = param.getString("username");
                TrainCtripOfflineUtil trainCtripOfflineUtil = new TrainCtripOfflineUtil();
                CloseableHttpClient defaultClient = HttpClients.createDefault();
                JSONArray reqResult = trainCtripOfflineUtil.fetchOrderList(logName, random, defaultClient, username);
                WriteLog.write(logName, random + "---拉取订单结果---" + reqResult);
                resResult = addCtripOffline(logName, random, resResult, reqResult);
            }
        }
        catch (Exception e) {
            //记录异常日志
            ExceptionUtil.writelogByException(logName, e, String.valueOf(random));
        }
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/json; charset=UTF-8");
        WriteLog.write(logName, random + "---返回结果---" + resResult);
        response.getOutputStream().write(resResult.toJSONString().getBytes(Charset.forName("UTF-8")));
    }

    /**
     * 添加订单信息
     * 
     * @param resResult
     * @param reqResult
     * @return
     * @time 2017年11月25日 上午10:52:01
     * @author liujun
     */
    private JSONArray addCtripOffline(String logName, int random, JSONArray resResult, JSONArray reqResult) {
        for (int j = 0; j < reqResult.size(); j++) {

            Boolean isSuccess = false;//程序是否执行成功
            Integer msgCode = 231099;//错误代码 true用231000,false用231099
            JSONObject jsonObject = new JSONObject();
            JSONObject reqData = reqResult.getJSONObject(j);

            WriteLog.write(logName, random + ":携程线下票出票请求-进入到方法-addCtripOffline--->reqData:" + reqData);

            // 订单号空判断
            String orderNo = reqData.getString("OrderNumber");//携程订单号 - 例:FT599U57UF210D174002304643
            jsonObject.put("hsOrderNo", orderNo);

            if (orderNo == null || orderNo.equals("")) {
                jsonObject.put("isSuccess", isSuccess);
                jsonObject.put("msgCode", msgCode);
                jsonObject.put("msgInfo", "该订单不存在,请联系查询传递的订单号");
                resResult.add(jsonObject);
                continue;
            }

            TrainOrderOffline trainOrderOffline = null;
            try {
                trainOrderOffline = trainOrderOfflineDao.findTrainOrderOfflineByOrderNumberOnline(orderNo);
            }
            catch (Exception e) {
                //幂等的设计和实现
                JSONObject jsonTemp = ExceptionCtripUtil.handleCtripException(e);
                resResult.add(jsonTemp);
                continue;
            }

            WriteLog.write(logName,
                    random + ":携程线下票出票请求--orderNo-->" + orderNo + ",trainOrderOffline-->" + trainOrderOffline);

            if (trainOrderOffline != null) {
                jsonObject.put("isSuccess", isSuccess);
                jsonObject.put("msgCode", msgCode);
                jsonObject.put("msgInfo", "该订单已存在,请勿重复提交");
                resResult.add(jsonObject);
                continue;
            }

            // 获取车次信息
            JSONArray ticketItemList = reqData.getJSONArray("TicketItem");
            // 往返车次
            if (ticketItemList.size() != 1) {
                jsonObject.put("isSuccess", isSuccess);
                jsonObject.put("msgCode", msgCode);
                jsonObject.put("msgInfo", "往返订单，暂定不出票");
                // TODO往返订单暂定不出票
                resResult.add(jsonObject);
                continue;
            }

            // 车次乘客信息
            JSONObject ticketItem = ticketItemList.getJSONObject(0);

            String remark = reqData.toJSONString();//订单信息
            // 订单信息获取
            //            String orderTime = reqData.getString("OrderTime");
            //            String orderState = reqData.getString("OrderState");
            //            String agentTime = reqData.getString("AgentTime");
            //            String partnerName = reqData.getString("PartnerName");
            //            String printTime = reqData.getString("PrintTime");
            //            String insuranceCount = reqData.getString("InsuranceCount");
            //            String payTime = reqData.getString("PayTime");
            //            String unReadNoticeCount = reqData.getString("UnReadNoticeCount");
            String contactMobile = reqData.getString("ContactMobile");//联系人姓名
            //            String contactTel = reqData.getString("ContactTel");
            //            String agentUserName = reqData.getString("AgentUserName");
            //            String sendedTime = reqData.getString("SendedTime");
            String contactName = reqData.getString("ContactName");//联系人电话
            //            String lockTime = reqData.getString("LockTime");
            //            String orderFlag = reqData.getString("OrderFlag");
            String deliverAddress = reqData.getString("DeliverAddress");//地址
            //            String weekendDeliver = reqData.getString("WeekendDeliver");
            //            Double orderPrice = reqData.getDouble("OrderPrice");

            // 车次乘客信息
            String passportName = ticketItem.getString("PassportName");//乘客姓名"李俊江,赵兴盛,付东东,马晓君"
            String fromStationName = ticketItem.getString("FromStationName");//出发站
            Double ticketPrice = ticketItem.getDouble("TicketPrice");//票价
            String toStationName = ticketItem.getString("ToStationName");//到达站
            String passportNumber = ticketItem.getString("PassportNumber");//证件号码"120224199210203659,140121198811208054,410311199011201530,120102198108053270"
            String trainNumber = ticketItem.getString("TrainNumber");//车次号
            //            String ticketType = ticketItem.getString("TicketType");//车次种类"1,1,1,1"
            String passportType = ticketItem.getString("PassportType");//身份种类"ED二代,ED二代,ED二代,ED二代"
            String seatName = ticketItem.getString("SeatName");//车次类别
            String acceptSeatName = ticketItem.getString("AcceptSeatName");// 定制服务
            //            String uiDownListAcceptSeat = ticketItem.getString("UIDownListAcceptSeat");//备选车次
            //            String uiPromptSeatName = ticketItem.getString("UIPromptSeatName");// 备选车次定制服务
            String ticketDate = ticketItem.getString("TicketDate");//发车日期
            String ticketTime = ticketItem.getString("TicketTime");//发车时间
            Integer ticketCount = ticketItem.getInteger("TicketCount");// 数量

            //创建火车票线下订单TrainOrderOffline
            //处理相关的数据进行入库操作
            WriteLog.write(logName, random + ":携程线下票出票请求-trainOrderOffline--开始>");
            trainOrderOffline = new TrainOrderOffline();
            trainOrderOffline.setContactUser(contactName);//联系人姓名
            trainOrderOffline.setContactTel(contactMobile);//联系人手机
            trainOrderOffline.setIsDelivery(1);//送票到站标识 ??? 0-不送票到站 1-送票到站 - 默认值是 0 
            trainOrderOffline.setIsGrab(0);//携程抢票订单标识  0: 普通订单,1: 抢票订单 - 默认值是 0 
            String orderNumber = "XC" + TrainOrderOfflineUtil.timeMinID();//平台订单号
            trainOrderOffline.setOrderNumber(orderNumber);
            trainOrderOffline.setOrderNumberOnline(orderNo);//接口订单号
            trainOrderOffline.setOrderStatus(1);//订单状态
            trainOrderOffline.setLockedStatus(1);//锁单状态
            trainOrderOffline.setLockedStatus(1);//锁单状态
            trainOrderOffline.setTicketCount(ticketCount);// 车票数量
            //通过票的信息进行单独的订单的金额的总的计算 - 
            trainOrderOffline.setOrderPrice(ticketPrice * ticketCount);
            trainOrderOffline.setCreateUId(90);
            trainOrderOffline.setCreateUser("携程_破解");
            trainOrderOffline.setExtSeat("无");
            Integer agentid = 467;
            trainOrderOffline.setAgentId(agentid);//TODO 暂定全部分天津64号
            // 定制服务优化
            if (acceptSeatName.contains("###")) {
                acceptSeatName = acceptSeatName.substring(0, acceptSeatName.indexOf("###"));
                if (acceptSeatName.contains("0张")) {
                    acceptSeatName = acceptSeatName.replace("0张", 1 + "张");
                }
            }
            trainOrderOffline.setTradeNo(acceptSeatName);// 定制服务
            //这两个即使没有,也必须传入
            trainOrderOffline.setPaperBackup(1);//当下铺/靠窗/连坐无票时,是否支持非下铺/非靠窗/非连坐(0不接受,1接受)
            trainOrderOffline.setPaperLowSeatCount(0);//至少接受下铺/靠窗/连坐数量
            trainOrderOffline.setPaperType(0);
            trainOrderOffline.setAgentProfit(0.0);//待补充 - 暂时设定为0
            WriteLog.write(logName, random + ":携程线下票出票请求-trainOrderOffline--结束>" + trainOrderOffline);

            // 乘客和车次信息
            List<TrainTicketOffline> trainTicketOfflineList = new ArrayList<TrainTicketOffline>();
            List<TrainPassengerOffline> trainPassengerOfflineList = new ArrayList<TrainPassengerOffline>();
            for (int i = 0; i < ticketCount; i++) {
                //创建火车票线下订单TrainPassengerOffline
                //处理相关的数据进行入库操作
                WriteLog.write(logName, random + ":携程线下票出票请求-trainPassengerOffline--开始>");
                TrainPassengerOffline trainPassengerOffline = new TrainPassengerOffline();
                int id = new Random().nextInt(9000) + 1000;
                trainPassengerOffline.setPassengerId(String.valueOf(id + i));//携程乘客Id
                trainPassengerOffline.setName(passportName.split(",")[i]);//乘客姓名
                trainPassengerOffline.setIdNumber(passportNumber.split(",")[i]);//乘客证件号码
                //证件类型:1: 身份证,2:护照,3:台胞证,4:港澳通行证
                // TODO 暂定二代身份证，后续添加
                if ("ED二代".equals(passportType.split(",")[i])) {
                    trainPassengerOffline.setIdType(1);
                }
                else if ("HZ护照".equals(passportType.split(",")[i])) {
                    trainPassengerOffline.setIdType(3);
                }
                else {
                    trainPassengerOffline.setIdType(1);
                }
                trainPassengerOfflineList.add(trainPassengerOffline);
                WriteLog.write(logName, random + ":携程线下票出票请求-trainPassengerOffline--结束>" + trainPassengerOffline);

                //创建火车票线下订单trainTicketOffline
                //处理相关的数据进行入库操作
                WriteLog.write(logName, random + ":携程线下票出票请求-trainTicketOffline--开始>");
                TrainTicketOffline trainTicketOffline = new TrainTicketOffline();

                trainTicketOffline.setTrainNo(trainNumber);// 车次
                trainTicketOffline.setDeparture(fromStationName);// 出发站
                trainTicketOffline.setArrival(toStationName); // 到达站
                trainTicketOffline.setDepartTime(ticketDate.substring(0, 10) + " " + ticketTime + ".000");// 出发日期- 2016-06-28 18:10:00.000
                trainTicketOffline.setStartTime(ticketTime);// 发车时间
                trainTicketOffline.setArrivalTime(""); // 到达时间
                trainTicketOffline.setCostTime("");// 历时
                trainTicketOffline.setTicketType(1);//票种ID。 - 与票种名称对应关系: - 1:成人票,2:儿童票 - 是  乘客类型:1:成人,2:儿童
                trainTicketOffline.setSeatType(seatName);// 车次类别
                trainTicketOffline.setPrice(ticketPrice);//票价
                trainTicketOfflineList.add(trainTicketOffline);
                WriteLog.write(logName, random + ":携程线下票出票请求-trainTicketOffline--结束>" + trainTicketOffline);
            }

            //创建火车票线下订单TrainOrderOffline
            //处理相关的数据进行入库操作
            WriteLog.write(logName, random + ":携程线下票出票请求-mailAddress--开始>");
            MailAddress mailAddress = new MailAddress();
            mailAddress.setMAILNAME(contactName);
            mailAddress.setPOSTCODE("");//邮编
            mailAddress.setMAILTEL(contactMobile);
            mailAddress.setPROVINCENAME("");//收件省
            mailAddress.setCITYNAME("");//收件市
            mailAddress.setREGIONNAME("");//收件区
            mailAddress.setADDRESS(deliverAddress);//收件地址
            mailAddress.setExpressAgent(99);//配送到站
            WriteLog.write(logName, random + ":携程线下票出票请求-mailAddress--结束>" + mailAddress);

            //健壮性入库判定
            TrainOrderOffline trainOrderOfflineTemp = null;
            try {
                trainOrderOfflineTemp = trainOrderOfflineDao.findTrainOrderOfflineByOrderNumberOnline(orderNo);
            }
            catch (Exception e) {
                JSONObject jsonTemp = ExceptionCtripUtil.handleCtripException(e);
                resResult.add(jsonTemp);
                continue;
            }
            if (trainOrderOfflineTemp != null) {
                //插入数据之前的幂等的二次判断逻辑
                isSuccess = true;
                msgCode = 231000;//isSuccess true用231000,false用231099
                jsonObject.put("isSuccess", isSuccess);
                jsonObject.put("msgCode", msgCode);
                jsonObject.put("msgInfo", "请求已经接收");
                resResult.add(jsonObject);
                continue;
            }

            //插入线下订单表 - 
            Long offlineOrderId = 0L;
            try {
                offlineOrderId = Long.valueOf(trainOrderOfflineDao.addTrainOrderOfflineTC(trainOrderOffline));
                WriteLog.write(logName, random + ":订单信息插入结果" + offlineOrderId);
                String sql = "UPDATE TrainOrderOffline SET ctripOrderInfo = '" + remark + "' WHERE Id = "
                        + offlineOrderId;
                DBHelperOffline.UpdateData(sql);
            }
            catch (Exception e) {
                JSONObject jsonTemp = ExceptionCtripUtil.handleCtripException(e);
                resResult.add(jsonTemp);
                continue;
            }

            //更新快递时效信息到数据库中
            trainOrderOfflineDao.updateTrainOrderOfflineexpressDeliverById(offlineOrderId, "送票到站");

            try {
                mailAddress.setORDERID(offlineOrderId.intValue());
                String reString = mailAddressDao.addMailAddressTC(mailAddress);
                WriteLog.write(logName, random + ":邮寄信息插入结果" + reString);
            }
            catch (Exception e1) {
                JSONObject jsonTemp = ExceptionCtripUtil.handleCtripException(e1);
                resResult.add(jsonTemp);
                continue;
            }

            //插入乘客表 - 插入火车票表
            for (int i = 0; i < trainPassengerOfflineList.size(); i++) {
                //插入乘客表
                TrainPassengerOffline trainPassengerOffline = trainPassengerOfflineList.get(i);
                trainPassengerOffline.setOrderId(offlineOrderId);//关联系统平台的 - Id
                Long offlinePassengerId = 0L;
                try {
                    offlinePassengerId = Long
                            .valueOf(trainPassengerOfflineDao.addTrainPassengerOffline(trainPassengerOffline));
                    WriteLog.write(logName, random + ":乘客信息插入结果" + offlinePassengerId);
                }
                catch (Exception e) {
                    JSONObject jsonTemp = ExceptionCtripUtil.handleCtripException(e);
                    resResult.add(jsonTemp);
                    continue;
                }

                //插入火车票表
                TrainTicketOffline trainTicketOffline = trainTicketOfflineList.get(i);
                trainTicketOffline.setTrainPid(offlinePassengerId);
                trainTicketOffline.setOrderId(offlineOrderId);
                try {
                    String reString = trainTicketOfflineDao.addTrainTicketOffline(trainTicketOffline);
                    WriteLog.write(logName, random + ":车次信息插入结果" + reString);
                }
                catch (Exception e) {
                    JSONObject jsonTemp = ExceptionCtripUtil.handleCtripException(e);
                    resResult.add(jsonTemp);
                    continue;
                }
            }
            //记录日志
            TrainOrderOfflineUtil.CreateBusLogAdminOtherDealResult(offlineOrderId, agentid, "订单接收成功", 0);
            isSuccess = true;
            msgCode = 231000;//isSuccess true用231000,false用231099
            jsonObject.put("isSuccess", isSuccess);
            jsonObject.put("msgCode", msgCode);
            String alternativeTrainNumber = ticketItem.getString("AlternativeTrainNumber");
            // 备选车次不为空
            if (alternativeTrainNumber != null) {
                jsonObject.put("msgInfo", "请求已经接收，备选车次订单，备选车次暂时不考虑");
            }
            else {
                jsonObject.put("msgInfo", "请求已经接收");
            }
            resResult.add(jsonObject);
        }

        return resResult;
    }
}
