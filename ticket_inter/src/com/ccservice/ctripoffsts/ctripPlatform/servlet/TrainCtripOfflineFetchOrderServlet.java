package com.ccservice.ctripoffsts.ctripPlatform.servlet;

import java.io.IOException;
import java.nio.charset.Charset;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.util.ExceptionUtil;
import com.ccservice.crack.ctrippffsts.TrainCtripOfflineUtil;
import com.ccservice.offline.util.RequestStreamUtil;
import com.ccservice.offlineExpress.util.CommonUtil;

public class TrainCtripOfflineFetchOrderServlet extends HttpServlet implements Servlet {

    private static final long serialVersionUID = 7875921500303712967L;

    private static final String logName = "线下火车票_携程点击获取订单_ticket_inter";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //随机
        int random = CommonUtil.randomNum();
        //编码
        request.setCharacterEncoding("utf-8");
        //结果
        JSONObject responseJson = new JSONObject();
        boolean success = false;
        String message = "";
        String number = "";
        try {
            // 请求参数转换
            JSONObject param = RequestStreamUtil.reqToJson(request);
            //日志
            WriteLog.write(logName, random + "---请求信息---" + param);
            if (param != null) {
                // 用户名
                String username = param.getString("username");
                success = true;
                TrainCtripOfflineUtil trainCtripOfflineUtil = new TrainCtripOfflineUtil();
                CloseableHttpClient defaultClient = HttpClients.createDefault();
                number = trainCtripOfflineUtil.fetchOrder(logName, random, defaultClient, username);
                // 如果返回-1，已到了提单限制，请先处理完剩余订单
                if ("-1".equals(number)) {
                    message = "已到了提单限制，请先处理完剩余订单";
                }
                else if ("0".equals(number)) {
                    message = "没有可提取的订单";
                }
                else {
                    message = "提取" + number + "条订单成功";
                }
            }
            else {
                message = "请求参数为空";
            }
        }
        catch (Exception e) {
            message = "请求参数解析失败";
            //记录异常日志
            ExceptionUtil.writelogByException(logName, e, String.valueOf(random));
        }
        // 统一返回
        responseJson.put("success", success);
        responseJson.put("number", number);
        responseJson.put("message", message);
        String result = responseJson.toJSONString();
        WriteLog.write(logName, random + "---回调结果---" + result);
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/json; charset=UTF-8");
        response.getOutputStream().write(result.getBytes(Charset.forName("UTF-8")));
    }
}
