package com.ccservice.ctripoffsts.back;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Random;

import org.apache.http.HttpEntity;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.offline.dao.TrainOfflineConfigDao;
import com.ccservice.offline.dao.TrainPassengerOfflineDao;
import com.ccservice.offline.dao.TrainTicketOfflineDao;
import com.ccservice.offline.domain.TrainPassengerOffline;
import com.ccservice.offline.domain.TrainTicketOffline;

/**
 * BackInput --手工回填
 * BatchDeliverySend -- 批量发件
 * Print -- 设置物流 - 打印
 * Package -- 设置物流 - 已包装
 * @author wjh
 *
 */
public class TrainCtripOfflineBack {
    private static final String LOGNAME = "同程线下票送票到站物流跟踪请求接口-CN发起";

    private int r1 = new Random().nextInt(10000000);

    public String Cookie = null;

    private TrainOfflineConfigDao trainOfflineConfigDao = new TrainOfflineConfigDao();

    private TrainPassengerOfflineDao trainPassengerOfflineDao = new TrainPassengerOfflineDao();

    private TrainTicketOfflineDao trainTicketOfflineDao = new TrainTicketOfflineDao();

    /**
     * 构造函数
     * @param username 用户名
     */
    public TrainCtripOfflineBack(String username) {
        String keyName = "CtripOfflineCookieStr" + username;
        try {
            Cookie = trainOfflineConfigDao.findValueByKeyName(keyName);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据 TrainPid 获取票信息
     * @param tickets
     * @param TrainPid
     * @return
     */
    private TrainTicketOffline FindTrainTicketOfflines(List<TrainTicketOffline> tickets, Long TrainPid) {
        TrainTicketOffline result = null;
        for (TrainTicketOffline p : tickets) {
            if (p.getTrainPid().equals(TrainPid)) {
                result = p;
                break;
            }

        }
        return result;
    }

    /**
     * 根据身份证号取乘客信息
     * @param tickets
     * @param passportNumber 份证号
     * @return
     */
    private TrainPassengerOffline FindPassengers(List<TrainPassengerOffline> tickets, String passportNumber) {
        TrainPassengerOffline result = null;
        for (TrainPassengerOffline p : tickets) {
            if (p.getIdNumber().equals(passportNumber)) {
                result = p;
                break;
            }
        }
        return result;
    }

    /**
     * 手工回填
     * @param OrderId 订单ID
     * @param jonString 拉单时采集JSON
     * {
    	  "OrderNumber": "5050383478",
    	  "PartnerName": "Ctrip.Train",
    	  "PayTime": "11-25 10:45",
    	  "OrderPrice": 0,
    	  "OrderTime": "11-25 10:44",
    	  "LockTime": "11-25 10:45",
    	  "ContactTel": null,
    	  "ContactMobile": "18736063127",
    	  "OrderState": 0,
    	  "AgentTime": "",
    	  "PrintTime": "",
    	  "SendedTime": "",
    	  "SignTime": null,
    	  "ContactName": "叶旺盛",
    	  "DeliverNo": null,
    	  "DeliverCompany": null,
    	  "SupplierName": null,
    	  "AreaName": null,
    	  "DeliverAddress": "天津站柜台 天津站南进站口向北走50米，西侧鑫龙门（烟酒、食品）便利店",
    	  "AgentUserName": "tjz01",
    	  "WeekendDeliver": "0",
    	  "InsuranceCount": 0,
    	  "AreaId": null,
    	  "RuleId": null,
    	  "OrderFlag": 0,
    	  "TicketItem": [
    	    {
    	      "FromStationName": "天津",
    	      "ToStationName": "天津西",
    	      "TicketDate": "2017-12-11 00:00:00",
    	      "TicketTime": "19:06:00",
    	      "TrainNumber": "D6738",
    	      "AlternativeTrainNumber": null,
    	      "SeatName": "二等座",
    	      "AcceptSeatName": "",
    	      "UIPromptSeatName": null,
    	      "UIDownListAcceptSeat": "二等座",
    	      "TicketPrice": 6,
    	      "TicketCount": 1,
    	      "PassportName": "叶旺盛",
    	      "PassportNumber": "411122199109238074",
    	      "PassportType": "ED二代",
    	      "TicketType": "1",
    	      "TicketResult": null,
    	      "TempRealTicketItem": [
    	        {
    	          "TicketType": 1,
    	          "TicketPrice": 6,
    	          "SeatName": "二等座",
    	          "CarriageNo": "04",
    	          "SeatNo": "003"
    	        }
    	      ]
    	    }
    	  ],
    	  "RealTicketItem": null,
    	  "RealTrainNo": null,
    	  "JsonString": null,
    	  "UnReadNoticeCount": 0
    	}
     * @return
     * @throws Exception
     */
    public boolean BackInput(long OrderId, String jonString) throws Exception {
        JSONObject json = JSONObject.parseObject(jonString);
        List<TrainTicketOffline> tickets = trainTicketOfflineDao.findTrainTicketOfflineListByOrderId(OrderId);
        List<TrainPassengerOffline> passengers = trainPassengerOfflineDao
                .findTrainPassengerOfflineListByOrderId(OrderId);

        if (tickets == null || passengers == null)
            return false;

        JSONArray TicketItems = (JSONArray) json.get("TicketItem");
        JSONObject TicketItem = (JSONObject) TicketItems.get(0);

        String PassportNumber = TicketItem.getString("PassportNumber"); //120113196705285619,120113196212255614,120113197506094814
        String[] PassportNumbers = PassportNumber.split(",");

        JSONArray TempRealTicketItem = (JSONArray) TicketItem.get("TempRealTicketItem");

        for (int i = 0; i < TempRealTicketItem.size(); i++) {
            JSONObject ticket = TempRealTicketItem.getJSONObject(i);
            TrainPassengerOffline passenger = FindPassengers(passengers, PassportNumbers[i]);
            TrainTicketOffline realTicket = FindTrainTicketOfflines(tickets, passenger.getId());

            ticket.put("TicketType", realTicket.getTicketType());// 1成人2儿童
            ticket.put("TicketPrice", realTicket.getSealPrice()); // 价格
            ticket.put("SeatName", realTicket.getSeatType()); //二等座

            ticket.put("CarriageNo", realTicket.getCoach());//车厢
            ticket.put("SeatNo", realTicket.getSeatNo());//坐号
        }
        return HttpBackInput(json.toJSONString());
    }

    /**
     * HTTP请求回填
     * @param jonstring
     * @return
     */
    public boolean HttpBackInput(String jonstring) {
        String url = "http://www.tiexiaoer.com/TicketAgent/SetTicketV2/SetHasTicketV2";
        CloseableHttpClient httpClient = HttpClients.createDefault();
        ResponseHandler<String> responseHandler = new BasicResponseHandler();
        HttpPost post = new HttpPost(url);
        post.setHeader("Cookie", this.Cookie);
        String returnValue = null;
        try {
            StringEntity requestEntity = new StringEntity(jonstring, "utf-8");

            WriteLog.write(LOGNAME, r1 + ":携程_破解线下票交互请求-jonstring:" + jonstring);

            requestEntity.setContentEncoding("UTF-8");
            post.setEntity(requestEntity);
            returnValue = httpClient.execute(post, responseHandler);

            WriteLog.write(LOGNAME, r1 + ":携程_破解线下票交互请求-returnValue:" + returnValue);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return "success".equals(returnValue);
    }

    /**
     * 批量发件
     * @return
     */
    private boolean BatchDeliverySend() {
        String url = "http://www.tiexiaoer.com/TicketAgent/SendExpress/DeliverAllOrdersExceptAddressChanged";
        return "success".equals(httpRequest(url));
    }

    /**
     * 设置物流 - 打印
     * @param orderNumber 订单号  5051103640
     * @param fromStationName 出发站 天津
     * @param toStationName 到达站 天津西
     * @return
     */
    @SuppressWarnings("deprecation")
    private boolean Print(String orderNumber, String fromStationName, String toStationName) {
        String url = "";
        try {
            url = "http://www.tiexiaoer.com/TicketAgent/DeliveryPrintV2/HotPrint" + "?orderNumber=" + orderNumber
                    + "&deliverNo=" + "&supplierName=" + URLEncoder.encode("自送", "utf-8") + "&ruleId=&areaId="
                    + "&fromStationName=" + URLEncoder.encode(fromStationName, "utf-8") + "&toStationName="
                    + URLEncoder.encode(toStationName, "utf-8");
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String html = httpRequest(url);

        return html != null && html.contains("自送热敏打印");
    }

    /**
     * 设置物流 - 已包装
     * @param orderNumber 5051103640
     * @return
     */
    private boolean Package(String orderNumber) {
        String url = "http://www.tiexiaoer.com/TicketAgent/SetDeliverV2/Package?orderNumber=" + orderNumber;
        return "success".equals(httpRequest(url));
    }

    public boolean ctripSendToStation(String orderNumber, String fromStationName, String toStationName) {
        boolean result = Print(orderNumber, fromStationName, toStationName);
        if (!result)
            return result;

        result = Package(orderNumber);
        /*if (!result)
            return result;
        
        result = BatchDeliverySend();*/
        return result;
    }

    //*Print--设置物流-打印*Package--设置物流-已包装*BatchDeliverySend--批量发件

    public String httpRequest(String url) {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet get = new HttpGet(url);

        WriteLog.write(LOGNAME, r1 + ":携程_破解线下票交互请求-url:" + url);

        get.setHeader("Cookie", this.Cookie);

        String returnValue = null;
        try {
            CloseableHttpResponse res = httpClient.execute(get);
            HttpEntity entity = res.getEntity();
            returnValue = EntityUtils.toString(entity);

            WriteLog.write(LOGNAME, r1 + ":携程_破解线下票交互请求-returnValue:" + returnValue);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return returnValue;
    }

    public static void main(String[] args) throws Exception {
        TrainCtripOfflineBack back = new TrainCtripOfflineBack("TJZ01");
        //back.BackInput();

        String orderNumber = "5051103640";
        String deliverNo = "5051103640";
        String fromStationName = "天津";
        String toStationName = "天津西";

        boolean printed = back.Print(orderNumber, fromStationName, toStationName);

        System.out.println(printed);
    }

}
