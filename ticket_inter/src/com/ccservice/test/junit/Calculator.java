/**
 * 
 */
package com.ccservice.test.junit;

/**
 * 
 * @time 2016年1月22日 下午4:22:31
 * @author chendong
 */
public class Calculator {
    public int evaluate(String expression) {
        int sum = 0;
        for (String summand : expression.split("\\+"))
            sum += Integer.valueOf(summand);
        return sum;
    }
}
