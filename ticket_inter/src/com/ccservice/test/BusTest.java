/**
 * 
 */
package com.ccservice.test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sourceforge.pinyin4j.PinyinHelper;

import com.ccservice.inter.job.Util.JobTrainUtil;
import com.ccservice.inter.server.Server;

/**
 * 
 * @time 2015年9月15日 下午1:59:27
 * @author chendong
 */
public class BusTest {

    public static void main(String[] args) {
        transferCity();

    }

    /**
     * 转移 e2go 的城市到 总表 city
     * @time 2015年9月15日 下午1:59:54
     * @author chendong
     */
    private static void transferCity() {
        List list = Server.getInstance().getSystemService()
                .findMapResultBySql("SELECT * FROM E2goBusStationName order by id", null);
        for (int i = 0; i < list.size(); i++) {
            Map map = (Map) list.get(i);
            //            System.out.println(map);
            String StopName = map.get("StopName").toString();
            String StopId = map.get("StopId").toString();
            String Address = map.get("Address").toString();
            String ID = map.get("ID").toString();
            String Province = map.get("Province").toString();
            String City = map.get("City").toString();
            updateBusCity(StopName, StopId, Address, ID, Province, City);
        }
    }

    /**
     * 
     * @time 2015年9月15日 下午2:37:44
     * @author chendong
     * @param city 
     * @param province 
     * @param iD 
     * @param address 
     * @param stopId 
     * @param stopName 
     */
    private static void updateBusCity(String stopName, String stopId, String address, String iD, String province,
            String city) {
        String stopName1 = stopName;
        if ("市".equals(stopName1.substring(stopName1.length() - 1))) {
            stopName1 = stopName.substring(0, stopName.length() - 1);
        }
        String sql1 = "SELECT * FROM BusCity where Name='" + stopName1 + "' order by id ";
        List list1 = Server.getInstance().getSystemService().findMapResultBySql(sql1, null);
        Map map1 = new HashMap();
        if (list1.size() > 0) {//BusCity 表里有数据了
            map1 = (Map) list1.get(0);

            String Id = map1.get("Id").toString();
            String ProvinceName = map1.get("ProvinceName").toString();
            String E2goStopId = map1.get("E2goStopId").toString();
            if ("0".equals(ProvinceName)) {
                //                String sql2 = "update BusCity set ProvinceName = '" + province + "' where id=" + Id;
                //                int count2 = Server.getInstance().getSystemService().excuteAdvertisementBySql(sql2);
                //                System.out.println(count2 + "-->" + sql2);
            }
            if ("0".equals(E2goStopId)) {
                System.out.println(iD + ":" + stopId + ":" + stopName + ":" + city + ":" + province + ":" + "--->"
                        + map1);
                String sql2 = "update BusCity set E2goStopId = '" + stopId + "' where id=" + Id;
                int count2 = Server.getInstance().getSystemService().excuteAdvertisementBySql(sql2);
                //                System.out.println(count2 + "-->" + sql2);
            }
        }
        else {//BusCity 表里没有数据
            Server.dangqianheyancount++;
            System.out.println(iD + ":" + stopId + ":" + stopName + ":" + city + ":" + province + ":"
                    + "------------没有-->" + Server.dangqianheyancount);
            String tocitypinyin = JobTrainUtil.parsePinYinName(stopName);
            String tocityshortpinyin = JobTrainUtil.parseJianPinYinName(stopName);

            Server.getInstance()
                    .getSystemService()
                    .findMapResultByProcedure(
                            "sp_Insert_Bus_City '" + stopName + "','" + tocitypinyin + "','" + tocityshortpinyin
                                    + "','" + province + "','0','0','0','" + stopId + "'");
        }
    }
}
