package com.ccservice.test.rabbitmq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class MyThreadTestSend extends Thread {

    private final String QUEUE_NAME = "hello";

    Channel channel;

    public MyThreadTestSend(Channel channel) {
        this.channel = channel;
    }

    public MyThreadTestSend() {
    }

    @Override
    public void run() {
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("43.241.229.127");
            factory.setUsername("rabbitmq");
            factory.setPassword("aeka3OhV");
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();

            channel.queueDeclare(QUEUE_NAME, false, false, false, null);
            String message = "Hello World!";
            channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
            System.out.println(" [x] Sent '" + message + "'");

            channel.close();
            connection.close();
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
