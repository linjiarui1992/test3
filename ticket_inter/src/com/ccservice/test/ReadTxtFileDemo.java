package com.ccservice.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.net.URLEncoder;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class ReadTxtFileDemo {

    public static void main(String[] args) {
        for (int i = 50; i < 52; i++) {
            readFile(i);
        }
    }

    public static void readFile(int num) {

        File file = new File("D:/sendtomeituanpersondata/" + num + "/TongchengCallBackPassengerServlet.log");
        BufferedReader reader = null;
        InputStream in = null;
        InputStreamReader readers = null;
        try {
            in = new FileInputStream(file);
            readers = new InputStreamReader(in, "GBK");
            BufferedReader bf = new BufferedReader(readers);
            String read = "";
            while ((read = bf.readLine()) != null) {
                parsingDataMethod(read);
                Thread.sleep(100L);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void parsingDataMethod(String read) {

        if (read.indexOf("backjson-->") > -1) {
            int start = read.indexOf("{");
            int end = read.lastIndexOf("}");
            read = read.substring(start, end + 1);
            JSONObject json = JSONObject.parseObject(read);
            JSONArray array = json.getJSONArray("accounts");
            JSONObject PassengerMappings_encode = new JSONObject();
            for (int i = 0; i < array.size(); i++) {
                JSONObject jsonaccount = array.getJSONObject(i);
                JSONArray jsonpassengers = jsonaccount.getJSONArray("passengers");
                for (int j = 0; j < jsonpassengers.size(); j++) {
                    JSONObject jsonpassenger = jsonpassengers.getJSONObject(j);
                    String passengersename = getURLEncoder(jsonpassenger.getString("passengersename"));
                    String passportseno = jsonpassenger.getString("passportseno");
                    String passporttypeseid = jsonpassenger.getString("passporttypeseid");
                    String passporttypeseidname = getURLEncoder(jsonpassenger.getString("passporttypeseidname"));
                    String passengertypeid = jsonpassenger.getString("passengertypeid");
                    String passengertypename = getURLEncoder(jsonpassenger.getString("passengertypename"));
                    String operationtypename = getURLEncoder(jsonpassenger.getString("operationtypename"));
                    String operationtime = jsonpassenger.getString("operationtime");
                    String operationtypeid = jsonpassenger.getString("operationtypeid");
                    if ("1".equals(operationtypeid)) {
                        operationtypeid = "0";
                    }
                    else if ("2".equals(operationtypeid)) {
                        operationtypeid = "1";
                    }
                    PassengerMappings_encode.put("passengersename", getURLDncode(passengersename)); //乘客姓名
                    PassengerMappings_encode.put("passportseno", passportseno); //乘客证件号码
                    PassengerMappings_encode.put("passporttypeseid", passporttypeseid); //证件类型 ID
                    PassengerMappings_encode.put("passporttypeseidname", getURLDncode(passporttypeseidname)); //证件类型名称
                    PassengerMappings_encode.put("passengertypeid", passengertypeid); //旅客类型 ID
                    PassengerMappings_encode.put("passengertypename", getURLDncode(passengertypename)); //旅客类型名称
                    PassengerMappings_encode.put("operationtypeid", operationtypeid); //操作类型 ID
                    PassengerMappings_encode.put("operationtypename", getURLDncode(operationtypename)); //操作类型名称
                    PassengerMappings_encode.put("operationtime", operationtime); //操作时间

                    System.out.println(PassengerMappings_encode.toString());
                    String callBackUrl = "http://i.meituan.com/uts/train/agentpassenger/updaterealtime/104/HANGTIANHUAYOU";
                    String result = submitPost(callBackUrl, PassengerMappings_encode.toString(), "UTF-8").toString();
                    System.out.println(result);
                }
            }
        }

    }

    public void writeToExcel() {

        //创建一个Excel文件薄
        HSSFWorkbook workbook = new HSSFWorkbook();
        //创建一个工作sheet
        HSSFSheet sheet = workbook.createSheet();
        //创建第一行
        HSSFRow row = sheet.createRow(0);
        HSSFCell cell = null;
        //插入第1行数据
        for (int i = 0; i < 5; i++) {
            cell = row.createCell(i);
            cell.setCellValue("");

        }

    }

    /**
     * java.net实现 HTTP POST方法提交
     * 
     * @param url
     * @param paramContent
     * @return
     */
    public static StringBuffer submitPost(String url, String paramContent, String codetype) {
        StringBuffer responseMessage = null;
        java.net.URLConnection connection = null;
        java.net.URL reqUrl = null;
        OutputStream reqOut = null;
        InputStream in = null;
        BufferedReader br = null;
        try {
            String param = paramContent;
            // System.out.println("url=" + url + "?" + paramContent + "\n");
            // System.out.println("===========post method start=========");
            responseMessage = new StringBuffer();
            reqUrl = new java.net.URL(url);
            connection = reqUrl.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            reqOut = connection.getOutputStream();
            reqOut.write(param.getBytes("UTF-8"));
            reqOut.flush();
            int charCount = -1;
            in = connection.getInputStream();

            br = new BufferedReader(new InputStreamReader(in, codetype));
            while ((charCount = br.read()) != -1) {
                responseMessage.append((char) charCount);
            }
            // System.out.println(responseMessage);
            // System.out.println("===========post method end=========");
        }
        catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("url=" + url + "?" + paramContent + "\n e=" + ex);
        }
        finally {
            try {
                in.close();
                br.close();
                reqOut.close();
            }
            catch (Exception e) {
                System.out.println("paramContent=" + paramContent + "|err=" + e);
            }
        }
        return responseMessage;
    }

    private static String getURLEncoder(String string) {

        if (string == null && "".equals(string)) {
            return "";
        }
        try {
            string = URLEncoder.encode(string, "UTF-8");
        }
        catch (Exception e) {
        }
        return string;
    }

    private static String getURLDncode(String passengersename) {
        try {
            passengersename = URLDecoder.decode(passengersename, "UTF-8");
        }
        catch (Exception e) {
        }
        return passengersename;
    }

}
