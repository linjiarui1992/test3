/**
 * 
 */
package com.ccservice.test.cd;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.service12306.TimeUtil;
import com.ccservice.inter.server.Server;
import com.ccservice.test.thread.Rehuidiaozhanzuoshibai_thread;

/**
 * 
 * @time 2015年10月23日 上午9:42:53
 * @author chendong
 */
public class CdTrainTest {
    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            chongxinhuidiao();
            try {
                Thread.sleep(6000L);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    static long lastTrainOrderid = 0;

    /**
     * 
     * @time 2015年10月23日 上午9:42:17
     * @author chendong
     */
    private static void chongxinhuidiao() {
        int dataType = 1;//1同程,2空铁
        int type = 2; //回调1回调占座失败,2扔队列里重新下单,3回调占座成功,4回调出票失败,5重新审核
        int state12306 = 1;//1">等待下单 2">正在下单 3">下单失败 4">下单成功等待支付 5">下单成功支付中 6">支付成功 7">支付失败 8">支付成功审核中 
        int orderstatus = 1;//订单状态 1等待支付2等待出票3出票完成
        String C_ISQUESTIONORDER = "0";//0为非问题订单；1为下单问题订单；2支付问题订单；3采购问题订单；4改签问题订单；5退票问题订单；6退款失败问题订单
        int topcount = 1;//查多少个订单
        //==============回调1回调出票失败2,扔队列里重新下单
        String startTime = " 00:53:00";
        String endTime = " 07:10:00";
        String mqUrl = "tcp://121.199.25.199:61616";//同程 
        String cn_servierUrl = "http://121.40.62.200:40000/cn_service/service/";//同程
        //        String tcTrainCallBack = "http://121.199.25.199:29716/tcTrainCallBack";
        String tcTrainCallBack = "http://tctraincallback.tc.hangtian123.net/cn_interface/tcTrainCallBack";
        if (dataType == 2) {
            cn_servierUrl = "http://121.40.174.4:39001/cn_service/service/";//空铁
            mqUrl = "tcp://120.26.100.206:61616";
            tcTrainCallBack = "http://120.26.100.206:19222/cn_interface/tcTrainCallBack";
        }
        List list = getTrainorderinfo(orderstatus, C_ISQUESTIONORDER, topcount, state12306, cn_servierUrl, type,
                endTime, startTime);
        System.out.println("订单量:" + list.size() + "-------lastTrainOrderid-" + lastTrainOrderid);
        //=====回调1回调占座失败,2扔队列里重新下单,3回调占座成功,4回调出票失败,5重新审核
        rehuidiaozhanzuoshibai(type, list, mqUrl, cn_servierUrl, tcTrainCallBack);
    }

    /**
     * 
     * 回调1回调占座失败,2扔队列里重新下单,3回调占座成功,4回调出票失败,5重新审核
     * @param type 回调1回调占座失败,2扔队列里重新下单,3回调占座成功,4回调出票失败,5重新审核
     * @time 2015年1月30日 上午11:20:03
     * @author chendong
     * @param topcount 
     * @param orderstatus 
     * @param state12306 
     */
    private static void rehuidiaozhanzuoshibai(int type, List list, String mqUrl, String cn_servierUrl,
            String tcTrainCallBack) {
        //        System.out.println(list.size());
        // 创建一个可重用固定线程数的线程池
        ExecutorService pool = Executors.newFixedThreadPool(100);
        // 创建实现了Runnable接口对象，Thread对象当然也实现了Runnable接口
        for (int i = 0; i < list.size(); i++) {
            Thread t1 = null;
            Map map = (Map) list.get(i);
            String id = map.get("ID").toString();
            lastTrainOrderid = Long.parseLong(id);
            if (type == 1) {//回调占座失败 
                //将线程放入池中进行执行
                t1 = new Rehuidiaozhanzuoshibai_thread(map, tcTrainCallBack, type, cn_servierUrl);
            }
            else if (type == 2) {//扔队列里重新下单

                t1 = new Rehuidiaozhanzuoshibai_thread(id, mqUrl, type);
            }
            else if (type == 3) {//回调占座成功

                t1 = new Rehuidiaozhanzuoshibai_thread(map, tcTrainCallBack, type, cn_servierUrl);
            }
            else if (type == 4) {//支付失败回调出票失败
                //                String id = map.get("ID").toString();
                t1 = new Rehuidiaozhanzuoshibai_thread(map, tcTrainCallBack, type, cn_servierUrl);
            }
            else if (type == 5) {//重新审核
                String orderids = map.get("ID").toString();
                goToShenhe(mqUrl, orderids);
            }
            pool.execute(t1);
        }
        // 关闭线程池
        pool.shutdown();
    }

    /**
     * 重新发送审核订单
     * 
     * @param orderid
     * @param mqURl "tcp://120.26.100.206:61616"
     * @time 2015年2月11日 下午10:04:11
     * @author chendong
     */
    private static void goToShenhe(String mqURl, String orderids) {
        String[] ss = orderids.split(",");
        for (int i = 0; i < ss.length; i++) {
            Long orderid = Long.parseLong(ss[i]);
            JSONObject jsoseng = new JSONObject();
            jsoseng.put("orderid", orderid);
            System.out.println(jsoseng.toJSONString());
            try {
                //                ActiveMQUtil.sendMessage(mqURl, "query_trainorder", ss[i]);
                String url = "http://121.199.25.199:45010/ticket_inter/QueryTrainorder.jsp?id=" + ss[i];
                //                String url = "http://120.26.100.206:49410/ticket_inter/QueryTrainorder.jsp?id=" + ss[i];
                System.out.println(orderid);
                SendPostandGet.submitGet(url, "UTF-8");
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            //            "tcp://120.26.100.206:61616"
            //            ActiveMQUtil.sendMessage("tcp://121.199.25.199:61616", "query_trainorder", ss[i]);
        }
    }

    /**
     * 获取某些状态的订单的id
     * @param orderstatus 订单状态
     * @param C_ISQUESTIONORDER 问题订单类型
     * @param topcount 获取多少个
     * @return
     * @time 2015年1月22日 下午1:05:36
     * @author chendong
     * @param type 
     * @param startTime 
     */
    private static List getTrainorderinfo(int orderstatus, String C_ISQUESTIONORDER, int topcount, int state12306,
            String cn_serverUrl, int type, String endTime, String startTime) {
        Map map = new HashMap();
        String sql = "SELECT TOP " + topcount
                + " ID,C_AGENTID,C_QUNARORDERNUMBER,C_ORDERNUMBER FROM T_TRAINORDER WHERE (C_ORDERSTATUS = "
                + orderstatus + ") " + " and c_state12306=" + state12306 + " AND (C_ISQUESTIONORDER = "
                + C_ISQUESTIONORDER + ") " + " AND (C_CREATETIME > '" + TimeUtil.gettodaydate(1) + " " + startTime
                + "') " + " AND (C_CREATETIME < '" + TimeUtil.gettodaydate(1) + " " + endTime + "') ";
        if (lastTrainOrderid > 0) {
            sql += " and id>" + lastTrainOrderid + " ";
        }
        if (type == 3) {
            sql += " ORDER BY ID desc";
        }
        else {
            sql += " ORDER BY ID asc";
        }
        //        sql = "select * from T_trainorder with(nolock) where c_qunarordernumber in('TGT_S55D3F58A2100831116','TGT_S55D3F5U92100302737','TGT_S55D3F77921003D1524','TGT_S55D3F78021003D1738','TGT_S55D3F7FB2100411990','TGT_S55D3F81C2100411632','TGT_S55D3F8292100315745','TGT_S55D3F830210040C951','TGT_S55D3F83A2100410578','TGT_S55D3F83C210026C316','TGT_S55D3F847210026C181','TGT_S55D3F854210026C419','TGT_S55D3F858210026C710','TGT_S55D3F8692100302616','TGT_S55D3F8792100831677','TGT_S55D3F87D2100314153','TGT_S55D3F88C2100411621','TGT_S55D3F89A2100302231','TGT_S55D3F8AC2100837797','TGT_S55D3F8B42100410073','TGT_S55D3FBC4210040C100','TGT_S55D3FBD021003U4678','TGT_S55D3FC132100408349','TGT_S55D3FC202100410279','TGT_S55D3FC4F21003U4404','TGT_S55D3FC52210040C790','TGT_S55D3FC6U21003U4330','TGT_S55D3FCD1210033C661','TGT_S55D3FCDA210026C392','TGT_S55D4045421003BA498','TGT_S55D4049021003D1935','TGT_S55D404CD21003BA338','TGT_S55D40514210033C907','TGT_S55D4092721003D0899','TGT_S55D409442100412405','TGT_S55D409602100837727','TGT_S55D409D52100410550','TC_55D40AF8210031U046','TGT_S55D3F84D21002D9509','TGT_S55D4041F21002D9707','TGT_S55D4042221002D9947','TGT_S55D4041B210034F735','TGT_S55D40426210034F375','TGT_S55D4049D21002D8872','TGT_S55D4041921002D8701','TGT_S55D404A521002D8970','TGT_S55D404902100350521','TGT_S55D404A621002D9352','TGT_S55D404242100350755','TGT_S55D4042021002D8975','TGT_S55D4053D21002D8544','TGT_S55D4047A210034F323','TGT_S55D404382100350955','TGT_S55D404A821002D9433','TGT_S55D4046221002C1514','TGT_S55D4042D210038U857','TGT_S55D4049D21002D8261','TGT_S55D40428210038U375','TGT_S55D404AC21002D9358','TGT_S55D404102100350006','TGT_S55D4046421002C1526','TGT_S55D404A721002D8407')";
        System.out.println(sql);
        List list = Server.getInstance().getSystemService(cn_serverUrl).findMapResultBySql(sql, null);
        return list;
    }
}
