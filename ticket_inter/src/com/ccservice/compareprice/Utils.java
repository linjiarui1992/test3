package com.ccservice.compareprice;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

import com.ccservice.b2b2c.atom.server.Server;
import com.ccservice.b2b2c.base.city.City;
import com.ccservice.huamin.WriteLog;

public class Utils {
	/**
	 * 判断当前时间 23:50--1:10更新没数据
	 * @return
	 */
	public static boolean getFlag() {
		boolean flag = true;
		long time = System.currentTimeMillis();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		String str1 = "23:51";
		String str2 = "23:59";
		String str3 = "00:00";
		String str4 = "00:08";
		Date date = null;
		try {
			date = sdf.parse(sdf.format(time));
			Date date1 = sdf.parse(str1);
			Date date2 = sdf.parse(str2);
			if (date.after(date1) && date.before(date2)) {
				return false;
			}
			Date date3 = sdf.parse(str3);
			Date date4 = sdf.parse(str4);
			if (date.after(date3) && date.before(date4)) {
				return false;
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return flag;
	}

	public static String getcityNamebyId(long cityid) {
		// System.out.println(cityid);
		City city = Server.getInstance().getHotelService().findCity(cityid);
		if (city != null) {
			return city.getName();
		} else {

			return "";
		}
	}

	public static void write(File fileName, String logString) {

		try {
			PrintWriter printWriter = new PrintWriter(new FileOutputStream(
					fileName, true));
			printWriter.println(logString);
			printWriter.flush();
		} catch (FileNotFoundException e) {
			e.getMessage();

		}

	}

	/**
	 * 将汉字转换为全拼
	 * 
	 * @param src
	 * @return String
	 */
	public static String getPinYin(String src) {
		char[] t1 = null;
		t1 = src.toCharArray();
		// System.out.println(t1.length);
		String[] t2 = new String[t1.length];
		// System.out.println(t2.length);
		// 设置汉字拼音输出的格式
		HanyuPinyinOutputFormat t3 = new HanyuPinyinOutputFormat();
		t3.setCaseType(HanyuPinyinCaseType.LOWERCASE);
		t3.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
		t3.setVCharType(HanyuPinyinVCharType.WITH_V);
		String t4 = "";
		int t0 = t1.length;
		try {
			for (int i = 0; i < t0; i++) {
				// 判断是否为汉字字符
				// System.out.println(t1[i]);
				if (Character.toString(t1[i]).matches("[\\u4E00-\\u9FA5]+")) {
					t2 = PinyinHelper.toHanyuPinyinStringArray(t1[i], t3);// 将汉字的几种全拼都存到t2数组中
					t4 += t2[0];// 取出该汉字全拼的第一种读音并连接到字符串t4后
				} else {
					// 如果不是汉字字符，直接取出字符并连接到字符串t4后
					t4 += Character.toString(t1[i]);
				}
			}
		} catch (BadHanyuPinyinOutputFormatCombination e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return t4.toUpperCase();
	}

	/**
	 * 提取每个汉字的首字母
	 * 
	 * @param str
	 * @return String
	 */
	public static String getPinYinHeadChar(String str) {
		String convert = "";
		for (int j = 0; j < str.length(); j++) {
			char word = str.charAt(j);
			// 提取汉字的首字母
			String[] pinyinArray = PinyinHelper.toHanyuPinyinStringArray(word);
			if (pinyinArray != null) {
				convert += pinyinArray[0].charAt(0);
			} else {
				convert += word;
			}
		}
		return convert.toUpperCase();
	}

	/**
	 * 将字符串转换成ASCII码
	 * 
	 * @param cnStr
	 * @return String
	 */
	public static String getCnASCII(String cnStr) {
		StringBuffer strBuf = new StringBuffer();
		// 将字符串转换成字节序列
		byte[] bGBK = cnStr.getBytes();
		for (int i = 0; i < bGBK.length; i++) {
			// System.out.println(Integer.toHexString(bGBK[i] & 0xff));
			// 将每个字符转换成ASCII码
			strBuf.append(Integer.toHexString(bGBK[i] & 0xff));
		}
		return strBuf.toString();
	}

	public int difcount(String startDate, String endDate) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date time = df.parse(startDate);
			Date time2 = df.parse(endDate);
			long manyday = (time2.getTime() - time.getTime())
					/ (24 * 3600 * 1000);
			int many = (int) manyday;
			return many;
		} catch (Exception e) {
			WriteLog.write("酷讯日期转换异常", e.getMessage());
		}
		return 0;
	}

	// 根据床型id查找房名
	public String findbedtypename(long bedtypeid) {
		return Server.getInstance().getHotelService().findBedtype(bedtypeid)
				.getTypename();
	}

	// <em class='sort x' style='display:inline-block;width:39px'><span
	// class='x0'>华</span><span class='x28'>房</span><span
	// class='x24'>豪主</span></em><span class='enc1' style='display: block;
	// text-indent: 39px;'>(双早)</span>
	// 处理以上字符串，获取汉字内容
	public String getString(String str) {
		String[] st = str.split("<(\\S*?)[^>]*>.*?");
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < st.length; i++) {
			String temp = st[i].trim();
			if (!temp.equals("")) {
				sb.append(temp);
			}
		}
		return sb.toString();
	}
}
