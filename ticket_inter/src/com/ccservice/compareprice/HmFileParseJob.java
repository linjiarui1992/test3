package com.ccservice.compareprice;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class HmFileParseJob implements Job{

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		long time1=System.currentTimeMillis();
		new HmFileParse().test();
		long time2=System.currentTimeMillis();
		System.out.println("根据文件更新所用时间："+(time2-time1)/1000/60+"分钟");
	}

}
