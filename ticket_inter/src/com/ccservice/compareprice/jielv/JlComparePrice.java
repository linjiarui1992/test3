package com.ccservice.compareprice.jielv;

import java.util.*;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.compareprice.HotelData;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.base.hotelgooddata.HotelGoodData;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;

public class JlComparePrice {

    private int catchdays = Integer.parseInt(PropertyUtil.getValue("catchdays").trim());

    private int addPrice = Integer.parseInt(PropertyUtil.getValue("addPrice").trim());//加价

    private int pricescale = Integer.parseInt(PropertyUtil.getValue("pricescale").trim());

    public List<HotelGoodData> loadQunarData(Map<String, List<HotelGoodData>> jlMap, Hotel hotel, String startDate,
            String endDate) throws Exception {
        if (jlMap == null || jlMap.size() == 0 || hotel == null
                || ElongHotelInterfaceUtil.StringIsNull(hotel.getQunarId())) {
            return new ArrayList<HotelGoodData>();
        }
        System.out.println("Load Qunar Data：" + hotel.getName());
        //循环请求去哪儿
        String qunarid = hotel.getQunarId().trim();
        String url = new String(PropertyUtil.getValue("qunarPriceUrl").getBytes("iso8859-1"), "utf-8");
        //预付
        url += "?payType=2&reqType=ComparePirce&QunarHotelId=" + qunarid + "&startDate=" + startDate + "&endDate="
                + endDate;
        return reqQunar(jlMap, hotel, url);
    }

    private List<HotelGoodData> reqQunar(Map<String, List<HotelGoodData>> jlMap, Hotel hotel, String url)
            throws Exception {
        List<HotelGoodData> datas = new ArrayList<HotelGoodData>();
        String str = SendPostandGet.submitPost(url, "", "utf-8").toString();
        if (!ElongHotelInterfaceUtil.StringIsNull(str)) {
            Map<String, List<HotelData>> roomDatas = new HashMap<String, List<HotelData>>();
            JSONArray json = JSONArray.fromObject(str);
            for (int i = 0; i < json.size(); i++) {
                JSONObject obj = json.getJSONObject(i);
                //0:现付 1:预付、易订行
                if (obj.getInt("paytype") != 1 || "wiotappb065".equals(obj.getString("agentid"))
                        || "易订行".equals(obj.getString("agentname"))) {
                    continue;
                }
                //new
                HotelData hoteldata = new HotelData();
                hoteldata.setSealprice((int) obj.getDouble("rPrice"));
                hoteldata.setAgentNo(obj.getString("agentid"));
                hoteldata.setAgentNmae(obj.getString("agentname"));
                hoteldata.setHotelid(hotel.getId() + "");
                hoteldata.setHotelname(hotel.getName());
                hoteldata.setRoomnameBF(obj.getString("bfstr"));
                hoteldata.setRoomstatus(obj.getInt("roomstatus"));
                hoteldata.setType(obj.getInt("paytype"));
                String roomname = obj.getString("roomname").trim();
                hoteldata.setRoomname(roomname);
                hoteldata.setAgentRoom(obj.getString("agentRoom"));
                List<HotelData> hoteldatas = new ArrayList<HotelData>();
                if (roomDatas.containsKey(roomname)) {
                    hoteldatas = roomDatas.get(roomname);
                }
                hoteldatas.add(hoteldata);
                roomDatas.put(roomname, hoteldatas);
            }
            System.out.println("去哪房型有价格数量：" + roomDatas.size());
            if (roomDatas.size() > 0) {
                datas = compare(jlMap, roomDatas);
                if (datas.size() > 0) {
                    System.out.println("===存在优势价格数量===" + datas.size());
                }
            }
        }
        return datas;
    }

    private List<HotelGoodData> compare(Map<String, List<HotelGoodData>> jlMap, Map<String, List<HotelData>> quMap)
            throws Exception {
        //有优势
        List<HotelGoodData> datas = new ArrayList<HotelGoodData>();
        //dayJLDate：捷旅数据、qunarDatas：去哪儿数据
        for (String key : jlMap.keySet()) {
            List<HotelGoodData> jlPriceList = jlMap.get(key);
            if (jlPriceList == null || jlPriceList.size() != catchdays) {
                continue;
            }
            HotelGoodData first = jlPriceList.get(0);
            String jlRoomName = first.getRoomtypename().trim();
            int localBed = first.getBedtypeid().intValue();
            long jlBf = first.getBfcount().longValue();
            //深捷旅平均价格
            Double jlTotalPrice = 0d;
            for (HotelGoodData jl : jlPriceList) {
                jlTotalPrice = ElongHotelInterfaceUtil.add(jlTotalPrice, jl.getBaseprice().longValue());
            }
            Double jlAvgBase = jlTotalPrice / jlPriceList.size();
            //平均加价
            Double jlAvgPrice = ElongHotelInterfaceUtil.add(jlAvgBase, addPrice);
            //去哪儿价格
            out: for (String qunarRoomName : quMap.keySet()) {
                //去哪儿正式房型
                if (CompareRoomNameUtil.IsSame(jlRoomName, localBed, qunarRoomName)) {
                    List<HotelData> quList = quMap.get(qunarRoomName);
                    for (HotelData qu : quList) {
                        //去哪儿平均价格
                        int qunarPrice = qu.getSealprice().intValue();
                        //比较价格
                        if (jlAvgPrice.intValue() <= qunarPrice) {
                            //比较早餐
                            boolean bfFlag = false;
                            //早餐比较
                            long bf = getQunarBf(qu.getRoomnameBF());
                            if (jlBf == bf) {
                                bfFlag = true;
                            }
                            //深捷旅含早、双早、三早，去哪儿单早
                            else if ((jlBf == -1 || jlBf == 2 || jlBf == 3) && bf == 1) {
                                bfFlag = true;
                            }
                            //深捷旅三早，去哪儿双早
                            else if (jlBf == 3 && bf == 2) {
                                bfFlag = true;
                            }
                            if (bfFlag) {
                                long addprice = getAddPirce(jlAvgBase, qunarPrice);
                                for (HotelGoodData currentJl : jlPriceList) {
                                    currentJl.setSealprice(qu.getSealprice().longValue());
                                    currentJl.setAgentname(qu.getAgentNmae());
                                    currentJl.setQunarName(qunarRoomName);
                                    if (addprice > 0) {
                                        currentJl.setProfit(addprice);
                                        currentJl.setShijiprice((long) ElongHotelInterfaceUtil.add(currentJl
                                                .getBaseprice().longValue(), addprice));
                                    }
                                    datas.add(currentJl);
                                }
                                break out;
                            }
                        }
                    }
                }
            }
        }
        return datas;
    }

    /**
     * 比例取价格，利润最大化
     */
    private long getAddPirce(double jlBasePrice, double qunarPrice) {
        long addprice = 0;
        try {
            //差价
            Double price = ElongHotelInterfaceUtil.subtract(qunarPrice, jlBasePrice);
            if (price > addPrice) {
                //100*30*0.01 = 30
                double temp = ElongHotelInterfaceUtil.multiply(price, pricescale * 0.01);
                //100-30=70
                price = ElongHotelInterfaceUtil.subtract(price, temp);
                if (price > addPrice) {
                    addprice = price.longValue();
                }
            }
        }
        catch (Exception e) {
        }
        return addprice;
    }

    private long getQunarBf(String bfstr) {
        long bf = 0;
        if ("单早".equals(bfstr)) {
            bf = 1;
        }
        else if ("双早".equals(bfstr)) {
            bf = 2;
        }
        else if ("三早".equals(bfstr)) {
            bf = 3;
        }
        else if ("四早".equals(bfstr)) {
            bf = 4;
        }
        else if ("含早".equals(bfstr)) {
            bf = -1;
        }
        return bf;
    }
}
