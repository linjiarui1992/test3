package com.ccservice.compareprice;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.b2b2c.base.hotelgooddata.HotelGoodData;
import com.ccservice.b2b2c.base.sysconfig.Sysconfig;
import com.ccservice.huamin.WriteLog;
import com.ccservice.inter.server.Server;

/**
 * @author wzc 根据qunar网页数据分析易订行开关房状态推荐
 */
public class FindRoomStaus extends Utils {

	public final static String YDX_ID = "wiotatts037";// 易定行id

	private List<String> roomnames;

	// 返回去哪对应的数据集合
	private Map<String, List<HotelData>> roomDatas;

	// 返回酒店列表qunar数据集合
	private Map<Long, Map<String, List<HotelData>>> qunarDatas;

	// qunar供应商信息
	private Map<Long, List<AgentInfo>> agentinfos;

	public String format1(String pattern, double value) { // 此方法专门用于完成数字的格式化显示
		DecimalFormat df = null; // 声明一个DecimalFormat类的对象
		df = new DecimalFormat(pattern); // 实例化对象，传入模板
		String str = df.format(value); // 格式化数字
		return str;
	}

	// wiotappb005 - 美景假期
	// wiotatts048 - 嗨！假期旅行网
	// wiotatts004 - 旅行佳商旅网
	// wiotappb001 - 百川通旅行网
	// 关房
	public boolean getRoomStatus(List<HotelData> otadatas,
			List<AgentInfo> agentinfos) {
		boolean flag = false;// 是否关房
		float gf = 0.0f;
		int m = 0;// 总数
		int closenum = 0;// 关房数
		if (agentinfos.size() > 0) {
			label: for (HotelData hoteldata : otadatas) {
				if (hoteldata.getAgentNo().equals("wiotatts037")) {
					continue;
				}
				for (AgentInfo agentInfo : agentinfos) {
					if (agentInfo.isOnSale()) {
						if (agentInfo.getAgentno().equals(
								hoteldata.getAgentNo())) {
							if (hoteldata.getType() == 1) {
								m++;
								if (hoteldata.getRoomstatus() == -1) {
									closenum++;
									if (hoteldata.getAgentNo().equals(
											"wiotappb001")) {// 百川通旅行网 房态控制不好
										System.out.println("百川通旅行网过滤");
										continue label;
									}
									if (hoteldata.getAgentNo().equals(
											"wiotappb005")) {// 美景假期
										System.out.println("美景假期关房");
										gf += 0.45;
										continue label;
									} else if (hoteldata.getAgentNo().equals(
											"wiotatts048")) {// 嗨！假期旅行网
										System.out.println("嗨！假期旅行网关房");
										gf += 0.45;
										continue label;
									} else if (hoteldata.getAgentNo().equals(
											"wiotatts004")) {// 旅行佳商旅网
										System.out.println("旅行佳商旅网关房");
										gf += 0.45;
										continue label;
									} else if (hoteldata.getAgentNo().equals(
											"wiexpotia00")) {// 汇通客(原世博会官方订房网)
										System.out.println("汇通客关房");
										gf += 0.45;
										continue label;
									} else {
										System.out.println(hoteldata
												.getAgentName(hoteldata
														.getAgentNo())
												+ "关房");
										gf += 0.24;
										continue label;
									}
								}
							}
						}
					}
				}
			}
		}
		if (m > 0) {
			float result = closenum / m;
			if (result > 0.5) {
				flag = true;
				System.out.println("---------------比例关房-------------------");
			}
		}
		if (gf >= 0.5 && !flag) {
			flag = true;
			System.out.println("---------------最终关房-------------------");
		}
		return flag;
	}

	/**
	 * 
	 * @param otadatas
	 * @return 如果所有代理商都关房
	 */
	public boolean gethoteldataallflag(List<HotelData> otadatas) {
		boolean flag = true;
		if (otadatas.size() > 0) {
			for (HotelData hotelData : otadatas) {
				if (hotelData.getType() == 1) {
					if (hotelData.getRoomstatus() == 1) {
						return false;
					}
				}
			}
		}
		return flag;
	}

	/**
	 * 根据对应房型查询对应的开关房状态
	 * 
	 * @param otadatas
	 * @return
	 */
	public boolean gethoteldataflag(List<HotelData> otadatas) {
		boolean flag = false;
		if (otadatas.size() > 0) {
			for (HotelData hotelData : otadatas) {
				if (hotelData.getType() == 1) {
					if (hotelData.getRoomstatus() == -1) {
						WriteLog.write("房态更新", "最低价："
								+ hotelData
										.getAgentName(hotelData.getAgentNo())
								+ "关房");
						flag = true;
						return flag;
					}
					return flag;
				}
			}
		}
		return flag;
	}
	public static void main(String[] args) {
		new FindRoomStaus().test();
	}
	public void test() {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Calendar start = GregorianCalendar.getInstance();
			start.add(Calendar.DAY_OF_MONTH, 0);
			String startdate = sdf.format(start.getTime());
			Sysconfig config = null;
			List<Sysconfig> configs = Server.getInstance().getSystemService()
					.findAllSysconfig("where C_NAME='online'", "", -1, 0);

			if (configs.size() > 0) {
				config = configs.get(0);
			}
			if (config.getValue().equals("1")) {
				Calendar end = GregorianCalendar.getInstance();
				end.add(Calendar.DAY_OF_MONTH, 5);
				String endDate = sdf.format(end.getTime());

				List<Hotel> hotels = Server
						.getInstance()
						.getHotelService()
						.findAllHotel(
								"where  c_push=1 and "
										+ "c_qunarid is not null and c_qunarid!='' and ID in (select distinct c_hotelid from T_HOTELGOODDATA "
										+ "where C_ROOMSTATUS=0 and c_openclose=1 and C_DATENUM>='"
										+ startdate + "' and C_DATENUM<'" + endDate
										+ "')", "order by id desc ", -1, 0);
				int total = hotels.size();
				for (Hotel hotel : hotels) {
					configs = Server.getInstance().getSystemService()
							.findAllSysconfig("where C_NAME='online'", "", -1, 0);
					if (configs.size() > 0) {
						config = configs.get(0);
					}
					if (config.getValue().equals("1")) {
						total--;
						System.out.println("剩余酒店数量：" + total + ",当前酒店："
								+ hotel.getName() + "当前酒店id：" + hotel.getId());
						Calendar caltime = GregorianCalendar.getInstance();
						caltime.setTime(start.getTime());
						while (caltime.before(end)) {
							List<HotelGoodData> hoteldatas = Server
									.getInstance()
									.getHotelService()
									.findAllHotelGoodData(
											"where C_HOTELID="
													+ hotel.getId()
													+ " and C_DATENUM='"
													+ sdf.format(caltime.getTime())
													+ "' and C_ROOMSTATUS=0 and c_openclose=1 and c_qunarname is not null and c_qunarname!='null' and c_qunarname!=''",
											"order by C_HOTELID desc", -1, 0);
							if(hoteldatas.size()>0){
								agentinfos = new HashMap<Long, List<AgentInfo>>();
								qunarDatas = new HashMap<Long, Map<String, List<HotelData>>>();
								loadQunarData(hotel, caltime.getTime());
								if (agentinfos.size() > 0 && qunarDatas.size() > 0) {
									Map<String, List<HotelData>> roomtypesmap = qunarDatas
									.get(hotel.getId());
									List<AgentInfo> agentinfo = agentinfos.get(hotel
											.getId());
									for (HotelGoodData hotelGoodData : hoteldatas) {
										List<HotelData> otadatas = roomtypesmap
										.get(hotelGoodData.getQunarName());
										if (otadatas != null && otadatas.size() > 0) {
											boolean allflag = getRoomStatus(otadatas,
													agentinfo);
											if (allflag) {
												hotelGoodData.setOpenclose(0);
												int i = Server.getInstance()
												.getHotelService()
												.updateHotelGoodData(
														hotelGoodData);
												System.out.println("更改条数：" + i);
											}
										}
									}
								}
							}
							caltime.add(Calendar.DAY_OF_MONTH, 1);
						}

					}
				}

			}
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}

	// 加载去哪酒店数据
	public void loadQunarData(Hotel hotel, Date date) {
		try {
			Thread.sleep(5000);
			System.out.println("停顿5s……");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = GregorianCalendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_MONTH, 1);
		String citycode = "";
		citycode = hotel.getQunarId().substring(0,
				hotel.getQunarId().lastIndexOf("_"));
		String url = "http://hotel.qunar.com/price/detail.jsp?fromDate="
				+ sdf.format(date) + "&toDate=" + sdf.format(cal.getTime())
				+ "&cityurl=" + citycode + "&HotelSEQ="
				+ hotel.getQunarId().trim();
		System.out.println(url);
		String str = HttpClient.httpget(url, "utf-8");
		roomnames = new ArrayList<String>();
		if (str != null && !"".equals(str)) {
			if (str != null && !str.equals("")) {
				String key = str.trim();
				if (!key.equals("")) {
					String strstr = key.substring(key.indexOf("(") + 1, key
							.trim().indexOf("/*"));
					JSONObject datas = JSONObject.fromObject(strstr + "}");
					JSONObject result = datas.getJSONObject("result");
					Iterator<String> strs = result.keys();
					HotelData hoteldata;
					roomDatas = new HashMap<String, List<HotelData>>();
					while (strs.hasNext()) {
						hoteldata = new HotelData();
						String keys = strs.next();
						JSONArray arra = JSONArray.fromObject(result.get(keys));
						if (arra.get(0) != null) {
							hoteldata.setSealprice(Integer.parseInt(arra.get(0)
									.toString()));
						}
						hoteldata.setAgentNo(keys.substring(0, keys.trim()
								.indexOf("|")));
						hoteldata.setHotelid(hotel.getId() + "");
						hoteldata.setHotelname(hotel.getName());
						if (arra.get(2) != null) {
							String strtemp = arra.getString(2);
							hoteldata.setRoomnameBF(getString(strtemp));
						}
						// 开关房
						if (arra.get(9) != null) {
							String strtemp = arra.getString(9);
							hoteldata.setRoomstatus(Integer.parseInt(strtemp));
						}
						// 现预付
						if (arra.get(14) != null) {
							String strtemp = arra.getString(14);
							hoteldata.setType(Integer.parseInt(strtemp));
						}
						if (arra.get(4) != null) {
							String strtemp = arra.getString(4);
							if (YDX_ID.equals(hoteldata.getAgentNo())) {
								String roomtypeidtemp = strtemp.substring(
										strtemp.indexOf("roomId=") + 7,
										strtemp.indexOf("&cpcRoomType")).trim();
								String roomtypeid = roomtypeidtemp
										.substring(roomtypeidtemp
												.lastIndexOf("_") + 1);
								hoteldata.setRoomtypeid(Long
										.parseLong(roomtypeid));
							}
						}
						if (arra.get(3) != null) {
							hoteldata.setRoomname(arra.getString(3));
							if (!roomnames.contains(arra.getString(3))) {
								roomnames.add(arra.getString(3));
							}
							if (roomDatas.containsKey(arra.getString(3))) {
								List<HotelData> datastemp = roomDatas.get(arra
										.getString(3));
								datastemp.add(hoteldata);
								Collections.sort(datastemp);
								roomDatas.put(arra.getString(3), datastemp);
							} else {
								List<HotelData> hoteldatas = new ArrayList<HotelData>();
								hoteldatas.add(hoteldata);
								Collections.sort(hoteldatas);
								roomDatas.put(arra.getString(3), hoteldatas);
							}
						}
					}
					qunarDatas.put(hotel.getId(), roomDatas);
					JSONObject detailBasic = datas.getJSONObject("detailBasic");
					JSONObject vendors = detailBasic.getJSONObject("vendors");
					Iterator<String> names = vendors.keys();
					List<AgentInfo> agentinfo = new ArrayList<AgentInfo>();
					while (names.hasNext()) {
						String agentnul = names.next();
						JSONObject agent = JSONObject.fromObject(vendors
								.get(agentnul));
						if (agent != null) {
							AgentInfo agentinfotemp = new AgentInfo();
							agentinfotemp.setAgentno(agentnul);
							String agentname = agent.getString("name");
							agentinfotemp.setName(agentname);
							if (agent.containsKey("orderAvailable")) {
								JSONArray orderAvailables = agent
										.getJSONArray("orderAvailable");
								Double orderAvailable = Double
										.parseDouble(orderAvailables.get(0)
												.toString());
								agentinfotemp.setOrderAvailable(orderAvailable);
							} else {
								continue;
							}
							if (agent.containsKey("serviceScore")) {
								JSONArray serviceScores = agent
										.getJSONArray("serviceScore");
								Double serviceScore = Double
										.parseDouble(serviceScores.get(0)
												.toString());
								agentinfotemp.setServiceScore(serviceScore);
							} else {
								continue;
							}
							if (agent.containsKey("userSatisfaction")) {
								JSONArray userSatisfactions = agent
										.getJSONArray("userSatisfaction");
								Double userSatisfaction = Double
										.parseDouble(userSatisfactions.get(0)
												.toString());
								agentinfotemp
										.setUserSatisfaction(userSatisfaction);
							} else {
								continue;
							}
							if (agent.containsKey("responseTimeDesc")) {
								JSONArray responseTimeDescs = agent
										.getJSONArray("responseTimeDesc");
								String responseTimeDesc = responseTimeDescs
										.get(0).toString();
								agentinfotemp
										.setResponseTimeDesc(responseTimeDesc);
							} else {
								continue;
							}
							Boolean onSale = agent.getBoolean("onSale");
							agentinfotemp.setOnSale(onSale);
							agentinfo.add(agentinfotemp);
						}
					}
					Collections.sort(agentinfo);
					agentinfos.put(hotel.getId(), agentinfo);
				}
			}
		}
	}

	public List<String> getRoomnames() {
		return roomnames;
	}

	public void setRoomnames(List<String> roomnames) {
		this.roomnames = roomnames;
	}

	public Map<String, List<HotelData>> getRoomDatas() {
		return roomDatas;
	}

	public void setRoomDatas(Map<String, List<HotelData>> roomDatas) {
		this.roomDatas = roomDatas;
	}

	public Map<Long, Map<String, List<HotelData>>> getQunarDatas() {
		return qunarDatas;
	}

	public void setQunarDatas(Map<Long, Map<String, List<HotelData>>> qunarDatas) {
		this.qunarDatas = qunarDatas;
	}

}
