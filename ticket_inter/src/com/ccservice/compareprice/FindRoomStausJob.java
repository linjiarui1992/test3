package com.ccservice.compareprice;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * 
 * @author wzc
 * 房态更新过程……
 * 
 */
public class FindRoomStausJob implements Job {

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		System.out.println("房态更新开始……");
		long time1=System.currentTimeMillis();
		new FindRoomStaus().test();
		long time2=System.currentTimeMillis();
		System.out.println("遍历一遍用时："+(time2-time1)/1000/60+"分钟");
		
	}

}
