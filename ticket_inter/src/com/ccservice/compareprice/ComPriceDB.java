package com.ccservice.compareprice;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.ccservice.b2b2c.atom.server.Server;
import com.ccservice.b2b2c.base.city.City;
import com.ccservice.b2b2c.base.hmhotelprice.Hmhotelprice;
import com.ccservice.b2b2c.base.hmhotelprice.PriceResult;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.b2b2c.base.hotelgooddata.HotelGoodData;
import com.ccservice.b2b2c.base.hotelproduct.hotelproduct;
import com.ccservice.b2b2c.base.roomtype.Roomtype;
/**
 * 
 * @author wzc
 * 华闽，去哪比价实现，数据插入优势数据库
 *
 */
public class ComPriceDB extends Utils {
	private static final long serialVersionUID = 1L;
	// 城市id
	private String cityid = "";
	// 入住日期
	private String startDate = "";
	// 离店日期
	private String endDate = "";
	// 酒店id
	private String hotelid = "";
	// 酒店名称
	private String hotelname = "";
	// 城市名称
	private String cityname;
	// 本地城市id
	private Long profit = 20l;
	//在去哪比其他代理商低1元
	private Long lower=1l;
	// 返回去哪对应的数据集合
	private Map<String, List<HotelData>> roomDatas;
	// 返回的酒店集合
	private List<String> roomnames;
	private Map<Long, List<Roomtype>> mapRoom=new HashMap<Long, List<Roomtype>>();
	// 返回酒店列表qunar数据集合
	private Map<Long, Map<String, List<HotelData>>> qunarDatas;
	// 华敏房价信息
	private Map<Long, Map<Integer, List<Hmhotelprice>>> hmroomprice = new HashMap<Long, Map<Integer, List<Hmhotelprice>>>();

	// 华敏房价信息
	private Map<Long, Map<Integer, List<hotelproduct>>> prods = new HashMap<Long, Map<Integer, List<hotelproduct>>>();
	private Map<Long, PriceResult> dayHMData = new HashMap<Long, PriceResult>();
	// 房型集合
	private List<Roomtype> roomtypes = new ArrayList<Roomtype>();
	// 比价后的数据集合 0没有优势 1有优势 2保持的
	private Map<Integer, List<HotelGoodData>> resultsDatas;
	// 房型对应的早餐集合
	private Map<Long, List<Integer>> roomtypsbf = new HashMap<Long, List<Integer>>();
	
	ComProcPublic pro;

	public ComPriceDB() {
		pro=new ComProcPublic();
		profit=Long.parseLong(PropertyUtil.getValue("profit"));
		lower=Long.parseLong(PropertyUtil.getValue("lower"));
		System.out.println("系统初始化利润："+profit+":"+lower);
	}
	public void readftpfile(Set<String> hotelcodes){
		System.out.println("开始读取ftp文件…………………………");
		try {
		FtpUpfile fUp = new FtpUpfile("123.196.114.122", 21, "HuaMin","q1w2e3r4t5");
		fUp.login();
		ArrayList<String> list = fUp.fileNames("");
		for (String filename : list) {
			if(filename.lastIndexOf(".xml")>0){
				if (filename.indexOf("HMC_ALLOTUPDATE") == 0|| filename.indexOf("HMC_HC") == 0) {
					System.out.println("文件名称："+filename);
					SAXBuilder sb = new SAXBuilder();
					String content=new String(fUp.downFile(filename));
					Document doc=sb.build(new StringReader(content));
					Element root = doc.getRootElement();
					if (filename.indexOf("HMC_HC") == 0) {
						List<Element> contracts = root.getChildren("CONTRACTS");
						for (Element element : contracts) {
							//String contract = element.getChildText("CONTRACT");
							String hotelid = element.getChildText("HOTEL");
							System.out.println("ftp:"+hotelid);
							hotelcodes.add(hotelid);
						}
					}else if (filename.indexOf("HMC_ALLOTUPDATE") == 0
							|| filename.indexOf("CopyHMC_ALLOTUPDATE") == 0) {
						Element CONTRACTS = root.getChild("CONTRACTS");
						List<Element> contracts = CONTRACTS.getChildren("CONTRACT_LIST");
						for (Element element : contracts) {
							//String contract = element.getChildText("CONTRACT");
							String hotelid = element.getChildText("HT_CODE");
							System.out.println("ftp:"+hotelid);
							hotelcodes.add(hotelid);
						}
					}
				}
			}
		}
		for (String filename : list) {
			if (filename.indexOf("HMC_ALLOTUPDATE") == 0
					|| filename.indexOf("HMC_HC") == 0){
				System.out.println("删除："+filename);
				fUp.deleteLoadFile(fUp.getFtpclient(), filename);
			}
		}
		fUp.logout();
		} catch (Exception e) {
			e.printStackTrace();
			for(String hotelcode:hotelcodes){
				StringBuilder sbuf = new StringBuilder();
				File temp = new File("d:\\酒店价格\\CopyHMC_HC" + hotelcode
						+ ".xml");
				if (!temp.exists()) {
					try {
						temp.createNewFile();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
				sbuf.append("<?xml version='1.0' encoding='UTF-8'?><QPUSHALLOTUPDATE><CONTRACTS>/r");
				e.printStackTrace();
				sbuf.append("<CONTRACT_LIST>/r");
				sbuf.append("<CONTRACT></CONTRACT>/r");
				sbuf.append("<HT_CODE>" + hotelcode + "</HT_CODE>/r");
				sbuf.append("</CONTRACT_LIST>/r");
				sbuf.append("</CONTRACTS></QPUSHALLOTUPDATE>/r");
				write(temp, sbuf.toString());
			}
		}
		System.out.println("结束读取ftp文件…………………………");
	}
	public static void main(String[] args) throws Exception {
		new ComPriceDB().getHotelDatas();
	}
	public void getHotelDatas() throws Exception {
		while(true){
			String where =PropertyUtil.getValue("sql");
			List<Hotel> hotels = Server.getInstance().getHotelService().findAllHotel(where, "order by id desc ", -1, 0);
			if (hotels.size() > 0) {
				int i=hotels.size();
				for (Hotel hotel : hotels) {
					//从配置文件中读取的酒店id
					//updateFileHotel();
					long time1=System.currentTimeMillis();
					updateOneHotel(hotel);
					long time2=System.currentTimeMillis();
					System.out.println("用时："+(time2-time1)/1000+"s");
					System.out.println("剩余更新数量:"+i--);
				}
			}
		}
	}
	
	
	public void updateFileHotel() throws Exception{
		Set<String> hotelcodes = new HashSet<String>();
		readftpfile(hotelcodes);
		StringBuilder sbu = new StringBuilder();
		if (hotelcodes.size() > 0) {
			int i = 0;
				for (String code : hotelcodes) {
					if (i == hotelcodes.size() - 1) {
						sbu.append("'" + code + "'");
					} else {
						sbu.append("'" + code + "',");
					}
					i++;
				}
			}
		String where = " where C_NAME not like '%锦江之星%'  and c_star in (3,4,5) and c_paytype=2  and c_state=3  and c_sourcetype=3 ";
		where+=" and c_cityid in ("+PropertyUtil.getValue("cityid")+")";
		String code=sbu.toString();
		if(!"".equals(code)){
			where+=" and c_hotelcode in ("+code+")";
		}
		List<Hotel> hotels=Server.getInstance().getHotelService().findAllHotel(where, "order by id", -1, 0);
		if(hotels.size()>0){
			int i=hotels.size();
			for (Hotel hotel : hotels) {
				System.out.println("变价更新酒店数量："+i--);
				long time1=System.currentTimeMillis();
				updateOneHotel(hotel);
				long time2=System.currentTimeMillis();
				System.out.println("用时："+(time2-time1)/1000+"s");
			}
		}
	}
	/**
	 * 更新单个酒店
	 * @param hotel
	 * @param startt
	 * @param endt
	 * @throws Exception
	 */	
	public void updateOneHotel(Hotel hotel) throws Exception{
		Calendar start = GregorianCalendar.getInstance();
		start.add(Calendar.DAY_OF_MONTH, 0);
		Date t1=start.getTime();
		Calendar end = GregorianCalendar.getInstance();
		end.add(Calendar.DAY_OF_MONTH,28);
		Date t2=end.getTime();
		List<HotelGoodData> datas=Server.getInstance().getIHMHotelService().getQrateHotelData(hotel.getHotelcode(), t1, t2, "", "", "", "" ,"", "","");
		if(datas.size()>0){
			//Map<String, Map<Long, Map<String, List<HotelData>>>> dayqunardatas=pro.loadQunarData(hotel, t1,t2);
			//pro.comparequnar(dayqunardatas, datas, profit, lower, "3");
		}
	}
	private Long roomtypeid;// 房型id
	private String roomtypename;// 去哪房型名称
	// 返回城市名称
	public String getCityname(String cityid) {
		if (!"".equals(cityid)) {
			City city = Server.getInstance().getHotelService().findCity(
					Long.parseLong(cityid));
			if (city != null) {
				return city.getName();
			}
		} else {
			return "未知";
		}
		return "";
	}

	public void loadRoomTypeName(long hotelid) {
		roomtypes = Server.getInstance().getHotelService().findAllRoomtype(
				"where c_hotelid=" + hotelid, "order by id asc", -1, 0);
	}

	// 加载去哪酒店数据
	public void loadQunarData(Hotel hotel, Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = GregorianCalendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_MONTH, 1);
		String citycode = "";
		citycode = hotel.getQunarId().substring(0,
				hotel.getQunarId().lastIndexOf("_"));
		String url = "http://hotel.qunar.com/price/detail.jsp?fromDate="
				+ sdf.format(date) + "&toDate=" + sdf.format(cal.getTime())
				+ "&cityurl=" + citycode + "&HotelSEQ="
				+ hotel.getQunarId().trim();
		System.out.println(url);
		String str = HttpClient.httpget(url, "utf-8");
		roomnames = new ArrayList<String>();
		if (str!=null&&!str.equals("")) {
			String key = str.trim();
			if (!key.equals("")) {
				String strstr = key.substring(key.indexOf("(") + 1, key.trim().indexOf("/*"));
				JSONObject datas = JSONObject.fromObject(strstr + "}");
				JSONObject result = datas.getJSONObject("result");
				Iterator<String> strs = result.keys();
				HotelData hoteldata;
				roomDatas = new HashMap<String, List<HotelData>>();
				while (strs.hasNext()) {
					hoteldata = new HotelData();
					String keys = strs.next();
					JSONArray arra = JSONArray.fromObject(result.get(keys));
					if (arra.get(0) != null) {
						hoteldata.setSealprice(Integer.parseInt(arra.get(0).toString()));
					}
					hoteldata.setAgentNo(keys.substring(0, keys.trim().indexOf("|")));
					hoteldata.setHotelid(hotel.getId() + "");
					hoteldata.setHotelname(hotel.getName());
					if (arra.get(2) != null) {
						String strtemp = arra.getString(2);
						hoteldata.setRoomnameBF(getString(strtemp));
					}
					// 开关房
					if (arra.get(9) != null) {
						String strtemp = arra.getString(9);
						hoteldata.setRoomstatus(Integer.parseInt(strtemp));
					}
					// 现预付
					if (arra.get(14) != null) {
						String strtemp = arra.getString(14);
						hoteldata.setType(Integer.parseInt(strtemp));
					}
					if (arra.get(3) != null) {
						hoteldata.setRoomname(arra.getString(3));
						if (!roomnames.contains(arra.getString(3))) {
							roomnames.add(arra.getString(3));
						}
						if (roomDatas.containsKey(arra.getString(3))) {
							List<HotelData> datastemp = roomDatas.get(arra
									.getString(3));
							datastemp.add(hoteldata);
							Collections.sort(datastemp);
							roomDatas.put(arra.getString(3), datastemp);
						} else {
							List<HotelData> hoteldatas = new ArrayList<HotelData>();
							hoteldatas.add(hoteldata);
							Collections.sort(hoteldatas);
							roomDatas.put(arra.getString(3), hoteldatas);
						}
					}
				}
				qunarDatas.put(hotel.getId(), roomDatas);
				JSONObject detailBasic = datas.getJSONObject("detailBasic");
				JSONObject vendors = detailBasic.getJSONObject("vendors");
				Iterator<String> names = vendors.keys();
				Map<String, String> agentnos = new HashMap<String, String>();
	//			 while (names.hasNext()) {
	//			 String name = names.next();
	//			 JSONObject obj = vendors.getJSONObject(name);
//				 if (!session.containsKey("name")) {
//				 session.put(name, obj.get("name"));
//				 System.out.println("name:" + name + "," + "id:"
//				 + obj.get("name"));
//				 }
//				 if (!agentnos.containsKey(name)) {
//				 agentnos.put(name, obj.get("name").toString());
//				 }
//				 }
				// Set<String> keys= agentnos.keySet();
				// for (String k : keys) {
				// WriteLog.write("去哪代理商大全", k+" - "+agentnos.get(k));
				// }

			}
		}

	}


	public static String getRoomTypeName(String huamincode) {
		List<Roomtype> roomtypes = Server.getInstance().getHotelService()
				.findAllRoomtypeBySql(
						"select * from T_ROOMTYPE where C_ROOMCODE='"
								+ huamincode + "'", -1, 0);
		if (roomtypes.size() > 0) {
			return roomtypes.get(0).getName();
		}
		return "";
	}

	public String SubString(String str, int len) {
		if (str == null)
			return str;
		if (str.length() <= len)
			return str;

		return str.substring(0, Math.abs(len));
	}

	// 根据床型id查找房名
	public String findbedtypename(long bedtypeid) {
		return Server.getInstance().getHotelService().findBedtype(bedtypeid)
				.getTypename();
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getHotelid() {
		return hotelid;
	}

	public void setHotelid(String hotelid) {
		this.hotelid = hotelid;
	}

	public String getHotelname() {
		return hotelname;
	}

	public void setHotelname(String hotelname) {
		this.hotelname = hotelname;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getCityid() {
		return cityid;
	}

	public void setCityid(String cityid) {
		this.cityid = cityid;
	}

	public String getCityname() {
		return cityname;
	}

	public void setCityname(String cityname) {
		this.cityname = cityname;
	}

	public Map<String, List<HotelData>> getRoomDatas() {
		return roomDatas;
	}

	public void setRoomDatas(Map<String, List<HotelData>> roomDatas) {
		this.roomDatas = roomDatas;
	}

	public List<String> getRoomnames() {
		return roomnames;
	}

	public void setRoomnames(List<String> roomnames) {
		this.roomnames = roomnames;
	}

	public Map<Long, Map<Integer, List<Hmhotelprice>>> getHmroomprice() {
		return hmroomprice;
	}

	public void setHmroomprice(
			Map<Long, Map<Integer, List<Hmhotelprice>>> hmroomprice) {
		this.hmroomprice = hmroomprice;
	}

	public List<Roomtype> getRoomtypes() {
		return roomtypes;
	}

	public void setRoomtypes(List<Roomtype> roomtypes) {
		this.roomtypes = roomtypes;
	}

	public Map<Long, Map<String, List<HotelData>>> getQunarDatas() {
		return qunarDatas;
	}

	public void setQunarDatas(Map<Long, Map<String, List<HotelData>>> qunarDatas) {
		this.qunarDatas = qunarDatas;
	}

	public Map<Long, List<Roomtype>> getMapRoom() {
		return mapRoom;
	}

	public void setMapRoom(Map<Long, List<Roomtype>> mapRoom) {
		this.mapRoom = mapRoom;
	}

	public Long getRoomtypeid() {
		return roomtypeid;
	}

	public void setRoomtypeid(Long roomtypeid) {
		this.roomtypeid = roomtypeid;
	}

	public String getRoomtypename() {
		return roomtypename;
	}

	public void setRoomtypename(String roomtypename) {
		this.roomtypename = roomtypename;
	}

	public Long getProfit() {
		return profit;
	}

	public void setProfit(Long profit) {
		this.profit = profit;
	}

	public Map<Integer, List<HotelGoodData>> getResultsDatas() {
		return resultsDatas;
	}


	public void setResultsDatas(Map<Integer, List<HotelGoodData>> resultsDatas) {
		this.resultsDatas = resultsDatas;
	}


	public Map<Long, PriceResult> getDayHMData() {
		return dayHMData;
	}

	public void setDayHMData(Map<Long, PriceResult> dayHMData) {
		this.dayHMData = dayHMData;
	}

	public Map<Long, Map<Integer, List<hotelproduct>>> getProds() {
		return prods;
	}

	public void setProds(Map<Long, Map<Integer, List<hotelproduct>>> prods) {
		this.prods = prods;
	}

	public Map<Long, List<Integer>> getRoomtypsbf() {
		return roomtypsbf;
	}

	public void setRoomtypsbf(Map<Long, List<Integer>> roomtypsbf) {
		this.roomtypsbf = roomtypsbf;
	}


	public Long getLower() {
		return lower;
	}


	public void setLower(Long lower) {
		this.lower = lower;
	}

}
