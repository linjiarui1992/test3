package com.ccservice.compareprice.travelsky;

import java.util.Date;
import org.quartz.Job;
import java.text.SimpleDateFormat;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import com.ccservice.inter.server.Server;

/**
 * 比价，定时关当天房
 * @author WH
 */

public class SuppliersCloseRoomJob implements Job {

    public void execute(JobExecutionContext context) throws JobExecutionException {
        //刷新时间，让去哪儿同步数据，接口作关房逻辑操作
        String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        String sql = "update t_hotel set c_goqunarupdatetime = '" + time + "' where c_push in (1,2)";
        Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        System.out.println(time + "=====酒店比价上去哪儿、定时关当日房间=====");
    }

}
