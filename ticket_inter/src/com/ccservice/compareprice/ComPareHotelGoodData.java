package com.ccservice.compareprice;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.b2b2c.base.hotelgooddata.HotelGoodData;
import com.ccservice.b2b2c.base.sysconfig.Sysconfig;
import com.ccservice.inter.server.Server;

public class ComPareHotelGoodData {
	
//	private Float profit3 = 0.023f;//1-1000  3个点
//	private Float profit25 = 0.02f;//1000-2000 2.5个点
//	private Float profit2 = 0.012f;//2000以上  2 个点

	public Long getSealPricef(Long baseprice){
//		int fee=0;
//		if(baseprice<=1000){
//			fee=(int)(baseprice*0.01+baseprice*profit3)+12;
//		}else if(baseprice>1000&&baseprice<=2000){
//			fee=(int)(baseprice*0.005+baseprice*profit25)+12;
//		}else if(baseprice>2000){
//			fee=(int)(baseprice*0.003+baseprice*profit2)+12;
//		}
//		return baseprice+fee;
		return baseprice+23;
	}
	
	ComProcPublic pro;
	
	Long profit=20l;
	
	Long lower=1l;
	public ComPareHotelGoodData() {
		pro=new ComProcPublic();
	}

	public static void main(String[] args) throws Exception {
		new ComPareHotelGoodData().loadData();
	}
	/**
	 * 加载优势数据表数据进行比价
	 * @throws Exception 
	 */
	public void loadData() throws Exception {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Calendar start = GregorianCalendar.getInstance();
			start.add(Calendar.DAY_OF_MONTH, 0);
			Date t1 = start.getTime();
			Calendar end = GregorianCalendar.getInstance();
			end.add(Calendar.DAY_OF_MONTH, 7);
			Date t2 = end.getTime();
			String sql = "where id in (select distinct c_hotelid from t_hotelgooddata where c_roomstatus=0 and c_openclose=1) and c_push>0";
			List<Hotel> hotels = Server.getInstance().getHotelService().findAllHotel(sql, "order by id desc ", -1, 0);
			System.out.println(hotels.size());
			int i=hotels.size();
			for (Hotel hotel : hotels) {
				List<Sysconfig> configs = Server.getInstance().getSystemService().findAllSysconfig("where C_NAME='online'", "", -1, 0);
				Sysconfig config = null;
				if (configs.size() > 0) {
					config = configs.get(0);
				}
				if (config.getValue().equals("1")) {
				System.out.println("剩余酒店数量:"+i--);
				System.out.println(hotel.getName());
				Calendar cal=Calendar.getInstance();
				cal.setTime(t1);
				while(cal.getTime().before(t2)){
					String sqltemp = "where c_hotelid=" + hotel.getId()
					+ " and C_DATENUM='" + sdf.format(cal.getTime()) + 
					" ' AND C_QUNARNAME IS NOT NULL AND C_QUNARNAME!='' and C_QUNARNAME!='null' and C_BASEPRICE>0 " +
					"and c_openclose=1 and c_roomstatus=0";
					List<HotelGoodData> gooddatas = Server.getInstance().getHotelService().findAllHotelGoodData(sqltemp,"order by C_DATENUM asc", -1, 0);
					if (gooddatas.size() > 0) {
						long time1 = System.currentTimeMillis();
						Map<String, Map<Long, Map<String, List<HotelData>>>> dayqunardatas = pro.loadQunarData(hotel, cal.getTime());
						comparequnar(dayqunardatas, gooddatas, profit, lower);
						long time2 = System.currentTimeMillis();
						long jiange=(time2 - time1) / 1000;
						System.out.println("用时：" + jiange + "秒");
						if(jiange==0){
							Thread.sleep(700);
							System.out.println("暂停700ms……");
						}
					}
					cal.add(Calendar.DAY_OF_MONTH, 1);
				}
				
			}
		}
	}
	/**
	 * 比价代码程序
	 * huc
	 * @param dayqunardatas
	 *            去哪数据集合
	 * @param profit
	 *            最低利润点
	 * @param lower
	 *            比别人家低的价格
	 * @throws Exception
	 */
	public void comparequnar(Map<String, Map<Long, Map<String, List<HotelData>>>> dayqunardatas,
			List<HotelGoodData> hotelGoodDataList, Long profit, Long lower)
	throws Exception {
		label: 	for (HotelGoodData hotelGoodData : hotelGoodDataList) {
			System.out.println("比价日期：" + hotelGoodData.getDatenum());
			Map<Long, Map<String, List<HotelData>>> tem = dayqunardatas.get(hotelGoodData.getDatenum());
			if (tem != null) {
					Map<String, List<HotelData>> rooms = tem.get(hotelGoodData.getHotelid());
					if(rooms!=null&&rooms.size()>0){
						List<HotelData> hoteltdatas = rooms.get(hotelGoodData.getQunarName());
						if (hoteltdatas != null && hoteltdatas.size() > 0) {
							for (HotelData hotelData : hoteltdatas) {
								hotelGoodData.setAgentname(hotelData.getAgentName(hotelData.getAgentNo()));
								hotelGoodData.setSealprice(hotelData.getSealprice().longValue());
								Long sp = hotelGoodData.getBaseprice()+ profit;
								Long profittemp=hotelData.getSealprice()-hotelGoodData.getBaseprice();
								if(profittemp>(profit+lower)){
									hotelGoodData.setProfit(profittemp-lower);
									hotelGoodData.setShijiprice(hotelData.getSealprice()-lower);
								}else{
									hotelGoodData.setProfit(23l);
									hotelGoodData.setShijiprice(getSealPricef(hotelGoodData.getBaseprice()));
								}
								//易订行过滤  关房过滤 休息中过滤
								if (hotelData.getAgentNo().equals("wiotatts037")||hotelData.getRoomstatus() == -1||hotelData.getRoomstatus() == -2) {// 
									continue;
								}
								if (hotelGoodData.getBfcount()==0) {
									if (sp > hotelData.getSealprice()) {
											hotelGoodData.setRoomflag("2");
											Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(hotelGoodData);
											continue label;//没有优势 跳出最外层循环
									} else {
										if (hotelData.getType() == 0&& profittemp>(profit+10)) {
											hotelGoodData.setShijiprice(hotelData.getSealprice()- 10l);
											hotelGoodData.setProfit(hotelData.getSealprice()- hotelGoodData.getBaseprice()- 10l);
										} else if (hotelData.getType() == 0&& profittemp < (profit+10)) {
											hotelGoodData.setRoomflag("2");
											Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(hotelGoodData);
											continue  label;// 跳出最外层循环
										}
										if (hotelGoodData.getProfit() < profit) {
											hotelGoodData.setRoomflag("2");
											Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(hotelGoodData);
											continue label;// 跳出最外层循环
										}
										if (hotelData.getRoomnameBF().contains("早")
												&& !hotelData.getRoomnameBF().equals("无早")
												&& !hotelData.getRoomnameBF().equals("不含早")) {
											if (hotelGoodData.getProfit() > (profit + 10)) {
												hotelGoodData.setShijiprice(hotelGoodData.getShijiprice() - 10);
												hotelGoodData.setProfit(hotelGoodData.getProfit() - 10);
												hotelGoodData.setRoomflag("1");
												System.out.println("当前利润："+ hotelGoodData.getProfit());
												Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(hotelGoodData);
												continue  label;// 跳出最外层循环
											} else if (hotelGoodData.getProfit() > profit + 5) {
												hotelGoodData.setShijiprice(hotelGoodData.getShijiprice() - 5);
												hotelGoodData.setProfit(hotelGoodData.getProfit() - 5);
												hotelGoodData.setRoomflag("1");
												System.out.println("当前利润："+ hotelGoodData.getProfit());
												Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(hotelGoodData);
												continue  label;// 跳出最外层循环
											} else {
												hotelGoodData.setRoomflag("1");
												System.out.println("当前利润："+ hotelGoodData.getProfit());
												Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(hotelGoodData);
												continue  label;// 跳出最外层循环
											}
										} else {
											hotelGoodData.setShijiprice(hotelGoodData.getShijiprice());
											hotelGoodData.setProfit(hotelGoodData.getProfit());
											hotelGoodData.setRoomstatus(0l);
											hotelGoodData.setProfit(profittemp);
											hotelGoodData.setRoomflag("1");
											Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(hotelGoodData);
											continue  label;// 跳出最外层循环
										}
									}
								} else if (hotelGoodData.getBfcount()==1) {
									if (sp > hotelData.getSealprice()) {
											hotelGoodData.setRoomflag("2");
											Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(hotelGoodData);
										continue  label;// 跳出最外层循环
									} else {
										if (hotelData.getRoomnameBF().contains("早")
												&& !hotelData.getRoomnameBF().contains("无早")) {
											if (hotelData.getType() == 0&& profittemp>(profit+10)) {
												hotelGoodData.setShijiprice(hotelData.getSealprice()- 10l);
												hotelGoodData.setProfit(hotelData.getSealprice()- hotelGoodData.getBaseprice()- 10l);
											} else if (hotelData.getType() == 0&& profittemp < (profit+10))  {
												if(!hotelGoodData.getRoomflag().equals("2")){
													hotelGoodData.setRoomflag("2");
													Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(hotelGoodData);
												}
												continue  label;// 跳出最外层循环
											}
											if (hotelGoodData.getProfit() < profit) {
													hotelGoodData.setRoomflag("2");
													Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(hotelGoodData);
												continue  label;// 跳出最外层循环
											}
											if (hotelData.getRoomnameBF().contains("单早")|| hotelData.getRoomnameBF().contains("1份早餐")) {
												hotelGoodData.setRoomflag("1");
												System.out.println("当前利润："+ hotelGoodData.getProfit());
												Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(hotelGoodData);
												continue  label;// 跳出最外层循环
											} else if (hotelData.getRoomnameBF().equals("")|| hotelData.getRoomnameBF().contains("无早")|| hotelData.getRoomnameBF().contains("不含早")) {
												hotelGoodData.setRoomflag("1");
												Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(hotelGoodData);
												System.out.println("当前利润："+ hotelGoodData.getProfit());
												continue  label;// 跳出最外层循环
											} else {
												if (hotelGoodData.getProfit() > (profit + 10)) {
													hotelGoodData.setShijiprice(hotelGoodData.getShijiprice() - 10);
													hotelGoodData.setProfit(hotelGoodData.getProfit() - 10);
													hotelGoodData.setRoomflag("1");
													System.out.println("当前利润："+ hotelGoodData.getProfit());
													Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(hotelGoodData);
													continue  label;// 跳出最外层循环
												} else if (hotelGoodData.getProfit() >= (profit + 5)) {
													hotelGoodData.setShijiprice(hotelGoodData.getShijiprice() - 5);
													hotelGoodData.setProfit(hotelGoodData.getProfit() - 5);
													hotelGoodData.setRoomstatus(0l);
													hotelGoodData.setRoomflag("1");
													System.out.println("当前利润："+ hotelGoodData.getProfit());
													Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(hotelGoodData);
													continue  label;// 跳出最外层循环
												} else {
													hotelGoodData.setRoomflag("1");
													System.out.println("当前利润："+ hotelGoodData.getProfit());
													Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(hotelGoodData);
													continue  label;// 跳出最外层循环
												}
											}
										}
									}
								} else if (hotelGoodData.getBfcount()==2) {
									if (sp > hotelData.getSealprice()) {
										hotelGoodData.setRoomflag("2");
										Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(hotelGoodData);
										continue  label;// 跳出最外层循环
									} else {
										if (hotelData.getType() == 0&& profittemp>(profit+10)) {
											hotelGoodData.setShijiprice(hotelData.getSealprice()- 10l);
											hotelGoodData.setProfit(hotelData.getSealprice()- hotelGoodData.getBaseprice()- 10l);
										} else if (hotelData.getType() == 0&& profittemp < (profit+10)) {
											hotelGoodData.setRoomflag("2");
											Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(hotelGoodData);
											continue  label;// 跳出最外层循环
										}
										if (hotelGoodData.getProfit() < profit) {
											hotelGoodData.setRoomflag("2");
											Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(hotelGoodData);
											continue  label;// 跳出最外层循环
										}
										
										if (hotelData.getRoomnameBF().contains("双早")|| hotelData.getRoomnameBF().equals("")
												|| hotelData.getRoomnameBF().equals("无早")
												|| hotelData.getRoomnameBF().equals("不含早")
												|| hotelData.getRoomnameBF().equals("单早")
												|| hotelData.getRoomnameBF().equals("1份早餐")
												|| hotelData.getRoomnameBF().contains("2份早餐")
												|| hotelData.getRoomnameBF().contains("含早")) {
											hotelGoodData.setRoomflag("1");
											System.out.println("当前利润："+ hotelGoodData.getProfit());
											Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(hotelGoodData);
											continue  label;// 跳出最外层循环
										} else {
											if (hotelGoodData.getProfit() >= (profit + 10)) {
												hotelGoodData.setShijiprice(hotelGoodData.getShijiprice() - 10);
												hotelGoodData.setProfit(hotelGoodData.getProfit() - 10);
											} else if (hotelGoodData.getProfit() >= (profit + 5)) {
												hotelGoodData.setShijiprice(hotelGoodData.getShijiprice() - 5);
												hotelGoodData.setProfit(hotelGoodData.getProfit() - 5);
											}
											System.out.println("当前利润："+ hotelGoodData.getProfit());
											hotelGoodData.setRoomflag("1");
											Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(hotelGoodData);
											continue label;// 跳出最外层循环
										}
									}
								} else if (hotelGoodData.getBfcount()==3) {
									if (sp > hotelData.getSealprice()) {
										hotelGoodData.setRoomflag("2");
										Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(hotelGoodData);
										continue label;// 跳出最外层循环
									} else {
										if (hotelData.getType() == 0&& profittemp>(profit+10)) {
											hotelGoodData.setShijiprice(hotelData.getSealprice()- 10l);
											hotelGoodData.setProfit(hotelData.getSealprice()- hotelGoodData.getBaseprice()- 10l);
										} else if (hotelData.getType() == 0&& profittemp < (profit+10)) {
											hotelGoodData.setRoomflag("2");
											Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(hotelGoodData);
											continue label;// 跳出最外层循环
										}
										if (hotelData.getRoomnameBF().contains("三早")
												|| hotelData.getRoomnameBF().contains("双早")
												|| hotelData.getRoomnameBF().contains("1份早餐")
												|| hotelData.getRoomnameBF().contains("2份早餐")
												|| hotelData.getRoomnameBF().contains("3份早餐")
												|| hotelData.getRoomnameBF().contains("")
												|| hotelData.getRoomnameBF().contains("无早")
												|| hotelData.getRoomnameBF().contains("不含早")
												|| hotelData.getRoomnameBF().contains("单早")
												|| hotelData.getRoomnameBF().contains("含早")) {
											System.out.println("当前利润："+ hotelGoodData.getProfit());
											hotelGoodData.setRoomflag("1");
											Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(hotelGoodData);
											continue label;// 跳出最外层循环
										} else {
											if (hotelGoodData.getProfit() >= (profit + 10)) {
												hotelGoodData.setShijiprice(hotelGoodData.getShijiprice() - 10);
												hotelGoodData.setProfit(hotelGoodData.getProfit() - 10);
												hotelGoodData.setRoomstatus(0l);
												hotelGoodData.setRoomflag("1");
												System.out.println("当前利润："+ hotelGoodData.getProfit());
												Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(hotelGoodData);
												continue label;// 跳出最外层循环
											} else if (hotelGoodData.getProfit() >= (profit + 5)) {
												hotelGoodData.setShijiprice(hotelGoodData.getShijiprice() - 5);
												hotelGoodData.setProfit(hotelGoodData.getProfit() - 5);
												System.out.println("当前利润："+ hotelGoodData.getProfit());
												hotelGoodData.setRoomflag("1");
												Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(hotelGoodData);
												continue label;// 跳出最外层循环
											} else {
												System.out.println("当前利润："+ hotelGoodData.getProfit());
												hotelGoodData.setRoomflag("1");
												Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(hotelGoodData);
												continue label;// 跳出最外层循环
											}
										}
									}
								} else if (hotelGoodData.getBfcount()==4) {
									if (sp > hotelData.getSealprice()) {
										hotelGoodData.setRoomflag("2");
										Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(hotelGoodData);
										continue label;// 跳出最外层循环
									} else {
										if (hotelData.getType() == 0&& profittemp>(profit+10)) {
											hotelGoodData.setShijiprice(hotelData.getSealprice()- 10l);
											hotelGoodData.setProfit(hotelData.getSealprice()- hotelGoodData.getBaseprice()- 10l);
										} else if (hotelData.getType() == 0&& profittemp < (profit+10)) {
											hotelGoodData.setRoomflag("2");
											Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(hotelGoodData);
											continue label;// 跳出最外层循环
										}
										if (hotelGoodData.getProfit() < profit) {
											hotelGoodData.setRoomflag("2");
											Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(hotelGoodData);
											continue label;// 跳出最外层循环
										}
										if (hotelData.getRoomnameBF().contains("四早")
												|| hotelData.getRoomnameBF().contains("双早")
												|| hotelData.getRoomnameBF().contains("2份早餐")
												|| hotelData.getRoomnameBF().contains("4份早餐")
												|| hotelData.getRoomnameBF().contains("三早")
												|| hotelData.getRoomnameBF().contains("3份早餐")
												|| hotelData.getRoomnameBF().contains("")
												|| hotelData.getRoomnameBF().contains("无早")
												|| hotelData.getRoomnameBF().contains("不含早")
												|| hotelData.getRoomnameBF().contains("单早")
												|| hotelData.getRoomnameBF().contains("1份早餐")
												|| hotelData.getRoomnameBF().contains("含早")) {
											hotelGoodData.setRoomflag("1");
											System.out.println("当前利润："+ hotelGoodData.getProfit());
											Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(hotelGoodData);
											continue label;// 跳出最外层循环
										} else {
											if (hotelGoodData.getProfit() >= (profit + 10)) {
												hotelGoodData.setShijiprice(hotelGoodData.getShijiprice() - 10);
												hotelGoodData.setProfit(hotelGoodData.getProfit() - 10);
												System.out.println("当前利润："+ hotelGoodData.getProfit());
												hotelGoodData.setRoomflag("1");
												Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(hotelGoodData);
												continue label;// 跳出最外层循环
											} else if (hotelGoodData.getProfit() >= (profit + 5)) {
												hotelGoodData.setShijiprice(hotelGoodData.getShijiprice() - 5);
												hotelGoodData.setProfit(hotelGoodData.getProfit() - 5);
												hotelGoodData.setRoomflag("1");
												Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(hotelGoodData);
												continue label;// 跳出最外层循环
											} else {
												hotelGoodData.setRoomflag("1");
												Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(hotelGoodData);
												continue label;// 跳出最外层循环
											}
										}
									}
								}
							}
						}
					}
				}
			}
	}

	public ComProcPublic getPro() {
		return pro;
	}

	public void setPro(ComProcPublic pro) {
		this.pro = pro;
	}

}
