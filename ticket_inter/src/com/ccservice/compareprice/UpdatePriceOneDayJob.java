package com.ccservice.compareprice;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
/**
 * 
 * @author wzc
 *  更新第31天的数据
 */
public class UpdatePriceOneDayJob implements Job{

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		System.out.println("开始更新第31天的数据");
		try {
			new UpdatePriceOneDay().test();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("更新第31天的数据结束");
	}

}
