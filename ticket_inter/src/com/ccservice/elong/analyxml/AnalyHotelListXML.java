package com.ccservice.elong.analyxml;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.b2b2c.base.service.IHotelService;
import com.ccservice.elong.inter.DateSwitch;

/**
 * 解析XML文件
 * 
 * @author 师卫林
 * 
 */
public class AnalyHotelListXML {
	public static void main(String[] args) {
		readXML("D:\\酒店数据\\hotellist.xml");
	}
	@SuppressWarnings("unchecked")
	public static void readXML(String filename){
		// 解析器
		SAXReader reader=new SAXReader();
		// 指定XML文件
		File file=new File(filename);
		try{
			Document doc=reader.read(file);
			Element rootElement=doc.getRootElement();
			//System.out.println(rootElement.getName());
			//获取所有HotelInfoForIndex的元素集合
			List list=rootElement.elements("HotelInfoForIndex");
			parseXML(list);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	@SuppressWarnings("unchecked")
	private static void parseXML(List list) throws FileNotFoundException, SQLException {
		// TODO Auto-generated method stub
		String url = "http://localhost:8080/cn_service/service/";

		HessianProxyFactory factory = new HessianProxyFactory();
		IHotelService servier = null;
		try {
			servier = (IHotelService) factory.create(IHotelService.class, url
					+ IHotelService.class.getSimpleName());
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		long startTime=System.currentTimeMillis();
		System.out.println("一次循环开始时间:"+startTime);
		Iterator it=list.iterator();
		while(it.hasNext()){
			Hotel hotel=new Hotel();
			Element element=(Element)it.next();
			//酒店中文名
			if(!element.elementText("Hotel_name").equals("")){
				System.out.println("Hotel_name:"+element.elementText("Hotel_name"));
				String hotelName=element.elementText("Hotel_name");
				hotel.setName(hotelName);
				
			}
			//酒店英文名
			if(!element.elementText("Hotel_name_en").equals("")){
				System.out.println("Hotel_name_en:"+element.elementText("Hotel_name_en"));
				String hotelEnName=element.elementText("Hotel_name_en");
				hotel.setEnname(hotelEnName);
			}
			//酒店id（酒店在艺龙系统中的唯一标识HotelID）
			if(!element.elementText("Hotel_id").equals("")){
				System.out.println("Hotel_id:"+element.elementText("Hotel_id"));
				String hotel_id=element.elementText("Hotel_id");
				hotel.setHotelcode(hotel_id);
			}
			//标示此酒店是否可用	表示当前酒店可用与否 3表示可用 0表示暂时不可用
			if(!element.elementText("Isreserve").equals("")){
				System.out.println("Isreserve:"+element.elementText("Isreserve"));
				String Isreserve=element.elementText("Isreserve");
				if(Isreserve.equals("0")){
					hotel.setState(3);
					hotel.setStatedesc("该酒店可用");
				}else{
					hotel.setState(0);
					hotel.setStatedesc("该酒店暂不可用");
				}
			}
			//此酒店上线销售时间
			if(!element.elementText("Addtime").equals("")){
				//System.out.println("Addtime:"+element.elementText("Addtime"));
			}
//			//此酒店下线停止销售时间
//			if(!element.elementText("Deltime").equals("")){
//				//System.out.println("Deltime:"+element.elementText("Deltime"));
//			}
			//此酒店最近一次修改时间  暂时不使用
			if(!element.elementText("Modifytime").equals("")){
				System.out.println("Modifytime:"+element.elementText("Modifytime"));
				String modifytime=element.elementText("Modifytime");
				hotel.setRepaildate(modifytime);
			}
			List<Hotel> hotelList=servier.findAllHotel("where "+Hotel.COL_hotelcode+"='"+hotel.getHotelcode()+"'", "", -1, 0);
			if(hotelList.size()>0){
				hotel.setId(hotelList.get(0).getId());
				servier.updateHotelIgnoreNull(hotel);
			}else{
				hotel=servier.createHotel(hotel);
			}
			System.out.println();
		}
		System.out.println("ok!!!");
		long endTime=System.currentTimeMillis();
		System.out.println("一次循环结束时间:"+endTime);
		System.out.println("共需要时间:"+DateSwitch.showTime(endTime-startTime));
	}
}
