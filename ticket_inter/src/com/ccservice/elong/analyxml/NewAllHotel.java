package com.ccservice.elong.analyxml;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.XPath;
import org.dom4j.io.SAXReader;

import com.ccservice.elong.inter.DateSwitch;
import com.ccservice.elong.inter.DownFile;

public class NewAllHotel {
	public static void main(String[] args) throws Exception {
		DownFile.download("http://114-svc.elong.com/xml/hotellist.xml",
		"D:\\酒店数据\\hotellist.xml");
		readXML("D:\\酒店数据\\hotellist.xml");
		//anay("31201727");
		//anay("31701111");
	}
	@SuppressWarnings("unchecked")
	public static void readXML(String filename){
		// 解析器
		SAXReader reader=new SAXReader();
		// 指定XML文件
		File file=new File(filename);
		try{
			Document doc=reader.read(file);
			Element rootElement=doc.getRootElement();
			//System.out.println(rootElement.getName());
			//获取所有HotelInfoForIndex的元素集合
			List list=rootElement.elements("HotelInfoForIndex");
			parseXML(list);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	@SuppressWarnings("unchecked")
	public static void parseXML(List list) throws Exception {
		long startTime = System.currentTimeMillis();
		System.out.println("一次循环开始时间:" + startTime);
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Element element = (Element) it.next();
			// 标示此酒店是否可用 表示当前酒店可用与否 3表示可用 0表示暂时不可用
			if (!element.elementText("Isreserve").equals("")) {
				System.out.println("Isreserve:" + element.elementText("Isreserve"));
				String Isreserve = element.elementText("Isreserve");
				if (Isreserve.equals("0")) {
					anay(element.elementText("Hotel_id"));
				}
			}
			System.out.println("ok!!!");
			long endTime = System.currentTimeMillis();
			System.out.println("一次循环结束时间:" + endTime);
			System.out.println("共需要时间:" + DateSwitch.showTime(endTime - startTime));
		}
	}

	@SuppressWarnings("unchecked")
	public static void anay(String hotelID) {
		String urltemp = "http://114-svc.elong.com/xml/v1.2/perhotelcn/" + hotelID + ".xml";
		System.out.println("urltemp==" + urltemp);
		try {
			URL url = new URL(urltemp);
			URLConnection con = url.openConnection();
			//设置连接主机超时(单位:毫秒)
			con.setConnectTimeout(60000);
			//设置从主机读取数据超时(单位:毫秒)
			con.setReadTimeout(60000);
			//是否想url输出
			con.setDoOutput(true);
			//设定传送的内容类型是可序列化的对象
			con.setRequestProperty("Content-type", "application/x-java-serialized-object");
			String sCurrentLine;
			String sTotalString;
			sCurrentLine = "";
			sTotalString = "";
			InputStream in = con.getInputStream();
			BufferedReader bf = new BufferedReader(new InputStreamReader(in, "utf-8"));
			while ((sCurrentLine = bf.readLine()) != null) {
				sTotalString += sCurrentLine;
			}
			// System.out.println(sTotalString);
			Map map = new HashMap();
			map.put("q1:", "http://api.elong.com/staticInfo");
			org.dom4j.Document document = DocumentHelper.parseText(sTotalString);
			XPath path = document.createXPath("HotelDetail");
			path.setNamespaceURIs(map);
			List nodelist = path.selectNodes(document);
			NewHotel.parseXML(nodelist);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
