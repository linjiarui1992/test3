package com.ccservice.elong.analyxml;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.XPath;
import org.dom4j.io.SAXReader;

/**
 * 解析所有酒店的信息
 * 
 * @author 师卫林
 * 
 */
public class AnalyAllHotelId {
	public static void main(String[] args) throws Exception {
		listFile("F:\\静态数据\\酒店详细信息");
	}

	@SuppressWarnings("unchecked")
	public static void listFile(String filePath) throws Exception {
		File file = new File(filePath);

		if (file.exists()) {
			if (file.isDirectory()) {
				System.out.println("Folder:" + file.toString());
				File[] fileArray = file.listFiles();
				//System.out.println("includes" + fileArray.length+ "files/folders directly");
				for (File subFile : fileArray) {
					listFile(subFile.toString());
				}
			} else {
				// System.out.println(file.toString());
				Map map = new HashMap();
				map.put("q1:", "http://api.elong.com/staticInfo");
				SAXReader reader = new SAXReader();
				File file1 = new File(file.toString());
				Document document = reader.read(file1);
				XPath path = document.createXPath("HotelDetail");
				path.setNamespaceURIs(map);
				List nodelist = path.selectNodes(document);
				//System.out.println(nodelist.size());
				AnalyHotelIdXML.parseXML(nodelist);
			}
		} else {
			// System.err.print("The file/folder doesn't exist!");
		}
	}
}