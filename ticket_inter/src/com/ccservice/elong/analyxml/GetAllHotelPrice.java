package com.ccservice.elong.analyxml;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.XPath;

import com.ccservice.b2b2c.atom.server.Server;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.elong.hoteldb.GetHotelPrice;

/**
 * 用来获取所有酒店的价格信息
 * 
 * @author 师卫林
 * 
 */
public class GetAllHotelPrice {
	public static String findHotel = "SELECT ID,C_HOTELCODE,C_NAME FROM T_HOTEL WHERE C_STATE=3 ORDER BY C_HOTELCODE";

	public static void main(String[] args) {
		System.out.println("start updatestartprice.........");
		updatestartprice();
		//anay("01901220");
		System.out.println("updatestartprice over.........");
	}

	@SuppressWarnings("unchecked")
	public static void updatestartprice() {
		List<Hotel> hotellist = Server.getInstance().getSystemService().findMapResultBySql(findHotel, null);
		for (int i = 0; i < hotellist.size(); i++) {
			Map map = (Map) hotellist.get(i);
			String hotelId = map.get("C_HOTELCODE").toString();
			anay(hotelId);
		}
	}
	@SuppressWarnings("unchecked")
	public static void anay(String hotelID) {
		String urltemp = "http://114-svc.elong.com/xml/v1.2/perhotelcn/" + hotelID + ".xml";
		System.out.println("urltemp==" + urltemp);
		try {
			URL url = new URL(urltemp);
			URLConnection con = url.openConnection();
			//设置连接主机超时(单位:毫秒)
			con.setConnectTimeout(60000);
			//设置从主机读取数据超时(单位:毫秒)
			con.setReadTimeout(60000);
			//是否想url输出
			con.setDoOutput(true);
			//设定传送的内容类型是可序列化的对象
			con.setRequestProperty("Content-type", "application/x-java-serialized-object");
			String sCurrentLine;
			String sTotalString;
			sCurrentLine = "";
			sTotalString = "";
			InputStream in = con.getInputStream();
			BufferedReader bf = new BufferedReader(new InputStreamReader(in, "utf-8"));
			while ((sCurrentLine = bf.readLine()) != null) {
				sTotalString += sCurrentLine;
			}
			Map map = new HashMap();
			map.put("q1:", "http://api.elong.com/staticInfo");
			org.dom4j.Document document = DocumentHelper.parseText(sTotalString);
			XPath path = document.createXPath("HotelDetail");
			path.setNamespaceURIs(map);
			List nodelist = path.selectNodes(document);
			readhotel(nodelist);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public static void readhotel(List list) throws Exception {
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Element element = (Element) it.next();
			// 酒店id
			String hotelId = element.elementText("id");
			System.out.println("酒店ID:"+hotelId);
			// 酒店名称
			String hotelName = element.elementText("name");
			System.out.println("酒店名字:"+hotelName);
			// 酒店所在城市
			String cityId = element.elementText("city");
			System.out.println("酒店所在城市:"+cityId);
			GetHotelPrice.getHotelPrice(hotelName, hotelId, cityId);
		}
		System.out.println("readHotel ok!!!");
	}

}
