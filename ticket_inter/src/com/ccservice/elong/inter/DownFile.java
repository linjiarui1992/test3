package com.ccservice.elong.inter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import com.ccervice.huamin.update.HMRequestUitl;
import com.ccervice.huamin.update.Util;
/**
 * 下载文件
 * @author 师卫林
 */
public class DownFile {
	public static void main(String[] args) throws Exception {
		//String encodedStr = java.net.URLEncoder.encode("我是中文", "GBK"); // 有时用UTF-8
		//download("http://www.baidu.com?args=" + encodedStr, "c:\\ret.html");
		File file=new File("D:\\酒店数据");
		if(!file.exists()){
			file.mkdirs();
		}
		download(HMRequestUitl.getHMRequestUrlHeader() + "&api=qcountryareacity&p_lang=SIM&p_country=TWN"
, "D:\\华闽酒店数据\\中国台湾.xml");
	}

	public static void download(String urlString, String filename) throws Exception {
		File file=new File("D:\\酒店数据");
		if(!file.exists()){
			file.mkdirs();
		}
		System.out.println("download xml start.......");
		URL url = new URL(urlString);
		URLConnection con = url.openConnection();
		InputStream is = con.getInputStream();

		byte[] bs = new byte[1024];
		int len;
		OutputStream os = new FileOutputStream(filename);
		while ((len = is.read(bs)) != -1) {
			os.write(bs, 0, len);
		}
		os.close();
		is.close();
		System.out.println("download xml over.......");
	}
}
