package com.ccservice.elong.inter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
/**
 * 用于解析艺龙所有可有酒店的ID
 * @author 师卫林
 *
 */
public class CreateLstFile {
	public static void main(String[] args) {
		readXML("F:\\航天华有\\酒店文档\\艺龙酒店API接口说明文档_V1.1.6(1)\\hotellist.xml");
	}

	@SuppressWarnings("unchecked")
	public static void readXML(String filename) {
		// 解析器
		SAXReader reader = new SAXReader();
		// 指定XML文件
		File file = new File(filename);
		try {
			Document doc = reader.read(file);
			Element rootElement = doc.getRootElement();
			// System.out.println(rootElement.getName());
			// 获取所有HotelInfoForIndex的元素集合
			List list = rootElement.elements("HotelInfoForIndex");
			parseXML(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	private static void parseXML(List list) throws FileNotFoundException, SQLException {
		Iterator it = list.iterator();
		PrintWriter pw = new PrintWriter(new FileOutputStream("C:\\Documents and Settings\\Administrator\\桌面\\HotelID.lst"));
		while (it.hasNext()) {
			Element element = (Element) it.next();
			// http://114-svc.elong.com/xml/v1.1/perhotelcn/HotelID.xml
			if (element.elementText("Isreserve") != "") {
				String hotel_id = element.elementText("Hotel_id");
				String Isreserve = element.elementText("Isreserve");
				if (Isreserve.equals("0")) {
					System.out.println("酒店ID:" + hotel_id);
					StringBuilder sb = new StringBuilder();
					sb.append("http://114-svc.elong.com/xml/v1.1/perhotelcn/").append(hotel_id).append(".xml").append("\n");
					pw.write(sb.toString());
				} else {
					continue;
				}
			}
		}
		pw.flush();
		pw.close();
		System.out.println("ok!!!");
	}
}
