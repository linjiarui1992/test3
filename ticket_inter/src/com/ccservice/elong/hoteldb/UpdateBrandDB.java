package com.ccservice.elong.hoteldb;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Iterator;

import org.quartz.Job;
import org.dom4j.Element;
import org.dom4j.Document;
import org.dom4j.io.SAXReader;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.inter.server.Server;
import com.ccservice.elong.inter.DownFile;
import com.ccservice.elong.inter.DateSwitch;
import com.ccservice.b2b2c.base.chaininfo.Chaininfo;

/**
 * 酒店品牌
 */
public class UpdateBrandDB implements Job{
	public static void main (String[] args) throws Exception{
		long start = System.currentTimeMillis();
		System.out.println("start time:" + new Date());
		//DownFile.download("http://114-svc.elong.com/xml/brand_cn.xml","D:\\酒店数据\\brand_cn.xml");
		readXML("D:\\酒店数据\\brand_cn.xml");
		//GetAllHotelPrice.updatestartprice();
		long end = System.currentTimeMillis();
		System.out.println("end time:" + new Date());
		System.out.println("耗时:" + DateSwitch.showTime(end - start));
	}
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {
			DownFile.download("http://114-svc.elong.com/xml/brand_cn.xml","D:\\酒店数据\\brand_cn.xml");
			long start = System.currentTimeMillis();
			System.out.println("start time:" + new Date());
			readXML("D:\\酒店数据\\brand_cn.xml");
			//GetAllHotelPrice.updatestartprice();
			long end = System.currentTimeMillis();
			System.out.println("end time:" + new Date());
			System.out.println("耗时:" + DateSwitch.showTime(end - start));
		} catch (Exception e) {
		}
	}

	@SuppressWarnings("unchecked")
	public static void readXML(String filename) {
		// 解析器
		SAXReader reader = new SAXReader();
		// 指定XML文件
		File file = new File(filename);
		try {
			Document doc = reader.read(file);
			Element rootElement = doc.getRootElement();
			// 获取所有brand的元素集合
			List list = rootElement.elements("brand");
			parseXML(list);
		} catch (Exception e) {
		}
	}
	/**
	 * 解析
	 * @param list
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static void parseXML(List list) throws Exception {
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Chaininfo chaininfo=new Chaininfo();
			Element element = (Element) it.next();
			// 连锁酒店品牌中文简称
			if (element.elementText("brandName")!=null && !element.elementText("brandName").equals("")) {
				chaininfo.setName(element.elementText("brandName"));
			}
			// 连锁品牌ID 
			if (element.elementText("brandID")!=null && !element.elementText("brandID").equals("")) {
				chaininfo.setBindid(element.elementText("brandID"));
			}
			// 连锁品牌名称首字母
			if (element.elementText("brandFirstletter")!=null && !element.elementText("brandFirstletter").equals("")) {
				chaininfo.setShortname(element.elementText("brandFirstletter"));
			}
			// 连锁品牌图片地址的URL
			if (element.elementText("picURL")!=null && !element.elementText("picURL").equals("")) {
				chaininfo.setImagepic(element.elementText("picURL"));
			}
			// 连锁品牌包含酒店数量
			if (element.elementText("hotelCount")!=null && !element.elementText("hotelCount").equals("")) {
				chaininfo.setTotal(element.elementText("hotelCount"));
			}
			// 全称
			if (!element.elementText("brandPinYin") .equals("")) {
				chaininfo.setFullname(element.elementText("brandPinYin"));
			}
			List<Chaininfo> chainList = 
				Server.getInstance().getHotelService().findAllChaininfo("where C_BRANDID = '"+chaininfo.getBindid()+"'", "", -1, 0);
			if(chainList!=null && chainList.size()>0){
				chaininfo.setId(chainList.get(0).getId());
				Server.getInstance().getHotelService().updateChaininfoIgnoreNull(chaininfo);
				System.out.println("更新酒店品牌----------" + chaininfo.getName());
			}else{
				chaininfo=Server.getInstance().getHotelService().createChaininfo(chaininfo);
				System.out.println("新增酒店品牌----------" + chaininfo.getName());
			}
		}
	}
}
