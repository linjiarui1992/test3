package com.ccservice.elong.hoteldb;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.ccservice.b2b2c.atom.server.Server;
import com.ccservice.b2b2c.base.city.City;
import com.ccservice.b2b2c.base.hotellandtype.HotelLandType;
import com.ccservice.b2b2c.base.landmark.Landmark;

public class ElongLandMark {
	@SuppressWarnings("unchecked")
	public static void main(String[] args){
		List<City> localCitys = Server.getInstance().getHotelService().findAllCity("where C_TYPE = 1 and C_ELONGCITYID is not null", "ORDER BY ID", -1, 0);
		for(City city:localCitys){
			System.out.println("开始解析城市："+city.getName());
			try {
				String current = "jsonp" + System.currentTimeMillis();
				String elcityid = city.getElongcityid().length()==3 ? "0" + city.getElongcityid() : city.getElongcityid();
				String link = "http://hotel.elong.com/hotsuggestall.html?callback="+current+"&cityId="+elcityid+"&language=cn";
				URL url = new URL(link);
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setDoInput(true);
				conn.setDoOutput(false);
				conn.connect();
				InputStream inputStream = conn.getInputStream();
				BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
				StringBuilder sb = new StringBuilder();
				String str = "";
				while ((str = br.readLine()) != null) {
					sb.append(str);
				}
				conn.disconnect();
				inputStream.close();
				br.close();
				String strReturn = sb.toString().trim();
				if((current+"([])").equals(strReturn)){
					continue;
				}
				strReturn = strReturn.replace(current+"(", "{\""+current+"\":").replace(")", "}");
				JSONObject datas = JSONObject.fromObject(strReturn);
				JSONArray arys = datas.getJSONArray(current);
				for(int i = 0 ; i < arys.size() ; i++){
					JSONObject obj = (JSONObject) arys.get(i);
					String typeName = (String) obj.get("TypeName");
					String typeId = obj.getString("TypeId");
					if(typeName==null||"".equals(typeName.trim())){
						continue;
					}
					if(!"1".equals(typeId) && !"2".equals(typeId)){ //1-交通枢纽 ； 2-地铁站
						continue;
					}
					
					JSONArray suggestTagDataNodes = obj.getJSONArray("SuggestTagDataNodes");
					for(int j = 0 ; j < suggestTagDataNodes.size() ; j++){
						JSONObject tag = (JSONObject) suggestTagDataNodes.get(j);
						String tagName = (String) tag.get("TagName");
						if(tagName==null || "".equals(tagName.trim())){
							continue;
						}
						HotelLandType hlt = new HotelLandType();
						if("2".equals(typeId)){
							hlt.setName(tagName);
							hlt.setParentid("1".equals(typeId)?1l:2l);
							hlt.setCityid(city.getId());
							String sql = "where " + HotelLandType.COL_name + " = '" + hlt.getName() + 
									     "' and " + HotelLandType.COL_parentid + " = " + hlt.getParentid() + 
									     " and " + HotelLandType.COL_cityid + " = " + hlt.getCityid();
							List<HotelLandType> list = Server.getInstance().getHotelService().findAllHotelLandType(sql, "", -1, 0);
							if(list==null || list.size()==0){
								hlt = Server.getInstance().getHotelService().createHotelLandType(hlt);
								System.out.println(city.getName() + "新增交通枢纽或地铁站类型：" + tagName);
							}else{
								hlt = list.get(0);
							}
						}else{
							if("火车站".equals(tagName)){
								hlt.setId(3l);
							}
							if("机场".equals(tagName)){	
								hlt.setId(4l);
							}
						}
						
						JSONArray suggestDatas = tag.getJSONArray("SuggestDatas");
						for(int k = 0 ; k < suggestDatas.size(); k++){
							JSONObject data = (JSONObject) suggestDatas.get(k);
							String dataName = (String) data.get("DataName");
							if(dataName==null || "".equals(dataName.trim())){
								continue;
							}
							Landmark l = new Landmark();
							l.setName(dataName);
							l.setCityid(Long.valueOf(city.getId()));
							l.setType(hlt.getId()+"");
							l.setLanguage(0);
							l.setElongmarkid(data.getString("PropertiesId"));
							if(data.getString("GoogleLng")!=null&&!"".equals(data.getString("GoogleLng").trim())){
								l.setLng(Double.valueOf(data.getString("GoogleLng")));
								l.setLat(Double.valueOf(data.getString("GoogleLat")));
							}
							String sql = "where " + Landmark.COL_cityid + " = " + l.getCityid() + 
										 " and " + Landmark.COL_type + " = '" + l.getType() + 
										 "' and " + Landmark.COL_elongmarkid + " = '" + l.getElongmarkid() + "'";
							List<Landmark> list = Server.getInstance().getHotelService().findAllLandmark(sql, "", -1, 0);
							if(list==null || list.size()==0){
								Server.getInstance().getHotelService().createLandmark(l);
								System.out.println(city.getName() + "新增地标：" + dataName);
							}
						}
					}
				}
			} catch (Exception e) {
				System.out.println("解析城市："+city.getName()+"异常");
			}
		}
	}
}
