package com.ccservice.elong.hoteldb;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.ElementHandler;
import org.dom4j.ElementPath;
import org.dom4j.XPath;

import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.b2b2c.base.hotelimage.Hotelimage;
import com.ccservice.elong.inter.DateSwitch;
/**
 * 
 * @author wzc
 * 图片xml处理
 */
public class MyElementHandlerImage implements ElementHandler{

//	Map<String, Long> keyvalues=new HashMap<String, Long>();
	
	
	public MyElementHandlerImage() {
//		long t1=System.currentTimeMillis();
//		List result=Server.getInstance().getSystemService().findMapResultBySql("select id,c_hotelcode from t_hotel where c_sourcetype=1", null);
//		for (int i = 0; i < result.size(); i++) {
//			Map m=(Map)result.get(i);
//			String hotelcode=m.get("c_hotelcode").toString();
//			Long id=Long.parseLong(m.get("id").toString());
//			keyvalues.put(hotelcode, id);
//		}
//		long t2=System.currentTimeMillis();
//		System.out.println("用时："+(t2-t1)/1000+"s");
	}
	
	@Override
	public void onEnd(ElementPath path) {
		// 获取当前节点
		Element row = path.getCurrent();
		// System.out.println("row:"+row);
		// 对节点进行操作...
		String tree = path.getPath().substring(26);
		// System.out.println("tree:"+tree);
		if (tree.equals("HotelInfoForIndex")) {
			updateHotel(row);
		}
		// 处理当前节点后,将其从dom树种剪除
		row.detach();
		
	}
	@SuppressWarnings("unchecked")
	public void updateHotel(Element root) {
		System.out.println("...............updateHotel..............");
		List<Element> list = root.elements();
		Hotel hotel = new Hotel();
		String isOk = "";
		String hotelCode = "";
		String deltime = "";
		String addtime = "";
		String modifytime = "";
		String ExtensionData="";
		for (int i = 0; i < list.size(); i++) {
			Element e=list.get(i);
			switch (i) {
			case 0:
				ExtensionData=e.getText();
				break;
			case 1:
				addtime = e.getText();
				break;
			case 2:
				// 此酒店下线停止销售时间
				deltime = e.getText();
				break;
			case 3:
				// 酒店id（酒店在艺龙系统中的唯一标识HotelID）
				hotelCode = e.getText();
				System.out.println("酒店ID:" + hotelCode);
				hotel.setHotelcode(hotelCode);
				break;
			case 4:
				hotel.setName(e.getText());// 酒店中文名
				break;
			case 5:
				hotel.setEnname(e.getText());// 酒店英文名
				break;
			case 6:
				isOk = e.getText();// 酒店是否可用
				break;
			case 7:
				modifytime=e.getText();// 最新修改时间
				break;
			default:
				break;
			}
		}
		if (deltime.equals("")) {
			if (isOk.equals("0")) {
				try {
					//hotel.setId(keyvalues.get(hotelCode));
					addOrUpdateHotelimage(hotelCode);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			} else {
				//if (keyvalues.containsKey(hotelCode)) {
					hotel.setState(0);
				//	hotel.setStatedesc("该酒店暂不可用");
				//	System.out.println("不可用\\\\");
				//	hotel.setId(keyvalues.get(hotelCode));
				//	Server.getInstance().getHotelService().updateHotelIgnoreNull(hotel);
				//}
			}
		} else {
			Date delTime;
			try {
				delTime = DateSwitch.StringSwitchUDate(deltime);
				if (isOk.equals("0") || delTime.after(new Date())) {
					//hotel.setId(keyvalues.get(hotelCode));
					addOrUpdateHotelimage(hotelCode);
				} else {
					//if (keyvalues.containsKey(hotelCode)) {
						hotel.setState(0);
						hotel.setStatedesc("该酒店暂不可用");
						System.out.println("不可用....");
					//	hotel.setId(keyvalues.get(hotelCode));
					//	Server.getInstance().getHotelService().updateHotelIgnoreNull(hotel);
					//}
				}
			} catch (Exception e1) {
				
			}
		}
	}
	
	public static void addOrUpdateHotelimage(String hotelCode){
		File file = new File("D:\\hotelimage\\"+hotelCode+"\\");
		if(file.exists()){
			File[] files=file.listFiles();
			if(files.length<=0){
				addOrUpdateHotel(hotelCode);
			}else {
				System.out.println("图片已存在，不用下载");
			}
		}else{
			addOrUpdateHotel(hotelCode);
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void addOrUpdateHotel(String hotelCode) {
		String urltemp = "http://114-svc.elong.com/xml/v1.2/perhotelcn/" + hotelCode + ".xml";
		System.out.println("urltemp==" + urltemp);
		try {
			URL url = new URL(urltemp);
			URLConnection con = url.openConnection();
			// 设置连接主机超时(单位:毫秒)
			con.setConnectTimeout(120000);
			// 设置从主机读取数据超时(单位:毫秒)
			con.setReadTimeout(120000);
			// 是否想url输出
			con.setDoOutput(true);
			// 设定传送的内容类型是可序列化的对象
			con.setRequestProperty("Content-type", "application/x-java-serialized-object");
			String sCurrentLine;
			StringBuilder sTotalString = new StringBuilder();
			sCurrentLine = "";
			InputStream in = con.getInputStream();
			BufferedReader bf = new BufferedReader(new InputStreamReader(in, "utf-8"));
			while ((sCurrentLine = bf.readLine()) != null) {
				sTotalString.append(sCurrentLine);
			}
			Map map = new HashMap();
			map.put("q1:", "http://api.elong.com/staticInfo");
			org.dom4j.Document document = DocumentHelper.parseText(sTotalString.toString());
			XPath path = document.createXPath("HotelDetail");
			path.setNamespaceURIs(map);
			List nodelist = path.selectNodes(document);
			parseHotelXML(nodelist, hotelCode);
			bf.close();
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@SuppressWarnings( { "unchecked", "unchecked" })
	public static void parseHotelXML(List list, String hotelCode) throws Exception {
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Element element = (Element) it.next();
			// 酒店图片信息
			if (!element.elementText("images").equals("")) {
				List image = element.elements("images");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					List image2 = ele.elements("image");
					for (Iterator Image2 = image2.iterator(); Image2.hasNext();) {
						Hotelimage hotelImage = new Hotelimage();
						Element ele2 = (Element) Image2.next();
						// 图片URL地址
						if (!ele2.elementText("imgUrl").equals("")) {
							hotelImage.setPath(ele2.elementText("imgUrl"));
						}
						// 图片类型 各值表示如下：0-展示图；1-餐厅；2-休闲室；3-会议室
						// ；4-服务；5-酒店外观
						// ；6-大堂/接待台；7-酒店介绍；8-房型；9-背景图；10-其他
						// 图片标题
						hotelImage.setLanguage(0);
						File file = new File("D:\\hotelimage\\"+hotelCode+"\\");
						if (!file.exists()) {
							file.mkdirs();
						}
						String filename=hotelImage.getPath().substring(hotelImage.getPath().lastIndexOf("/"),hotelImage.getPath().lastIndexOf(".")) + ".jpg";
						String pathname="D:\\hotelimage\\"+hotelCode+"\\"+filename;
						download(hotelImage.getPath(),pathname);
					}
				}
			}
		}
	}
	
	/**
	 * 下載圖片
	 * 
	 * @param urlString
	 * @param filename
	 * @throws Exception
	 */
	public static void download(String urlString, String filename)
			throws Exception {
		System.out.println("开始下载图片---"+filename);
		URL url = new URL(urlString);
		URLConnection con = url.openConnection();
		InputStream is = con.getInputStream();

		byte[] bs = new byte[1024];
		int len;
		OutputStream os = new FileOutputStream(filename);
		while ((len = is.read(bs)) != -1) {
			os.write(bs, 0, len);
		}
		os.close();
		is.close();
	}
	@Override
	public void onStart(ElementPath arg0) {
		
	}

}
