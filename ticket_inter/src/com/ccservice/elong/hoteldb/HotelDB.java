package com.ccservice.elong.hoteldb;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.dom4j.Element;
import org.dom4j.ElementHandler;
import org.dom4j.io.SAXReader;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.b2b2c.base.chaininfo.Chaininfo;
import com.ccservice.b2b2c.base.city.City;
import com.ccservice.b2b2c.base.country.Country;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.b2b2c.base.hotelimage.Hotelimage;
import com.ccservice.b2b2c.base.hotellandmark.Hotellandmark;
import com.ccservice.b2b2c.base.landmark.Landmark;
import com.ccservice.b2b2c.base.nearlandmark.Nearlandmark;
import com.ccservice.b2b2c.base.province.Province;
import com.ccservice.b2b2c.base.region.Region;
import com.ccservice.b2b2c.base.roomtype.Roomtype;
import com.ccservice.elong.inter.DateSwitch;
import com.ccservice.elong.inter.DownFile;
import com.ccservice.inter.server.Server;

/**
 * 酒店详细信息 T_HOTEL/T_HOTELIMAGE/T_NEARLANDMARK/T_ROOMTYPE/T_HOTELLANDMARK
 * 
 * @author 师卫林
 * 
 */
public class HotelDB implements Job {
	public static String findCity = "SELECT ID,C_ELONGCITYID FROM T_CITY WHERE C_TYPE=1 AND C_ELONGCITYID IS NOT NULL ORDER BY C_ELONGCITYID DESC";
	public static String findProvince = "SELECT ID,C_CODE FROM T_PROVINCE";
	public static String findRegion = "SELECT ID,C_REGIONID,C_CITYID FROM T_REGION WHERE C_REGIONID IS NOT NULL";
	//public static String findChainInfo = "SELECT ID,C_BRANDID FROM T_CHAININFO";
	public static String findLandmark = "SELECT ID,C_ELONGMARKID,C_CITYID FROM T_LANDMARK";

	public static void main(String[] args) throws Exception {
		long start = System.currentTimeMillis();
		System.out.println("开始执行时间:" + new Date());
		// DownFile.download("http://114-svc.elong.com/xml/v1.2/hotellist.xml","D:\\酒店数据\\hotellist.xml");
		readXML("D:\\酒店数据\\hotellist.xml");
		long end = System.currentTimeMillis();
		System.out.println("执行结束时间:" + new Date());
		System.out.println("耗时:" + DateSwitch.showTime(end - start));
	}

	@SuppressWarnings("unchecked")
	public static void readXML(String filename) throws Exception {
		FileInputStream fis = new FileInputStream(filename);
		// 解析器
		SAXReader reader = new SAXReader();
		// 建立MyElementHandler的实例
		ElementHandler addHandler = new HotelDBElementHandler();
		reader.addHandler("/ArrayOfHotelInfoForIndex/HotelInfoForIndex",
				addHandler);
		reader.read(fis);
	}

	@SuppressWarnings( { "unchecked", "unchecked" })
	public static void parseHotelXML(List list, long hotelId) throws Exception {
		List<City> cityList = Server.getInstance().getSystemService().findMapResultBySql(findCity, null);
		List<Province> provinceList = Server.getInstance().getSystemService().findMapResultBySql(
				findProvince, null);
		List<Region> regionList = Server.getInstance().getSystemService().findMapResultBySql(findRegion,
				null);
		List<Landmark> landmarkList = Server.getInstance().getSystemService().findMapResultBySql(
				findLandmark, null);
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Hotel hotel = new Hotel();
			hotel = Server.getInstance().getHotelService().findHotel(hotelId);
			System.out.println("-------------" + hotel.getId() + "酒店名字:"
					+ hotel.getName());
			List<Nearlandmark> nearlandmarkList = 
					Server.getInstance().getHotelService().findAllNearlandmark(" WHERE "+ Nearlandmark.COL_hotelid+ "="+ hotel.getId(), "", -1, 0);
			Element element = (Element) it.next();
			// 酒店id
			if (!element.elementText("id").equals("")) {
				System.out.println("酒店ID:" + element.elementText("id"));
				hotel.setHotelcode(element.elementText("id"));
			}
			// // 酒店名称
			// if (!element.elementText("name").equals("")) {
			// hotel.setName(element.elementText("name"));
			// }
			// 酒店地址
			if (!element.elementText("address").equals("")) {
				hotel.setAddress(element.elementText("address"));
			}
			// 来源类型 艺龙1
			hotel.setSourcetype(1l);
			// 酒店所在地邮编
			if (!element.elementText("zip").equals("")) {
				hotel.setPostcode(element.elementText("zip"));
			}
			// 酒店房间总数
			if (!element.elementText("roomNumber").equals("")) {
				hotel.setRooms(Integer.valueOf(element
						.elementText("roomNumber")));
			}
			// 酒店特殊信息提示
			if (!element.elementText("availPolicy").equals("")) {
				hotel.setAvailPolicy(element.elementText("availPolicy"));
			}
			// 酒店所在位置的纬度
			if (element.elementText("lat")!=null&&!element.elementText("lat").equals("")) {
				hotel.setLat(Double.valueOf(element.elementText("lat")));
			}
			// 酒店所在位置的经度
			if (element.elementText("lon")!=null&&!element.elementText("lon").equals("")) {
				hotel.setLng(Double.valueOf(element.elementText("lon")));
			}
			// 酒店所在国家
			if (!element.elementText("country").equals("")) {
				String countryname = element.elementText("country");
				if("中国".equals(countryname)){
					hotel.setCountryid(168l);
				}else{
					List<Country> countryfromtable = Server.getInstance().getSystemService()
					.findMapResultBySql(" SELECT ID FROM T_COUNTRY WHERE C_ZHNAME='"+ countryname + "'", null);
					if (countryfromtable.size() > 0) {
						Map map = (Map) countryfromtable.get(0);
						hotel.setCountryid(Long.parseLong(map.get("ID").toString()));
					}
				}
			}
			// 酒店所在省份
			if (!element.elementText("province").equals("")) {
				System.out.println("省份:" + element.elementText("province"));
				String provinceid = element.elementText("province");
				for (int i = 0; i < provinceList.size(); i++) {
					Map map = (Map) provinceList.get(i);
					if (provinceid.equals(map.get("C_CODE"))) {
						hotel.setProvinceid(Long.valueOf(map.get("ID")
								.toString()));
						System.out.println("省份id:入库~~");
						break;
					}
				}
			}
			// 酒店所在城市
			System.out.println("城市:" + element.elementText("city"));
			long cityid = 0l;
			if (!element.elementText("city").equals("")) {
				cityid = Long.parseLong(element.elementText("city"));
				for (int i = 0; i < cityList.size(); i++) {
					Map map = (Map) cityList.get(i);
					if (cityid == Long.parseLong(map.get("C_ELONGCITYID")
							.toString())) {
						System.out.println("CITY表中的ID:"
								+ Long.valueOf(map.get("ID").toString()));
						hotel.setCityid(Long
								.parseLong(map.get("ID").toString()));
						System.out.println("城市id：" + hotel.getCityid());
						break;
					}
				}
				// 酒店所在行政区 region1
				if (!element.elementText("district").equals("")) {
					long regionid1 = Long.parseLong(element
							.elementText("district"));
					for (int i = 0; i < regionList.size(); i++) {
						Map map = (Map) regionList.get(i);
						if (regionid1 == Long.parseLong(map.get("C_REGIONID")
								.toString())
								&& (hotel.getCityid() == Long.parseLong(map
										.get("C_CITYID").toString()))) {
							hotel.setRegionid1(Long.parseLong(map.get("ID")
									.toString()));
							break;
						}
					}
				}
				// 酒店所在商业区 region2
				if (!element.elementText("businessZone").equals("")) {
					long regionid2 = Long.parseLong(element
							.elementText("businessZone"));
					for (int i = 0; i < regionList.size(); i++) {
						Map map = (Map) regionList.get(i);
						if (regionid2 == Long.parseLong(map.get("C_REGIONID")
								.toString())
								&& (hotel.getCityid() == Long.parseLong(map
										.get("C_CITYID").toString()))) {
							hotel.setRegionid2(Long.parseLong(map.get("ID")
									.toString()));
							break;
						}
					}
				}

			}
			// 酒店介绍信息
			if (!element.elementText("introEditor").equals("")) {
				hotel.setDescription(element.elementText("introEditor").trim());
			}
			// 酒店描述
			if (!element.elementText("description").equals("")) {
				System.out.println(element.elementText("description"));
			}
			// 可支持的信用卡
			if (!element.elementText("ccAccepted").equals("")) {
				hotel.setCarttype(element.elementText("ccAccepted"));
			}
			// 酒店免费接机信息
			if (!element.elementText("airportPickUpService").equals("")) {
				List image = element.elements("airportPickUpService");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					hotel.setAirportservice("免费接机开始日期:"
							+ DateSwitch.SwitchString(DateSwitch
									.SwitchCalendar(ele
											.elementText("StartDate")))
							+ "<br/>"
							+ "免费接机结束日期:"
							+ DateSwitch
									.SwitchString(DateSwitch.SwitchCalendar(ele
											.elementText("EndDate")))
							+ "<br/>"
							+ "接机每天开始时间:"
							+ DateSwitch.SwitchString(DateSwitch
									.SwitchCalendar(ele
											.elementText("StartTime")))
							+ "<br/>"
							+ "接机每天结束时间:"
							+ DateSwitch
									.SwitchString(DateSwitch.SwitchCalendar(ele
											.elementText("EndTime"))));
				}
			}
			// 酒店服务设施信息
			if (!element.elementText("generalAmenities").equals("")) {
				List image = element.elements("generalAmenities");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					// 服务设施概述
					if (!ele.elementText("Overview").equals("")) {
						hotel.setServiceitem(ele.elementText("Overview"));
						System.out.println("房间服务设施概述："
								+ ele.elementText("Overview").trim());
					}
					// // 服务设施列表
					// if (!ele.elementText("AmenitySimpleList").equals("")) {
					// System.out.println("酒店服务设施列表~~~~~~~~~~~~~~");
					// List l = ele.elements("AmenitySimpleList");
					// System.out.println("
					// "+ele.elementText("AmenitySimpleList"));
					// String serviceItem = "";
					// for (int i = 0; i < l.size(); i++) {
					// System.out.println("酒店服务设施列表项:" + l.size());
					// Element ele2 = (Element) l.get(i);
					// System.out.println(ele2.elementText("string"));
					// System.out.println(ele2.elementText("string"));
					// System.out.println(ele2.elementText("string"));
					// System.out.println(ele2.elementText("string"));
					// System.out.println(ele2.elementText("string"));
					//							
					// List list2=ele2.elements("string");
					// for(Iterator itr=list2.iterator();itr.hasNext();){
					// if (!ele2.elementText("string").equals("")) {
					// serviceItem = ele2.elementText("string") + "、";
					// }
					// }
					// hotel.setServiceitem(serviceItem);
					// }
					// }
				}
			}
			// 房间服务设施信息
			if (!element.elementText("roomAmenities").equals("")) {
				List image = element.elements("roomAmenities");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					// 服务设施概述
					if (!ele.elementText("Overview").equals("")) {
						hotel.setRoomAmenities(ele.elementText("Overview")
								.trim());
					}
					// // 服务设施列表
					// if (!ele.elementText("AmenitySimpleList").equals("")) {
					// System.out.println("房间服务设施列表~~~~~~~~~~~~~~~");
					// List l = ele.elements("AmenitySimpleList");
					// String fuwu = "";
					// for (int i = 0; i < l.size(); i++) {
					// System.out.println("房间服务设施列表项:" + l.size());
					// Element ele2 = (Element) l.get(i);
					// List list2=ele2.elements("string");
					// for(Iterator itr=list2.iterator();itr.hasNext();){
					// if (!ele2.elementText("string").equals("")) {
					// fuwu = ele2.elementText("string") + "、";
					// }
					// }
					// hotel.setRoomAmenities(fuwu.trim());
					// }
					// }
				}
			}
			// 休闲服务设施信息
			if (!element.elementText("recreationAmenities").equals("")) {
				List image = element.elements("recreationAmenities");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					// 休闲服务设施概述
					if (!ele.elementText("Overview").equals("")) {
						hotel.setPlayitem(ele.elementText("Overview").trim());
					}
					// // 服务设施列表
					// if (!ele.elementText("AmenitySimpleList").equals("")) {
					// System.out.println("休闲服务设施列表~~~~~~~~~~~");
					// List l = ele.elements("AmenitySimpleList");
					// String fuwu = "";
					// for (int i = 0; i < l.size(); i++) {
					// System.out.println("休闲服务设施列表项:" + l.size());
					// Element ele2 = (Element) l.get(i);
					// List list2=ele2.elements("string");
					// for(Iterator itr=list2.iterator();itr.hasNext();){
					// if (!ele2.elementText("string").equals("")) {
					// fuwu = ele2.elementText("string") + "、";
					// }
					// }
					// hotel.setPlayitem(fuwu.trim());
					// }
					// }
				}
			}
			// 会议服务设施信息
			if (!element.elementText("conferenceAmenities").equals("")) {
				List image = element.elements("conferenceAmenities");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					// 酒店会议室设施概述
					if (!ele.elementText("Overview").equals("")) {
						hotel
								.setMeetingitem(ele.elementText("Overview")
										.trim());
					}
					// // 会议室列表
					// if (!ele.elementText("AmenityList").equals("")) {
					//
					// }
				}
			}
			// 餐饮服务设施信息
			if (!element.elementText("diningAmenities").equals("")) {
				List image = element.elements("diningAmenities");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					// 酒店餐厅设施概述
					if (!ele.elementText("Overview").equals("")) {
						hotel.setFootitem(ele.elementText("Overview").trim());
					}
				}
			}
			// 酒店特色信息
			if (!element.elementText("featureInfo").equals("")) {
				List image = element.elements("featureInfo");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					// 酒店特色信息概述
					String sellPonint = "";
					if (!ele.elementText("DrivingGuide").equals("")) {
						sellPonint += ele.elementText("DrivingGuide").trim()
								+ "<br/>";
					}
					if (!ele.elementText("PropertyOtherHightlights").equals("")) {
						sellPonint += ele.elementText(
								"PropertyOtherHightlights").trim()
								+ "<br/>";
					}
					if (!ele.elementText("PropertyAmenitiesHightlights")
							.equals("")) {
						sellPonint += ele.elementText(
								"PropertyAmenitiesHightlights").trim()
								+ "<br/>";
					}
					if (!ele.elementText("LocationHighlights").equals("")) {
						sellPonint += ele.elementText("LocationHighlights")
								.trim()
								+ "<br/>";
					}
					if (!ele.elementText("Overview").equals("")) {
						sellPonint += ele.elementText("Overview").trim();
					}
					hotel.setSellpoint(sellPonint);
				}
			}
			// 酒店电话(前台)
			if (!element.elementText("Phone").equals("")) {
				hotel.setMarkettell(element.elementText("Phone"));
			}
			// 酒店传真(前台)
			if (!element.elementText("Fax").equals("")) {
				hotel.setFax1(element.elementText("Fax"));
			}
			// 酒店开业日期
			if (!element.elementText("OpeningDate").equals("")) {
				String OpeningDate = element.elementText("OpeningDate");
				java.sql.Date date = DateSwitch.SwitchSqlDate(DateSwitch
						.SwitchCalendar(OpeningDate));
				hotel.setOpendate(date);
			}
			// 酒店装修日期
			if (!element.elementText("RenovationDate").equals("")) {
				hotel.setRepaildate(element.elementText("RenovationDate"));
			}
			// 酒店挂牌星级 0-无星级；1-一星级；2-二星级；3-三星级；4-四星级；5-五星级
			if (!element.elementText("star").equals("")) {
				hotel.setStar(Integer.valueOf(element.elementText("star")));
			}
			// 酒店所属连锁品牌ID
			if (!element.elementText("brandID").equals("")) {
				String brandId = element.elementText("brandID");
				if(brandId!=null&&!"".equals(brandId)){
					List<Chaininfo> chList=Server.getInstance().getHotelService().findAllChaininfo("where C_BRANDID="+brandId, "", -1, 0);
					if(chList!=null && chList.size()>0){
						hotel.setChaininfoid(chList.get(0).getId());
					}
				}
			}
			hotel.setType(1);
			hotel.setLanguage(0);
			// 周边交通信息 1
			if (!element.elementText("trafficAndAroundInformations").equals("")) {
				System.out.println("周边交通信息~~~~~~~~~~~~~~~");
				List traffics = element
						.elements("trafficAndAroundInformations");
				for (int i = 0; i < traffics.size(); i++) {
					Element ele = (Element) traffics.get(i);
					// // 暂时不用
					// if (ele.elementText("ExtensionData") !=null) {
					// System.out.println("ExtensionData:" +
					// ele.elementText("ExtensionData"));
					// }
					// 周边交通信息概述
					if (!ele.elementText("Overview").equals("")) {
						hotel.setTrafficinfo(ele.elementText("Overview"));
					}
					// 周边交通信息列表
					if (!ele.elementText("TrafficAndAroundInformationList")
							.equals("")) {
						List trafficList = ele
								.elements("TrafficAndAroundInformationList");
						for (int j = 0; j < trafficList.size(); j++) {
							Element ele2 = (Element) trafficList.get(j);
							if (!ele2.elementText(
									"HotelTrafficAndAroundInformation").equals(
									"")) {
								List trafficList2 = ele2
										.elements("HotelTrafficAndAroundInformation");
								for (int m = 0; m < trafficList2.size(); m++) {
									Nearlandmark nearlandmark = new Nearlandmark();
									// 周边交通名称
									Element ele3 = (Element) trafficList2.get(m);
									if (!ele3.elementText("Name").equals("")) {
										System.out.println("-------------"+ele3.elementText("Name"));
										if (ele3.elementText("Name").contains(
												"'")) {
											nearlandmark.setLandmarkname(ele3
													.elementText("Name")
													.replace("'", "‘"));
										}else{
											nearlandmark.setLandmarkname(ele3
													.elementText("Name"));
										}
										
									if (!ele3.elementText("Distances").equals("")) {
										nearlandmark.setLandmarkdistance(ele3
												.elementText("Distances"));
									}
									
									// 备注信息
									if (!ele3.elementText("Note").equals("")) {
										nearlandmark.setLandmarkdesc(ele3
												.elementText("Note"));
									}
									// 所用时间
									if (!ele3.elementText("TimeTaken").equals(
											"")) {
										nearlandmark.setWastetime(ele3
												.elementText("TimeTaken"));
									}
									// 交通花费费用
									if (!ele3.elementText("TransportFee")
											.equals("")) {
										nearlandmark.setTraficfee(ele3
												.elementText("TransportFee"));
									}
									// 乘坐的交通工具
									if (!ele3.elementText("Transportations")
											.equals("")) {
										if (ele3.elementText("Transportations")
												.equals("1")) {
											nearlandmark.setTraficmethod("出租车");
										}
										if (ele3.elementText("Transportations")
												.equals("2")) {
											nearlandmark.setTraficmethod("公交车");
										}
										if (ele3.elementText("Transportations")
												.equals("3")) {
											nearlandmark.setTraficmethod("地铁");
										}
										if (ele3.elementText("Transportations")
												.equals("4")) {
											nearlandmark.setTraficmethod("步行");
										}
										if (ele3.elementText("Transportations")
												.equals("5")) {
											nearlandmark.setTraficmethod("自行车");
										}
										if (ele3.elementText("Transportations")
												.equals("6")) {
											nearlandmark.setTraficmethod("火车");
										}
									}
									nearlandmark.setHotelid(hotel.getId());
									nearlandmark.setLandmarktype(1);
									if(nearlandmark.getLandmarkname()==null || "".equals(nearlandmark.getLandmarkname().trim())){
										nearlandmark.setLandmarkname("");
									}
									Nearlandmark localNearlandmark = null;
									if(nearlandmarkList!=null && nearlandmarkList.size()>0){
										for(Nearlandmark temp:nearlandmarkList){
											if(temp.getLandmarkname()==null || "".equals(temp.getLandmarkname().trim())){
												temp.setLandmarkname("");
											}
											if(nearlandmark.getLandmarkname().equals(temp.getLandmarkname())){
												localNearlandmark = temp;
												break;
											}
										}
									}
									if (localNearlandmark!=null) {
										nearlandmark.setId(localNearlandmark.getId());
										Server.getInstance().getHotelService().updateNearlandmarkIgnoreNull(nearlandmark);
									} else {
										nearlandmark = Server.getInstance().getHotelService().createNearlandmark(nearlandmark);
									}
									}
								}
							}
						}
					}
				}
			}
			// 周边餐饮信息 2
			if (!element.elementText("surroundingRestaurants").equals("")) {
				System.out.println("周边餐饮信息~~~~~~~~~~~~~~~");
				List restaurants = element.elements("surroundingRestaurants");
				for (int i = 0; i < restaurants.size(); i++) {
					Element ele = (Element) restaurants.get(i);
					if (!ele.elementText("surroundingRestaurant").equals("")) {
						List restaurantList = ele
								.elements("surroundingRestaurant");
						for (int j = 0; j < restaurantList.size(); j++) {
							Nearlandmark nearlandmark = new Nearlandmark();
							Element ele2 = (Element) restaurantList.get(j);
							// 餐厅描述
							if (!ele2.elementText("Description").equals("")) {
								nearlandmark.setLandmarkdesc(ele2
										.elementText("Description"));
							}
							// 到酒店距离
							if (!ele2.elementText("Distances").equals("")) {
								nearlandmark.setLandmarkdistance(ele2
										.elementText("Distances"));
							}
							// 周边餐厅名字
							if (!ele2.elementText("Name").equals("")) {
								if (ele2.elementText("Name").contains("'")) {
									nearlandmark.setLandmarkname(ele2
											.elementText("Name").replace("'",
													"‘"));
								}else{
									nearlandmark.setLandmarkname(ele2
											.elementText("Name"));
								}
							}
							nearlandmark.setHotelid(hotel.getId());
							nearlandmark.setLandmarktype(2);
							if(nearlandmark.getLandmarkname()==null || "".equals(nearlandmark.getLandmarkname().trim())){
								nearlandmark.setLandmarkname("");
							}
							Nearlandmark localNearlandmark = null;
							if(nearlandmarkList!=null && nearlandmarkList.size()>0){
								for(Nearlandmark temp:nearlandmarkList){
									if(temp.getLandmarkname()==null || "".equals(temp.getLandmarkname().trim())){
										temp.setLandmarkname("");
									}
									if(nearlandmark.getLandmarkname().equals(temp.getLandmarkname())){
										localNearlandmark = temp;
										break;
									}
								}
							}
							if (localNearlandmark!=null) {
								nearlandmark.setId(localNearlandmark.getId());
								Server.getInstance().getHotelService().updateNearlandmarkIgnoreNull(nearlandmark);
							} else {
								nearlandmark = Server.getInstance().getHotelService().createNearlandmark(nearlandmark);
							}
						}
					}
				}
			}
			// 周边景致信息 3
			if (!element.elementText("surroundingAttractions").equals("")) {
				System.out.println("周边景致信息~~~~~~~~~~~~~~~");
				List attractions = element.elements("surroundingAttractions");
				for (int i = 0; i < attractions.size(); i++) {
					Element ele = (Element) attractions.get(i);
					if (!ele.elementText("surroundingAttraction").equals("")) {
						List attractionList = ele
								.elements("surroundingAttraction");
						for (int j = 0; j < attractionList.size(); j++) {
							Nearlandmark nearlandmark = new Nearlandmark();
							Element ele2 = (Element) attractionList.get(j);
							// 到酒店距离
							if (!ele2.elementText("Distances").equals("")) {
								nearlandmark.setLandmarkdistance(ele2
										.elementText("Distances"));
							}
							// 周边餐厅名字
							if (!ele2.elementText("Name").equals("")) {
								if (ele2.elementText("Name").contains("'")) {
									nearlandmark.setLandmarkname(ele2
											.elementText("Name").replace("'",
													"‘"));
								}else{
									nearlandmark.setLandmarkname(ele2
											.elementText("Name"));
								}
							}
							nearlandmark.setHotelid(hotel.getId());
							nearlandmark.setLandmarktype(3);
							if(nearlandmark.getLandmarkname()==null || "".equals(nearlandmark.getLandmarkname().trim())){
								nearlandmark.setLandmarkname("");
							}
							Nearlandmark localNearlandmark = null;
							if(nearlandmarkList!=null && nearlandmarkList.size()>0){
								for(Nearlandmark temp:nearlandmarkList){
									if(temp.getLandmarkname()==null || "".equals(temp.getLandmarkname().trim())){
										temp.setLandmarkname("");
									}
									if(nearlandmark.getLandmarkname().equals(temp.getLandmarkname())){
										localNearlandmark = temp;
										break;
									}
								}
							}
							if (localNearlandmark!=null) {
								nearlandmark.setId(localNearlandmark.getId());
								Server.getInstance().getHotelService().updateNearlandmarkIgnoreNull(nearlandmark);
							} else {
								nearlandmark = Server.getInstance().getHotelService().createNearlandmark(nearlandmark);
							}
						}
					}
				}
			}
			// 周边购物信息 4
			if (!element.elementText("surroundingShops").equals("")) {
				System.out.println("周边购物信息~~~~~~~~~~~~~~~");
				List shops = element.elements("surroundingShops");
				for (int i = 0; i < shops.size(); i++) {
					Element ele = (Element) shops.get(i);
					if (!ele.elementText("surroundingShop").equals("")) {
						List shopList = ele.elements("surroundingShop");
						for (int j = 0; j < shopList.size(); j++) {
							Nearlandmark nearlandmark = new Nearlandmark();
							Element ele2 = (Element) shopList.get(j);
							// 商业区描述
							if (!ele2.elementText("Description").equals("")) {
								nearlandmark.setLandmarkdesc(ele2
										.elementText("Description"));
							}
							// 到酒店距离
							if (!ele2.elementText("Distances").equals("")) {
								nearlandmark.setLandmarkdistance(ele2
										.elementText("Distances"));
							}
							// 商业区名称
							if (!ele2.elementText("Name").equals("")) {
								System.out.println("商业区名称："
										+ ele2.elementText("Name"));
								if (ele2.elementText("Name").contains("'")) {
									nearlandmark.setLandmarkname(ele2
											.elementText("Name").replace("'",
													"‘"));
								}else{
									nearlandmark.setLandmarkname(ele2
											.elementText("Name"));
								}
							}
							nearlandmark.setHotelid(hotel.getId());
							nearlandmark.setLandmarktype(4);
							if(nearlandmark.getLandmarkname()==null || "".equals(nearlandmark.getLandmarkname().trim())){
								nearlandmark.setLandmarkname("");
							}
							Nearlandmark localNearlandmark = null;
							if(nearlandmarkList!=null && nearlandmarkList.size()>0){
								for(Nearlandmark temp:nearlandmarkList){
									if(temp.getLandmarkname()==null || "".equals(temp.getLandmarkname().trim())){
										temp.setLandmarkname("");
									}
									if(nearlandmark.getLandmarkname().equals(temp.getLandmarkname())){
										localNearlandmark = temp;
										break;
									}
								}
							}
							if (localNearlandmark!=null) {
								nearlandmark.setId(localNearlandmark.getId());
								Server.getInstance().getHotelService().updateNearlandmarkIgnoreNull(nearlandmark);
							} else {
								nearlandmark = Server.getInstance().getHotelService().createNearlandmark(nearlandmark);
							}
						}
					}
				}
			}
			// 周边商务信息 5
			if (!element.elementText("surroundingCommerces").equals("")) {
				System.out.println("周边商务信息~~~~~~~~~~~~~~~");
				List commerces = element.elements("surroundingCommerces");
				// System.out.println(element.elements("surroundingCommerces"));
				for (int i = 0; i < commerces.size(); i++) {
					Element ele = (Element) commerces.get(i);
					if (!ele.elementText("surroundingCommerce").equals("")) {
						List commerceList = ele.elements("surroundingCommerce");
						for (int j = 0; j < commerceList.size(); j++) {
							Element ele2 = (Element) commerceList.get(j);
							Nearlandmark nearlandmark = new Nearlandmark();
							// 到酒店距离
							if (!ele2.elementText("Distances").equals("")) {
								System.out.println("到酒店距离："
										+ ele2.elementText("Distances"));
								nearlandmark.setLandmarkdistance(ele2
										.elementText("Distances"));
							}
							// 周边商业区名称
							if (!ele2.elementText("Name").equals("")) {
								System.out.println("周边商业区名称："
										+ ele2.elementText("Name"));
								if (ele2.elementText("Name").contains("'")) {
									nearlandmark.setLandmarkname(ele2
											.elementText("Name").replace("'",
													"‘"));
								}else{
									nearlandmark.setLandmarkname(ele2
											.elementText("Name"));
								}
							}
							// 备注信息
							if (!ele2.elementText("Note").equals("")) {
								System.out.println("备注信息："
										+ ele2.elementText("Note"));
								nearlandmark.setLandmarkdesc(ele
										.elementText("Note"));
							}
							// 所花时间
							if (!ele2.elementText("TimeTaken").equals("")) {
								System.out.println("所花时间："
										+ ele2.elementText("TimeTaken"));
								nearlandmark.setWastetime(ele
										.elementText("TimeTaken"));
							}
							// 交通话费费用
							if (!ele2.elementText("TransportFee").equals("")) {
								nearlandmark.setTraficfee(ele2
										.elementText("TransportFee"));
							}
							// 乘坐的交通工具
							if (!ele2.elementText("Transportations").equals("")) {
								System.out.println("交通工具:"
										+ ele2.elementText("Transportations"));
								if (ele2.elementText("Transportations").equals(
										"1")) {
									nearlandmark.setTraficmethod("出租车");
								}
								if (ele2.elementText("Transportations").equals(
										"2")) {
									nearlandmark.setTraficmethod("公交车");
								}
								if (ele2.elementText("Transportations").equals(
										"3")) {
									nearlandmark.setTraficmethod("地铁");
								}
								if (ele2.elementText("Transportations").equals(
										"4")) {
									nearlandmark.setTraficmethod("步行");
								}
								if (ele2.elementText("Transportations").equals(
										"5")) {
									nearlandmark.setTraficmethod("自行车");
								}
								if (ele2.elementText("Transportations").equals(
										"6")) {
									nearlandmark.setTraficmethod("火车");
								}
							}
							nearlandmark.setHotelid(hotel.getId());
							nearlandmark.setLandmarktype(5);
							if(nearlandmark.getLandmarkname()==null || "".equals(nearlandmark.getLandmarkname().trim())){
								nearlandmark.setLandmarkname("");
							}
							Nearlandmark localNearlandmark = null;
							if(nearlandmarkList!=null && nearlandmarkList.size()>0){
								for(Nearlandmark temp:nearlandmarkList){
									if(temp.getLandmarkname()==null || "".equals(temp.getLandmarkname().trim())){
										temp.setLandmarkname("");
									}
									if(nearlandmark.getLandmarkname().equals(temp.getLandmarkname())){
										localNearlandmark = temp;
										break;
									}
								}
							}
							if (localNearlandmark!=null) {
								nearlandmark.setId(localNearlandmark.getId());
								Server.getInstance().getHotelService().updateNearlandmarkIgnoreNull(nearlandmark);
							} else {
								nearlandmark = Server.getInstance().getHotelService().createNearlandmark(nearlandmark);
							}
						}
					}
				}
			}
			// 预付-2 现付-1
			hotel.setPaytype(1l);
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			hotel.setLastupdatetime(sdf.format(new Date(System.currentTimeMillis())));
			Server.getInstance().getHotelService().updateHotelIgnoreNull(hotel);
			// 酒店附近标志物
			if (!element.elementText("Landmarks").equals("")) {
				List landmarks = element.elements("Landmarks");
				for (int i = 0; i < landmarks.size(); i++) {
					Element ele = (Element) landmarks.get(i);
					if (!ele.elementText("HotelLandMark").equals("")) {
						List list2 = ele.elements("HotelLandMark");
						for (int j = 0; j < list2.size(); j++) {
							Hotellandmark hotellandmark = new Hotellandmark();
							Element element2 = (Element) list2.get(j);
							hotellandmark.setHotelid(hotel.getId());
							// 酒店附近标志物ID
							if (!element2.elementText("LandMarkID").equals("")) {
								String landmarkID = element2
										.elementText("LandMarkID");
								for (int n = 0; n < landmarkList.size(); n++) {
									// System.out.println("landmarkList.size():"
									// + landmarkList.size());
									Map map = (Map) landmarkList.get(n);
									// System.out.println("hotel.getCityid():" +
									// hotel.getCityid());
									// System.out.println("landmark表中的城市id;"+map.get("C_CITYID").toString());
									if (landmarkID.equals(map.get(
											"C_ELONGMARKID").toString())
											&& (hotel.getCityid() == Long
													.parseLong(map.get(
															"C_CITYID")
															.toString()))) {
										hotellandmark.setLandmarkid(Long
												.parseLong(map.get("ID")
														.toString()));
									}
								}
							}
							hotellandmark.setLanguage(0);
							// System.out.println("酒店id~!~~~~~:" +
							// hotel.getId());
							List<Hotellandmark> hotelandmarkList = Server.getInstance().getHotelService()
									.findAllHotellandmark(" where "
											+ Hotellandmark.COL_landmarkid
											+ "="
											+ hotellandmark.getLandmarkid(),
											"", -1, 0);
							if (hotelandmarkList==null || hotelandmarkList.size() == 0) {
								hotellandmark = Server.getInstance().getHotelService().createHotellandmark(hotellandmark);
							}
						}
					}
				}
			}
			// 房间类型信息
			boolean haveRoom = false;
			if (element.elementText("roomInfo")!=null && !element.elementText("roomInfo").equals("")) {
				//查询该酒店下所有房型
				List<Roomtype> roomTypeList = 
					Server.getInstance().getHotelService().findAllRoomtype("where " + Roomtype.COL_hotelid + "='" + hotel.getId() + "'", "", -1, 0);
				//EL与本地匹配上房型
				List<Roomtype> matchList = new ArrayList<Roomtype>();
				List image = element.elements("roomInfo");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					if (ele.elementText("room")!=null && !ele.elementText("room").equals("")) {
						List list2 = ele.elements("room");
						if(list2==null || list2.size()==0){
							continue;
						}else{
							haveRoom = true;
						}
						for (Iterator iterator = list2.iterator(); iterator.hasNext();) {
							Roomtype roomtype = new Roomtype();
							Element element2 = (Element) iterator.next();
							// 房型ID
							if (element2.elementText("roomTypeId")!=null && !element2.elementText("roomTypeId").equals("")) {
								roomtype.setRoomcode(element2.elementText("roomTypeId"));
							}else{
								continue;
							}
							// 房型名称
							if (!element2.elementText("roomName").equals("")) {
								roomtype.setName(element2
										.elementText("roomName"));
							}
							// 房间面积
							if (!element2.elementText("area").equals("")) {
								roomtype.setAreadesc(element2
										.elementText("area"));
							}
							// 房间所在楼层
							if (!element2.elementText("floor").equals("")) {
								roomtype
										.setLayer(element2.elementText("floor"));
							}
							// 是否有带宽 0表示无宽带，1 表示免费 2表示收费
							if (!element2.elementText("hasBroadnet").equals("")) {
								roomtype.setWideband(Integer.valueOf(element2
										.elementText("hasBroadnet")));
							}
							// 宽待是否收费 0 表示免费 1 表示收费
							if (!element2.elementText("broadnetFee").equals("")) {
								if (element2.elementText("broadnetFee").equals(
										"1")) {
									roomtype.setWideband(2);
								}
							}
							// 备注
							if (!element2.elementText("note").equals("")) {
								roomtype.setNote(element2.elementText("note")
										.trim());
							}
							// 房间描述
							if (!element2.elementText("bedDescription").equals(
									"")) {
								roomtype.setRoomdesc(element2.elementText(
										"bedDescription").trim());
							}
							// 床型描述信息 1 单人床 2 大床 3 双床 4 大或双 5 其他
							if (!element2.elementText("bedType").equals("")) {
								if (element2.elementText("bedType").contains(
										"单人床")) {
									roomtype.setBed(1);
								} else if (element2.elementText("bedType")
										.contains("大床")) {
									roomtype.setBed(2);
								} else if (element2.elementText("bedType")
										.contains("双床")) {
									roomtype.setBed(3);
								} else if (element2.elementText("bedType")
										.contains("大或双")) {
									roomtype.setBed(4);
								} else {
									roomtype.setBed(5);
								}
							}
							roomtype.setLanguage(0);
							roomtype.setState(1);//可用
							roomtype.setHotelid(hotel.getId());
							Roomtype localRoom = null;
							for(Roomtype room:roomTypeList){
								if(roomtype.getRoomcode().equals(room.getRoomcode())){
									roomtype.setId(room.getId());
									matchList.add(room);
									localRoom = room;
									break;
								}
							}
							if(localRoom!=null){
								//防止空指针异常
								roomtype.setName(roomtype.getName()==null?"":roomtype.getName());
								roomtype.setAreadesc(roomtype.getAreadesc()==null?"":roomtype.getAreadesc());
								roomtype.setLayer(roomtype.getLayer()==null?"":roomtype.getLayer());
								roomtype.setWideband(roomtype.getWideband()==null?0:roomtype.getWideband());
								roomtype.setNote(roomtype.getNote()==null?"":roomtype.getNote());
								roomtype.setRoomdesc(roomtype.getRoomdesc()==null?"":roomtype.getRoomdesc());
								roomtype.setBed(roomtype.getBed()==null?5:roomtype.getBed());
								
								if(!roomtype.getName().equals(localRoom.getName()) ||
										!roomtype.getAreadesc().equals(localRoom.getAreadesc()) || 
											!roomtype.getLayer().equals(localRoom.getLayer()) ||
												!roomtype.getWideband().equals(localRoom.getWideband()) ||
													!roomtype.getNote().equals(localRoom.getNote()) ||
														!roomtype.getRoomdesc().equals(localRoom.getRoomdesc()) ||
															!roomtype.getBed().equals(localRoom.getBed())){
									System.out.println("更新一条房型……");
									Server.getInstance().getHotelService().updateRoomtypeIgnoreNull(roomtype);
								}
							}else{
								System.out.println("创建一条新房型……");
								roomtype = Server.getInstance().getHotelService().createRoomtype(roomtype);
							}
						}
					}
				}
				//未匹配上房型房型状态置为不可用
				roomTypeList.removeAll(matchList);
				if(roomTypeList!=null && roomTypeList.size()>0){
					for(Roomtype room:roomTypeList){
						//房型状态 列名 0不可用 1 可用
						room.setState(0);
						Server.getInstance().getHotelService().updateRoomtypeIgnoreNull(room);
						System.out.println("-----房型未匹配上-----" + room.getHotelid() + "-----" + room.getName());
					}
				}
				System.out.println("roomType........ok!!!");
			}
			if(!haveRoom){
				hotel.setState(2);
				hotel.setStatedesc("酒店可用,但是艺龙没有提供给我们房型!");
				Server.getInstance().getHotelService().updateHotelIgnoreNull(hotel);
			}
			// 酒店图片信息
			if (element.elementText("images")!=null && !element.elementText("images").equals("")) {
				List<Hotelimage> imageList = Server.getInstance().getHotelService()
					.findAllHotelimage(" WHERE " + Hotelimage.COL_hotelid + "="+ hotel.getId(),"", -1, 0);
				List image = element.elements("images");
				//判断是否存在固定长边350
				boolean have350 = false;
				label:for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					for (Iterator img = ele.elements("image").iterator(); img.hasNext();) {
						Element e = (Element) img.next();
						if("1".equals(e.elementText("sizeType"))){
							have350 = true;
							break label;
						}
					}
				}
				out:for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					List image2 = ele.elements("image");
					for (Iterator Image2 = image2.iterator(); Image2.hasNext();) {
						Hotelimage hotelImage = new Hotelimage();
						Element ele2 = (Element) Image2.next();
						String sizeType = "";
						/**
						 * sizeType  默认取类型为1的，没有取第一张图片
						 * 
						 * 1：jpg图片，固定长边350，固定长边350缩放(用于详情页图片展示)
						 * 2：jpg图片，尺寸70x70(用于详情页图片列表的缩微图)
						 * 3：jpg图片，尺寸120x120(用于列表页)
						 * 5：png图片，尺寸70x70
						 * 6：png图片，尺寸120x120
						 * 7：png图片，固定长边640放缩
						 */
						if(!"".equals(ele2.elementText("sizeType"))){
							sizeType = ele2.elementText("sizeType");
							hotelImage.setSizeType(Integer.valueOf(sizeType));
						}
						if(!"1".equals(sizeType) && have350){
							continue;
						}
						// 图片URL地址
						if (!ele2.elementText("imgUrl").equals("")) {
							hotelImage.setPath(ele2.elementText("imgUrl"));
						}
						// 图片类型 各值表示如下：0-展示图；1-餐厅；2-休闲室；3-会议室
						// ；4-服务；5-酒店外观
						// ；6-大堂/接待台；7-酒店介绍；8-房型；9-背景图；10-其他
						if (!ele2.elementText("imgType").equals("")) {
							hotelImage.setType(Integer.valueOf(ele2.elementText("imgType")));
						}
						// 图片标题
						if (!ele2.elementText("title").equals("")) {
							hotelImage.setDescription(ele2.elementText("title"));
						}
						hotelImage.setHotelid(hotel.getId());
						hotelImage.setLanguage(0);

						File file = new File("D:\\hotelimage\\"+hotel.getHotelcode());
						if (!file.exists()) {
							file.mkdirs();
						}
						try {
							//EL图片地址
							String elImgUrl = hotelImage.getPath();
							//库中存储图片名称
							String localImgName = elImgUrl.substring(elImgUrl.lastIndexOf("/")+1,elImgUrl.lastIndexOf(".")) + ".jpg";
							//本地存储路径
							String localPath = "D:\\hotelimage\\"+hotel.getHotelcode()+"\\"+localImgName;
							if(!new File(localPath).exists()){
								//下载EL图片
								download(elImgUrl, localPath);
							}
							//修改数据库记录
							hotelImage.setPath("/hotelimage/"+hotel.getHotelcode()+"/"+localImgName);
							Hotelimage localImage = null;
							for(Hotelimage img:imageList){
								if(hotelImage.getPath().equals(img.getPath())){
									hotelImage.setId(img.getId());
									localImage = img;
									break;
								}
							}
							if (localImage!=null) {
								hotelImage.setType(hotelImage.getType()==null?10:hotelImage.getType());
								hotelImage.setDescription(hotelImage.getDescription()==null?"":hotelImage.getDescription());
								
								if(!hotelImage.getType().equals(localImage.getType()) ||
										!hotelImage.getDescription().equals(localImage.getDescription()) || 
											!hotelImage.getSizeType().equals(localImage.getSizeType())){
									System.out.println("更改了一条图片记录!!!");
									Server.getInstance().getHotelService().updateHotelimageIgnoreNull(hotelImage);	
								}
							} else {
								System.out.println("增加了一条图片记录");
								hotelImage = Server.getInstance().getHotelService().createHotelimage(hotelImage);
							}
						} catch (Exception e) {
							
						}
						if(!have350){
							break out;
						}
					}
				}
				System.out.println("hotelImage......ok!!!!");
			}
		}
	}

	@Override
	public void execute(JobExecutionContext context)throws JobExecutionException {
		try {
			long start = System.currentTimeMillis();
			System.out.println("开始下载酒店数据");
			DownFile.download(
					"http://114-svc.elong.com/xml/v1.2/hotellist.xml",
					"D:\\酒店数据\\hotellist.xml");
			System.out.println("下载完成");
			readXML("D:\\酒店数据\\hotellist.xml");
			long end = System.currentTimeMillis();
			System.out.println("执行结束时间:" + new Date());
			System.out.println("耗时:" + DateSwitch.showTime(end - start));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 下載圖片
	 * 
	 * @param urlString
	 * @param filename
	 * @throws Exception
	 */
	public static void download(String urlString, String filename)throws Exception {
		System.out.println("开始下载图片---"+filename);
		URL url = new URL(urlString);
		URLConnection con = url.openConnection();
		InputStream is = con.getInputStream();

		byte[] bs = new byte[1024];
		int len;
		OutputStream os = new FileOutputStream(filename);
		while ((len = is.read(bs)) != -1) {
			os.write(bs, 0, len);
		}
		os.close();
		is.close();
	}
}
