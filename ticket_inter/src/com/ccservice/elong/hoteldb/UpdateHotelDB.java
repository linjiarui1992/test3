package com.ccservice.elong.hoteldb;

import java.io.File;
import java.io.FileInputStream;
import java.util.Date;

import org.dom4j.ElementHandler;
import org.dom4j.io.SAXReader;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.elong.inter.DateSwitch;
import com.ccservice.elong.inter.DownFile;

/**
 * 更新酒店数据
 * 
 * @author vinegar
 * 
 */
public class UpdateHotelDB implements Job {
	public static void main(String[] args) throws Exception{
		File file=new File("D:\\酒店数据");
		if(!file.exists()){
			file.mkdirs();
		}
		System.out.println("开始...");
		//DownFile.download("http://114-svc.elong.com/xml/v1.2/hotellist.xml",
		//"D:\\酒店数据\\hotellist.xml");
		readXML("D:\\酒店数据\\hotellist.xml");
		System.out.println("结束..");
	}
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		// TODO Auto-generated method stub
		try {
			DownFile.download("http://114-svc.elong.com/xml/v1.2/hotellist.xml",
			"D:\\酒店数据\\hotellist.xml");
			long start = System.currentTimeMillis();
			System.out.println("start time:" + new Date());
			readXML("D:\\酒店数据\\hotellist.xml");
			long end = System.currentTimeMillis();
			System.out.println("end time:" + new Date());
			System.out.println("现付酒店更新完所用时间:" + DateSwitch.showTime(end - start));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@SuppressWarnings("unchecked")
	public static void readXML(String filename) throws Exception {
		FileInputStream fis=new FileInputStream(filename);
		// 解析器
		SAXReader reader = new SAXReader();
		// 建立MyElementHandler的实例
		ElementHandler addHandler=new MyElementHandler();
		reader.addHandler("/ArrayOfHotelInfoForIndex/HotelInfoForIndex", addHandler);
		reader.read(fis);
	}
	
}
