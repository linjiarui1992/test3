package com.ccservice.elong.hotelorderupdate;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccervice.huamin.update.WriteLog;
import com.ccservice.b2b2c.base.customeragent.Customeragent;
import com.ccservice.b2b2c.base.dnsmaintenance.Dnsmaintenance;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.b2b2c.base.hotelorder.Hotelorder;
import com.ccservice.b2b2c.base.hotelorderrc.Hotelorderrc;
import com.ccservice.elong.hotelorderupdate.SMSTemplet.SMSType;
import com.ccservice.inter.server.Server;

public class HotelOrderUpdate implements Job {
	
	/**
	 * 
	 * @param codestr
	 * 根据艺龙订单号更新艺龙的订单状态
	 */
	public void updateHotelOrderStatus(List<Hotelorder> orderlist,String codestr){
		if(codestr.lastIndexOf(",")>0){
			codestr=codestr.substring(0, codestr.lastIndexOf(","));
			System.out.println("需要更新的订单号："+codestr);
		}
		Map<String, String> result = Server.getInstance().getIELongHotelService().GetHotelOrderListById(codestr);
		if(result.get("resultcode").equals("0")){
			for (Hotelorder hotelorder : orderlist) {
				String orderStateCode=result.get(hotelorder.getWaicode().trim());
				if ("V".equals(orderStateCode)) {
				} else if ("N".equals(orderStateCode)) {
				} else if ("R".equals(orderStateCode)) {
				} else if ("A".equals(orderStateCode)) {//a
					hotelorder.setState(3);
					Server.getInstance().getHotelService().updateHotelorderIgnoreNull(hotelorder);
					writehotelorderrc("系统自动确认订单", hotelorder);
					//发短信
					Hotel hotel=Server.getInstance().getHotelService().findHotel(hotelorder.getHotelid());
					Customeragent agent=Server.getInstance().getMemberService().findCustomeragent(hotelorder.getCreateuserid());
					if(agent!=null){
						String agentstr=agent.getId()+","+agent.getParentstr();
						List<Dnsmaintenance> dnses=Server.getInstance().getSystemService().findAllDnsmaintenance("where c_agentid in ("+agentstr+")", "order by c_agentid desc", -1, 0);
						if(dnses!=null&&dnses.size()>0){
							Dnsmaintenance dns=dnses.get(0);
							sendConfimSms(hotel,hotelorder,dns);
						}
					}
				} else if ("E".equals(orderStateCode)) {
					hotelorder.setOutorderstate(8l);//已取消
					hotelorder.setState(6);
					Server.getInstance().getHotelService().updateHotelorderIgnoreNull(hotelorder);
					writehotelorderrc("系统自动取消订单", hotelorder);
				} else if ("O".equals(orderStateCode)) {
					hotelorder.setOutorderstate(7l);//满房
					hotelorder.setState(16);//满房
					Server.getInstance().getHotelService().updateHotelorderIgnoreNull(hotelorder);
					writehotelorderrc("系统自动确认订单满房。", hotelorder);
				} else if ("H".equals(orderStateCode)) {
				} else if ("G".equals(orderStateCode)) {
				} else if ("F".equals(orderStateCode)) {
					hotelorder.setState(20);// 已入住（b2c已入住)
					Server.getInstance().getHotelService().updateHotelorderIgnoreNull(hotelorder);
					writehotelorderrc("系统自动确认已入住", hotelorder);
				} else if ("B".equals(orderStateCode)) {
					hotelorder.setState(13);//noshow
					hotelorder.setOutorderstate(9l);//NoShow
					Server.getInstance().getHotelService().updateHotelorderIgnoreNull(hotelorder);
					writehotelorderrc("系统自动确认为NoShow状态", hotelorder);
				} else if ("C".equals(orderStateCode)) {
					hotelorder.setOutorderstate(5l);//已结帐
					hotelorder.setState(22);
					Server.getInstance().getHotelService().updateHotelorderIgnoreNull(hotelorder);
					writehotelorderrc("系统自动更新为已结帐状态", hotelorder);
				} else if ("D".equals(orderStateCode)) {
					hotelorder.setState(6);//订单自动取消
					hotelorder.setOutorderstate(6l);//已删除
					Server.getInstance().getHotelService().updateHotelorderIgnoreNull(hotelorder);
					writehotelorderrc("系统自动取消订单", hotelorder);
				}
			}
		}
		
	}
	private SimpleDateFormat simplefromatyymmdd = new SimpleDateFormat("yyyy-MM-dd");
	private SimpleDateFormat simplefromat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public String formatStringTimetoyyyymmdd(String date) {
		try {
			return this.simplefromatyymmdd.format((simplefromat.parse(date)));

		} catch (Exception e) {
			return null;
		}
	}
	public void sendConfimSms(Hotel hotel,Hotelorder hotelorder,Dnsmaintenance dnsmaintenance){
		try {
			// 发送短信
			String smstemple = "";
			smstemple = SMSTemplet.getSMSTemplet(SMSType.HOTELORDERSCONFIRMATION,dnsmaintenance);
			//您好，您预订的[入住时间]入住的，[酒店名称],[房型名称],订单号[订单号]已确认，酒店地址：[酒店地址]电话：[电话]。如行程有变，请及时与易订行客服联系，客服电话:010-57793325 

			smstemple = smstemple.replace("[入住时间]",formatStringTimetoyyyymmdd(hotelorder.getComedate().toString()));
			smstemple = smstemple.replace("[酒店名称]", hotelorder.getName());
			smstemple = smstemple.replace("[房型名称]", hotelorder.getRoomtypename());
			smstemple = smstemple.replace("[订单号]", hotelorder.getOrderid());
			smstemple = smstemple.replace("[酒店地址]", hotel.getAddress());
			if(hotel.getSourcetype()==1){
				String tall=hotel.getMarkettell();
				if(tall!=null){
					if(tall.contains("艺龙")){
						tall=tall.replaceAll("艺龙", "");
					}
				}
				smstemple = smstemple.replace("[酒店电话]", tall);
			}else{
				smstemple = smstemple.replace("[酒店电话]", hotel.getTortell());
			}
			Server.getInstance().getAtomService().sendSms(
					new String[] { "" + hotelorder.getLinkmobile() + "" },
					smstemple, hotelorder.getId(),
					hotelorder.getCreateuserid(), dnsmaintenance,2);
		} catch (Exception ex) {
			WriteLog.write("信息发送失败", "发送短信失败:" + ex.getMessage());
		}
	}

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		System.out.println("订单状态自动更新程序开始……");
		String where =" where C_WAICODE	is not null and C_WAICODE!='' and C_OUTORDERSTATE not in (5,6,7,8,9) and C_ORDERSOURCE=1";
		List<Hotelorder> hotelOrders = Server.getInstance().getHotelService().findAllHotelorder(where, " order by ID ", -1, 0);
		int i=0;
		int si=hotelOrders.size();
		StringBuffer yelongcode=new StringBuffer();
		List<Hotelorder> orderlist=new ArrayList<Hotelorder>();
		for (Hotelorder hotelorder : hotelOrders) {
			if(hotelorder.getWaicode()!=null){
				i++;
				yelongcode.append(hotelorder.getWaicode().trim()+",");
				si--;
				if(i==10){
					orderlist.add(hotelorder);
					updateHotelOrderStatus(orderlist,yelongcode.toString());
					
					//初始化这些条件
					i=0;
					orderlist=new ArrayList<Hotelorder>();
					yelongcode=new StringBuffer();
				}else{
					if(si==0){
						orderlist.add(hotelorder);
						updateHotelOrderStatus(orderlist,yelongcode.toString());
					}else{
						orderlist.add(hotelorder);
					}
				}
			}
		}
		System.out.println("订单状态自动更新程序结束……");
	}

	public void writehotelorderrc(String content, Hotelorder hotelorder) {
		Hotelorderrc rc = new Hotelorderrc();
		rc.setCreatetime(new Timestamp(System.currentTimeMillis()));
		rc.setContent(content);
		rc.setHandleuser("0");
		rc.setOrderid(hotelorder.getOrderid());
		try {
			rc = Server.getInstance().getHotelService().createHotelorderrc(rc);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		long backstatus = rc.getId();
		if (backstatus > 0) {
			WriteLog.write("订单处理记录", "处理成功" + backstatus);
		} else {
			WriteLog.write("订单处理记录", "处理失败" + backstatus);
		}
	}

}
